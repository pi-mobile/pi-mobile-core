/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.qnames;

/**
 * Interface for model keys.
 * Implementation must not have any refernece to the model instance using the key,
 * otherwise e.g. cloneModel(mull) will fail. A reference to model's type is allowed. 
 */
public interface Key {

  public static final String KEY_ATTR = "--KEY-ATTRIBUTE--";
  
  /**
   * Returns the value at keyValueIndex from this key
   * @param keyValueIndex 
   * @return the value at keyValueIndex from this key
   */
  public Object getKeyValue(int keyValueIndex);

  /**
   * Returns the number of values (columns) this key consists of.
   * @return the number of values (columns) this key consists of.
   */
  public int keyValueCount();

  /**
   * Writes this key to a key string: Each key value is converted
   * to Base32. Then the Base32 strings are connected by a '_'
   * in the key values order. If namespace tabel is not null it is
   * used to optimize QNames by usage of namespace prefixes.
   * @param namespaces namespace tabele to be used for optimization
   *                   of QName reparesentation, null for no optimzation
   * @return a string representation of this key
   */
  public String toKeyString( NamespaceTable namespaces );
}
