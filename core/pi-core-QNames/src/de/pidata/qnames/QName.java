/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.qnames;

public class QName implements Key
{
  private Namespace namespace;
  private String    name;
  private QName     next;
  private int       hashCode;
  private String    keyString;

  QName(Namespace namespace, String name) {
    this.namespace = namespace;
    this.name      = name;
    this.hashCode  = StringHash.calcHashCode(name);
  }

  /**
   * Creates a Combined key from its string representation, see toKeyString()
   * @param keyString the strign to create key from
   */
  public static QName fromKeyString( String keyString, NamespaceTable nsTable ) {
    byte[] bytes = Base32.decode( keyString );
    QName key = QName.getInstance( new String(bytes), nsTable );
    if (key.keyString == null) {
      key.keyString = keyString;
    }  
    return key;
  }

  /**
   * Writes this key to a key string: Each key value is converted
   * to Base32. Then the Base32 strings are connected by a '_'
   * in the key values order.
   *
   * @return a string representation of this key  @param namespaces
   */
  public String toKeyString( NamespaceTable namespaces ) {
    if (keyString == null) {
      byte[] bytes = toString( namespaces ).getBytes();
      keyString = Base32.encode( bytes );
    }
    return keyString;
  }

  /**
   * Returns a QName based on the given namespace and name.
   * @param namespace the namespace
   * @param name the name
   * @return QName object
   */
  public static QName getInstance(Namespace namespace, String name) {
    return namespace.getQName(name);
  }

  /**
   * Returns a QName based on the given full qualified name. This
   * means the String is a combination of namespace prefix and name.
   * The namespace part must NOT be a namespace URI.
   * <pre>
   *   namespace.name
   * </pre>
   * @param fullQualifiedQName combination of namespace prefix and name
   * @return QName object  or null if fullQualifiedQName was null
   */
  public static QName getInstance( String fullQualifiedQName, NamespaceTable namespaces ) {
    if (fullQualifiedQName == null) {
      return null;
    }

    Namespace namespace;
    String name;
    String prefix;
    int pos;

    //----- Remove namespace prefix if exists
    pos = fullQualifiedQName.indexOf(':');
    if (pos >= 0) {
      prefix = fullQualifiedQName.substring( 0, pos );
      namespace = namespaces.getByPrefix( prefix );
      if (namespace == null) {
        namespace = Namespace.getInstance( prefix );
      }
      name = fullQualifiedQName.substring(pos+1 );
    }
    else {
      name = fullQualifiedQName;
      namespace = namespaces.getDefaultNamespace();
      if (namespace == null) {
        throw new IllegalArgumentException( "QName has no namespace-prefix and default namespace is not defined, QName="+fullQualifiedQName );
      }
    }

    return getInstance( namespace, name) ;
  }

  public static QName getInstance( char[] buffer, int startIndex, int endIndex, NamespaceTable nsTable ) {
    Namespace ns;
    int i = startIndex;
    while ((i < endIndex) && (buffer[i] != ':')) i++;
    if (i < endIndex) {
      ns = nsTable.getByPrefix( buffer, startIndex, i);
      if (ns == null) {
        throw new IllegalArgumentException( "Namespace not found for prefix=" + new String( buffer, startIndex, i-startIndex ) );
      }
      startIndex = i + 1;
    }
    else {
      ns = nsTable.getDefaultNamespace();
    }
    return ns.getQName(buffer, startIndex, endIndex);
  }

  public String getName()
  {
    return this.name;
  }

  public Namespace getNamespace()
  {
    return namespace;
  }

  /**
   * Returns the next QName stored at the same hash index than this QName
   *
   * @return the next QName stored at the same hash index than this QName
   */
  public QName getNext()
  {
    return next;
  }

  /**
   * Sets the next QName stored at the same hash index than this QName.
   * This Method must not be called by anyone others than StringHash.
   *
   * @param next the next QName stored at the same hash index than this QName
   */
  void setNext(QName next)
  {
    this.next = next;
  }

  /**
   * Returns a hash code value for the object. This method is
   * supported for the benefit of hashtables such as those provided by
   * <code>java.util.Hashtable</code>.
   *
   * StrignID get its has code from <code>name.hashCode()</code>. That
   * value is calculated on construction of QName and storted in an
   * attribute.
   *
   * @return  a hash code value for this object.
   * @see     Object#equals(Object)
   * @see     java.util.Hashtable
   */
  public int hashCode()
  {
    return this.hashCode;
  }

  /**
   * Returns true if value's name eqals this StrignID's name
   *
   * @param   value the value to be compared
   * @return  true if value's name eqals this StrignID's name
   */
  public boolean equalsName(QName value)
  {
    return value.getName().equals(this.name);
  }

  /**
   * Returns true if name eqals this StrignID's name
   *
   * @param   name the name to be compared
   * @return  true if name eqals this StrignID's name
   */
  public boolean equalsName(String name)
  {
    return name.equals(this.name);
  }

  /**
   * Returns true if the character sequence between startIndex (inclusive) and
   * endIndex (exclusive) within buffer equals to this QName's name
   *
   * @param buffer     the buffer containing the character sequence to be compared
   * @param startIndex the start index (inclusive) within buffer
   * @param endIndex   the end index (exclucsive) within buiffer
   * @return true if the character sequence equals this QName's name
   */
  public boolean equalsName(StringBuffer buffer, int startIndex, int endIndex)
  {
    int len = this.name.length();
    if (len != (endIndex - startIndex))
    {
      return false;
    }

    int pos = 0;
    String name = this.name;
    while ((pos < len) && (buffer.charAt(startIndex+pos) == name.charAt(pos)))
    {
      pos++;
    }
    return (pos == len);
  }

  /**
   * Returns true if the character sequence between startIndex (inclusive) and
   * endIndex (exclusive) within buffer equals to this QName's name
   *
   * @param buffer     the buffer containing the character sequence to be compared
   * @param startIndex the start index (inclusive) within buffer
   * @param endIndex   the end index (exclucsive) within buiffer
   * @return true if the character sequence equals this QName's name
   */
  public boolean equalsName(char[] buffer, int startIndex, int endIndex)
  {
    int len = this.name.length();
    if (len != (endIndex - startIndex))
    {
      return false;
    }

    int pos = 0;
    String name = this.name;
    while ((pos < len) && (buffer[startIndex+pos] == name.charAt(pos)))
    {
      pos++;
    }
    return (pos == len);
  }

  /**
   * Returns a string representation of the object.
   *
   * @return  a string representation of the object.
   */
  public String toString()
  {
    return this.namespace.toString() + ":" + this.name;
  }

  /**
   * Returns optimized string representation of this QName. Uses prefixes
   * defined in namespaces
   * @param namespaces namespace table defining prefixes
   * @return optimized string representation of this QName
   */
  public String toString( NamespaceTable namespaces ) {
    String prefix;
    if (namespaces == null) {
      prefix = null;
    }
    else {
      prefix = namespaces.getPrefix( this.namespace );
    }
    if (prefix == null) {
      prefix = namespaces.getOrCreatePrefix( this );
      namespaces.addNamespace( namespace, prefix );
    }
    if (prefix.equals( "" )) {
      return this.name;
    }
    else {
      return prefix + ':' + this.name;
    }
  }

  /**
   * Returns the value at keyValueIndex from this key
   *
   * @param keyValueIndex
   * @return the value at keyValueIndex from this key
   */
  public Object getKeyValue(int keyValueIndex) {
    if (keyValueIndex != 0) {
      throw new IllegalArgumentException("This key only has one value, but keyValueIndex="+keyValueIndex);
    }
    return this;
  }

  /**
   * Returns the number of values (columns) this key consists of.
   *
   * @return the number of values (columns) this key consists of.
   */
  public int keyValueCount() {
    return 1;
  }

  public static String getNameOrNull( QName qName ) {
    if (qName == null) {
      return null;
    }
    else {
      return qName.getName();
    }
  }
}


