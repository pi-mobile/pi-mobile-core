/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.qnames;

public class StringHash
{
  /**
   * The uri this hash binding is used for
   */
  private Namespace namespace;

  /**
   * The hash binding data.
   */
  private transient QName table[];

  /**
   * The total number of entries in the hash binding.
   */
  private transient int count;

  /**
   * The binding is rehashed when its size exceeds this threshold.  (The
   * value of this field is (int)(capacity * loadFactor).)
   */
  private int threshold;

  /**
   * The load factor for the hashtable in per cent.
   */
  private int loadFactor;

  /**
   * The number of times this Hashtable has been structurally modified
   * Structural modifications are those that change the number of entries in
   * the Hashtable or otherwise modify its internal structure (e.g.,
   * rehash).  This field is used to make iterators on Collection-views of
   * the Hashtable fail-fast.  (See ConcurrentModificationException).
   */
  private transient int modCount = 0;

  /**
   * Constructs a new, empty hashtable with the specified initial
   * capacity and the specified load factor.
   *
   * @param      namespace         the uri this hash binding is used for
   * @param      initialCapacity   the initial capacity of the hashtable.
   * @param      loadFactor        the load factor of the hashtable in per cent
   * @exception  IllegalArgumentException  if the initial capacity is less
   *             than zero, or if the load factor is nonpositive.
   */
  public StringHash(Namespace namespace, int initialCapacity, int loadFactor)
  {
    if (initialCapacity < 0)
      throw new IllegalArgumentException("Illegal Capacity: " +
                                         initialCapacity);
    if (loadFactor <= 0)
      throw new IllegalArgumentException("Illegal Load: " + loadFactor);

    this.namespace = namespace;

    if (initialCapacity == 0)
      initialCapacity = 1;
    this.loadFactor = loadFactor;
    table = new QName[initialCapacity];
    threshold = initialCapacity * loadFactor / 100;
  }

  /**
   * Constructs a new, empty hashtable with the specified initial capacity
   * and default load factor, which is <tt>0.75</tt>.
   *
   * @param      namespace         the uri this hash binding is used for
   * @param     initialCapacity   the initial capacity of the hashtable.
   * @exception IllegalArgumentException if the initial capacity is less
   *              than zero.
   */
  public StringHash(Namespace namespace, int initialCapacity)
  {
    this(namespace, initialCapacity, 75);
  }

  /**
   * Constructs a new, empty hashtable with a default initial capacity (11)
   * and load factor, which is <tt>0.75</tt>.
   *
   * @param      namespace         the uri this hash binding is used for
   */
  public StringHash(Namespace namespace)
  {
    this(namespace, 11, 75);
  }

  /**
   * Returns the number of keys in this hashtable.
   *
   * @return  the number of keys in this hashtable.
   */
  public int size()
  {
    return count;
  }

  /**
   * Tests if this hashtable maps no keys to values.
   *
   * @return  <code>true</code> if this hashtable maps no keys to values;
   *          <code>false</code> otherwise.
   */
  public boolean isEmpty()
  {
    return count == 0;
  }

  /**
   * Returns the QName for value in this binding or null
   *
   * @param   value   the value to look for
   * @return  the QName for value in this binding or null
   */
  public QName get(String value)
  {
    QName[] tab = table;
    QName   entry;
    int        hash = calcHashCode(value);
    int        index = (hash & 0x7FFFFFFF) % tab.length;

    for (entry = tab[index]; entry != null; entry = entry.getNext())
    {
      if ((entry.hashCode() == hash) && entry.equalsName(value))
      {
        return entry;
      }
    }
    return null;
  }

  /**
   * Returns the value to which the specified key is mapped in this hashtable.
   *
   * @param buffer      the buffer containing the character sequence
   * @param startIndex index of the first character inside buffer
   * @param endIndex   1 + index of the last character inside buffer
   * @return  the value to which the key is mapped in this hashtable;
   *          <code>null</code> if the key is not mapped to any value in
   *          this hashtable.
   */
  public QName get(StringBuffer buffer, int startIndex, int endIndex)
  {
    QName[] tab = table;
    QName   entry;
    int        hash = calcHashCode(buffer, startIndex, endIndex);
    int        index = (hash & 0x7FFFFFFF) % tab.length;

    for (entry = tab[index]; entry != null; entry = entry.getNext())
    {
      if ((entry.hashCode() == hash) && entry.equalsName(buffer, startIndex, endIndex))
      {
        return entry;
      }
    }
    return null;
  }

  /**
   * Returns the value to which the specified key is mapped in this hashtable.
   *
   * @param buffer      the buffer containing the character sequence
   * @param startIndex index of the first character inside buffer
   * @param endIndex   1 + index of the last character inside buffer
   * @return  the value to which the key is mapped in this hashtable;
   *          <code>null</code> if the key is not mapped to any value in
   *          this hashtable.
   */
  public QName get(char[] buffer, int startIndex, int endIndex)
  {
    QName[] tab = table;
    QName   entry;
    int        hash = calcHashCode(buffer, startIndex, endIndex);
    int        index = (hash & 0x7FFFFFFF) % tab.length;

    for (entry = tab[index]; entry != null; entry = entry.getNext())
    {
      if ((entry.hashCode() == hash) && entry.equalsName(buffer, startIndex, endIndex))
      {
        return entry;
      }
    }
    return null;
  }

  /**
   * Increases the capacity of and internally reorganizes this
   * hashtable, in order to accommodate and access its entries more
   * efficiently.  This method is called automatically when the
   * number of keys in the hashtable exceeds this hashtable's capacity
   * and load factor.
   */
  protected void rehash()
  {
    QName[] oldTable    = table;
    int        oldCapacity = oldTable.length;
    int        newCapacity = oldCapacity * 2 + 1;
    QName   newTable[]  = new QName[newCapacity];
    QName   oldEntry, newEntry;
    int        index;

    this.modCount++;
    this.threshold = newCapacity * loadFactor / 100;
    this.table     = newTable;

    for (int i = oldCapacity; i-- > 0;)
    {
      for (oldEntry = oldTable[i]; oldEntry != null;)
      {
        newEntry = oldEntry;
        oldEntry = oldEntry.getNext();

        index = (newEntry.hashCode() & 0x7FFFFFFF) % newCapacity;
        newEntry.setNext(newTable[index]);
        newTable[index] = newEntry;
      }
    }
  }

  /**
   * Calculates the hash code for the characters between startIndex (included) and
   * endIndex (excluded) of buffer.
   *
   * @param buffer      the buffer containing the character sequence
   * @param startIndex index of the first character inside buffer
   * @param endIndex   1 + index of the last character inside buffer
   * @return the calculated hash code
   */
  public static final int calcHashCode(StringBuffer buffer, int startIndex, int endIndex)
  {
    int result = 0;
    for (int i = startIndex; i < endIndex; i++)
    {
      result += buffer.charAt(i);
    }
    return result;
  }

  /**
   * Calculates the hash code for the characters of value
   *
   * @param value      the value from which the has code is calculated
   * @return the calculated hash code
   */
  public static final int calcHashCode(String value)
  {
    int endIndex = value.length();
    int result = 0;
    for (int i = 0; i < endIndex; i++)
    {
      result += value.charAt(i);
    }
    return result;
  }

  /**
   * Calculates the hash code for the characters between startIndex (included) and
   * endIndex (excluded) of buffer.
   *
   * @param buffer      the buffer containing the character sequence
   * @param startIndex index of the first character inside buffer
   * @param endIndex   1 + index of the last character inside buffer
   * @return the calculated hash code
   */
  public static final int calcHashCode(char[] buffer, int startIndex, int endIndex)
  {
    int result = 0;
    for (int i = startIndex; i < endIndex; i++)
    {
      result += buffer[i];
    }
    return result;
  }

  /**
   * Looks for a QName having a name equal to the character sequence
   * between startIndex (inclusive) and endIndex (exclusive) inside buffer.
   * If that QName does not exist it is created.
   *
   * If a new StrignID shall not be created use teh <code>get</code> method.
   *
   * @param buffer      the buffer containing the character sequence
   * @param startIndex index of the first character inside buffer
   * @param endIndex   1 + index of the last character inside buffer
   * @return the QName for the character sequence
   * @exception  NullPointerException  if the buffer is null
   * @see     Object#equals(Object)
   */
  public synchronized QName put(StringBuffer buffer, int startIndex, int endIndex)
  {
    int        hash, tableIndex;
    QName[] tab;
    QName   entry;
    char[]     chars;

    //do not add empty Strings
    if (startIndex >= endIndex) {
      return null;
    }

    // Makes sure the key is not already in the hashtable.
    entry = get(buffer, startIndex, endIndex);

    if (entry == null)
    {
      // Creates the new entry.
      tab = this.table;
      chars = new char[endIndex-startIndex];
      buffer.getChars(startIndex, endIndex, chars, 0);
      entry = new QName(this.namespace, new String(chars));
      tableIndex = (entry.hashCode() & 0x7FFFFFFF) % tab.length;;
      entry.setNext(tab[tableIndex]);
      tab[tableIndex] = entry;
      count++;

      modCount++;
      if (count >= threshold)
      {
        // Rehash the binding if the threshold is exceeded
        rehash();
      }
    }
    return entry;
  }

  /**
   * Looks for a QName having a name equal to the character sequence
   * between startIndex (inclusive) and endIndex (exclusive) inside buffer.
   * If that QName does not exist it is created.
   *
   * If a new StrignID shall not be created use teh <code>get</code> method.
   *
   * @param buffer      the buffer containing the character sequence
   * @param startIndex index of the first character inside buffer
   * @param endIndex   1 + index of the last character inside buffer
   * @return the QName for the character sequence
   * @exception  NullPointerException  if the buffer is null
   * @see     Object#equals(Object)
   */
  public synchronized QName put(char[] buffer, int startIndex, int endIndex)
  {
    int        hash, tableIndex;
    QName[] tab;
    QName   entry;

    //do not add empty Strings
    if (startIndex >= endIndex) {
      return null;
    }

    // Makes sure the key is not already in the hashtable.
    entry = get(buffer, startIndex, endIndex);

    if (entry == null)
    {
      // Creates the new entry.
      tab = this.table;
      entry = new QName(this.namespace, new String(buffer,startIndex, endIndex-startIndex));
      tableIndex = (entry.hashCode() & 0x7FFFFFFF) % tab.length;;
      entry.setNext(tab[tableIndex]);
      tab[tableIndex] = entry;
      count++;

      modCount++;
      if (count >= threshold)
      {
        // Rehash the binding if the threshold is exceeded
        rehash();
      }
    }
    return entry;
  }

  /**
   * Looks for a QName having a name equal value.
   * If that QName does not exist it is created.
   *
   * If a new StrignID shall not be created use the <code>get</code> method.
   *
   * @param value   the value to be put into the binding
   * @return the QName for the character sequence
   * @exception  NullPointerException  if the value is null.
   * @see     Object#equals(Object)
   */
  public synchronized QName put(String value)
  {
    int        tableIndex;
    QName[] tab;
    QName   entry;

    //do not add empty Strings
    if ((value == null) || (value.length() == 0)) {
      return null;
    }

    // Makes sure the key is not already in the hashtable.
    entry = get(value);

    if (entry == null)
    {
      // Creates the new entry.
      tab = this.table;
      entry = new QName(this.namespace, value);
      tableIndex = (entry.hashCode() & 0x7FFFFFFF) % tab.length;;
      entry.setNext(tab[tableIndex]);
      tab[tableIndex] = entry;
      count++;

      modCount++;
      if (count >= threshold)
      {
        // Rehash the binding if the threshold is exceeded
        rehash();
      }
    }
    return entry;
  }

  /**
   * Removes the key (and its corresponding value) from this
   * hashtable. This method does nothing if the key is not in the hashtable.
   *
   * @param   value   the key that needs to be removed.
   */
  public void remove(QName value)
  {
    QName[] tab   = table;
    int        hash  = value.hashCode();
    int        index = (hash & 0x7FFFFFFF) % tab.length;
    QName   entry, prev;

    for (entry = tab[index], prev = null; entry != null; prev = entry, entry = entry.getNext())
    {
      if (entry == value)
      {
        modCount++;
        if (prev != null)
        {
          prev.setNext(entry.getNext());
        }
        else
        {
          tab[index] = entry.getNext();
        }
        count--;
      }
    }
  }

  /**
   * Clears this hashtable so that it contains no keys.
   */
  public void clear()
  {
    QName tab[] = table;
    modCount++;
    for (int index = tab.length; --index >= 0;)
    {
      tab[index] = null;
    }
    count = 0;
  }

  /**
   * Returns the hash code value for this Map as per the definition in the
   * Map interface.
   */
  public int hashCode()
  {
    /*
     * This code detects the recursion caused by computing the hash code
     * of a self-referential hash binding and prevents the stack overflow
     * that would otherwise result.  This allows certain 1.1-era
     * applets with self-referential hash tables to work.  This code
     * abuses the loadFactor field to do double-duty as a hashCode
     * in progress flag, so as not to worsen the space performance.
     * A negative load factor indicates that hash code computation is
     * in progress.
     */
    int h = 0;
    if (count == 0 || loadFactor < 0)
      return h;  // Returns zero

    loadFactor = -loadFactor;  // Mark hashCode computation in progress
    QName[] tab = table;
    for (int i = 0; i < tab.length; i++)
    {
      for (QName e = tab[i]; e != null; e = e.getNext())
      {
        h += e.hashCode();
      }
    }
    loadFactor = -loadFactor;  // Mark hashCode computation complete

    return h;
  }
}