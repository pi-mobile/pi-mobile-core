/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.qnames;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Namespace {
  
  private static List<Namespace> namespaces = new ArrayList<Namespace>();
  private String uri;
  private StringHash qNames = new StringHash(this);

  /**
   * Returns the Namespace for the given namespaceURI or prefix. If it does not exist
   * a new one is created.
   *
   * @param namespaceUri the Namespace's URI or prefix
   * @return the Namespace tor namespaceURI
   */
  public static synchronized Namespace getInstance(String namespaceUri) {
    Namespace result;

    result = findNamespace(namespaceUri);

    if (result == null) {
      result = new Namespace(namespaceUri);
      namespaces.add(result);
    }
    return result;
  }

  public static boolean equalsString(String compStr, char[] buffer, int startIndex, int endIndex) {
    int len = compStr.length();
    if (len != (endIndex - startIndex)) {
      return false;
    }
    int pos = 0;
    while ((pos < len) && (buffer[startIndex+pos] == compStr.charAt(pos))) {
      pos++;
    }
    return (pos == len);
  }

  private static Namespace findNamespace(String namespaceUri) {
    Namespace ns;
    if (namespaceUri == null) {
      namespaceUri = "";
    }
    for (int i = namespaces.size()-1; i >= 0; i--) {
      ns = namespaces.get(i);
      if (ns.getUri().equals(namespaceUri)) {
        return ns;
      }
    }
    return null;
  }

  /**
   * Returns count of registered Namespaces
   * @return count of registered Namespaces
   */
  public static int nsCount() {
    return namespaces.size();
  }

  /**
   * Returns an iterator over all registered namespaces
   * @return an iterator over all registered namespaces
   */
  public static Iterator nsEnum() {
    return namespaces.iterator();
  }

  //-----------------------------------------------------------------------------------------

  private Namespace(String namespaceUri) {
    this.uri = namespaceUri;
  }

  public String toString() {
    return uri;
  }

  public String getUri() {
    return uri;
  }

  /**
   * Returns the QName object for name within this uri. If that QName does
   * not exist it is created.
   * @param buffer      the buffer containing the character sequence
   * @param startIndex index of the first character inside buffer
   * @param endIndex   1 + index of the last character inside buffer
   * @return the QName object for name within this uri
   */
  public QName getQName(char[] buffer, int startIndex, int endIndex) {
    return this.qNames.put(buffer, startIndex, endIndex);
  }

  /**
   * Returns the QName object for name within this uri. If that QName does
   * not exist it is created.
   * @param buffer      the buffer containing the character sequence
   * @param startIndex index of the first character inside buffer
   * @param endIndex   1 + index of the last character inside buffer
   * @return the QName object for name within this uri
   */
  public QName getQName(StringBuffer buffer, int startIndex, int endIndex) {
    return this.qNames.put(buffer, startIndex, endIndex);
  }

  /**
   * Returns the QName object for name within this uri. If that QName does
   * not exist it is created.
   * @param  name the name to be encapsulated by the QName
   * @return the QName object for name within this uri
   */
  public QName getQName(String name) {
    return this.qNames.put(name);
  }
}
