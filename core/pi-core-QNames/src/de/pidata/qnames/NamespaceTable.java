/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.qnames;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This class keeps a table of namespaces and prefixes used is a specific
 * context.
 */
public class NamespaceTable {

  private NamespaceTable parent;
  private Object owner = null;
  private List<String> prefixTable = new ArrayList<String>();
  private List<Namespace> namespaceTable = new ArrayList<Namespace>();

  /**
   * keeps the number to assign to the next prefix which will be newly build
   */
  private int newPrefixesCount = 0;

  public NamespaceTable() {
    this.parent = null;
  }

  public NamespaceTable( NamespaceTable parent ) {
    this.parent = parent;
  }

  public NamespaceTable( String[] definitions ) {
    if (definitions != null) {
      for (int i = 0; i < definitions.length; i++) {
        int pos = definitions[i].indexOf( '=' );
        if (pos < 0) {
          throw new IllegalArgumentException( "Invalid definition format at index="+i+": "+definitions[i] );
        }
        else {
          prefixTable.add( definitions[i].substring( 0, pos ) );
          namespaceTable.add( Namespace.getInstance( definitions[i].substring( pos+1 ) ) );
        }
      }
    }
  }

  public Object getOwner() {
    return owner;
  }

  public void setOwner( Object owner ) {
    this.owner = owner;
  }

  /**
   * Tries to find a parent NamespaceTable by calling namspaceTable() on
   * owner's parent Model, if owner and owner's parent are not null.
   *
   * @return parent NamspaceTable or null if NamespaceTable does not exist
   */
  public NamespaceTable getParent() {
    return parent;
  }

  public Namespace getByPrefix( String prefix ) {
    int index = prefixTable.indexOf( prefix );
    if (index >= 0) {
      return namespaceTable.get( index );
    }
    else if (parent == null) {
      return null;
    }
    else {
      return parent.getByPrefix( prefix );
    }
  }

  public Namespace getByPrefix( char[] buffer, int startIndex, int endIndex ) {
    if (endIndex <= (startIndex+1)) {
      throw new IllegalArgumentException("endIndex must at least be startIndex+2");
    }
    for (int i = prefixTable.size()-1; i >= 0; i--) {
      String prefix = prefixTable.get( i );
      if (Namespace.equalsString( prefix, buffer, startIndex, endIndex )) {
        return namespaceTable.get( i );
      }
    }
    if (parent == null) {
      return null;
    }
    else {
      return parent.getByPrefix( buffer, startIndex, endIndex );
    }
  }

  public Namespace getDefaultNamespace() {
    return getByPrefix( "" );
  }

  public String getPrefix( Namespace ns ) {
    int index = namespaceTable.indexOf( ns );
    if (index >= 0) {
      return prefixTable.get( index );
    }
    else {
      return null;
    }
  }

  public Iterator<Namespace> getNamespaceIter() {
    return namespaceTable.iterator();
  }

  /**
   * Returns the the size of this NamespaceTable, i.e. the number of prefix namespace pairs
   *
   * @return the size of this table
   */
  public int size() {
    return namespaceTable.size();
  }

  /**
   * Removes all entries from this namespace table
   */
  public void removeAll() {
    this.prefixTable.clear();
    this.namespaceTable.clear();
    this.newPrefixesCount = 0;
  }

  public StringBuilder buildNamespaceDeclaration( boolean prettyPrint ) {
    StringBuilder nsDeclaration = new StringBuilder();
    Namespace nameSpace;
    String prefix;
    for (int i = 0; i < prefixTable.size(); i++) {
      if (prettyPrint) nsDeclaration.append( "\n" ); else nsDeclaration.append( " " );
      nameSpace = namespaceTable.get( i );
      prefix = prefixTable.get( i );
      if (prettyPrint) nsDeclaration.append( "\t" );
      if ("".equals( prefix )) { // default namespace
        nsDeclaration.append( "xmlns" + "=\"" + nameSpace.toString() + "\"" );
      }
      else {
        nsDeclaration.append( "xmlns:" + prefix + "=\"" + nameSpace.toString() + "\"" );
      }
    }
    return nsDeclaration;
  }

  public boolean isDefined( String prefix, Namespace namespace ) {
    int oldIndex = prefixTable.indexOf( prefix );
    if (oldIndex >= 0) {
      if (namespaceTable.get( oldIndex ) != namespace) {
        throw new RuntimeException( "prefix '" + prefix + "' already in use for another namespace: " + namespaceTable.get( oldIndex ) );
      }
      else {
        return true;
      }
    }
    else {
      return false;
    }
  }

  public void addNamespace( Namespace namespace, String prefix ) {
    if (prefix == null) {
      throw new IllegalArgumentException( "prefix must not be null (for default Namespace use empty String)" );
    }
    if (namespaceTable == null) {
      namespaceTable = new ArrayList<Namespace>();
      prefixTable = new ArrayList<String>();
    }
    if (!isDefined( prefix, namespace )) {
      namespaceTable.add( namespace );
      prefixTable.add( prefix );
    }
  }

  public void checkOrAddNamespace( Namespace namespace, String prefix ) {
    Namespace foundNamespace = namespaceTable.stream().filter( ns -> ns.equals( namespace ) ).findFirst().orElse( null );
    if (foundNamespace == null) {
      addNamespace( namespace, prefix );
    }
  }

  /**
   * Return a prefix for the namespace of the given QName.
   * If no prefix exists for id, a new prefix is created.
   * The namspace definition will be searched within the namespace table hierarchy.
   * If model is null, a new prefix will be build.
   *
   * @param id the QName a namespace prefix is needed for
   * @return the namespace prefix for the given id
   */

  public String getOrCreatePrefix( QName id ) {
    Namespace namespace = id.getNamespace();
    return getOrCreatePrefix( namespace, true );
  }

  public String getOrCreatePrefix( QName id, boolean allowDefault ) {
    Namespace namespace = id.getNamespace();
    return getOrCreatePrefix( namespace, allowDefault );
  }

  public String getOrCreatePrefix( Namespace namespace, boolean allowDefault ) {
    String prefix;
    int ind = namespaceTable.indexOf( namespace );
    if (ind >= 0) {
      prefix = prefixTable.get( ind );
    }
    else {
      prefix = createNewPrefix( namespace, allowDefault );
    }
    return prefix;
  }

  /**
   * Creates a new prefix for Namespace ns and store Namespace and prefix in this NamspaceTable.
   *
   * @param ns           the Namespace to create a prefix for
   * @param allowDefault if true and default Namespace not alread defined defautl ns is used
   * @return the new prefix
   */
  public String createNewPrefix( Namespace ns, boolean allowDefault ) {
    String prefix;
    if (allowDefault) {
      prefix = "";
    }
    else {
      prefix = "ns" + newPrefixesCount++;
    }
    while (prefixTable.indexOf( prefix ) >= 0) {
      prefix = "ns" + newPrefixesCount++;
    }
    addNamespace( ns, prefix );
    return prefix;
  }

  /**
   * Add the namespaces defined within additionalNamespaces to the current namespaces
   *
   * @param additionalNamespaces NamespaceTabel containing the namespaces and prefixes to be added
   */
  public void addNamespaces( NamespaceTable additionalNamespaces ) {
    for (int i = 0; i < additionalNamespaces.namespaceTable.size(); i++) {
      Namespace locNamespace = additionalNamespaces.namespaceTable.get( i );
      String locPrefix = additionalNamespaces.prefixTable.get( i );

      int ind = namespaceTable.indexOf( locNamespace );
      if (ind >= 0) {
        // namespace already defined - ignore current prefix definition
      }
      else {
        ind = prefixTable.indexOf( locPrefix );
        if (ind >= 0) {
          // prefix already in use - create new one
          addNamespace( locNamespace, createNewPrefix( locNamespace, true ) );
        }
        else {
          // namespace not defined yet - add to global namespace list
          addNamespace( locNamespace, locPrefix );
        }
      }
    }
  }


  public String[] toStringArray() {
    String[] result = new String[namespaceTable.size()];
    for (int i = 0; i < namespaceTable.size(); i++) {
      result[i] = prefixTable.get( i ) + "=" + namespaceTable.get( i );
    }
    return result;
  }
}
