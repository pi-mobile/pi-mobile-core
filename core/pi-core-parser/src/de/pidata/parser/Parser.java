/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.parser;

import de.pidata.log.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;

public abstract class Parser {

  protected StringBuilder inputBuffer = new StringBuilder();
  protected int inputPos = -1;
  private List<ParseRange> parseRanges;
  protected InputStream dataStream;
  protected InputStreamReader reader;
  private int row;
  private int col;
  private int currentStartPos;
  private int currentStartRow;
  private int currentStartCol;
  private int currentEndPos;
  private int currentEndRow;
  private int currentEndCol;
  private boolean dumpOnError = true;

  public void initBuffer( InputStream dataStream, String encoding ) throws UnsupportedEncodingException {
    this.dataStream = dataStream;
    this.reader = new InputStreamReader( dataStream, encoding );
    this.inputBuffer.setLength( 0 );
    this.parseRanges = new LinkedList<ParseRange>();
    this.inputPos = 0;
    this.row = 0;
    this.col = 0;
  }

  public boolean isDumpOnError() {
    return dumpOnError;
  }

  public void setDumpOnError( boolean dumpOnError ) {
    this.dumpOnError = dumpOnError;
  }

  public abstract void doParse() throws IOException;

  protected void setCurrentStartPos() {
    currentStartPos = inputPos;
    currentStartCol = col;
    currentStartRow = row;
  }

  protected void setCurrentStartOnEndPos() {
    currentStartPos = currentEndPos;
    currentStartCol = currentEndCol;
    currentStartRow = currentEndRow;
  }

  protected void setCurrentEndPos() {
    currentEndPos = inputPos;
    currentEndCol = col;
    currentEndRow = row;
  }

  protected void addParseRange( Object parsedObject ) throws IOException {
    if (currentEndPos <= currentStartPos) {
      parseError( "New ParseRange must have at least one character, currentStartPos="+currentStartPos+", currentEndPos="+currentEndPos );
    }
    if (parseRanges.size() > 0) {
      ParseRange lastRange = parseRanges.get( parseRanges.size() - 1 );
      int lastPos = lastRange.getEndPos();
      if (currentStartPos < lastPos) {
        parseError( "New ParseRange must not start before end of last range="+lastRange+", currentStartPos="+currentStartPos );
      }
    }
    parseRanges.add( new ParseRange( parsedObject, currentStartPos, currentStartRow, currentStartCol, currentEndPos, currentEndRow, currentEndCol ) );
    currentStartPos = currentEndPos;
    currentStartRow = currentEndRow;
    currentStartCol = currentEndCol;
  }

  protected void addParseRangeFromStart( Object parsedObject, int numChars ) throws IOException {
    int endPos = currentEndPos;
    currentEndPos = currentStartPos + numChars;
    addParseRange( parsedObject );
    currentStartPos = currentEndPos + 1;
    currentEndPos = endPos;
  }

  public int nextChar() throws IOException {
    int prevChar = -1;
    int result;
    if (inputPos > inputBuffer.length()) {
      return -1;
    }
    if (inputPos > 0) {
      prevChar = inputBuffer.charAt( inputPos-1 );
    }
    if (inputPos < inputBuffer.length()) {
      result = inputBuffer.charAt( inputPos );
      inputPos++;
    }
    else {
      result = reader.read();
      if (result > 0) {
        inputBuffer.append( (char) result );
        inputPos = inputBuffer.length();
      }
      else {
        inputPos = inputBuffer.length() + 1;
      }
    }
    if (prevChar == '\n'){
      col = 0;
      row++;
    }
    else {
      col++;
    }
    return result;
  }

  public int currentChar() {
    if (inputPos > inputBuffer.length()) {
      return -1; // last read already reached end of data stream
    }
    else if (inputPos > 0) {
      return inputBuffer.charAt( inputPos - 1 );
    }
    else {
      return 0;
    }
  }

  protected void back() {
    if (inputPos > 0) {
      inputPos--;
    }
    if ((inputPos > 1) && (inputBuffer.charAt( inputPos-1 ) == '\n')) {
      int i = inputPos-1;
      while ((i > 0) && (inputBuffer.charAt( i ) != '\n')) {
        i--;
      }
      col = inputPos - i - 1;
      row--;
    }
    else {
      col--;
    }
  }

  protected int parseUntil( StringBuilder resultBuf, String stopChars, boolean moveCurrentEndPos ) throws IOException {
    int ch = nextChar();
    if (ch == -1) {
      return ch;
    }
    while (stopChars.indexOf( ch ) < 0) {
      resultBuf.append( (char) ch );
      if (moveCurrentEndPos) {
        setCurrentEndPos();
      }
      ch = nextChar();
      if (ch == -1) {
        return ch;
      }
    }
    return ch;
  }

  protected void skipUntil( char stopChar ) throws IOException {
    int ch = nextChar();
    if (ch == -1) return;
    while (ch != stopChar) {
      ch = nextChar();
      if (ch == -1) return;
    }
  }

  protected int skipWhitespace( boolean moveStartPos ) throws IOException {
    if (moveStartPos) {
      setCurrentStartPos();
    }
    int ch = nextChar();
    if (ch == -1) return -1;
    while (ch <= ' ') {
      if (moveStartPos) {
        setCurrentStartPos();
      }
      ch = nextChar();
      if (ch == -1) return -1;
    }
    return ch;
  }

  protected int skipBlank() throws IOException {
    int ch = nextChar();
    if (ch == -1) return -1;
    while ((ch == ' ') || (ch == '\t')) {
      ch = nextChar();
      if (ch == -1) return -1;
    }
    return ch;
  }

  protected void dumpRemainingMsg() {
    int pos = inputPos;
    try {
      int ch;
      do {
        ch = nextChar();
      } while (ch != -1);
    }
    catch (Exception e) {
      // ignore
    }
    Logger.info( "RemainingMsg="+inputBuffer.substring( pos ) );
  }

  protected void parseError( String message ) throws IOException {
    int start = inputPos - 30;
    int end = inputPos;
    message = "@"+row+","+col+" " + message + ", input=";
    if (dumpOnError) {
      dumpRemainingMsg();
    }
    if (start <= 0) {
      throw new ParseException( message + inputBuffer.substring( 0, end ) );
    }
    else {
      throw new ParseException( message + inputBuffer.substring( start, end ) );
    }
  }

  protected void endOfInputError( String message ) throws IOException {
    int start = inputPos - 30;
    if (start < 0) start = 0;
    throw new IOException( message + ", ("+row+","+col+") input=.."+inputBuffer.substring( start, inputPos ) );
  }

  protected void parseFixedString( String fixedString, boolean ignoreCase ) throws IOException {
    int ch;
    if (ignoreCase) {
      fixedString = fixedString.toLowerCase();
    }
    for (int i = 0; i < fixedString.length(); i++ ) {
      ch = nextChar();
      char expectedChar = fixedString.charAt( i );
      if (ch == -1) {
        endOfInputError( "Unexpected end of input, expected char='"+expectedChar+"'" );
      }
      else {
        if (ignoreCase) {
          ch = Character.toLowerCase( ch );
        }
        if (ch != expectedChar) {
          parseError( "Wrong character, expected char='"+expectedChar+"', but is='"+ch+"'" );
        }
      }
    }
  }

  protected String parseQuotedString( char quoteChar ) throws IOException {
    setCurrentStartPos();
    StringBuilder buffer = new StringBuilder();
    parseUntil( buffer, ""+quoteChar, true );
    return buffer.toString();
  }

  protected String parseName() throws IOException {
    setCurrentStartPos();
    StringBuilder buffer = new StringBuilder();
    int ch = nextChar();
    if (ch == -1) return null;
    while ( ((ch >= 'A') && (ch <= 'Z'))
            || ((ch >= 'a') && (ch <= 'z'))
            || (ch == '_')) {
      buffer.append( (char) ch );
      ch = nextChar();
      if (ch == -1) return buffer.toString();
    }
    return buffer.toString();
  }

  protected int getRow() {
    return row;
  }

  protected int getCol() {
    return col;
  }

  public List<ParseRange> getParseRanges() {
    return parseRanges;
  }
}
