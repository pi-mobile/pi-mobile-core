/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.parser;

public class ParseRange {

  private int startPos;
  private int startRow;
  private int startCol;
  private int endPos;
  private int endRow;
  private int endCol;

  private Object parsedObject;

  public ParseRange( Object parsedObject, int startPos, int startRow, int startCol, int endPos, int endRow, int endCol ) {
    this.startPos = startPos;
    this.startRow = startRow;
    this.startCol = startCol;
    this.endPos = endPos;
    this.endRow = endRow;
    this.endCol = endCol;
    this.parsedObject = parsedObject;
  }

  public int getStartPos() {
    return startPos;
  }

  public int getStartRow() {
    return startRow;
  }

  public int getStartCol() {
    return startCol;
  }

  public int getEndPos() {
    return endPos;
  }

  public int getEndRow() {
    return endRow;
  }

  public int getEndCol() {
    return endCol;
  }

  public Object getParsedObject() {
    return parsedObject;
  }

  @Override
  public String toString() {
    return "["+startPos+"->"+endPos+"],("+startRow+","+startCol+")-("+endRow+","+endCol+"):"+parsedObject;
  }
}
