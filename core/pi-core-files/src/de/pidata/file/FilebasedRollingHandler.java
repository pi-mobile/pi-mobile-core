/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.file;

import de.pidata.log.BaseHandler;
import de.pidata.log.DefaultFormatter;
import de.pidata.log.Logger;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

/**
 * {@link java.util.logging.Handler} implementation which creates and manages a {@link FilebasedRollingLogger}.
 */
public class FilebasedRollingHandler extends BaseHandler {

  private int logfileExpireDays;
  private String logfileName;
  private int logfilePurgeDays;

  public FilebasedRollingHandler() {
    super();
    configure();

    logger = new FilebasedRollingLogger( logfileName, logfileExpireDays, logfilePurgeDays, logLevel );
    Logger.addLogger( logger );
  }

  public FilebasedRollingHandler( String logfileName, int logfileExpireDays, int logfilePurgeDays, int logLevel) {
    super();
    this.logfileName = logfileName;
    this.logfileExpireDays = logfileExpireDays;
    this.logfilePurgeDays = logfilePurgeDays;
  }

  /**
   * Configure a FileHandler from LogManager properties and/or default values
   * as specified in the class javadoc.
   */
  @Override
  public void configure() {
    String cname = getClass().getName();

    // TODO: dependency to system-base to get access to SysremManager?
    String logfileDirName = getStringLogProperty( cname + "." + FilebasedRollingLogger.KEY_DIR, "logs" );
    String baseName = getStringLogProperty( cname + "." + FilebasedRollingLogger.KEY_LOGFILE, "logfile" );

    // TODO: pattern? no user for Android/iOs
    String userName = System.getenv( "USERNAME" );
    String fileName = baseName + "." + userName + FilebasedRollingLogger.LOGFILE_SUFFIX;
    File logFile = new File( logfileDirName, fileName );
    try {
      logfileName = logFile.getCanonicalPath();
    }
    catch (IOException e) {
      System.out.println( "error creating configured logfile; use default instead");
      logfileName = FilebasedRollingLogger.KEY_LOGFILE + FilebasedRollingLogger.LOGFILE_SUFFIX;
    }

    logfileExpireDays = getIntLogProperty( cname + "." + FilebasedRollingLogger.KEY_EXPIREDAYS, 1 );
    if (logfileExpireDays < 0) {
      logfileExpireDays = 0;
    }
    logfilePurgeDays = getIntLogProperty( cname + "." + FilebasedRollingLogger.KEY_PURGEDAYS, 30 );
    if (logfilePurgeDays < 0) {
      logfilePurgeDays = 0;
    }
    Level levelLogProperty = getLevelLogProperty( cname + ".level", Level.ALL );
    logLevel = de.pidata.log.Level.fromValue( levelLogProperty.intValue() );
    setLevel( levelLogProperty );
    setFormatter( new DefaultFormatter() );
  }
}
