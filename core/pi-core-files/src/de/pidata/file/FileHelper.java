package de.pidata.file;

import de.pidata.log.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.List;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * Helper class for handling files and directories.
 *
 * @author Markus Näher
 */
public class FileHelper {

  public static boolean debugEnabled;

  /**
   * Enable debug output according to commandline option DEBUG.
   */
  public static void enableDebug () {
    debugEnabled = true;
  }

  /**
   * Tries to get the canonical path of the given file.
   * If an error occurs, returns the absolute path of the file.
   *
   * @param file the file to examine
   * @return the canonical path if possible, else the absolute path
   */
  public static String getFullPath( File file) {
    String pathName = "- invalid -";
    if (file != null) {
      if (file.exists()) {
        try {
          pathName = file.getCanonicalPath();
        }
        catch (IOException e) {
          // something went wrong, fallback to absolute path if possible
          pathName = file.getAbsolutePath();
        }
      }
      else {
        pathName = file.getAbsolutePath();
      }
    }
    return pathName;
  }

  /**
   * Recursively copies a directory. There is no existence or conflict check.
   * Conflict may be e.g. trying to copy a directory over a file.
   *
   * @param srcPath  souce directory
   * @param destPath destination directory
   * @throws IOException on any error
   */
  public static void copyFolder( File srcPath, File destPath ) throws IOException {
    copyFolder( srcPath, destPath, null, null );
  }

  /**
   * Recursively copies a directory. There is no existence or conflict check.
   * Conflict may be e.g. trying to copy a directory over a file.
   *
   * @param srcPath      souce directory
   * @param destPath     destination directory
   * @param excludeFiles skip files; names are converted to lowercase
   * @throws IOException on any error
   */
  public static void copyFolder( File srcPath, File destPath, List<String> excludeFiles, List<String> excludeExtensions ) throws IOException {
    String srcName = srcPath.getName();
    if (!srcName.equals( destPath.getName() )) {
      destPath = new File( destPath, srcName );
    }
    destPath.mkdirs();
    File[] files = srcPath.listFiles();
    for (File file : files) {
      String filenameLower = file.getName().toLowerCase();
      int extSep = filenameLower.lastIndexOf( "." );
      String fileExt = "";
      if (extSep >= 0) {
        fileExt = filenameLower.substring( extSep + 1 );
      }
      if ((excludeFiles == null || !excludeFiles.contains( filenameLower )) && (excludeExtensions == null || !excludeExtensions.contains( fileExt ))) {
        if (file.isDirectory()) {
          copyFolder( file, new File( destPath, file.getName() ), excludeFiles, excludeExtensions );
        }
        else {
          copyFile( new File( srcPath, file.getName() ), new File( destPath, file.getName() ) );
        }
      }
    }
  }

  /**
   * Copies a single file.
   *
   * @param source source file
   * @param dest   destination file or directory
   * @throws IOException on any error
   */
  public static void copyFile( final File source, final File dest ) throws IOException {
    File destFile = dest.isDirectory() ? new File( dest, source.getName() ) : dest;
    Files.copy( source.toPath(), destFile.toPath(), REPLACE_EXISTING );
  }

  /**
   * Recursively deletes a directory (if it exists).
   *
   * @param path souce directory
   * @throws IOException on any error
   */
  public static void deleteFolder( File path ) throws IOException {
    if (path.exists()) {
      File[] files = path.listFiles();
      for (File file : files) {
        if (file.isDirectory()) {
          deleteFolder( file );
        }
        else {
          if (!file.delete()) {
            throw new IOException( "Error deleting " + file.getCanonicalPath() );
          }
        }
      }
      if (!path.delete()) {
        throw new IOException( "Error deleting " + path.getCanonicalPath() );
      }
    }
  }

  /**
   * Creates the checksum for the given file using the given digest.
   * <br/>
   * Inspired by https://howtodoinjava.com/java/io/sha-md5-file-checksum-hash
   *
   * @param digest the digest to use to create the checksum
   * @param file   the file to get the checksum for
   * @return checksum converted to hexadecimal string
   * @throws IOException
   */
  public static String getFileChecksum( MessageDigest digest, File file ) throws IOException {
    byte[] bytes = getFileChecksumBytes( digest, file );

    //This bytes[] has bytes in decimal format;
    //Convert it to hexadecimal format
    String hexString = byteArrayToHexString( bytes );

    //return complete hash
    return hexString;
  }

  /**
   * Creates the checksum for the given file using the given digest.
   * <br/>
   * Inspired by https://howtodoinjava.com/java/io/sha-md5-file-checksum-hash
   *
   * @param digest the digest to use to create the checksum
   * @param file   the file to get the checksum for
   * @return checksum as byte array
   * @throws IOException
   */
  public static byte[] getFileChecksumBytes( MessageDigest digest, File file ) throws IOException {
    //Get file input stream for reading the file content
    FileInputStream fis = new FileInputStream( file );

    //Create byte array to read data in chunks
    byte[] byteArray = new byte[1024];
    int bytesCount = 0;

    //Read file data and update in message digest
    digest.reset();
    while ((bytesCount = fis.read( byteArray )) != -1) {
      digest.update( byteArray, 0, bytesCount );
    }

    //close the stream; We don't need it now.
    fis.close();

    //Get the hash's bytes
    byte[] bytes = digest.digest();
    if (debugEnabled) {
      Logger.info( digest.getAlgorithm() + ": " + byteArrayToHexString( bytes ) + " [" + file.getName() + "]" );
    }
    return bytes;
  }

  /**
   * Creates the checksum for the given directory using the given digest.
   *
   * @param digest       the digest to use to create the checksum
   * @param directory    the directory to get the checksum for
   * @param excludeFiles files/directories not to include into checksum calculation
   * @param excludeExtensions  files/directories having this extensions not to include into checksum calculation
   * @return the hash string representing the given directory
   * @throws IOException
   */
  public static String getDirChecksum( MessageDigest digest, File directory, List<String> excludeFiles, List<String> excludeExtensions ) throws IOException {
    // collect entry checksums
    StringBuilder dirChecksumStr = new StringBuilder();
    String dirChecksum = getDirChecksumStr( digest, directory, dirChecksumStr, excludeFiles, excludeExtensions );
    if (debugEnabled) {
      Logger.info( "[" + directory.getName() + "] total checksum: " + dirChecksum );
    }
    return dirChecksum;
  }

  /**
   * Create checksum for one directory.
   * Descend into subdirectories if necessary and create hashes for each of them.
   *
   * @param digest       the digest to use to create the checksum
   * @param directory    the directory to get the checksum for
   * @param contentChecksumStr  the string to which file/subdirectory checksums are added
   * @param excludeFiles files/directories not to include into checksum calculation
   * @param excludeExtensions files/directories having this extensions not to include into checksum calculation
   * @return the hash string representing the given directory
   * @throws IOException
   */
  private static String getDirChecksumStr( MessageDigest digest, File directory, StringBuilder contentChecksumStr, List<String> excludeFiles, List<String> excludeExtensions ) throws IOException {
    // check if hash file exists
    String directoryName = directory.getName();
    try {
      File hashFile = directory.getParentFile().toPath().resolve( directoryName + "." + digest.getAlgorithm().toLowerCase() ).toFile();
      if (hashFile.exists()) {
        List<String> hashContent = Files.readAllLines( hashFile.toPath() );
        if (hashContent.size() != 1) {
          Logger.error( "invalid hash file: " + hashFile.getCanonicalPath() + "; generate hash from content" );
        }
        else {
          // use it as is
          String hashStr = hashContent.get( 0 );
          if (debugEnabled) {
            Logger.info( digest.getAlgorithm() + ": " + hashStr + " (from md5 file) [" + directoryName + "]" );
          }
          return hashStr;
        }
      }
    }
    catch (Exception ex) {
      Logger.error( "error reading hash file; generate hash from content (" + ex.getMessage() + ")" );
    }
    // create new hash
    if (debugEnabled) {
      Logger.info( "... descend into " + directoryName );
    }
    List<File> filenameList = Arrays.asList( directory.listFiles() );
    filenameList.sort( ( o1, o2 ) -> o1.getName().compareToIgnoreCase( o2.getName() ) );  // sort ascending
    for (File file : filenameList) {
      String filenameLower = file.getName().toLowerCase();
      int extSep = filenameLower.lastIndexOf( "." );
      String fileExt = "";
      if (extSep >= 0) {
        fileExt = filenameLower.substring( extSep + 1 );
      }
      if (!excludeFiles.contains( filenameLower ) && !excludeExtensions.contains( fileExt )) {
        contentChecksumStr.append( filenameLower );
        if (file.isDirectory()) {
          StringBuilder dirChecksumStr = new StringBuilder();
          String dirChecksum = getDirChecksumStr( digest, file, dirChecksumStr, excludeFiles, excludeExtensions );
          contentChecksumStr.append( dirChecksum );
        }
        else {
          String fileChecksum = getFileChecksum( digest, file );
          contentChecksumStr.append( fileChecksum );
        }
      }
    }
    // get checksum for all
    digest.reset();
    digest.update( contentChecksumStr.toString().getBytes() );

    byte[] bytes = digest.digest();
    String dirChecksum = byteArrayToHexString( bytes );
    if (debugEnabled) {
      Logger.info( "[" + directoryName + "] (content checksum string): \n" + contentChecksumStr );
      Logger.info( "[" + directoryName + "] (content checksum): " + dirChecksum );
    }
    return dirChecksum;
  }

  public static String byteArrayToHexString( byte[] b ) {
    String result = "";
    for (int i = 0; i < b.length; i++) {
      result += Integer.toString( (b[i] & 0xff) + 0x100, 16 ).substring( 1 );
    }
    return result;
  }
}
