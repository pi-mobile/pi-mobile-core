/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.file;

import de.pidata.date.DateHelper;
import de.pidata.log.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Logger which provides rolling logfiles.
 *
 * Created by dsc on 29.06.17.
 */
public class FilebasedRollingLogger implements LoggerInterface, FileOwner {

  private static final boolean DEBUG = false;

  public static final String KEY_LOGFILE = "logfile";
  public static final String KEY_DIR = "dir";
  public static final String KEY_EXPIREDAYS = "expiredays";
  public static final String KEY_PURGEDAYS = "purgedays";
  public static final String KEY_LOGFILE_EXPIREDAYS = "logfile.expiredays";
  public static final String KEY_LOGFILE_PURGEDAYS = "logfile.purgedays";


  public static final String LOGFILE_SUFFIX = ".log";
  public static final int LOGFILE_SUFFIX_LENGTH = LOGFILE_SUFFIX.length();


  private final String logfileBase;
  private final int logfileExpireDays;
  private final int logfilePurgeDays;
  private Level logLevel = Level.DEBUG;

  private long lastLogfileDateMillis;
  private String logfileName;
  private PrintStream logStream;

  public FilebasedRollingLogger( String logfileName, int logfileExpireDays, int logfilePurgeDays, Level logLevel ) {
    if (logfileName.endsWith( LOGFILE_SUFFIX )) {
      this.logfileName = logfileName;
      this.logfileBase = logfileName.substring( 0, logfileName.length() - LOGFILE_SUFFIX_LENGTH );
    }
    else {
      this.logfileBase = logfileName;
      this.logfileName = this.logfileBase + LOGFILE_SUFFIX;
    }
    this.logfileExpireDays = logfileExpireDays;
    this.logfilePurgeDays = logfilePurgeDays;
    this.logLevel = logLevel;

    Logger.info( "start FilebasedRollingLogger on " + logfileName );

    openLogFile();
  }

  @Override
  public void close() {
    if (logStream != null) {
      logStream.flush();
      logStream.close();
      logStream = null;
    }
  }

  private void openLogFile() {
    long todayMillis = DateHelper.getDate( new Date() );
    File logFile = new File( logfileName ).getAbsoluteFile();

    //--- Close and rename current log file
    long logfileTime;
    boolean canWriteLogfile = logFile.exists() && logFile.canWrite();
    if (canWriteLogfile) {
      logfileTime = DateHelper.getDate( new Date( logFile.lastModified() ) );
    }
    else {
      logfileTime = DateHelper.subtractDays( todayMillis, 1 );
    }

    if (logfileExpired( todayMillis, logfileTime )) {
      retireLogfile( logfileTime ); // closes logStream (sets logStream = null)
    }

    if (logStream != null) {
      // If log stream is (still) opened, we did not switch logfile.
      // Continue logging to currently opened logfile.
      return;
    }

    //--- Remove old logfiles
    if (logfilePurgeDays > 0) {
      purgeOldLogfiles( todayMillis );
    }

    //--- Open or create new log file
    try {
      doLog( Level.INFO, "new logfile=" + logfileName, null );
      //OutputStream logFileStream = logStorage.write( logfileName, false, true );
      //this.logfile = new PrintStream( logFileStream );
      if (logFile.exists()) {
        if (logFile.canWrite()) {
          this.lastLogfileDateMillis = DateHelper.getDate( new Date( logFile.lastModified() ) );
        }
        else {
          doLog( Level.ERROR, "cannot write to new logfile " + logFile.getAbsolutePath(), null );
          doLog( Level.ERROR, "       fallback log to System.out", null );
          logStream = null;
          return;
        }
      }
      else {
        logFile.getParentFile().getAbsoluteFile().mkdirs();
        this.lastLogfileDateMillis = todayMillis;
      }
      FileOutputStream outputStream = new FileOutputStream( logFile, true );
      logStream = new PrintStream( outputStream );

      doLog( Level.INFO, "started new logfile " + logFile.getAbsolutePath(), null );
    }
    catch (Exception e) {
      doLog( Level.INFO, "Error opening log file name=" + logfileName, e );
    }
  }

  private void retireLogfile( long fileDateMillis ) {
    String dateStr = DateHelper.toDateString( fileDateMillis );
    String retiredLogfileName = logfileBase + "_" + dateStr + LOGFILE_SUFFIX;

    doLog( Level.INFO, "retire logfile to=" + retiredLogfileName, null );

    File retiredLogfile = new File( retiredLogfileName ).getAbsoluteFile();
    if (!retiredLogfile.exists()) {
      close();
      new File( logfileName ).getAbsoluteFile().renameTo( retiredLogfile );
    }
  }

  private void purgeOldLogfiles( long todayMillis ) {
    long purgeDateMillis = DateHelper.subtractDays( todayMillis, logfilePurgeDays );

    File log = new File( logfileName ).getAbsoluteFile();
    String logfileBasePath = null;
    try {
      logfileBasePath = log.getCanonicalPath();
    }
    catch (IOException e) {
      doLog( Level.INFO, "Error getting canonical logfile base path from [" + logfileName + "]", e );
      return;
    }
    // On iOS at least isDirectory() and list() only work on File returned by getAbsoluteFile() !?
    File logfileParent = log.getParentFile().getAbsoluteFile();
    if (logfileParent == null) {
      return;
    }
    logfileParent.mkdirs();
    File[] logfileList = null;
    try {
      logfileList = logfileParent.listFiles();
    }
    catch (Exception ex) {
      doLog( Level.INFO, "Error listing logfile parent=" + logfileParent, ex );
      return;
    }

    if (logfileList != null) {
      for (File oldFilex : logfileList) {
        File oldFile = oldFilex.getAbsoluteFile();
        String filePath = "???";
        try {
          filePath = oldFile.getCanonicalPath();
          if (filePath.startsWith( logfileBasePath )) {
            String fileName = oldFile.getName();
            int pos = fileName.lastIndexOf( '_' );
            int pos2 = fileName.lastIndexOf( '.' );
            if ((pos > 0) && pos2 > 0) {
              String fileDateStr = fileName.substring( pos + 1, pos2 );
              long logDateMillis = DateHelper.parseDateString( fileDateStr );
              if (logDateMillis < purgeDateMillis) {
                oldFile.delete();
              }
            }
          }
        }
        catch (Exception ex) {
          doLog( Level.INFO, "Error removing expired log name=" + filePath, ex );
        }
      }
    }
  }

  private boolean logfileExpired( long todayMillis, long logfileTime ) {
    if (logfileExpireDays > 0) {
      long expireDateMillis = DateHelper.subtractDays( todayMillis, logfileExpireDays );
      return logfileTime <= expireDateMillis;
    }
    return false;
  }

  /**
   * Do logging
   *
   * @param logLevel the logging level, one of the constants LOG_*
   * @param message  the message to log
   * @param cause    the Throwable which led to this log entry
   */
  @Override
  public void log( Level logLevel, String message, Throwable cause ) {
    if (logLevel.getLevelValue() >= getLogLevel().getLevelValue()) {
      openLogFile();
      doLog( logLevel, message, cause );
    }
  }

  private void doLog( Level logLevel, String message, Throwable cause ) {
    if (logStream == null) {
      DefaultLogger.printToLog( System.out, Level.ERROR, "no logStream opened for " + logfileName, null );
      DefaultLogger.printToLog( System.out, logLevel, message, cause );
    }
    else {
      DefaultLogger.printToLog( logStream, logLevel, message, cause );
    }
  }

  @Override
  public void setLogLevel( Level loglevel ) {
    this.logLevel = loglevel;
  }

  /**
   * Returns current log level
   *
   * @return current log level, one of the constants LOG_*
   */
  @Override
  public Level getLogLevel() {
    return this.logLevel;
  }

  @Override
  public List<String> getOwnedFiles() {

    List<String> fileList = new ArrayList<String>();

    File log = new File( logfileBase ).getAbsoluteFile();
    // On iOS at least isDirectory() and list() only work on File returned by getAbsoluteFile() !?
    File logfileParent = log.getParentFile().getAbsoluteFile();
    logfileParent.mkdirs();

    try {
      File[] logfileList = logfileParent.listFiles();
      for (File logFile_x : logfileList) {
        File logFile = logFile_x.getAbsoluteFile();
        String filePath = "";
        try {
          filePath = logFile.getCanonicalPath();
          if (filePath.contains( logfileBase )) {
            fileList.add( filePath );
          }
        }
        catch (Exception ex) {
          doLog( Level.INFO, "Error adding logfile name=" + filePath, ex );
        }
      }

    }
    catch (Exception ex) {
      doLog( Level.INFO, "Error listing logfiles for=" + logfileParent, ex );
    }

    return fileList;
  }
}
