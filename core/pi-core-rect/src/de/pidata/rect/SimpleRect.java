/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rect;

/**
 * SimpleRect extends SimplePos by a width an height.
 */
public class SimpleRect extends SimplePos implements Rect {

  private double width;
  private double height;

  public SimpleRect( Rect copyFrom ) {
    super( copyFrom );
    width = copyFrom.getWidth();
    height = copyFrom.getHeight();
  }

  public SimpleRect( double x, double y, double width, double height ) {
    super( x, y );
    this.width = width;
    this.height = height;
  }

  public double getWidth() {
    return width;
  }

  public double getHeight() {
    return height;
  }

  @Override
  public double getRight() {
    return getX() + getWidth();
  }

  @Override
  public double getBottom() {
    return getY() + getHeight();
  }

  protected void fireSizeChanged( double oldWidth, double oldHeight ) {
    if (eventSender != null) {
      if ((oldWidth != getWidth()) || (oldHeight != getHeight())) {
        eventSender.fireSizeChanged( oldWidth, oldHeight );
      }
    }
  }

  @Override
  public void setWidth( double width ) {
    double oldWidth = getWidth();
    double oldHeight = getHeight();
    this.width = width;
    fireSizeChanged( oldWidth, oldHeight );
  }

  @Override
  public void setHeight( double height ) {
    double oldWidth = getWidth();
    double oldHeight = getHeight();
    this.height = height;
    fireSizeChanged( oldWidth, oldHeight );
  }

  @Override
  public void setSize( double width, double height ) {
    double oldWidth = getWidth();
    double oldHeight = getHeight();
    this.width = width;
    this.height = height;
    fireSizeChanged( oldWidth, oldHeight );
  }
}
