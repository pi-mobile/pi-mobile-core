/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rect;

/**
 * RectRelatedPos is a Pos related to a base Rect by factors for X and Y.
 *
 * Static createXXX can be used for typical position related to base Rect, e.g. center.
 *
 * RectRelatedPos listens on base Rect, so it also fires posChanged() when
 * only base Rect has changed.
 */
public class RectRelatedPos extends AbstractPosDir implements PosSizeEventListener {

  private Rect baseRect;
  private Rotation rotation = null;
  private double deltaX;
  private double factorX;
  private double deltaY;
  private double factorY;

  /**
   * Creates a {@link Pos} attached to the given baseRect and aligns it dynamically to the
   * baseRect's coordinates.
   *
   * @param baseRect the {@link Rect} to align to
   * @param factorX the proportion of the baseRect's width at which the X-coordinate should be attached
   * @param deltaX the static offset of the X-coordinate from the X-coordinate calculated using the baseRects width and the factorX
   * @param factorY the proportion of the baseRect's height at which the Y-coordinate should be attached
   * @param deltaY the static offset of the Y-coordinate from the Y-coordinate calculated using the baseRects height and the factorY
   */
  public RectRelatedPos( Rect baseRect, double factorX, double deltaX, double factorY, double deltaY ) {
    this.baseRect = baseRect;
    this.deltaX = deltaX;
    this.factorX = factorX;
    this.deltaY = deltaY;
    this.factorY = factorY;
    baseRect.getEventSender().addListener( this );
  }

  /**
   * Creates a {@link Pos} attached to the given baseRect and aligns it dynamically to the
   * baseRect's coordinates.
   *
   * @param baseRect the {@link Rect} to align to
   * @param factorX the proportion of the baseRect's width at which the X-coordinate should be attached
   * @param deltaX the static offset of the X-coordinate from the X-coordinate calculated using the baseRects width and the factorX
   * @param factorY the proportion of the baseRect's height at which the Y-coordinate should be attached
   * @param deltaY the static offset of the Y-coordinate from the Y-coordinate calculated using the baseRects height and the factorY
   * @param rotation
   */
  public RectRelatedPos( Rect baseRect, double factorX, double deltaX, double factorY, double deltaY, Rotation rotation ) {
    this.baseRect = baseRect;
    this.deltaX = deltaX;
    this.factorX = factorX;
    this.deltaY = deltaY;
    this.factorY = factorY;
    this.rotation = rotation;
    baseRect.getEventSender().addListener( this );
  }

  @Override
  public double getX() {
    return baseRect.getX() + (baseRect.getWidth() * factorX) + deltaX;
  }

  @Override
  public double getY() {
    return baseRect.getY() + (baseRect.getHeight() * factorY) + deltaY;
  }

  @Override
  public Rotation getRotation() {
    if (rotation == null) {
      if (baseRect instanceof RectDir) {
        return ((RectDir) baseRect).getRotation();
      }
      else {
        return Rotation.NONE;
      }
    }
    else {
      return rotation;
    }
  }

  public void setRotation( Rotation rotation ) {
    Rotation oldRotation = this.rotation;
    this.rotation = rotation;
    fireRotationChanged( oldRotation );
  }

  @Override
  public void setX( double x ) {
    double oldX = getX();
    double oldY = getY();
    this.deltaX = x - (baseRect.getX() + (baseRect.getWidth() * factorX));
    firePosChanged( oldX, oldY );
  }

  @Override
  public void setY( double y ) {
    double oldX = getX();
    double oldY = getY();
    this.deltaY = y - (baseRect.getY() + (baseRect.getHeight() * factorY));
    firePosChanged( oldX, oldY );
  }

  @Override
  public void setPos( double x, double y ) {
    double oldX = getX();
    double oldY = getY();
    this.deltaX = x - (baseRect.getX() + (baseRect.getWidth() * factorX));
    this.deltaY = y - (baseRect.getY() + (baseRect.getHeight() * factorY));
    firePosChanged( oldX, oldY );
  }

  public void setPosRel( double factorX, double deltaX, double factorY, double deltaY ) {
    double oldX = getX();
    double oldY = getY();
    this.factorX = factorX;
    this.deltaX = deltaX;
    this.factorY = factorY;
    this.deltaY = deltaY;
    firePosChanged( oldX, oldY );
  }

  @Override
  public void posChanged( Pos pos, double oldX, double oldY ) {
    double myOldX = oldX + (baseRect.getWidth() * factorX) + deltaX;
    double myOldY = oldY + (baseRect.getHeight() * factorY) + deltaY;
    firePosChanged( myOldX, myOldY );
  }

  @Override
  public void sizeChanged( Rect rect, double oldWidth, double oldHeight ) {
    double myOldX = baseRect.getX() + (oldWidth * factorX) + deltaX;
    double myOldY = baseRect.getY() + (oldHeight * factorY) + deltaY;
    firePosChanged( myOldX, myOldY );
  }

  @Override
  public void rotationChanged( PosDir posDir, Rotation oldRotation ) {
    if (rotation == null) {
      fireRotationChanged( oldRotation );
    }
  }

  public Rect getBaseRect(){
    return this.baseRect;
  }

  public static RectRelatedPos createTopRight( Rect baseRect ) {
    return new RectRelatedPos( baseRect, 1.0, 0, 0, 0 );
  }

  public static RectRelatedPos createBottomLeft( Rect baseRect ) {
    return new RectRelatedPos( baseRect, 0, 0, 1.0, 0 );
  }

  public static RectRelatedPos createBottomRight( Rect baseRect ) {
    return new RectRelatedPos( baseRect, 1.0, 0, 1.0, 0 );
  }

  public static RectRelatedPos createCenter( Rect baseRect ) {
    return new RectRelatedPos( baseRect, 0.5, 0, 0.5, 0 );
  }

  public static RectRelatedPos createCenterTop( Rect baseRect ) {
    return new RectRelatedPos( baseRect, 0.5, 0, 0, 0 );
  }

  public static RectRelatedPos createCenterLeft( Rect baseRect ) {
    return new RectRelatedPos( baseRect, 0, 0, 0.5, 0 );
  }

  public static RectRelatedPos createCenterRight( Rect baseRect ) {
    return new RectRelatedPos( baseRect, 1.0, 0, 0.5, 0 );
  }

  public static RectRelatedPos createCenterBottom( Rect baseRect ) {
    return new RectRelatedPos( baseRect, 0.5, 0, 1.0, 0 );
  }
}
