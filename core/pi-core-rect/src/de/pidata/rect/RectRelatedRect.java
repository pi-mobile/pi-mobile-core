/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rect;

/**
 * RectRelatedRect extends a RectRelatedPos by a (not related) width and height.
 *
 * It is used for example for displaying a Figure's resize handles.
 */
public class RectRelatedRect extends RectRelatedPos implements RectDir {

  private double width;
  private double height;

  /**
   * Creates a {@link Rect} attached to the given baseRect and aligns it dynamically to the
   * baseRect's coordinates.
   * The anchor point of the new rectangle is it's upper left corner.
   *
   * @param baseRect the {@link Rect} to align to
   * @param factorX the proportion of the baseRect's width at which the X-coordinate of the anchor point should be attached
   * @param deltaX the static offset of the X-coordinate of the anchor point from the X-coordinate calculated using the baseRects width and the factorX
   * @param factorY the proportion of the baseRect's height at which the Y-coordinate of the anchor point should be attached
   * @param deltaY the static offset of the Y-coordinate of the anchor point from the Y-coordinate calculated using the baseRects height and the factorY
   * @param width the initial width
   * @param height the initial height
   */
  public RectRelatedRect( Rect baseRect, double factorX, double deltaX, double factorY, double deltaY, double width, double height ) {
    super( baseRect, factorX, deltaX, factorY, deltaY );
    this.width = width;
    this.height = height;
  }

  public RectRelatedRect( Rect baseRect, double factorX, double deltaX, double factorY, double deltaY, double width, double height, Rotation rotation ) {
    super( baseRect, factorX, deltaX, factorY, deltaY, rotation );
    this.width = width;
    this.height = height;
  }

  @Override
  public double getWidth() {
    return width;
  }

  @Override
  public double getHeight() {
    return height;
  }

  @Override
  public double getRight() {
    return getX() + getWidth();
  }

  @Override
  public double getBottom() {
    return getY() + getHeight();
  }

  protected void fireSizeChanged( double oldWidth, double oldHeight ) {
    if (eventSender != null) {
      if ((oldWidth != getWidth()) || (oldHeight != getHeight())) {
        eventSender.fireSizeChanged( oldWidth, oldHeight );
      }
    }
  }

  @Override
  public void setWidth( double width ) {
    double oldWidth = getWidth();
    double oldHeight = getHeight();
    this.width = width;
    fireSizeChanged( oldWidth, oldHeight );
  }

  @Override
  public void setHeight( double height ) {
    double oldWidth = getWidth();
    double oldHeight = getHeight();
    this.height = height;
    fireSizeChanged( oldWidth, oldHeight );
  }

  @Override
  public void setSize( double width, double height ) {
    double oldWidth = getWidth();
    double oldHeight = getHeight();
    this.width = width;
    this.height = height;
    fireSizeChanged( oldWidth, oldHeight );
  }
}
