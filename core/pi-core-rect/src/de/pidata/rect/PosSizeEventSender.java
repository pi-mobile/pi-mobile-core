/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rect;

import de.pidata.log.Logger;

import java.util.LinkedList;
import java.util.List;

public class PosSizeEventSender {

  private Pos owner;
  private List<PosSizeEventListener> listenerList;

  public PosSizeEventSender( Pos owner ) {
    this.owner = owner;
  }

  public Object getOwner() {
    return owner;
  }

  public void addListener( PosSizeEventListener listener)  {
    if (this.listenerList == null) {
      this.listenerList = new LinkedList<PosSizeEventListener>();
    }
    this.listenerList.add(listener);
  }

  public void removeListener( PosSizeEventListener listener ) {
    if (this.listenerList != null) {
      this.listenerList.remove(listener);
    }
  }

  public void firePosChanged( double oldX, double oldY ) {
    if (this.listenerList != null) {
      // Copying listeners prevents problems if listeners are added or removed while send event loop
      PosSizeEventListener[] listenerArr = new PosSizeEventListener[listenerList.size()];
      listenerList.toArray(listenerArr);
      PosSizeEventListener listener;
      for (int i = 0; i < listenerArr.length; i++) {
        listener = listenerArr[i];
        try {
          listener.posChanged( owner, oldX, oldY );
        }
        catch (Exception ex) {
          Logger.error( "Error while event processing", ex );
        }
      }
    }
  }

  public void fireSizeChanged( double oldWidth, double oldHeight ) {
    if (this.listenerList != null) {
      // Copying listeners prevents problems if listeners are added or removed while send event loop
      PosSizeEventListener[] listenerArr = new PosSizeEventListener[listenerList.size()];
      listenerList.toArray(listenerArr);
      PosSizeEventListener listener;
      for (int i = 0; i < listenerArr.length; i++) {
        listener = listenerArr[i];
        try {
          listener.sizeChanged( (Rect) owner, oldWidth, oldHeight );
        }
        catch (Exception ex) {
          Logger.error( "Error while event processing", ex );
        }
      }
    }
  }

  public void fireRotationChanged( Rotation oldRotation ) {
    if (this.listenerList != null) {
      // Copying listeners prevents problems if listeners are added or removed while send event loop
      PosSizeEventListener[] listenerArr = new PosSizeEventListener[listenerList.size()];
      listenerList.toArray(listenerArr);
      PosSizeEventListener listener;
      for (int i = 0; i < listenerArr.length; i++) {
        listener = listenerArr[i];
        try {
          listener.rotationChanged( (PosDir) owner, oldRotation );
        }
        catch (Exception ex) {
          Logger.error( "Error while event processing", ex );
        }
      }
    }
  }

  public int listenerCount() {
    if (listenerList == null) {
      return 0;
    }
    else {
      return listenerList.size();
    }
  }
}
