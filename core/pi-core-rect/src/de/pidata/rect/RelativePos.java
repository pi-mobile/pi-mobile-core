/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rect;

/**
 * A Relative Pos is a PosDir which is relative to a base Pos.
 *
 * Its position is calculated by adding X,Y to base Pos's X, Y.
 *
 * RelativePos listens on base Pos, so it also fires posChanged() when
 * only base Pos has changed.
 */
public class RelativePos extends AbstractPosDir implements PosSizeEventListener {

  private boolean ignoreEvents = false;
  private Pos    basePos;
  private double deltaX = 0;
  private double deltaY = 0;
  private double deltaAngle = 0;

  public RelativePos( Pos basePos, double deltaX, double deltaY ) {
    this( basePos, deltaX, deltaY, 0 );
  }

  public RelativePos( Pos basePos, double deltaX, double deltaY, double deltaAngle ) {
    this.basePos = basePos;
    this.deltaX = deltaX;
    this.deltaY = deltaY;
    this.deltaAngle = deltaAngle;
    basePos.getEventSender().addListener( this );
  }

  public Pos getBasePos() {
    return basePos;
  }

  public double getX() {
    if (basePos == null) {
      return deltaX;
    }
    else {
      return basePos.getX() + deltaX;
    }
  }

  public double getY() {
    if (basePos == null) {
      return deltaY;
    }
    else {
      return basePos.getY() + deltaY;
    }
  }

  public void setX( double x ) {
    double oldX = getX();
    double oldY = getY();
    this.deltaX = x - basePos.getX();
    firePosChanged( oldX, oldY );
  }

  public void setY( double y ) {
    double oldX = getX();
    double oldY = getY();
    this.deltaY = y - basePos.getY();
    firePosChanged( oldX, oldY );
  }

  @Override
  public void setPos( double x, double y ) {
    double oldX = getX();
    double oldY = getY();
    this.deltaX = x - basePos.getX();
    this.deltaY = y - basePos.getY();
    firePosChanged( oldX, oldY );
  }

  @Override
  public Rotation getRotation() {
    if (basePos instanceof PosDir) {
      return ((PosDir) basePos).getRotation();
    }
    else {
      return Rotation.NONE;
    }
  }

  public double getDeltaX() {
    return deltaX;
  }

  public double getDeltaY() {
    return deltaY;
  }

  public double getDeltaAngle() {
    return deltaAngle;
  }

  @Override
  public void posChanged( Pos pos, double oldX, double oldY ) {
    if (!ignoreEvents) {
      double myOldX = oldX + getDeltaX();
      double myOldY = oldY + getDeltaY();
      firePosChanged( myOldX, myOldY );
    }
  }

  @Override
  public void sizeChanged( Rect rect, double oldWidth, double oldHeight ) {
    // not relevant - ignore
  }

  @Override
  public void rotationChanged( PosDir posDir, Rotation oldRotation ) {
    fireRotationChanged( oldRotation );
  }
}
