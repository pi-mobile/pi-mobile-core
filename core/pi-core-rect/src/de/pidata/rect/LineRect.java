/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rect;

/**
 * A LineRect is a Rect identified by a start and end point, e.g. of a line
 */
public class LineRect extends AbstractRect implements RectDir, PosSizeEventListener {

  private Pos startPos;
  private Pos endPos;

  public LineRect( Pos startPos, Pos endPos ) {
    this.startPos = startPos;
    this.endPos = endPos;
    startPos.getEventSender().addListener( this );
    endPos.getEventSender().addListener( this );
  }

  @Override
  public double getX() {
    double x = startPos.getX();
    if (x > endPos.getX()) {
      x = endPos.getX();
    }
    return x;
  }

  @Override
  public double getY() {
    double y = startPos.getY();
    if (y > endPos.getY()) {
      y = endPos.getY();
    }
    return y;
  }

  @Override
  public double getWidth() {
    double dx = Math.abs( startPos.getX() - endPos.getX() );
    return dx;
  }


  @Override
  public double getHeight() {
    double dy = Math.abs( startPos.getY() - endPos.getY() );
    return dy;
  }

  @Override
  public void setWidth( double width ) {
    if (startPos.getX() < endPos.getX()) {
      endPos.setX( startPos.getX() + width );
    }
    else {
      endPos.setX( startPos.getX() - width );
    }
  }

  @Override
  public void setHeight( double height ) {
    if (startPos.getY() < endPos.getY()) {
      endPos.setY( startPos.getY() + height );
    }
    else {
      endPos.setY( startPos.getY() - height );
    }
  }

  @Override
  public void setSize( double width, double height ) {
    double oldWidth = getWidth();
    double oldHeight = getHeight();
    if (width != oldWidth) {
      setWidth( width );
    }
    if (height != oldHeight) {
      setHeight( height );
    }
  }

  @Override
  public Rotation getRotation() {
    if (startPos instanceof PosDir) {
      return ((PosDir) startPos).getRotation();
    }
    else {
      return Rotation.NONE;
    }
  }

  public Pos getStartPos() {
    return startPos;
  }

  public Pos getEndPos() {
    return endPos;
  }

  @Override
  public void setX( double x ) {
    double oldX = getX();
    double oldY = getY();
    if (startPos.getX() < endPos.getX()) {
      startPos.setX( x );
    }
    else {
      endPos.setX( x );
    }
    firePosChanged( oldX, oldY );
  }

  @Override
  public void setY( double y ) {
    double oldX = getX();
    double oldY = getY();
    if (startPos.getY() < endPos.getY()) {
      startPos.setY( y );
    }
    else {
      endPos.setY( y );
    }
    firePosChanged( oldX, oldY );
  }

  @Override
  public void setPos( double x, double y ) {
    double oldX = getX();
    double oldY = getY();
    if (startPos.getX() < endPos.getX()) {
      startPos.setX( x );
    }
    else {
      endPos.setX( x );
    }
    if (startPos.getY() < endPos.getY()) {
      startPos.setY( y );
    }
    else {
      endPos.setY( y );
    }
    firePosChanged( oldX, oldY );
  }

  @Override
  public void posChanged( Pos pos, double oldX, double oldY ) {
    //calculate old pos/size and decide which change to fire
    double myOldX;
    double myOldY;
    double myOldWidth;
    double myOldHeight;
    if (pos == startPos) {
      if (oldX < endPos.getX()) {
        myOldX = oldX;
      }
      else {
        myOldX = endPos.getX();
      }
      if (oldY < endPos.getY()) {
        myOldY = oldY;
      }
      else {
        myOldY = endPos.getY();
      }
      myOldWidth = Math.abs( endPos.getX() - oldX );
      myOldHeight = Math.abs( endPos.getY() - oldY );
    }
    else {
      if (oldX < startPos.getX()) {
        myOldX = oldX;
      }
      else {
        myOldX = startPos.getX();
      }
      if (oldY < startPos.getY()) {
        myOldY = oldY;
      }
      else {
        myOldY = startPos.getY();
      }
      myOldWidth = Math.abs( startPos.getX() - oldX );
      myOldHeight = Math.abs( startPos.getY() - oldY );
    }
    firePosChanged( myOldX, myOldY );
    fireSizeChanged( myOldWidth, myOldHeight );
  }

  @Override
  public void sizeChanged( Rect rect, double oldWidth, double oldHeight ) {
    fireSizeChanged( oldWidth, oldHeight );
  }

  protected void fireRotationChanged( Rotation oldRotation ) {
    if (eventSender != null) {
      if (!oldRotation.equals( getRotation() )) {
        eventSender.fireRotationChanged( oldRotation );
      }
    }
  }

  @Override
  public void rotationChanged( PosDir posDir, Rotation oldRotation ) {
    fireRotationChanged( oldRotation );
  }
}
