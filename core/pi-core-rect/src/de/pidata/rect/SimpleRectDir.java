/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rect;

import de.pidata.string.Helper;

/**
 * SimpleRectDir extends a SimpleRect by a rotation.
 */
public class SimpleRectDir extends SimpleRect implements RectDir {

  private Rotation rotation;

  public SimpleRectDir(double x, double y, double width, double height ) {
    this( x, y, width, height, Rotation.NONE );
  }

  public SimpleRectDir( double x, double y, double width, double height, Rotation rotation ) {
    super( x, y, width, height );
    this.rotation = rotation;
  }

  @Override
  public Rotation getRotation() {
    return rotation;
  }

  public void setRotation( Rotation rotation ) {
    Rotation oldRotation = this.rotation;
    this.rotation = rotation;
    fireRotationChanged( oldRotation );
  }

  protected void fireRotationChanged( Rotation oldRotation ) {
    if (eventSender != null) {
      Rotation newRotation = getRotation();
      if (!Helper.equals( oldRotation, newRotation )) {
        eventSender.fireRotationChanged( oldRotation );
      }
    }
  }
}
