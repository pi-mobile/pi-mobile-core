/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rect;

/**
 * A DeltaRect is a Rect which can having position and size relative to a baseRect.
 * So whenever the baseRect's position is changed this DeltaRect's position is kept
 * in fix distance to baseRect. Also for this DeltaRect's width an height also is kept
 * in a fix delta to the baseRect.
 */
public class DeltaRect extends AbstractRect implements RectDir, PosSizeEventListener {

  private Rect    baseRect;
  private boolean ignoreEvents = false;

  private double deltaX = 0;
  private double deltaY = 0;
  private double deltaWidth = 0;
  private double deltaHeight= 0;

  public DeltaRect( Rect baseRect ) {
    this.baseRect = baseRect;
    if (baseRect != null) {
      baseRect.getEventSender().addListener( this );
    }
  }

  public DeltaRect( double x, double y, double width, double height ) {
    this( x, y, width, height, Rotation.NONE );
  }

  public DeltaRect( double x, double y, double width, double height, Rotation rotation ) {
    baseRect = new SimpleRectDir( x,y, width, height, rotation );
    baseRect.getEventSender().addListener( this );
  }

  public DeltaRect( Rect baseRect, double x, double y, double width, double height ) {
    this.baseRect = baseRect;
    this.deltaX = x;
    this.deltaY = y;
    this.deltaWidth = width;
    this.deltaHeight = height;
    if (baseRect != null) {
      baseRect.getEventSender().addListener( this );
    }
  }

  public Rect getBaseRect() {
    return baseRect;
  }

  public double getX() {
    if (baseRect == null) {
      return deltaX;
    }
    else {
      return baseRect.getX() + deltaX;
    }
  }

  public double getY() {
    if (baseRect == null) {
      return deltaY;
    }
    else {
      return baseRect.getY() + deltaY;
    }
  }

  @Override
  public double getWidth() {
    if (baseRect == null) {
      return deltaWidth;
    }
    else {
      return baseRect.getWidth() + deltaWidth;
    }
  }

  @Override
  public double getHeight() {
    if (baseRect == null) {
      return deltaHeight;
    }
    else {
      return baseRect.getHeight() + deltaHeight;
    }
  }

  @Override
  public Rotation getRotation() {
    if (baseRect instanceof RectDir) {
      return ((RectDir) baseRect).getRotation();
    }
    else {
      return Rotation.NONE;
    }
  }

  public double getDeltaX() {
    return deltaX;
  }

  public double getDeltaY() {
    return deltaY;
  }

  public double getDeltaWidth() {
    return deltaWidth;
  }

  public double getDeltaHeight() {
    return deltaHeight;
  }

  @Override
  public void setX( double x ) {
    double oldX = getX();
    double oldY = getY();
    if (baseRect == null) {
      this.deltaX = x;
    }
    else {
      this.deltaX = x - baseRect.getX();
    }
    firePosChanged( oldX, oldY );
  }

  @Override
  public void setY( double y ) {
    double oldX = getX();
    double oldY = getY();
    if (baseRect == null) {
      this.deltaY = y;
    }
    else {
      this.deltaY = y - baseRect.getY();
    }
    firePosChanged( oldX, oldY );
  }

  @Override
  public void setPos( double x, double y ) {
    double oldX = getX();
    double oldY = getY();
    if (baseRect == null) {
      this.deltaX = x;
      this.deltaY = y;
    }
    else {
      this.deltaY = y - baseRect.getY();
      this.deltaX = x - baseRect.getX();
    }
    firePosChanged( oldX, oldY );
  }

  @Override
  public void setWidth( double width ) {
    double oldWidth = getWidth();
    double oldHeight = getHeight();
    this.deltaWidth = width;
    fireSizeChanged( oldWidth, oldHeight );
  }

  @Override
  public void setHeight( double height ) {
    double oldWidth = getWidth();
    double oldHeight = getHeight();
    this.deltaHeight = height;
    fireSizeChanged( oldWidth, oldHeight );
  }

  @Override
  public void setSize( double width, double height ) {
    double oldWidth = getWidth();
    double oldHeight = getHeight();
    this.deltaWidth = width;
    this.deltaHeight = height;
    fireSizeChanged( oldWidth, oldHeight );
  }

  public void setDeltaPos( double deltaX, double deltaY ) {
    double oldX = getX();
    double oldY = getY();
    this.deltaX = deltaX;
    this.deltaY = deltaY;
    firePosChanged( oldX, oldY );
  }

  public void setDeltaSize( double deltaWidth, double deltaHeight ) {
    double oldWidth = getWidth();
    double oldHeight = getHeight();
    this.deltaWidth = deltaWidth;
    this.deltaHeight = deltaHeight;
    fireSizeChanged( oldWidth, oldHeight );
  }

  protected void fireSizeChanged( double oldWidth, double oldHeight ) {
    if (eventSender != null) {
      if ((oldWidth != getWidth()) || (oldHeight != getHeight())) {
        eventSender.fireSizeChanged( oldWidth, oldHeight );
      }
    }
  }

  protected void fireRotationChanged( Rotation oldRotation ) {
    if (eventSender != null) {
      if (!oldRotation.equals( getRotation() )) {
        eventSender.fireRotationChanged( oldRotation );
      }
    }
  }

  @Override
  public void posChanged( Pos pos, double oldX, double oldY ) {
    if (!ignoreEvents) {
      double myOldX = oldX + deltaX;
      double myOldY = oldY + deltaY;
      firePosChanged( myOldX, myOldY );
    }
  }

  @Override
  public void sizeChanged( Rect rect, double oldWidth, double oldHeight ) {
    if (!ignoreEvents) {
      double myOldWidth = oldWidth + deltaWidth;
      double myOldHeight = oldHeight + deltaHeight;
      fireSizeChanged( myOldWidth, myOldHeight );
    }
  }

  @Override
  public void rotationChanged( PosDir posDir, Rotation oldRotation ) {
    fireRotationChanged( oldRotation );
  }
}
