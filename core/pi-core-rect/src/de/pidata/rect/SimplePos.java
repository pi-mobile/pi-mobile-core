/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rect;

/**
 * SimplePos is a simple Pos i.e. without dependencies on any other Pos objects
 */
public class SimplePos extends AbstractPos {

  public static final Pos ORIGIN = new SimplePos( 0, 0 );
  
  private double x;
  private double y;

  public SimplePos( double x, double y ) {
    this.x = x;
    this.y = y;
  }

  public SimplePos( Rect copyFrom ) {
    this.x = copyFrom.getX();
    this.y = copyFrom.getY();
  }

  @Override
  public double getX() {
    return x;
  }

  @Override
  public double getY() {
    return y;
  }

  @Override
  public void setX( double x ) {
    double oldX = getX();
    double oldY = getY();
    this.x = x;
    firePosChanged( oldX, oldY );
  }

  @Override
  public void setY( double y ) {
    double oldX = getX();
    double oldY = getY();
    this.y = y;
    firePosChanged( oldX, oldY );
  }

  @Override
  public void setPos( double x, double y ) {
    double oldX = getX();
    double oldY = getY();
    this.x = x;
    this.y = y;
    firePosChanged( oldX, oldY );
  }
}
