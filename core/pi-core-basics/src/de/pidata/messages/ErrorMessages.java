package de.pidata.messages;

public class ErrorMessages {

  public static final String FAILED = "Failed";
  public static final String ERROR = "Error";
  public static final String INTERNAL_ERROR = "Internal Error";
  public static final String ABORTED = "Aborted";
}
