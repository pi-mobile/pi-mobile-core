/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.geometry;

/**
 * A point is defined by its coordinates. Use {@link #Point(Vector)} to
 * describe a point by its coordinate vector.
 *
 * @author dsc
 */
public class Point extends Vector {

  public Point( double x, double y ) {
    super( x, y );
  }

  public Point( Vector v ) {
    super( v.getX(), v.getY() );
  }

  /**
   * Returns the distance between this and another point.
   *
   * @param other
   * @return
   */
  public double getDistance( Point other ) {
    double xd = other.getX() - this.getX();
    double yd = other.getY() - this.getY();
    double distance = Math.sqrt( xd * xd + yd * yd );
    return distance;
  }

  /**
   * Returns true if the distance to the other point is less than the given delta.
   *
   * @param other
   * @param delta
   * @return
   */
  public boolean isNearTo( Point other, double delta ) {
    double distance = getDistance( other );
    return  (distance < delta);
  }
}
