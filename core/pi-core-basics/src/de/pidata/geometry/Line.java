/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.geometry;

/**
 * Describes a line by using 2 {@link Point}s.
 *
 * @author dsc
 */
public class Line {

  private final Point p1;
  private final Point p2;

  public Line( Point p1, Point p2 ) {
    this.p1 = p1;
    this.p2 = p2;
  }

  public Point getP1() {
    return p1;
  }

  public Point getP2() {
    return p2;
  }

  /**
   * Returns the {@link Point} on the line with the given distance from p1.
   *
   * @param distance
   * @return
   */
  public Point getPointAt( double distance ) {
    double propDist = distance / p1.getDistance( p2 );
    double newX = ((p2.getX() - p1.getX()) * propDist) + p1.getX();
    double newY = ((p2.getY() - p1.getY()) * propDist) + p1.getY();
    Point newPoint = new Point( newX, newY );
    return newPoint;
  }

  /**
   * Returns the line running in parallel to this line at given distance.
   *
   * @param distance
   * @return
   */
  public Line getParallel( double distance ) {
    Vector p2Base = p2.add( new Vector( -p1.getX(), -p1.getY() ) );
    Vector p2BaseOrthogonal = p2Base.getOrthogonal();

    Vector p1Orthogonal = p2BaseOrthogonal.add( p1 );
    Vector p2Orthogonal = p2BaseOrthogonal.add( p2 );

    Point newP1 = new Line( p1, new Point( p1Orthogonal ) ).getPointAt( distance );
    Point newP2 = new Line( p2, new Point( p2Orthogonal ) ).getPointAt( distance );

    Line newLine = new Line( newP1, newP2 );
    return newLine;
  }

  /**
   * Returns the intersection {@link Point} between this line and the given other line.
   * Returns null if there is no intersection, that is the lines are parallel.
   *
   * @param otherLine
   * @return
   */
  public Point getIntersection( Line otherLine ) {
    double sx;
    double sy;

    Point o1 = otherLine.getP1();
    Point o2 = otherLine.getP2();

    double x1 = p1.getX();
    double x2 = p2.getX();

    double x3 = o1.getX();
    double x4 = o2.getX();

    double y1 = p1.getY();
    double y2 = p2.getY();

    double y3 = o1.getY();
    double y4 = o2.getY();

    double sxDividend = ((((x1 * y2) - (y1 * x2)) * (x3 - x4)) - ((x1 - x2) * ((x3 * y4) - (y3 * x4))));
    double syDividend = ((((x1 * y2) - (y1 * x2)) * (y3 - y4)) - ((y1 - y2) * ((x3 * y4) - (y3 * x4))));
    double sDivisor = (((x1 - x2) * (y3 - y4)) - ((y1 - y2) * (x3 - x4)));

    if (sDivisor == 0) {
      // no intersection
      return null;
    }
    sx = sxDividend / sDivisor;
    sy = syDividend / sDivisor;

    return new Point( sx, sy );
  }

}
