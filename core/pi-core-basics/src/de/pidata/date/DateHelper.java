/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.date;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by pru on 28.01.16.
 */
public class DateHelper {

  public static final long ONE_DAY_MILLIS = 24 * 3600 * 1000;

  /**
   * Pattern for Date format
   */
  public static final String DATE_PATTERN = "yyyy-MM-dd";

  /**
   * Pattern for standard Timestamp format
   */
  public static final String TIMESTAMP_PATTERN = "yyyy-MM-dd_HHmmssS";

  /**
   * Pattern for compact Timestamp format
   * e.g. used in SAP
   */
  public static final String COMPACT_TIMESTAMP_PATTERN = "yyyyMMddHHmmss";

  /**
   * Returns a string representing date with given separator and milii precision.
   * @param timeMillis the date in millis
   * @param withMillis if true result contains .milli otherwise not not
   * @param millisPrec precision for milli
   * @param sep sparator between hh mm ss milli
   * @return a string reprenting date using given format information
   */
  public static String toTimeString(long timeMillis, boolean withMillis, int millisPrec, char sep) {
    String temp;
    StringBuffer timeBuf = new StringBuffer();
    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(timeMillis);

    temp = Integer.toString(cal.get(Calendar.HOUR_OF_DAY));
    if (temp.length() == 1) timeBuf.append('0');
    timeBuf.append(temp).append(sep);

    temp = Integer.toString(cal.get(Calendar.MINUTE));
    if (temp.length() == 1) timeBuf.append('0');
    timeBuf.append(temp).append(sep);

    temp = Integer.toString(cal.get(Calendar.SECOND));
    if (temp.length() == 1) timeBuf.append('0');
    timeBuf.append(temp);

    if (withMillis) {
      timeBuf.append('.');
      temp = Integer.toString(cal.get(Calendar.MILLISECOND));
      for (int i = temp.length(); i < millisPrec; i++) {
        timeBuf.append('0');
      }
      timeBuf.append(temp);
    }
    return timeBuf.toString();
  }

  /**
   * Returns a string representing date using format YYYY-MM-DD
   * @see #parseDateString(String)
   *
   * @param timeMillis the date in millis
   * @return a string representing date using format YYYY-MM-DD
   */
  public static String toDateString(long timeMillis) {
    String temp;
    StringBuffer dateBuf = new StringBuffer();
    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(timeMillis);
    dateBuf.append(Integer.toString(cal.get(Calendar.YEAR))).append('-');
    temp = Integer.toString(cal.get(Calendar.MONTH)+1 );
    if (temp.length() == 1) dateBuf.append('0');
    dateBuf.append(temp).append('-');
    temp = Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
    if (temp.length() == 1) dateBuf.append('0');
    dateBuf.append(temp);
    return dateBuf.toString();
  }

  /**
   * Trys to get a date from a string representing date using format YYYY-MM-DD
   * @see #toDateString(long)
   *
   * @param dateString a string representing date using format YYYY-MM-DD
   * @return the date in millis
   */
  public static long parseDateString( String dateString ) throws ParseException {
    Date date = parseTimestamp( dateString, DATE_PATTERN );
    return date.getTime();
  }

  /**
   * returns Date of given timestamp in given pattern
   * @param timestamp to get Date
   * @param pattern to get Date
   * @return Date of given timestamp
   * @throws ParseException if timestamp could not be parsed
   */
  public static Date parseTimestamp(String timestamp, String pattern) throws ParseException {
    DateFormat dateFormat = new SimpleDateFormat( pattern );
    dateFormat.setLenient( false ); // don't apply any intelligent parsing
    return dateFormat.parse( timestamp );
  }

  /**
   * Returns a string reprenting date unsing format YYYY-MM-DD hh:mm:ss.milli
   * @param timeMillis the date in millis
   * @param withMillis if true result contains .milli otherwise not not
   * @return a string reprenting date unsing format YYYY-MM-DD hh:mm:ss.milli
   */
  public static String toDateTimeString(long timeMillis, boolean withMillis) {
    return toDateString(timeMillis) + " " + toTimeString(timeMillis, withMillis, 3, ':');
  }

  public static String timestamp() {
    return (toDateTimeString( System.currentTimeMillis(), true ) + " - ");
  }

  /**
   * returns timestamp of given Date using {@link #TIMESTAMP_PATTERN}
   * @param date to get timestamp
   * @return timestamp of given Date
   */
  public static String formatTimestamp( Date date ) {
    return new SimpleDateFormat( TIMESTAMP_PATTERN ).format( date );
  }

  /**
   * returns timestamp of given Date using {@link #COMPACT_TIMESTAMP_PATTERN}
   * @param date to get timestamp
   * @return timestamp of given Date
   */
  public static String formatCompactTimestamp( Date date ) {
    SimpleDateFormat timestampFormat = new SimpleDateFormat( COMPACT_TIMESTAMP_PATTERN );
    return timestampFormat.format( date );
  }

  public static String formatDuration( Date start, Date finish ) {
    long diff = finish.getTime() - start.getTime();
    long diffMillis = diff / 1000;
    long diffSeconds = diff / 1000 % 60;
    long diffMinutes = diff / (60 * 1000) % 60;
    long diffHours = diff / (60 * 60 * 1000);
    return String.format( "%02d:%02d:%02d.%03d", Long.valueOf( diffHours ), Long.valueOf( diffMinutes ), Long.valueOf( diffSeconds ), Long.valueOf( diffMillis ) );
  }

  /**
   * Returns the date part of this object by setting hour, minute, second and millisecond to zero
   * @return the date part of this object
   */
  public static long getDate( Date date) {
    Calendar calendar = Calendar.getInstance();
    Date tmpDate = new Date( date.getTime() );
    calendar.setTime( tmpDate );
    calendar.set( Calendar.HOUR_OF_DAY, 0 );
    calendar.set( Calendar.MINUTE, 0 );
    calendar.set( Calendar.SECOND, 0 );
    calendar.set( Calendar.MILLISECOND, 0 );
    return calendar.getTimeInMillis();
  }

  /**
   * Note: In some cases 24 houres are not the same as 1 day because set-up of daylightsaving
   *
   * @apiNote should go to a model independent Date class
   */
  public static long subtractDays( long date, int days ) throws IllegalArgumentException {
    if (days < 0) {
      throw new IllegalArgumentException ("Argument days are wrong: days < 0");
    }
    if (days != 0) {
      Calendar cal = Calendar.getInstance();
      long newdate = date - days * ONE_DAY_MILLIS;
      return adjustDate(date, cal, newdate);
    }
    else {
      return date;
    }
  }

  public static Date addTime( Date date, int timeOffset ) {
    Calendar cal = Calendar.getInstance();
    cal.setTime( date );
    cal.add( Calendar.SECOND, timeOffset );
    return cal.getTime();
  }

  /**
   *
   * @param date
   * @param cal
   * @param newdate
   * @return
   *
   * @apiNote should go to a model independent Date class
   */
  private static long adjustDate(long date, Calendar cal, long newdate) {
    int offsetDate;
    int offsetNewdate;
    cal.setTime(new Date(date));
    offsetDate = cal.getTimeZone().getOffset(1, cal.get(Calendar.YEAR),
        cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.DAY_OF_WEEK),
        cal.get(Calendar.MILLISECOND));
    cal.setTime(new Date(newdate));
    offsetNewdate = cal.getTimeZone().getOffset(1, cal.get(Calendar.YEAR),
        cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.DAY_OF_WEEK),
        cal.get(Calendar.MILLISECOND));

    if (offsetDate == offsetNewdate) {
      return newdate;
    }
    else {
      return newdate + offsetDate - offsetNewdate;
    }
  }
}
