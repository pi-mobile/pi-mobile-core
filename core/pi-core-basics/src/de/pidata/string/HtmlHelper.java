package de.pidata.string;

import java.util.stream.Collectors;

public class HtmlHelper {

  public static String escapeHTML(String str) {  // TODO:  handle tabs, spaces etc.
    return str.chars().mapToObj(c -> c > 127 || "\"'<>&".indexOf(c) != -1 ?
        "&#" + c + ";" : String.valueOf((char) c)).collect( Collectors.joining());
  }

}
