package de.pidata.string;

public class UriHelper {

  /**
   * Strip the namespace part from the URI and return the simple name. Typically used with OSLC.
   *
   * @param uri
   * @return
   */
  public static String getNamePartFromUri( String uri ) {
    String nsName;
    String simpleName;
    int nsSepPos = uri.lastIndexOf( '#' );
    if (nsSepPos < 0) {
      nsSepPos = uri.lastIndexOf( '/' );
    }
    if (nsSepPos >= 0 && nsSepPos < uri.length()) {
      simpleName = uri.substring( nsSepPos + 1 );
      return simpleName;
    }
    return null;
  }
}
