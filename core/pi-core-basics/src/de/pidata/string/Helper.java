/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.string;

import de.pidata.log.Logger;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <add description here>
 *
 * @author Doris Schwarzmaier
 */
public class Helper {


  public static String getNonbreakingString( String string ) {
    if (string == null) return string;
    else return string.replaceAll( "\\n", "" );
  }

  public static Iterator<Object> sortIterator ( Iterator<Object> iter ) {
    List keys = new ArrayList();
    while ( iter.hasNext() ) {
      keys.add( iter.next() );
    }
    Collections.sort( keys );
    iter = keys.iterator();

    return iter;
  }

  public static String removeWhitespace( String text ) {
    if (text == null) return "";
    text = text.trim();
    StringBuilder builder = new StringBuilder();
    boolean lastWasSpace = false;
    for (int i = 0; i < text.length(); i++) {
      char ch = text.charAt( i );
      if ((ch <= ' ')            // control chars and space
              || (ch == 160)) {      // non breakable space
        if (!lastWasSpace) {
          builder.append( ' ' );
          lastWasSpace = true;
        }
      }
      else if (ch == 8201) {  // thin space
        // ignore character
      }
      else {
        builder.append( ch );
        lastWasSpace = false;
      }
    }
    return builder.toString();
  }

  public static boolean equalsIgnoreWhitespace( String str1, String str2 ) {
    String tmp1 = str1.replaceAll( "\\s", "" );
    String tmp2 = str2.replaceAll( "\\s", "" );
    return  tmp1.equals( tmp2 );
  }

  public static String sanitizeFilename( String origFilename, boolean keepExtensionSeparator ) {
    if (!isNullOrEmpty( origFilename )) {
      String regEx;
      if (keepExtensionSeparator) {
        regEx = "[^a-zA-Z0-9\\.\\-]";
      }
      else {
        regEx = "[^a-zA-Z0-9\\-]";
      }
      String sanitizedFilename = origFilename.replaceAll( regEx, "_" );
      return sanitizedFilename;
    }
    return origFilename;
  }

  /**
   * IMPORTANT: currently unused, do not convert names in this level - might be necessary on business/function leve.
   * @param name
   * @return
   */
  public static String convertToValidName( String name ) {
    if (!isNullOrEmpty( name )) {
      String regEx = "[^a-zA-Z0-9_]";
      String sanitizedName = name.replaceAll( regEx, "_" );
      regEx = "^[0-9]";
      if (sanitizedName.matches( regEx )) {
        sanitizedName = "_" + sanitizedName;
      }
      return sanitizedName;
    }
    return name;
  }

  /**
   * Checks if texts match. If one of the given texts ends with a wildcard then the
   * starting portion is checked to match.
   *
   * @param currentText
   * @param expectedText
   * @return
   */
  public static boolean matchesText( String currentText, String expectedText ) {
    if (currentText == null || expectedText == null) {
      return false;
    }
    if (currentText.equals( expectedText )) {
      return true;
    }

    if (currentText.endsWith( "..." )) {
      String testCurrent = currentText.substring( 0, currentText.length() - "...".length() );
      if (expectedText.endsWith( "*" )) {
        String testExpected = expectedText.substring( 0, expectedText.length() - "*".length() );
        return testCurrent.startsWith( testExpected );
      }
      else {
        return expectedText.startsWith( testCurrent );
      }
    }
    else {
      if (expectedText.endsWith( "*" )) {
        String testExpected = expectedText.substring( 0, expectedText.length() - "*".length() );
        return currentText.startsWith( testExpected );
      }
    }
    return false;
  }

  public static boolean isNotNullAndNotEmpty( Object obj ) {
    return !isNullOrEmpty( obj );
  }

  public static boolean isNullOrEmpty( Object obj ) {
    if (obj == null) {
      return true;
    }
    else if (obj instanceof String) {
      return ((String) obj).isEmpty();
    }
    else if (obj instanceof StringBuilder) {
      return ((StringBuilder) obj).length() == 0;
    }
    else if (obj instanceof Collection) {
      return ((Collection) obj).isEmpty();
    }
    else if (obj instanceof HashMap) {
      return ((HashMap<?, ?>) obj).isEmpty();
    }
    else {
      Logger.warn( "isNullOrEmpty unknown for [" + obj.getClass().getName() + "] " + obj );
    }
    return false;
  }

  public static boolean equals( Object obj1, Object obj2 ) {
    if (obj1 == null) {
      return (obj2 == null);
    }
    else {
      return (obj1.equals( obj2 ));
    }
  }

  /**
   * Returns the boolean value of the given boolObject. If boolObject is null, returns false.
   *
   * @param boolObject the Boolean to analyse
   * @return false if boolOBject is null; the boolean value of boolObject otherwise
   */
  public static boolean getBoolean(Object boolObject) {
    if (boolObject == null) return false;
    else if (boolObject instanceof Boolean) return ((Boolean) boolObject).booleanValue();
    else return false;
  }

  public static AbstractMap.SimpleEntry<String, String> splitKeyValue( String keyValueString, String separator ) {
    String tmp = keyValueString.replaceAll( "\\s", "" );
    String[] keyValue = tmp.split( separator );
    if (keyValue.length == 2) {
      return new AbstractMap.SimpleEntry<String, String>( keyValue[0], keyValue[1] );
    }
    return null;
  }

  public static List<Integer> getIntegerArrayFromString(String string, String separator){
    List<Integer> numList = new ArrayList<Integer>();
    if (!Helper.isNullOrEmpty( string )) {
      String stringArray[] = string.split( separator );
      for (String numString : stringArray) {
        int num = Integer.parseInt( numString );
        numList.add( Integer.valueOf( num ) );
      }
    }
    return numList;
  }

  /**
   * Replaces each single occurence of a LF by CR/LF
   *
   * @param text the text to fix
   * @return text having for each line break a CR/LF
   */
  public static String fixLineEndsWindows( String text ) {
    StringBuilder builder = new StringBuilder( text );
    for (int i = builder.length()-1; i >= 0; i--) {
      char ch = builder.charAt( i );
      if (ch == '\n') {
        if ((i == 0) || (builder.charAt( i-1 ) != '\r')) {
          builder.insert( i, '\r' );
        }
      }
    }
    return builder.toString();
  }

  /**
   * Sorts a Dictionary. Copies all into a SortedList. Consider to use one originally.
   *
   * @param <T>
   * @param theDictionary The Dictionary to sort.
   * @return
   */
  public static <T extends Object> SortedMap<Integer, T> getSortedList( Map<Integer, T> theDictionary ) {
    SortedMap<Integer, T> sortList = new TreeMap<Integer, T>();
    for (Integer dictKey : theDictionary.keySet()) {
      sortList.put( dictKey, theDictionary.get( dictKey ) );
    }
    return sortList;
  }

  /**
   * Sorts a Dictionary. Copies all into a SortedList. Consider to use one originally.
   *
   * @param theDictionary The Dictionary to sort.
   * @param <T>
   * @return
   */
  public static <T extends Object> SortedMap<String, T> getSortedStringList( Map<String, T> theDictionary ) {
    SortedMap<String, T> sortList = new TreeMap<String, T>();
    for (String dictKey : theDictionary.keySet()) {
      sortList.put( dictKey, theDictionary.get( dictKey ) );
    }
    return sortList;
  }

  /**
   * Create a mostly randomized string of given length. May be used to create partial kind of GUIDs.
   *
   * @param targetStringLength
   * @return
   * @implNote inspired by https://www.baeldung.com/java-random-string
   */
  public static String getRandomizedString( int targetStringLength ) {
    int leftLimit = 48; // numeral '0'
    int rightLimit = 122; // letter 'z'
    Random random = new Random();

    String generatedString = random.ints(leftLimit, rightLimit + 1)
        .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
        .limit(targetStringLength)
        .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
        .toString();

    System.out.println(generatedString);
    return generatedString;
  }

  /**
   * for info log messages that are only of interest if debug is set to true
   * @param message
   * @param debug
   */
  public static void infoLogOnDebug( String message, boolean debug ) {
    if (debug) {
      Logger.info( message );
    }
  }

  public static void debugLogOnDebug( String message, boolean debug ) {
    if (debug) {
      Logger.debug( message );
    }
  }

  /**
   * removes prefix:: from prefix::restOfTheString
   * @param stringWithPrefix
   * @return
   */
  public static String removePrefix( String stringWithPrefix ) {
      if(stringWithPrefix.contains( "::" )) {
          stringWithPrefix = stringWithPrefix.substring( stringWithPrefix.indexOf( "::" ) + 2 );
      }

      return stringWithPrefix;
  }

  public static String trimIfPossible( String stringToTrim ) {
    if(stringToTrim != null) {
      stringToTrim = stringToTrim.trim();
    }

    return stringToTrim;
  }

  public static String replaceParams( String str, String paramStart, String paramEnd, Properties paramProps ) {
    if (paramProps == null) {
      return str;
    }
    if (str == null) {
      return null;
    }
    int pos = str.indexOf( paramStart );
    if (pos < 0) {
      return str;
    }
    int start = 0;
    StringBuilder result = new StringBuilder();
    while (pos >= 0) {
      result.append( str.substring( start, pos ) );
      int pos2 = str.indexOf( paramEnd, pos+1 );
      if (pos2 > 0) {
        String paramName = str.substring( pos + paramStart.length(), pos2 );
        Object value = paramProps.get( paramName );
        result.append( value );
        start = pos2 + paramEnd.length();
      }
      else {
        start = pos + paramStart.length();
      }
      pos = str.indexOf( paramStart, start );
    }
    result.append( str.substring( start ) );
    return result.toString();
  }


  /**
   * sometimes it is useful to have log entries in a specific order, e.g. first all successful findings, then all unsuccessful
   * they can be collect in a list, and by using this method they can be added to the log in a overview-friendly order
   * @param logList
   */
  public static void addLogEntriesFromList(ArrayList<String> logList, boolean loggingEnabled) {

    if(loggingEnabled) {
      for (String s : logList) {
        Logger.debug( s );
      }
    }

  }

  public static String getValueOrDefaultStringIfNull(String value) {
   return getValueOrDefaultStringIfNull(value, "");
  }

  public static String getValueOrDefaultStringIfNull(String value, String defaultIfNull) {
    if(Helper.isNullOrEmpty( value )) {
      if(Helper.isNotNullAndNotEmpty( defaultIfNull )) {
        return defaultIfNull;
      } else {
        return "";
      }
    } else {
      return value;
    }
  }

  public static void logUsefulExceptionReasons( Exception e ) {

    String usefulMsg = "";

    String errorMsg = e.getMessage();

    if (errorMsg.contains( "Unauthorized" )) {
      usefulMsg = usefulMsg + "got statuscode 401 - unauthorized";
    } else {
      usefulMsg = e.getMessage();
    }

    if (isNotNullAndNotEmpty( usefulMsg )) {
      Logger.error( usefulMsg );
    }
  }

  /**
   * removes the attribute shape from any "a" html/xml tag
   * @param input
   * @return
   */
  public static String removeShapeAttributeFromATags( String input ) {
    return removeAttributeFromAllTags( input, "a", "shape" );
  }


  /**
   * removes the attribute with the given name from all tags with the given name
   * may be useful for testing or hyperspecialized calls where model operations may be
   * complicated or not possible out of the box
   * @param input
   * @param tagName e.g. a html body p
   * @param attributeName
   * @return
   */
  public static String removeAttributeFromAllTags( String input, String tagName, String attributeName ) {

    if (Helper.isNullOrEmpty( input )) {
      return input;
    }

    if (Helper.isNullOrEmpty( tagName )) {
      Logger.warn( "removeAttributeFromAllTags tagName is null or empty - returning input as is" );
      return input;
    }

    if (Helper.isNullOrEmpty( attributeName )) {
      Logger.warn( "removeAttributesFromAllTags attributeName is null or empty - returning input as is" );
      return input;
    }

    // Define the regex pattern dynamically using the given tag and attribute names
    String regex = "(<" + tagName + "\\b[^>]*?)\\s+" + attributeName + "=\"[^\"]*\"(.*?>)";

    // Compile the regex pattern
    Pattern pattern = Pattern.compile( regex );

    // Create a matcher for the given input string
    Matcher matcher = pattern.matcher( input );

    // Replace all occurrences of the specified attribute in the specified tag
    return matcher.replaceAll( "$1$2" );
  }


  public static String getMessageByBooleanValue( boolean b, String messageIfTrue, String messageIfFalse ) {
    if (b) {
      return messageIfTrue;
    }
    else {
      return messageIfFalse;
    }
  }



}
