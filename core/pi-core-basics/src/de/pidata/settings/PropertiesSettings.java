/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.settings;

import java.util.Properties;

public class PropertiesSettings implements Settings {

  private Properties properties;

  public PropertiesSettings( Properties properties ) {
    this.properties = properties;
  }

  public Properties getProperties() {
    return properties;
  }

  @Override
  public String getString( String key, String defaultValue ) {
    String str = properties.getProperty( key );
    if (str == null) {
      return defaultValue;
    }
    else {
      return str;
    }
  }

  @Override
  public boolean getBoolean( String key, boolean defaultValue ) {
    String str = properties.getProperty( key );
    if (str == null) {
      return defaultValue;
    }
    else {
      return Boolean.parseBoolean( str );
    }
  }

  @Override
  public int getInt( String key, int defaultValue ) {
    String str = properties.getProperty( key );
    if (str == null) {
      return defaultValue;
    }
    else {
      return Integer.parseInt( str );
    }
  }

  @Override
  public double getDouble( String key, double defaultValue ) {
    String str = properties.getProperty( key );
    if (str == null) {
      return defaultValue;
    }
    else {
      return Double.parseDouble( str );
    }
  }

  @Override
  public void setString( String key, String value ) {
    properties.setProperty( key, value );
  }

  @Override
  public void setBoolean( String key, boolean value ) {
    properties.setProperty( key, Boolean.toString( value ) );
  }

  @Override
  public void setInt( String key, int value ) {
    properties.setProperty( key, Integer.toString( value ) );
  }

  @Override
  public void setDouble( String key, double value ) {
    properties.setProperty( key, Double.toString( value ) );
  }
}
