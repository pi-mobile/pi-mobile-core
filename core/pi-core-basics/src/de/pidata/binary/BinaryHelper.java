package de.pidata.binary;

/**
 * Java does not have real unsigned / byte handling so this provides helper methods
 * to easily deal with binary and hex values represented as int.
 * Also provides helper methods to build readable string representations for binary and hex values
 * which might be useful for analysis.
 */
public class BinaryHelper {

  /**
   * Builds a hex representation of the given value in groups of 2 digits.
   *
   * @param value
   * @return
   */
  public static String hexByte( byte value ) {
    int intValue = Byte.toUnsignedInt( value );
    return hexByte( intValue );
  }

  /**
   * Builds a hex representation of the given value in groups of 2 digits.
   *
   * @param value
   * @return
   */
  public static String hexByte( int value ) {
    String hex = Integer.toHexString( value );
    if (hex.length() == 1) {
      return "0" + hex;
    }
    else if (hex.length() == 2) {
      return hex;
    }
    else {
      return hex.substring( hex.length()-2 );
    }
  }

  /**
   * Composes an unsigned representation of the given byte values.
   *
   * @param high
   * @param low
   * @return
   */
  public static int word( int high, int low ) {
    return (high << 8) + low;
  }

  /**
   * Composes an unsigned representation of the given byte values.
   *
   * @param high
   * @param low
   * @return
   */
  public static int word( int high, byte low ) {
    return (high << 8) + Byte.toUnsignedInt(low);
  }

  /**
   * Returns true if the bit at the given position is set within the given value;
   * false otherwise.
   *
   * @param byteValue
   * @param bit
   * @return
   */
  public static boolean bitRead( byte byteValue, int bit ) {
    int value = Byte.toUnsignedInt( byteValue );
    return ((value & (1 << bit)) != 0);
  }

  /**
   * Returns true if the bit at the given position is set within the given value;
   * false otherwise.
   *
   * @param value
   * @param bit
   * @return
   */
  public static boolean bitRead( int value, int bit ) {
    return ((value & (1 << bit)) != 0);
  }

  public static int bitSet( int value, int bit ) {
    return ((1 << bit) | value);
  }

  public static int bitClear( int value, int bit ) {
    return (~(1 << bit) & value);
  }

  /**
   * Retrieves the lower byte part from the given unsigned representation.
   *
   * @param value
   * @return
   */
  public static int lowByte( int value ) {
    return (value & 0xFF);
  }

  /**
   * Retrieves the higher byte part from the given unsigned representation.
   *
   * @param value
   * @return
   */
  public static int highByte( int value ) {
    return ((value >> 8) & 0xFF);
  }

  /**
   * Builds a binary representation of the given value; zero is replaced by lowercase 'o'.
   *
   * @param value
   * @return
   */
  public static String binByte( byte value ) {
    int intValue = Byte.toUnsignedInt( value );
    return binByte( intValue );
  }

  /**
   * Builds a binary representation of the given value; zero is replaced by lowercase 'o'.
   *
   * @param value
   * @return
   */
  public static String binByte( int value ) {
    String binaryString = Integer.toBinaryString( value );
    binaryString = String.format( "%16s", binaryString )
        .replace( ' ', 'o' )
        .replace( '0', 'o' );
    return binaryString;
  }

  /**
   * Builds a String representation of the given data array
   * as groups of 2 hexadecimal digits.
   *
   * @param data
   * @return
   */
  public static String hexStr( byte[] data ) {
    StringBuilder sb = new StringBuilder();
    for (int x = 0; x < data.length; x++) {
      sb.append( hexByte( data[x] ) );
      sb.append( " " );
    }
    return sb.toString();
  }

  /**
   * Builds a String representation of the given data array
   * as groups of 2 hexadecimal digits.
   *
   * @param data
   * @return
   */
  public static String hexStr( int[] data ) {
    StringBuilder sb = new StringBuilder();
    for (int x = 0; x < data.length; x++) {
      sb.append( hexByte( data[x] ) );
      sb.append( " " );
    }
    return sb.toString();
  }
}