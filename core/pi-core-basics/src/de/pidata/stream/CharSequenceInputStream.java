/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.stream;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by PIData on 17.05.2016.
 */
public class CharSequenceInputStream extends InputStream {

  private CharSequence charSequence;
  private int pos = -1;
  private int end = -1;

  public CharSequenceInputStream( CharSequence charSequence ) {
    this.charSequence = charSequence;
  }

  public CharSequenceInputStream( CharSequence charSequence, int start, int end ) {
    this.charSequence = charSequence;
    this.pos = start - 1;
    this.end = end;
  }

  @Override
  public int read() throws IOException {
    pos++;
    if (end < 0) {
      if (pos < charSequence.length()) {
        return charSequence.charAt( pos );
      }
    }
    else {
      if (pos < end) {
        return charSequence.charAt( pos );
      }
    }
    return -1;
  }
}
