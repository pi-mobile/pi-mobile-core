/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.stream;

import de.pidata.log.Logger;
import de.pidata.progress.ProgressListener;

import java.io.*;

public class StreamHelper {

  /**
   * Writes data from buffer to os until end of buffer or maxCharCount is reached
   * @param buffer
   * @param os
   * @param maxCharCount the maximum number of char to write to os
   * @param progressListener
   * @return
   * @throws IOException
   */
  public static int buffer2Stream( StringBuilder buffer, OutputStreamWriter os, int maxCharCount, ProgressListener progressListener ) throws IOException {
    int bufSize = 100;
    char[] buf = new char[bufSize];
    int sum = 0;
    int len = buffer.length();
    if (len > maxCharCount) len = maxCharCount;
    int count;
    if (progressListener != null) progressListener.setMaxValue( len );
    while (sum < len) {
      count = len-sum;
      if (count > bufSize) count = bufSize;
      buffer.getChars(sum, sum+count, buf, 0);
      if (progressListener != null) progressListener.updateProgress( null, sum );
      os.write(buf, 0, count);
      sum += count;
    }
    os.flush();
    return sum;
  }

  public static boolean stream2Buffer( InputStream input, StringBuilder buffer, int maxCharCount, boolean testRepeat ) {
    InputStreamReader reader = new InputStreamReader(input);
    //BufferedReader br = new BufferedReader( reader );
    return stream2Buffer( reader, buffer, maxCharCount, testRepeat );
  }  

  /**
   * Reads lines from input until EOF, empty line or maxCharCount is reached
   * @param br
   * @param buffer
   * @param maxCharCount th maximum number of char to read from input
   * @param testRepeat if set to true, the stream will be cut after reaching maxCharCount
   * @return false if a IOException occured while reading stream
   */
  public static boolean stream2Buffer( Reader br, StringBuilder buffer, int maxCharCount, boolean testRepeat ) {
    String line;
    int count = 0;
    try {
      while ((line = readLine(br)) != null) {
        if (count + line.length() < maxCharCount) {
          buffer.append(line).append("\n");
          count += line.length() + 1;
        }
        else {
          buffer.append( line.substring( 0, maxCharCount -count ) );
          if (testRepeat)
            throw new IOException("...should start recovering...");
          else
            return true;
        }
      }
    }
    catch (IOException e) {
      Logger.error("Could not read input", e);
      return false;
    }
    return true;
  }

  public static String readLine( InputStream input ) throws IOException {
    return readUntil( input, '\n' );
  }

  public static String readUntil( InputStream input, char stopChar ) throws IOException {
    int ch;
    StringBuffer buf = new StringBuffer();
    ch = input.read();
    if (ch == -1) return null;
    while (ch != stopChar) {
      buf.append( (char) ch);
      ch = input.read();
      if (ch == -1) return null;
    }
    return buf.toString();
  }

  public static String readLine( Reader reader ) throws IOException {
    int ch;
    StringBuffer buf = new StringBuffer();
    ch = reader.read();
    if (ch == -1) return null;
    while (ch != '\n') {
      buf.append( (char) ch);
      ch = reader.read();
      if (ch == -1) return null;
    }
    return buf.toString();
  }


  /**
   * Read given {@link InputStream} in to a byte[].
   *
   * @param stream
   * @return the byte[] containing the stream's data
   * @throws IOException
   */
  public static byte[] toBytes( InputStream stream ) throws IOException {
    ByteArrayOutputStream fileBytesOutputStream = new ByteArrayOutputStream();
    try {
      int read;
      byte[] next = new byte[1024];
      while ((read = stream.read( next )) != -1) {
        fileBytesOutputStream.write( next, 0, read );
      }
    }
    finally {
      stream.close();
      fileBytesOutputStream.close();
    }
    return fileBytesOutputStream.toByteArray();
  }

  /**
   * Closes stream and catches any exceptions
   * @param stream the stream to close or null
   */
  public static void close( InputStream stream ) {
    if (stream != null) {
      try {
        stream.close();
      }
      catch (Exception ex) {
        // do nothing
      }
    }
  }

  /**
   * Closes reader and catches any exceptions
   * @param reader the reader to close or null
   */
  public static void close( Reader reader ) {
    if (reader != null) {
      try {
        reader.close();
      }
      catch (IOException e) {
        // do nothing
      }
    }
  }

  /**
   * Closes stream and catches any exceptions
   * @param stream the stream to close or null
   */
  public static void close( OutputStream stream ) {
    if (stream != null) {
      try {
        stream.close();
      }
      catch (Exception ex) {
        // do nothing
      }
    }
  }

  /**
   * Closes stream and catches any exceptions
   * @param writer the stream to close or null
   */
  public static void close( OutputStreamWriter writer ) {
    if (writer != null) {
      try {
        writer.close();
      }
      catch (Exception ex) {
        // do nothing
      }
    }
  }

  /**
   * Closes stream and catches any exceptions
   * @param writer the stream to close or null
   */
  public static void close( PrintWriter writer ) {
    if (writer != null) {
      try {
        writer.close();
      }
      catch (Exception ex) {
        // do nothing
      }
    }
  }
}
