/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.worker;

import de.pidata.log.Logger;

/**
 * Created by pru on 22.07.2016.
 */
public abstract class WorkerJob {

  private String name;
  private boolean finished = false;
  protected boolean doLogging;
  private Exception exception = null;
  private Exception creatorTrace;

  public WorkerJob( String name, boolean doLogging ) {
    this.name = name;
    this.doLogging = doLogging;
    this.creatorTrace = new Exception( "WorkerJob creator trace" );
  }

  public String getName() {
    return name;
  }

  public boolean hasFinished() {
    return finished;
  }

  public Exception getException() {
    return exception;
  }

  public Exception getCreatorTrace() {
    return creatorTrace;
  }

  public abstract void run();

  public final void execute() {
    try {
      if (doLogging) Logger.info( "---> Starting WorkerJob name=" + name );
      long startMillis = System.currentTimeMillis();
      run();
      long finishedMillis = System.currentTimeMillis();
      if (doLogging) Logger.info( "<--- Finished WorkerJob name=" + name + " [" + (finishedMillis - startMillis) + "]" );
    }
    catch (Exception ex) {
      this.exception = ex;
      Logger.error( "Error running WorkerJob name="+name, ex );
    }
    finished = true;
  }
}
