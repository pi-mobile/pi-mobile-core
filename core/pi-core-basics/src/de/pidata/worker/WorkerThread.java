package de.pidata.worker;

public class WorkerThread extends Thread {

  private WorkerJob job = null;

  public WorkerJob getJob() {
    return job;
  }

  protected void setJob( WorkerJob job ) {
    this.job = job;
  }

  public WorkerThread( Runnable target, String name ) {
    super( target, name );
  }
}
