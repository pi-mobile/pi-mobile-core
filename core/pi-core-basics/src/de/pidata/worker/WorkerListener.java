/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.worker;

public interface WorkerListener {

  /**
   * Called by worker whenever a job has finished and no further job is available, i.e. worker is idle.
   * Internal idle() is called before this notification.
   * @param worker calling worker
   */
  void workerStateIdle( Worker worker );

  /**
   * Called by worker just before job is started.
   * @param worker calling worker
   * @param job job to be started
   */
  void workerStateStarting( Worker worker, WorkerJob job );

  /**
   * Called by worker just after job has finished and worker has processed internal jobFinished().
   * @param worker calling worker
   * @param job job which has finished
   */
  void workerStateFinished( Worker worker, WorkerJob job );

}
