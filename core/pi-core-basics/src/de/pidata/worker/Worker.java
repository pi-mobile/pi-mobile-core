/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.worker;

import de.pidata.log.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

/**
 * Created by pru on 26.07.2016.
 */
public class Worker implements Runnable {

  private static final boolean DEBUG = false;

  private List<WorkerJob> jobList = new LinkedList<WorkerJob>();
  private boolean running = false;
  private boolean working = false;
  private List<WorkerListener> listeners = new LinkedList<WorkerListener>();

  public synchronized void addJob( WorkerJob workerJob ) {
    this.jobList.add( workerJob );
  }

  public void close() {
    working = false;
  }

  public boolean isRunning() {
    return running;
  }

  protected void starting() {
    // default do nothing
  }

  protected void stopping() {
    // default do nothing
  }

  protected void jobFinished( WorkerJob job, boolean success ) {
    // do nothing
  }

  protected void jobError( WorkerJob job, Exception ex ) {
    Logger.error( "Exception running job name=" + job.getName(), ex );
  }

  @Override
  public void run() {
    running = true;
    working = true;
    WorkerThread workerThread = (WorkerThread) Thread.currentThread();
    try {
      starting();
      while (working) {
        if (DEBUG) Logger.debug( "- Check for job" );
        WorkerJob job = null;
        synchronized (this) {
          if (jobList.size() > 0) {
            job = jobList.remove( 0 );
          }
        }
        if (job == null) {
          if (DEBUG) Logger.debug( "  no job" );
          idle();

          //---send state idle to listeners
          for (WorkerListener listener : listeners) {
            try {

              listener.workerStateIdle( this );
            }
            catch (Exception e) {
              e.printStackTrace();
              Logger.error( "Exception while firing worker event: Idle", e );
            }
          }
        }
        else {
          if (DEBUG) Logger.debug( "  process job " + job.getName() );
          boolean success = false;

          //---send state starting to listeners
          for (WorkerListener listener : listeners) {
            try {
              listener.workerStateStarting( this, job );
            }
            catch (Exception e) {
              e.printStackTrace();
              Logger.error( "Exception while firing worker event: starting", e );
            }
          }

          //---execute job
          workerThread.setJob( job );
          try {
            job.execute();
            success = true;
          }
          catch (Exception ex) {
            jobError( job, ex );
          }
          jobFinished( job, success );
          workerThread.setJob( null );

          //---send state finished to listeners
          for (WorkerListener listener : listeners) {
            try {
              listener.workerStateFinished( this, job );
            }
            catch (Exception e) {
              e.printStackTrace();
              Logger.error( "Exception while firing worker event: finished", e );
            }
          }

          if (DEBUG) Logger.debug( "  finished job" );
        }
      }
      stopping();
    }
    finally {
      running = false;
    }
  }

  /**
   * Called by run loop if job list is empty.
   * Should at least sleep a little bit.
   */
  protected void idle() {

    try {
      Thread.sleep( 100 );
    }
    catch (InterruptedException e) {
      // do nothing
    }
  }

  public synchronized boolean addJobIfIdle( WorkerJob workerJob ) {
    if (jobList.size() == 0) {
      addJob( workerJob );
      return true;
    }
    else if (jobList.size() == 1 && jobList.get( 0 ).getName().equals( workerJob.getName() )) {
      return true;
    }
    else {
      return false;
    }
  }

  /**
   * Add listener to this worker.
   *
   * @param workerListener the listener
   */
  public void addWorkerListener( WorkerListener workerListener ) {
    if (this.listeners == null) {
      this.listeners = new LinkedList<WorkerListener>();
    }
    this.listeners.add( workerListener );
  }

  /**
   * Remove listner from this worker.
   *
   * @param workerListener the listener
   */
  public void removeWorkerListener( WorkerListener workerListener ) {
    if (this.listeners != null) {
      this.listeners.remove( workerListener );
    }
  }

  public synchronized int jobCount() {
    return jobList.size();
  }
}
