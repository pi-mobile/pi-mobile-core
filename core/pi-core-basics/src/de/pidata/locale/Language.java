package de.pidata.locale;

public enum Language {
  ARABIC,
  BENGALI,
  CHINESE,
  DUTCH,
  ENGLISH,
  FRENCH,
  GERMAN,
  HINDI,
  INDONESIAN,
  ITALIAN,
  JAPANESE,
  KOREAN,
  MANDARIN,
  POLISH,
  PORTUGUESE,
  ROMANIAN,
  SPANISH,
  TURKISH,
  UKRAINIAN,
  URDU,
  VIETNAMESE;

  @Override
  public String toString() {
    // Format the enum name to a more readable format
    return name().charAt(0) + name().substring(1).toLowerCase();
  }
}
