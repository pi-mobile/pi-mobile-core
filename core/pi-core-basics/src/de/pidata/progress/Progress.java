/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.progress;

import de.pidata.messages.ErrorMessages;
import de.pidata.log.Logger;
import de.pidata.string.Helper;

public class Progress {

  private static final boolean DEBUG = false;

  private double start;
  private double end;
  private int    steps;
  private double stepSize;
  private double current;

  private ProgressListener progressListener;

  private Progress childProgress = null;

  /**
   * Creates a new progress starting at 0.0 and ending at 100.0
   * @param steps number of steps on top level of progress
   * @param progressListener that view is called whenever current progress value changes, may be null
   */
  public Progress( int steps, ProgressListener progressListener ) {
    this( 0.0, 100.0, steps, progressListener );
  }

  /**
   * Creates a new progress
   * @param start initial progress value
   * @param end   final progress value
   * @param steps number of steps on top level of progress
   * @param progressListener that view is called whenever current progress value changes, may be null
   */
  public Progress( double start, double end, int steps, ProgressListener progressListener ) {
    this.start = start;
    this.end = end;
    this.steps = steps;
    if (steps == 0) {
      stepSize = 0;
    }
    else {
      stepSize = (end - start) / steps;
    }
    this.current = start;
    this.progressListener = progressListener;
    setCurrent( 0 );
    resetColor();
    if (DEBUG) Logger.debug( "Progress start="+start+", end="+end+", stepSize="+stepSize );
  }

  public double getStart() {
    return start;
  }

  public double getEnd() {
    return end;
  }

  /**
   * Retuns overall current position
   * @return
   */
  public double getCurrent() {
    if (childProgress == null) {
      return current;
    }
    else {
      return childProgress.getCurrent();
    }
  }

  public double setCurrent( double newCurrent, String message ) {
    if (DEBUG) Logger.debug( "Progress.setCurrent="+newCurrent );
    if (newCurrent < current) {
      if (DEBUG) Logger.debug( ErrorMessages.ERROR + " newCurrent="+newCurrent+" < current="+current );
    }
    else {
      this.current = newCurrent;
      if (current > end) {
        if (DEBUG) Logger.debug( ErrorMessages.ERROR + " newCurrent="+newCurrent+" > end="+end );
        current = end;
      }
    }
    if (progressListener != null) {
      progressListener.updateProgress( message, current );
    }
    return current;
  }

  public double setCurrent( double newCurrent ) {
    if (DEBUG) Logger.debug( "Progress.setCurrent="+newCurrent );
    if (newCurrent < current) {
      if (DEBUG) Logger.debug( ErrorMessages.ERROR + " newCurrent="+newCurrent+" < current="+current );
    }
    else {
      this.current = newCurrent;
      if (current > end) {
        if (DEBUG) Logger.debug( ErrorMessages.ERROR + " newCurrent="+newCurrent+" > end="+end );
        current = end;
      }
    }
    if (progressListener != null) {
      progressListener.updateProgress( current );
    }
    return current;
  }

  /**
   * Moves current step of last child in chain to next position
   * @return new overall current progress
   */
  public double next( String message ) {
    if (childProgress == null) {
      return setCurrent( current + stepSize, message );
    }
    else {
      if (DEBUG) Logger.debug( "." );
      return childProgress.next( message );
    }
  }

  /**
   * Moves current step of last child in chain to next position
   * @return new overall current progress
   */
  public double next(  ) {
    if (childProgress == null) {
      return setCurrent( current + stepSize );
    }
    else {
      if (DEBUG) Logger.debug( "." );
      return childProgress.next(  );
    }
  }

  /**
   * Sets current step of last child in chain to position
   * @param position new progress position
   * @param message  the message to show in progressView
   * @return new overall current progress
   */
  public double setCurrentStep( int position, String message ) {
    if (childProgress == null) {
      return setCurrent( start + (position * stepSize), message );
    }
    else {
      if (DEBUG) Logger.debug( "." );
      return childProgress.setCurrentStep( position, message );
    }
  }

  /**
   * Sets current step of last child in chain to position
   * @param position new progress position
   * @return new overall current progress
   */
  public double setCurrentStep( int position) {
    if (childProgress == null) {
      return setCurrent( start + (position * stepSize) );
    }
    else {
      if (DEBUG) Logger.debug( "." );
      return childProgress.setCurrentStep( position );
    }
  }

  /**
   * Create a child progress splitting one of our progress steps
   * in childsteps
   * @param childSteps number of steps for the child progress
   */
  public void createChild( int childSteps ) {
    if (childSteps > 0) {
      if (childProgress == null) {
        double childEnd = current + stepSize;
        if (childEnd > end) {
          if (DEBUG) Logger.debug( ErrorMessages.ERROR + " childEnd=" + childEnd + " > end=" + end );
          childEnd = end;
        }
        childProgress = new Progress( current, childEnd, childSteps, progressListener );
      }
      else {
        if (DEBUG) Logger.debug( "." );
        childProgress.createChild( childSteps );
      }
    }
  }

  /**
   * Create a child progress splitting from current to childEnd in childsteps.
   * To split e.g. remaining progress call createChild( steps, progress.getEnd() ).
   * @param childSteps number of steps for the child progress
   */
  public void createChild( int childSteps, double childEnd ) {
    if (childProgress == null) {
      if (childEnd > end) {
        if (DEBUG) Logger.debug( ErrorMessages.ERROR + " childEnd="+childEnd+" > end="+end );
        childEnd = end;
      }
      childProgress = new Progress( current, childEnd, childSteps, progressListener );
    }
    else {
      if (DEBUG) Logger.debug( "." );
      childProgress.createChild( childSteps );
    }
  }

  /**
   * Close last child in chain
   * @return true if a child was removed
   */
  public boolean closeChild( String message ) {
    if (childProgress == null) {
      return false;
    }
    if (DEBUG) Logger.debug( "." );
    if (!childProgress.closeChild( message )) {
      // if child did not remove a child we remove our child
      double newCurrent = childProgress.getEnd();
      childProgress = null;
      setCurrent( newCurrent, message );
      if (DEBUG) Logger.debug( "Close child" );
    }
    return true;
  }

  public boolean closeChild(  ) {
    if (childProgress == null) {
      return false;
    }
    if (DEBUG) Logger.debug( "." );
    if (!childProgress.closeChild(  )) {
      // if child did not remove a child we remove our child
      double newCurrent = childProgress.getEnd();
      childProgress = null;
      setCurrent( newCurrent );
      if (DEBUG) Logger.debug( "Close child" );
    }
    return true;
  }

  /**
   * sets the progress to finished
   * @param message shown in the progressbar
   */
  public void finished( String message ) {
    if (childProgress != null) {
      childProgress.finished( message );
      childProgress = null;
    }
    setCurrent( end, message );
  }

  /**
   * sets the progress to finished and
   * sets the progressbar color from blue to yellow or green
   * sets the error box to visible
   * Note: errorbox will only be visible if errorBoxMessage is not null or empty string
   *
   * @param progressMessage shown in the progressbar
   * @param finishedWithWarnings if true, the color turns yellow, else it turns green
   * @param errorBoxMessage shown in the errorbox below the progressbar
   */
  public void finished (String progressMessage, boolean finishedWithWarnings, String errorBoxMessage){
    finished(progressMessage);
    if(finishedWithWarnings){
      showWarning( errorBoxMessage );
    }
    else{
      showInfo( errorBoxMessage );
    }
  }

  public void setMessage( String msg ) {
    if (progressListener != null) {
      progressListener.updateProgress( msg, getCurrent() );
    }
  }

  public void showError( String errorMessage ) {
    if (progressListener != null && !Helper.isNullOrEmpty(errorMessage)) {
      finished( "" );
      progressListener.showError( errorMessage );
    }
  }

  public void showInfo( String infoMessage ) {
    if (progressListener != null && !Helper.isNullOrEmpty(infoMessage)) {
      progressListener.showInfo( infoMessage );
    }
  }

  public void showWarning ( String warningMessage ){
    if (progressListener != null && !Helper.isNullOrEmpty(warningMessage)) {
      progressListener.showWarning( warningMessage );
    }
  }

  public void hideError(  ) {
    if (progressListener != null) {
      progressListener.hideError();
    }
  }

  public void resetColor(){
    if (progressListener != null){
      progressListener.resetColor();
    }
  }

  public void reset() {
    this.current = 0;
    this.steps = 1;
    this.stepSize = 100;
    this.setMessage( "Please wait..." );
    this.resetColor();
    this.hideError();
    childProgress = null;
    if (progressListener != null) {
      progressListener.updateProgress( 0 );
    }
  }
}
