/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.progress;

import java.io.PrintStream;

/**
 * Created by pru on 10.08.2015.
 */
public class ProgressStream implements ProgressListener {

  private PrintStream progressStream;

  /**
   * Creates new progress view printing progress to System.out
   */
  public ProgressStream() {
    this.progressStream = System.out;
  }

  /**
   * Creates new progress view printing progress to given progressStream
   * @param progressStream the strem to use for progress messages
   */
  public ProgressStream( PrintStream progressStream ) {
    this.progressStream = progressStream;
  }

  /**
   * Called to set an optional max value. this allows prograss messages
   * like 8 of 25
   *
   * @param maxValue the max value, following calls to setProgress mus not
   *                 exceed this value
   */
  @Override
  public void setMaxValue( double maxValue ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Updates progress indicator with message and per cent value.
   *
   * @param newMessage the message to display or null
   * @param progress   progress in per cent or -1 for aborted with error
   */
  @Override
  public void updateProgress( String newMessage, double progress ) {
    progressStream.println( progress + " --- " + newMessage );
  }

  /**
   * Updates progress indicator with message and per cent value.
   *
   * @param progress progress in per cent or -1 for aborted with error
   */
  @Override
  public void updateProgress( double progress ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void showError( String errorMessage ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void hideError() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void showInfo( String infoMessage ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void showWarning( String warningMessage ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void resetColor ( ){
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }
}
