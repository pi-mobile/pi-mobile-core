/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.progress;

public interface ProgressListener {

  /**
   * Called to set an optional max value. this allows prograss messages
   * like 8 of 25
   * @param maxValue the max value, following calls to setProgress mus not
   *                 exceed this value
   */
  void setMaxValue( double maxValue );

  /**
   * Updates progress indicator with message and per cent value.
   * @param newMessage the message to display or null
   * @param progress   progress in per cent or -1 for aborted with error
   */
  void updateProgress( String newMessage, double progress );

  /**
   * Updates progress indicator with message and per cent value.
   * @param progress   progress in per cent or -1 for aborted with error
   */
  void updateProgress( double progress );

  void showError( String errorMessage );

  void hideError( );

  void showInfo( String infoMessage );

  void showWarning ( String warningMessage );

  void resetColor();
}

