/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.log;

import java.io.PrintStream;

/**
 * Calculates the time gap between two log entries and appends it to the log message.
 *
 * @author dsc
 */
public class DefaultDurationLogger extends DefaultLogger {

  private long lastLogTime = 0;
  private long lastDuration = 0;

  public DefaultDurationLogger( PrintStream logStream ) {
    super( logStream );
  }

  public DefaultDurationLogger( PrintStream logStream, Level logLevel ) {
    super( logStream, logLevel );
  }

  public DefaultDurationLogger() {
    super();
  }

  public DefaultDurationLogger( Level logLevel ) {
    super( logLevel );
  }

  /**
   * Sets the start time to current time.
   */
  public void reset() {
    lastLogTime = System.currentTimeMillis();
  }

  /**
   * Sets the start time to current time before logging the given message.
   *
   * @param message
   */
  public void start( String message ) {
    reset();
    log( getLogLevel(), message, null );
  }

  /**
   * Logs the message together with duration infomration when the {@link DefaultDurationLogger} is used standalone,
   * that is not added to the common logging mechanism.
   *
   * @param message
   */
  public void logDuration( String message ) {
    log( getLogLevel(), message, null );
  }

  @Override
  public void log( Level logLevel, String message, Throwable ex ) {
    long currentLogTime = System.currentTimeMillis();
    lastDuration = currentLogTime - lastLogTime;

    String timedMessage = String.format( "[%6d] %s", Long.valueOf( lastDuration ), message );
    super.log( getLogLevel(), timedMessage, ex );

    lastLogTime = currentLogTime;
  }

  public long getLastDuration() {
    return lastDuration;
  }
}
