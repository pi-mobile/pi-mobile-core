/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.log;

import java.util.ArrayList;
import java.util.List;

/**
 * Logger writing log to multiple loggers
 */
public class MultiLogger implements LoggerInterface {

  public List<LoggerInterface> loggerList = new ArrayList<LoggerInterface>();

  public MultiLogger() {
  }

  public MultiLogger( LoggerInterface defaultLogger ) {
    loggerList.add(defaultLogger);
  }

  public void addLogger(LoggerInterface newLogger) {
    if (!loggerList.contains( newLogger )){
      loggerList.add(newLogger);
    }
  }

  public void removeLogger( LoggerInterface loggerToRemove ) {
    loggerList.remove( loggerToRemove );
  }

  @Override
  public void log( Level logLevel, String message, Throwable ex ) {
    for (LoggerInterface logger : loggerList) {
      logger.log( logLevel, message, ex);
    }
  }

  @Override
  public void close() {
    for (LoggerInterface logger : loggerList) {
      logger.close();
    }
  }

  @Override
  public void setLogLevel( Level loglevel ) {
    for (LoggerInterface logger : loggerList) {
      logger.setLogLevel( loglevel );
    }
  }

  @Override
  public Level getLogLevel() {
    Level maxLoglevel = Level.OFF;
    for (LoggerInterface logger : loggerList) {
      Level loglevel = logger.getLogLevel();
      if (loglevel.getLevelValue() < maxLoglevel.getLevelValue()) {
        maxLoglevel = loglevel;
      }
    }
    return maxLoglevel;
  }
}
