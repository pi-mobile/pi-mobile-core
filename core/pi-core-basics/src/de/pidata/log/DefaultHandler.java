/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.log;

import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * A handler which forwards all log requests from java.util.logging to the PI-Mobile {@link Logger}.
 */
public class DefaultHandler extends Handler {

  /**
   * Publish a <tt>LogRecord</tt>.
   * <p>
   * The logging request was made initially to a <tt>Logger</tt> object,
   * which initialized the <tt>LogRecord</tt> and forwarded it here.
   * <p>
   * The <tt>Handler</tt>  is responsible for formatting the message, when and
   * if necessary.  The formatting should include localization.
   *
   * @param record description of the log event. A null record is
   *               silently ignored and is not published
   */
  @Override
  public void publish( LogRecord record ) {
    Level logLevel = Level.fromValue( record.getLevel().intValue() );
    de.pidata.log.Logger.log( logLevel, record.getMessage(), record.getThrown() );
  }

  /**
   * Flush any buffered output.
   */
  @Override
  public void flush() {
    // leave it to de.pidata.Logger registered loggers to flush
  }

  /**
   * Close the <tt>Handler</tt> and free all associated resources.
   * <p>
   * The close method will perform a <tt>flush</tt> and then close the
   * <tt>Handler</tt>.   After close has been called this <tt>Handler</tt>
   * should no longer be used.  Method calls may either be silently
   * ignored or may throw runtime exceptions.
   *
   * @throws SecurityException if a security manager exists and if
   *                           the caller does not have <tt>LoggingPermission("control")</tt>.
   */
  @Override
  public void close() throws SecurityException {
    // leave it to de.pidata.Logger to close registered loggers
  }
}
