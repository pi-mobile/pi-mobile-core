/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.log;

import java.util.logging.Level;

/**
 * {@link java.util.logging.Handler} implementation which creates and manages a {@link DefaultLogger}.
 */
public class ConsoleHandler extends BaseHandler {

  public ConsoleHandler() {
    super();
    configure();

    // TODO: might add console logging twice
    logger = new DefaultLogger( logLevel );
    Logger.addLogger( logger );
  }


  @Override
  public void configure() {
    String cname = getClass().getName();

    Level levelLogProperty = getLevelLogProperty( cname + ".level", Level.ALL );
    logLevel = de.pidata.log.Level.fromValue( levelLogProperty.intValue() );
    setLevel( levelLogProperty );
    setFormatter( new DefaultFormatter() );
  }
}
