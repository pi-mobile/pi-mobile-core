/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.log;

import de.pidata.date.DateHelper;

import java.io.PrintStream;

/**
 * Logger writing log to a PrintStream.
 * <p>
 * Please use {@link DefaultLogger} for writing log to System.out!
 */
public class StreamLogger implements LoggerInterface {

  private Level logLevel = Level.DEBUG;
  public PrintStream logStream;

  public StreamLogger( PrintStream logStream ) {
    this( logStream, Level.DEBUG );
  }

  public StreamLogger( PrintStream logStream, Level logLevel ) {
    if (logStream == System.out) {
      System.out.append( "WARNING: Please use DefaultLogger instead of StreamLogger to have System.out as logging target!" );
    }
    this.logLevel = logLevel;
    this.logStream = logStream;
  }

  @Override
  public void close() {
    if (logStream != null) {
      logStream.flush();
      logStream.close();
      logStream = null;
    }
  }

  @Override
  public void log( Level logLevel, String message, Throwable ex ) {
    String level = logLevel.getLevelName();
    try {
      if (logLevel.getLevelValue() >= getLogLevel().getLevelValue()) {
        if (logStream == null) {
          System.out.append( "LOGGER ERROR: no logStream opened" );
          DefaultLogger.printToLog( System.out, logLevel, message, ex );
        }
        else {
          DefaultLogger.printToLog( logStream, logLevel, message, ex );
        }
      }
    }
    catch (Exception e) {
      // Print out the exception anyway
      System.out.println( "[" + level + "] " + DateHelper.timestamp() + " - " + message );
    }
  }

  public void setLogLevel( Level loglevel ) {
    this.logLevel = loglevel;
  }

  public Level getLogLevel() {
    return logLevel;
  }

}
