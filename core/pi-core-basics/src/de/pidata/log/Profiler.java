package de.pidata.log;

import java.util.HashMap;
import java.util.Map;

/**
 * Can be used to do some simple count of e.g. method calls.
 */
public class Profiler {

  private static boolean doProfile = false;
  private static boolean doLogDefault;

  private static Map<String, Integer> managedCounters = new HashMap<String, Integer>();

  public static void startProfiling( boolean defaultLog ) {
    Profiler.doLogDefault = defaultLog;
    doProfile = true;
  }

  public static void stopProfiling() {
    doProfile = false;
  }

  public static synchronized void countWithLog( String counterName ) {
    count( counterName, true );
  }

  public static synchronized void count( String counterName ) {
    count( counterName, doLogDefault );
  }

  public static synchronized void count( String counterName, boolean doLog ) {
    if (doProfile) {
      Integer countValue = managedCounters.get( counterName );
      if (countValue == null) {
        managedCounters.put( counterName, Integer.valueOf( 1 ) );
      }
      else {
        managedCounters.put( counterName, Integer.valueOf( countValue.intValue() + 1 ) );
      }
      if (doLog) {
        Logger.debug( counterName );
      }
    }
  }

  public static synchronized void logCountersAndReset() {
    logCounters( true );
  }

  public static synchronized void logCountersAndContinue() {
    logCounters( false );
  }

  public static synchronized void logCounters( boolean reset ) {
    if (doProfile) {
      Logger.debug( "\n\nCOUNTER snapshot:" );
      if (managedCounters.isEmpty()) {
        Logger.debug( "nothing was counted" );
      }
      else {
        for (Map.Entry<String, Integer> counterEntry : managedCounters.entrySet()) {
          Logger.debug( "number of " + counterEntry.getKey() + "\t" + counterEntry.getValue() );
        }
        if (reset) {
          resetCounters();
        }
      }
      Logger.debug( "\nCOUNTER snapshot finished\n\n" );
    }
  }

  public static synchronized void resetCounters() {
    managedCounters = new HashMap<String, Integer>();
  }
}
