/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.log;

/**
 * Created by dsc on 12.07.17.
 */
public interface LoggerInterface {

  /**
   * Do logging
   * @param logLevel the logging level, one of the constants LOG_*
   * @param message  the message to log
   * @param cause    the Throwable which led to this log entry
   */
  void log( Level logLevel, String message, Throwable cause );

  void setLogLevel( Level loglevel);
  Level getLogLevel();

  void close();
}
