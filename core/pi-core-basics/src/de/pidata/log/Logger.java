/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.log;

/**
 * Logger allowing easy switching of logging implementation.
 *
 * All static logging methods invoke method log on logging instance placed in field "logger".
 *
 * Initial logger is DefaultLogger writing to System.out
 */
public final class Logger {

  public static final String KEY_LOGLEVEL = "loglevel";
  public static final String KEY_ATTACH_JAVA_LOGGING = "attach.java.logging";

  private static LoggerInterface logger = new DefaultLogger( Level.DEBUG );
  private static LoggerInterface previousLogger = null;

  public static void setLogLevel( Level logLevel ) {
    logger.setLogLevel( logLevel );
  }

  /**
   * Sets the global logging mechanism. If you additionally want to reuse the previously used logger
   * use {@link #addLogger(LoggerInterface)}.
   * <p>
   * Keeps the last used logger for later use of {@link #reset()}.
   *
   * @param newLogger the new logger to use; if null: creates a new {@link DefaultLogger}
   */
  public static synchronized void setLogger( LoggerInterface newLogger ) {
    if (logger != null) {
      logger.close();
      // hold for eventually reset
      previousLogger = logger;
    }
    if (newLogger == null) {
      logger = new DefaultLogger( Level.DEBUG );
      previousLogger.close();
      previousLogger = null;
    }
    else {
      logger = newLogger;
    }
  }

  /**
   * Sets the global logging mechanism to the last known used logger prior to using {@link #setLogger(LoggerInterface)}.
   * If no prior logger is known, creates creates a new {@link DefaultLogger}.
   * @see {@link #setLogger(LoggerInterface)}
   */
  public static synchronized void reset() {
    setLogger( previousLogger );
  }

  /**
   * Adds a logger implementing {@link LoggerInterface} to the managed loggers. If not yet present,
   * creates and adds a {@link MultiLogger} as global logging mechanism to
   * hold multiple loggers.
   *
   * @param newLogger
   */
  public static synchronized void addLogger( LoggerInterface newLogger ) {
    MultiLogger multiLogger;
    if (logger != null && logger instanceof MultiLogger) {
      multiLogger = (MultiLogger)logger;
    }
    else {
      LoggerInterface oldLogger = logger;
      if (oldLogger == null) {
        oldLogger = new DefaultLogger();
      }
      multiLogger = new MultiLogger( oldLogger );
      logger = multiLogger;
    }
    multiLogger.addLogger( newLogger );
  }

  /**
   * Removes the logger from the managed loggers.
   *
   * @param loggerToRemove
   */
  public static synchronized void removeLogger( LoggerInterface loggerToRemove ) {
    if (logger != null && logger instanceof MultiLogger) {
      MultiLogger multiLogger = (MultiLogger) logger;
      multiLogger.removeLogger( loggerToRemove );
    }
  }

  /**
   * Returns the currently active {@link Logger} instance.
   * <p>
   * CAUTION: should not be used directly; for compatibility with former logging only!
   *
   * @return the currently active {@link Logger} instance
   *
   * @deprecated should not be used directly - please use Logger.addLogger if you just want to add a further Logger!
   */
  @Deprecated
  public static LoggerInterface getLogger() {
    return logger;
  }

  /**
   * Returns the global logging level.
   *
   * @return
   */
  public static Level getLogLevel() {
    return logger.getLogLevel();
  }

  /**
   * Base method which forwards the log request to all registered loggers.
   *
   * @param loglevel
   * @param message
   */
  static void log( Level loglevel, String message ) {
    log( loglevel, message, null );
  }

  /**
   * Base method which forwards the log request to all registered loggers.
   *
   * @param logLevel
   * @param message
   * @param ex
   */
  static void log( Level logLevel, String message, Throwable ex) {
    logger.log( logLevel, message, ex );
    if (ex != null) {
      Throwable cause = ex.getCause();
      while (cause != null) {
        logger.log( logLevel, "Caused by: " + cause.getMessage(), cause );
        cause = cause.getCause();
      }
    }
  }

  public static void debug( String message ) {
    log( Level.DEBUG, message, null );
  }

  public static void info( String message ) {
    log( Level.INFO, message, null );
  }

  public static void warn( String message ) {
    log( Level.WARN, message, null );
  }

  public static void error( String message ) {
    error( message, null );
  }

  public static void error( String message, Throwable ex) {
    log( Level.ERROR, message, ex );
  }

  public static void fatal( String message, Throwable ex) {
    log( Level.FATAL, message, ex );
  }
}
