/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.log;

import de.pidata.date.DateHelper;

import java.io.PrintStream;

/**
 * Logger writing log to System.out.
 */
public class DefaultLogger implements LoggerInterface {

  private Level logLevel = Level.DEBUG;
  private PrintStream logStream;

  public DefaultLogger( PrintStream logStream ) {
    this.logStream = logStream;
  }

  public DefaultLogger( PrintStream logStream, Level loglevel ) {
    this( logStream );
    this.logLevel = loglevel;
  }

  public DefaultLogger() {
    this( System.out );
  }

  public DefaultLogger( Level loglevel ) {
    this( System.out, loglevel );
  }

  @Override
  public void close() {
    // never close System.out
  }

  @Override
  public void log( Level logLevel, String message, Throwable ex ) {
    if (logLevel.getLevelValue() >= getLogLevel().getLevelValue()) {
      printToLog( logStream, logLevel, message, ex );
    }
  }

  public static void printToLog( PrintStream logStream, Level logLevel, String message, Throwable ex ) {
    String levelName = logLevel.getLevelName();
    try {
      //--- Create message in a StringBuilder to avoid intersection of messages form parallel threads
      StringBuilder builder = new StringBuilder( DateHelper.timestamp() );

      builder.append( levelName );
      if (levelName.length() == 4) {
        builder.append( " " );
      }
      builder.append( " - [" );

      builder.append( Thread.currentThread().getName() );
      builder.append( "]  " );
      builder.append( message );

      logStream.println( builder.toString() );
      if (ex != null) {
        ex.printStackTrace( logStream );
      }
      logStream.flush();
    }
    catch (Exception e) {
      // Print out the exception anyway
      System.out.println( "[" + levelName + "] " + DateHelper.timestamp() + " - " + message );
      if (ex != null) {
        ex.printStackTrace( System.out );
      }
    }
  }

  public void setLogLevel( Level loglevel ) {
    this.logLevel = loglevel;
  }

  public Level getLogLevel() {
    return logLevel;
  }
}
