/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.log;

import de.pidata.date.DateHelper;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * {@link Formatter} implementation which outputs the log request similar to the PI-Mobile default.
 */
public class DefaultFormatter extends Formatter {

  /**
   * Format the given log record and return the formatted string.
   * <p>
   * The resulting formatted String will normally include a
   * localized and formatted version of the LogRecord's message field.
   * It is recommended to use the {@link Formatter#formatMessage}
   * convenience method to localize and format the message field.
   *
   * @param record the log record to be formatted.
   * @return the formatted log record
   */
  @Override
  public String format( LogRecord record ) {
    StringBuilder sb = new StringBuilder();
    String level = record.getLevel().getName();
    try {
      sb.append( DateHelper.timestamp() );

      sb.append( level );
      // fill to same length TODO
      if (level.length() == 4) sb.append( " " );
      sb.append( " - [" );

      sb.append( Thread.currentThread().getName() );
      sb.append( "]  " );
      sb.append( record.getMessage() );
      sb.append( System.lineSeparator() );

      Throwable thrown = record.getThrown();
      if (thrown != null) {
        sb.append( thrown );

        for (StackTraceElement stackTraceElement : thrown.getStackTrace()) {
          sb.append( System.lineSeparator() + "at " + stackTraceElement );
        }

//        // Print suppressed exceptions, if any
//        for (Throwable se : thrown.getSuppressed())
//          se.printEnclosedStackTrace(s, trace, SUPPRESSED_CAPTION, "\t", dejaVu);

        // Print cause, if any
//        Throwable thrownCause = thrown.getCause();
//        if (thrownCause != null)
//          thrownCause.printEnclosedStackTrace(s, trace, CAUSE_CAPTION, "", dejaVu);
      }
    }
    catch (Exception e) {
      // Print out the exception anyway
      System.out.println( "[" + level + "] " + DateHelper.timestamp() + " - " + record.getMessage() );
      Throwable thrown = record.getThrown();
      if (thrown != null) {
        thrown.printStackTrace( System.out );
      }
    }
   return sb.toString();
  }
}
