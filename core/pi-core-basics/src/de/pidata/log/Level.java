/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.log;

import de.pidata.string.Helper;

public enum Level {
  OFF( "OFF", Integer.MAX_VALUE ), // java.util.logging.Level.OFF.intValue();
  FATAL( "FATAL", 1100 ), // java.util.logging.Level.SEVERE.intValue() + 100;
  ERROR( "ERROR", 1000 ), // java.util.logging.Level.SEVERE.intValue();
  WARN( "WARN", 900 ), // java.util.logging.Level.WARNING.intValue();
  INFO( "INFO", 800 ), // java.util.logging.Level.INFO.intValue();
  DEBUG( "DEBUG", 700 ), // java.util.logging.Level.INFO.intValue() - 100;
  ALL( "ALL", Integer.MIN_VALUE ); // java.util.logging.Level.ALL.intValue();

  private final String levelName;
  private final int levelValue;

  Level( String levelName, int levelValue ) {
    this.levelName = levelName;
    this.levelValue = levelValue;
  }

  public String getLevelName() {
    return levelName;
  }

  public int getLevelValue() {
    return levelValue;
  }

  /**
   * Default: INFO
   *
   * @param levelName
   * @return
   */
  public static Level fromName( String levelName ) {
    if (!Helper.isNullOrEmpty( levelName )) {
      levelName = levelName.toUpperCase();
      if (levelName.equals( ALL.levelName )) {
        return ALL;
      }
      if (levelName.equals( DEBUG.levelName )) {
        return DEBUG;
      }
      if (levelName.equals( INFO.levelName )) {
        return INFO;
      }
      if (levelName.equals( WARN.levelName )) {
        return WARN;
      }
      if (levelName.equals( ERROR.levelName )) {
        return ERROR;
      }
      if (levelName.equals( FATAL.levelName )) {
        return FATAL;
      }
      if (levelName.equals( OFF.levelName )) {
        return OFF;
      }
    }
    return INFO;
  }

  public static Level fromValue( int levelValue ) {
    if (levelValue <= ALL.getLevelValue()) {
      return ALL;
    }
    if (levelValue <= DEBUG.getLevelValue()) {
      return DEBUG;
    }
    if (levelValue <= INFO.getLevelValue()) {
      return INFO;
    }
    if (levelValue <= WARN.getLevelValue()) {
      return WARN;
    }
    if (levelValue <= ERROR.getLevelValue()) {
      return ERROR;
    }
    if (levelValue <= FATAL.getLevelValue()) {
      return FATAL;
    }
    return ALL;
  }
}
