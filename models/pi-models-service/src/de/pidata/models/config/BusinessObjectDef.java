// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.models.config;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.lang.String;
import java.util.Hashtable;

public class BusinessObjectDef extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://schema.pidata.de/config" );

  public static final QName ID_PORTTYPE = NAMESPACE.getQName("portType");
  public static final QName ID_CLASSNAME = NAMESPACE.getQName("className");

  public BusinessObjectDef() {
    super( null, ConfigFactory.BUSINESSOBJECTDEF_TYPE, null, null, null );
  }

  public BusinessObjectDef(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, ConfigFactory.BUSINESSOBJECTDEF_TYPE, attributeNames, anyAttribs, childNames);
  }

  protected BusinessObjectDef(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the attribute portType.
   *
   * @return The attribute portType
   */
  public QName getPortType() {
    return (QName) get( ID_PORTTYPE );
  }

  /**
   * Set the attribute portType.
   *
   * @param portType new value for attribute portType
   */
  public void setPortType( QName portType ) {
    set( ID_PORTTYPE, portType );
  }

  /**
   * Returns the attribute className.
   *
   * @return The attribute className
   */
  public String getClassName() {
    return (String) get( ID_CLASSNAME );
  }

  /**
   * Set the attribute className.
   *
   * @param className new value for attribute className
   */
  public void setClassName( String className ) {
    set( ID_CLASSNAME, className );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
