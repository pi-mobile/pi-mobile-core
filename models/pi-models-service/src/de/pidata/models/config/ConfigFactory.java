// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.models.config;

import de.pidata.models.tree.*;
import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.Model;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.*;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.util.Hashtable;

public class ConfigFactory extends de.pidata.models.tree.AbstractModelFactory {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://schema.pidata.de/config" );

  public static final QName ID_BLUETOOTHDEVICE = NAMESPACE.getQName("bluetoothDevice");
  public static final QName ID_DEVICETABLE = NAMESPACE.getQName("deviceTable");
  public static final QName ID_BINDING = NAMESPACE.getQName("binding");
  public static final QName ID_CONFIG = NAMESPACE.getQName("config");
  public static final QName ID_PARAMETER = NAMESPACE.getQName("parameter");
  public static final QName ID_BUSINESSOBJECTDEF = NAMESPACE.getQName("businessObjectDef");
  public static final QName ID_INSTANCE = NAMESPACE.getQName("instance");

  public static final ComplexType PARAMETER_TYPE = new DefaultComplexType( NAMESPACE.getQName( "parameter" ), Parameter.class.getName(), 1 );
  public static final ComplexType INSTANCE_TYPE = new DefaultComplexType( NAMESPACE.getQName( "instance" ), Instance.class.getName(), 0 );
  public static final ComplexType BINDING_TYPE = new DefaultComplexType( NAMESPACE.getQName( "binding" ), Binding.class.getName(), 0 );
  public static final ComplexType BUSINESSOBJECTDEF_TYPE = new DefaultComplexType( NAMESPACE.getQName( "businessObjectDef" ), BusinessObjectDef.class.getName(), 0 );
  public static final ComplexType CONFIG_TYPE = new DefaultComplexType( NAMESPACE.getQName( "config" ), Config.class.getName(), 0 );
  public static final ComplexType BLUETOOTHDEVICE_TYPE = new DefaultComplexType( NAMESPACE.getQName( "bluetoothDevice" ), BluetoothDevice.class.getName(), 1 );
  public static final ComplexType DEVICETABLE_TYPE = new DefaultComplexType( NAMESPACE.getQName( "deviceTable" ), DeviceTable.class.getName(), 0 );
  static {
    DefaultComplexType type;
    type = (DefaultComplexType) PARAMETER_TYPE;
    type.addKeyAttributeType( 0, Parameter.ID_NAME, QNameType.getInstance() );
    type.addAttributeType( Parameter.ID_VALUE, StringType.getDefString() );

    type = (DefaultComplexType) INSTANCE_TYPE;
    type.addAttributeType( Instance.ID_CLASSNAME, StringType.getDefString() );
    type.addAttributeType( Instance.ID_NAME, QNameType.getInstance() );
    type.addRelation( Instance.ID_PARAMETER, PARAMETER_TYPE, 0, Integer.MAX_VALUE);

    type = (DefaultComplexType) BINDING_TYPE;
    type.addAttributeType( Binding.ID_SERVICENAME, StringType.getDefString() );
    type.addAttributeType( Binding.ID_PORTTYPE, QNameType.getInstance() );
    type.addAttributeType( Binding.ID_CLASSNAME, StringType.getDefString() );
    type.addAttributeType( Binding.ID_CLIENTSERVICE, BooleanType.getDefault() );
    type.addRelation( Binding.ID_PARAMETER, PARAMETER_TYPE, 0, Integer.MAX_VALUE);

    type = (DefaultComplexType) BUSINESSOBJECTDEF_TYPE;
    type.addAttributeType( BusinessObjectDef.ID_PORTTYPE, QNameType.getInstance() );
    type.addAttributeType( BusinessObjectDef.ID_CLASSNAME, StringType.getDefString() );

    type = (DefaultComplexType) CONFIG_TYPE;
    type.addRelation( Config.ID_INSTANCE, INSTANCE_TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( Config.ID_BUSINESSOBJECTDEF, BUSINESSOBJECTDEF_TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( Config.ID_BINDING, BINDING_TYPE, 0, Integer.MAX_VALUE);

    type = (DefaultComplexType) BLUETOOTHDEVICE_TYPE;
    type.addKeyAttributeType( 0, BluetoothDevice.ID_ID, QNameType.getInstance() );
    type.addAttributeType( BluetoothDevice.ID_NAME, StringType.getDefString() );
    type.addAttributeType( BluetoothDevice.ID_ALIAS, StringType.getDefString() );

    type = (DefaultComplexType) DEVICETABLE_TYPE;
    type.addRelation( DeviceTable.ID_BLUETOOTHDEVICE, BLUETOOTHDEVICE_TYPE, 0, Integer.MAX_VALUE);

  }
  public ConfigFactory() {
    super( NAMESPACE, "http://schema.pidata.de/config", "3.0" );
    addType( PARAMETER_TYPE );
    addType( INSTANCE_TYPE );
    addType( BINDING_TYPE );
    addType( BUSINESSOBJECTDEF_TYPE );
    addType( CONFIG_TYPE );
    addType( BLUETOOTHDEVICE_TYPE );
    addType( DEVICETABLE_TYPE );
    addRootRelation( ID_PARAMETER, PARAMETER_TYPE, 1, 1);
    addRootRelation( ID_INSTANCE, INSTANCE_TYPE, 1, 1);
    addRootRelation( ID_BINDING, BINDING_TYPE, 1, 1);
    addRootRelation( ID_BUSINESSOBJECTDEF, BUSINESSOBJECTDEF_TYPE, 1, 1);
    addRootRelation( ID_CONFIG, CONFIG_TYPE, 1, 1);
    addRootRelation( ID_BLUETOOTHDEVICE, BLUETOOTHDEVICE_TYPE, 1, 1);
    addRootRelation( ID_DEVICETABLE, DEVICETABLE_TYPE, 1, 1);
  }

  public Model createInstance(Key key, Type typeDef, Object[] attributes, Hashtable anyAttribs, ChildList children) {
    Class modelClass = typeDef.getValueClass();
    if (modelClass == Parameter.class) {
      return new Parameter(key, attributes, anyAttribs, children);
    }
    if (modelClass == Instance.class) {
      return new Instance(key, attributes, anyAttribs, children);
    }
    if (modelClass == Binding.class) {
      return new Binding(key, attributes, anyAttribs, children);
    }
    if (modelClass == BusinessObjectDef.class) {
      return new BusinessObjectDef(key, attributes, anyAttribs, children);
    }
    if (modelClass == Config.class) {
      return new Config(key, attributes, anyAttribs, children);
    }
    if (modelClass == BluetoothDevice.class) {
      return new BluetoothDevice(key, attributes, anyAttribs, children);
    }
    if (modelClass == DeviceTable.class) {
      return new DeviceTable(key, attributes, anyAttribs, children);
    }
    return super.createInstance(key, typeDef, attributes, anyAttribs, children);
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
