// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.models.config;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.DefaultModel;
import de.pidata.models.tree.ModelCollection;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.util.Collection;
import java.util.Hashtable;

public class DeviceTable extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://schema.pidata.de/config" );

  public static final QName ID_BLUETOOTHDEVICE = NAMESPACE.getQName("bluetoothDevice");

  private Collection<BluetoothDevice> bluetoothDevices = new ModelCollection<BluetoothDevice>( ID_BLUETOOTHDEVICE, children );

  public DeviceTable() {
    super( null, ConfigFactory.DEVICETABLE_TYPE, null, null, null );
  }

  public DeviceTable(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, ConfigFactory.DEVICETABLE_TYPE, attributeNames, anyAttribs, childNames);
  }

  protected DeviceTable(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  public BluetoothDevice getBluetoothDevice( Key bluetoothDeviceID ) {
    return (BluetoothDevice) get( ID_BLUETOOTHDEVICE, bluetoothDeviceID );
  }

  public void addBluetoothDevice( BluetoothDevice bluetoothDevice ) {
    add( ID_BLUETOOTHDEVICE, bluetoothDevice );
  }

  public void removeBluetoothDevice( BluetoothDevice bluetoothDevice ) {
    remove( ID_BLUETOOTHDEVICE, bluetoothDevice );
  }

  public ModelIterator<BluetoothDevice> bluetoothDeviceIter() {
    return iterator( ID_BLUETOOTHDEVICE, null );
  }

  public int bluetoothDeviceCount() {
    return childCount( ID_BLUETOOTHDEVICE );
  }

  /**
   * Returns the collection bluetoothDevice.
   *
   * @return The collection bluetoothDevice
   */
  public Collection<BluetoothDevice> getBluetoothDevices() {
    return bluetoothDevices;
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
