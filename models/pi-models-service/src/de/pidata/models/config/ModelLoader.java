/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.config;

import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.system.base.Storage;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.SystemManager;

import java.io.IOException;
import java.io.InputStream;

public class ModelLoader implements Configurable {

  public static final QName paramStorage = ConfigFactory.NAMESPACE.getQName( "storage" );
  public static final QName paramFile = ConfigFactory.NAMESPACE.getQName( "file" );

  private Model model;

  public void init( Configurator configurator, Instance instanceDef ) throws IOException {
    String storageName = null;
    Parameter storageParam = instanceDef.getParameter( paramStorage );
    if (storageParam != null) {
      storageName = storageParam.getValue();
    }
    String fileName = instanceDef.getParameter( paramFile ).getValue();
    Storage storage = SystemManager.getInstance().getStorage( storageName );
    InputStream in = null;
    try {
      in = storage.read( fileName );
      if (in == null) {
        throw new IOException( "resource not found: "+fileName + ", storage="+storageName );
      }
      XmlReader reader = new XmlReader();
      this.model = reader.loadData( in, null );
    }
    finally {
      StreamHelper.close( in );
    }
  }

  public Model getModel() {
    return model;
  }
}
