/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.config;

import de.pidata.log.Logger;
import de.pidata.models.service.PortType;
import de.pidata.models.service.Service;
import de.pidata.models.service.ServiceManager;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.models.tree.ModelIterator;
import de.pidata.qnames.QName;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.system.base.SystemManager;

import java.io.InputStream;
import java.util.Hashtable;
import java.util.Vector;

public class Configurator {

  public static final String CONFIG_FILENAME = "/config.xml";

  private Config config;
  private Context context;
  private Vector wsdlDefinitions;
  private Vector businessObjects = new Vector();
  private static Vector clientServices = new Vector();
  private static Vector clientServicesDef = new Vector();

  /** Table containing portType definitions, key is portType name */
  private Hashtable portTypeTable = new Hashtable();

//  private SyncListener syncListener;

  private Hashtable instanceTable = new Hashtable();

  public Configurator( Config config, Context context ) throws Exception {
    this.config = config;
    init( context );
  }

  public Context getContext() {
    return context;
  }

  public Object getInstance( QName name ) {
    return instanceTable.get( name );
  }

  public void addInstance( QName name, Object instance ) {
    if (instanceTable.get( name ) != null) {
      throw new IllegalArgumentException( "Replacing instance is not allowed, name="+name );
    }
    this.instanceTable.put( name , instance );
  }

  private void init( Context context ) {
    this.context = context;
    ServiceManager serviceMgr = ServiceManager.getInstance();

    SystemManager sysMan = SystemManager.getInstance();
    Logger.info( "\n" );
    Logger.info( "load services for " + sysMan.getProgramName() + " " + sysMan.getProgramVersion() );
    Logger.info( "\n" );

    //----- Process all instance definitions, e.g. factories, WSDL, ...
    for (ModelIterator instanceIter = config.instanceIter(); instanceIter.hasNext(); ) {
      Instance instance = (Instance) instanceIter.next();
      processInstance( instance );
    }

    //String className = config.getSyncListenerClass();
    //if ((className != null) && (className.length() > 0)) {
    //  this.syncListener = (SyncListener) Class.forName(className).newInstance();
    //}

    //----- Load WSDL definitions listed in server config's service description
    for (ModelIterator bindingIter = config.bindingIter(); bindingIter.hasNext(); ) {
      Binding binding = (Binding) bindingIter.next();
      try {
        if (binding.isClientService()) {
          clientServices.addElement( binding );
        }
        else {
          createService( serviceMgr, binding );
        }
      }
      catch (Exception ex) {
        String msg = "Could not bind service name=" + binding.getServiceName() + ", class="+binding.getClassName();
        Logger.error( msg, ex );
        throw new IllegalArgumentException( msg );   // must not use ex as second argument due to MIDP compatibility
      }
    }
/*      //----- create dialog services
      //DialogService dlgService = null;
      DialogServiceType dlgServiceType;
      if (serviceDef.dialogServiceCount() > 0) {
        serviceLoader = (ServiceLoader) Class.forName("de.pidata.gui.guidef.GuiServiceLoader").newInstance();
        for (ModelIterator dlgServiceIter = serviceDef.dialogServiceIter(); dlgServiceIter.hasNext(); ) {
          dlgServiceType = (DialogServiceType) dlgServiceIter.next();
          dlgService = (DialogService) serviceLoader.createPort(serviceMgr, dlgServiceType.getPortType(), Class.forName("de.pidata.gui.guidef.DialogService"));
          dlgService.setBuilder(this, dlgServiceType, dataRoot);
        }
      }

      //----- load service classes
      if (serviceDef.serviceClassCount() > 0) {
        if (wsdlPath != null && wsdlPath.length() > 0) {
          serviceLoader = (ServiceLoader) Class.forName("de.pidata.wsdl.WsdlServiceLoader").newInstance();
          serviceLoader.init(wsdlPath);
        }
        else {
          serviceLoader = (ServiceLoader) Class.forName("de.pidata.gui.guidef.GuiServiceLoader").newInstance();
        }
        for (ModelIterator classIter = serviceDef.serviceClassIter(); classIter.hasNext(); ) {
          Binding classType = (Binding) classIter.next();
          Class serviceClass = Class.forName(classType.getClassname());
          Service service = serviceLoader.createPort(serviceMgr, classType.getPortType(), serviceClass);
          if (service instanceof GuiService) {
            GuiService gService = (GuiService)service;
            gService.init(this);
          }
        }
      }

      //----- load BPEL definitions
      ProcessType processType;
      if (serviceDef.processCount() > 0) {
        serviceLoader = (ServiceLoader) Class.forName("de.pidata.process.ProcessServiceLoader").newInstance();
        for (ModelIterator processIter = serviceDef.processIter(); processIter.hasNext(); ) {
          processType = (ProcessType) processIter.next();
          serviceLoader.init(processType.getBpel());
          serviceLoader.createPort(serviceMgr, processType.getTaLogging());
        }
      }
    } */

    serviceMgr.setActive(true);
    Logger.debug("...services loaded");
  
  }

  private Service createService( ServiceManager serviceMgr, Binding binding ) throws Exception {
    Class serviceClass = Class.forName( binding.getClassName() );
    Service service = (Service) serviceClass.newInstance();
    service.init( serviceMgr, this, binding );
    serviceMgr.registerService( binding.getServiceName(), service );
    return service;
  }

  private void processInstance( Instance instance ) {
    try {
      Logger.info("config: process class [" +instance.getClassName() + "] add as [" + instance.getName() + "]");
      Class instanceClass = Class.forName( instance.getClassName() );
      Object object = instanceClass.newInstance();
      if (object instanceof Configurable) {
        ((Configurable) object).init( this, instance );
      }
      QName name = instance.getName();
      if (name != null) {
        addInstance( name, object );
      }
    }
    catch (Exception ex) {
      String msg = "Could not create instance className="+instance.getClassName();
      Logger.error( msg, ex );
      throw new IllegalArgumentException( msg );  // must not use ex as second argument due to MIDP compatibility
    }
  }


  public static Configurator loadConfig( InputStream configStream, Context context ) throws Exception {
    XmlReader reader;
    Config config;
                                 
    // initialize data root
    ModelFactoryTable.getInstance().getOrSetFactory( ConfigFactory.NAMESPACE, ConfigFactory.class );
//    LogFactory.NAMESPACE.getFactory().addRelationsToRoot(root);

//    WSDLFactory.NAMESPACE.getFactory().addRelationsToRoot(root);
//    PartnerLinkFactory.NAMESPACE.getFactory().addRelationsToRoot(root);
//    de.pidata.bpel.Factory.NAMESPACE.getFactory().addRelationsToRoot(root);

    // loadWSDL config
    reader = new XmlReader();
    if (configStream == null) {
      configStream = Configurator.class.getResourceAsStream( CONFIG_FILENAME );
    }
    if (configStream == null) {
      throw new IllegalArgumentException( "Config file missing, name="+CONFIG_FILENAME );
    }

    //InputStream in = new FileInputStream( INF_DIR + File.separator + CONFIG_FILENAME );
    config = (Config) reader.loadData( configStream, null );
    configStream.close();

    Configurator configurator = new Configurator( config, context );
    return configurator;
  }

  public Vector getBusinessObjects() {
    return this.businessObjects;
  }

  /**
   * For configuration objects, e.g. WSDL: Add PortType to this configuration
   * This allows services to get their PortType definition via getPortType()
   * @param portType
   */
  public void addPortType( PortType portType ) {
    this.portTypeTable.put( portType.getName(), portType );
  }

  public PortType getPortType( QName portTypeName ) {
    return (PortType) this.portTypeTable.get( portTypeName );
  }

  public Service createClientService( ServiceManager serviceMgr, String serviceName ) {
    for (int i = 0; i < clientServices.size(); i++) {
      Binding binding = (Binding) clientServices.elementAt(i);
      if (binding.getPortType().getName().equals( serviceName ) ) {
        try {
          return createService( serviceMgr, binding );
        }
        catch(Exception ex) {
          Logger.error("Could not create client service, portTypeName="+serviceName, ex);
        }
      }
    }
    return null;
  }
}
