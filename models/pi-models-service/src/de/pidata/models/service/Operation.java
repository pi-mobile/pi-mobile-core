/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.service;

import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.service.base.ServiceException;

public interface Operation {

  public QName getName();

  /**
   * Returns a container which holds the message parts.
   * By now we use a SOAP body for all purposes.
   * Implementation may ask transport layer for the matching container!
   * ATTENTION: We support only operation style 'document', use 'literal', part 'element'!
   * @return a container which can hold message parts (SOAP body)
   */
  public Model getInputContainer() throws ServiceException;

  /**
   * Returns a container which holds the message parts for the output of the operation.
   * By now we use a SOAP body for all purposes.
   * Implementation may ask transport layer for the matching container!
   * ATTENTION: We support only operation style 'document', use 'literal', part 'element'!
   * @return a container which can hold message parts (SOAP body)
   */
  public Model getOutputContainer() throws ServiceException;

  /**
   * Add an element to the given input container. The element must be of the correct type!
   * Element is cloned before adding to input container. 
   * ATTENTION: the operation knows about the input and the published interface will be the part names,
   * so the method resides here and not elsewhere...
   * @param partContainer the container for the message parts
   * @param partName the part name within the container
   * @param element the element to add as part of the message
   */
  public void addInputPart(Model partContainer, QName partName, Model element);

  /**
   * Add an element to the given input container. The element must be of the correct type!
   * Element is cloned before adding to output container.
   * ATTENTION: the operation knows about the output and the published interface will be the part names,
   * so the method resides here and not elsewhere...
   * @param partContainer the container for the message parts
   * @param partName the part name within the container
   * @param element the element to add as part of the message
   */
  public void addOutputPart(Model partContainer, QName partName, Model element);

  /**
   * Get an element out of an input container.
   * ATTENTION: the element will be removed from the container!
   * @see #addInputPart
   * @param partContainer the container for the message parts
   * @param partName the part name within the container
   * @return the element found with the part name
   */
  public Model fetchInputPart(Model partContainer, QName partName);

  /**
   * Get an element out of an output container.
   * ATTENTION: the element will be removed from the container!
   * @see #addOutputPart
   * @param partContainer the container for the message parts
   * @param partName the part name within the container
   * @return the element found with the part name
   */
  public Model fetchOutputPart(Model partContainer, QName partName);
}
