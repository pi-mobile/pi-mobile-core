/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.service;

import de.pidata.log.Logger;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.service.base.ServiceException;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * This defines a singleton for service resolving. GetInstance returns null, until the
 * first instance of a sub class of ServiceController is created. Creating a second instance
 * is not allowed and will lead to an exception.
 */
public class ServiceManager {

  /** Service of type Application Logic */
  public static final int SERVICETYPE_APPLICATION = 10;

  /** Service of type Business Logic */
  public static final int SERVICETYPE_BUSINESSLOGIC = 11;

  /** Service of unspecified type */
  public static final int SERVICETYPE_UNSPECIFIED = 12;

  /** Map of serviceNames --> serviceProviders */
  protected Hashtable services;

  protected String targetID;
  private static ServiceManager instance = null;
  private static Hashtable instanceMap = new Hashtable();
  protected boolean active = true;

  public static ServiceManager getInstance( String targetID ) {
    if (targetID == null) {
      return instance;
    }
    if (instance == null) {
      throw new RuntimeException("Must create default ServiceManager before calling getInstance()" );
    }
    ServiceManager result = (ServiceManager) instanceMap.get( targetID );
    if (result == null) {
      result = instance.createInstance( targetID );
      instanceMap.put( targetID, result );
    }
    return result;
  }

  public static ServiceManager getInstance() {
    return getInstance( null );
  }


  /**
   * Creates a new ServiceController and sets instance.
   * @throws IllegalArgumentException if instance is already set
   */
  public ServiceManager() {
    if (instance != null) {
      throw new RuntimeException( "Changing default ServiceManager is not allowed." );
    }
    init();
    instance = this;
  }

  protected ServiceManager( String targetID ) {
    this.targetID = targetID;
    init();
  }

  private void init() {
    services = new Hashtable();
  }

  protected ServiceManager createInstance( String targetID ) {
    ServiceManager man = new ServiceManager( targetID );
    return man;
  }

  public String getTargetID() {
    return targetID;
  }

  /**
   * Sets this service manager active/inactive. If not active all calls to invokeService
   * will be suspended until we get active. Initial we are active.
   */
  public void setActive(boolean active) {
    this.active = active;
    if (active) {
      synchronized(this) {
        notifyAll();
      }
    }
  }

  /**
   * Suspends caller until we get active (see setActive())
   * @param opName operation or message name for log output if suspened
   */
  protected void waitActive( String opName ) {
    while (!active) {
      synchronized(this) {
        try {
          Logger.info("ServiceManager is inactive, suspending service call: "+opName);
          wait();
        }
        catch (InterruptedException e) {
          return;
        }
      }
    }
  }

  /**
   *
   * @param caller
   * @param portTypeName
   * @param operation
   * @param input
   * @return
   * @throws ServiceException
   * @deprecated bug in older versions of pimobile: services are registered by name and not by portType
   */
  @Deprecated
  public Model invokeService( Context caller, QName portTypeName, QName operation, Model input) throws ServiceException {
    return invokeService( caller, portTypeName.getName(), operation.getName(), input);
  }

  /**
   * Invokes <code>operation</code> on the service identified by serviceName.
   * @param caller
   * @param serviceName the port type to use
   * @param opName      the operation to call
   * @param input       parameters for the call, may be null. Parameters are identified by
   *                    a QName that represents their part-name in case of Web Service calls.
   * @return the service result
   * @throws ServiceException if no service for the given name has been registered, or the underlying
   * service throws an Exception
   */
  public Model invokeService( Context caller, String serviceName, String opName, Model input )
  throws ServiceException {

    waitActive(opName);

    Service provider = getService( serviceName );

    if (provider == null) {
      throw new ServiceException( ServiceException.SERVICE_NOT_FOUND,
                                  "Service name='" + serviceName + "' could not be found." );
    }
    QName operation = provider.portType().getName().getNamespace().getQName( opName );

    Model result = null;
    try {
      result = provider.invoke(caller, operation, input );
    }
    catch (ServiceException ex) {
      Logger.warn( "Target service call failed, serviceName="+serviceName+" opID="+operation+", msg="+ex.getMessage()  );
      throw new ServiceException( ex.getCode(), ex.getMessage(), ex );
    }
    catch (Exception ex) {
      String msg = "Target service call failed, serviceName="+serviceName+" opID="+operation+", msg="+ex.getMessage();
      Logger.warn( msg );
      throw new ServiceException(ServiceException.SERVICE_FAILED, msg, ex );
    }

    return result;
  }

  /**
   * Register Service with its portType name
   * @param portTypName the name to register service for, may differ from portType name
   * @param service
   * @deprecated bug in older versions of pimobile: services are registered by name and not by portType
   */
  @Deprecated
  public void registerService( QName portTypName, Service service ) {
    registerService( portTypName.getName(), service );
  }

  /**
   * Register Service <code>service</code>.
   * @param serviceName the name to register service for, may differ from portType name
   * @param service
   */
  public void registerService( String serviceName, Service service ) {
    services.put( serviceName, service );
  }

  /**
   * Register Service implemented by <code>serviceClass</code>.
   * @param serviceName the name to register service for, may differ from portType name
   * @param serviceClass
   */
  public void registerService( String serviceName, Class serviceClass) throws ServiceException {
    try {
      registerService( serviceName, (Service)serviceClass.newInstance() );
    }
    catch (Exception e) {
      throw new ServiceException( ServiceException.SERVICE_NOT_FOUND, "could not create instance for service name '" + serviceName + "'", e);
    }
  }

  /**
   * Remove Service <code>service</code>.
   * @param serviceName name of service to be removed, same as used with registerService()
   */
  public void removeService( String serviceName ) {
    services.remove( serviceName );
  }

  /**
   * Lookup Service with name <code>serviceName</code>
   * @param serviceName name of service to be retured, same as used with registerService()
   * @return the service that has been registered with serviceName or null if none can be found.
   */
  public Service getService( String serviceName ) {
    return (Service) services.get( serviceName );
  }

  /**
   * Calls shutdown() on all registered services. Called by GuiOperation "Exit" for client and
   * CommServer for server application.
   */
  public void shutdown() {
    Service service;
    for (Enumeration valEnum= services.elements(); valEnum.hasMoreElements(); ) {
      service = (Service) valEnum.nextElement();
      try {
        service.shutdown();
      }
      catch (Exception ex) {
        Logger.error("Could not shutdown service, portType= "+service.portType(), ex);
      }
    }
  }
}
