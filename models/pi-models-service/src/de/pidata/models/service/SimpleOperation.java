/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.service;

import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelFactory;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.Relation;
import de.pidata.qnames.QName;
import de.pidata.service.base.ServiceException;

import java.util.Hashtable;

public class SimpleOperation implements Operation {

  private QName name;
  private ComplexType inputType;
  private ComplexType outputType;
  private Hashtable inputParts;
  private Hashtable outputParts;

  public SimpleOperation( QName name, ComplexType inputType, ComplexType outputType ) {
    this.name = name;
    this.inputType  = inputType;
    this.outputType = outputType;
  }

  public void addInputPartDef( QName relationName ) {
    Relation rel = fetchRelation( relationName );
    addInputPartDef( rel.getRelationID(), rel );
  }

  private Relation fetchRelation( QName relationName ) {
    Namespace ns = relationName.getNamespace();
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( ns );
    if (factory == null) {
      throw new IllegalArgumentException( "Factory missing for namespace: "+ns );
    }
    Relation rel = ModelFactoryTable.getInstance().getFactory( ns ).getRootRelation( relationName );
    if (rel == null) {
      throw new IllegalArgumentException( "Missing root relation: "+relationName );
    }
    return rel;
  }

  public void addInputPartDef( QName partName, Relation partRelation ) {
    if (inputParts == null) inputParts = new Hashtable();
    inputParts.put( partName, partRelation );
  }

  public void addOutputPartDef( QName relationName ) {
    Relation rel = fetchRelation( relationName );
    addOutputPartDef( rel.getRelationID(), rel );
  }

  public void addOutputPartDef( QName partName, Relation partRelation ) {
    if (outputParts == null) outputParts = new Hashtable();
    outputParts.put( partName, partRelation );
  }

  public QName getName() {
    return name;
  }

  public Model getInputContainer() throws ServiceException {
    try {
      return (Model) inputType.getValueClass().newInstance();
    }
    catch (Exception ex) {
      throw new ServiceException( ServiceException.SERVICE_CONFIG_ERROR, "Could not create input container for op="+name, ex );
    }
  }

  public Model getOutputContainer() throws ServiceException {
    try {
      return (Model) outputType.getValueClass().newInstance();
    }
    catch (Exception ex) {
      throw new ServiceException( ServiceException.SERVICE_CONFIG_ERROR, "Could not create input container for op="+name, ex );
    }
  }

  private QName getElementName( QName partName, boolean input ) {
    return partName;
  }

  /**
   * Add an element to the input container. The element must be of the correct type!
   * Element is cloned before adding to input container. 
   * @param partContainer the container for the message parts
   * @param partName the element name within the container
   * @param element the element to add as part of the message
   */
  public void addInputPart(Model partContainer, QName partName, Model element ) {
    QName elemName = getElementName( partName, true );
    partContainer.add( elemName, element.clone(null, true, false ) );
  }

  /**
   * Add an element to the input container. The element must be of the correct type!
   * Element is cloned before adding to output container.
   * @param partContainer the container for the message parts
   * @param partName the element name within the container
   * @param element the element to add as part of the message
   */
  public void addOutputPart(Model partContainer, QName partName, Model element ) {
    QName elemName = getElementName( partName, false );
    partContainer.add( elemName, element.clone(null, true, false ) );
  }

  /**
   * Get an element out of an input container.
   * ATTENTION: the element will be removed from the container!
   *
   * @param partContainer the container for the message parts
   * @param partName      the part name within the container
   * @return the element found with the part name
   * @see #addInputPart
   */
  public Model fetchInputPart(Model partContainer, QName partName) {
    QName elemName = getElementName( partName, true );
    Model element = partContainer.get(elemName, null);
    if (element != null ) {
      partContainer.remove(elemName, element);
    }
    return element;
  }

  /**
   * Get an element out of an output container.
   * ATTENTION: the element will be removed from the container!
   *
   * @param partContainer the container for the message parts
   * @param partName      the part name within the container
   * @return the element found with the part name
   * @see #addOutputPart
   */
  public Model fetchOutputPart(Model partContainer, QName partName) {
    QName elemName = getElementName( partName, false );
    Model element = partContainer.get(elemName, null);
    if (element != null) {
      partContainer.remove(elemName, element);
    }
    return element;
  }
}
