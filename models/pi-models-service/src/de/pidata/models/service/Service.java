/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.service;

import de.pidata.models.config.Binding;
import de.pidata.models.config.Configurator;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.service.base.ServiceException;

public interface Service {

  /**
   * Called by Configurator to initialize this service.
   * @param serviceMgr   the serviceMgr this service will be added to 
   * @param configurator the calling configurator
   * @param binding      the binding config entry for this service
   */
  public void init( ServiceManager serviceMgr, Configurator configurator, Binding binding ) throws Exception;

  public PortType portType();

  public Model invoke( Context caller, QName operation, Model input) throws ServiceException;

  /**
   * Tells this service to shut down, e.g. close database connections
   */
  public void shutdown();
}
