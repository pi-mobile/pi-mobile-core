/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types.simple;

import de.pidata.log.Logger;

public class DecimalObjectTest {

  public static void testFunctions() {

  }

  public static void testSample( String sampleString, double sampleExpectedValue ) {
    try {
      DecimalObject sampleDecimalObject = new DecimalObject( sampleString );
      double resultValue = sampleDecimalObject.doubleValue();
      boolean resultEqual = (Double.compare( sampleExpectedValue, resultValue ) == 0);

      String resultString = sampleDecimalObject.toString();
      DecimalObject reParseDecimalObject = new DecimalObject( resultString );
      double reParseValue = reParseDecimalObject.doubleValue();
      boolean reParseEqual = (Double.compare( sampleExpectedValue, reParseValue ) == 0);

      StringBuilder testRes = new StringBuilder();
      if (resultEqual) {
        testRes.append( "     " );
        if (reParseEqual) {
          testRes.append( "      " );
        }
        else {
          testRes.append( " rfail" );
        }
      }
      else {
        testRes.append( "fail " );
        if (reParseEqual) {
          testRes.append( "      " );
        }
        else {
          testRes.append( " rfail" );
        }
      }
      Logger.info( testRes.toString() + " " + sampleString + " (" + sampleExpectedValue + ") -> " + resultValue + " " + resultEqual + " reparse: " + resultString + " (" + reParseValue + " " + reParseEqual + ")" );
    }
    catch (Exception ex) {
      Logger.error( sampleString, ex );
    }
  }

  public static void main( String[]args) {

    testSample( "0", 0 );
    testSample( "1", 1 );
    testSample( "-1", -1 );

    testSample( "0.42", 0.42 );
    testSample( "-0.42", -0.42 );

    testSample( "0.042", 0.042 );
    testSample( "-0.042", -0.042 );

    testSample( ".42", 0.42 );
    testSample( "-.42", -0.42 );

    testSample( "42", 42 );
    testSample( "-42", -42 );

    testSample( "42.", 42 );
    testSample( "-42.", -42 );

    testSample( "4.2", 4.2 );
    testSample( "-4.2", -4.2 );

    testSample( "4.20", 4.2 );
    testSample( "-4.20", -4.2 );

    testSample( "04.2", 4.2 );
    testSample( "-04.2", -4.2 );


    testSample( "42e3", 42e3 );
    testSample( "-42e3", -42e3 );

    testSample( "42e-3", 42e-3 );
    testSample( "-42e-3", -42e-3 );

    testSample( "4215e3", 4215e3 );
    testSample( "-4215e3", -4215e3 );

    testSample( "4215e-3", 4215e-3 );
    testSample( "-4215e-3", -4215e-3 );

    testSample( "4.2e3", 4.2e3 );
    testSample( "-4.2e3", -4.2e3 );

    testSample( "4.2156e3", 4.2156e3 );
    testSample( "-4.2156e3", -4.2156e3 );

    testSample( "4.2e-3", 4.2e-3 );
    testSample( "-4.2e-3", -4.2e-3 );

    testSample( "4.2156e-3", 4.2156e-3 );
    testSample( "-4.2156e-3", -4.2156e-3 );


    testSample( "5734.3", 5734.3 );
    testSample( "1269.74", 1269.74 );
    testSample( "5119.921875", 5119.921875 );

    testSample( "3.40282346e+38", 3.40282346e+38 );  // Float.MAX_VALUE
    testSample( "9.22337203685478e+18", 9.22337203685478e+18 ); //  Long.MAX_VALUE

    testSample( "4.8828125e-04", 4.8828125e-04 );
    testSample( "1.22e-04", 1.22e-04 );

    testSample( "-3276800.", -3276800. );
    testSample( "-214748364.8", -214748364.8 );
    testSample( "-2.147483648", -2.147483648 );
    testSample( "-8421504.50196078", -8421504.50196078 );

    testSample( "-3.40282346e+38", -3.40282346e+38 ); // -Float.MAX_VALUE
    testSample( "-9.22337203685478e+18", -9.22337203685478e+18 ); // - Long.MAX_VALUE

    testSample( "-3.2768e-02", -3.2768e-02 );
    testSample( "-2.e-03", -2.e-03 );
    testSample( "-1.455191522836685e-11", -1.455191522836685e-11 ); // SQL zero
  }

}