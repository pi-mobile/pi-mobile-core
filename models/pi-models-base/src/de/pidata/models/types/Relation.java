/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types;

import de.pidata.qnames.QName;

public interface Relation {

  Type getChildType();

  int getMaxOccurs();

  int getMinOccurs();

  QName getRelationID();

  /**
   * Returns the interface to be used as collection for this relation,
   * e.g. java.util.List or java.util.Set
   *
   * @return the interface to be used as collection for this relation
   */
  Class getCollection() throws ClassNotFoundException;

  /**
   * Returns the relationName this relation can substitute. So it is possible
   * this relation occurs withing a parent with its own name instead of an
   * element having returned substitution group name.
   *
   * @return the substitution group this relation belongs to or null
   */
  QName getSubstitutionGroup();
}
