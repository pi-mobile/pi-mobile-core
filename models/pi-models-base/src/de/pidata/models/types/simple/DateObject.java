/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types.simple;

import de.pidata.qnames.QName;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class DateObject {
  public static final long ONE_DAY_MILLIS = 24 * 3600 * 1000;
  public static final long ONE_HOUR_MILLIS = 3600 * 1000;
  public static final long ONE_MINUTE_MILLIS = 60 * 1000;
  public static final long ONE_SECOND_MILLIS = 1000;
  
  private long date; //Date in millisec
  private QName type; // one of DateTimeType.TYPE_*

  private static final Date BASE_DATE = new Date(0);
  private static Calendar calendar;

  /**
   * Creates a new DateObject for current system time
   * @param type DateTimeObject.TYPE_DATE, DateTimeObject.TYPE_TIME or DateTimeObject.TYPE_DATETIME
   */
  public DateObject( QName type ) {
    date = System.currentTimeMillis();
    setType( type );
  }

  /**
   * Creates a new DateObject for current system time
   * @param type DateTimeObject.TYPE_DATE, DateTimeObject.TYPE_TIME or DateTimeObject.TYPE_DATETIME
   * @param date date value in milli seconds as defined by java.util.date           
   */
  public DateObject( QName type, long date ) {
    this.date = date;
    setType( type );
  }

  public DateObject( QName type, int year, int month, int dayOfMonth ) {
    Calendar cal = getCalendar();
    cal.set( year, month, dayOfMonth );
    this.date = cal.getTimeInMillis();
    setType( type );
  }

  public DateObject( QName type, int year, int month, int dayOfMonth, int hourOfDay, int minute, int second ) {
    Calendar cal = getCalendar();
    cal.set( year, month, dayOfMonth, hourOfDay, minute, second );
    this.date = cal.getTimeInMillis();
    setType( type );
  }

  /**
   * Creates a date created from strValue in format YYYY-MM-DD hh:mm:ss
   * @param type DateTimeObject.TYPE_DATE, DateTimeObject.TYPE_TIME or DateTimeObject.TYPE_DATETIME
   * @param strValue the date string
   */
  public DateObject( QName type, String strValue ) {
    String    fieldStr;
    int       pos, start, fieldVal;
    int       tzPos = -1;
    Calendar cal = getCalendar();
    boolean  hasTime = false;
    int timeZoneHours = 0;
    int timeZoneMin = 0;

    cal.setTime(BASE_DATE);

    //----- parse year-month-day if exists
    pos = strValue.indexOf('-');
    if (pos >= 0) {
      fieldStr = strValue.substring(0,pos);
      fieldVal = Integer.parseInt(fieldStr);
      if (fieldVal < 100) fieldVal += 2000;
      cal.set(Calendar.YEAR, fieldVal);
      start = pos + 1;
      pos = strValue.indexOf('-', start);
      fieldStr = strValue.substring(start,pos);
      cal.set(Calendar.MONTH, Integer.parseInt(fieldStr)-1);
      start = pos + 1;
      pos = strValue.indexOf(' ', start);
      if (pos < 0) pos = strValue.indexOf('T', start); 
      if (pos < 0) fieldStr = strValue.substring(start,strValue.length());
      else fieldStr = strValue.substring(start,pos);
      cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(fieldStr));
      hasTime = (pos >= 0);
    }
    else {
      hasTime = true;
    }

    //----- parse hour:minute:second if exists
    if (hasTime) {
      start = pos + 1;
      pos = strValue.indexOf(':', start);
      fieldStr = strValue.substring(start,pos);
      cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(fieldStr.trim()));
      start = pos + 1;
      pos = strValue.indexOf(':', start);
      if (pos < 0) fieldStr = strValue.substring(start);
      else fieldStr = strValue.substring(start,pos);
      cal.set(Calendar.MINUTE, Integer.parseInt(fieldStr.trim()));
      if (pos >= 0) {
        start = pos + 1;
        pos = strValue.indexOf('.', start);
        tzPos = strValue.indexOf('Z', start);
        if (tzPos < 0) tzPos = strValue.indexOf('-', start);
        if (tzPos < 0) tzPos = strValue.indexOf('+', start);
        if (pos < 0) {
          if (tzPos < 0) fieldStr = strValue.substring(start);
          else fieldStr = strValue.substring(start,tzPos);
        }
        else {
          fieldStr = strValue.substring(start,pos);
        }
        cal.set(Calendar.SECOND, Integer.parseInt(fieldStr.trim()));
        if (pos >= 0) {
          if (tzPos < 0) fieldStr = strValue.substring(pos+1);
          else fieldStr = strValue.substring( pos+1, tzPos );
          cal.set(Calendar.MILLISECOND, Integer.parseInt(fieldStr.trim()));
        }
      }
    }
    if (tzPos >= 0) {
      if (strValue.charAt( tzPos ) == 'Z') {
        timeZoneHours = 0;
        timeZoneMin = 0;
      }
      else {
        fieldStr = strValue.substring( tzPos + 1 ).trim();
        pos = fieldStr.indexOf( ':' );
        if (pos < 0) {
          timeZoneHours = Integer.parseInt( fieldStr.substring( 0 ) );
          timeZoneMin = 0;
        }
        else {
          timeZoneHours = Integer.parseInt( fieldStr.substring( 0, pos ) );
          timeZoneMin = Integer.parseInt( fieldStr.substring( pos + 1 ) );
        }
        if (strValue.charAt( tzPos ) == '-') {
          timeZoneHours = -timeZoneHours;
          timeZoneMin = -timeZoneMin;
        }
      }
      cal.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
    }
    date = cal.getTime().getTime();
    if (timeZoneHours != 0) {
      date = addHours( date, -timeZoneHours );
    }
    if (timeZoneMin != 0) {
      date = addMinutes( date, -timeZoneMin );
    }
    setType( type );
  }

  private void setType( QName type ) {
    if (type == DateTimeType.TYPE_DATE) {
      this.type = type;
      this.date = getDate();
    }
    else if ((type == DateTimeType.TYPE_TIME) || (type == DateTimeType.TYPE_DATETIME)) {
      this.type = type;
    }
    else {
      throw new IllegalArgumentException( "Illegal type="+type+", expected "+DateTimeType.TYPE_DATE+", "
                                          +DateTimeType.TYPE_TIME+" or "+DateTimeType.TYPE_DATETIME );
    }
  }

  public QName getType() {
    return type;
  }

  public long getTime() {
    return this.date;
  }

  /**
   * Returns the calendar to use for date operations. Default is Calendar.getInstance().
   *
   * @return the calendar
   */
  public static Calendar getCalendar() {
    if (calendar == null) {
      calendar = Calendar.getInstance();
    }
    return calendar;
  }

  /**
   * Sets the calendar to use for date operations.
   * 
   * @param newCalendar the new calendar
   */
  public static void setCalendar( Calendar newCalendar ) {
    calendar = newCalendar;
  }

  /**
   * Note: In some cases 24 houres are not the same as 1 day because set-up of daylightsaving
   */
  public static DateObject addDuration( DateObject date, DurationObject duration ) {
    long millis = date.getTime();
    millis = addSeconds(millis, duration.getSeconds());
    millis = addMinutes(millis, duration.getMinutes());
    millis = addHours(millis, duration.getHours());
    millis = addDays(millis, duration.getDays());
    millis = addMonths(millis, duration.getMonths());
    millis = addYear(millis, duration.getYears());
    return new DateObject( date.getType(), millis);
  }

  /**
   * Note: In some cases 24 houres are not the same as 1 day because set-up of daylightsaving
   */
  public static DateObject subtractDuration( DateObject date, DurationObject duration ) {
    long millis = date.getTime();
    millis = subtractSeconds(millis, duration.getSeconds());
    millis = subtractMinutes(millis, duration.getMinutes());
    millis = subtractHours(millis, duration.getHours());
    millis = subtractDays(millis, duration.getDays());
    millis = subtractMonths(millis, duration.getMonths());
    millis = subtractYear(millis, duration.getYears());
    return new DateObject( date.getType(), millis );

  }

  public static long addYear( long date, int years ) throws IllegalArgumentException {
    if (years < 0) {
      throw new IllegalArgumentException ("Argument years are wrong: years < 0");
    }
    Calendar cal = getCalendar();
    Date Adate = new Date(date);
    cal.setTime(Adate);
    int yearFromDate = cal.get(Calendar.YEAR);
    yearFromDate = yearFromDate + years;
    cal.set(Calendar.YEAR, yearFromDate);
    return cal.getTime().getTime();
  }

  public static long addMonths( long date, int months ) throws IllegalArgumentException {
    if (months < 0) {
      throw new IllegalArgumentException ("Argument months are wrong: months < 0");
    }
    Calendar cal = getCalendar();
    Date tmpDate = new Date(date);
    cal.setTime(tmpDate);
    int monthFromDate = cal.get(Calendar.MONTH);
    int yearFromDate = cal.get(Calendar.YEAR);
    int years;
    monthFromDate = monthFromDate + months;
    if (monthFromDate > 11) {
      years = (monthFromDate + 1) / 12;
      yearFromDate = yearFromDate + years;
      monthFromDate = monthFromDate - years * 12;
    }
    cal.set(Calendar.YEAR, yearFromDate);
    cal.set(Calendar.MONTH, monthFromDate);
    return cal.getTime().getTime();
  }

  /**
   * Note: In some cases 24 houres are not the same as 1 day because set-up of daylightsaving
   */
  public static long addDays( long date, int days ) throws IllegalArgumentException {
    if (days < 0) {
      throw new IllegalArgumentException ("Argument days are wrong: days < 0");
    }
    if (days != 0) {
      Calendar cal = getCalendar();
      long newdate = date + days * ONE_DAY_MILLIS;
      return adjustDate(date, cal, newdate);
    }
    else {
      return date;
    }
  }

  public static long addHours( long date, int hours ) {
    return date + (hours * ONE_HOUR_MILLIS);
  }

  public static long addMinutes( long date, int minutes ) {
    return date + (minutes * ONE_MINUTE_MILLIS);
  }

  public static long addSeconds( long date, int seconds ) {
    return date + (seconds * ONE_SECOND_MILLIS);
  }

  public static long addMillisec( long date, long millisec ) {
    return date + millisec;
  }

  public static long subtractYear( long date, int years ) throws IllegalArgumentException {
    if (years < 0) {
      throw new IllegalArgumentException ("Argument years are years: days < 0");
    }
    Calendar cal = getCalendar();
    Date Adate = new Date(date);
    cal.setTime(Adate);
    int yearFromDate = cal.get(Calendar.YEAR);
    yearFromDate = yearFromDate - years;
    cal.set(Calendar.YEAR, yearFromDate);
    return cal.getTime().getTime();
  }

  public static long subtractMonths( long date, int months ) throws IllegalArgumentException {
    if (months < 0) {
      throw new IllegalArgumentException ("Argument months are wrong: months < 0");
    }
    Calendar cal = getCalendar();
    Date Adate = new Date(date);
    cal.setTime(Adate);
    int monthFromDate = cal.get(Calendar.MONTH);
    int yearFromDate = cal.get(Calendar.YEAR);
    int years;
    monthFromDate = monthFromDate - months;
    if (monthFromDate < 0) {
      years = Math.abs(monthFromDate) / 12;
      yearFromDate = yearFromDate - years;
      monthFromDate = monthFromDate + years * 12;
    }
    cal.set(Calendar.YEAR, yearFromDate);
    cal.set(Calendar.MONTH, monthFromDate);
    return cal.getTime().getTime();
  }

  /**
   * Note: In some cases 24 houres are not the same as 1 day because set-up of daylightsaving
   */

  public static long subtractDays( long date, int days ) throws IllegalArgumentException {
    if (days < 0) {
      throw new IllegalArgumentException ("Argument days are wrong: days < 0");
    }
    if (days != 0) {
      Calendar cal = getCalendar();
      long newdate = date - days * ONE_DAY_MILLIS;
      return adjustDate(date, cal, newdate);
    }
    else {
      return date;
    }
  }

  public static long subtractHours( long date, int hours ) {
    return date - (hours * ONE_HOUR_MILLIS);
  }

  public static long subtractMinutes( long date, int minutes ) {
    return date - (minutes * ONE_MINUTE_MILLIS);
  }

  public static long subtractSeconds( long date, int seconds ) {
    return date - (seconds * ONE_SECOND_MILLIS);
  }

  public static long subtractMillisec( long date, long millisec ) {
    return date - millisec;
  }

  private static long adjustDate(long date, Calendar cal, long newdate) {
    int offsetDate;
    int offsetNewdate;
    cal.setTime(new Date(date));
    offsetDate = cal.getTimeZone().getOffset(1, cal.get(Calendar.YEAR),
        cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.DAY_OF_WEEK),
        cal.get(Calendar.MILLISECOND));
    cal.setTime(new Date(newdate));
    offsetNewdate = cal.getTimeZone().getOffset(1, cal.get(Calendar.YEAR),
        cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.DAY_OF_WEEK),
        cal.get(Calendar.MILLISECOND));

    if (offsetDate == offsetNewdate) {
      return newdate;
    }
    else {
      return newdate + offsetDate - offsetNewdate;
    }
  }

  public boolean equals(Object obj) {
    if ((obj != null) && (obj instanceof DateObject)) {
      return ((DateObject) obj).getTime() == this.date;
    }
    else {
      return false;
    }
  }

  /**
   * Compares date part of this DateObject with otherDate. Time part of both objects are ignored.
   * @param otherDate the date to compare with
   * @return true if this and dateObject have the same date part
   */
  public boolean equalsDate( DateObject otherDate ) {
    Calendar cal = getCalendar();
    cal.setTime( new Date(date) );
    int year = cal.get( Calendar.YEAR );
    int month = cal.get( Calendar.MONTH );
    int day = cal.get( Calendar.DAY_OF_MONTH );

    cal.setTime( new Date(otherDate.getTime()) );
    return (year == cal.get( Calendar.YEAR )) && (month == cal.get( Calendar.MONTH )) && (day == cal.get( Calendar.DAY_OF_MONTH ));
  }

  /**
   * Compares this and obj.
   * @param type  the comparision type, one of DataTimeType.TYPE_*
   * @param obj the object ot compare with
   * @return 0 if this = obj, 1 if this > obj or obj=null, -1 if this < obj
   */
  public int compareTo( QName type, DateObject obj ) {
    if (obj == null) {
      return 1;
    }
    long objTime, myTime;
    if (type == DateTimeType.TYPE_DATE) {
      objTime = obj.getDate();
      myTime = getDate();
    }
    else {
      objTime = obj.getTime();
      myTime  = getTime();
    }
    if (myTime == objTime) return 0;
    else if (myTime > objTime) return 1;
    else  return -1;
  }

  /**
   * Returns the date part of this object by setting hour, minute, second and millisecond to zero
   * @return the date part of this object
   */
  public long getDate() {
    Calendar calendar = getCalendar();
    Date tmpDate = new Date(date);
    calendar.setTime(tmpDate);
    calendar.set(Calendar.HOUR, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    return calendar.getTime().getTime();
  }

  /**
   * Checks if this date/time is between start and end date/time
   * @param type  the comparision type, one of DataTimeType.TYPE_*
   * @param start the lower bound of the date or time range
   * @param end   the upper bound of the date or time range
   * @return true if start <= this <= end
   */
  public boolean between( QName type, DateObject start, DateObject end ) {
    return ( (compareTo( type, start ) >= 0) && (compareTo( type, end ) <= 0) );
  }

  public String toString() {
    return DateTimeType.toDateTimeString(this, true);
  }

  public Calendar toCalendar() {
    Calendar calendar = getCalendar();
    calendar.setTimeInMillis( date );
    return calendar;
  }

  /**
   * Converts date to a german date format
   * @param calendar
   * @param dateFormat one of constants DateTimeType.TYPE_*
   * @param shortYear  true for short year (2 digits)
   * @return the diplayable date
   */
  public String toDisplayString(Calendar calendar, QName dateFormat, boolean shortYear) {
    calendar.setTime(new Date(getTime()));
    return calendarToString( calendar, dateFormat, shortYear );
  }

  public static String calendarToString( Calendar calendar, QName dateFormat, boolean shortYear ) {
    StringBuffer sb = new StringBuffer();
    if (dateFormat == DateTimeType.TYPE_DATE) {
      calendarToDate( calendar, sb, shortYear );
    }
    else if (dateFormat == DateTimeType.TYPE_TIME) {
      calendarToTime( calendar, sb );
    }
    else if (dateFormat == DateTimeType.TYPE_DATETIME) {
      calendarToDate( calendar, sb, shortYear );
      sb.append(' ');
      calendarToTime( calendar, sb );
    }
    else {
      throw new IllegalArgumentException( "Invalid date format: "+dateFormat );
    }
    return sb.toString();
  }

  public static void calendarToTime( Calendar calendar, StringBuffer sb ) {
    short hour = (short) calendar.get(Calendar.HOUR_OF_DAY);
    if (hour > 9) {
      sb.append(Integer.toString(hour));
      sb.append(':');
    }
    else {
      sb.append('0');
      sb.append(Integer.toString(hour));
      sb.append(':');
    }

    short minute = (short) (calendar.get(Calendar.MINUTE));
    if (minute > 9) {
      sb.append(Integer.toString(minute));
    }
    else {
      sb.append('0');
      sb.append(Integer.toString(minute));
    }

    short second = (short) (calendar.get(Calendar.SECOND));
    if (second > 0) {
      sb.append( ':' );
      if (second > 9) {
        sb.append( Integer.toString( second ) );
      }
      else {
        sb.append( '0' );
        sb.append( Integer.toString( second ) );
      }
    }
    short milli = (short) (calendar.get(Calendar.MILLISECOND));
    if (milli > 0) {
      sb.append( '.' );
      if (milli > 99) {
        sb.append( Integer.toString( milli ) );
      }
      else if (milli > 9) {
        sb.append( '0' );
        sb.append( Integer.toString( milli ) );
      }
      else {
        sb.append( "00" );
        sb.append( Integer.toString( milli ) );
      }
    }
  }

  public static void calendarToDate( Calendar calendar, StringBuffer sb, boolean shortYear ) {
    short day = (short) calendar.get(Calendar.DAY_OF_MONTH);
    if (day > 9) {
      sb.append(Integer.toString(day));
      sb.append('.');
    }
    else {
      sb.append('0');
      sb.append(Integer.toString(day));
      sb.append('.');
    }

    short month = (short) (calendar.get(Calendar.MONTH) + 1);

    if (month > 9) {
      sb.append(Integer.toString(month));
      sb.append('.');
    }
    else {
      sb.append('0');
      sb.append(Integer.toString(month));
      sb.append('.');
    }

    short year = (short) calendar.get(Calendar.YEAR);
    if (shortYear) {
      if (year >= 2000)
        year -= 2000;
      if (year <= 9)
        sb.append('0');
    }

    sb.append(Integer.toString(year));
  }
}
