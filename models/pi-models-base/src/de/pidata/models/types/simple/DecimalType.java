/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types.simple;

import de.pidata.models.tree.ValidationException;
import de.pidata.qnames.NamespaceTable;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

public class DecimalType extends NumberType {
  public static final QName TYPE_DECIMAL = QName.getInstance(AbstractSimpleType.NAMESPACE_SCHEMA, "decimal");
  public static final QName TYPE_DOUBLE = QName.getInstance(AbstractSimpleType.NAMESPACE_SCHEMA, "double");
  private static DecimalType defaultType;
  private static DecimalType defaultDouble;
  private static final char DECIMAL_SEPARATOR_DE = ',';
  private static final char THOUSAND_SEPARATOR_DE = '.';
  private static DecimalObject aDecimal = new DecimalObject(0,0);

  private DecimalObject min;
  private DecimalObject max;
  private boolean inclusivemax;
  private boolean inclusivemin;
  private int totalDigits;
  private int fractionDigits;
  private DecimalObject defaultValue = null;

  private int tenthRoot(long val) {
    int i = 0;
    while (val != 0) {
      val = val / 10;
      i++;
    }
    return i;
  }

  public static DecimalType getDefault() {
    if (defaultType == null) {
      defaultType = new DecimalType(TYPE_DECIMAL, null);
    }
    return defaultType;
  }

  public static DecimalType getDefDouble() {
    if (defaultDouble == null) {
      defaultDouble = new DecimalType(TYPE_DOUBLE, null);
    }
    return defaultDouble;
  }

  public DecimalType(QName typeID, Type baseType) {
    super(typeID, aDecimal.getClass(), baseType);
    // use Integer as boundaries to avoid overflow when comparing with different scale
    min = new DecimalObject(Long.MIN_VALUE,0);
    max = new DecimalObject(Long.MAX_VALUE,0);
    inclusivemax = true;
    inclusivemin = true;
    fractionDigits = 15;  // double has 15 digits precision
    totalDigits = 20;     // long has max. 19 digits
  }

  public DecimalType(QName typeID, int totalDigits, int fractionDigits) {
    this(typeID, getDefault(), totalDigits, fractionDigits);
  }

  public DecimalType(QName typeID, Type baseType, int totalDigits, int fractionDigits) {
    super(typeID, aDecimal.getClass(), baseType);
    this.totalDigits = totalDigits;
    this.fractionDigits = fractionDigits;
    inclusivemax = true;
    inclusivemin = true;
    long maxVal = DecimalObject.powerOfTen(totalDigits-fractionDigits);
    min = new DecimalObject(-maxVal, 0);
    max = new DecimalObject(maxVal, 0);
  }

  public DecimalType( QName typeID, Type baseType, DecimalObject maxValue, DecimalObject minValue, boolean inclmax,
                      boolean inclmin) throws IllegalArgumentException {
    super(typeID, aDecimal.getClass(), baseType);
    inclusivemax = inclmax;
    inclusivemin = inclmin;
    if (inclusivemax && inclusivemin) {
      if (maxValue.compareTo(minValue) < 0) {
        throw new IllegalArgumentException("Arguments maxValue and minValue are wrong : maxValue < minValue !!!!");
      }
    }
    else if ((inclusivemax && !inclusivemin) || (!inclusivemax && inclusivemin) || (!inclusivemax && !inclusivemin)) {
      if ((maxValue.compareTo(minValue) < 0) || (maxValue.compareTo(minValue) == 0)) {
     // if (maxValue <= minValue) {
        throw new IllegalArgumentException("Arguments maxValue and minValue are wrong : maxValue <= minValue !!!!");
      }
    }
    max = maxValue;
    min = minValue;
    fractionDigits = -1;
    totalDigits = -1;
  }

  /**
   * Creates a value for this type by parsing the given stringValue
   *
   * @param stringValue string representation of the value to be created, may be null
   * @param namespaces
   * @return a value for this type by parsing the given stringValue
   */
  public Object createValue( String stringValue, NamespaceTable namespaces ) {
    if ((stringValue == null) || (stringValue.length() == 0)) {
      return null;
    }
    return new DecimalObject(stringValue);
  }

  /**
     * Checks if value is valid for this type.
     *
     * @param value       the value to be checked
     * @return NO_ERROR if value is valid, otherwise the errorID
     */
  public int checkValid(Object value) {
    int result = super.checkValid(value);
    DecimalObject decimalObjValue;

      if ((value != null) && (result == ValidationException.NO_ERROR)) {
        decimalObjValue = (DecimalObject) value;
        if (inclusivemax) {
          if (decimalObjValue.compareTo(max) > 0) {
            return ValidationException.ERROR_TOO_BIG;
          }
        }
        else {
          if (decimalObjValue.compareTo(max) > 0 || decimalObjValue.compareTo(max) == 0) {
            return ValidationException.ERROR_TOO_BIG;
          }
        }
        if (inclusivemin) {
          if (decimalObjValue.compareTo(min) < 0) {
            return ValidationException.ERROR_TOO_SMALL;
          }
        }
        else {
          if (decimalObjValue.compareTo(min) < 0 || decimalObjValue.compareTo(min) == 0) {
            return ValidationException.ERROR_TOO_SMALL;
          }
        }
        if (decimalObjValue.getFractionDigits() > this.fractionDigits) {
          return ValidationException.ERROR_FRACTION_TOO_LONG;
        }
      }
      return result;
    }

  /**
   * Returns the message for the given errorID
   *
   * @param errorID the ID of the error for the requested message
   * @return the message for the given errorID
   */
  public String getErrorMessage(int errorID) {
    if (errorID == ValidationException.ERROR_TOO_BIG) {
      return "Decimal is too big, max value="+this.max;
    }
    if (errorID == ValidationException.ERROR_TOO_SMALL) {
      return "Decimal or Long is too small, min value="+this.min;
    }
    if (errorID == ValidationException.ERROR_FRACTION_TOO_LONG) {
      return "Decimal has too many fraction digits, max digits="+this.fractionDigits;
    }
    return super.getErrorMessage(errorID);
  }

  public int getTotalDigits() {
    if (totalDigits < 0) {
      totalDigits = Math.max( Math.max( tenthRoot(max.getValue()), tenthRoot(min.getValue()) ), getFractionDigits() + 1 );
    }
    return totalDigits;
  }

  /**
   * Returns the fractionDigits property, if defined or the largest scale needed for min/max values
   * @return the fractionDigits property
   */
  public int getFractionDigits() {
    if (fractionDigits < 0) {
      return Math.max(max.getScale(), min.getScale());
    }
    else {
      return fractionDigits;
    }
  }

  /**
   * Returnas true if fractionDigits is defined
   * @return true if fractionDigits is defined
   */
  public boolean hasFractionDigits() {
    return (fractionDigits >= 0);
  }

  /**
   * Creatrers a default value for this type definition.
   * @return a default value for this type definition
   */
  public Object createDefaultValue() {
    return defaultValue;
  }

  public DecimalObject getMax() {
    return max;
  }

  public DecimalObject getMin() {
    return min;
  }

  public static String toLocaleString(DecimalObject decimalObject, String locale) {
    if (locale.equals("de")) {
      return (decimalObject.getIntegerPartString()
              + DECIMAL_SEPARATOR_DE + decimalObject.getFractionPartString( decimalObject.getScale() ));
    }
    else {
      return "";
      //TODO andere Sprachen implementieren
    }
  }

  public static String convertLocaleString(String decimal) {
    return decimal.replace(DECIMAL_SEPARATOR_DE, '.');
  }
}
