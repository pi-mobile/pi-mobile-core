/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types.simple;

import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;


public class BooleanType extends AbstractSimpleType {

  public static final QName TYPE_BOOLEAN = AbstractSimpleType.NAMESPACE_SCHEMA.getQName("boolean");
  private static BooleanType defaultType;
  public static Boolean TRUE = new Boolean(true);
  public static Boolean FALSE = new Boolean(false);

  public static BooleanType getDefault() {
    if (defaultType == null) {
      defaultType = new BooleanType(TYPE_BOOLEAN);
    }
    return defaultType;
  }

  public BooleanType(QName typeID) {
    super(typeID, TRUE.getClass(), null);
  }

  /**
   * Creates a value for this type by parsing the given stringValue
   *
   * @param stringValue string representation of the value to be created, may be null
   * @param namespaces
   * @return a value for this type by parsing the given stringValue
   */
  public Object createValue( String stringValue, NamespaceTable namespaces ) {
    if ((stringValue == null) || (stringValue.length() == 0)) {
      return null;
    }
    if ("true".equals(stringValue.toLowerCase())) {
      return BooleanType.TRUE;
    }
    else {
      return BooleanType.FALSE;
    }
  }

  /**
   * Creatrers a default value for this type definition.
   * @return a default value for this type definition
   */
  public Object createDefaultValue() {
    return FALSE;
  }

  /**
   * M
   * @param b
   * @return
   */
  public static Boolean valueOf( boolean bool ) {
    if (bool) return BooleanType.TRUE;
    else return BooleanType.FALSE;
  }
}
