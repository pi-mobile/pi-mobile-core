/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types.simple;

import de.pidata.models.tree.ValidationException;
import de.pidata.qnames.NamespaceTable;
import de.pidata.models.types.Ordered;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

public class IntegerType  extends NumberType implements Ordered {
  public static final QName TYPE_INTEGER = QName.getInstance(AbstractSimpleType.NAMESPACE_SCHEMA, "integer");
  public static final QName TYPE_LONG = QName.getInstance(AbstractSimpleType.NAMESPACE_SCHEMA, "long");
  public static final QName TYPE_INT = QName.getInstance(AbstractSimpleType.NAMESPACE_SCHEMA, "int");
  public static final QName TYPE_SHORT = QName.getInstance(AbstractSimpleType.NAMESPACE_SCHEMA, "short");
  public static final QName TYPE_UNSIGNED_SHORT = QName.getInstance(AbstractSimpleType.NAMESPACE_SCHEMA, "unsignedShort");
  public static final QName TYPE_UNSIGNED_INT = QName.getInstance(AbstractSimpleType.NAMESPACE_SCHEMA, "unsignedInt");
  public static final QName TYPE_UNSIGNED_LONG = QName.getInstance(AbstractSimpleType.NAMESPACE_SCHEMA, "unsignedLong");
  public static final QName TYPE_UNSIGNED_BYTE = QName.getInstance(AbstractSimpleType.NAMESPACE_SCHEMA, "unsignedByte");
  public static final QName TYPE_BYTE = QName.getInstance(AbstractSimpleType.NAMESPACE_SCHEMA, "byte");
  public static final QName TYPE_NEGATIVEINTEGER = QName.getInstance(AbstractSimpleType.NAMESPACE_SCHEMA, "negativeInteger");
  public static final QName TYPE_POSITIVEINTEGER = QName.getInstance(AbstractSimpleType.NAMESPACE_SCHEMA, "positiveInteger");
  public static final QName TYPE_NONNEGATIVEINTEGER = QName.getInstance(AbstractSimpleType.NAMESPACE_SCHEMA, "nonNegativeInteger");
  public static final QName TYPE_NONPOSITIVEINTEGER = QName.getInstance(AbstractSimpleType.NAMESPACE_SCHEMA, "nonPositiveInteger");

  private static IntegerType defaultInteger;
  private static IntegerType defaultLong;
  private static IntegerType defaultInt;
  private static IntegerType defaultShort;
  private static IntegerType defaultUnsignedShort;
  private static IntegerType defaultUnsignedInt;
  private static IntegerType defaultUnsignedLong;
  private static IntegerType defaultUnsignedByte;
  private static IntegerType defaultByte;
  private static IntegerType defaultNegInteger;
  private static IntegerType defaultPosInteger;
  private static IntegerType defaultNonNegInteger;
  private static IntegerType defaultNonPosInteger;

  private static Long aLong = new Long(0);
  private static Integer aInteger = new Integer(0);
  private static Short aShort = new Short((short)0);
  private static Byte aByte = new Byte((byte)0);

  private Long min;
  private Long max;
  private boolean maxInclusive;
  private boolean minInclusive;
  private int inc = 1;

  //TODO Für welchen Fall brauchen wir die totalDigits? Kann man Sie nicht indireckt über min, max festlegen?
  private int totalDigits;

  static {
    defaultInteger = new IntegerType(TYPE_INTEGER, aLong.getClass(), DecimalType.getDefault());
  }

  public static IntegerType getDefLong() {
    if (defaultLong == null) {
      defaultLong = new IntegerType(TYPE_LONG, aLong.getClass(), defaultInteger);
    }
    return defaultLong;
  }

  public static IntegerType getDefInt() {
    if (defaultInt == null) {
      defaultInt = new IntegerType(TYPE_INT, aInteger.getClass(), getDefLong(), Integer.MAX_VALUE, Integer.MIN_VALUE, true, true);
    }
    return defaultInt;
  }

  public static IntegerType getDefShort() {
    if (defaultShort == null) {
      defaultShort = new IntegerType(TYPE_SHORT, aShort.getClass(), getDefInt(), Short.MAX_VALUE, Short.MIN_VALUE, true, true);
    }
    return defaultShort;
  }

  public static IntegerType getDefUnsignedShort() {
    if (defaultUnsignedShort == null) {
      defaultUnsignedShort = new IntegerType(TYPE_UNSIGNED_SHORT, aInteger.getClass(), getDefInt(), 65535, 0, true, true);
    }
    return defaultUnsignedShort;
  }

  public static IntegerType getDefUnsignedInt() {
    if (defaultUnsignedInt == null) {
      defaultUnsignedInt = new IntegerType(TYPE_UNSIGNED_INT, aLong.getClass(), getDefLong(), 2*Integer.MAX_VALUE+1, 0, true, true);
    }
    return defaultUnsignedInt;
  }
  public static IntegerType getDefUnsignedLong() {
    if (defaultUnsignedLong == null) {
      defaultUnsignedLong = new IntegerType(TYPE_UNSIGNED_LONG, aLong.getClass(), getDefLong(), Long.MAX_VALUE, 0, true, true);
    }
    return defaultUnsignedLong;
  }

  public static IntegerType getDefUnsignedByte() {
    if (defaultUnsignedByte == null) {
      defaultUnsignedByte = new IntegerType(TYPE_UNSIGNED_BYTE, aShort.getClass(), getDefShort(), 255, 0, true, true);
    }
    return defaultUnsignedByte;
  }

  public static IntegerType getDefByte() {
    if (defaultByte == null) {
      defaultByte = new IntegerType(TYPE_BYTE, aByte.getClass(), getDefInt(), Byte.MAX_VALUE, Byte.MIN_VALUE, true, true);
    }
    return defaultByte;
  }

  public static IntegerType getDefNonNegativeInteger() {
    if (defaultNonNegInteger == null) {
      defaultNonNegInteger = new IntegerType(TYPE_NONNEGATIVEINTEGER, aInteger.getClass(), defaultInteger, Long.MAX_VALUE, 0, true, true);
    }
    return defaultNonNegInteger;
  }

  public static IntegerType getDefNonPositiveInteger() {
    if (defaultNonPosInteger == null) {
      defaultNonPosInteger = new IntegerType(TYPE_NONPOSITIVEINTEGER, aInteger.getClass(), defaultInteger, 0, Long.MIN_VALUE, true, true);
    }
    return defaultNonPosInteger;
  }

  public static IntegerType getDefNegativeInteger() {
    if (defaultNegInteger == null) {
      defaultNegInteger = new IntegerType(TYPE_NEGATIVEINTEGER, aInteger.getClass(), getDefNonPositiveInteger(), 0, Long.MIN_VALUE, false, true);
    }
    return defaultNegInteger;
  }

  public static IntegerType getDefPositiveInteger() {
    if (defaultPosInteger == null) {
      defaultPosInteger = new IntegerType(TYPE_POSITIVEINTEGER, aInteger.getClass(), getDefNonNegativeInteger(), Long.MAX_VALUE, 0, true, false);
    }
    return defaultPosInteger;
  }

  private int tenthRoot(long val) {
    int i = 0;
    while (val != 0) {
      val = val / 10;
      i++;
    }
    return i;
  }
  public static IntegerType getDefInteger() {
    return defaultInteger;
  }

  public IntegerType(QName typeID, Class valueClass, Type parentType) {
    super(typeID, valueClass, parentType);
    min = new Long(Long.MIN_VALUE);
    max = new Long(Long.MAX_VALUE);
    maxInclusive = true;
    minInclusive = true;
    totalDigits = -1;
  }

  public IntegerType(QName typeID, Class valueClass, Type parentType, long maxValue, long minValue, boolean inclmax,
                     boolean inclmin) throws IllegalArgumentException {
    super(typeID, valueClass, parentType);
    maxInclusive = inclmax;
    minInclusive = inclmin;
    if (maxInclusive && minInclusive) {
      if (maxValue < minValue){
        throw new IllegalArgumentException("Arguments maxValue and minValue are wrong : maxValue < minValue !!!!");
      }
    }
    else if (!maxInclusive && !minInclusive) {
      if (maxValue <= minValue + 1) {
        throw new IllegalArgumentException("Arguments maxValue and minValue are wrong : maxValue <= minValue + 1 !!!!");
      }
    }
    else if ((maxInclusive && !minInclusive) || (!maxInclusive && minInclusive)) {
      if (maxValue <= minValue) {
        throw new IllegalArgumentException("Arguments maxValue and minValue are wrong : maxValue <= minValue !!!!");
      }
    }
    max = new Long(maxValue);
    min = new Long(minValue);
    totalDigits = - 1;
  }

  /**
     * Checks if value is valid for this type.
     *
     * @param value       the value to be checked
     * @return NO_ERROR if value is valid, otherwise the errorID
     */
  public int checkValid(Object value) {
    int result = ValidationException.NO_ERROR;
    //int result = super.checkValid(value);
    long longvalue;

    if ((value != null) && (result == ValidationException.NO_ERROR)) {
      longvalue = longValue(value);
      return checkValid(longvalue);
    }
    return result;
  }

  public int checkValid(long longvalue) {
    if (maxInclusive) {
      if (longvalue > max.longValue()) {
        return ValidationException.ERROR_TOO_BIG;
      }
    }
    else {
      if (longvalue >= max.longValue()) {
        return ValidationException.ERROR_TOO_BIG;
      }
    }
    if (minInclusive) {
      if (longvalue < min.longValue()) {
        return ValidationException.ERROR_TOO_SMALL;
      }
    }
    else {
      if (longvalue <= min.longValue()) {
        return ValidationException.ERROR_TOO_SMALL;
      }
    }
    return ValidationException.NO_ERROR;
  }

  private long longValue(Object value) {
    if (value == null) return 0;
    else if (value instanceof Long) {
      return ((Long) value).longValue();
    }
    else if (value instanceof Integer) {
      return ((Integer) value).longValue();
    }
    else if (value instanceof Short) {
      return ((Short) value).shortValue();
    }
    else if (value instanceof Byte) {
      return ((Byte) value).byteValue();
    }
    else {
      throw new IllegalArgumentException("Unsupported value class: "+value.getClass().getName());
    }
  }

  /**
   * Returns the message for the given errorID
   *
   * @param errorID the ID of the error for the requested message
   * @return the message for the given errorID
   */
  public String getErrorMessage(int errorID) {
    if (errorID == ValidationException.ERROR_TOO_BIG) {
      return "Integer or Long is too big, max value="+this.max;
    }
    if (errorID == ValidationException.ERROR_TOO_SMALL) {
      return "Integer or Long is too small, min value="+this.min;
    }
    return super.getErrorMessage(errorID);
  }

  public int getTotalDigits() {
    if (totalDigits < 0) {
      totalDigits = Math.max(tenthRoot(max.longValue()), tenthRoot(min.longValue()));
    }
    return totalDigits;
  }

  /**
   * Creatrers a default value for this type definition.
   * @return a default value for this type definition
   */
  public Object createDefaultValue() {
    return null;
  }

  /**
   * Creates a value for this type by parsing the given stringValue
   *
   * @param stringValue string representation of the value to be created, may be null
   * @param namespaces
   * @return a value for this type by parsing the given stringValue
   */
  public Object createValue( String stringValue, NamespaceTable namespaces ) {
    if ((stringValue == null) || (stringValue.length() == 0)) {
      return null;
    }
    long value = Long.parseLong(stringValue);
    return createValue(value);
  }

  public Object createValue(long value) {
    int error = checkValid(value);
    if (error != ValidationException.NO_ERROR) {
      throw new IllegalArgumentException("Value is out of range: "+value);
    }
    if (valueClass.getName().endsWith("Integer")) {
      return new Integer((int)value);
    }
    if (valueClass.getName().endsWith("Long")) {
      return new Long(value);
    }
    if (valueClass.getName().endsWith("Short")) {
      return new Short((short) value);
    }
    if (valueClass.getName().endsWith("Byte")) {
      return new Byte((byte) value);
    }
    return null;
  }

  /**
   * Returns the minimum value for this type
   * @return the minimum value for this type
   */
  public Object getMin() {
    return min;
  }

  /**
   * Returns the maximum value for this type
   * @return the maximum value for this type
   */
  public Object getMax() {
    return max;
  }

  public boolean isMaxInclusive() {
    return maxInclusive;
  }

  public boolean isMinInclusive() {
    return minInclusive;
  }

  /**
   * Returns the next larger value of this type or null if
   * value is equal to getMax()
   * @param value the next larger value of this value is returned
   * @return the next larger value of this type or null
   */
  public Object next(Object value) {
    long val = ((Long) value).longValue();
    val += inc;
    if (val > max.longValue())
      return null;
    else
      return new Long(val);
  }

  /**
   * Returns the next lesser value of this type or null if
   * value is equal to getMin()
   * @param value the next lesser value of this value is returned
   * @return the next lesser value of this type or null
   */
  public Object prev(Object value) {
    long val = ((Long) value).longValue();
    val -= inc;
    if (val < min.longValue())
      return null;
    else
      return new Long(val);
  }
}
