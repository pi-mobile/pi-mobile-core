/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types.simple;

import de.pidata.models.tree.ValidationException;
import de.pidata.models.types.Type;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;

public class StringType extends AbstractSimpleType {

  public static final QName TYPE_STRING = AbstractSimpleType.NAMESPACE_SCHEMA.getQName("string");
  public static final QName TYPE_NORMALIZEDSTRING = AbstractSimpleType.NAMESPACE_SCHEMA.getQName("normalizedString");
  public static final QName TYPE_TOKEN = AbstractSimpleType.NAMESPACE_SCHEMA.getQName("token");
  public static final QName TYPE_LANGUAGE = AbstractSimpleType.NAMESPACE_SCHEMA.getQName("language");
  public static final QName TYPE_NMTOKEN = AbstractSimpleType.NAMESPACE_SCHEMA.getQName( "NMTOKEN" );
  public static final QName TYPE_NMTOKENS = AbstractSimpleType.NAMESPACE_SCHEMA.getQName( "NMTOKENS" );
  public static final QName TYPE_ANYURI = AbstractSimpleType.NAMESPACE_SCHEMA.getQName( "anyURI" );
  public static final QName TYPE_IDREFS = AbstractSimpleType.NAMESPACE_SCHEMA.getQName( "IDREFS" );

  private static StringType defaultString;
  private static StringType normalizedString;
  private static StringType token;
  private static StringType defaultLanguage;
  //TODO ist das nicht eigentlich eine QName oder besser sogar ein Path
  private static StringType defaultNMTOKEN;
  private static StringType defaultNMTOKENS;
  private static StringType defaultAnyURI;
  private static StringType idRefs;

  private static String aString = "";

  protected int minLength = 0;
  protected int maxLength = 100;

  static {
    defaultString = new StringType(TYPE_STRING, null, 0, Integer.MAX_VALUE);
    normalizedString = new StringType(TYPE_NORMALIZEDSTRING, defaultString, 0, Integer.MAX_VALUE);
    token = new StringType(TYPE_TOKEN, normalizedString, 0, Integer.MAX_VALUE);
  }

  public static StringType getDefString() {
    return defaultString;
  }

  public static StringType getNormalizedString() {
    return normalizedString;
  }

  public static StringType getDefToken() {
    return token;
  }

  public static StringType getDefLanguage() {
    if (defaultLanguage == null) {
      defaultLanguage = new StringType(TYPE_LANGUAGE, token, 0, Integer.MAX_VALUE);
    }
    return defaultLanguage;
  }

  public static StringType getDefNMTOKEN() {
    if (defaultNMTOKEN == null) {
      defaultNMTOKEN = new StringType( TYPE_NMTOKEN, token, 0, Integer.MAX_VALUE );
    }
    return defaultNMTOKEN;
  }

  public static StringType getDefNMTOKENS() {
    if (defaultNMTOKENS == null) {
      defaultNMTOKENS = new StringType( TYPE_NMTOKENS, token, 0, Integer.MAX_VALUE );
    }
    return defaultNMTOKENS;
  }

  public static StringType getDefAnyURI() {
    if (defaultAnyURI == null) {
      defaultAnyURI = new StringType( TYPE_ANYURI, null, 0, Integer.MAX_VALUE );
    }
    return defaultAnyURI;
  }

  public static StringType getDefIDREFS() {
    if (idRefs == null) {
      idRefs = new StringType( TYPE_IDREFS, null, 0, Integer.MAX_VALUE );
    }
    return idRefs;
  }

  public StringType(QName typeID, Type parentType, boolean nullable) {
    super(typeID, aString.getClass(), parentType);
  }

  public StringType( QName typeID, Type parentType, Class enumeration ) {
    super(typeID, enumeration, parentType);
    if (!enumeration.isEnum()) {
      throw new IllegalArgumentException( "Enumeration must be a Enum class" );
    }
  }

  public StringType(QName typeID, int minLength, int maxLength) {
    this(typeID, getDefString(), minLength, maxLength);
  }

  public StringType(QName typeID, Type parentType, int minLength, int maxLength) {
    super(typeID, aString.getClass(), parentType);
    this.minLength = minLength;
    this.maxLength = maxLength;
  }

  protected StringType(QName typeID, String className, Type parentType, int minLength, int maxLength) {
    super(typeID, className, parentType);
    this.minLength = minLength;
    this.maxLength = maxLength;
  }

  public static String removeLeadingZeros( String strValue ) {
    int index = 0;
    int len = strValue.length();
    while ((index < len) && (strValue.charAt(index) == '0')) index++;
    if (index > 0) {
      strValue = strValue.substring(index);
    }
    return strValue;
  }

  public static String addLeadingZeros( String strValue, int requiredLength ) {
    if (strValue == null) strValue = "";
    int len = strValue.length();
    if (len < requiredLength) {
      StringBuffer result = new StringBuffer( requiredLength );
      for (int i = len; i < requiredLength; i++) {
        result.append( '0' );
      }
      result.append( strValue );
      return result.toString();
    }
    else {
      return strValue;
    }
  }

  public int getMaxLength() {
    return maxLength;
  }

  public int getMinLength() {
    return minLength;
  }

  /**
   * Creates a value for this type by parsing the given stringValue
   *
   * @param stringValue string representation of the value to be created, may be null
   * @param namespaces
   * @return a value for this type by parsing the given stringValue
   */
  public Object createValue( String stringValue, NamespaceTable namespaces ) {

    if (stringValue == null) {
      return null;
    }

    if (valueClass.isEnum()) {
      return stringValue.isEmpty() ? null : Enum.valueOf( valueClass, stringValue );
    }
    else {
      return stringValue;
    }
  }

  protected int checkString(String value) {
    int length;
    if (value == null) length = 0;
    else length = value.length();
    if (length < minLength) {
      return ValidationException.ERROR_TOO_SHORT;
    }
    else if (length > maxLength) {
      return ValidationException.ERROR_TOO_LONG;
    }
    return ValidationException.NO_ERROR;
  }

  /**
   * Checks if value is valid for this type.
   *
   * @param value       the value to be checked
   * @return NO_ERROR if value is valid, otherwise the errorID
   */
  public int checkValid(Object value) {
    int result = super.checkValid(value);

    if ((value != null) && (result == ValidationException.NO_ERROR)) {
      result = checkString( value.toString() );
    }
    return result;
  }


  public String getErrorMessage(int errorID) {
    if (errorID == ValidationException.ERROR_TOO_SHORT) {
      return "String is too short, min length="+this.minLength;
    }
    if (errorID == ValidationException.ERROR_TOO_LONG) {
      return "String is too long, max length="+this.maxLength;
    }
    return super.getErrorMessage(errorID);
  }

  /**
   * Creatrers a default value for this type definition.
   * @return a default value for this type definition
   */
  public Object createDefaultValue() {
    if (this.minLength > 0) {
      StringBuffer result = new StringBuffer();
      for (int i = 0; i < this.minLength; i++) {
        result.append(" ");
      }
      return result.toString();
    }
    else {
      return null;
    }
  }

  private static String trimLeft(String source, String chars) {
    if (source == null || source.equals("")
        || chars == null || chars.equals("")
    ) {
      return source;
    }
    int length = source.length();
    int posMemory = -1;
    for (int i = 0; i < length; i++) {
      char ch = source.charAt(i);
      int pos = chars.indexOf(ch);
      if (pos < 0) {
        break;
      }
      else {
        posMemory = i;
      }
    }
    if (posMemory == -1) {
      return source;
    }
    if (posMemory == length-1) {
      return "";
    }
    return source.substring(posMemory+1);
  }

  private static String trimRight(String source, String chars) {
    if (source == null || source.equals("")
        || chars == null || chars.equals("")
    ) {
      return source;
    }
    int length = source.length();
    int posMemory = length;
    int pos = -1;
    for (int i = length - 1; i >= 0; i--) {
      char ch = source.charAt(i);
      pos = chars.indexOf(ch);
      if (pos < 0) {
        break;
      }
      posMemory = i;
    }
    if (posMemory == length) {
      return source;
    }
    if (posMemory == 0) {
      return "";
    }
    return source.substring(0, posMemory);
  }

  public static String trim(String source, String leftTrimChars, String rightTrimChars) {
    return trimRight(trimLeft(source, leftTrimChars), rightTrimChars);
  }

  public static String fill(String str, int index, int totalLength, char fillchar) {
    if ((str != null) && (str.length() >= totalLength)) {
      return str;
    }
    StringBuffer buf = new StringBuffer();
    int count;
    if (str == null) {
      count = totalLength;
    }
    else {
      count = totalLength - str.length();
    }
    for (int i = 0; i < count; i++) {
      buf.append(fillchar);
    }
    if (str != null) {
      buf.append(str);
    }
    return buf.toString();
  }

  /**
   * Returns true if Strign is null or String length is zero
   * @param str the string to be checked
   * @return true if Strign is null or String length is zero
   */
  public static boolean isEmpty( String str ) {
    return ((str == null) || (str.length() == 0));
  }
}
