/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types.simple;

import de.pidata.qnames.QName;

import java.util.List;
import java.util.Vector;

/**
 * Encapsulates all simple type restrictions known by XML Schema.
 * A restriction can be used to create a simple type. That type
 * fetches only that members from the restriction it supports.
 */
public interface Restriction {

  public static final QName WS_COLLAPSE = AbstractSimpleType.NAMESPACE_SCHEMA.getQName("collapse");
  public static final QName WS_PRESERVE = AbstractSimpleType.NAMESPACE_SCHEMA.getQName("preserve");
  public static final QName WS_REPLACE  = AbstractSimpleType.NAMESPACE_SCHEMA.getQName("replace");

  public String pattern();

  /**
   * Returns a List of String values allowed by this restriction.
   *
   * @return a List of String values allowed by this restriction
   */
  public List<String> enumeration();

  public int length();

  public int minLength();

  public int maxLength();

  /**
   * @return one of the constants WS_*
   */
  public QName whiteSpace();

  public String maxInclusive();

  public String maxExclusive();

  public String minInclusive();

  public String minExclusive();

  public int totalDigits();

  public int fractionDigits();
}
