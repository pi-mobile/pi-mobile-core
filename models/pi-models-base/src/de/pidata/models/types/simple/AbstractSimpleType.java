/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types.simple;

import de.pidata.models.types.AbstractType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.ValueEnum;
import de.pidata.qnames.QName;

import java.util.Enumeration;

public abstract class AbstractSimpleType extends AbstractType implements SimpleType {

  public static final Namespace NAMESPACE_SCHEMA = Namespace.getInstance("http://www.w3.org/2001/XMLSchema");

  protected ValueEnum valueEnum;

  public AbstractSimpleType( QName typeID, String valueClassName, Type parentType) {
    super(typeID, valueClassName, parentType);
  }

  public AbstractSimpleType(QName typeID, Class valueClass, Type parentType) {
    super(typeID, valueClass, parentType);
  }

  /**
   * Returns this simple type's root type by recursively calling getBaseType().
   *
   * @return this simple type's root type
   */
  public SimpleType getRootType() {
    Type baseType = this;
    while (baseType.getBaseType() != null) {
      baseType = baseType.getBaseType();
    }
    return (SimpleType) baseType;
  }

  /**
   * For a simple type the content is its value, so just return this
   * @return this
   */
  @Override
  public SimpleType getContentType() {
    return this;
  }

  /**
   * Returns this type's value enumeration or null
   *
   * @return this type's value enumeration or null
   */
  public ValueEnum getValueEnum() {
    return this.valueEnum;
  }
}
