/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types.simple;

import de.pidata.models.tree.ValidationException;
import de.pidata.qnames.NamespaceTable;
import de.pidata.models.types.Ordered;
import de.pidata.qnames.QName;

import java.util.Calendar;
import java.util.Date;

public class DateTimeType extends AbstractSimpleType implements Ordered {

  public static final QName TYPE_DATETIME = QName.getInstance(AbstractSimpleType.NAMESPACE_SCHEMA, "dateTime");
  public static final QName TYPE_DATE = QName.getInstance(AbstractSimpleType.NAMESPACE_SCHEMA, "date");
  public static final QName TYPE_TIME = QName.getInstance(AbstractSimpleType.NAMESPACE_SCHEMA, "time");
  public static final long ONE_DAY_MILLIS = 24 * 3600 * 1000;

  private static DateTimeType defaultDateTime;
  private static DateTimeType defaultDate;
  private static DateTimeType defaultTime;

  public static DateObject DEFAULT_MIN;
  public static DateObject DEFAULT_MAX;
  static {
    Calendar cal = DateObject.getCalendar();
    cal.set(Calendar.YEAR, 1900);
    cal.set(Calendar.MONTH, 0);
    cal.set(Calendar.DATE, 1);
    cal.set(Calendar.HOUR, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    DEFAULT_MIN = new DateObject( DateTimeType.TYPE_DATETIME, cal.getTime().getTime() );
    DEFAULT_MAX = new DateObject( DateTimeType.TYPE_DATETIME, Long.MAX_VALUE );
  }

  private DateObject min;
  private DateObject max;
  private boolean inclusivemax;
  private boolean inclusivemin;
  private long inc = ONE_DAY_MILLIS;
  private QName type;

  public static DateTimeType getDefDateTime() {
    if (defaultDateTime == null) {
      defaultDateTime = new DateTimeType( TYPE_DATETIME );
    }
    return defaultDateTime;
  }

  public static DateTimeType getDefDate() {
    if (defaultDate == null) {
      defaultDate = new DateTimeType( TYPE_DATE );
    }
    return defaultDate;
  }

  public static DateTimeType getDefTime() {
    if (defaultTime == null) {
      defaultTime = new DateTimeType( TYPE_TIME );
    }
    return defaultTime;
  }

  private DateTimeType( QName typeID ) {
    super( typeID, "de.pidata.models.types.simple.DateObject", null );
    this.type = typeID;
    min = DEFAULT_MIN;
    max = DEFAULT_MAX;
    inclusivemax = true;
    inclusivemin = true;
  }

  public DateTimeType( QName typeID, DateTimeType parentType ) {
    this( typeID, parentType, DEFAULT_MAX, DEFAULT_MIN, true, true );
  }

  public DateTimeType( QName typeID, DateTimeType parentType, DateObject maxValue, DateObject minValue,
                       boolean inclmax, boolean inclmin ) throws IllegalArgumentException {
    super(typeID, "de.pidata.models.types.simple.DateObject", parentType);
    this.type = parentType.getType();
    inclusivemax = inclmax;
    inclusivemin = inclmin;
    if (inclusivemax && inclusivemin) {
      if (maxValue.getTime() < minValue.getTime()){
        throw new IllegalArgumentException("Arguments maxValue and minValue are wrong : maxValue < minValue !!!!");
      }
    }
    else if (!inclusivemax && !inclusivemin) {
      if (maxValue.getTime() <= minValue.getTime() + 1) {
        throw new IllegalArgumentException("Arguments maxValue and minValue are wrong : maxValue <= minValue + 1 !!!!");
      }
    }
    else if ((inclusivemax && !inclusivemin) || (!inclusivemax && inclusivemin)) {
      if (maxValue.getTime() <= minValue.getTime()) {
        throw new IllegalArgumentException("Arguments maxValue and minValue are wrong : maxValue <= minValue !!!!");
      }
    }
    min = minValue;
    max = maxValue;
  }

  public QName getType() {
    return type;
  }

  /**
   * Creatrers a default value for this type definition.
   * @return a default value for this type definition
   */
  public Object createDefaultValue() {
    return null;
  }

  /**
   * Creates a value for this type by parsing the given stringValue
   *
   * @param stringValue string representation of the value to be created, may be null
   * @param namespaces
   * @return a value for this type by parsing the given stringValue
   */
  public Object createValue( String stringValue, NamespaceTable namespaces ) {
    if ((stringValue == null) || (stringValue.length() == 0)) {
      return null;
    }
    return new DateObject( this.type, stringValue );
  }

  /**
     * Checks if value is valid for this type.
     *
     * @param value       the value to be checked
     * @return NO_ERROR if value is valid, otherwise the errorID
     */
  public int checkValid(Object value) {
    int result = super.checkValid(value);

    if ((value != null) && (result == ValidationException.NO_ERROR)) {
      long datevalue = ((DateObject)value).getTime();
      if (inclusivemax) {
        if (datevalue > max.getTime()) {
          return ValidationException.ERROR_TOO_BIG;
        }
      }
      else {
        if (datevalue >= max.getTime()) {
          return ValidationException.ERROR_TOO_BIG;
        }
      }
      if (inclusivemin) {
        if (datevalue < min.getTime()) {
          return ValidationException.ERROR_TOO_SMALL;
        }
      }
      else {
        if (datevalue <= min.getTime()) {
          return ValidationException.ERROR_TOO_SMALL;
        }
      }
    }
    return result;
  }

  /**
   * Converts value to be compatible with this type. Use this method if checkValid retuned
   * ValidationException.NEEDS_CONVERSION
   *
   * @param value the value to be converted
   * @return the converted value
   * @throws IllegalArgumentException if conversion is not possible
   */
  public Object convert( Object value ) {
    int valid = checkValid(value);
    if (valid == ValidationException.NO_ERROR) {
      return value;
    }
    else if (valid == ValidationException.NEEDS_CONVERSION) {
      return new DateObject( getType(), ((DateObject) value).getDate() );
    }
    else {
      throw new IllegalArgumentException( "Value='"+value+"' cannot be converted for type="+this );
    }
  }

  /**
   * Returns the message for the given errorID
   *
   * @param errorID the ID of the error for the requested message
   * @return the message for the given errorID
   */
  public String getErrorMessage(int errorID) {
    if (errorID == ValidationException.ERROR_TOO_BIG) {
      return "DateTime is too big, max value="+this.max.getTime();
    }
    if (errorID == ValidationException.ERROR_TOO_SMALL) {
      return "DateTime is too small, min value="+this.min.getTime();
    }
    return super.getErrorMessage(errorID);
  }

  /**
   * Returns the maximum value for this type
   * @return the maximum value for this type
   */
  public Object getMax() {
    return max;
  }

  /**
   * Returns the minimum value for this type
   * @return the minimum value for this type
   */
  public Object getMin() {
    return min;
  }

  /**
   * Returns the next larger value of this type or null if
   * value is equal to getMax()
   * @param value the next larger value of this value is returned
   * @return the next larger value of this type or null
   */
  public Object next(Object value) {
    DateObject date = (DateObject) value;
    long time = date.getTime();
    time = time + inc;
    if (time < max.getTime()) {
      return new DateObject( date.getType(), time);
    }
    else
      return null;
  }

  /**
   * Returns the next lesser value of this type or null if
   * value is equal to getMin()
   * @param value the next lesser value of this value is returned
   * @return the next lesser value of this type or null
   */
  public Object prev(Object value) {
    DateObject date = (DateObject) value;
    long time = date.getTime();
    time = time - inc;
    if (time > min.getTime()) {
      return new DateObject( date.getType(), time );
    }
    else
      return null;
  }

  public static void calendarDateAdd(Calendar cal, int dayCount) {
    Date date = cal.getTime();
    long time = date.getTime();
    time = time + ONE_DAY_MILLIS * dayCount;
    cal.setTime( new Date(time) );  // TODO geht auf MIDP nicht: cal.setTimeInMillis(time);
  }

//TODO bni: wie soll die timeAdd aussehen ?
  /**
   * Returns a string reprenting date unsing format YYYY-MM-DD hh:mm:ss.milli
   * @param date the date to convert into a string
   * @param withMillis if true result contains .milli otherwise not not
   * @return a string reprenting date unsing format YYYY-MM-DD hh:mm:ss.milli
   */
  public static String toDateTimeString(DateObject date, boolean withMillis) {
    //TODO Schema konformes format schreiben - verträgt das auch dtf/SQL (TeleService!)
    // TODO folgende Codezeile wäre richtig, ist aber inkompatibel mit dem Konstruktor von DateObject
    // in alten Clients: MMontage bis v75, MService bis v30
    // return DateTimeType.toDateString((DateObject) object) + "T" + DateTimeType.toTimeString((DateObject) object, true, 3, ':', true);
    return toDateString(date) + " " + toTimeString(date, withMillis);
  }

  /**
   * Returns a string reprenting date unsing format YYYY-MM-DD hh:mm:ss
   * @param date the date to convert into a string
   * @return a string reprenting date unsing format YYYY-MM-DD hh:mm:ss
   */
  public static String toDateTimeString(DateObject date) {
    //TODO Schema konformes format schreiben - verträgt das auch dtf/SQL (TeleService!)
    // TODO folgende Codezeile wäre richtig, ist aber inkompatibel mit dem Konstruktor von DateObject
    // in alten Clients: MMontage bis v75, MService bis v30
    // return DateTimeType.toDateString((DateObject) object) + "T" + DateTimeType.toTimeString((DateObject) object, true, 3, ':', true);
    return toDateString(date) + " " + toTimeString(date, false);
  }

  /**
   * Returns a string reprenting date unsing format YYYY-MM-DD
   * @param date the date to convert into a string
   * @return a string reprenting date unsing format YYYY-MM-DD
   */

  public static String toDateString(DateObject date) {
    //TODO Schema konformes format schreiben - verträgt das auch dtf/SQL
    // TODO folgende Codezeile wäre richtig, ist aber inkompatibel mit dem Konstruktor von DateObject
    // in alten Clients: MMontage bis v75, MService bis v30
    // return DateTimeType.toDateString((DateObject) object) + "T" + DateTimeType.toTimeString((DateObject) object, true, 3, ':', true);
    if (date == null) return "";
    String temp;
    StringBuffer dateBuf = new StringBuffer();
    Calendar cal = DateObject.getCalendar();
    cal.setTime(new Date(date.getTime()));
    dateBuf.append(Integer.toString(cal.get(Calendar.YEAR))).append('-');
    temp = Integer.toString(cal.get(Calendar.MONTH)+1);
    if (temp.length() == 1) dateBuf.append('0');
    dateBuf.append(temp).append('-');
    temp = Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
    if (temp.length() == 1) dateBuf.append('0');
    dateBuf.append(temp);
    return dateBuf.toString();
  }

  /**
   * Returns a string reprenting date unsing format hh:mm:ss
   * @param date the date to convert into a string
   * @return a string reprenting date unsing format hh:mm:ss
   */
  public static String toTimeString(DateObject date) {
    return toTimeString(date, false);
  }

  /**
   * Returns a string reprenting date unsing format hh:mm:ss.milli
   * @param date the date to convert into a string
   * @param withMillis if true result contains .milli otherwise not not
   * @return a string reprenting date unsing format hh:mm:ss.milli
   */
  public static String toTimeString(DateObject date, boolean withMillis) {
    //TODO Schema konformes format schreiben - verträgt das auch dtf/SQL
    // TODO folgende Codezeile wäre richtig, ist aber inkompatibel mit dem Konstruktor von DateObject
    // in alten Clients: MMontage bis v75, MService bis v30
    // return DateTimeType.toDateString((DateObject) object) + "T" + DateTimeType.toTimeString((DateObject) object, true, 3, ':', true);
    return toTimeString(date, withMillis, 3, ':', false);
  }

  /**
   * Returns a string representing date with given separator and milii precision.
   * @param date the date to convert into a string
   * @param withMillis if true result contains .milli otherwise not not
   * @param millisPrec precision for milli
   * @param timeSep sparator between hh mm ss milli
   * @return a string reprenting date using given format information
   */
  public static String toTimeString(DateObject date, boolean withMillis, int millisPrec, char timeSep, boolean addTimeZone) {
    String temp;
    if (date == null) return "";
    StringBuffer timeBuf = new StringBuffer();
    Calendar cal = Calendar.getInstance();
    long timeMillis = date.getTime();
    cal.setTime(new Date(timeMillis));

    temp = Integer.toString(cal.get(Calendar.HOUR_OF_DAY));
    if (temp.length() == 1) timeBuf.append('0');
    timeBuf.append(temp).append(timeSep);

    temp = Integer.toString(cal.get(Calendar.MINUTE));
    if (temp.length() == 1) timeBuf.append('0');
    timeBuf.append(temp).append(timeSep);

    temp = Integer.toString(cal.get(Calendar.SECOND));
    if (temp.length() == 1) timeBuf.append('0');
    timeBuf.append(temp);

    if (withMillis) {
      timeBuf.append('.');
      temp = Integer.toString(cal.get(Calendar.MILLISECOND));
      for (int i = temp.length(); i < millisPrec; i++) {
        timeBuf.append('0');
      }
      timeBuf.append(temp);
    }
    if (addTimeZone) {
      int year = cal.get( Calendar.YEAR );
      int month = cal.get( Calendar.MONTH );
      int day = cal.get( Calendar.DATE );
      int dayOfWeek = cal.get( Calendar.DAY_OF_WEEK );
      int millisOfDay = cal.get( Calendar.MILLISECOND )
                      + cal.get( Calendar.SECOND ) * 1000
                      + cal.get( Calendar.MINUTE ) * 1000 * 60
                      + cal.get( Calendar.HOUR ) * 1000 * 60 * 60;
      int offset = cal.getTimeZone().getOffset( 1, year, month, day, dayOfWeek, millisOfDay );
      if (offset == 0) {
        timeBuf.append( "Z" );
      }
      else if (offset > 0) {
        timeBuf.append( "+" );
        appendTimeZone( timeBuf, Math.abs( offset ) );
      }
      else {
        timeBuf.append( "-" );
        appendTimeZone( timeBuf, Math.abs( offset ) );
      }
    }
    return timeBuf.toString();
  }

  private static void appendTimeZone( StringBuffer timeBuf, int offset ) {
    int offsetMin = offset / 1000 / 60;
    int hours = offsetMin / 60;
    offsetMin = offsetMin - (60 * hours);
    if (hours < 10) timeBuf.append( "0" );
    timeBuf.append( hours ).append( ":" );
    if (offsetMin < 10) timeBuf.append( "0" );
    timeBuf.append( offsetMin );
  }
}
