/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types.simple;

import de.pidata.qnames.NamespaceTable;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

public class DurationType extends AbstractSimpleType {
  public static final QName TYPE_DURATION = QName.getInstance(AbstractSimpleType.NAMESPACE_SCHEMA, "duration");
  private static DurationType defaultType;
  private static DurationObject defaultValue = new DurationObject(0,0,0);

  public static DurationType getDefault() {
    if (defaultType == null) {
      defaultType = new DurationType(TYPE_DURATION, null, true);
    }
    return defaultType;
  }

  public DurationType( QName typeID, Type parentType, boolean nullable) {
    super(typeID, "de.pidata.models.types.simple.DurationObject", parentType);
  }

  /**
   * Creates a value for this type by parsing the given stringValue
   *
   * @param stringValue string representation of the value to be created, may be null
   * @param namespaces
   * @return a value for this type by parsing the given stringValue
   */
  public Object createValue( String stringValue, NamespaceTable namespaces ) {
    // PnYnMnDTnHnMnS, e.g. PT1H30M
    if (stringValue.startsWith( "P" )) {
      int years = 0;
      int months = 0;
      int days = 0;
      int hours = 0;
      int minutes = 0;
      int seconds = 0;

      int timePos = stringValue.indexOf( 'T' );
      if (timePos < 0) timePos = stringValue.length();

      int start = 1;
      int pos = stringValue.indexOf( 'Y', start );
      if (pos >= 0) {
        if (pos-1 > start) {
          years = Integer.parseInt( stringValue.substring( start, pos-1 ) );
        }
        start = pos + 1;
      }
      pos = stringValue.indexOf( 'M', start );
      if ((pos >= 0) && (pos < timePos)) {
        if (pos-1 > start) {
          months = Integer.parseInt( stringValue.substring( start, pos-1 ) );
        }
        start = pos + 1;
      }
      pos = stringValue.indexOf( 'D', start );
      if (pos >= 0) {
        if (pos-1 > start) {
          days = Integer.parseInt( stringValue.substring( start, pos-1 ) );
        }
      }
      if (timePos < stringValue.length()) {
        start = timePos + 1;
        pos = stringValue.indexOf( 'H', start );
        if (pos >= 0) {
          if (pos-1 > start) {
            hours = Integer.parseInt( stringValue.substring( start, pos-1 ) );
          }
          start = pos + 1;
        }
        pos = stringValue.indexOf( 'M', start );
        if (pos >= 0) {
          if (pos-1 > start) {
            minutes = Integer.parseInt( stringValue.substring( start, pos-1 ) );
          }
          start = pos + 1;
        }
        pos = stringValue.indexOf( 'S', start );
        if (pos >= 0) {
          if (pos-1 > start) {
            seconds = Integer.parseInt( stringValue.substring( start, pos-1 ) );
          }
        }
      }
      return new DurationObject( years, months, days, hours, minutes, seconds );
    }
    else {
      throw new IllegalArgumentException( "Value is not a duration - doen not start with 'P'" );
    }
  }

  public Object createDefaultValue() {
    return null;
  }
}
