/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types.simple;

public class DecimalObject extends Number {

  public static DecimalObject ZERO = new DecimalObject(0,0);
  public static DecimalObject ONE = new DecimalObject(1,0);

  // ATTENTION - Following 2 constants must not be internationalized, becaus e they are used
  // by toStrign, which ist used for externalization to JDBC and XML
  public static final char DECIMAL_SEPARATOR = '.';
  public static final char THOUSAND_SEPARATOR = ',';
  public static final char SIGNUM_SIGN = '-';

  private static final int MAXDIGITSFORMULTIPLY = 18;

  private long value;
  private int scale;
  private int totalDigits;
  private int exponent;

  /**
   * Returns the exp'th power of 10, e.g. if exp is 2 result = 10*10= 100
   * @param exp the exponent
   * @return the exp'th power of 10
   */
  public static long powerOfTen(int exp) {
    if (exp >= 19) {
      throw new NumberFormatException( "Too big exp" );
    }
    long result = 1;

    if (exp == 0) {
      result = 1;
    }
    else {
      for (int i = 1; i <= exp; ++i) {
        result = result * 10;
      }
    }
    return result;
  }

  public DecimalObject(double value, int scale) {
    if (scale < 0)
      throw new NumberFormatException("Negative scale");
    this.scale = scale;
    this.value = (long) (value * powerOfTen( scale ));
    this.totalDigits = tenthRoot(this.value);
  }

  public DecimalObject(long value, int scale) {
    if (scale < 0)
        throw new NumberFormatException("Negative scale");
    this.scale = scale;
    this.value = value;
    this.totalDigits = tenthRoot(value);
  }

  public DecimalObject( String strValue ) {
     this( strValue, DECIMAL_SEPARATOR, THOUSAND_SEPARATOR);
  }

  public DecimalObject(String strValue, char decimalSeparator, char thousandSeparator ) {
    long value, fraction;
    long factor;
    int pos;
    String temp;
    try {
      String digitsStr;
      // get exponent
      String[] eParts = strValue.split( "[eE]" );
      if (eParts.length == 2) {
        // found E format
        digitsStr = eParts[0];
        String expoStr = eParts[1];
        exponent = Integer.parseInt( expoStr );
      }
      else {
        digitsStr = strValue;
      }
      pos = digitsStr.indexOf( SIGNUM_SIGN );
      boolean isNegative = false;
      if (pos >= 0) {
        isNegative = true;
        digitsStr = digitsStr.substring( pos + 1 );
      }
      pos = digitsStr.indexOf( decimalSeparator );
      if (pos < 0) {
        this.scale = 0;
        this.value = Long.parseLong( digitsStr );
      }
      else {
        temp = digitsStr.substring( 0, pos );
        if (temp.length() > 0) {
          value = Long.parseLong( temp );
        }
        else {
          value = 0;
        }
        temp = digitsStr.substring( pos + 1 );
        if (temp.length() > 0) {
          fraction = Long.parseLong( temp );
          this.scale = temp.length();
        }
        else {
          fraction = 0;
          this.scale = 0;
        }
        factor = powerOfTen( this.scale );
        if ((value != 0) && (factor > (Long.MAX_VALUE - fraction) / value)) {
          this.value = Long.MAX_VALUE;
        }
        else {
          this.value = (value * factor) + fraction;
        }
      }
      if (isNegative) {
        this.value = -this.value;
      }
      this.totalDigits = tenthRoot( this.value );
    }
    catch (Exception ex) {
      StringBuilder valueMsg = new StringBuilder();
      valueMsg.append( strValue );
      if (exponent == 38) {
        valueMsg.append( " (Float.MAX_VALUE?)" );
      }
      else if (exponent == -11) {
        valueMsg.append( " (Float.NaN?)" );
      }
      throw new NumberFormatException( "Cannot parse value: " + valueMsg );
    }
  }

  /**
   * Returns values's scale
   * @return scale
   */
  public int getScale() {
    return scale;
  }

  /**
   * Returns scaled value - to get value without fraction use intValue() or longValue().
   * @return scaled value
   */
  public long getValue() {
    return value;
  }

  public DecimalObject add(DecimalObject obj) {
    if (exponent != 0) {
      // TODO - exponent not yet implemented
      throw new RuntimeException( "TODO - exponent not yet implemented");
    }
    if (obj == null) return this;
    long addresult;
    int this_scale = 0;
    int obj_scale = 0;
    if (obj.getScale() < this.scale) {
      obj_scale = this.scale - obj.getScale();
    }
    else if (obj.getScale() > this.scale){
      this_scale = obj.getScale() - this.scale;
    }
    addresult = this.scaleTo(this_scale) + obj.scaleTo(obj_scale);
    return new DecimalObject(addresult, this.scale + this_scale);
  }

  public DecimalObject subtract(DecimalObject obj) {
    if (exponent != 0) {
      // TODO - exponent not yet implemented
      throw new RuntimeException( "TODO - exponent not yet implemented");
    }
    if (obj == null) return this;
    long subtractresult;
    int this_scale = 0;
    int obj_scale = 0;
    if (obj.getScale() < this.scale) {
      obj_scale = this.scale - obj.getScale();
    }
    else if (obj.getScale() > this.scale){
      this_scale = obj.getScale() - this.scale;
    }
    subtractresult = this.scaleTo(this_scale) - obj.scaleTo(obj_scale);
    return new DecimalObject(subtractresult, this.scale + this_scale);

  }

  public DecimalObject multiply(DecimalObject obj) {
    if (exponent != 0) {
      // TODO - exponent not yet implemented
      throw new RuntimeException( "TODO - exponent not yet implemented");
    }
    if (obj == null) return ZERO;
    long multiplyresult;
    int new_scale;
    if ((this.totalDigits + obj.totalDigits) > MAXDIGITSFORMULTIPLY) {
      obj = scalingDownAccuracy(obj);
    }
    multiplyresult = this.value * obj.value;
    new_scale = this.scale + obj.scale;
    //check scale
    if (new_scale < 0) {
      throw new ArithmeticException("multiply: out of range");
    }
    return new DecimalObject(multiplyresult, new_scale);
  }

  private DecimalObject scalingDownAccuracy(DecimalObject obj) {
    if (exponent != 0) {
      // TODO - exponent not yet implemented
      throw new RuntimeException( "TODO - exponent not yet implemented");
    }
    if (obj == null) obj = ZERO;
    obj.cutZeros();
    //obj.scale = 0;
    this.cutZeros();
    int digits = this.totalDigits + obj.totalDigits;
    int tooManyDigits;
    if(digits > MAXDIGITSFORMULTIPLY) {
      tooManyDigits = digits - MAXDIGITSFORMULTIPLY;
      int weightThis = (this.totalDigits*1000)/(digits);
      int tooManyDigitsThis = tooManyDigits * weightThis / (1000);
      int tooManyDigitsObj = tooManyDigits - tooManyDigitsThis;

      // cut digits for This
      this.value = this.value/DecimalObject.powerOfTen(tooManyDigitsThis);
      this.scale = this.scale - tooManyDigitsThis;
      this.totalDigits = tenthRoot(this.value);

      // cut digits for obj
      obj.value = obj.value/DecimalObject.powerOfTen(tooManyDigitsObj);
      obj.scale = obj.scale - tooManyDigitsObj;
      obj.totalDigits = tenthRoot(obj.value);
    }
    return obj;
  }

  /**
   * Returns a DecimalObject scaled to the first not null fraction digit, e.g scales 10.0500 to 10.05
   * @return DecimalObject equal to this but having minimum scale
   */
  public DecimalObject optimizeScale() {
    DecimalObject result = new DecimalObject( getValue(), getScale() );
    result.cutZeros();
    return result;
  }

  private void cutZeros() {
    int i = 1;
    long expTen = 10;
    int borderTotalDigits = this.totalDigits;
    int newScale = this.scale - 1;
    long val = this.value % expTen;
    while (val == 0 && newScale >= 0 && i < borderTotalDigits) {
      newScale--;
      i++;
      expTen = expTen * 10;
      val = this.value % expTen;
    }
    this.scale = newScale + 1;
    this.value = this.value/(expTen/10);
    this.totalDigits = tenthRoot(this.value);
  }

  public DecimalObject divide(DecimalObject obj, int scale) {
    if (exponent != 0) {
      // TODO - exponent not yet implemented
      throw new RuntimeException( "TODO - exponent not yet implemented");
    }
    if (obj == null) obj = ZERO;
    if (scale < 0) {
      throw new ArithmeticException("Negative scale");
    }
    long divideresult;
    int this_scale = 0;
    int obj_scale = 0;
    int lastDigit = 0;
    if (obj.getScale() < this.scale) {
      obj_scale = this.scale - obj.getScale();
    }
    else if (obj.getScale() > this.scale){
      this_scale = obj.getScale() - this.scale;
    }
    divideresult = this.scaleTo(this_scale + scale + 1) / obj.scaleTo(obj_scale);
    lastDigit = (int) (divideresult - (divideresult/10)*10);
    if (lastDigit > 4) {
      divideresult = divideresult + 10;
    }
    return new DecimalObject(divideresult/powerOfTen(1) , scale);
  }

  /**
   * Returns a new DecimalObject being this one rounded to given number
   * of fraction digits
   * @param fracionDigits the number of fraction digits to round to
   * @return the rounded DecimalObject or this if rounding is not necessary
   */
  public DecimalObject round( int fracionDigits ) {
    if (exponent != 0) {
      // TODO - exponent not yet implemented
      throw new RuntimeException( "TODO - exponent not yet implemented");
    }
    if (scale <= fracionDigits) {
      return this;
    }
    else {
      long factor = powerOfTen( this.scale - fracionDigits );
      long newValue = this.value / factor;
      return new DecimalObject( newValue, fracionDigits );
    }
  }

  /**
   * Returns the integer part of this decimal object
   * @return the integer part of this decimal object
   */
  @Override
  public long longValue() {
    if (this.scale < this.exponent) {
      return 0;
    }
    else {
      long factor = powerOfTen( this.scale - this.exponent );
      long result = (this.value / factor);
      return result;
    }
  }

  @Override
  public int intValue() {
    return (int) longValue();
  }

  /**
   * Returns the fraction part of this decimal object scaled by this
   * decimal's scale.
   * @return the fraction part of this decimal object; does not contain leading 0
   */
  public long getFraction() {
    long factor = powerOfTen(this.scale);
    return Math.abs( this.value % factor );
  }

  public int getFractionDigits() {
    if (exponent != 0) {
      // TODO - exponent not yet implemented
      throw new RuntimeException( "TODO - exponent not yet implemented");
    }
    long fraction = getFraction();
    int count = this.scale;
    while ((count > 0) && (fraction % 10) == 0) {
      fraction = fraction / 10;
      count --;
    }
    return count;
  }

  public int getTotalDigits() {
    return totalDigits;
  }

  public int getExponent() {
    return exponent;
  }

  /**
   * Multiplies the value by the given scale of power of ten.
   * If value is defined as infinite (i.e. Long.MIN_VALUE/Long.MAX_VALUE) return the infinite value instead
   * (Long.MIN_VALUE/Long.MAX_VALUE according to signum of value).
   *
   * @param ascale
   * @return the scaled value with respect to the infinite limit
   * @deprecated might return wrong values on overflow; use {@link #scaleToAdjusted(int)} instead
   */
  @Deprecated
  public long scaleTo(int ascale) {
    if (exponent != 0) {
      // TODO - exponent not yet implemented
      throw new RuntimeException( "TODO - exponent not yet implemented");
    }
    if (ascale < 0) {
      throw new NumberFormatException("Negative scale");
    }
    long result = this.value * powerOfTen(ascale);
    //----- simple overflow handling: keep sign
    //TODO besseres overflow handling!!
    if  ((this.value > 0) && (result < 0)) result = Long.MAX_VALUE;
    else if ((this.value < 0) && (result > 0)) result = Long.MIN_VALUE;
    return result;
  }

  /**
   * Compares this and obj
   * @param obj the object ot compare with
   * @return 0 if this = obj, 1 if this > obj, -1 if this < obj
   */
  public int compareTo (DecimalObject obj) {
    if (obj == null) obj = ZERO;
    // cut short if sign differs
    if (this.value == 0) {
      if (obj.getValue() == 0) {
        return 0;
      }
      else if (obj.getValue() < 0) {
        return 1;
      }
      else if (obj.getValue() > 0) {
        return -1;
      }
    }
    if (this.value > 0 && obj.getValue() <= 0) {
      return 1;
    }
    if (this.value < 0 && obj.getValue() >= 0) {
      return -1;
    }
    int this_int_magnitude = this.totalDigits - this.scale + this.exponent;
    if (this.exponent < 0) {
      this_int_magnitude -= 1;
    }
    int obj_int_magnitude = obj.getTotalDigits() - obj.getScale() + obj.getExponent();
    if (obj.getExponent() < 0) {
      obj_int_magnitude -= 1;
    }
    // don't scale if integer magnitude differs
    if (this_int_magnitude > obj_int_magnitude) {
      if (this.value > 0) {
        return 1;
      }
      else {
        return -1;
      }
    }
    else if (this_int_magnitude < obj_int_magnitude) {
      if (this.value > 0) {
        return -1;
      }
      else {
        return 1;
      }
    }
    if (this.exponent != 0 || obj.getExponent() != 0) {
      // TODO - exponent not yet implemented
      throw new RuntimeException( "TODO - exponent not yet implemented" );
    }
    int result = 0;
    int this_scale = 0;
    int obj_scale = 0;
    if (obj.getScale() < this.scale) {
      obj_scale = this.scale - obj.getScale();
    }
    else if (obj.getScale() > this.scale) {
      this_scale = obj.getScale() - this.scale;
    }
    // prevent scale if min/max are defined as infinite
    long myValue = this.scaleToAdjusted( this_scale );
    long otherValue = obj.scaleToAdjusted( obj_scale );
    // compare
    if (myValue == otherValue) {
      result = 0;
    }
    else if (myValue > otherValue) {
      result = 1;
    }
    else if (myValue < otherValue) {
      result = -1;
    }
    return result;
  }

  /**
   * Multiplies the value by the given scale of power of ten.
   * If value is defined as infinite (i.e. Long.MIN_VALUE/Long.MAX_VALUE) return the infinite value instead
   * (Long.MIN_VALUE/Long.MAX_VALUE according to signum of value).
   * @param aScale
   * @return the scaled value with respect to the infinite limit
   */
  public long scaleToAdjusted( int aScale ) {
    if (exponent != 0) {
      // TODO - exponent not yet implemented
      throw new RuntimeException( "TODO - exponent not yet implemented");
    }
    if (aScale < 0) {
      throw new NumberFormatException( "Negative scale" );
    }
    long result;
    if ((aScale != 0) && (this.value != 0)) {
      if (this.value == Long.MAX_VALUE) {
        result = Long.MAX_VALUE;
      }
      else if (this.value == Long.MIN_VALUE) {
        result = Long.MIN_VALUE;
      }
      else {
        long scaleFactor = powerOfTen( aScale );
        if ((this.value > 0) && (scaleFactor > Long.MAX_VALUE / this.value)) {
          result = Long.MAX_VALUE;
        }
        else if ((this.value < 0) && (scaleFactor < Long.MIN_VALUE / this.value)) {
          result = Long.MIN_VALUE;
        }
        else {
          result = this.scaleTo( aScale );
        }
      }
    }
    else {
      result = this.value;
    }
    return result;
  }

  public boolean equals( Object o ) {
    if (o instanceof DecimalObject) {
      return (compareTo( (DecimalObject) o) == 0);
    }
    else {
      return false;
    }
  }

  public String getIntegerPartString() {
    if (exponent != 0) {
      // TODO - exponent not yet implemented
      throw new RuntimeException( "TODO - exponent not yet implemented");
    }
    long value;
    long factor = powerOfTen( this.scale );
    value = this.value / factor;
    return Long.toString(value);
  }

  public String getFractionPartString( int fractionDigits ) {
    if (fractionDigits <= 0) {
        throw new NumberFormatException( "fractionDigits must not be negative: "+fractionDigits );
    }
    StringBuffer buffer = new StringBuffer();
    buffer.append( getFraction() );

    //---- leading zeros
    while (buffer.length() < this.scale)  {
      buffer.insert( 0, "0" );
    }

    int fractionLenth = buffer.length();
    if (fractionLenth < fractionDigits) {
      //--- trailing zeros
      for (int i = 0; i < (fractionDigits - fractionLenth); i++  )  {
        buffer.append("0");
      }
    }
    else if (fractionLenth > fractionDigits) {
      char lastDigit = buffer.charAt( fractionDigits-1 );
      char nextDigit = buffer.charAt( fractionDigits );
      if (nextDigit >= '5') lastDigit += 1;
      buffer.setCharAt( fractionDigits-1, lastDigit);
      buffer.setLength( fractionDigits );
    }
    return buffer.toString();
  }

  /**
   * Returns this decimal as string in format xxx.yy
   * @return this decimal as string in format xxx.yy
   */
  public String toString() {
    return toString( DECIMAL_SEPARATOR, this.scale );
  }

  public String toString( char decimalSeparator, int fractionDigits ) {
    if (fractionDigits < 0) {
      throw new NumberFormatException( "fractionDigits must not be negative: " + fractionDigits );
    }
    StringBuilder result = new StringBuilder();
    long value;
    boolean isNegative = (this.value < 0);
    long factor = powerOfTen( this.scale );
    value = this.value / factor;
    if (fractionDigits == 0) {
      result.append( Long.toString( value ) );
    }
    else {
      String fractionStr = this.getFractionPartString( fractionDigits );
      if (isNegative && value >= 0) {
        // integer part 0 lost sign
        result.append( SIGNUM_SIGN + Long.toString( value ) + decimalSeparator + fractionStr );
      }
      else {
        result.append( Long.toString( value ) + decimalSeparator + fractionStr );
      }
    }
    if (this.exponent != 0) {
      result.append( "e" ).append( Integer.toString( this.exponent ) );
    }
    return result.toString();
  }

  private int tenthRoot(long val) {
    int i = 0;
    while (val != 0) {
      val = val / 10;
      i++;
    }
    return i;
  }

  /**
   * Returns the value of the specified number as a {@code double},
   * which may involve rounding.
   *
   * @return the numeric value represented by this object after conversion
   * to type {@code double}.
   */
  @Override
  public double doubleValue() {
    // TODO optimieren
    return Double.parseDouble( toString() );
  }

  /**
   * Returns the value of the specified number as a {@code float},
   * which may involve rounding.
   *
   * @return the numeric value represented by this object after conversion
   * to type {@code float}.
   */
  @Override
  public float floatValue() {
    // TODO optimieren
    return Float.parseFloat( toString() );
  }
}
