/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types.simple;


public class DurationObject {
  /**
   * Note: In some cases 24 houres are not the same as 1 day because set-up of daylightsaving
   */
  private int years;
  private int months;
  private int days;
  private int hours;
  private int minutes;
  private int seconds;

  public DurationObject (int years, int months, int days, int hours, int minutes, int seconds) throws IllegalArgumentException {
    if ((years < 0) || (months < 0) || (days < 0) || (hours < 0) || (minutes < 0) || (seconds < 0) )
      throw new IllegalArgumentException("Arguments are wrong : one or more Arguments are < 0 !!!!");
    this.years = years;
    this.months = months;
    this.days = days;
    this.hours = hours;
    this.minutes = minutes;
    this.seconds = seconds;
  }

  public DurationObject(int years, int months, int days ) throws IllegalArgumentException {
    if ((years < 0) || (months < 0) || (days < 0))
      throw new IllegalArgumentException("Arguments are wrong : one or more Arguments are < 0 !!!!");
    this.years = years;
    this.months = months;
    this.days = days;
    this.hours = 0;
    this.minutes = 0;
    this.seconds = 0;
  }

  public DurationObject(int hours, int minutes, int seconds, int millisec) throws IllegalArgumentException {
    if ((hours < 0) || (minutes < 0) || (seconds < 0) || (millisec < 0))
      throw new IllegalArgumentException("Arguments are wrong : one or more Arguments are < 0 !!!!");
    this.hours = hours;
    this.minutes = minutes;
    this.seconds = seconds;
    this.years = 0;
    this.months = 0;
    this.days = 0;
  }

  public int getDays() {
    return days;
  }

  public int getHours() {
    return hours;
  }

  public int getMinutes() {
    return minutes;
  }

  public int getMonths() {
    return months;
  }

  public int getSeconds() {
    return seconds;
  }

  public int getYears() {
    return years;
  }

  public String toDisplayString() {
    if ((years > 0) || (months > 0) || (days > 0)) {
      return toString();
    }
    else {
      int h = hours;
      int m = minutes;
      int s = seconds;
      if (s >= 60 ) {
        m += s / 60;
        s = s % 60;
      }
      if (m >= 60) {
        h += m / 60;
        m = m % 60;
      }

      StringBuilder builder = new StringBuilder();
      builder.append( h ).append( ":" );
      if (m < 10) {
        builder.append( "0" );
      }
      builder.append( m );
      if (s > 0) {
        builder.append( ":" );
        if (s < 10) {
          builder.append( "0" );
        }
        builder.append( s );
      }
      return builder.toString();
    }
  }

  public String toString() {
    StringBuffer buffer = new StringBuffer( 'P' );
    if (years != 0) {
      buffer.append( years ).append( 'Y' );
    }
    if (months != 0) {
      buffer.append( months ).append( 'M' );
    }
    if (days != 0) {
      buffer.append( days ).append( 'D' );
    }
    if ((hours != 0) || (minutes != 0) || (seconds != 0)) {
      buffer.append( 'T' );
      if (hours != 0) {
        buffer.append( hours ).append( 'H' );
      }
      if (minutes != 0) {
        buffer.append( minutes ).append( 'M' );
      }
      if (seconds != 0) {
        buffer.append( seconds ).append( 'S' );
      }
    }
    return buffer.toString();
  }
}
