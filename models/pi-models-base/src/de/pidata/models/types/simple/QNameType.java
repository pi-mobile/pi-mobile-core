/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types.simple;

import de.pidata.models.tree.ValidationException;
import de.pidata.qnames.NamespaceTable;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

public class QNameType extends StringType {

  public static final QName TYPE_QNAME = AbstractSimpleType.NAMESPACE_SCHEMA.getQName( "QName" );
  public static final QName TYPE_NAME = AbstractSimpleType.NAMESPACE_SCHEMA.getQName( "Name" );
  public static final QName TYPE_NCNAME = AbstractSimpleType.NAMESPACE_SCHEMA.getQName( "NCName" );
  public static final QName TYPE_ID = AbstractSimpleType.NAMESPACE_SCHEMA.getQName( "ID" );
  public static final QName TYPE_IDREF = AbstractSimpleType.NAMESPACE_SCHEMA.getQName( "IDREF" );
  public static final QName TYPE_ENTITY = AbstractSimpleType.NAMESPACE_SCHEMA.getQName( "ENTITY" );

  private static QNameType defaultQName;
  private static QNameType defaultNCName;
  private static QNameType defaultName;
  private static QNameType defaultID;
  private static QNameType defaultIDREF;
  private static QNameType defaultENTITY;

  static {
    defaultQName = new QNameType(TYPE_QNAME, StringType.getDefToken(), 0, 256);
    defaultName = new QNameType(TYPE_NAME, StringType.getDefToken(), 0, 256);
  }

  public static QNameType getInstance() {
    return defaultQName;
  }

  public static QNameType getQName() {
    return defaultQName;
  }

  public static QNameType getNameType() {
    return defaultName;
  }

  public static QNameType getNCName() {
    if (defaultNCName == null) {
      defaultNCName = new QNameType( TYPE_NCNAME, defaultName, 0, Integer.MAX_VALUE );
    }
    return defaultNCName;
  }

  public static QNameType getIDType() {
    if (defaultID == null) {
      defaultID = new QNameType( TYPE_ID, getNCName(), 0, Integer.MAX_VALUE );
    }
    return defaultID;
  }

  public static QNameType getIDREFType() {
    if (defaultIDREF == null) {
      defaultIDREF = new QNameType( TYPE_IDREF, getNCName(), 0, Integer.MAX_VALUE );
    }
    return defaultIDREF;
  }

  public static QNameType getENTITYType() {
    if (defaultENTITY == null) {
      defaultENTITY = new QNameType( TYPE_ENTITY, getNCName(), 0, Integer.MAX_VALUE );
    }
    return defaultENTITY;
  }

  public QNameType(QName typeID, int minLen, int maxLen) {
    this(typeID, defaultName, minLen, maxLen);
  }

  public QNameType( QName typeID, Type parentType, int minLen, int maxLen) {
    super(typeID, "de.pidata.qnames.QName", parentType, minLen,  maxLen);
  }

  /**
   * Creates a value for this type by parsing the given stringValue
   *
   * @param stringValue string representation of the value to be created, may be null
   * @param namespaces
   * @return a value for this type by parsing the given stringValue
   */
  public Object createValue( String stringValue, NamespaceTable namespaces ) {
    if ((stringValue == null) || (stringValue.length() == 0)) {
      return null;
    }
    return QName.getInstance( stringValue, namespaces );
  }

  /**
   * Checks if value is valid for this type.
   *
   * @param value       the value to be checked
   * @return NO_ERROR if value is valid, otherwise the errorID
   */
  public int checkValid(Object value) {
    if ((value == null) || (valueClass.isAssignableFrom(value.getClass()))) {
      if (value == null) return checkString(null);
      else return checkString(((QName) value).getName());
    }
    else {
      return ValidationException.ERROR_WRONG_CLASS;
    }
  }

  /**
   * Creatrers a default value for this type definition.
   * @return a default value for this type definition
   */
  public Object createDefaultValue() {
    if (this.minLength > 0) {
      StringBuffer result = new StringBuffer();
      for (int i = 0; i < this.minLength; i++) {
        result.append(" ");
      }
      return name().getNamespace().getQName(result, 0, result.length());
    }
    else {
      return null;
    }
  }
}
