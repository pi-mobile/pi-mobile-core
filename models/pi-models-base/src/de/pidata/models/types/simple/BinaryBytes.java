/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types.simple;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class BinaryBytes implements Binary {

  protected byte[] data;
  protected int length;

  public BinaryBytes() {
    data = null;
    length = 0;
  }

  public BinaryBytes( byte[] data ) {
    this( data, data.length );
  }

  public BinaryBytes( byte[] data, int length ) {
    this.data = data;
    this.length = length;
  }

  public void readBytes( InputStream byteStream ) throws IOException {
    ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    byte[] buf = new byte[100];
    int count;
    do {
      count = byteStream.read(buf);
      if (count > 0) {
        outStream.write(buf, 0, count);
      }
    }
    while (count > 0);
    this.data = outStream.toByteArray();
    this.length = data.length;
  }


  public int size() {
    if (data == null) return 0;
    else return length;
  }

  public void writeBytes( OutputStream out ) throws IOException {
    out.write(data, 0, length);
  }

  public byte[] getBytes() {
    return data;
  }
}

