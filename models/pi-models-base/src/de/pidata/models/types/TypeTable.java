/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types;

import de.pidata.models.types.simple.AbstractSimpleType;
import de.pidata.models.types.simple.BooleanType;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.models.types.simple.DecimalType;
import de.pidata.models.types.simple.DurationType;
import de.pidata.models.types.simple.IntegerType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Namespace;

import java.util.Hashtable;

/**
 * Tabble mapping typeIDs to Types
 */
public class TypeTable {

  private static Hashtable namespaceTypeTables = new Hashtable();

  private Namespace namespace = null;
  private Hashtable types = new Hashtable();

  static {
    initSchemaTypes();
  }

  /**
   * Creates a new Typetable for the given namespace
   * @param namespace the namespace for this type binding
   */
  private TypeTable(Namespace namespace) {
    this.namespace = namespace;
  }

  /**
   * Initilialize the standard schema and internal types
   */
  private static void initSchemaTypes() {
    // all schema types
    TypeTable schema = getInstance(AbstractSimpleType.NAMESPACE_SCHEMA);

    schema.addType(StringType.getDefString());
    schema.addType(StringType.getDefLanguage());
    schema.addType(StringType.getDefNMTOKEN() );
    schema.addType(StringType.getDefAnyURI() );

    schema.addType(IntegerType.getDefInteger());
    schema.addType(IntegerType.getDefLong());
    schema.addType(IntegerType.getDefInt());
    schema.addType(IntegerType.getDefShort());

    schema.addType(BooleanType.getDefault());

    schema.addType(DecimalType.getDefault());

    schema.addType(DateTimeType.getDefDateTime());
    schema.addType(DateTimeType.getDefDate());
    schema.addType(DateTimeType.getDefTime());

    schema.addType(DurationType.getDefault());

    schema.addType( QNameType.getInstance() );
    schema.addType( QNameType.getQName() );
    schema.addType( QNameType.getNCName() );
    schema.addType( QNameType.getNameType() );
    schema.addType( QNameType.getIDType() );
    schema.addType( QNameType.getIDREFType() );
    schema.addType( QNameType.getENTITYType() );

    //   types.put(DoubleType.TYPE_DOUBLE, DoubleType.getDefault());
    //   types.put(FloatType.TYPE_FLOAT, FloatType.getDefault());
    namespaceTypeTables.put(AbstractSimpleType.NAMESPACE_SCHEMA, schema);
  }

  /**
   * Returns an instance of TypeTable for the given namespace
   * @param namespace the namespace for the typetable
   * @return the typetable for this workspace
   */
  public static TypeTable getInstance(Namespace namespace) {
    TypeTable typeTable = null;
    if (namespaceTypeTables.containsKey(namespace)) {
      typeTable = (TypeTable)namespaceTypeTables.get(namespace);
    }
    else {
      typeTable = new TypeTable(namespace);
      namespaceTypeTables.put(namespace,typeTable);
    }
    return typeTable;
  }

  public static Type findType(QName typeID) {
    TypeTable table = getInstance(typeID.getNamespace());
    return table.getType(typeID);
  }

  /**
   * Returns the Type for the given typeID
   * @param typeID the ID of the type to be returned
   * @return the Type for the given typeID
   */
  public Type getType(QName typeID) {
    return (Type) types.get(typeID);
  }

  /**
   * Adds the given type to this type binding
   * @param type the type to be added
   */
  public void addType(Type type) {
    this.types.put(type.name(), type);
  }
}
