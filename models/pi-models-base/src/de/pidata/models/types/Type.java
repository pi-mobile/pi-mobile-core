/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types;

import de.pidata.qnames.QName;

public interface Type {

  public QName name();

  /**
   * Returns this type's base type. Is null for base schema simpleTypes and
   * for complexTypes without base type definition.
   * @return this type's parent type or null
   */
  public Type getBaseType();

  /**
   * Returns the message for the given errorID
   *
   * @param errorID the ID of the error for the requested message
   * @return the message for the given errorID
   */
  public String getErrorMessage(int errorID);

  /**
   * Returns the Class to be used for values conforming to this Type
   *
   * @return the Class to be used for values conforming to this Type
   *
   */
  public Class getValueClass();

  /**
   * Checks if value is valid for this type.
   *
   * @param value       the value to be checked
   * @return NO_ERROR if value is valid, otherwise the errorID
   */
  public int checkValid(Object value);


  /**
   * Converts value to be compatible with this type. Use this method if checkValid returned
   * ValidationException.NEEDS_CONVERSION
   * @param value the value to be converted
   * @return the converted value
   * @throws IllegalArgumentException if conversion is not possible
   */
  public Object convert( Object value );

  /**
   * Returns true if this type is the same or a base type (recursive)
   * of childType
   * @param childType the type to check assignability
   * @return true if instances of childType can be assigned to fields of this type
   */
  public boolean isAssignableFrom(Type childType);

  /**
   * Returns the content type for this type.
   * For a SimpleType it is the type itself.
   * For a ComplexType it is either null for mixed content
   * or the type to be used for this type's content (see Model.getContent()).
   * @return the content type or null
   */
  SimpleType getContentType();
}
