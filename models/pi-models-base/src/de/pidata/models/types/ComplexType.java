/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types;

import de.pidata.models.tree.ModelReference;
import de.pidata.models.tree.QNameIterator;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

public interface ComplexType extends Type {

  Namespace NAMESPACE_MODELS = Namespace.getInstance("de.pidata.models");
  QName CDATA = NAMESPACE_MODELS.getQName( "CDATA" );

  /**
   * Returns true if this type can hold mixed content
   * @return true if this type  can hold mixed content
   */
  public  boolean isMixed();

  /**
   * Returns true if this type is an abstract type
   * @return true if this type is an abstract type
   */
  public boolean isAbstract();

  /**
   * Returns the number of attributes for Objects using this type
   *
   * @return the number of attributes for Objects using this type
   */
  public int attributeCount();

  /**
   * Returns the number of attributes forming this model's key
   * beginning with attribute at index 0. Result will be 0 or 1
   * in most cases. The attribute list must be ordered, so that
   * all attributes beeing part of the key are coming frist.
   * @return the nmumber of key attributes
   */
  public int keyAttributeCount();

  /**
   * Returns the type definition for the attribute at index.
   *
   * @param index index of the attribute
   * @return the type definition for the attribute at index.
   */
  public SimpleType getAttributeType(int index);

  /**
   * Returns the ID (name) for the attribute at index.
   *
   * @param index index of the attribute
   * @return the ID (name) for the attribute at index.
   */
  public QName getAttributeName(int index);

  /**
   * Returns the default value for the attribute at index or null if
   * a default is not defined.
   *
   * @param index index of the attribute
   * @return default value for the attribute at index or null
   */
  public Object getAttributeDefault(int index);

  /**
   * Returns the index of the attribute identified by attributeName.
   * This may be used to store attributes in an array. The order
   * of attributes depends on the order in the type definition
   *
   * @param attributeName the attribute's ID
   * @return the index of attributeName or -1 if attributeName does
   *         not exist
   */
  public int indexOfAttribute(QName attributeName);

  /**
   * Returns this types anyAttribiute definition or null if not present
   * @return  this types anyAttribiute definition or null
   */
  public AnyAttribute getAnyAttribute();

  /**
   * Returns the type definition for the child relation identified by relationName
   *
   * @param relationName  ID of the child relation for which type information is requested
   * @return the type definition for the child relation identified by relationName
   */
  public Type getChildType(QName relationName);

  /**
   * Returns the child relation identified by relationName or null if relationName is
   * not defined.
   *
   * @param relationName  ID of the child relation for which type information is requested
   * @return the child relation identified by relationName
   */
  public Relation getRelation(QName relationName);

  /**
   * Checks if this type allows child having childDef to be added to a model
   * having this type.
   *
   * @param relationName  ID of the child relation for which checking is done
   * @param childType the child definition to be checked
   * @return NO_ERROR if child is allowed, otherwise the error number
   */
  public int allowsChild(QName relationName, Type childType);

  /**
   * Returns an Enumeration over this type's child relation names
   *
   * @return an Enumeration over this type's child relation names
   */
  public QNameIterator relationNames();

  /**
   * Returns the count of child relationNames defined by this type
   * @return the count of child relationNames defined by this type
   */
  public int relationCount();

  /**
   * Returns index within key of attributeName or -1 if attributeName is not part of the key
   * @param attributeName the attribute's name
   * @return index within key of attributeName or -1 if attributeName is not part of the key
   */
  public int getKeyIndex(QName attributeName);

  /**
   * Returns the name of the ith key attribute
   * @param i the index of attribute within key
   * @return the name of the ith key attribute
   */
  public QName getKeyAttribute(int i);

  /**
   * Returns the type definition for the key attribute at index.
   *
   * @param index index of the key attribute
   * @return the type definition for the key attribute at index.
   */
  public SimpleType getKeyAttributeType(int index);

  /**
   * Returns the key reference with name refName
   *
   * @param refName name of the key reference
   * @return the key reference with name refName
   */
  public ModelReference getKeyReference( QName refName );

  /**
   * Returns an iterator over all key reference names
   * @return an iterator over all key reference names
   */
  public QNameIterator keyRefNames();

  /**
   * Create array to store attributes defined by this type.
   * Needed by XML parser.
   * @return object array for attributes with matching size for this type
   */
  public Object[] createAttributeArray();
}
