/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types;

import de.pidata.log.Logger;
import de.pidata.models.tree.ValidationException;
import de.pidata.qnames.QName;

public abstract class AbstractType implements Type {

  protected Class  valueClass;
  private   QName  typeID;
  private   Type   parentType;

  /**
   * Creates a new Type instance and tries to instantiate valueClassName.
   * @param typeID         the new Type's ID
   * @param valueClassName the name of this class to use for this Type's values
   * @param parentType     the new Type's parent type
   */
  public AbstractType(QName typeID, String valueClassName, Type parentType) {
    this(typeID);
    init(valueClassName, parentType);
  }

  protected AbstractType(QName typeID, Class valueClass, Type parentType) {
    this(typeID);
    this.parentType = parentType;
    this.valueClass = valueClass;
  }

  /**
   * Creates a new Type instance. Do not forget to call init before using this Type!
   * @param typeID   the new Type's ID
   */
  protected AbstractType(QName typeID) {
    if (typeID == null) {
      throw new IllegalArgumentException("TypeID must not be null!");
    }
    this.typeID   = typeID;
  }

  /**
   * Initializes this type. Called automatically bv public constructor.
   * @param valueClassName the name of this class to use for this Type's values
   * @param parentType     the new Type's parent type
   */
  protected void init(String valueClassName, Type parentType) {
    this.parentType = parentType;
    if (valueClassName != null) {
      try {
        this.valueClass = Class.forName(valueClassName);
      }
      catch (ClassNotFoundException e) {
        Logger.warn("AbstractType: value class not found: " + valueClassName);
      }
    }
  }

  /**
   * Returns this Type's ID.
   *
   * @return this Type's ID, never null
   */
  public QName name() {
    return this.typeID;
  }

  /**
   * Retunrs the Class to be used for values conforming to this Type
   *
   * @return the Class to be used for values conforming to this Type
   *
   */
  public Class getValueClass() {
    return this.valueClass;
  }

  /**
   * Returns this type's base type. Is null for base schema simpleTypes and
   * for complexTypes without base type definition.
   *
   * @return this type's parent type or null
   */
  public Type getBaseType() {
    return parentType;
  }

  /**
   * Checks if value is valid for this type.
   *
   * @param value       the value to be checked
   * @return NO_ERROR if value is valid, otherwise the errorID
   */
  public int checkValid(Object value) {
    if ((value == null) || (valueClass == value.getClass()) || (valueClass.isAssignableFrom( value.getClass() ))) {
      return ValidationException.NO_ERROR;
    }
    else {
      return ValidationException.ERROR_WRONG_CLASS;
    }
  }

  /**
   * Converts value to be compatible with this type. Use this method if checkValid returned
   * ValidationException.NEEDS_CONVERSION
   *
   * @param value the value to be converted
   * @return the converted value
   * @throws IllegalArgumentException if conversion is not possible
   */
  public Object convert( Object value ) {
    int valid = checkValid(value);
    if ((valid == ValidationException.NO_ERROR) || (valid == ValidationException.NEEDS_CONVERSION)) {
      return value;
    }
    else {
      throw new IllegalArgumentException( "Value='"+value+"' cannot be converted for type="+this );
    }
  }

  /**
   * Returns true if this type is the same or a base type (recursive)
   * of childType
   *
   * @param childType the type to check assignability
   * @return true if instances of childType can be assigned to fields of this type
   */
  public boolean isAssignableFrom(Type childType) {
    if (childType.name() == this.typeID) {
      return true;
    }
    Type baseType = childType.getBaseType();
    while (baseType != null) {
      if (baseType.name() == this.typeID) {
        return true;
      }
      baseType = baseType.getBaseType();
    }
    return false;
  }

  /**
   * Returns the message for the given errorID
   *
   * @param errorID the ID of the error for the requested message
   * @return the message for the given errorID
   */
  public String getErrorMessage(int errorID) {
    return createErrorMessage(errorID, this);
  }

  /**
   * Creates the error message for the given errorID nad type
   *
   * @param errorID the ID of the error for the requested message
   * @param type the type for which the error mesage is returned
   * @return the message for the given errorID and type
   */
  public static String createErrorMessage(int errorID, Type type) {
    if (errorID == ValidationException.ERROR_WRONG_CLASS) {
      return "The value's class was wrong, expected " + type.getValueClass().toString();
    }
    else if (errorID == ValidationException.ERROR_NOT_NULLABLE) {
      return "The value's class must not be null";
    }
    else if (errorID == ValidationException.ERROR_WRONG_TYPE) {
      return "The value's type was wrong, expected " + type.name();
    }
    else if (errorID == ValidationException.ERROR_ATTR_UNKNOWN) {
      return "Unknown Attribute";
    }
    else if (errorID == ValidationException.ERROR_ANY_ATTR_UNKNOWN) {
      return "Unknown AnyAttribute";
    }
    else if (errorID == ValidationException.ERROR_ATTR_READONLY) {
      return "Attribute is read only";
    }
    else if (errorID == ValidationException.ERROR_KEY_ATTR_READONLY) {
      return "Attribute is read only because ist's part of the key";
    }
    else if (errorID == ValidationException.ERROR_TOO_BIG) {
      return "Value is too big";
    }
    else if (errorID == ValidationException.ERROR_TOO_SMALL) {
      return "Value is too small";
    }
    else if (errorID == ValidationException.ERROR_NAN_NOT_ALLOW) {
      return "Value NAN is not allowed";
    }
    else if (errorID == ValidationException.ERROR_TOO_SHORT) {
      return "Value is too short";
    }
    else if (errorID == ValidationException.ERROR_TOO_LONG) {
      return "Value is too long";
    }
    else {
      return "Unknown error id=" + errorID;
    }
  }

  @Override
  public int hashCode() {
    return name().hashCode();
  }

  @Override
  public boolean equals( Object obj ) {
    if (obj == null) {
      return false;
    }
    else if (obj instanceof AbstractType) {
      AbstractType other = (AbstractType) obj;
      return this.name() == other.name();
    }
    else {
      return false;
    }
  }

  @Override
  public String toString() {
    if (typeID == null) {
      return "-- unnamed --";
    }
    return typeID.toString();
  }
}
