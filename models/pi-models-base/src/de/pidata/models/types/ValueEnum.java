/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types;

import de.pidata.qnames.QName;

/**
 * Interface for value enumerations like XML Schema enumerations or foreign key definitions.
 */
public interface ValueEnum {

  /**
   * Returns the path to the model containing the value enumeration
   * @return the path to the model containing the value enumeration
   */
  public String getPath();

  /**
   * Returns the relation name within model identified by path
   * @return the relation name within model identified by path
   */
  public QName getRelation();

  /**
   * Returns the name of the attribute to be used for displaying a
   * model of relation within a selection list.
   * @return the name of the display attribute
   */
  public QName getDisplayAttribute();
}
