/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types.complex;

import de.pidata.models.types.Relation;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

public class DefaultRelation implements Relation {

  private int  minOccurs;
  private int  maxOccurs;
  private Type childType;
  private QName relationID;
  private Class collection;
  private QName substitutionGroup;

  public DefaultRelation( QName relationID, Type childType, int minOccurs, int maxOccurs, Class collection, QName substitutionGroup ) {
    if (relationID == null) {
      throw new IllegalArgumentException("RelationID must not be null");
    }
    this.relationID = relationID;
    if (childType == null) {
      throw new IllegalArgumentException("Child type must not be null, relationID="+relationID);
    }
    this.childType = childType;
    this.minOccurs = minOccurs;
    this.maxOccurs = maxOccurs;
    this.collection = collection;
    this.substitutionGroup = substitutionGroup;
  }

  public Type getChildType() {
    return childType;
  }

  public int getMaxOccurs() {
    return maxOccurs;
  }

  public int getMinOccurs() {
    return minOccurs;
  }

  public QName getRelationID() {
    return relationID;
  }

  @Override
  public Class getCollection() {
    return collection;
  }

  @Override
  public QName getSubstitutionGroup() {
    return substitutionGroup;
  }
}
