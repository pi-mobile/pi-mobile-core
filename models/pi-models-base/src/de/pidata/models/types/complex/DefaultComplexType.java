/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types.complex;

import de.pidata.models.tree.*;
import de.pidata.models.types.AbstractType;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.QName;

import java.util.*;

/**
 * Complex type definition including type hierarchy.
 * For attributes all parent attributes ar counted first.
 * Method relationNames() also starts with parent realtion names.
 * Type checking accepts child types.
 */
public class DefaultComplexType extends AbstractType implements ComplexType {

  protected Hashtable childRelations = new Hashtable();
  private QName[]   keyAttributeNames = null;
  private List<SimpleType> attributeTypes = new ArrayList<SimpleType>();
  private List<QName>  attributeIDs = new ArrayList<QName>();
  private List<Object> attributeDefaults = new ArrayList<Object>();
  private AnyAttribute anyAttribute = null;
  private Hashtable keyReferences = new Hashtable();
  private boolean   abstractType;
  private boolean   mixedType;
  private SimpleType contentType;

  public DefaultComplexType(QName typeID, String modelClassName, int keyAttributeCount) {
    this( typeID, modelClassName, keyAttributeCount, null, false, false );
  }

  public DefaultComplexType( QName typeID, String modelClassName, int additionalKeyAttributeCount, Type parentType ) {
    this( typeID, modelClassName, additionalKeyAttributeCount, parentType, false, false );
  }

  public DefaultComplexType( QName typeID, String modelClassName, int additionalKeyAttributeCount, Type parentType, boolean abstractType, boolean mixedType ) {
    super( typeID, modelClassName, parentType );
    this.abstractType = abstractType;
    this.mixedType = mixedType;
    if (mixedType) {
      setContentType( null );
    }
    else {
      setContentType( StringType.getDefString() );
    }

    if (additionalKeyAttributeCount > 0) {
      keyAttributeNames = new QName[additionalKeyAttributeCount];
    }
  }

  /**
   * Returns true if this type can hold mixed content
   *
   * @return true if this type  can hold mixed content
   */
  @Override
  public boolean isMixed() {
    return mixedType;
  }

  /**
   * Returns true if this type is an abstract type
   *
   * @return true if this type is an abstract type
   */
  @Override
  public boolean isAbstract() {
    return abstractType;
  }

  /**
   * Returns either null (for mixed content) 
   * or the type to be used for this type's content (see Model.getContent()).
   * @return the content type or null
   */
  @Override
  public SimpleType getContentType() {
    return this.contentType;
  }

  protected void setContentType( SimpleType contentType ) {
    this.contentType = contentType;
  }

  //---------------------------------------------------------------------------------
  // Attribute operations
  //---------------------------------------------------------------------------------

  /**
   * Returns the number of attributes forming this model's key
   * beginning with attribute at index 0. Result will be 0 or 1
   * in most cases. The attribute list must be ordered, so that
   * all attributes being part of the key are coming first.
   *
   * @return the number of key attributes
   */
  public int keyAttributeCount() {
    int count;
    if (keyAttributeNames == null) {
      count = 0;
    }
    else {
      count = this.keyAttributeNames.length;
    }
    Type basetype = getBaseType();
    if (basetype instanceof ComplexType) {
      count += ((ComplexType) basetype).keyAttributeCount();
    }
    return count;
  }

  /**
   * Returns the number of attributes for Objects using this type
   *
   * @return the number of attributes for Objects using this type
   */
  public int attributeCount() {
    int count = this.attributeTypes.size();
    Type basetype = getBaseType();
    if (basetype instanceof ComplexType) {
      count += ((ComplexType) basetype).attributeCount();
    }
    return count;
  }

  /**
   * Create array to store attributes defined by this type.
   * Needed by XML parser.
   * @return object array for attributes with matching size for this type
   */
  @Override
  public Object[] createAttributeArray() {
    return new Object[attributeCount()];
  }

  /**
   * Returns the type definition for the attribute at index.
   *
   * @param index index of the attribute
   * @return the type definition for the attribute at index.
   */
  public SimpleType getAttributeType(int index) {
    int firstIndex = 0;
    Type basetype = getBaseType();
    if (basetype instanceof ComplexType) {
      firstIndex = ((ComplexType) basetype).attributeCount();
    }
    if (index >= firstIndex) {
      return this.attributeTypes.get( index - firstIndex );
    }
    else {
      return ((ComplexType) basetype).getAttributeType(index);
    }
  }

  /**
   * Returns the default value for the attribute at index or null if
   * a default is not defined.
   *
   * @param index index of the attribute
   * @return default value for the attribute at index or null
   */
  @Override
  public Object getAttributeDefault( int index ) {
    int firstIndex = 0;
    Type basetype = getBaseType();
    if (basetype instanceof ComplexType) {
      firstIndex = ((ComplexType) basetype).attributeCount();
    }
    if (index >= firstIndex) {
      return this.attributeDefaults.get( index - firstIndex );
    }
    else {
      return ((ComplexType) basetype).getAttributeDefault( index );
    }
  }

  /**
   * Returns the value of this Model's attribute with attributeID.
   *
   * @return the value for attributeID or null if attributeID does not exist
   */
  public SimpleType getAttributeType(QName attributeID) {
    int index = indexOfAttribute(attributeID);
    if (index < 0) return null;
    else return getAttributeType(index);
  }

  /**
   * Returns the ID (name) for the attribute at index.
   *
   * @param index index of the attribute
   * @return the ID (name) for the attribute at index.
   */
  public QName getAttributeName(int index) {
    int firstIndex = 0;
    Type basetype = getBaseType();
    if (basetype instanceof ComplexType) {
      firstIndex = ((ComplexType) basetype).attributeCount();
    }
    if (index >= firstIndex) {
      return this.attributeIDs.get( index - firstIndex );
    }
    else {
      return ((ComplexType) basetype).getAttributeName(index);
    }
  }

  /**
   * Returns the index of the attribute identified by attributeID.
   * This may be used to store attributes in an array. The order
   * of attributes depends on the order in the type definition
   *
   * @param attributeID the attribute's ID
   * @return the index of attributeID or -1 if attributeID does
   *         not exist
   */
  public int indexOfAttribute(QName attributeID) {
    if (attributeID == null) {
      return -1;
    }
    Type basetype = getBaseType();
    int index = this.attributeIDs.indexOf(attributeID);
    if (index < 0) {
      if (basetype instanceof ComplexType) {
        return ((ComplexType) basetype).indexOfAttribute(attributeID);
      }
      else {
        return -1;
      }
    }
    else {
      if (basetype instanceof ComplexType) {
        index += ((ComplexType) basetype).attributeCount();
      }
      return index;
    }
  }

  /**
   * Adds a attribute definition to this complex type.
   * @param attributeName  the ID for children of attributeType
   * @param attributeType  the type definition for attributeName
   * @param attrDefault    the default value for attributeName
   */
  public void addAttributeType( QName attributeName, SimpleType attributeType, Object attrDefault ) {
    if (attributeType instanceof AnyAttribute) {
      setAnyAttribute((AnyAttribute) attributeType);
    }
    else {
      this.attributeIDs.add( attributeName );
      this.attributeTypes.add( attributeType );
      this.attributeDefaults.add( attrDefault );
    }  
  }

  /**
   * Adds a attribute definition to this complex type with default value null
   * @param attributeName  the ID for children of attributeType
   * @param attributeType  the type definition for attributeName
   */
  public void addAttributeType( QName attributeName, SimpleType attributeType ) {
    addAttributeType( attributeName, attributeType, null );
  }

  /**
   * Adds a attribute definition to this complex type and calls
   * setKeyAttribute(keyIndex, attributeName).
   * @param attributeName    the ID for children of attributeType
   * @param attributeType  the type definition for attributeName
   * @param attrDefault    the default value for attributeName
   */
  public void addKeyAttributeType( int keyIndex, QName attributeName, SimpleType attributeType, Object attrDefault ) {
    addAttributeType( attributeName, attributeType, attrDefault );
    setKeyAttribute( keyIndex, attributeName );
  }

  /**
   * Adds a attribute definition to this complex type  with default value null
   * and calls setKeyAttribute(keyIndex, attributeName).
   * @param attributeName    the ID for children of attributeType
   * @param attributeType  the type definition for attributeName
   */
  public void addKeyAttributeType( int keyIndex, QName attributeName, SimpleType attributeType ) {
    addKeyAttributeType( keyIndex, attributeName, attributeType, null );
  }

  /**
   * Defines the given attributeName as part of the key attributes.
   * @param index         index of the attribute within key
   * @param attributeName the name of the key attribute
   */
  public void setKeyAttribute(int index, QName attributeName) {
    int attrIndex = indexOfAttribute( attributeName );
    if (attrIndex < 0) {
      throw new IllegalArgumentException( "Key attribute must be an attribute of this type, name=" + attributeName );
    }
    if ((index < 0) || (index >= keyAttributeCount())) {
      throw new IllegalArgumentException( "Key index is out of range: index=" + index );
    }
    if (getKeyAttribute( index ) != null) {
      throw new IllegalArgumentException( "Must not redefine key attribute, key index=" + index );
    }
    doSetKeyAttribute( index, attributeName );
  }

  private void doSetKeyAttribute( int index, QName attributeName ) {
    int firstIndex = 0;
    ComplexType basetype = (ComplexType) getBaseType();
    if (basetype != null) {
      firstIndex = basetype.keyAttributeCount();
    }
    if (index >= firstIndex) {
      this.keyAttributeNames[index - firstIndex] = attributeName;
    }
    else {
      ((DefaultComplexType) basetype).doSetKeyAttribute( index, attributeName );
    }
  }

  /**
   * Returns index within key of attributeName or -1 if attributeName is not part of the key
   * @param attributeName the attribute's name
   * @return index within key of attributeName or -1 if attributeName is not part of the key
   */
  public int getKeyIndex(QName attributeName) {
    if (attributeName == null) {
      return -1;
    }
    Type basetype = getBaseType();
    int index = -1;
    if (keyAttributeNames != null) {
      for (int i = 0; i < keyAttributeNames.length; i++) {
        if (keyAttributeNames[i] == attributeName) {
          index = i;
          break;
        }
      }
    }
    if (index < 0) {
      if (basetype instanceof ComplexType) {
        return ((ComplexType) basetype).getKeyIndex( attributeName );
      }
      else {
        return -1;
      }
    }
    else {
      if (basetype instanceof ComplexType) {
        index += ((ComplexType) basetype).keyAttributeCount();
      }
      return index;
    }
  }

  /**
   * Returns the name of the ith key attribute
   *
   * @param i the index of attribute within key
   * @return the name of the ith key attribute
   */
  public QName getKeyAttribute(int i) {
    int firstIndex = 0;
    ComplexType basetype = (ComplexType) getBaseType();
    if (basetype != null) {
      firstIndex = basetype.keyAttributeCount();
    }
    if (i >= firstIndex) {
      return this.keyAttributeNames[i - firstIndex];
    }
    else {
      return basetype.getKeyAttribute( i );
    }
  }

  /**
   * Returns the type definition for the attribute at index.
   *
   * @param index index of the attribute
   * @return the type definition for the attribute at index.
   */
  public SimpleType getKeyAttributeType(int index) {
    return getAttributeType(getKeyAttribute(index));
  }

  /**
   * Returns this types anyAttribiute definition or null if not present
   *
   * @return this types anyAttribiute definition or null
   */
  public AnyAttribute getAnyAttribute() {
    if (this.anyAttribute != null) {
      return this.anyAttribute;
    }
    else {
      ComplexType basetype = (ComplexType) getBaseType();
      if (basetype != null) {
        return basetype.getAnyAttribute();
      }
      else {
        return null;
      }
    }
  }

  /**
   * Sets this types anyAttribute definition
   * @param anyAttribute the new anyAttribute definition
   * @throws IllegalArgumentException if anyAttribute is already defined
   */
  public void setAnyAttribute(AnyAttribute anyAttribute) {
    this.anyAttribute = anyAttribute;
  }

  //---------------------------------------------------------------------------------
  // Child operations
  //---------------------------------------------------------------------------------

  /**
   * Adds a child relation to this type definition.
   *
   * @param relationID  ID of the child relation to add
   * @param childType  the element definition for childID
   * @param minOccurs  the minimum occurcence of childtype in given relation
   * @param maxOccurs  the maximum occurcence of childtype in given relation
   */
  public void addRelation( QName relationID, Type childType, int minOccurs, int maxOccurs ) {
    addRelation( relationID, childType, minOccurs, maxOccurs, Collection.class, null );
  }

  public void addRelation( QName relationID, Type childType, int minOccurs, int maxOccurs, Class collection ) {
    addRelation( relationID, childType, minOccurs, maxOccurs, collection, null );
  }

  /**
   * Adds a child relation to this type definition.
   *
   * @param relationID  ID of the child relation to add
   * @param childType  the element definition for childID
   * @param minOccurs  the minimum occurcence of childtype in given relation
   * @param maxOccurs  the maximum occurcence of childtype in given relation
   */
  public void addRelation( QName relationID, Type childType, int minOccurs, int maxOccurs, Class collection, QName substitutionGroup ) {
    DefaultRelation relation = new DefaultRelation( relationID, childType, minOccurs, maxOccurs, collection, substitutionGroup );
    addRelation(relation);
  }

  /**
   * Adds a child relation to this type definition.
   *
   * @param relation  the child relation to add
   */
  public void addRelation(Relation relation) {
    this.childRelations.put(relation.getRelationID(), relation);
  }

  /**
   * Returns the child relation identified by relationID or null if relationID is
   * not defined.
   *
   * @param relationID ID of the child relation for which type information is requested
   * @return the child relation identified by relationID
   */
  public Relation getRelation(QName relationID) {
    Relation result = (Relation) this.childRelations.get(relationID);
    if (result == null) {
      ComplexType basetype = (ComplexType) getBaseType();
      if (basetype != null) {
        result = basetype.getRelation(relationID);
      }
    }
    return result;
  }

  /**
   * Returns the type definition for the child identified by relationID.
   * If none can be found a relation for 'any' is looked up. If such a
   * relation can be found the type definition of the element is taken
   * from the Root element, i.e. every element, that is used at an 'any'
   * element's position needs to be globally declared in the Root instance.
   *
   * @param relationID  ID of the child relation for which type information is requested
   * @return the type definition for the child identified by childID
   */
  public Type getChildType( QName relationID ) {
    Relation relation = getRelation( relationID );
    if (relation != null) {
      return relation.getChildType();
    }

    //--- test for substitution group
    Relation rootRelation = ModelFactoryTable.findRootRelation( relationID );
    if (rootRelation == null) {
      throw new IllegalArgumentException( "Unknown relation ID=" + relationID + ", type=" + name() );
    }
    QName substGroup = rootRelation.getSubstitutionGroup();
    if (substGroup != null) {
      relation = getRelation( substGroup );
      if (relation != null) {
        // if everything is valid return the original type for the relation in question
        return rootRelation.getChildType();
      }
    }

    //--- test for any element
    relation = getRelation( AnyElement.ANY_ELEMENT );
    if (relation == null) {
      // TODO: check if eventually this is a valid substitution... throw new IllegalArgumentException( "Unknown relation ID=" + relationID + ", type=" + name() );
    }
    Type type = rootRelation.getChildType();
    if (type == null) {
      throw new IllegalArgumentException( "Unknown relation ID=" + relationID + ", type=" + name() );
    }
    return type;
  }

  /**
   * Checks if this type allows child having childDef to be added to a model
   * having this type.
   *
   * @param relationID  ID of the child relation for which checking is done
   * @param childType the child definition to be checked
   * @return NO_ERROR if child is allowed, otherwise the error number
   */
  public int allowsChild(QName relationID, Type childType) {
    if (relationID == CDATA) {
      if (isMixed()) {
        if (childType == StringType.getDefString()) {
          return ValidationException.NO_ERROR;
        }
        else {
          return ValidationException.ERROR_WRONG_TYPE;
        }
      }
      else {
        return ValidationException.MIXED_CONTENT_NOT_ALLOWED;
      }
    }
    Type requiredType = getChildType(relationID);
    if (requiredType.isAssignableFrom( childType )) {
      return ValidationException.NO_ERROR;
    }
    else {
      return ValidationException.ERROR_WRONG_TYPE;
    }  
  }

  /**
   * Returns an Enumeration over this type's child relation names
   *
   * @return an Enumeration over this type's child relation names
   */
  public QNameIterator relationNames() {
    ComplexType basetype = (ComplexType) getBaseType();
    if (basetype == null) {
      return new QNameIteratorEnum(this.childRelations.keys());
    }
    else {
      return new QNameIteratorEnum(this.childRelations.keys(), basetype.relationNames());
    }
  }

  /**
   * Returns the count of child relationNames defined by this type
   * @return the count of child relationNames defined by this type
   */
  public int relationCount() {
    int size = childRelations.size();
    ComplexType basetype = (ComplexType) getBaseType();
    if (basetype != null) {
      size += basetype.relationCount();
    }
    return size;
  }

  //---------------------------------------------------------------------------------
  // Reference operations
  //---------------------------------------------------------------------------------

  /**
   * Adds the given reference to this type definition. A existing reference
   * with the same name will be replaced.
   * @param reference the reference to add to this type
   */
  public void addKeyReference( ModelReference reference ) {
    if (keyReferences == null) {
      keyReferences = new Hashtable();
    }
    this.keyReferences.put( reference.getName(), reference );
  }

  public void addKeyReference( QName name,QName refTypeName, QName refAttribute ) {
    DefaultReference reference = new DefaultReference( name, refTypeName, refAttribute );
    addKeyReference( reference );
  }

  /**
   * Returns the key reference with name refName
   *
   * @param refName name of the key reference
   * @return the key reference with name refName
   */
  public ModelReference getKeyReference( QName refName ) {
    if (keyReferences == null) {
      return null;
    }
    else {
      return (ModelReference) keyReferences.get( refName );
    }
  }

  /**
   * Returns an iterator over all key reference names
   *
   * @return an iterator over all key reference names
   */
  public QNameIterator keyRefNames() {
    if (this.keyReferences == null) {
      return QNameIteratorEnum.EMPTY_ITERATOR;
    }
    else {
      return new QNameIteratorEnum( this.keyReferences.keys() );
    }
  }
}
