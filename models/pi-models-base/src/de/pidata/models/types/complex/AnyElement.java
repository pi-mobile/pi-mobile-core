/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types.complex;

import de.pidata.models.tree.*;
import de.pidata.models.types.AbstractType;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

public class AnyElement extends AbstractType implements ComplexType {

  /**
   * Constant used for XML-Schema 'any' element type's name.
   */
  public static final QName ANY_ELEMENT = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" ).getQName( "any" );

  /**
   * Constant for XML-Schema 'any' element type.
   */
  public static final AnyElement ANY_TYPE = new AnyElement();

  public AnyElement() {
    super(ANY_ELEMENT, new Object().getClass(), null);
  }

  /**
   * Returns true if this type can hold mixed content
   *
   * @return true if this type  can hold mixed content
   */
  @Override
  public boolean isMixed() {
    return true; // TODO?
  }

  /**
   * Returns true if this type is an abstract type
   *
   * @return true if this type is an abstract type
   */
  @Override
  public boolean isAbstract() {
    return true;
  }

  /**
   * Returns null because AnyElement always allows mixed content
   * @return null
   */
  @Override
  public SimpleType getContentType() {
    return StringType.getDefString();
  }

  public Type fetchType( QName relationID) {
    Relation rootRel = ModelFactoryTable.findRootRelation( relationID );
    if (rootRel == null) {
      return null;
    }
    else {
      return rootRel.getChildType();
    }
  }

  /**
   * Returns the number of attributes forming this model's key
   * beginning with attribute at index 0. Result will be 0 or 1
   * in most cases. The attribute list must be ordered, so that
   * all attributes beeing part of the key are coming frist.
   *
   * @return the nmumber of key attributes
   */
  public int keyAttributeCount() {
    return 0;
  }

  /**
   * Defines the given attributeName as part of the key attributes
   *
   * @param index         index of the attribute witing key
   * @param attributeName the name of the key attribute
   */
  public void setKeyAttribute(int index, QName attributeName) {
    throw new IllegalArgumentException("setKeyAttribute() not supportet by anyElement");
  }

  /**
   * Returns index within key of attributeName or -1 if attributeName is not part of the key
   *
   * @param attributeName the attribute's name
   * @return index within key of attributeName or -1 if attributeName is not part of the key
   */
  public int getKeyIndex(QName attributeName) {
    return -1;
  }

  /**
   * Returns the name of the ith key attribute
   *
   * @param i the index of attribute within key
   * @return the name of the ith key attribute
   */
  public QName getKeyAttribute(int i) {
    throw new ArrayIndexOutOfBoundsException("AnyElement has no key attributes");
  }

  /**
   * Returns the type definition for the key attribute at index.
   *
   * @param index index of the key attribute
   * @return the type definition for the key attribute at index.
   */
  public SimpleType getKeyAttributeType(int index) {
    throw new ArrayIndexOutOfBoundsException("AnyElement has no key attributes");
  }

  /**
   * Returns the number of attributes for Objects using this type
   *
   * @return the number of attributes for Objects using this type
   */
  public int attributeCount() {
    return 0;
  }

  /**
   * Create array to store attributes defined by this type.
   * Needed by XML parser.
   * @return object array for attributes with matching size for this type
   */
  @Override
  public Object[] createAttributeArray() {
    return new Object[attributeCount()];
  }

  /**
   * Returns the type definition for the attribute at index.
   *
   * @param index index of the attribute
   * @return the type definition for the attribute at index.
   */
  public SimpleType getAttributeType(int index) {
    throw new ArrayIndexOutOfBoundsException("AnyElement has no attribute type definition");
  }

  /**
   * Returns the default value for the attribute at index or null if
   * a default is not defined.
   *
   * @param index index of the attribute
   * @return default value for the attribute at index or null
   */
  @Override
  public Object getAttributeDefault( int index ) {
    throw new ArrayIndexOutOfBoundsException("AnyElement has no attribute default definition");
  }

  /**
   * Returns the ID (name) for the attribute at index.
   *
   * @param index index of the attribute
   * @return the ID (name) for the attribute at index.
   */
  public QName getAttributeName(int index) {
    throw new ArrayIndexOutOfBoundsException("AnyElement has no attribute type definition");
  }

  /**
   * Returns the index of the attribute identified by attributeID.
   * This may be used to store attributes in an array. The order
   * of attributes depends on the order in the type definition
   *
   * @param attributeID the attribute's ID
   * @return the index of attributeID or -1 if attributeID does
   *         not exist
   */
  public int indexOfAttribute(QName attributeID) {
    return -1;
  }

  /**
   * Returns this types anyAttribiute definition or null if not present
   *
   * @return this types anyAttribiute definition or null
   */
  public AnyAttribute getAnyAttribute() {
    return null;
  }

  /**
   * Returns the type definition for the child relation identified by relationID
   *
   * @param relationID ID of the child relation for which type information is requested
   * @return the type definition for the child relation identified by relationID
   */
  public Type getChildType(QName relationID) {
    return null;
  }

  /**
   * Returns the child relation identified by relationID or null if relationID is
   * not defined.
   *
   * @param relationID ID of the child relation for which type information is requested
   * @return the child relation identified by relationID
   */
  public Relation getRelation(QName relationID) {
    return null;
  }

  /**
   * Checks if this type allows child having childDef to be added to a model
   * having this type.
   *
   * @param relationID ID of the child relation for which checking is done
   * @param childType  the child definition to be checked
   * @return NO_ERROR if child is allowed, otherwise the error number
   */
  public int allowsChild(QName relationID, Type childType) {
    return ValidationException.ERROR_WRONG_TYPE;
  }

  /**
   * Returns an Enumeration over this type's child types
   *
   * @return an Enumeration over this type's child types
   */
  public QNameIterator relationNames() {
    return QNameIteratorEnum.EMPTY_ITERATOR;
  }

  /**
   * Returns the count of child relationNames defined by this type
   *
   * @return the count of child relationNames defined by this type
   */
  public int relationCount() {
    return 0;
  }


  /**
   * Returns true if this type is the same or a base type (recursive)
   * of childType
   *
   * @param childType the type to check assignability
   * @return true if instances of childType can be assigned to fields of this type
   */
  public boolean isAssignableFrom( Type childType ) {
    return true;
  }

  /**
   * Returns the key reference with name refName
   *
   * @param refName name of the key reference
   * @return the key reference with name refName
   */
  public ModelReference getKeyReference( QName refName ) {
    return null;
  }

  /**
   * Returns an iterator over all key reference names
   *
   * @return an iterator over all key reference names
   */
  public QNameIterator keyRefNames() {
    return QNameIteratorEnum.EMPTY_ITERATOR;
  }
}
