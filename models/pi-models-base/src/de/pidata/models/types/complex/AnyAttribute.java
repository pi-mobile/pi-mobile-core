/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types.complex;

import de.pidata.models.tree.ModelFactory;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.models.types.AbstractType;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.NamespaceTable;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.ValueEnum;
import de.pidata.qnames.QName;

public class AnyAttribute extends AbstractType implements SimpleType {

  public static final QName ANY_ATTRIBUTE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" ).getQName( "anyAttribute" );

  /**
   * Constant for XML-Schema 'anyAttribute' element type.
   */
  public static final AnyAttribute ANY_ATTR_TYPE = new AnyAttribute(null);

  private Namespace[] namespaces;

  public AnyAttribute(Namespace[] namespaces) {
    super(ANY_ATTRIBUTE, new Object().getClass(), null);
    this.namespaces = namespaces;
  }

  /**
   * Returns this simple type's root type by recursively calling getBaseType().
   *
   * @return this simple type's root type
   */
  public SimpleType getRootType() {
    return this;
  }

  @Override
  public SimpleType getContentType() {
    return this;
  }

  /**
   * Returns the type for the given attrName
   * @param attrName the attribute's name
   * @return the type for the given attrName
   * @throws IllegalArgumentException if factory for that type was not found 
   */
  public static SimpleType findType(QName attrName) {
    Namespace ns = attrName.getNamespace();
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( ns );
    if (factory == null) {
      throw new IllegalArgumentException("No factory found for attribute name="+attrName);
    }
    return factory.getAttribute(attrName);
  }

  public int namespaceCount() {
    if (namespaces == null) return 0;
    else return namespaces.length;
  }

  public Namespace getNamespace(int index) {
    return namespaces[index];
  }

  /**
   * Creates a value for this type by parsing the given stringValue
   *
   * @param stringValue string representation of the value to be created, may be null
   * @param namespaces
   * @return a value for this type by parsing the given stringValue
   */
  public Object createValue( String stringValue, NamespaceTable namespaces ) {
    return null;
  }

  /**
   * Creates a default value for this type.
   *
   * @return a default value for this type
   */
  public Object createDefaultValue() {
    return null;
  }

  /**
   * Checks if this anyAttribute definition allows the namespace ns
   * @param ns the namespace to be checked
   * @return true if this anyAttribute definition allows the namespace ns
   */
  public boolean allowsNamespace(Namespace ns) {
    if (namespaces == null) {
      return true;
    }
    else {
      for (int i = 0;  i < namespaces.length; i++) {
        if (namespaces[i] == ns) return true;
      }
      return false;
    }
  }

  /**
   * Returns this type's value enumeration or null
   *
   * @return this type's value enumeration or null
   */
  public ValueEnum getValueEnum() {
    return null;
  }
}
