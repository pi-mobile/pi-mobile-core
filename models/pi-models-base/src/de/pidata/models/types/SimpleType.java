/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types;

import de.pidata.qnames.NamespaceTable;

/**
 * Simple type like defined in XML Schema. This type has no attributes.
 */
public interface SimpleType extends Type {
  /**
   * Creates a default value for this type.
   *
   * @return a default value for this type
   */
  public Object createDefaultValue();
  //TODO der Default value gehört eigentlich zur Attribut-/Element-Definition

  /**
   * Creates a value for this type by parsing the given stringValue
   * @param stringValue string representation of the value to be created, may be null
   * @param namespaces
   * @return a value for this type by parsing the given stringValue
   */
  public Object createValue( String stringValue, NamespaceTable namespaces );

  /**
   * Returns this simple type's root type by recursively calling getBaseType().
   *
   * @return this simple type's root type
   */
  public SimpleType getRootType();

  /**
   * Returns this type's value enumeration or null
   * @return this type's value enumeration or null
   */
  public ValueEnum getValueEnum();
}
