/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.types;

/**
 * Interface for all types being countable.
 */
public interface Ordered {
  /**
   * Returns the minimum value for this type
   * @return the minimum value for this type
   */
  public Object getMin();

  /**
   * Returns the maximum value for this type
   * @return the maximum value for this type
   */
  public Object getMax();

  /**
   * Returns the next larger value of this type or null if
   * value is equal to getMax()
   * @param value the next larger value of this value is returned
   * @return the next larger value of this type or null
   */
  public Object next(Object value);

  /**
   * Returns the next lesser value of this type or null if
   * value is equal to getMin()
   * @param value the next lesser value of this value is returned
   * @return the next lesser value of this type or null
   */
  public Object prev(Object value);
}
