/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.crypt;

import de.pidata.log.Logger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Provides a default SHA-1 implementation using the basic {@link MessageDigest}
 * which should be implemented within all Java platforms.
 * <br/>
 * Inspired from http://www.sha1-online.com/sha1-java/
 */
public class DefaultSHA1Encrypter implements Encrypter {
  private final MessageDigest crypt;

  public DefaultSHA1Encrypter() throws NoSuchAlgorithmException {
    crypt = MessageDigest.getInstance( "SHA-1" );
  }

  @Override
  public String encrypt( String text ) {
    crypt.reset();
    byte[] bytes = crypt.digest( text.getBytes() );
    return byteArrayToHexString( bytes );
  }


  @Override
  public String decrypt( String text ) {
    throw new IllegalStateException( "decrpyt not supported by SHA-1" );
  }

  public static String byteArrayToHexString( byte[] b ) {
    String result = "";
    for (int i = 0; i < b.length; i++) {
      result += Integer.toString( (b[i] & 0xff) + 0x100, 16 ).substring( 1 );
    }
    return result;
  }

  public static void main(String[] args) {
    try {
      DefaultSHA1Encrypter sha1Encrypter = new DefaultSHA1Encrypter();

      // samples from old lcrypto
      String  testVec1 = "";
      String  resVec1 = "da39a3ee5e6b4b0d3255bfef95601890afd80709";

      String  testVec2 = "61";
      String  resVec2 = "6c1e671f9af5b46d9c1a52067bdf0e53685674f7"; // old crypt: "86f7e437faa5a7fce15d1ddcb9eaeaea377667b8";

      String  testVec3 = "hello world";
      String  resVec3 = "2aae6c35c94fcfb415dbe95f408b9ce91ee846ed"; // old crypt: "6ade8ee129c93e51461ceac21b977cdbabc32afd";

      String resStr;

      resStr = sha1Encrypter.encrypt( testVec1 );
      if (resStr.equals( resVec1 )) {
        Logger.info( "encrypt succeeded for " + testVec1 );
      }
      else {
        Logger.error( "encrypt failed for " + testVec1 );
      }

      resStr = sha1Encrypter.encrypt( testVec2 );
      if (resStr.equals( resVec2 )) {
        Logger.info( "encrypt succeeded for " + testVec2 );
      }
      else {
        Logger.error( "encrypt failed for " + testVec2 );
      }

      resStr = sha1Encrypter.encrypt( testVec3 );
      if (resStr.equals( resVec3 )) {
        Logger.info( "encrypt succeeded for " + testVec3 );
      }
      else {
        Logger.error( "encrypt failed for " + testVec3 );
      }

    }
    catch (Exception ex) {
      Logger.error( "error", ex );
    }
  }
}
