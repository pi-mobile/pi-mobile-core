/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.crypt;

import de.pidata.log.Logger;

import java.security.NoSuchAlgorithmException;

/**
 * Simple Helper to create an encrypted string. Encrypts the given text
 * and prints the result.
 * <p/>
 * Please extend this as soon as more encrypt algorithms are supported.
 */
public class EncryptHelper {

  public static void main( String[] args ) {
    String encryptFkt;
    String text;

    if (args.length < 1) {
      System.out.println( "usage: EncryptHelper <text2encrypt>" );
      System.exit( 1 );
    }

    try {
      String text2encrypt = args[0];
      Encrypter encrypter = new DefaultSHA1Encrypter();
      String encryptedText = encrypter.encrypt( text2encrypt );
      System.out.println( encryptedText );
    }
    catch (NoSuchAlgorithmException ex) {
      String msg = "Could not create encrypter";
      Logger.error( msg, ex );
      throw new IllegalArgumentException( msg );
    }
  }
}
