/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.qnames.QName;

/**
 * Instances of this class define a job for reference resolving. Such jobs are
 * created while binding e.g. of xml data. After binding has finished Root
 * resolves all ResolveJobs registered while binding.
 */
public class ResolveJob {

  private QName attributeID;
  private QName reference;
  private Model owner;

  /**
   * Creates a new resolve job. After creation this job should be added to the
   * Root model is added to.
   * @param owner       the model which needs reference resolving
   * @param attributeID the ID of the attribute to be resolved
   * @param reference   the string to be converted into a model reference
   */
  public ResolveJob(Model owner, QName attributeID, QName reference) {
    this.attributeID = attributeID;
    this.reference = reference;
    this.owner = owner;
  }

  public QName getAttributeID() {
    return attributeID;
  }

  public QName getReference() {
    return reference;
  }

  public Model getOwner() {
    return owner;
  }
}
