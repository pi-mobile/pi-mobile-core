/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.qnames.QName;

/**
 * Compares two models by the values of an attribute.
 */
public class ComparatorNumber implements Comparator<Model> {

  private QName attributeName;
  private XPath modelPath;

  /**
   * Creates a new number comparator
   * @param attributeName the attrbiute containing a number value to use for comparision
   */
  public ComparatorNumber( QName attributeName ) {
    this( null, attributeName );
  }

  public ComparatorNumber( XPath modelPath, QName attributeName ) {
    if (attributeName == null) {
      throw new IllegalArgumentException( "attributeName must not be null" );
    }
    this.attributeName = attributeName;
    this.modelPath = modelPath;
  }

  /**
   * Compares models m1 and m2 by their values of attribute given via constructor.
   * Null is treated as zero
   *
   * @param m1 a model
   * @param m2 a model
   * @return -1 (m1 less m2), 0 (m1 equal m2) or 1 (m1 greater m2)
   * @throws IllegalArgumentException if models cannot be compared by this SortRole
   */
  public int compare( Model m1, Model m2 ) {
    long value1 = fetchValue( m1 );
    long value2 = fetchValue( m2 );
    if (value1 == value2) return 0;
    else if (value1 > value2) return 1;
    else return -1;
  }

  protected long fetchValue( Model model ) {
    if (modelPath != null) {
      model = modelPath.getModel( model, null );
      if (model == null) return 0;
    }
    Object attr1 = model.get( attributeName );
    if (attr1 == null) return 0;
    else if (attr1 instanceof Long) return ((Long) attr1).longValue();
    else if (attr1 instanceof Integer) return ((Integer) attr1).intValue();
    else if (attr1 instanceof Short) return ((Short) attr1).shortValue();
    else if (attr1 instanceof Byte) return ((Byte) attr1).byteValue();
    throw new IllegalArgumentException( "Unsupported value type, value="+attr1 );
  }
}
