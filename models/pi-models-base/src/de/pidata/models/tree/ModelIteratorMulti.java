package de.pidata.models.tree;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ModelIteratorMulti implements ModelIterator<Model> {

  private Model next;
  private List<ModelIterator<Model>> modelIteratorList = new ArrayList<>();
  private int iterIndex = 0;

  public ModelIteratorMulti( ModelIterator firstIter, ModelIterator secondIter ) {
    modelIteratorList.add( firstIter );
    modelIteratorList.add( secondIter );
    findNext();
  }

  /**
   * Returns an iterator over elements of type {@code T}.
   *
   * @return an Iterator.
   */
  @Override
  public Iterator<Model> iterator() {
    return this;
  }

  private void findNext() {
    while (iterIndex < modelIteratorList.size()) {
      ModelIterator iter = modelIteratorList.get( iterIndex );
      if (iter.hasNext()) {
        next = iter.next();
        return;
      }
      else {
        iterIndex++;
        next = null;
      }
    }
  }

  /**
   * Returns true if there is a further Model in this iteration, i.e. the next call to next()
   * will return a Model not null.
   *
   * @return true if there is a further Model in this iteration
   */
  public boolean hasNext() {
    return (next != null);
  }

  /**
   * Returns the next Model of this iterator and sets the internal pointer to the next Model.
   * Removing the returned Model from it's parent does NOT confuse this iterator.
   *
   * @return the next Model of this iteration
   * @throws IllegalArgumentException if there is no next Model in this iterator
   */
  public Model next() {
    Model current;
    if (next == null) {
      throw new IllegalArgumentException( "No more Elements in iterator." );
    }
    current = next;
    findNext();
    return current;
  }

  /**
   * Removes from the underlying collection the last element returned
   * by this iterator (optional operation).  This method can be called
   * only once per call to {@link #next}.  The behavior of an iterator
   * is unspecified if the underlying collection is modified while the
   * iteration is in progress in any way other than by calling this
   * method.
   *
   * @throws UnsupportedOperationException if the {@code remove}
   *                                       operation is not supported by this iterator
   * @throws IllegalStateException         if the {@code next} method has not
   *                                       yet been called, or the {@code remove} method has already
   *                                       been called after the last call to the {@code next}
   *                                       method
   * @implSpec The default implementation throws an instance of
   * {@link UnsupportedOperationException} and performs no other action.
   */
  @Override
  public void remove() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  public String toString() {
    return "ModelIteratorMulti iterIndex="+iterIndex+", next="+next;
  }
}

