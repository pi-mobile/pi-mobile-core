/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.qnames.QName;

import java.util.Enumeration;
import java.util.Iterator;

public class QNameIteratorEnum implements QNameIterator {

  public static QNameIteratorEnum EMPTY_ITERATOR = new QNameIteratorEnum(null);

  private Enumeration idEnum;
  private QNameIterator parentIter;

  /**
   * Creates a ner QNameIterator based on an Enumeration over QNames
   * @param idEnum the Enumeration to use as source for this iterator, may be null
   */
  public QNameIteratorEnum(Enumeration idEnum) {
    this.idEnum = idEnum;
  }

  /**
   * Creates a ner QNameIterator based on an Enumeration over QNames. Before
   * starting with idEnum parentIter ist iterated
   * @param idEnum the Enumeration to use as source for this iterator, may be null
   * @param parentIter the Iterator to start with (before idEnum)
   */
  public QNameIteratorEnum(Enumeration idEnum, QNameIterator parentIter) {
    this.idEnum = idEnum;
    this.parentIter = parentIter;
  }

  /**
   * Returns true if there is a further ID in this iteration, i.e. the next call to next()
   * will return a ID not null.
   *
   * @return true if there is a further ID in this iteration
   */
  public boolean hasNext() {
    if (parentIter != null) {
      if (parentIter.hasNext()) return true;
    }
    if (idEnum == null) return false;
    return idEnum.hasMoreElements();
  }

  /**
   * Returns the next ID of this iterator and sets the internal pointer to the next ID.
   *
   * @return the next ID of this iteration
   * @throws IllegalArgumentException if there is no next ID in this iterator
   */
  public QName next() {
    if (parentIter != null) {
      if(parentIter.hasNext()) return parentIter.next();
      else parentIter = null; //just to prevent this path from eval
    }
    return (QName) idEnum.nextElement();
  }

  /**
   * Returns an iterator over elements of type {@code T}.
   *
   * @return an Iterator.
   */
  @Override
  public Iterator<QName> iterator() {
    return this;
  }

  /**
   * Removes from the underlying collection the last element returned
   * by this iterator (optional operation).  This method can be called
   * only once per call to {@link #next}.  The behavior of an iterator
   * is unspecified if the underlying collection is modified while the
   * iteration is in progress in any way other than by calling this
   * method.
   *
   * @throws UnsupportedOperationException if the {@code remove}
   *                                       operation is not supported by this iterator
   * @throws IllegalStateException         if the {@code next} method has not
   *                                       yet been called, or the {@code remove} method has already
   *                                       been called after the last call to the {@code next}
   *                                       method
   * @implSpec The default implementation throws an instance of
   * {@link UnsupportedOperationException} and performs no other action.
   */
  @Override
  public void remove() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }
}
