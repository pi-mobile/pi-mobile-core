/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.simple.*;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;

import java.util.Hashtable;

public class XPathLocationStep extends XPathValue {

  public static Namespace NAMESPACE = Namespace.getInstance("de.pidata.models");

  public static QName TYPE_AXIS = NAMESPACE.getQName("axis" );
  public static QName TYPE_NODETEST = NAMESPACE.getQName("nodeTest" );

  //------ Axes --------------------------------------------------------------------------

  /**
   * abbreviated syntax for the 'self' - axis
   */
  public static QName AXIS_S_SELF = NAMESPACE.getQName("." );
  /**
   * abbreviated syntax for the 'parent' - axis
   */
  public static QName AXIS_S_PARENT = NAMESPACE.getQName(".." );
  /**
   * abbreviated syntax for the 'attribute' - axis
   */
  public static QName AXIS_S_ATTRIBUTE = NAMESPACE.getQName("@" );
  /**
   *  syntax for the 'variable' - axis
   */
  public static QName AXIS_VARIABLE = NAMESPACE.getQName("$" );
  /**
   * abbreviated syntax for the 'descendant-or-self::node()/' - expression
   */
  public static QName AXIS_S_DESCENDANT_OR_SELF = NAMESPACE.getQName("//" );
  /**
   * the child axis contains the children of the context node
   */
  public static QName AXIS_CHILD = NAMESPACE.getQName("child::" );
  /**
   * the descendant axis contains the descendants of the context node;
   * a descendant is a child or a child of a child and so on; thus the
   * descendant axis never contains attribute or namespace nodes
   */
  public static QName AXIS_DESCENDANT = NAMESPACE.getQName("descendant::" );
  /**
   * the parent axis contains the parent of the context node, if there is one
   */
  public static QName AXIS_PARENT = NAMESPACE.getQName("parent::" );
  /**
   * the ancestor axis contains the ancestors of the context node;
   * the ancestors of the context node consist of the parent of context node
   * and the parent's parent and so on; thus, the ancestor axis will always
   * include the root node, unless the context node is the root node
   */
  public static QName AXIS_ANCESTOR = NAMESPACE.getQName("ancestor::" );
  /**
   * the following-sibling axis contains all the following siblings of the
   * context node; if the context node is an attribute node or namespace node,
   * the following-sibling axis is empty
   */
  public static QName AXIS_FOLLOWING_SIBLING = NAMESPACE.getQName("following-sibling::" );
  /**
   * the preceding-sibling axis contains all the preceding siblings of the context node;
   * if the context node is an attribute node or namespace node, the preceding-sibling axis is empty
   */
  public static QName AXIS_PRECEDING_SIBLING = NAMESPACE.getQName("preceding-sibling::" );
  /**
   * the following axis contains all nodes in the same document as the context node
   * that are after the context node in document order, excluding any descendants and
   * excluding attribute nodes and namespace nodes
   */
  public static QName AXIS_FOLLOWING = NAMESPACE.getQName("following::" );
  /**
   * the preceding axis contains all nodes in the same document as the context
   * node that are before the context node in document order, excluding any ancestors
   * and excluding attribute nodes and namespace nodes
   */
  public static QName AXIS_PRECEDING = NAMESPACE.getQName("preceding::" );
  /**
   * the attribute axis contains the attributes of the context node;
   * the axis will be empty unless the context node is an element
   */
  public static QName AXIS_ATTRIBUTE = NAMESPACE.getQName("attribute::" );
  /**
   * the namespace axis contains the namespace nodes of the context node;
   * the axis will be empty unless the context node is an element
   */
  public static QName AXIS_NAMESPACE = NAMESPACE.getQName("namespace::" );
  /**
   * the self axis contains just the context node itself
   */
  public static QName AXIS_SELF = NAMESPACE.getQName("self::" );
  /**
   * the descendant-or-self axis
   * contains the context node and the descendants of the context node
   */
  public static QName AXIS_DESCENDANT_OR_SELF = NAMESPACE.getQName("descendant-or-self::" );
  /**
   * the ancestor-or-self axis contains the context node and the ancestors
   * of the context node; thus, the ancestor axis will always include the root node
   */
  public static QName AXIS_ANCESTOR_OR_SELF = NAMESPACE.getQName("ancestor-or-self::" );

  //------ Node Types --------------------------------------------------------------------------

  /**
   * Node type any
   */
  public static QName NODE_ANY = NAMESPACE.getQName("*" );
  /**
   * Comment - Node type
   */
  public static QName NODE_COMMENT = NAMESPACE.getQName("comment()" );
  /**
   * Processing-instruction - Node type
   */
  public static QName NODE_PROCESSING_INSTRUCTION = NAMESPACE.getQName("processing-instruction()");
  /**
   * Text - Node type
   */
  public static QName NODE_TEXT = NAMESPACE.getQName("text()" );
  /**
   * Node - Node type
   */
  public static QName NODE_NODE = NAMESPACE.getQName("node()" );

  //------ Other Keywords --------------------------------------------------------------------------

  /**
   * Variable - Reference keyword
   */
  public static QName KEYWORD_VARIABLE_REFERENCE = NAMESPACE.getQName("$" );
  /**
   * Path - Separator keyword
   */
  public static QName KEYWORD_PATH_SEPARATOR = NAMESPACE.getQName("/" );
  /**
   * Coma - Separator keyword
   */
  public static QName KEYWORD_COMA = NAMESPACE.getQName("," );
  /**
   * Left - Bracket keyword
   */
  public static QName KEYWORD_LEFT_BRACKET = NAMESPACE.getQName("[" );
  /**
   * Right - Bracket keyword
   */
  public static QName KEYWORD_RIGHT_BRACKET = NAMESPACE.getQName("]" );
  /**
   * Left - Parenthesis keyword
   */
  public static QName KEYWORD_LEFT_PARENTHESIS = NAMESPACE.getQName("(" );
  /**
   * Right - Parenthesis keyword
   */
  public static QName KEYWORD_RIGHT_PARENTHESIS = NAMESPACE.getQName(")" );
  /**
   * Help keyword, for tokenizing.
   */
  public static QName KEYWORD_ABBREVIATED_NOT_EQUAL = NAMESPACE.getQName("!" );

  /**
  * Path Pointer key word
  */
 public static QName KEYWORD_PATH_POINTER = NAMESPACE.getQName("^" );


 /** public access for validator implementation **/
  public static Hashtable keywords;

  static {
    keywords = new Hashtable();
    keywords.put(AXIS_S_SELF, TYPE_AXIS);
    keywords.put(AXIS_S_PARENT, TYPE_AXIS);
    keywords.put(AXIS_S_ATTRIBUTE, TYPE_AXIS);
    keywords.put(AXIS_S_DESCENDANT_OR_SELF, TYPE_AXIS);
    keywords.put(AXIS_CHILD, TYPE_AXIS);
    keywords.put(AXIS_DESCENDANT, TYPE_AXIS);
    keywords.put(AXIS_PARENT, TYPE_AXIS);
    keywords.put(AXIS_ANCESTOR, TYPE_AXIS);
    keywords.put(AXIS_FOLLOWING_SIBLING, TYPE_AXIS);
    keywords.put(AXIS_PRECEDING_SIBLING, TYPE_AXIS);
    keywords.put(AXIS_FOLLOWING, TYPE_AXIS);
    keywords.put(AXIS_PRECEDING, TYPE_AXIS);
    keywords.put(AXIS_ATTRIBUTE, TYPE_AXIS);
    keywords.put(AXIS_VARIABLE, TYPE_AXIS);
    keywords.put(AXIS_NAMESPACE, TYPE_AXIS);
    keywords.put(AXIS_SELF, TYPE_AXIS);
    keywords.put(AXIS_DESCENDANT_OR_SELF, TYPE_AXIS);
    keywords.put(AXIS_ANCESTOR_OR_SELF, TYPE_AXIS);

    keywords.put(NODE_ANY, TYPE_NODETEST);
    keywords.put(NODE_COMMENT, TYPE_NODETEST);
    keywords.put(NODE_PROCESSING_INSTRUCTION, TYPE_NODETEST);
    keywords.put(NODE_TEXT, TYPE_NODETEST);
    keywords.put(NODE_NODE, TYPE_NODETEST);
  }


  //--------------------------------------------------------------------------------

  private QName axis;
  private QName nodeTest;
  private XPathExpression predicate;
  private XPathLocationStep nextLocationStep;
  private boolean pointer = false;

  /**
   * Used by validator implementation.
   *
   * @return
   */
  public QName getAxis() {
    return axis;
  }

  /**
   * Used by validator implementation.
   *
   * @return
   */
  public QName getNodeTest() {
    return nodeTest;
  }

  /**
   * Used by validator implementation.
   *
   * @return
   */
  public XPathExpression getPredicate() {
    return predicate;
  }

  public XPathLocationStep( NamespaceTable namespaceTable ) {
    super( null, namespaceTable );
  }

  public XPathLocationStep( QName axis, QName nodeTest, XPathExpression predicate, NamespaceTable namespaceTable ) {
    super( null, namespaceTable );
    this.axis = axis;
    this.nodeTest = nodeTest;
    this.predicate = predicate;
  }

  public Model getResult( Model currentModel, Context context, boolean readOnly ) {
    Model result = SimpleReference.createContainer();
    evaluate( currentModel, context, result, readOnly );
    return result;
  }

  public Object getValue( Model currentModel, int index, Context context, boolean readOnly ) {
    if ((axis == AXIS_ATTRIBUTE) || (axis == AXIS_S_ATTRIBUTE)) {
      setAttributeNamespace( currentModel );
      return currentModel.get(nodeTest);
    }
    else {
      Model result = getResult( currentModel, context, readOnly );
      QName lastAxis = getLastAxis();
      if ((lastAxis == AXIS_ATTRIBUTE) || (lastAxis == AXIS_S_ATTRIBUTE)) {
        int size = result.childCount( SimpleReference.ID_SIMPLE_REFERENCE );
        if (size == 0) {
          return null;
        }
        else { //TODO wie gehen wir mit size > 1 um --> was sagt die XPath Spec ?
          Model resultValue = ((SimpleReference) result.get( SimpleReference.ID_SIMPLE_REFERENCE, null )).refModel();
          return resultValue.getContent();
        }
      }
      else if (lastAxis == AXIS_VARIABLE) {
        int size = result.childCount( SimpleReference.ID_SIMPLE_REFERENCE );
        if (size == 0) {
          return null;
        }
        else { //TODO wie gehen wir mit size > 1 um --> was sagt die XPath Spec ?
          Model resultValue = ((SimpleReference) result.get( SimpleReference.ID_SIMPLE_REFERENCE, null )).refModel();
          if (resultValue instanceof SimpleModel) return resultValue.getContent();
          else return result;
        }
      }
      else {
        return result;
      }
    }
  }

  /**
   * AN attribute doesn't have a namespace except 'any', so it inherits the
   * namespace from the model to which it belongs.
   * @param context the model to which the attribute belongs
   */
  private void setAttributeNamespace(Model context) {
    if (nodeTest.getNamespace() == XPathLocationStep.NAMESPACE) {
      nodeTest = context.type().name().getNamespace().getQName(nodeTest.getName());
    }
  }

  public QName getQNameValue( Model currentModel, int index, Context context ) {
    Object val = getValue( currentModel, index, context, false );
    if ((val == null) || (val instanceof QName)) {
      return (QName) val;
    }
    else {
      return QName.getInstance( val.toString(), namespaceTable );
    }
  }

  private QName getLastAxis() {
    if (this.nextLocationStep == null) {
      return this.axis;
    }
    else {
      return this.nextLocationStep.getLastAxis();
    }
  }

  public XPathLocationStep getNextLocationStep() {
    return nextLocationStep;
  }

  private void processNodeTest( Model model, int index, Context context, Model result, boolean readOnly ) {
    boolean accepted = false;
    if ((nodeTest == null) || (nodeTest == NODE_ANY)) {
      accepted = true;
    }
    else if (nodeTest == NODE_NODE) {
      accepted = (model.type() instanceof ComplexType);
    }
    else if (nodeTest == NODE_COMMENT) {
      //TODO
      throw new RuntimeException("TODO");
    }
    else if (nodeTest == NODE_PROCESSING_INSTRUCTION) {
      //TODO
      throw new RuntimeException("TODO");
    }
    else if (nodeTest == NODE_TEXT) {
      //TODO
      throw new RuntimeException("TODO");
    }

    if (accepted) {
      if ((predicate == null) || (predicate.evaluate( model, index, context, readOnly ))) {
        if (this.pointer) {
          if (model instanceof ModelReference) {
            model = ((ModelReference) model).refModel();
            readOnly = true;
            if (model == null) return;
          }
          else if (model instanceof ReadOnlyWrapper) {
            Model wrappedModel = ((ReadOnlyWrapper) model).wrappedModel();
            if (wrappedModel instanceof ModelReference) {
              model = ((ModelReference) wrappedModel).refModel();
              readOnly = true;
              if (model == null) return;
            }
            else {
              return;
            }
          }
          else {
            return;
          }
        }
        if (nextLocationStep == null) {
          add( model, result, readOnly );
        }
        else nextLocationStep.evaluate( model, context, result, readOnly );
      }
    }
  }

  private void add( Model model, Model result, boolean readOnly ) {
    if (!contains(result, model, readOnly)) {
      if (readOnly) {
        SimpleReference.addReference( result, new ReadOnlyWrapper( model ) );
      }
      else {
        SimpleReference.addReference( result, model );
      }
    }
  }

  private boolean contains( Model result, Model model, boolean readOnly ) {
    for (ModelIterator iter = result.iterator( SimpleReference.ID_SIMPLE_REFERENCE, null ); iter.hasNext(); ) {
      SimpleReference ref = (SimpleReference) iter.next();
      Model refModel = ref.refModel();
      if (refModel == model) {
        return true;
      }
      if (readOnly && (refModel instanceof ReadOnlyWrapper)) {
        if (((ReadOnlyWrapper) refModel).wrappedModel() == model) {
          return true;
        }
      }
    }
    return false;
  }

  private void processAttribute( Model model, Model result, boolean readOnly ) {
    ComplexType modelType = (ComplexType) model.type();
    setAttributeNamespace(model);
    Object value = model.get(nodeTest);
    SimpleType type = null;
    int i = modelType.indexOfAttribute(nodeTest);
    if (i >= 0) {
      type = modelType.getAttributeType( i );
    }
    else if (model instanceof Transient) {
      ComplexType transientType = ((Transient) model).transientType();
      int k = transientType.indexOfAttribute( nodeTest );
      if (k >= 0) {
        type = transientType.getAttributeType( k );
      }
    }
    if (type == null) {
      throw new IllegalArgumentException("Attribute name="+nodeTest+" not known for complexType name="+modelType.name());
    }
    SimpleModel attrModel = new SimpleModel(type, value);
    add( attrModel, result, readOnly );
  }

  private void addAncestors( Model model, int index, Context context, Model result, boolean readOnly ) {
    if (model != null) {
      Model parent = model.getParent( true );
      if (parent != null) {
        processNodeTest( parent, index, context, result, readOnly );
        addAncestors( parent, index+1, context, result, readOnly );
      }
    }  
  }

  private int addChildren(Model model, int index, Context context, Model result, boolean recursive, boolean readOnly) {
    Model childNode;
    if (model != null) {
      for (ModelIterator childs=model.iterator(null, null); childs.hasNext(); ) {
        childNode = childs.next();
        processNodeTest( childNode, index, context, result, readOnly );
        index++;
        if (recursive) {
          index = addChildren( childNode, index, context, result, recursive, readOnly );
        }
      }
    }
    return index;
  }

  public void evaluate( Model currentModel, Context context, Model result, boolean readOnly ) {
    QName type = (QName) keywords.get(axis);
    if (type == null) { // axis is a relationID
      int i = 1;
      if (currentModel != null) {
        for (ModelIterator iter = currentModel.iterator(axis, null); iter.hasNext(); ) {
          processNodeTest( iter.next(), i, context, result, readOnly );
          i++;
        }
      }
    }
    else if ((axis == AXIS_S_SELF) || (axis == AXIS_SELF)) {
      processNodeTest( currentModel, 1, context, result, readOnly );
    }
    else if ((axis == AXIS_S_PARENT) || (axis == AXIS_PARENT)) {
      if (currentModel != null) {
        Model parent = currentModel.getParent( true );
        processNodeTest( parent, 1, context, result, readOnly );
      }
    }
    else if ((axis == AXIS_S_ATTRIBUTE) || (axis == AXIS_ATTRIBUTE)) {
      processAttribute( currentModel, result, readOnly );
    }
    else if (axis == AXIS_VARIABLE) {
      Object variable = context.get( nodeTest );
      if (variable != null) {
        if (variable instanceof Model) {
          processNodeTest( (Model) variable, 1, context, result, readOnly );
        }
        else {
          SimpleType simpleType;
          if (variable instanceof Integer) simpleType = IntegerType.getDefInt();
          else if (variable instanceof Long) simpleType = IntegerType.getDefLong();
          else if (variable instanceof Byte) simpleType = IntegerType.getDefByte();
          else if (variable instanceof Short) simpleType = IntegerType.getDefShort();
          else if (variable instanceof Boolean) simpleType = BooleanType.getDefault();
          else if (variable instanceof DateObject) simpleType = DateTimeType.getDefDateTime();
          else if (variable instanceof DecimalObject) simpleType = DecimalType.getDefault();
          else {
            simpleType = StringType.getDefString();
            variable = variable.toString();
          }
          SimpleModel attrModel = new SimpleModel(simpleType, variable);
          add( attrModel, result, readOnly );
        }
      }
    }
    else if ((axis == AXIS_S_DESCENDANT_OR_SELF) || (axis == AXIS_DESCENDANT_OR_SELF)) {
      processNodeTest( currentModel, 1, context, result, readOnly );
      addChildren( currentModel, 2, context, result, true, readOnly );
    }
    else if (axis == AXIS_CHILD) {
      addChildren( currentModel, 1, context, result, false, readOnly );
    }
    else if (axis == AXIS_DESCENDANT) {
      addChildren( currentModel, 1, context, result, true, readOnly );
    }
    else if (axis == AXIS_ANCESTOR) {
      addAncestors( currentModel, 1, context, result, readOnly );
    }
    else if (axis == AXIS_ANCESTOR_OR_SELF) {
      processNodeTest( currentModel, 1, context, result, readOnly );
      addAncestors( currentModel, 2, context, result, readOnly );
    }
    else if (axis == AXIS_FOLLOWING_SIBLING) {
      //TODO
      throw new RuntimeException("TODO");
    }
    else if (axis == AXIS_PRECEDING_SIBLING) {
      //TODO
      throw new RuntimeException("TODO");
    }
    else if (axis == AXIS_FOLLOWING) {
      //TODO
      throw new RuntimeException("TODO");
    }
    else if (axis == AXIS_PRECEDING) {
      //TODO
      throw new RuntimeException("TODO");
    }
    else if (axis == AXIS_NAMESPACE) {
      //TODO
      throw new RuntimeException("TODO");
    }
  }

  public int parse( XPath xPath, char[] pathExpr, int start, int len) {
    int pos;
    int end;
    char ch = 0;
    QName type;
    Namespace ns = null;

    while (pathExpr[start] == ' ') {
      start++;
    }
    pos = start;
    end = start;

    if (pathExpr[pos] == '@') {
      axis = AXIS_S_ATTRIBUTE;
    }
    while (axis == null) {
      ch = pathExpr[pos];
      if (ch == ':') {
        if (pathExpr[pos+1] == ':') {
          end = pos;
          createAxis( pathExpr, start, end+2, ns );
        }
        else {
          // Extract namespace prefix if exists
          String nsPrefix = new String(pathExpr, start, (pos-start));
          ns = namespaceTable.getByPrefix( nsPrefix );
          start = pos+1;
        }
      }
      else if (ch == '[') {
        end = pos;
        createAxis( pathExpr, start, end, ns );
        if (nodeTest == null) nodeTest = NODE_NODE;
        break;
      }
      else if (ch == ']') {
        end = pos;
        createAxis( pathExpr, start, end, ns );
        len = pos;
        break;
      }
      else if (ch == '/') {
        end = pos;
        createAxis( pathExpr, start, end, ns );
        nextLocationStep = new XPathLocationStep( namespaceTable );
        return nextLocationStep.parse( xPath, pathExpr, pos+1, len );
      }
      else if (ch == '@') {
        end = pos;
        createAxis(pathExpr, start, end, ns );
        nextLocationStep = new XPathLocationStep( namespaceTable );
        return nextLocationStep.parse( xPath, pathExpr, pos, len );
      }
      else if (ch == '^') {
        end = pos;
        this.pointer = true;
        createAxis( pathExpr, start, end, ns );
        if (pos+1 < len) {
          nextLocationStep = new XPathLocationStep( namespaceTable );
          return nextLocationStep.parse( xPath, pathExpr, pos+1, len );
        }
      }
      else  if ((ch == '<') || (ch == '>') || (ch == '=') || (ch == '!') || (ch == ' ')) {
        end = pos;
        createAxis( pathExpr, start, end, ns );
        return end;
      }
      else if (pos+1 == len) {
        end = pos+1;
        createAxis( pathExpr, start, end, ns );
      }
      pos++;
    }

    if ((pos < len) && (pathExpr[pos] != '/')) {
      start = pos+1;
      while ((pos < len) && (nodeTest == null)) {
        ch = pathExpr[pos];
        if ((ch == '(') && (pathExpr[pos+1] == ')')) {
          nodeTest = XPathLocationStep.NAMESPACE.getQName( pathExpr, start, pos+2 );
          pos += 2;
        }
        else if ((ch == '[') || (ch == ']') || (ch == '/') || (ch == '<') || (ch == '>') || (ch == '=') || (ch == '!') || (ch == ' ')) {
          if (pos > start) {
            nodeTest =  XPathLocationStep.NAMESPACE.getQName( pathExpr, start, pos );
          }
          else {
            nodeTest = NODE_NODE;
          }
        }
        else if (pos+1 == len) {
          nodeTest =  XPathLocationStep.NAMESPACE.getQName( pathExpr, start, pos+1 );
          pos++;
        }
        else {
          pos++;
        }
      }
      if ((nodeTest != null) && (keywords.get(nodeTest) != TYPE_NODETEST)) {
        if (nodeTest.getNamespace() == XPathLocationStep.NAMESPACE) {
          if( (axis != AXIS_S_ATTRIBUTE) && (axis != AXIS_ATTRIBUTE)) {
            //need the default namespace on attribute for evaluation
            nodeTest = namespaceTable.getDefaultNamespace().getQName( nodeTest.getName() );
          }
        }
      }

      if ((pos < len) && (pathExpr[pos] == '[')) {
        predicate = new XPathExpression( namespaceTable );
        pos = predicate.parse( xPath, pathExpr, pos+1, len);
        pos++;
      }
      if ((pos < len) && (pathExpr[pos] == '^')) {
        this.pointer = true;
        pos++;
      }
    }

    if ((pos < len) && (pathExpr[pos] == '/')) {
      nextLocationStep = new XPathLocationStep( namespaceTable );
      return nextLocationStep.parse( xPath, pathExpr, pos+1, len );
    }
    else {
      return pos;
    }
  }

  private void createAxis( char[] pathExpr, int start, int end, Namespace ns ) {
    QName type;
    if (end > start) {
      if (pathExpr[start] == '$') {
        axis = AXIS_VARIABLE;
        nodeTest = QName.getInstance( pathExpr, start + 1, end, namespaceTable );
      }
      else {
        axis = XPathLocationStep.NAMESPACE.getQName(pathExpr, start, end);
        type = (QName) keywords.get(axis);
        if (type == TYPE_NODETEST) {
          nodeTest = axis;
          axis = AXIS_CHILD;
        }
        else if (type == null) {
          // axis is interpreted as relationID within given namespace
          if (ns != null) axis = ns.getQName(pathExpr, start, end);
          else axis = QName.getInstance( pathExpr, start, end, namespaceTable );
        }
      }
    }
    else {
      axis = AXIS_CHILD;
    }
  }


  public String toString() {
    StringBuffer result = new StringBuffer();
    if (axis != null) {
      result.append(axis.getName());
    }
    if ((nodeTest != null)) {
      if (axis != AXIS_VARIABLE) result.append("::");
      result.append(nodeTest.getName());
    }
    if (predicate != null) {
      result.append('[').append(predicate.toString()).append(']');
    }
    if (pointer) {
      result.append( "^" );
    }
    if (nextLocationStep != null) {
      if (pointer) result.append( '^');
      else if (nextLocationStep.axis != AXIS_S_ATTRIBUTE) result.append('/');
      result.append(nextLocationStep.toString());
    }
    return result.toString();
  }
}
