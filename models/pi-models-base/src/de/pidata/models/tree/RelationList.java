/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.qnames.QName;

public class RelationList {

  private QName relationName;
  private Model first;
  private Model last;
  private int   size = 0;

  public RelationList(QName relationName) {
    this.relationName = relationName;
  }

  public QName getRelationName() {
    return relationName;
  }

  /**
   * Returns the first member of this relation or null if empty
   * @return the first member of this relation or null if empty
   */
  public Model getFirst() {
    return first;
  }

  public void setFirst(Model first) {
    this.first = first;
  }

  /**
   * Returns the last member of this relation or null if empty
   * @return the last member of this relation or null if empty
   */
  public Model getLast() {
    return last;
  }

  public void setLast(Model last) {
    if (this.first == null) this.first = last;
    this.last = last;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }
}
