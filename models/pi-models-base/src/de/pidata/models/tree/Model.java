/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.qnames.Key;
import de.pidata.qnames.NamespaceTable;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

import java.util.Enumeration;

/**
 * Models are used for all Objects which can be read from a resource or data
 * stream. Typically these Model will not maintain a Vector or Object array
 * of child Models. For type safety and faster access they use member vaiables
 * to store their children. So fetchChildren() does not copy an existing
 * structure: in most case it copies values frow different member variables
 * into the result Vector.
 */
public interface Model {

  //----- NOTE: copied values exist in class ChildList
  int PREV = 0;
  int NEXT = 1;
  int PREV_IN_REL = 2;
  int NEXT_IN_REL = 3;

  /**
   * Returns this Model's ID. That ID is also used to identify this Model
   * within its parent Model.
   *
   * @return this Model's ID, never null
   */
  Key key();

  /**
   * Returns this Model's element defiition.
   *
   * @return this Model's element defiition
   */
  Type type();

  /**
   * Returns an Enumeration over all attribute names assigned to anyAtribute
   * @return an Enumeration of QName
   */
  Enumeration anyAttributeNames();

  /**
   * Returns this model's parent or null if this model does not have a parent
   *
   * @return this model's parent or null
   * @param useCloneSource  if true in case of parent==null cloneSource's parent is returned
   */
  Model getParent( boolean useCloneSource );

  /**
   * Creates an XPath from fromAncestor to this model
   * @param fromAncestor the ancestor from which the XPath will start
   * @return an XPath
   */
  StringBuffer createPath( Model fromAncestor );

  /**
   * Return this model's relationID within its parent or null if parent is null
   * @return this model's relationID within its parent or null
   */
  QName getParentRelationID();

  /**
   * Set the parent Model for this Model. If parent is null all siblings are
   * cleared, too.
   *
   * @param parent the parent Model to be set or null
   * @param parentRelationID this model's relationID within parent
   */
  void setParent( Model parent, QName parentRelationID );

  /**
   * Returns the attribute value for the given attrName.
   *
   * @param attrName the ID for the value to be returned
   * @return the attribute value for the given attrName or null if attribute is not known
   */
  Object get( QName attrName );

 /**
   * Returns the child for the given childKey and within relationName or null if child is not present.
   *
   * @param relationName the relation to which child belongs, if null relation is ignored
   * @param childKey     key of the child to be returned, if null the first child is returned
   * @return child for the given elementID or null if child is not present.
   */
 Model get( QName relationName, Key childKey );

  /**
   * Sets the value for the attribute identified by attributeName. It depends on the model's
   * implementation how to map elementIDs to attributes.
   *
   * @param attributeName the ID for the attribute value to be set
   * @param value       the new value for the attribute identified by attributeName
   * @throws ValidationException if this model is not writeable, attributeName is unknown
   *                             or value is not valid for attribute's type
   */
  void set( QName attributeName, Object value ) throws ValidationException;

  /**
   * Returns this model's content or null if not available
   * @return this model's content or null if not available
   */
  Object getContent();

  /**
   * Sets this model's content
   * @param content this model's new content
   * @throws ValidationException if model's type does not allow any content
   *                             or content's type is wrong
   */
  void setContent( Object content ) throws ValidationException;

  /**
   * Returns the count of this Model's when using relation ID as filter. If filter is null the
   * count of all children is returned
   *
   * @param  relationName the relation ID to use as filter, may be null
   * @return the count of this Model's when using relation ID as filter
   */
  int childCount( QName relationName );

  /**
   * Adds childModel to this Model. If child relation is a composition this Model is set as child's parent.
   *
   * @param relationName the relation ID to add childModel to
   * @param childModel the new child model
   */
  void add( QName relationName, Model childModel );

  /**
   * Adds/Replaces childModel to this model
   *
   * @param relationName the relation ID to add childModel to
   * @param value the new child model or value
   */
  void replace( QName relationName, Object value );

  /**
   * Removes childModel from this Model's relation relationName. If child relation is a composition child's parent is set to null.
   *
   * @param relationName the relation ID to remove childModel from
   * @param childModel  the child nodo to remove
   * @return true if model was found (and removed), false otherwise
   */
  boolean remove( QName relationName, Model childModel );

  /**
   * Removes all children of the given realtion.
   * If relationName is null all relation's children are removed.
   * @param relationName the realtion
   */
  void removeAll( QName relationName );

  /**
   * Returns an iterator over this Model's children having relation ID as filter. If filter is null an
   * iterator over all children is returned.
   *
   * @param  relationName  name of the relation to use as filter, may be null
   * @param filter                 the filter to use for skipping not matching children, leave null to get all children of relation
   * @return an iterator over this Model's children filtered by relation and filter
   */
  ModelIterator<Model> iterator( QName relationName, Filter filter );
  
  /**
   * Creates a deep copy of this model an assigns it the new key <code>id</code>.
   * If <code>id</code> is null this model's id is used.
   * @param newKey  the new key for the clone or null if clone should have same key
   * @param deep    if true, makes a deep copy
   * @param linked  if true clone is recursively linked to its clone source
   * @return copy of this model
   * @throws RuntimeException if this model does not support cloning
   */
  Model clone( Key newKey, boolean deep, boolean linked );

  /**
   * Adds listener to this Model's listener list
   *
   * @param listener the listener to be added
   */
  void addListener( EventListener listener );

  /**
   * Removes listener from this Model's listener list
   *
   * @param listener the listener to be removed
   */
  void removeListener( EventListener listener );

  /**
   * Sets this Models's own namespaceTable. If it already exists IllegalArgumentException is thrown.
   *
   * @throws IllegalArgumentException if own namespaceTable already exists
   */
  void ownNamespaceTable( NamespaceTable namespaceTable );

  /**
   * Returns a table containing all namespace definitions for this model.
   * If this model does not have it's own NamespaceTable the parent's
   * NamespaceTable is returned. If this model does not have a parent and
   * namespaceTable does not exist, a new own namespaceTable is created.
   *
   * @return this model's NamespaceTable, never null
   */
  NamespaceTable namespaceTable();

  /**
   * Find the next sibling within given relation. If relationName is null, search all siblings.
   * @param relationName the relation to search or null to search all siblings
   * @return next sibling or null if relation has no more siblings
   */
  Model nextSibling( QName relationName );

  /**
   * Find the preceeding sibling within given relation. If relationName is null, search all siblings.
   * @param relationName the relation to search or null to search all siblings
   * @return previous sibling or null if relation has no preceeding sibling
   */
  Model prevSibling( QName relationName );

  /**
   * Returns current index of child within given relation. Be careful using that index. It will
   * change when adding/removing children to/from this model!
   * @param relationName  ID if the relation to search for child
   * @param child       the child to search for
   * @return child's index or -1 if not found
   */
  int indexOfChild( QName relationName, Model child );

  /**
   * Find the first child within given relation. If relationName is null, search all childre.
   * @param relationName the relation to search or null to search all children
   * @return first child or null if relation has no children
   */
  Model firstChild( QName relationName );

  /**
   * Find the last child within given relation. If relationName is null, search all childre.
   * @param relationName the relation to search or null to search all children
   * @return last child or null if relation has no children
   */
  Model lastChild( QName relationName );

  /**
   * Changes the siblingType to given sibling
   * @param siblingType the type of sibling: NEXT, PREV, PREV_IN_REL, NEXT_IN_REL
   * @param sibling     the new sibling
   */
  void changeSibling( int siblingType, Model sibling );

  /**
   * Updates this Model by replacing all attribute values with the
   * attribute values of updateModel. UpdateModel must have the same
   * type as this Model.
   * @param updateModel the Model containing the new attribute values
   * @return true if at least one attribute has been modified by this update
   */
  boolean update( Model updateModel );

  void insert(QName relationID, Model childModel, Model beforeChild);

  /**
   * Returns the model this model was cloned from or null
   * @return the model this model was cloned from or null
   */
  Model cloneSource();

  /**
   * Only for internal use: Called by cloneModel() with the model this model was cloned from
   * @param cloneSource the model this model was cloned from
   */
  void clonedFrom( Model cloneSource );

  /**
   * Sorts all children in relationName.
   * @param relationName name of the relation to be sorted
   * @param comparator     the SortRule to use for sorting
   * @return true if sort was successful, false if not successful or not supported
   */
  boolean sort( QName relationName, Comparator<? extends Model> comparator );

  /**
   * Returns true if attributeName is readOnly
   * @return true if attributeName is readOnly
   */
  boolean isReadOnly( QName attributeName );


  // ------------------------------  Entsorgte Methoden zu Namespaces -----------------------------------------------

  /**
   * Add predefined namespaces to the model (e.g. while cloning a model)
   * @param namespaces
   * @param prefixes
   */
//  public void addNamespaces(Vector namespaces, Vector prefixes);

  /**
   * Returns a prefix for the model's namespace.
   * The namespace is searched within the hierarchie of the model.
   * If the namspace was defined previously, then this method returns the first prefix found for
   * the namespace. Otherwise a new prefix is build.
   * @param fromAncestor
   * @return the prefix for the namespace
   */
//  public String getPrefix(Model fromAncestor);

  /**
   * Returns a prefix for the given namespace valid within this model.
   * The namespace is searched within the hierarchie of the model.
   * If the namspace  was defined previously, then this method returns the first prefix found for
   * the namespace. Otherwise a new prefix is build.
   * @param namespace
   * @param fromAncestor
   * @return the prefix for the given namespace.
   */
//  public String getPrefix(Namespace namespace, Model fromAncestor);

  /**
   * Appends the namespaces and their prefixes to the namespaceTable and the prefixTable.
   * @param namespaceTable
   * @param prefixTable
   */
//  public void getNamespaces(Vector namespaceTable, Vector prefixTable);

  /**
   * Add a namespace declaration to this model
   * @param namespace the namespace to be added
   * @param prefix
   */
//  public void addNamespace(Namespace namespace, String prefix);

  /**
   * Returns the number of namespace declarations of this model
   * @return the number of namespace declarations of this model
   */
//  public int namespaceCount();

}


