/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.models.types.simple.DecimalObject;
import de.pidata.qnames.QName;

public class ComparatorDecimal implements Comparator<Model> {

  private QName attributeName;
  private XPath modelPath;

  /**
   * Creates a new decimal comparator
   * @param attributeName the attrbiute containing a decimal value to use for comparision
   */
  public ComparatorDecimal( QName attributeName ) {
    this( null, attributeName );
  }

  public ComparatorDecimal( XPath modelPath, QName attributeName ) {
    if (attributeName == null) {
      throw new IllegalArgumentException( "attributeName must not be null" );
    }
    this.attributeName = attributeName;
    this.modelPath = modelPath;
  }

  /**
   * Compares models m1 and m2 by their values of attribute given via constructor.
   * Null is treated as empty String
   *
   * @param m1 a model
   * @param m2 a model
   * @return -1 (m1 less m2), 0 (m1 equal m2) or 1 (m1 greater m2)
   * @throws IllegalArgumentException if models cannot be compared by this SortRole
   */
  public int compare( Model m1, Model m2 ) {
    DecimalObject value1 = fetchValue( m1 );
    DecimalObject value2 = fetchValue( m2 );
    int compare = value1.compareTo( value2 );
    if (compare == 0) return 0;
    else if (compare > 0) return 1;
    else return -1;
  }

  private DecimalObject fetchValue( Model model ) {
    if (modelPath != null) {
      model = modelPath.getModel( model, null );
      if (model == null) return DecimalObject.ZERO;
    }
    Object value = model.get( attributeName );
    if (value == null) return DecimalObject.ZERO;
    else return (DecimalObject) value;
  }

}
