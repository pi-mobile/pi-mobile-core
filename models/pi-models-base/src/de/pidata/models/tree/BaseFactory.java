/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.qnames.Namespace;
import de.pidata.models.types.simple.BinaryType;
import de.pidata.models.types.simple.BooleanType;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.models.types.simple.DecimalType;
import de.pidata.models.types.simple.DurationType;
import de.pidata.models.types.simple.IntegerType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;

import java.util.Hashtable;

public class BaseFactory extends AbstractModelFactory {

  public static final Namespace NAMESPACE = Namespace.getInstance("http://www.w3.org/2001/XMLSchema");
  public static final QName ID_PATH = NAMESPACE.getQName("path");
  public static final QName ID_RELATION = NAMESPACE.getQName("relation");
  public static final QName ID_DISPLAY_ATTRIBUTE = NAMESPACE.getQName("displayAttribute");
  public static final QName ID_INT = NAMESPACE.getQName("int");
  public static final QName ID_LONG = NAMESPACE.getQName("long");
  public static final QName ID_STRING = NAMESPACE.getQName("string");
  public static final QName ID_BINARY = NAMESPACE.getQName("binary");
  public static final QName ID_BOOLEAN = NAMESPACE.getQName("boolean");
  public static final QName ID_KEY = NAMESPACE.getQName("key");
  public static final String UNBOUNDED = "unbounded";

  public BaseFactory() {
    super(NAMESPACE, "http://www.w3.org/2001/XMLSchema", "3.0" );
    initBaseTypes();
  }

  protected void initBaseTypes() {
    int i = 0;
    addType( StringType.getDefString() );
    addType( StringType.getNormalizedString() );
    addType( StringType.getDefLanguage() );
    addType( StringType.getDefToken() );
    addType( StringType.getDefNMTOKEN() );
    addType( StringType.getDefNMTOKENS() );
    addType( StringType.getDefAnyURI() );
    addType( StringType.getDefIDREFS() );

    addType( IntegerType.getDefInteger() );
    addType( IntegerType.getDefLong() );
    addType( IntegerType.getDefInt() );
    addType( IntegerType.getDefShort() );
    addType( IntegerType.getDefUnsignedShort() );
    addType( IntegerType.getDefUnsignedLong() );
    addType( IntegerType.getDefUnsignedByte() );
    addType( IntegerType.getDefByte() );
    addType( IntegerType.getDefNegativeInteger() );
    addType( IntegerType.getDefPositiveInteger() );
    addType( IntegerType.getDefNonNegativeInteger() );
    addType( IntegerType.getDefNonPositiveInteger() );

    addType( BooleanType.getDefault() );

    addType( DecimalType.getDefault() );
    addType( DecimalType.getDefDouble() );

    addType( DateTimeType.getDefDateTime() );
    addType( DateTimeType.getDefDate() );
    addType( DateTimeType.getDefTime() );

    addType( DurationType.getDefault() );

    addType( QNameType.getQName() );
    addType( QNameType.getNCName() );
    addType( QNameType.getIDType() );
    addType( QNameType.getIDREFType() );
    addType( QNameType.getNameType() );
    addType( BinaryType.base64Binary );

    addType( SimpleReference.TYPE );

    attributes = new Hashtable();
    attributes.put(ID_PATH, StringType.getDefString());
    attributes.put(ID_RELATION, QNameType.getQName());
    attributes.put(ID_DISPLAY_ATTRIBUTE, QNameType.getQName());
  }
}
