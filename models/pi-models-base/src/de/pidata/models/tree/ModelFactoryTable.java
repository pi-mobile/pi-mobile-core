/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.log.Logger;
import de.pidata.models.types.Relation;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pru on 12.05.16.
 */
public class ModelFactoryTable {

  protected Map<Namespace,ModelFactory> modelFactoryMap = new HashMap<Namespace, ModelFactory>();

  private static ModelFactoryTable instance;

  public static synchronized ModelFactoryTable getInstance() {
    if (instance == null) {
      new ModelFactoryTable();
    }
    return instance;
  }

  protected ModelFactoryTable() {
    instance = this;
  }

  /**
   * Sets this namespace's model factory.
   * @param factory  this namespace's model factory
   * @throws IllegalArgumentException if factory is already defined
   */
  public void setFactory( Namespace namespace, ModelFactory factory ) {
    if (getFactory( namespace ) != null) {
      throw new IllegalArgumentException("Must not redefine Namespace's factory, NS-Uri="+namespace.getUri());
    }
    modelFactoryMap.put( namespace, factory );
  }

  /**
   * Returns this namespace's model factory
   * @return this namespace's model factory
   */
  public ModelFactory getFactory( Namespace namespace ) {
    return modelFactoryMap.get( namespace );
  }

  public synchronized ModelFactory getOrSetFactory( Namespace namespace, Class factoryClass ) {
    ModelFactory factory = getFactory( namespace );
    if (factory == null) {
      try {
        factory = (ModelFactory) factoryClass.newInstance(); // registers itself
      }
      catch (Exception e) {
        String msg = "Could not create model factory for ns=" + namespace.getUri() + ", class=" + factoryClass;
        Logger.error( msg, e );
        throw new IllegalArgumentException( msg );
      }
    }
    else {
      if (factoryClass != factory.getClass()) {
        throw new IllegalArgumentException("Incompatible factory class, ns="+namespace.getUri()+", current factory="+factory
            + "required factory class="+factoryClass );
      }
    }
    return factory;
  }

  public static Relation findRootRelation( QName relationName ) {
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( relationName.getNamespace() );
    if (factory == null) {
      throw new IllegalArgumentException( "No factory defined for namespace="+relationName.getNamespace() );
    }
    return factory.getRootRelation( relationName );
  }
}
