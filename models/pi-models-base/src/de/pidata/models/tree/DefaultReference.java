/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.qnames.QName;

public class DefaultReference implements ModelReference {

  private QName name;
  private QName refTypeName;
  private QName[] refAttributes;
  private Model refModel;

  public DefaultReference( QName name, QName refTypeName, QName[] refAttributes ) {
    this.name = name;
    this.refTypeName = refTypeName;
    this.refAttributes = refAttributes;
  }

  public DefaultReference( QName name, QName refTypeName, QName singleRefAttribute ) {
    this.name = name;
    this.refTypeName = refTypeName;
    this.refAttributes = new QName[1];
    this.refAttributes[0] = singleRefAttribute;
  }

  /**
   * Returns the name of this reference
   *
   * @return the name of this reference
   */
  public QName getName() {
    return name;
  }

  /**
   * Returns the name of the type referenced
   *
   * @return the name of the type referenced
   */
  public QName getRefTypeName() {
    return refTypeName;
  }

  /**
   * Returns the name of the referencing attribute at index.
   * Order and type of the attributes is the same as defined by the
   * key of the referenced type
   *
   * @param index index of the referencing attribute
   * @return name of the referencing attribute at index
   */
  public QName getRefAttribute( int index ) {
    return refAttributes[index];
  }

  /**
   * Returns the model referenced by this model
   *
   * @return the model referenced by this model
   */
  public Model refModel() {
    if (refModel == null) {
      refModel = ModelHelper.findModel( this );
    }
    return refModel;
  }
}
