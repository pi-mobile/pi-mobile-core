/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.models.types.ComplexType;
import de.pidata.qnames.QName;
import de.pidata.qnames.Key;

import java.util.Vector;

public class ModelHelper {

  /**
   * Reallocates a Model array with a new size, and copies the contents
   * of the old array to the new array.
   * @param oldArray  the old array, to be reallocated.
   * @param newSize   the new array size.
   * @return          A new array with the same contents.
   */
  public static Model[] resizeArray( Model[] oldArray, int newSize ) {
    int oldSize = oldArray.length;
    Model[] newArray = new Model[newSize];
    int preserveLength = Math.min(oldSize, newSize);
    if (preserveLength > 0) {
      System.arraycopy(oldArray, 0, newArray, 0, preserveLength);
    }
    return newArray;
  }

  /**
   * Returns true if checkAncestor is an ancestor of model
   * @param checkAncestor possible anchestor
   * @param model         model to check
   * @return true if checkAncestor is an ancestor of model
   */
  public static boolean isAncestor( Model checkAncestor, Model model ) {
    while (model != null) {
      model = model.getParent( false );
      if (model == checkAncestor) return true;
    }
    return false;
  }

  /**
   * Recursively updates original with all attributes and children from clone.
   * All original's children not existing as clone's children are removed from original.
   * This method is e.g. used by DialogController to transfer all modifications made on the clone
   * of original back to the original if user pressed the OK button.
   * @param original the model to be updated
   * @param clone    the clone
   */
  public static void updateOriginal( Model original, Model clone ) {
    if (original == clone) {
      return;
    }
    if (original.type() != clone.type()) {
      throw new IllegalArgumentException( "For updateOriginal both models must have same type, original type="
                                          +original.type().name()+", clone type="+clone.type().name() );
    }
    if (clone.cloneSource() != original) {
      throw new IllegalArgumentException( "Clone is not a clone of original="+original+", clone.cloneSource="+clone.cloneSource());
    }
    Vector originalUpdated = new Vector();
    Vector added = new Vector();
    original.update( clone );
    for (ModelIterator childIter = clone.iterator(null, null); childIter.hasNext(); ) {
      Model cloneChild = childIter.next();
      Model originalChild = cloneChild.cloneSource();
      if (originalChild == null) {
        // cloneSource is null. If child has a key, check if equal child exists
        // this may happen if original has already been updated, e.g. by call to a service just before closing the dialog
        Key childKey = cloneChild.key();
        if (childKey != null) {
          originalChild = original.get( cloneChild.getParentRelationID(), childKey );
          if (originalChild != null) {
            cloneChild.clonedFrom( originalChild );
          }
        }
      }
      if (originalChild != null) {
        // modified whithin dialog
        updateOriginal( originalChild, cloneChild );
        originalUpdated.addElement( originalChild );
      }
      else {
        // added whithin dialog
        // we must not add new childs before having removed deleted, because replace operations may
        // be reason for added child. For maxOccus=1 adding before removing will lead to an exception
        added.addElement( cloneChild );
      }
    }
    // removed in dialog are all children not contained in originalUpdated
    Vector deleted = new Vector();
    for (ModelIterator childIter = original.iterator(null, null); childIter.hasNext(); ) {
      Model originalChild = childIter.next();
      if (!originalUpdated.contains( originalChild )) {
        deleted.addElement( originalChild );
      }
    }
    for (int i = 0; i < deleted.size(); i++) {
      Model originalChild = (Model) deleted.elementAt( i );
      original.remove( originalChild.getParentRelationID(), originalChild );
    }
    for (int i = 0; i < added.size(); i++) {
      Model cloneChild = (Model) added.elementAt( i );
      Model originalChild = cloneChild.clone( null, true, false );
      original.add( cloneChild.getParentRelationID(), originalChild );
    }
  }

  /**
   * Recursively updates original with all attributes and children from copy.
   * All original's children not existing as copy's children are removed from original.
   * The copy child is identified by having same relationID and key.
   * This method is e.g. used by Service routines to update BOs in memory.
   *
   * @param original the model to be updated
   * @param copy    the clone / copy
   */
  public static void updateRecursive( Model original, Model copy ) {
    if (original == copy) {
      return;
    }
    if (original.type() != copy.type()) {
      throw new IllegalArgumentException( "For updateRecursive both models must have same type, original type="
          + original.type().name() + ", copy type=" + copy.type().name() );
    }
    if (!original.key().equals( copy.key() )) {
      throw new IllegalArgumentException( "For updateRecursive both model's keys must be equal, original key="
          + original.key() + ", copy key=" + copy.key() );
    }
    Vector originalUpdated = new Vector();
    Vector added = new Vector();
    original.update( copy );
    for (ModelIterator childIter = copy.iterator( null, null ); childIter.hasNext();) {
      Model cloneChild = childIter.next();
      Key childKey = cloneChild.key();
      Model originalChild = original.get( cloneChild.getParentRelationID(), childKey );
      if (originalChild != null) {
        // modified2
        updateRecursive( originalChild, cloneChild );
        originalUpdated.addElement( originalChild );
      }
      else {
        // added
        // we must not add new childs before having removed deleted, because replace operations may
        // be reason for added child. For maxOccus=1 adding before removing will lead to an exception
        added.addElement( cloneChild );
      }
    }
    // removed in dialog are all children not contained in originalUpdated
    Vector deleted = new Vector();
    for (ModelIterator childIter = original.iterator( null, null ); childIter.hasNext();) {
      Model originalChild = childIter.next();
      if (!originalUpdated.contains( originalChild )) {
        deleted.addElement( originalChild );
      }
    }
    for (int i = 0; i < deleted.size(); i++) {
      Model originalChild = (Model) deleted.elementAt( i );
      original.remove( originalChild.getParentRelationID(), originalChild );
    }
    for (int i = 0; i < added.size(); i++) {
      Model cloneChild = (Model) added.elementAt( i );
      Model originalChild = cloneChild.clone( null, true, false );
      original.add( cloneChild.getParentRelationID(), originalChild );
    }
  }

  /**
   * Recursively updates attributes in newModel with all non-null attributes and children from oldModel.
   * All newModel's children not existing in oldModel are left unchanged.
   * The oldModel child is identified by having same relationID and key.
   * This method is e.g. used to copy setting from a previous version to a new version
   * New and Old Model it is not required to have a key,
   * Update will only work for children having a key.
   *
   * @param newModel the model to be updated
   * @param oldModel the old model to get attribute values from
   */
  public static void updateFromOlder( Model newModel, Model oldModel, boolean childrenOnly) {
    if (newModel == oldModel) {
      return;
    }
    if (newModel.type() != oldModel.type()) {
      throw new IllegalArgumentException( "For updateFromOlder both models must have same type, new type="
          + newModel.type().name() + ", old type=" + oldModel.type().name() );
    }
    //--- Update attributes
    if(!childrenOnly) {
      ComplexType modelType = (ComplexType) newModel.type();
      for (int i = 0; i < modelType.attributeCount(); i++) {
        QName attrName = modelType.getAttributeName( i );
        int keyIndex = modelType.getKeyIndex( attrName );
        if (keyIndex < 0) {
          Object oldValue = oldModel.get( attrName );
          if (oldValue != null) {
            newModel.set( attrName, oldValue );
          }
        }
      }
    }

    //--- Recurse into children
    for (ModelIterator newChildIter = newModel.iterator( null, null ); newChildIter.hasNext(); ) {
      Model newChild = newChildIter.next();
      Key childKey = newChild.key();
      ComplexType modelType = (ComplexType) newModel.type();
      int maxOccurs = modelType.getRelation( newChild.getParentRelationID() ).getMaxOccurs();
      if (childKey != null || maxOccurs == 1) {
        Model oldChild = oldModel.get( newChild.getParentRelationID(), childKey );
        if (oldChild != null) {
          updateFromOlder( newChild, oldChild, false );
        }
      }
    }
  }


  /**
   * Returns the next Model having attribute with given value
   * @param from                first Model to check, may be null (then returns null)
   * @param relationName name of the relation to follow, if null all siblings are checked
   * @param forward          search formwar if true, otherwise backward
   * @param attrName       name of the attribvute to check
   * @param value              required value for attrName
   * @return the next model having attribute with given value or null if end of sibling list has reached without any match
   */
  public static Model findNext( Model from, QName relationName, boolean forward, QName attrName, Object value ) {
    while (from != null) {
      Object attrvalue = from.get( attrName );
      if (value == null) {
        if (attrvalue == null) {
          return from;
        }
      }
      else if (value.equals( attrvalue )) {
        return from;
      }
      if (forward) from = from.nextSibling( relationName );
      else from = from.prevSibling( relationName );
    }
    return null;
  }

  /**
   * Retrieves the model referenced by reference
   * @param reference teh reference to a model
   * @return the referenced model or null if not found
   */
  public static Model findModel( ModelReference reference ) {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  public static Model[] toArray( Model parent, QName relationID, Filter filter ) {
    Model[] result = new Model[parent.childCount( relationID )];
    int i = 0;
    for (Model child : parent.iterator( relationID, filter )) {
      result[i] = child;
      i++;
    }
    return result;
  }
}
