/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.models.types.ComplexType;

/**
 * Interface for transient Extrensions e.g. of a Model
 */
public interface Transient {

  /**
   * Returns the complex type defining all transient values of implementor.
   * @return the complex type defining all transient values
   */
  public ComplexType transientType();

  /**
   * Returns the transient value identified by valueID
   * @param transientIndex index of transient value within transientType()
   * @return the transient value identified by valueID or null if valueID is not known
   */
  public Object transientGet( int transientIndex );

  /**
   * Returns the transient value identified by valueID if possible
   * @param transientIndex index of transient value within transientType()
   * @param value
   * @throws IllegalArgumentException if valueID is unkmown or read-only
   */
  public void transientSet( int transientIndex, Object value );
}
