/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.qnames.QName;
import de.pidata.qnames.Key;

import java.util.Vector;

public class ChildList {

  private Vector relations;
  private int size = 0;
  private Model firstChild;
  private Model lastChild;
  private Model parent;

  public ChildList() {
    this(null);
  }

  public ChildList(Model parent) {
    relations = new Vector();
    this.parent = parent;
  }

  public Model getParent() {
    return parent;
  }

  public synchronized void setParent(Model parent) {
    this.parent = parent;
    Model child = getFirstChild(null);
    while (child != null) {
      child.setParent(parent, child.getParentRelationID());
      child = child.nextSibling(null);
    }
  }

  public synchronized int relCount() {
    return relations.size();
  }

  public synchronized RelationList findRelation(QName relationName) {
    RelationList rel;
    for (int i = relations.size()-1; i >= 0; i--) {
      rel = (RelationList) relations.elementAt(i);
      if (rel.getRelationName() == relationName) {
        return rel;
      }
    }
    return null;
  }

  private synchronized RelationList getRelation(QName relationName) {
    RelationList rel = findRelation(relationName);
    if (rel == null) {
      rel = new RelationList(relationName);
      relations.addElement(rel);
    }
    return rel;
  }

  public synchronized void add(QName relationName, Model childModel) {
    if (childModel.getParent( false ) != null) {
      throw new IllegalArgumentException("Cannot add child, parent is not null relationName="+relationName);
    }

    RelationList rel = getRelation(relationName);
    Model lastInRelation = rel.getLast();
    Model last = getLastChild(null);

    childModel.setParent(this.parent, relationName);
    childModel.changeSibling( Model.PREV, last );
    childModel.changeSibling( Model.PREV_IN_REL, lastInRelation );
    childModel.changeSibling( Model.NEXT, null );
    childModel.changeSibling( Model.NEXT_IN_REL, null );

    if (last != null) {
      last.changeSibling( Model.NEXT, childModel );
    }
    if (lastInRelation != null) {
      lastInRelation.changeSibling( Model.NEXT_IN_REL, childModel );
    }

    rel.setLast(childModel);
    if (this.firstChild == null) this.firstChild = childModel;
    this.lastChild = childModel;
    rel.setSize(rel.getSize()+1);
    size++;
  }

  public synchronized void insert(QName relationName, Model childModel, Model beforeModel) {
    if (beforeModel == null) {
      add(relationName, childModel);
    }
    else {
      RelationList rel = getRelation(relationName);
      childModel.setParent(this.parent, relationName);

      Model prev = beforeModel.prevSibling(null);
      childModel.changeSibling( Model.PREV, prev );
      if (prev != null) prev.changeSibling( Model.NEXT, childModel );

      Model prevInRel = beforeModel.prevSibling(relationName);
      childModel.changeSibling( Model.PREV_IN_REL, prevInRel );
      if (prevInRel != null) prevInRel.changeSibling( Model.NEXT_IN_REL, childModel );

      childModel.changeSibling( Model.NEXT, beforeModel );
      beforeModel.changeSibling( Model.PREV, childModel );

      Model nextInRel;
      if (beforeModel.getParentRelationID() == relationName) {
        nextInRel = beforeModel;
      }
      else {
        nextInRel = beforeModel.nextSibling( relationName );
      }
      childModel.changeSibling( Model.NEXT_IN_REL, nextInRel );
      if (nextInRel == null) {
        rel.setLast( childModel );
      }
      else {
        nextInRel.changeSibling( Model.PREV_IN_REL, childModel );
      }
      if (this.firstChild == beforeModel) this.firstChild = childModel;
      if (rel.getFirst() == nextInRel) rel.setFirst(childModel);
      rel.setSize(rel.getSize()+1);
      size++;
    }
  }

  public int size() {
    return size;
  }

  public synchronized int size(QName relationName) {
  if (relationName == null) return size();
    RelationList rel = findRelation(relationName);
    if (rel == null) return 0;
    else return rel.getSize();
  }

  public synchronized Model getFirstChild(QName relationName) {
    if (relationName == null) {
      return this.firstChild;
    }
    else {
      RelationList rel = findRelation(relationName);
      if (rel == null) return null;
      else return rel.getFirst();
    }
  }

  public synchronized Model getLastChild(QName relationName) {
    if (relationName == null) {
      return this.lastChild;
    }
    else {
      RelationList rel = findRelation(relationName);
      if (rel == null) return null;
      else return rel.getLast();
    }
  }

  /**
   * Returns current index of model within given relation. Be careful using that index. It will
   * change when adding/removing children to/from this model!
   * @param relationName  name of the relation to search for child or null to search all children
   * @param model        the child to search for
   * @return model's index or -1 if not found
   */
  public synchronized int indexOf(QName relationName, Model model) {
    Model child = getFirstChild(relationName);
    int index = 0;
    while (child != null) {
      if (child == model) return index;
      child = child.nextSibling(relationName);
      index++;
    }
    return -1;
  }

  /**
   * Returns the first child having ID childID or null if not found
   * @param relationName  name of the relation to search within or null for
   *                      searching all relationNames
   * @param childKey
   * @return the first child having ID childID or null if not found
   */
  public synchronized Model getChildModel(QName relationName, Key childKey) {
    if (childKey == null) {
      throw new IllegalArgumentException( "Child model's key must not be null!" );
    }
    Model child = getFirstChild(relationName);
    while (child != null) {
      if (childKey.equals(child.key())) return child;
      child = child.nextSibling(relationName);
    }
    return null;
  }

  /**
   * Returns child at index within relation
   * @param relationName name of the relation, max be null
   * @param index index of child within relation or within all children if relationName == null
   * @return child or null if index is out of range
   */
  public Model getChildAt( QName relationName, int index ) {
    Model child = getFirstChild( relationName );
    int i = 0;
    while (child != null) {
      if (i == index) return child;
      child = child.nextSibling(relationName);
      i++;
    }
    return null;
  }

  /**
   * Returns the first model of the given relationName and removes
   * it from this child list.
   * @param relationName the relation of the child
   * @return the first model of the given relationID or null if none
   */
  public synchronized Model pickFirst(QName relationName) {
    Model child = getFirstChild(relationName);
    if (child != null) {
      remove(relationName, child);
    }
    return child;
  }


  /**
   * Removes the first occurence of model within relationID from this ChildList.
   * Returns true if model was found, false otherwise.
   * @param relationName  the model's relationID, may be null
   * @param model         the model to be removed
   * @return true if model was found (and removed), false otherwise
   */
  public synchronized boolean remove(QName relationName, Model model ) {
    Model prevInRel = model.prevSibling(relationName);
    Model prev = model.prevSibling(null);
    Model next = model.nextSibling(null);
    Model nextInRel = model.nextSibling(relationName);

    if (prevInRel != null) {
      prevInRel.changeSibling( Model.NEXT_IN_REL, nextInRel );
    }
    if (prev != null) {
      prev.changeSibling( Model.NEXT, next );
    }
    model.setParent(null, null);  //todo: wieso dazwischen???
    if (next != null) {
      next.changeSibling( Model.PREV, prev );
    }
    if (nextInRel != null) {
      nextInRel.changeSibling( Model.PREV_IN_REL, prevInRel );
    }
    if (relationName != null) {
      RelationList rel = findRelation(relationName);
      rel.setSize(rel.getSize()-1);
      if (rel.getFirst() == model) {
        rel.setFirst(nextInRel);
      }
      if (rel.getLast() == model) {
        rel.setLast(prevInRel);
      }
    }
    this.size--;
    if (this.firstChild == model) {
      this.firstChild = next;
    }
    if (this.lastChild == model) {
      this.lastChild = prev;
    }
    return true;
  }

  /**
   * Removes all children of the given realtion.
   * If relationID is null all relation's children are removed.
   * @param relationName the realtion
   */
  public synchronized void removeAll(QName relationName) {
    Model child, prev;
    child = getLastChild(relationName);
    prev = child;
    if (relationName == null) {
      while (prev != null) {
        prev = child.prevSibling(relationName);
        child.setParent(null, null);
        child = prev;
      }
      this.firstChild = null;
      this.lastChild = null;
      this.size = 0;
      for (int i = 0; i < this.relations.size(); i++)  {
        RelationList rel = (RelationList) relations.elementAt(i);
        rel.setFirst(null);
        rel.setLast(null);
        rel.setSize(0);
      }
    }
    else {
      while (prev != null) {
        prev = child.prevSibling(relationName);
        remove(relationName, child);
        child = prev;
      }
    }
  }

  /**
   * Swap positions of m1 and m2 in this childlist
   * @param m1 one of this ChildList's child models
   * @param m2 another of this ChildList's child models
   */
  public synchronized void swap( Model m1, Model m2 ) {
    // Following cases must work:
    //  m1 is direct next to m2
    //  m2 is direct next to m1
    //  there is at least one other model between m1 and m2

    checkParent( m1 );
    checkParent( m2 );
    QName relationName = m1.getParentRelationID();

    Model m1Next = m1.nextSibling(null);
    Model m2Next = m2.nextSibling(null);

    // if m1 was not next to m2 move m1 before old next of m2
    if (m2Next != m1) {
      remove( relationName, m1 );
      insert( relationName, m1, m2Next );
    }

    // if m2 was not next to m1 move m2 before old next of m1
    if (m1Next != m2) {
      relationName = m2.getParentRelationID();
      remove( relationName, m2 );
      insert( relationName, m2, m1Next );
    }
  }

  private void checkParent( Model model ) {
    if (model.getParent( false ) != parent) {
      throw new IllegalArgumentException( "Model has a different parent than this child list" );
    }
  }

  /**
   * Sorts the given relation
   * @param relationName name of the relation to be sorted
   * @param comparator   the comparator tio use for comparing models of relationName
   * @return true if childlist was modified, i.e. at least two childs had to be swapped 
   */
  public boolean sort( QName relationName, Comparator<Model> comparator ) {
    boolean modified = false;
    int count = size( relationName );
    if (count > 1) {
      Model start = getFirstChild( relationName );
      Model end = getLastChild( relationName );
      while (start != end) {
        Model current = end;
        while (current != start) {
          Model prev = current.prevSibling( relationName );
          if (comparator.compare( prev, current ) == 1) {
            swap( prev, current );
            modified = true;
            if (prev == start) start = current;
            if (current == end) end = prev;
          }
          else {
            current = prev;
          }
        }
        start = start.nextSibling( relationName );
      }
    }
    return modified;
  }
}
