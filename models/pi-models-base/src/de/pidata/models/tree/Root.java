/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.Relation;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.AnyElement;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;

import java.util.Vector;

public class Root extends AbstractModel {

  public static final Namespace NAMESPACE = Namespace.getInstance("de.pidata.models");
  public static final QName ID_ROOT = NAMESPACE.getQName("Root");

  public static DefaultComplexType createRootType() {
    DefaultComplexType type = new DefaultComplexType(ID_ROOT, Root.class.getName(), 0);
    type.addRelation( AnyElement.ANY_ELEMENT, AnyElement.ANY_TYPE, 0, Integer.MAX_VALUE);
    return type;
  }

  private ChildList   rootModels = new ChildList(this);
  private Vector      resolveJobs = new Vector();

  public Root() {
    this(ID_ROOT);
  }

  public Root(QName processID) {
    this(processID, createRootType());
  }

  public Root(QName processID, DefaultComplexType type) {
    super(processID, type);
  }

  /**
   * Returns this model's parent or null if this model does not have a parent
   *
   * @return this model's parent or null
   * @param useCloneSource
   */
  public Model getParent( boolean useCloneSource ) {
    return null;
  }

  /**
   * Set the parent Model for this Model.
   *
   * @param parent the parent Model to be set.
   * @param parentRelationID this model's relationID within parent
   */
  public void setParent(Model parent, QName parentRelationID) {
    throw new IllegalArgumentException("Cannot set a Root's parent!");
  }

  /**
   * Returns this Model's element defiition.
   *
   * @return this Model's element defiition
   */
  public Type type() {
    return this.type;
  }

  /**
   * Updates this Model by replacing all attribute values with the
   * attribute values of updateModel. UpdateModel must have the same
   * type as this Model.
   *
   * @param updateModel the Model containing the new attribute values
   * @return true if at least one attribute has been modified by this update
   */
  public boolean update( Model updateModel ) {
    if (updateModel.type() != this.type()) {
      throw new IllegalArgumentException( "Can't update this Model (type="+type()+") with Model having different type="+updateModel.type() );
    }
    // do nothing, because we have no attributes
    return false;
  }

  /**
   * Adds a new relation to this root's child relationNames
   * @param relation teh new relation
   */
  public void addRelation(Relation relation) {
    ((DefaultComplexType) this.type).addRelation(relation);
  }

  /**
   * Returns the attribute value for the given attrName.
   *
   * @param attrName the ID for the value to be returned
   * @return the attribute value for the given attrName or null if attribute is not known
   */
  public Object get(QName attrName) {
    return null;
  }

  /**
   * Sets the value for the attribute identified by attributeID. It depends on the model's
   * implementation how to map elementIDs to attributes.
   *
   * @param attributeID the ID for the attribute value to be set
   * @param value       the new value for the attribute identified by attributeID
   * @throws ValidationException allways thrown - root's attributes are read only
   */
  public void set(QName attributeID, Object value) throws ValidationException {
    throw new ValidationException( this.type, this.key, attributeID, ValidationException.ERROR_ATTR_READONLY );
  }

  /**
   * Returns current index of child within given relation. Be careful using that index. It will
   * change when adding/removing children to/from this model!
   *
   * @param relationID ID if the relation to search for child
   * @param child      the child to search for
   * @return child's index or -1 if not found
   */
  public int indexOfChild(QName relationID, Model child) {
    return rootModels.indexOf(relationID, child);
  }

  /**
   * Adds childModel to this Model. If child relation is a composition this Model is set as child's parent.
   *
   * @param relationID the relation ID to add childModel to
   * @param childModel the new child model
   */
  public void add(QName relationID, Model childModel) {
    int errorID = ((DefaultComplexType) this.type).allowsChild(relationID, childModel.type());
    if (errorID != ValidationException.NO_ERROR) {
      throw new IllegalArgumentException("Cannot add child: "+type.getErrorMessage(errorID));
    }
    //TODO folgende Pr�fung sollte eingebaut werden, tut aber z.B. mit WSDLLoader nicht
    //QName key = childModel.name();
    //for (int i = this.rootModels.size()-1; i >= 0; i--) {
    //  if (key == this.rootModels.getChildModel(i).name()) {
    //    throw new IllegalArgumentException("Child alread exists ID="+key);
    //  }
    //}
    this.rootModels.add(relationID, childModel);
    fireDataAdded(relationID, childModel);
  }

  public void insert(QName relationID, Model childModel, Model beforeChild) {
    rootModels.insert( relationID, childModel, beforeChild );
    fireDataAdded(relationID, childModel);
  }

  /**
   * Returns the count of this Model's when using relation ID as filter. If filter is null the
   * count of all children is returned
   *
   * @param  relationID the relation ID to use as filter, may be null
   * @return the count of this Model's when using relation ID as filter
   */
  public int childCount(QName relationID) {
    return this.rootModels.size(relationID);
  }

  /**
   * Returns an iterator over this Model's children having relation ID as filter. If filter is null an
   * iterator over all children is returned.
   *
   * @param  relationName  name of the relation to use as filter, may be null
   * @param filter                 the filter to use for skipping not matching children, leave null to get all children of relation
   * @return an iterator over this Model's children filtered by relation and filter
   */
  public ModelIterator iterator( QName relationName, Filter filter ) {
    return new ModelIteratorChildList( this.rootModels, relationName, filter );
  }

  /**
   * Adds/Replaces childModel to this model
   *
   * @param relationID the relation ID to add childModel to
   * @param value      the new child model or value
   */
  public void replace(QName relationID, Object value) {
    Model childModel;
    if (value instanceof Model) {
      childModel = (Model) value;
    }
    else {
      Type childType = ((ComplexType) type).getChildType( relationID );
      childModel = new SimpleModel(childType, value);
    }
    Model oldValue = rootModels.getChildModel(relationID, childModel.key());
    if (oldValue != null) {
      remove( relationID, childModel );
    }
    add(relationID, childModel);
  }

  /**
   * Removes childModel from this Model's relation relationID. If child relation is a composition child's parent is set to null.
   *
   * @param relationID the relation ID to remove childModel from
   * @param childModel  the child ndoe to remove
   * @return true if model was found (and removed), false otherwise
   */
  public boolean remove(QName relationID, Model childModel) {
    Model next = childModel.nextSibling( relationID );
    boolean result = this.rootModels.remove(relationID, childModel);
    fireDataRemoved( relationID, childModel, next );
    return result;
  }

  /**
   * Removes all children of the given realtion.
   * If relationID is null all relation's children are removed.
   * @param relationID the realtion
   */
  public void removeAll(QName relationID) {
    this.rootModels.removeAll(relationID);
    fireDataRemoved( relationID, null, null );
  }

  public boolean sort( QName relationName, Comparator comparator ) {
    if (rootModels.sort( relationName, comparator )) {
      fireDataChanged( relationName, null, null );
    }
    return true;
  }

//--------------------------------------------------------------------------
  // Reference resolving
  //--------------------------------------------------------------------------

  /**
   * Adds a new resolve job. Such jobs are created while loading/binding
   * e.g. of xml data. After binding has finished Root resolves all
   * jobs registered while binding.
   * @param owner       the model which needs reference resolving
   * @param attributeID the ID of the attribute to be resolved
   * @param reference   the string to be converted into a model reference
   */
  public void addResolveJob(Model owner, QName attributeID, QName reference) {
    ResolveJob job = new ResolveJob(owner, attributeID, reference);
    this.resolveJobs.addElement(job);
  }

  public Model clone( Key newKey, boolean deep, boolean linked ) {
    throw new RuntimeException( "Model class does not support cloning." );
  }
}
