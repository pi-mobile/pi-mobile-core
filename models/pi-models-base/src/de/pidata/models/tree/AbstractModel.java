/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.models.types.ComplexType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.NamespaceTable;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public abstract class AbstractModel implements Model {

  protected Key key;
  protected Type type;
  private Model parent;
  private QName parentRelationID;
  protected Object content;
  protected NamespaceTable namespaceTable;
  private Model next, prev, nextInRel, prevInRel;
  private Model cloneSource;
  protected boolean dirty = false;

  private EventSender eventSender = new EventSender( this );

  public AbstractModel( Key key, Type type ) {
    if (type == null) {
      throw new IllegalArgumentException( "Type must not be null, key=" + key );
    }
    this.key = key;
    this.type = type;
    // default for parent relation ID is typeID
    this.parentRelationID = type.name();
  }

  /**
   * Returns this Model's type definition
   *
   * @return this Model's type definition
   */
  public Type type() {
    return this.type;
  }

  /**
   * Returns an Enumeration over all attribute names assigned to anyAtribute
   *
   * @return an Enumeration of QName
   */
  public Enumeration anyAttributeNames() {
    return EmptyEnumerator.getInstance();
  }

  /**
   * Returns true if attributeName is readOnly
   *
   * @return true if attributeName is readOnly
   */
  public boolean isReadOnly( QName attributeName ) {
    return false;
  }

  /**
   * Returns true if at least one of this model's attribes or children has been changed
   *
   * @return true if this model's has been changed
   */
  public boolean isDirty() {
    return this.dirty;
  }

  /**
   * Returns this Model's key, may be null
   *
   * @return this Model's name, may be null
   */
  public Key key() {
    return this.key;
  }

  /**
   * Adds listener to this Model's listener list
   *
   * @param listener the listener to be added
   */
  public void addListener( EventListener listener ) {
    this.eventSender.addListener( listener );
  }

  /**
   * Removes listener from this Model's listener list
   *
   * @param listener the listener to be removed
   */
  public void removeListener( EventListener listener ) {
    this.eventSender.removeListener( listener );
  }

  /**
   * Returns this model's parent or null if this model does not have a parent
   *
   * @param useCloneSource
   * @return this model's parent or null
   */
  public Model getParent( boolean useCloneSource ) {
    if ((parent == null) && useCloneSource) {
      if (cloneSource == null) {
        return null;
      }
      else {
        Model sourceParent = cloneSource.getParent( true );
        if (sourceParent == null) return null;
        else return new ReadOnlyWrapper( sourceParent );
      }
    }
    else {
      return this.parent;
    }
  }

  public Model cloneSource() {
    return cloneSource;
  }

  public void clonedFrom( Model cloneSource ) {
    this.cloneSource = cloneSource;
  }

  /**
   * Creates an XPath from fromAncestor to this model.
   *
   * @param fromAncestor the ancestor from which the XPath will start
   * @return an XPath
   */
  public StringBuffer createPath( Model fromAncestor ) {
    if (this == fromAncestor) {
      return new StringBuffer( "." );
    }
    else if (this.parent == null) {
      if (fromAncestor == null) {
        return new StringBuffer();
      }
      else {
        throw new IllegalArgumentException( "Model name=" + fromAncestor + " is no ancestor of this model." );
      }
    }
    else {
      StringBuffer result = parent.createPath( fromAncestor );
      QName parentRelationID = this.getParentRelationID();
      int childIndex = parent.indexOfChild( parentRelationID, this ) + 1;
      String indStr = "position() = " + childIndex;
      result.append( "/" );

      NamespaceTable nsTab = namespaceTable();
      Namespace ns = fromAncestor.type().name().getNamespace();
      String prefix = nsTab.getPrefix( ns );
      if (prefix == null) {
        prefix = nsTab.createNewPrefix( ns, false );
      }
      if (!prefix.equals( "" )) {
        result.append( prefix + ":" );
      }
      result.append( parentRelationID.getName() + "[" + indStr + "]" );
      return result;
    }
  }

  /**
   * Return this model's relationID within its parent or null if parent is null
   *
   * @return this model's relationID within its parent or null
   */
  public QName getParentRelationID() {
    return this.parentRelationID;
  }

  /**
   * Set the parent Model for this Model. If parent is null all siblings are
   * cleared, too.
   *
   * @param parent           the parent Model to be set.
   * @param parentRelationID this model's relationID within parent
   */
  public void setParent( Model parent, QName parentRelationID ) {
    Model oldParent = this.parent;
    if (oldParent != null && parent != null) {
      if (parent != oldParent) {
        throw new IllegalArgumentException( "change parent not allowed: [" + this + "] from [" + oldParent + "] to [" + parent + "]" );
      }
      else if (parentRelationID != this.parentRelationID) {
        throw new IllegalArgumentException( "change relation not allowed: [" + this + "] from " + this.parentRelationID + " to " + parentRelationID );
      }
    }
    // Forbid cycles
    Model ancestor = parent;
    while (ancestor != null) {
      if (ancestor == this) {
        throw new IllegalArgumentException( "this must not be an ancestor of parent" );
      }
      ancestor = ancestor.getParent( true );
    }
    this.parent = parent;
    this.parentRelationID = parentRelationID;
    if (parent == null) {
      this.next = null;
      this.nextInRel = null;
      this.prev = null;
      this.prevInRel = null;
    }
  }

  /**
   * Returns the child for the given childID and within relationID or null if child is not present.
   *
   * @param relationID the relation to which child belongs, if null relation is ignored
   * @param childKey
   * @return child for the given elementID or null if child is not present.
   */
  public Model get( QName relationID, Key childKey ) {
    Model child;
    ModelIterator iter = iterator( relationID, null );
    while (iter.hasNext()) {
      child = iter.next();
      if ((childKey == null) || (childKey.equals( child.key() ))) {
        return child;
      }
    }
    return null;
  }

  /**
   * Returns the childs within relationID or null if child is not present.
   *
   * @param relationID the relation to which child belongs, if null relation is ignored
   * @return childs or null if childs are not present.
   */
  public Model[] getAll( QName relationID ) {
    ModelIterator iter = iterator( relationID, null );
    ModelIterator iterCount = iterator( relationID, null );

    int i;
    for (i = 0; iterCount.hasNext(); i++)
      iterCount.next();
    if (i > 0) {
      Model[] child = new Model[i];

      for (i = 0; iter.hasNext(); i++)
        child[i] = iter.next();

      return child;
    }
    return null;
  }

  /**
   * Returns this model's content or null if not available
   *
   * @return this model's content or null if not available
   */
  public Object getContent() {
    return this.content;
  }

  /**
   * Sets this model's content
   *
   * @param content this model's new content
   * @throws ValidationException if model's type does not allow any content
   *                             or content's type is wrong
   */
  public void setContent( Object content ) {
    SimpleType contentType = type.getContentType();
    if (contentType == null) {
      throw new ValidationException( this.type, this.key, null, ValidationException.CONTENT_NOT_ALLOWED );
    }
    int valid = contentType.checkValid( content );
    if (valid != ValidationException.NO_ERROR) {
      throw new ValidationException( this.type, this.key, null, valid );
    }
    Object oldContent = this.content;
    if (((oldContent != null) && (!oldContent.equals( content )))
        || (oldContent == null) && (content != null)) {
      this.content = content;
      fireDataChanged( null, oldContent, content );
    }
  }

  /**
   * Returns the index of the element in elemList having ID elementID or -1 if not found
   * @param elementID the ID of the element to find
   * @param elemList  Vector containing instances of TypedModel
   * @return the index of the element in elemList having ID elementID
   */
  protected int indexOf(QName elementID, Vector elemList) {
    int i = elemList.size() - 1;
    while ((i >= 0) && (((Model) elemList.elementAt(i)).key() != elementID)) i--;
    return i;
  }

  /**
   * Changes the siblingType to given sibling
   *
   * @param siblingType the type of sibling: NEXT, PREV, PREV_IN_REL, NEXT_IN_REL
   * @param sibling     the new sibling
   */
  public void changeSibling(int siblingType, Model sibling) {
    if ((sibling != null) && (sibling.getParent( false ) != parent)) {
      throw new IllegalArgumentException("Sibling must have same parent!");
    }
    switch (siblingType) {
      case NEXT: next = sibling; return;
      case PREV: prev = sibling; return;
    }

    if ((sibling != null) && (sibling.getParentRelationID() != parentRelationID)) {
      throw new IllegalArgumentException("Sibling in relation must have same parent relation!");
    }
    switch (siblingType) {
      case NEXT_IN_REL: nextInRel = sibling; return;
      case PREV_IN_REL: prevInRel = sibling; return;
    }
  }

  /**
   * Find the next sibling within given relation. If relationID is null, search all siblings.
   * @param relationID the relation to search or null to search all siblings
   * @return next sibling or null if relation has no more siblings
   */
  public Model nextSibling(QName relationID) {
    if (relationID == null) {
      return this.next;
    }
    if (relationID == parentRelationID) {
      return this.nextInRel;
    }
    Model nextChild = this.next;
    while (nextChild != null) {
      if (nextChild.getParentRelationID() == relationID) {
        return nextChild;
      }
      nextChild = nextChild.nextSibling(null);
    }
    return null;
  }

  /**
   * Find the preceeding sibling within given relation. If relationID is null, search all siblings.
   * @param relationID the relation to search or null to search all siblings
   * @return previous sibling or null if relation has no preceeding sibling
   */
  public Model prevSibling(QName relationID) {
    if (relationID == null) {
      return this.prev;
    }
    if (relationID == parentRelationID) {
      return this.prevInRel;
    }
    Model prevChild = this.prev;
    while (prevChild != null) {
      if (prevChild.getParentRelationID() == relationID) {
        return prevChild;
      }
      prevChild = prevChild.prevSibling(null);
    }
    return null;
  }

  /**
   * Find the first child within given relation. If relationID is null, search all childre.
   *
   * @param relationID the relation to search or null to search all children
   * @return first child or null if relation has no children
   */
  public Model firstChild(QName relationID) {
    ModelIterator iter = iterator(relationID, null);
    if (iter.hasNext()) return iter.next();
    else return null;
  }

  /**
   * Find the last child within given relation. If relationID is null, search all childre.
   * @param relationID the relation to search or null to search all children
   * @return last child or null if relation has no children
   */
  public Model lastChild(QName relationID) {
    Model iterChild;
    Model prevChild = null;
    for (ModelIterator iter = iterator(relationID, null); iter.hasNext(); ) {
      iterChild = iter.next();
      prevChild = iterChild;
    }

    return prevChild;
  }

  /**
   * Sets this Models's own namespaceTable. If it already exists IllegalArgumentException is thrown.
   *
   * @throws IllegalArgumentException if own namespaceTable already exists
   */
  public void ownNamespaceTable( NamespaceTable namespaceTable ) {
    if ((this.namespaceTable == null) || (this.namespaceTable.size() == 0)) {
      this.namespaceTable = namespaceTable;
      this.namespaceTable.setOwner( this );
    }
    else {
      throw new IllegalArgumentException( "Must not replace existing namespace table" );
    }
  }

  /**
   * Returns a table containing all namespace definitions for this model.
   * If this model does not have it's own NamespaceTable the parent's
   * NamespaceTable is returned. If this model does not have a parent and
   * namespaceTable does not exist, a new own namespaceTable is created.
   *
   * @return this model's NamespaceTable, never null
   */
  public NamespaceTable namespaceTable() {
    if (namespaceTable == null) {
      Model parent = getParent( true );
      if (parent == null) {
        namespaceTable = new NamespaceTable( (NamespaceTable) null );
      }
      else {
        return parent.namespaceTable();
      }
    }
    return this.namespaceTable;
  }

  /**
   * Adds all elements of elemList to children
   * @param elemList the Vector containing the elements to be added
   * @param children the Vector to which the elements are added
   */
  protected void addAllElements(Vector elemList, Vector children) {
    for (int i = 0; i < elemList.size(); i++) {
      children.addElement(elemList.elementAt(i));
    }
  }

  /**
   * Adds all elements of elemList to children
   * @param elemTable the Hastable containing the elements to be added
   * @param children the Vector to which the elements are added
   */
  protected void addAllElements(Hashtable elemTable, Vector children) {
    for (Enumeration keyEnum = elemTable.keys(); keyEnum.hasMoreElements();) {
      children.addElement(elemTable.get(keyEnum.nextElement()));
    }
  }

  protected void fireEvent( int eventID, Object source, QName elementID, Object oldValue, Object newValue) {
    this.eventSender.fireEvent( eventID, source, elementID, oldValue, newValue );
    Model parenModel = getParent( false );
    if ((parenModel != null) && (parenModel instanceof AbstractModel)) {
      ((AbstractModel) parenModel).fireEvent( eventID, source, elementID, oldValue, newValue);
    }
  }

  protected void fireDataChanged(QName elementID, Object oldValue, Object newValue) {
    fireEvent( EventListener.ID_MODEL_DATA_CHANGED, this, elementID, oldValue, newValue );
  }

  protected void fireDataAdded(QName elementID, Object newValue) {
    fireEvent( EventListener.ID_MODEL_DATA_ADDED, this, elementID, null, newValue );
  }

  /**
   *
   * @param relationID the relation from which oldValue was removed, if null
   *                   all realtions are affected
   * @param oldValue   the removed child, if null all children of relationID
   *                   have been removed
   * @param nextValue  the next child after removed child
   */
  protected void fireDataRemoved( QName relationID, Object oldValue, Model nextValue ) {
    fireEvent( EventListener.ID_MODEL_DATA_REMOVED, this, relationID, oldValue, nextValue );
  }

  /**
   * Returns the factory this Model has been built with
   * @return the factory this Model has been built with
   */
  public ModelFactory getFactory() {
    return ModelFactoryTable.getInstance().getFactory( this.type.name().getNamespace() );
  }

  /**
   * Creates a deep copy of this model an assigns it the new key <code>id</code>.
   * If <code>id</code> is null this model's id is used.
   * @param newKey  the new key for the clone or null if clone should have same key
   * @param deep    if true, makes a deep copy
   * @param linked  if true clone is recursively linked to its clone source
   * @return deep copy of this model
   * @throws RuntimeException This implementation throws always an Exception.
   */
  public Model clone( Key newKey, boolean deep, boolean linked ) {
    throw new RuntimeException( "Model class does not support cloning." );
  }

  /**
   * By default sort() is not supported, so always returns false
   * @param relationName name of the relation to be sorted
   * @param comparator     the SortRule to use for sorting
   * @return true if sort was successful, false if not successful or not supported
   */
  public boolean sort( QName relationName, Comparator<? extends Model> comparator ) {
    return false;
  }

  private boolean isEqual( Object o1, Object o2 ) {
    if (o1 == null) {
      return (o2 == null);
    }
    else {
      return o1.equals( o2 );
    }
  }

  /**
   * Compares this Model with another Object. If object is not a Model false is returned.
   * Models having SimpleType are equal if type and content are equal.
   * For Models having ComplexType aditionally key, attributes and children (recursive) must be equal.
   * @param object  the object to compare with
   * @return true if this and object are equal
   */
  public boolean equals( Object object ) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof Model)) {
      return false;
    }

    //--- Simple and complex models
    Model other = (Model) object;
    if (!other.type().equals(type())) return false;
    if (!isEqual(other.getContent(), getContent())) return false;
    if (this instanceof SimpleType) return true;

    //--- Attributes of complex Model
    if (!isEqual( other.key(), key() )) return false;
    if (type instanceof ComplexType) {
      ComplexType type = (ComplexType) type();
      for (int i = type.keyAttributeCount(); i < type.attributeCount(); i++) {
        QName attrName = type.getAttributeName( i );
        if (!isEqual( this.get( attrName ), other.get( attrName ) )) return false;
      }
    }

    //--- Children of complex Model
    if (this.childCount( null ) != other.childCount( null )) {
      return false;
    }
    ModelIterator thisIter = this.iterator(null, null);
    ModelIterator otherIter = other.iterator(null, null);
    while (thisIter.hasNext()) {
      if (otherIter.hasNext()) {
        if (!thisIter.next().equals( otherIter.next() )) {
          return false;
        }
      }
      else {
        return false;
      }
    }
    return true;
  }
}
