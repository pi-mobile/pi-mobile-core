/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.NamespaceTable;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

import java.util.Enumeration;

/**
 * This class wraps around another model and throws exceptions for all writing methods.
 * Methods returning parents or children return also red only wrapped Models.
 */
public class ReadOnlyWrapper implements Model, Transient {

  private Model wrappedModel;

  public ReadOnlyWrapper( Model wrapModel ) {
    this.wrappedModel = wrapModel;
  }

  public Model wrappedModel() {
    return wrappedModel;
  }

  /**
   * Returns this Model's ID. That ID is also used to identify this Model
   * within its parent Model.
   *
   * @return this Model's ID, never null
   */
  public Key key() {
    return wrappedModel.key();
  }

  /**
   * Returns this Model's element defiition.
   *
   * @return this Model's element defiition
   */
  public Type type() {
    return wrappedModel.type();
  }

  /**
   * Returns an Enumeration over all attribute names assigned to anyAtribute
   *
   * @return an Enumeration of QName
   */
  public Enumeration anyAttributeNames() {
    return wrappedModel.anyAttributeNames();
  }

  /**
   * Returns this model's parent or null if this model does not have a parent
   *
   * @return this model's parent or null
   * @param useCloneSource
   */
  public Model getParent( boolean useCloneSource ) {
    Model parent = wrappedModel.getParent( true );
    if (parent instanceof ReadOnlyWrapper) {
      return parent;
    }
    else {
      return new ReadOnlyWrapper( parent );
    }
  }

  /**
   * Creates an XPath from fromAncestor to this model
   *
   * @param fromAncestor the ancestor from which the XPath will start
   * @return an XPath
   */
  public StringBuffer createPath( Model fromAncestor ) {
    if (fromAncestor instanceof ReadOnlyWrapper) {
      fromAncestor = ((ReadOnlyWrapper) fromAncestor).wrappedModel();
    }
    return wrappedModel.createPath( fromAncestor );
  }

  /**
   * Return this model's relationID within its parent or null if parent is null
   *
   * @return this model's relationID within its parent or null
   */
  public QName getParentRelationID() {
    return wrappedModel.getParentRelationID();
  }

  /**
   * Set the parent Model for this Model. If parent is null all siblings are
   * cleared, too.
   *
   * @param parent           the parent Model to be set or null
   * @param parentRelationID this model's relationID within parent
   */
  public void setParent( Model parent, QName parentRelationID ) {
    throw new IllegalArgumentException( "This model is read only (wrapped)" );
  }

  /**
   * Returns the attribute value for the given attrName.
   *
   * @param attrName the ID for the value to be returned
   * @return the attribute value for the given attrName or null if attribute is not known
   */
  public Object get( QName attrName ) {
    return wrappedModel.get( attrName );
  }

  /**
   * Returns the child for the given childKey and within relationName or null if child is not present.
   *
   * @param relationName the relation to which child belongs, if null relation is ignored
   * @param childKey     key of the child to be returned, if null the first child is returned
   * @return child for the given elementID or null if child is not present.
   */
  public Model get( QName relationName, Key childKey ) {
    Model child = wrappedModel.get( relationName, childKey );
    if (child == null) return null;
    else return new ReadOnlyWrapper( child );
  }

  /**
   * Returns true if attributeName is readOnly
   *
   * @return true if attributeName is readOnly
   */
  public boolean isReadOnly( QName attributeName ) {
    return true;
  }

  /**
   * Sets the value for the attribute identified by attributeName. It depends on the model's
   * implementation how to map elementIDs to attributes.
   *
   * @param attributeName the ID for the attribute value to be set
   * @param value         the new value for the attribute identified by attributeName
   * @throws de.pidata.models.tree.ValidationException
   *          if this model is not writeable, attributeName is unknown
   *          or value is not valid for attribute's type
   */
  public void set( QName attributeName, Object value ) throws ValidationException {
    throw new IllegalArgumentException( "This model is read only (wrapped)" );
  }

  /**
   * Returns this model's content or null if not available
   *
   * @return this model's content or null if not available
   */
  public Object getContent() {
    return wrappedModel.getContent();
  }

  /**
   * Sets this model's content
   *
   * @param content this model's new content
   * @throws de.pidata.models.tree.ValidationException
   *          if model's type does not allow any content
   *          or content's type is wrong
   */
  public void setContent( Object content ) throws ValidationException {
    throw new IllegalArgumentException( "This model is read only (wrapped)" );
  }

  /**
   * Returns the count of this Model's when using relation ID as filter. If filter is null the
   * count of all children is returned
   *
   * @param relationName the relation ID to use as filter, may be null
   * @return the count of this Model's when using relation ID as filter
   */
  public int childCount( QName relationName ) {
    return wrappedModel.childCount( relationName );
  }

  /**
   * Adds childModel to this Model. If child relation is a composition this Model is set as child's parent.
   *
   * @param relationName the relation ID to add childModel to
   * @param childModel   the new child model
   */
  public void add( QName relationName, Model childModel ) {
    throw new IllegalArgumentException( "This model is read only (wrapped)" );
  }

  /**
   * Adds/Replaces childModel to this model
   *
   * @param relationName the relation ID to add childModel to
   * @param value        the new child model or value
   */
  public void replace( QName relationName, Object value ) {
    throw new IllegalArgumentException( "This model is read only (wrapped)" );
  }

  /**
   * Removes childModel from this Model's relation relationName. If child relation is a composition child's parent is set to null.
   *
   * @param relationName the relation ID to remove childModel from
   * @param childModel   the child ndoe to remove
   * @return true if model was found (and removed), false otherwise
   */
  public boolean remove( QName relationName, Model childModel ) {
    throw new IllegalArgumentException( "This model is read only (wrapped)" );
  }

  /**
   * Removes all children of the given realtion.
   * If relationName is null all relation's children are removed.
   *
   * @param relationName the realtion
   */
  public void removeAll( QName relationName ) {
    throw new IllegalArgumentException( "This model is read only (wrapped)" );
  }

  /**
   * Returns an iterator over this Model's children having relation ID as filter. If filter is null an
   * iterator over all children is returned.
   *
   * @param relationName name of the relation to use as filter, may be null
   * @param filter               the filter to use for skipping not matching children, leave null to get all children of relation
   * @return an iterator over this Model's children filtered by relation and filter
   */
  public ModelIterator iterator( QName relationName, Filter filter ) {
    return new ReadOnlyIterator( wrappedModel.iterator( relationName, filter ) );
  }

  /**
   * Creates a deep copy of this model an assigns it the new key <code>id</code>.
   * If <code>id</code> is null this model's id is used.
   *
   * @param newKey the new key for the clone or null if clone should have same key
   * @param deep   if true, makes a deep copy
   * @param linked if true clone is recursively linked to its clone source
   * @return copy of this model
   * @throws RuntimeException if this model does not support cloning
   */
  public Model clone( Key newKey, boolean deep, boolean linked ) {
    if (linked) {
      throw new IllegalArgumentException( "Cannot create a linked clone of a read only wrapped model" );
    }
    return wrappedModel.clone( newKey, deep, linked );
  }

  /**
   * Adds listener to this Model's listener list
   *
   * @param listener the listener to be added
   */
  public void addListener( EventListener listener ) {
    wrappedModel.addListener( listener );
  }

  /**
   * Removes listener from this Model's listener list
   *
   * @param listener the listener to be removed
   */
  public void removeListener( EventListener listener ) {
    wrappedModel.removeListener( listener );
  }

  /**
   * Find the next sibling within given relation. If relationName is null, search all siblings.
   *
   * @param relationName the relation to search or null to search all siblings
   * @return next sibling or null if relation has no more siblings
   */
  public Model nextSibling( QName relationName ) {
    return new ReadOnlyWrapper( wrappedModel.nextSibling( relationName ) );
  }

  /**
   * Find the preceeding sibling within given relation. If relationName is null, search all siblings.
   *
   * @param relationName the relation to search or null to search all siblings
   * @return previous sibling or null if relation has no preceeding sibling
   */
  public Model prevSibling( QName relationName ) {
    return new ReadOnlyWrapper( wrappedModel.prevSibling( relationName ) );
  }

  /**
   * Returns current index of child within given relation. Be careful using that index. It will
   * change when adding/removing children to/from this model!
   *
   * @param relationName ID if the relation to search for child
   * @param child        the child to search for
   * @return child's index or -1 if not found
   */
  public int indexOfChild( QName relationName, Model child ) {
    if (child instanceof ReadOnlyWrapper) {
      child = ((ReadOnlyWrapper) child).wrappedModel();
    }
    return wrappedModel.indexOfChild( relationName, child );
  }

  /**
   * Find the first child within given relation. If relationName is null, search all childre.
   *
   * @param relationName the relation to search or null to search all children
   * @return first child or null if relation has no children
   */
  public Model firstChild( QName relationName ) {
    return new ReadOnlyWrapper( wrappedModel.firstChild( relationName ) );
  }

  /**
   * Find the last child within given relation. If relationName is null, search all childre.
   *
   * @param relationName the relation to search or null to search all children
   * @return last child or null if relation has no children
   */
  public Model lastChild( QName relationName ) {
    return new ReadOnlyWrapper( wrappedModel.lastChild( relationName ) );
  }

  /**
   * Sets this Models's own namespaceTable. If it already exists IllegalArgumentException is thrown.
   *
   * @throws IllegalArgumentException if own namespaceTable already exists
   */
  public void ownNamespaceTable( NamespaceTable namespaceTable ) {
    throw new IllegalArgumentException( "This model is read only (wrapped)" );
  }

  /**
   * Returns a table containing all namespace definitions for this model.
   * If this model doen not have it's own NamespaceTable the parent's
   * NamespaceTable is returned. In a complete model tree at least the
   * root model should have a not null NamespaceTable.
   *
   * @return this model's NamespaceTable
   */
  public NamespaceTable namespaceTable() {
    return wrappedModel.namespaceTable();
  }

  /**
   * Changes the siblingType to given sibling
   *
   * @param siblingType the type of sibling: NEXT, PREV, PREV_IN_REL, NEXT_IN_REL
   * @param sibling     the new sibling
   */
  public void changeSibling( int siblingType, Model sibling ) {
    throw new IllegalArgumentException( "This model is read only (wrapped)" );
  }

  /**
   * Updates this Model by replacing all attribute values with the
   * attribute values of updateModel. UpdateModel must have the same
   * type as this Model.
   *
   * @param updateModel the Model containing the new attribute values
   * @return true if at least one attribute has been modified by this update
   */
  public boolean update( Model updateModel ) {
    throw new IllegalArgumentException( "This model is read only (wrapped)" );
  }

  public void insert( QName relationID, Model childModel, Model beforeChild ) {
    throw new IllegalArgumentException( "This model is read only (wrapped)" );
  }

  /**
   * Returns the model this model was cloned from or null
   *
   * @return the model this model was cloned from or null
   */
  public Model cloneSource() {
    throw new IllegalArgumentException( "Clone source not supported for ReadOnlyWrapper." );
  }

  /**
   * Only for internal use: Called by cloneModel() with the model this model was cloned from
   *
   * @param cloneSource the model this model was cloned from
   */
  public void clonedFrom( Model cloneSource ) {
    throw new IllegalArgumentException( "Clone source not supported for ReadOnlyWrapper." );
  }

  /**
   * Sorts all children in relationName.
   *
   * @param relationName name of the relation to be sorted
   * @param comparator   the SortRule to use for sorting
   * @return true if sort was successful, false if not successful or not supported
   */
  public boolean sort( QName relationName, Comparator comparator ) {
    throw new IllegalArgumentException( "This model is read only (wrapped)" );
  }

  //------------------------------------------------------------------------------------
  // Interface Transient
  //------------------------------------------------------------------------------------

  public ComplexType transientType() {
    if (wrappedModel instanceof Transient) {
      return ((Transient) wrappedModel).transientType();
    }
    else {
      return null;
    }
  }

  public Object transientGet( int transientIndex ) {
    if (wrappedModel instanceof Transient) {
      return ((Transient) wrappedModel).transientGet( transientIndex );
    }
    else {
      return null;
    }
  }

  public void transientSet( int transientIndex, Object value ) {
    throw new IllegalArgumentException( "This model is read only (wrapped)" );
  }
}
