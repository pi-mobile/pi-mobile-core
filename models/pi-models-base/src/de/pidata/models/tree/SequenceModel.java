/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.log.Logger;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;
import de.pidata.qnames.Key;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;

public abstract class SequenceModel extends AbstractModel implements Transient {

  private Object[] attributes;
  protected ChildList children;
  protected Hashtable<QName, Object> anyAttribs; // ToDo: check: can we use HashMap? Does it need to be Synchronized?
  protected final String attrSemaphore = "PI";

  /**
   * @deprecated Use other constructor with anyAttribs == null
   * @param type
   * @param attributes
   * @param children
   */
  @Deprecated
  protected SequenceModel( Key key, ComplexType type, Object[] attributes, ChildList children) {
    this(key, type, attributes, null, children);
  }

  protected SequenceModel(Key key, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children) {
    super(key, type);
    SimpleType  attributeType;
    QName       attrName;
    Object      value;

    int keyAttrCount = type.keyAttributeCount();
    if ((keyAttrCount > 0) && (key == null)) {
      throw new IllegalArgumentException("Key must not be null: keyAttributeCount > 0 for type="+type().name());
    }
    for (int i = 0; i < keyAttrCount; i++) {
      attributeType = type.getKeyAttributeType(i);
      attrName = type.getKeyAttribute(i);
      value = key.getKeyValue(i);
      checkAttribute(attributeType, value, attrName);
    }

    if (attributes == null) {
      attributes = new Object[ type.attributeCount() ];
      for (int i = 0; i < type.attributeCount(); i++) {
        attributes[i] = type.getAttributeDefault( i );
      }
    }
    for (int i = 0; i < attributes.length; i++) {
      attributeType = type.getAttributeType( i );
      // might have placeholders
      if (type instanceof DynamicType && attributeType != null) {
        attrName = type.getAttributeName( i );
        int keyIndex = type.getKeyIndex( attrName );
        if (keyIndex < 0) {
          value = attributes[i];
          checkAttribute( attributeType, value, attrName );
        }
      }
    }
    this.attributes = attributes;

    if (anyAttribs != null) {
      AnyAttribute anyAttribute = type.getAnyAttribute();
      for (Enumeration<QName> keyEnum = anyAttribs.keys(); keyEnum.hasMoreElements(); ) {
        attrName = keyEnum.nextElement();
        attributeType = AnyAttribute.findType(attrName);
        checkAttribute(attributeType, anyAttribs.get(attrName), attrName);
      }
      this.anyAttribs = anyAttribs;
    }

    if (children == null) {
      this.children = new ChildList();
    }
    else {
      this.children = children;
    }
    this.children.setParent(this);

    initTransient();
    onInitialized();
  }

  /**
   * Returns an Enumeration over all attribute names assigned to anyAtribute
   *
   * @return an Enumeration of QName
   */
  public Enumeration<QName> anyAttributeNames () {
    if (this.anyAttribs == null) return EmptyEnumerator.getInstance();
    Enumeration<QName> result = this.anyAttribs.keys();
    if (result == null) return EmptyEnumerator.getInstance();
    else return result;
  }

  /**
   * Returns the attribute value for the given attrName.
   *
   * @param attrName the ID for the value to be returned
   * @return the attribute value for the given attrName or null if attribute is not known
   */
  public Object get(QName attrName) {
    ComplexType type = (ComplexType) type();
    synchronized (attrSemaphore) {
      int attrIndex = getAndCheckIndexOfAttribute( type, attrName );
      if (attrIndex >= 0) {
        int keyIndex = type.getKeyIndex( attrName );
        if (keyIndex >= 0) {
          return key.getKeyValue( keyIndex );
        }
        else {
          return this.attributes[attrIndex];
        }
      }
      ComplexType trType = transientType();
      if (trType != null) {
        int trIndex = trType.indexOfAttribute( attrName );
        if (trIndex >= 0) {
          return transientGet( trIndex );
        }
      }
      if (this.anyAttribs != null) {
        return this.anyAttribs.get( attrName );
      }
      else {
        return null;
      }
    }
  }

  /**
   * Returns true if attributeName is readOnly
   * @return true if attributeName is readOnly
   */
  public boolean isReadOnly( QName attributeName ) {
    int keyIndex = ((ComplexType) type()).getKeyIndex(attributeName);
    return (keyIndex >= 0);
  }

  /**
   * Sets the value for the attribute identified by attributeName. It depends on the model's
   * implementation how to map elementIDs to attributes.
   *
   * @param attributeName the ID for the attribute value to be set
   * @param value       the new value for the attribute identified by attributeName
   * @throws ValidationException if this model is not writeable, attributeName is unknown
   *                             or value is not valid for attribute's type
   */
  public void set(QName attributeName, Object value) throws ValidationException {
    Object oldValue;
    ComplexType type = (ComplexType) type();
    SimpleType attributeType;
    boolean fireEvent = false;

    synchronized (attrSemaphore) {
      int keyIndex = type.getKeyIndex( attributeName );
      if (keyIndex >= 0) {
        throw new ValidationException( this.type, this.key, attributeName, ValidationException.ERROR_KEY_ATTR_READONLY );
      }
      int attrIndex = getAndCheckIndexOfAttribute( type, attributeName );

      if (attrIndex >= 0) {
        attributeType = type.getAttributeType( attrIndex );
        oldValue = this.attributes[attrIndex];
        if (((oldValue != null) && (!oldValue.equals( value )))
            || ((oldValue == null) && (value != null))) {
          // Do not check isReadOnly( attributeName )) because readOnly state may change while update activities
          // and may lead to Exceptions not wanted
          checkAttribute( attributeType, value, attributeName );
          this.attributes[attrIndex] = value;
          fireEvent = true;
        }
      }
      else {
        ComplexType trType = transientType();
        if (trType != null) {
          int trIndex = trType.indexOfAttribute( attributeName );
          if (trIndex >= 0) {
            transientSet( trIndex, value );
            return;
          }
        }
        AnyAttribute anyAttribute = type.getAnyAttribute();
        if (anyAttribute == null) {
          throw new ValidationException( this.type, this.key, attributeName, ValidationException.ERROR_ATTR_UNKNOWN );
        }
        attributeType = AnyAttribute.findType( attributeName );
        if (attributeType == null) {
          throw new ValidationException( this.type, this.key, attributeName, ValidationException.ERROR_ANY_ATTR_UNKNOWN );
        }
        checkAttribute( attributeType, value, attributeName );
        if (this.anyAttribs == null) {
          this.anyAttribs = new Hashtable<>();
          oldValue = null;
        }
        else {
          oldValue = this.anyAttribs.get( attributeName );
        }
        if (((oldValue != null) && (!oldValue.equals( value )))
            || ((oldValue == null) && (value != null))) {
          if (isReadOnly( attributeName )) {
            throw new ValidationException( this.type, this.key, attributeName, ValidationException.ERROR_ATTR_READONLY );
          }
          this.anyAttribs.put( attributeName, value );
          fireEvent = true;
        }
      }
    }
    if (fireEvent) {
      fireDataChanged( attributeName, oldValue, value );
    }
  }

  /**
   * Retrieves the index of the attribute identified by attributeName, see {@link ComplexType#indexOfAttribute(QName)}.
   * Checks and extends the attributes array if necessary.
   * <p/>
   * Be sure to call this instead of {@link ComplexType#indexOfAttribute(QName)} if it might be called on
   * a {@link DynamicType} which may add attributes.
   *
   * @param type
   * @param attributeName
   */
  protected int getAndCheckIndexOfAttribute( ComplexType type, QName attributeName ) {
    int attrIndex = type.indexOfAttribute(attributeName);

    // maybe DynamicType has added attributes so check and extend
    if (attributes != null) {
      int newAttrCount = type.attributeCount();
      int oldAttrCount = attributes.length;
      if (oldAttrCount < newAttrCount) {
        Object[] extendedAttributes = Arrays.copyOf( attributes, newAttrCount );

        // fill with defaults for newly added attributes
        for (int i = oldAttrCount; i < newAttrCount; i++) {
          extendedAttributes[i] = type.getAttributeDefault( i );
        }

        // do further check, see Constructor
        SimpleType attributeType;
        QName attrName;
        Object value;
        for (int i = oldAttrCount; i < newAttrCount; i++) {
          attributeType = type.getAttributeType( i );
          attrName = type.getAttributeName( i );
          int keyIndex = type.getKeyIndex( attrName );
          if (keyIndex < 0) {
            value = extendedAttributes[i];
            checkAttribute( attributeType, value, attrName );
          }
        }
        attributes = extendedAttributes;
      }
    }
    return attrIndex;
  }

  /**
   * Checks attribute value if it is compatible to attributeType and converts if necessary/possible.
   * @param attributeType
   * @param value
   * @param attributeID
   * @return
   * @throws ValidationException
   */
  private Object checkAttribute( SimpleType attributeType, Object value, QName attributeID ) throws ValidationException {
    int valid = attributeType.checkValid(value);
    if (valid == ValidationException.NEEDS_CONVERSION) {
      return attributeType.convert( value );
    }
    else if (valid == ValidationException.NO_ERROR) {
      return value;
    }
    else {
      throw new ValidationException( this.type, this.key, attributeID, valid );
    }
  }

  /**
   * Returns the count of this Model's when using relation ID as filter. If filter is null the
   * count of all children is returned
   *
   * @param  relationID the relation ID to use as filter, may be null
   * @return the count of this Model's when using relation ID as filter
   */
  public int childCount(QName relationID) {
    return children.size(relationID);
  }

  /**
   * Returns current index of child within given relation. Be careful using that index. It will
   * change when adding/removing children to/from this model!
   * @param relationID  ID if the relation to search for child
   * @param child       the child to search for
   * @return child's index or -1 if not found
   */
  public int indexOfChild(QName relationID, Model child) {
    ComplexType transientType = transientType();
    if ( relationID != null && (transientType != null) && (transientType.getRelation( relationID ) != null)) {
      int index = 0;
      for (ModelIterator<Model> iter = transientChildIter( relationID, null ); iter.hasNext(); ) {
        Model transChild = iter.next();
        if (transChild == child) {
          return index;
        }
        index++;
      }
      return -1;
    }
    else {
      return children.indexOf( relationID, child );
    }
  }

  protected void checkChild( QName relationID, Model childModel ) {
    int error = ((ComplexType) this.type).allowsChild( relationID, childModel.type() );
    if (error != ValidationException.NO_ERROR) {
      String reason;
      Type expectedType = ((ComplexType) this.type).getChildType( relationID );
      if (expectedType != null) reason = expectedType.getErrorMessage( error );
      else reason = type.getErrorMessage( error );
      throw new IllegalArgumentException( "Could not add model typeID=" + childModel.type().name()
          + " to parent typeID=" + type.name() + ", reason: " + reason );
    }
  }

  /**
   * Adds childModel to this Model. If child relation is a composition this Model is set as child's parent.
   *
   * @param relationID the relation ID to add childModel to
   * @param childModel the new child model
   */
  public void add(QName relationID, Model childModel) {
    checkChild( relationID, childModel );
    children.add( relationID, childModel );
    fireDataAdded(relationID, childModel);
  }

  public void insert(QName relationID, Model childModel, Model beforeChild) {
    checkChild( relationID, childModel );
    children.insert( relationID, childModel, beforeChild );
    fireDataAdded(relationID, childModel);
  }

  /**
   * Adds/Replaces childModel to this model
   *
   * @param relationID the relation ID to add childModel to
   * @param value the new child model or value
   */
  public void replace(QName relationID, Object value) {
    Model childModel;
    if (value instanceof Model) {
      childModel = (Model) value;
    }
    else {
      Type childType = ((ComplexType) type).getChildType( relationID );
      childModel = new SimpleModel(childType, value);
    }
    Model oldValue = children.getChildModel(relationID, childModel.key());
    if (oldValue != null) {
      Model next = oldValue.nextSibling( relationID );
      children.remove(relationID, oldValue);
      fireDataRemoved( relationID, oldValue, next );
    }
    add(relationID, childModel);
  }

  public void swap (SequenceModel child1, SequenceModel child2){
    children.swap( child1, child2 );
    this.fireDataChanged( child1.getParentRelationID(), null, null );
  }

  /**
   * Removes childModel from this Model's relation relationID. If child relation is a composition child's parent is set to null.
   *
   * @param relationID the relation ID to remove childModel from
   * @param childModel  the child ndoe to remove
   */
  public boolean remove(QName relationID, Model childModel) {
    int index = children.indexOf( relationID, childModel );
    if (index >= 0) {
      Model next = childModel.nextSibling( relationID );
      boolean result = children.remove( relationID, childModel );
      fireDataRemoved( relationID, childModel, next );
      return result;
    }
    else {
      return false;
    }
  }

  /**
   * Removes all children of the given realtion.
   * If relationID is null all relation's children are removed.
   * @param relationID the realtion
   */
  public void removeAll(QName relationID) {
    children.removeAll(relationID);
    fireDataRemoved( relationID, null, null );
  }

  /**
   * Adds childModel as the only child in this Model's relation relationID. Previously added children are removed.
   * If child relation is a composition this Model is set as child's parent.
   *
   * @param relationID the relation ID to add childModel to
   * @param childModel the new child model
   */
  public void setChild(QName relationID, Model childModel) {
    if (childCount( relationID ) > 0) {
      removeAll( relationID );
    }
    if (childModel == null) {
      // TODO: does not work for substitutionGroup, i.e. allowed children with different tag
      Relation relation = ((ComplexType) type()).getRelation( relationID );
      if (relation.getMinOccurs() > 0) {
        throw new IllegalArgumentException( "Must not set child null for relation="+relationID );
      }
    }
    else {
      add( relationID, childModel );
    }
  }

  /**
   * Returns an iterator over this Model's children having relationName as filter.
   * If filter is null an iterator over all (persistent) children is returned.
   * If relationName is unknown an empty iterator is returned.
   *
   * @param relationName  name of the relation to use as filter, may be null
   * @param filter        the filter to use for skipping not matching children, leave null to get all children of relation
   * @return an iterator over this Model's children filtered by relationName and filter
   */
  public ModelIterator iterator( QName relationName, Filter filter ) {
    if (relationName == null) {
      return new ModelIteratorChildList( children, null, filter );
    }
    else {
      // check if there are elements in the relation - might be dynamically added elements as for SubstitutionGroup
      RelationList relList = children.findRelation( relationName );
      if (relList == null) {
        // no children from declared relations found so check if there might be a transient declaration
        ComplexType transientType = transientType();
        if (transientType != null) {
          Relation relation = transientType.getRelation( relationName );
          if (relation != null) {
            return transientChildIter( relationName, filter );
          }
        }
        return ModelIteratorChildList.EMPTY_ITER;
      }
      else {
        return new ModelIteratorChildList( children, relationName, filter );
      }
    }
  }

  protected Model createClone(Key newKey, Object[] newAttributes, Hashtable<QName, Object> newAnyAttribs, ChildList cList, boolean linked ) {
    ModelFactory factory = getFactory();
    if (factory == null) {
      throw new IllegalArgumentException( "ModelFactory is missing for type name="+type.name() );
    }
    Model clone = factory.createInstance( newKey, type, newAttributes, newAnyAttribs, cList );
    if (linked) clone.clonedFrom( this );
    return clone;
  }

  public Model clone( Key newKey, boolean deep, boolean linked ) {
    if (newKey == null) {
      newKey = key();
    }

    Model clone = null;
    ChildList cList = new ChildList();

    if (deep) {
      ModelIterator it = iterator(null, null);
      while (it.hasNext()) {
        Model m = it.next();
        QName relName = m.getParentRelationID();
        m = m.clone( null, deep, linked );
        cList.add( relName, m );
      }
    }
    Object[] newAttributes = new Object[attributes.length];
    for (int i=0; i<attributes.length; i++) {
      newAttributes[i] = attributes[i];
    }
    Hashtable<QName, Object> newAnyAttribs = null;
    if (anyAttribs != null) {
      newAnyAttribs = new Hashtable<>();
      for (Enumeration<QName> keyEnum = anyAttribs.keys(); keyEnum.hasMoreElements(); ) {
        QName key = keyEnum.nextElement();
        newAnyAttribs.put(key, anyAttribs.get(key));
      }
    }
    try {
      clone = createClone( newKey, newAttributes, newAnyAttribs, cList, linked );
      if (this.namespaceTable != null) {
        NamespaceTable cloneNsTab = new NamespaceTable( (NamespaceTable) null );
        cloneNsTab.addNamespaces( this.namespaceTable );
        clone.ownNamespaceTable( cloneNsTab );
      }
    }
    catch (Exception ex) {
      Logger.error("Could not clone Model", ex);
    }

    return clone;
  }

  /**
   * Updates this Model by replacing all (non key) attribute's values with
   * the attribute values of updateModel. UpdateModel must have the same
   * type as this Model.
   *
   * @param updateModel the Model containing the new attribute values
   * @return true if at least one attribute has been modified by this update
   */
  public boolean update( Model updateModel ) {
    if (updateModel.type() != this.type()) {
      throw new IllegalArgumentException( "Can't update this Model (type="+type()+") with Model having different type="+updateModel.type() );
    }
    boolean modified = false;
    ComplexType modelType = (ComplexType) type();
    for (int i = 0; i < modelType.attributeCount(); i++) {
      boolean fireEvent = false;
      Object oldValue = null;
      Object value = null;
      synchronized (attrSemaphore) {
        int keyIndex = modelType.getKeyIndex( modelType.getAttributeName(i) );
        if (keyIndex < 0) {
          value = ((SequenceModel) updateModel).attributes[i];
          oldValue = this.attributes[i];
          if (((oldValue != null) && (!oldValue.equals(value)))
              || ((oldValue == null) && (value != null))) {
            this.attributes[i] = value;
            modified = true;
            fireEvent = true;
          }
        }
      }
      if (fireEvent) {
        fireDataChanged( modelType.getAttributeName( i ), oldValue, value );
      }
    }

    return modified;
  }


  /**
   * Sorts all children in relationName.
   * @param relationName name of the relation to be sorted
   * @param comparator     the SortRule to use for sorting
   * @return true if sort was successful, false if not successful or not supported
   */
  public boolean sort( QName relationName, Comparator<? extends Model> comparator ) {
    if (children.sort( relationName, (Comparator<Model>) comparator )) {
      fireDataChanged( relationName, null, null );
    }
    return true;
  }

  /**
   * This method is called as last step of SequenceModel's constructor (and after initTransient())
   * and allows to add individual initialization. Default implementation is empty.
   * Note: this is called before child class's field initialization and constructor execution.
   */
  protected void onInitialized() {
    // Hook method
  }

  //------------------------------------------------------------------------------------
  // Interface Transient
  //------------------------------------------------------------------------------------

  protected void initTransient() {
  }

  public ComplexType transientType() {
    return null;
  }

  public Object transientGet( int transientIndex ) {
    return null;
  }

  public void transientSet( int transientIndex, Object value ) {
    throw new IllegalArgumentException( "Unknown transient value or read-only, transientIndex="+ transientIndex );
  }

  public ModelIterator transientChildIter( QName relationName, Filter filter ) {
    return ModelIteratorChildList.EMPTY_ITER;
  }

  @Override
  public String toString() {
    return getClass().getName() +" type="+type().name();
  }
}
