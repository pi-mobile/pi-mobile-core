/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.qnames.QName;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Container for variables, used in Context to support scopes.
 */
public class Scope {

  private Hashtable variables;

  /**
   * Returns variableName's value
   * @param variableName the variable name
   * @return variableName's value
   */
  public Variable get( QName variableName ) {
    if (variables == null) {
      return null;
    }
    else {
      return (Variable) variables.get( variableName );
    }
  }

  /**
   * Adds variableto this scope.
   * @param variable the variable to add
   * @throws IllegalArgumentException  if a variable with same name already exists
   */
  public void add( Variable variable ) {
    if (variables == null) {
      variables = new Hashtable();
    }
    Variable oldVar = (Variable) variables.get( variable.getName() );
    if ((oldVar != null) && !(oldVar == variable)) {
      throw new IllegalArgumentException( "Cannot add variable, already exists, name="+variable.getName() );
    }
    variables.put( variable.getName(), variable );
  }

  public void remove( Variable variable ) {
    if (variables != null) {
      variables.remove( variable.getName() );
    }
  }

  public Enumeration elements() {
    if (variables == null) {
      return EmptyEnumerator.getInstance();
    }
    else {
      return variables.elements();
    }
  }
}
