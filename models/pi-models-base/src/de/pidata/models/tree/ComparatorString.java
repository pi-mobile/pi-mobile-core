/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.log.Logger;
import de.pidata.qnames.QName;

public class ComparatorString implements Comparator<Model> {

  public static final boolean DEBUG = false;

  private QName attributeName;
  private QName secondAttrName = null;
  private XPath modelPath;

  /**
   * Creates a new number comparator
   * @param attributeName the attrbiute containing a number value to use for comparision
   */
  public ComparatorString( QName attributeName ) {
    this( (XPath) null, attributeName );
  }

  public ComparatorString( QName attributeName, QName secondAttrName ) {
    this( (XPath) null, attributeName );
    this.secondAttrName = secondAttrName;
  }

  public ComparatorString( XPath modelPath, QName attributeName ) {
    if (attributeName == null) {
      throw new IllegalArgumentException( "attributeName must not be null" );
    }
    this.attributeName = attributeName;
    this.modelPath = modelPath;
  }

  /**
   * Compares models m1 and m2 by their values of attribute given via constructor.
   * Null is treated as empty String
   *
   * @param m1 a model
   * @param m2 a model
   * @return -1 (m1 less m2), 0 (m1 equal m2) or 1 (m1 greater m2)
   * @throws IllegalArgumentException if models cannot be compared by this SortRole
   */
  public int compare( Model m1, Model m2 ) {
    String value1 = fetchValue( m1, attributeName );
    String value2 = fetchValue( m2, attributeName );
    int compare = value1.compareToIgnoreCase( value2 );
    if ((compare == 0) && (secondAttrName != null)) {
      value1 = fetchValue( m1, secondAttrName );
      value2 = fetchValue( m2, secondAttrName );
      compare = value1.compareToIgnoreCase( value2 );
    }
    if (compare == 0) return 0;
    else if (compare > 0) return 1;
    else return -1;
  }

  protected String fetchValue( Model model, QName attrName ) {
    if (modelPath != null) {
      model = modelPath.getModel( model, null );
      if (model == null) return "";
    }
    Object value = model.get( attrName );
    if (DEBUG) {
      Logger.debug("Fetched value = " + value);
    }
    if (value == null) return "";
    else return value.toString();
  }
}
