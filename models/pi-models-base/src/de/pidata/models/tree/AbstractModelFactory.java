/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.NamespaceTable;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.DefaultRelation;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;

public abstract class AbstractModelFactory implements ModelFactory {

  private static int idCounter = Integer.MIN_VALUE;

  protected Hashtable<QName,Type> supportedTypes = new Hashtable<QName, Type>();
  protected Hashtable<QName,Relation> rootRelations = new Hashtable<QName,Relation>();
  protected String    schemaLocation;
  protected String    version;
  protected Namespace targetNamespace;
  protected Hashtable<QName,SimpleType> attributes;

  public AbstractModelFactory(Namespace targetNamespace, String schemaLocation, String version) {
    this.schemaLocation = schemaLocation;
    this.targetNamespace = targetNamespace;
    this.version = version;
    ModelFactoryTable.getInstance().setFactory( targetNamespace, this );
  }

  /**
   * Returns this factory's schema loacation
   *
   * @return this factory's schema loacation or null
   */
  public String getSchemaLocation() {
    return this.schemaLocation;
  }

  /**
   * Returns this Factory's target namespace. There is a 1:1 assuziation between Factory and
   * targetNamespce. 
   * @return this Factory's target namespace
   */
  public Namespace getTargetNamespace() {
    return this.targetNamespace;
  }

  /**
   * Returns the version of this factory. This is the value of the schema's version attribute.
   *
   * @return the version of this factory
   */
  public String getVersion() {
    return this.version;
  }

  /**
   * Adds relation to this factory's root relations
   * @param rootRel the relation to be added
   */
  public void addRootRelation(Relation rootRel) {
    QName relName = rootRel.getRelationID();
    if (relName.getNamespace() != getTargetNamespace()) {
      throw new IllegalArgumentException( "Must not add root relation to a factory with different Namespace, "
                                         + "relationName="+relName+", factory namspace="+getTargetNamespace() );
    }
    Relation oldRel = getRootRelation( relName );
    if (oldRel != null) {
      throw new IllegalArgumentException("Root relation is already defined: name="+rootRel.getRelationID());
    }
    this.rootRelations.put( relName, rootRel );
  }

  /**
   * Creates a relation for the given relationName and adds it to this
   * factory's root relations
   * @param relationName the relation name
   * @param type         the relations's type
   * @param minOccurs    the min occurence
   * @param maxOccurs    the max occurence
   * @param substitutionGroup the relations's substitutionGroup or null
   * @return the created relation
   */
  public Relation addRootRelation( QName relationName, Type type, int minOccurs, int maxOccurs, QName substitutionGroup ) {
    DefaultRelation rel = new DefaultRelation( relationName, type, minOccurs, maxOccurs, Collection.class, substitutionGroup );
    addRootRelation(rel);
    return rel;
  }

  public Relation addRootRelation( QName relationName, Type type, int minOccurs, int maxOccurs ) {
    return addRootRelation( relationName, type, minOccurs, maxOccurs, null );
  }

  /**
   * Returns the root relation for the given relationName or null if not defined
   *
   * @param relationName name of the relation to be returned
   * @return the root relation for the given relationName or null
   */
  public Relation getRootRelation(QName relationName) {
    return (Relation) rootRelations.get( relationName );
  }

  /**
   * Adds the given Type to this Factory
   * @param type the type to be added
   * @throws IllegalArgumentException if type's namespace is different from factories
   *               targetNamespace or if type already exists for this factory
   */
  public void addType( Type type ) {
    QName typeName = type.name();
    if (typeName.getNamespace() != getTargetNamespace()) {
      throw new IllegalArgumentException( "Must not add type to a factory with different Namespace, "
                                         + "typeName="+typeName+", factory namspace="+getTargetNamespace() );
    }
    if (getType( typeName ) == null) {
      supportedTypes.put( typeName, type );
    }
    else {
      // TODO: Is an exception necessary?
      // throw new IllegalArgumentException( "Must not replace a type, typeName="+typeName );
    }
  }

  /**
   * Returns the type for the given attribute name
   *
   * @param attrName the attribute's name
   * @return the attribute's type or null if not found
   */
  public SimpleType getAttribute(QName attrName) {
    if (attributes == null) return null;
    else return (SimpleType) attributes.get(attrName);
  }

  /**
   * Returns the type definition for the given type name.
   * Hint: Use FactoryTable to get a type you do not which factory supports it.
   * @param typeName the name of the type to be returned
   * @return the type definition for the given type name or null if type is unknown
   */
  public Type getType(QName typeName) {
    return (Type) supportedTypes.get( typeName );
  }

  protected synchronized QName nextID() {
    return NAMESPACE.getQName( Integer.toString( idCounter++ ) );
  }

  /**
   * Creates a new model instance for the given simple type.
   *
   * @param type   the new model's type
   * @param value  the new model's value
   * @return a new model instance for the given simple type
   */
  public Model createInstance(SimpleType type, Object value) {
    //TODO check if type is contained in this factory's supportedTypes[]
    return new SimpleModel(type, value);
  }

  /**
   * Creates a new instance for the given type.
   *
   * @param key
   * @param type    the type definition describing the model to be created
   * @param attributes  the attributes for the new instace - must be same order
   *                    than defined in type; may be null
   * @param anyAttribs  Hastable containing attributes according to anyAttribute
   *                    definition of type, may be null
   * @param children    the initial list of children, may be null
   * @return a new model instance for the given type
   */
  public Model createInstance( Key key, Type type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children) {
    throw new IllegalArgumentException( "Factory namespace="+targetNamespace+": Do not know how to create model type="+type.name() );
  }

  /**
   * Creates a new instance for the given type. Teh result's ID is the type name.
   *
   * @param type  the type definition describing the model to be created
   * @return a new model instance for the given type
   */
  public Model createInstance(ComplexType type) {
    return createInstance(type.name(), type, null, null, null);
  }

  /**
   * Creates a new instance for the root relation definied by rootRelationName
   *
   * @param rootRelationName the name of a root relation defined by this factory
   * @return a new instance for rootRelationName
   */
  public Model createInstance(QName rootRelationName) {
    Relation relation = getRootRelation(rootRelationName);
    Type inputType = relation.getChildType();
    return createInstance(rootRelationName, inputType, null, null, null);
  }

  public QNameIterator typeNames() {
    return new QNameIteratorEnum( supportedTypes.keys() );
  }

  public QNameIterator relationNames() {
    return new QNameIteratorEnum( rootRelations.keys() );
  }

  /**
   * Creates a key for a model having modelType from key's string representation
   * @param modelType type of the model the key will be used for
   * @param keyString key's string representation
   * @return the key
   */
  public static Key createKey( ComplexType modelType, String keyString, NamespaceTable namespaces ) {
    int count = modelType.keyAttributeCount();
    if (count == 0) {
      throw new IllegalArgumentException( "Cannot create key: modelType's key attribute count is 0, type name="+modelType.name() );
    }
    else if (count == 1) {
      SimpleType keyType = modelType.getKeyAttributeType( 0 );
      if (keyType instanceof QNameType) {
        return QName.fromKeyString( keyString, namespaces );
      }
      else {
        return SimpleKey.fromKeyString( keyType, keyString, namespaces );
      }
    }
    else {
      return CombinedKey.fromKeyString( modelType, keyString, namespaces );
    }
  }

  /**
   * Returns the type for typeName. The ModelFactory for typeName is found via typeName's Namespace.
   * @param typeName the type name
   * @return the type for typeName
   * @throws IOException if typeName's factory is not loaded
   *                     or type is unknown by the factory for typeName's Namesapvce
   */
  public static Type findType( QName typeName ) throws IOException {
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( typeName.getNamespace() );
    if (factory == null) {
      throw new IOException( "ModelFactory missing for type name="+typeName );
    }
    Type type = factory.getType( typeName );
    if (type == null) {
      throw new IOException( "Type definition unknown by factory, type name="+typeName );
    }
    return type;
  }
}
