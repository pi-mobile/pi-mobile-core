/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.log.Logger;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.Vector;

public class Path {
  public static final char DELIMITER = '/';

  private Object[] path;

  public Path(QName[] path) {
    this.path = path;
  }

  public Path(QName model0) {
    this.path = new QName[1];
    path[0] = model0;
  }

  public Path(QName model0, QName model1) {
    this.path = new QName[2];
    path[0] = model0;
    path[1] = model1;
  }

  public Path(QName model0, QName model1, QName model2) {
    this.path = new QName[3];
    path[0] = model0;
    path[1] = model1;
    path[2] = model2;
  }

  public Path(Namespace namespace, String pathExpression) {
    int count = 0;
    this.path = splitPath(namespace, pathExpression, 0, count);
  }

  public Path(Model model) {
    int count = 0;
    Vector modelIDs = new Vector();
    Model currentModel = model;
    do {
      modelIDs.insertElementAt(currentModel.key(), 0);
      currentModel = currentModel.getParent( true );
      count++;
    }
    while (currentModel != null);
    path = new QName[modelIDs.size()];
    for (int i = 0; i < modelIDs.size(); i++) {
      path[i] = (QName) modelIDs.elementAt(i);
    }
  }

  private QName[] splitPath(Namespace namespace, String pathExpr, int pos, int count) {
    QName[] result;
    int nsprefix;
    int i = pathExpr.indexOf(DELIMITER, pos);

    if (i >= 0) {
      result = splitPath(namespace, pathExpr, i + 1, count + 1);
    }
    else {
      result = new QName[count + 1];
      i = pathExpr.length();
    }

    // Extract namespace if exists
    nsprefix = pathExpr.indexOf(':', pos);
    if ((nsprefix > 0) && (nsprefix < i)) {
      namespace = Namespace.getInstance(pathExpr.substring(pos, nsprefix));
      pos = nsprefix + 1;
    }
    result[count] = namespace.getQName(pathExpr.substring(pos, i));
    return result;
  }

  /**
   * Returns the number of elements in this path
   *
   * @return the number of elements in this path
   */
  public int size() {
    return path.length;
  }

  /**
   * Returns the path element (Model ID) at index
   *
   * @param index the index of the name to be returned
   * @return the Model name at index
   */
  public QName modelIDAt(int index) {
    return (QName) path[index];
  }


  /**
   * Returns the Model identified by the path starting from startModel. If that
   * path does not exist from startModel null is returned.
   *
   * @param startModel the beginning Model for this path
   * @return the Model identifier by this path or null
   */
  public Model getModel(Model startModel) {
    int i = 0;
    int length = size();
    Model childModel = startModel;
    Object nextElement;
    QName name;

    try {
      while ((i < length) && (childModel != null)) {
        nextElement = path[i];
        if (nextElement instanceof QName) {
          if (childModel.key() != nextElement) {
            name = (QName) nextElement;
            childModel = (Model) childModel.get(null, name);
          }
        }
        else //if (nextElement instanceof AttributeValue)
        {
          throw new RuntimeException("TODO");
        }
        i++;
      }
    }
    catch (Exception ex) {
      Logger.error("Could not find model", ex);
      childModel = null;
    }
    return childModel;
  }

  public String toString() {
    StringBuffer result = new StringBuffer();
    for (int i = 0; i < path.length; i++) {
      if (i > 0) {
        result.append("/");
      }
      result.append(path[i]);
    }
    return result.toString();
  }

  public String toStringNoNS() {
    StringBuffer result = new StringBuffer();
    String modelName;
    int index;
    for (int i = (path.length - 1); i >= 0; i--) {
      if (i < (path.length - 1)) {
        result.append("/");
      }
      //delete namespace
      modelName = ((QName) path[i]).getName();
      index = modelName.lastIndexOf('.');

      if (index != 0) {
        modelName = modelName.substring(index + 1);
      }
      result.append(modelName);
    }
    return result.toString();
  }

  /**
   * We need this to be if two paths are equal when they are equal in the
   * meaning of xml!
   * @param obj a path obj
   * @return true if the paths are equal
   */
  public boolean equals(Object obj) {
    // check the easiest way befor doing complex checks!!
    if (obj == this) return true;

    // now do some complex checks
    boolean rc = false;
    // is the value null and do we have the same type of class ?
    if ((obj != null) && (obj.getClass() == this.getClass())) {
      Path checkPath = (Path) obj;

      // is the size the same?
      if (checkPath.size() == this.size()) {

        // now we have to switch the logic.
        // the path is not equal if one of the path elements
        // is not equal to the corresponding path element
        rc = true;
        for (int i = 0; i < path.length; i++) {
          if (checkPath.modelIDAt(i) != this.modelIDAt(i)) {
            rc = false;
            break;
          }
        }
      }
    }

    return rc;
  }

  /**
   * builds a hashcaode based on the hascodes of the path elements
   * @return the hashcode
   */
  public int hashCode() {
    int hashcode = 0;
    for (int i = 0; i < this.size(); i++) {
      hashcode += this.modelIDAt(i).hashCode();
    }
    return hashcode;
  }

}
