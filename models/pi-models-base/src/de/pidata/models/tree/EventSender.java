/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.log.Logger;
import de.pidata.qnames.QName;

import java.util.LinkedList;

public class EventSender {

  private Object owner;
  private LinkedList<EventListener> listeners;

  public EventSender( Object owner ) {
    this.owner = owner;
  }

  public Object getOwner() {
    return owner;
  }

  public synchronized void addListener( EventListener listener)  {
    if (this.listeners == null) {
      this.listeners = new LinkedList<EventListener>();
    }
    this.listeners.add(listener);
  }

  public synchronized void removeListener(EventListener listener) {
    if (this.listeners != null) {
      this.listeners.remove(listener);
    }
  }

  public void fireEvent( int eventID, Object source, QName modelID, Object oldValue, Object newValue) {
    Object[] listenerArr = null;

    synchronized (this) {
      if (this.listeners != null) {
        // Copying listeners prevents problems if listeners are added or removed while send event loop
        listenerArr = new Object[listeners.size()];
        listeners.toArray( listenerArr );
      }
    }
    if (listenerArr != null) {
      for (int i = 0; i < listenerArr.length; i++) {
        EventListener listener = (EventListener) listenerArr[i];
        try {
          listener.eventOccured( this, eventID, source, modelID, oldValue, newValue );
        }
        catch (Exception ex) {
          Logger.error( "Error while event processing", ex );
        }
      }
    }
  }

  public synchronized int listenerCount() {
    if (listeners == null) {
      return 0;
    }
    else {
      return listeners.size();
    }
  }

  /**
   * Checks if model is the event source. This may be either the model itself
   * or the model wrapped by model (see ReadOnlyWrapper)
   * @param source the event source
   * @param model  the model to compare source with
   * @return true if model is the event's source
   */
  public static boolean isSource( Object source, Model model ) {
    if (source == model) {
      return true;
    }
    else if (model instanceof ReadOnlyWrapper) {
      Model wrappedModel = ((ReadOnlyWrapper) model).wrappedModel();
      return (source == wrappedModel);
    }
    return false;
  }
}
