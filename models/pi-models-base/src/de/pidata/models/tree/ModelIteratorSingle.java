package de.pidata.models.tree;

import java.util.Iterator;

public class ModelIteratorSingle<CM extends Model> implements ModelIterator<CM> {

  private CM model;
  boolean hasNext;

  public ModelIteratorSingle( CM model ) {
    this.model = model;
    hasNext = (model != null);
  }

  /**
   * Returns true if there is a further Model in this iteration, i.e. the next call to next()
   * will return a Model not null.
   *
   * @return true if there is a further Model in this iteration
   */
  @Override
  public boolean hasNext() {
    return hasNext;
  }

  /**
   * Returns the next Model of this iterator and sets the internal pointer to the next Model.
   *
   * @return the next Model of this iteration
   * @throws IllegalArgumentException if there is no next Model in this iterator
   */
  @Override
  public CM next() {
    if (hasNext) {
      hasNext = false;
      return model;
    }
    else {
      throw new IllegalArgumentException( "No more Elements in iterator." );
    }
  }

  /**
   * Returns an iterator over elements of type {@code T}.
   *
   * @return an Iterator.
   */
  @Override
  public Iterator<CM> iterator() {
    return this;
  }

  public String toString() {
    return "ModelIteratorSingle, model="+model;
  }
}
