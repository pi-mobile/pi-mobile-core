/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.qnames.QName;

public interface EventListener
{
  /**
   * If there is any data changed in the model, the eventID of the thrown event is
   * ID_MODEL_DATA_CHANGED. The changes will be stored in a binding in the
   * If there will be more than one changes
   * on the same value, only the oldValue from the first call and the latest newValue
   * are stored.
   */
  int ID_MODEL_DATA_CHANGED = 0;

  /**
   * If there is a new value added to the model, the eventID of the thrown event is
   * ID_MODEL_DATA_ADDED. In this case, the oldValue in the method call must be
   * null and the new one must be the value that should be added to the model.
   */
  int ID_MODEL_DATA_ADDED   = 1;

  /**
   * If there is a new value added to the model, the eventID of the thrown event is
   * ID_MODEL_DATA_REMOVED. In this case, the oldValue in the method call must be
   * the removed value and newValue must be the next after the removed one.
   */
  int ID_MODEL_DATA_REMOVED = 2;

  /**
   * An event occurred.
   * @param eventSender the sender of this event
   * @param eventID the id of the event
   * @param source the instance where the event occurred on, may be a child of eventSender
   * @param modelID the id of the affected model
   * @param oldValue the old value of the model
   * @param newValue the new value of the model
   */
  void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue );
}
