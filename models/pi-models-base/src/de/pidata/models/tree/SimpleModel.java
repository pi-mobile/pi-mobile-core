/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;

public class SimpleModel extends AbstractModel {

  public static final Namespace NAMESPACE = Namespace.getInstance("de.pidata.models");
  public static final QName ID_ID = NAMESPACE.getQName("ID");
  public static final QName ID_VALUE = NAMESPACE.getQName("value");

  public static DefaultComplexType STRINGENTRY;
  static {
    DefaultComplexType complexType = new DefaultComplexType(NAMESPACE.getQName("stringEntry"), SimpleModel.class.getName(), 1);
    complexType.addKeyAttributeType(0, SimpleModel.ID_ID, QNameType.getInstance());
    complexType.addAttributeType(SimpleModel.ID_VALUE, StringType.getDefString());
    STRINGENTRY = complexType;
  }

  private SimpleType valueType;

  public SimpleModel(Type type) {
    super(null, type);
    if (type instanceof SimpleType) {
      this.valueType = (SimpleType) type;
    }
    else {
      ComplexType complex = (ComplexType) type;
      this.valueType = complex.getAttributeType(complex.indexOfAttribute(ID_VALUE));
    }
  }

  public SimpleModel(Type type, Object value) {
    this(type);
    set(ID_VALUE, value);
  }

  /**
   * Returns the attribute value or child of this Model identified by valueID.
   * ValueID is searched among all allowed attributes, defined 1:1 childs and existing 1:n childs.
   *
   * @param  valueID the ID of the value to be returned
   * @return the attribute value or child of this Model identified by valueID.
   */
  public Object get(QName valueID) {
    if ((valueID == null) || (valueID == ID_VALUE)) {
      return content;
    }
    if (valueID == ID_ID) {
      return key;
    }
    throw new IllegalArgumentException("Unknown value ID="+valueID);
  }

  /**
   * Sets the value for the attribute identified by attributeID. It depends on the model's
   * implementation how to map elementIDs to attributes.
   *
   * @param attributeName the name of the attribute value to be set
   * @param value                 the new value for the attribute identified by attributeID
   * @throws ValidationException if this model is not writeable, attributeName is unknown
   *                             or value is not valid for attribute's type
   */
  public void set(QName attributeName, Object value) throws ValidationException {
    if ((attributeName == null) || (attributeName == ID_VALUE)) {
      if (isReadOnly( attributeName )) {
        throw new ValidationException( this.type, this.key, attributeName, ValidationException.ERROR_ATTR_READONLY );
      }
      int valid = this.valueType.checkValid(value);
      if (valid == ValidationException.NO_ERROR) {
        Object oldValue = content;
        this.content = value;
        if (((oldValue != null) && (!oldValue.equals(value)))
            || ((oldValue == null) && (value != null))) {
          fireDataChanged(ID_VALUE, oldValue, value);
        }
      }
      else {
        throw new ValidationException( this.type, this.key, attributeName, valid );
      }
    }
    else {
      throw new ValidationException( this.type, this.key, attributeName, ValidationException.ERROR_ATTR_UNKNOWN );
    }
  }

  /**
   * Returns current index of child within given relation. Be careful using that index. It will
   * change when adding/removing children to/from this model!
   *
   * @param relationID ID if the relation to search for child
   * @param child      the child to search for
   * @return child's index or -1 if not found
   */
  public int indexOfChild(QName relationID, Model child) {
    return -1;
  }

  /**
   * Adds childModel to this Model. If child relation is a composition this Model is set as child's parent.
   *
   * @param relationID the relation ID to add childModel to
   * @param childModel the new child model
   */
  public void add(QName relationID, Model childModel) {
    throw new IllegalArgumentException("A simple model never has children!");
  }

  public void insert( QName relationID, Model childModel, Model beforeChild ) {
    throw new IllegalArgumentException("A simple model never has children!");
  }

  /**
   * Returns the count of this Model's when using relation ID as filter. If filter is null the
   * count of all children is returned
   *
   * @param  relationID the relation ID to use as filter, may be null
   * @return the count of this Model's when using relation ID as filter
   */
  public int childCount(QName relationID) {
    return 0;
  }

  /**
   * Returns an iterator over this Model's children having relation ID as filter. If filter is null an
   * iterator over all children is returned.
   *
   * @param  relationName  name of the relation to use as filter, may be null
   * @param filter                 the filter to use for skipping not matching children, leave null to get all children of relation
   * @return an iterator over this Model's children filtered by relation and filter
   */
  public ModelIterator iterator( QName relationName, Filter filter ) {
    return ModelIteratorChildList.EMPTY_ITER;
  }

  /**
   * Adds/Replaces childModel to this model
   *
   * @param relationID the relation ID to add childModel to
   * @param value      the new child model or value
   */
  public void replace(QName relationID, Object value) {
    throw new IllegalArgumentException("A simple model never has children!");
  }

  /**
   * Removes childModel from this Model's relation relationID. If child relation is a composition child's parent is set to null.
   *
   * @param relationID the relation ID to remove childModel from
   * @param childModel  the child ndoe to remove
   * @return true if model was found (and removed), false otherwise
   */
  public boolean remove(QName relationID, Model childModel) {
    throw new IllegalArgumentException("A simple model never has children!");
  }

  /**
   * Removes all children of the given realtion.
   * If relationID is null all relation's children are removed.
   * @param relationID the realtion
   */
  public void removeAll(QName relationID) {
    throw new IllegalArgumentException("A simple model never has children!");
  }

  /**
   * Creates a deep copy of this model an assigns it the new key <code>id</code>.
   * If <code>id</code> is null this model's id is used.
   *
   * @param newKey  the new key for the clone or null if clone should have same key
   * @param deep    if true, makes a deep copy
   * @param linked  if true clone is recursively linked to its clone source
   * @return deep copy of this model
   * @throws RuntimeException This implementation throws always an Exception.
   */
  public Model clone( Key newKey, boolean deep, boolean linked ) {
    Model clone = new SimpleModel(this.valueType, getContent());
    if (linked) clone.clonedFrom( this );
    return clone;
  }

  /**
   * Updates this Model by replacing all attribute values with the
   * attribute values of updateModel. UpdateModel must have the same
   * type as this Model.
   *
   * @param updateModel the Model containing the new attribute values
   * @return true if at least one attribute has been modified by this update
   */
  public boolean update( Model updateModel ) {
    if (updateModel.type() != this.type()) {
      throw new IllegalArgumentException( "Can't update this Model (type="+type()+") with Model having different type="+updateModel.type() );
    }
    // do nothing, because we have no attributes
    return false;
  }
}
