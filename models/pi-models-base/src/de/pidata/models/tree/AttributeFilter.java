/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.qnames.QName;

public class AttributeFilter implements Filter {

  private QName[]   attributeNames;
  private Object[]  values;
  private boolean[] checkEqual;

  public AttributeFilter( int attributeCount ) {
    createArrays( attributeCount );
  }

  public AttributeFilter( QName attrName, boolean equal, Object value ) {
    createArrays( 1 );
    setRule( 0, attrName, equal, value );
  }

  @Override
  public void init( ModelSource dataSource ) {
    if (dataSource != null) {
      throw new IllegalArgumentException( "Datasource not supported" );
    }
  }

  @Override
  public void setFilterListener( FilterListener filterListener ) {
    //do nothing
  }

  private void createArrays( int attributeCount ) {
    attributeNames = new QName[attributeCount];
    values = new Object[attributeCount];
    checkEqual = new boolean[attributeCount];
  }

  public void setRule( int index, QName attrName, boolean equal, Object value ) {
    attributeNames[index] = attrName;
    checkEqual[index] = equal;
    values[index] = value;
  }

  /**
   * Returns true if model matches this filter
   *
   * @param model the model to be matched
   * @return true if model matches this filter
   */
  public boolean matches( Model model ) {
    if (model == null) {
      return false;
    }
    int count = attributeNames.length;
    for (int i = 0; i < count; i++) {
      if (attributeNames[i] != null) {
        Object modelValue = model.get( attributeNames[i] );
        if (isEqual( modelValue, values[i] ) != checkEqual[i]) {
          return false;
        }
      }
    }
    return true;
  }

  private boolean isEqual( Object modelValue, Object value ) {
    if (modelValue == null) {
      return (value == null);
    }
    else {
      return modelValue.equals( value );
    }
  }
}
