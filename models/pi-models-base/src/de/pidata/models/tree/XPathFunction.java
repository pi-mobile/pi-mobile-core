/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;

import java.util.Vector;

public class XPathFunction extends XPathValue {

  //------ Functions --------------------------------------------------------------------------

  /**
   * Function true().
   */
  public static QName FUNCTION_TRUE = NAMESPACE.getQName("true" );
  /**
   * Function false().
   */
  public static QName FUNCTION_FALSE = NAMESPACE.getQName("false" );

  /**
   * Function last().
   */
  public static QName FUNCTION_LAST = NAMESPACE.getQName("last" );
  /**
   * Function position().
   */
  public static QName FUNCTION_POSITION = NAMESPACE.getQName("position" );

  /**
   * Function count().
   */
  public static QName FUNCTION_COUNT = NAMESPACE.getQName("count" );

  private QName functionName;
  private Vector functionParams;

  public XPathFunction( QName functionName, NamespaceTable namespaceTable ) {
    super( null, namespaceTable );
    this.functionName = functionName;

    if (functionName == FUNCTION_TRUE ||
        functionName == FUNCTION_FALSE ||
        functionName == FUNCTION_COUNT ||
        functionName == FUNCTION_POSITION) {
    }
    else {
      throw new IllegalArgumentException("TODO XPath functions not yet implemented: " + functionName);
    }
  }

  public Object getValue( Model currentModel, int index, Context context, boolean readOnly ) {
    if (functionName == FUNCTION_TRUE) {
      return new Boolean( true );
    }
    else if (functionName == FUNCTION_FALSE) {
      return new Boolean( false );
    }
    else if (functionName == FUNCTION_COUNT) {
      XPath param = (XPath) functionParams.elementAt(0);
      Model result = param.getModelList( currentModel, context );
      return new Integer( result.childCount( SimpleReference.ID_SIMPLE_REFERENCE ) );
    }
    else if (functionName == FUNCTION_POSITION) {
      QName parentRel = currentModel.getParentRelationID();
      Model parent = currentModel.getParent( true );
      int modelPos = parent.indexOfChild(parentRel, currentModel );
      return new Integer(modelPos + 1);
    }
    else {
      return null;
    }
  }

  public QName getQNameValue( Model currentModel, int index, Context context ) {
    return QName.getInstance( getValue( currentModel, index, context, false ).toString(), namespaceTable );
  }

  protected void parseError(String msg, char[] pathExpr, int pos) {
    String part;
    int start = pos - 30;
    if (start < 0) part = new String(pathExpr, 0, pos);
    else part = "..." + new String(pathExpr, start, pos-start);
    throw new IllegalArgumentException(msg+", pos="+pos+" part= "+part);
  }

  /**
   * Parses expression conatained in pathExpr beginning with pos. Stops parsing if end of
   * expression ']' is found or pos == len.
   *
   * @param pathExpr the bufffer containing the expression
   * @param pos      the start position for parsing
   * @param len      the end position for parsion
   * @return the position after the last parsed character
   */
  public int parse( char[] pathExpr, int pos, int len) {
    char ch = pathExpr[pos];
    int startPos = pos;
    functionParams = new Vector();
    XPath param;
    String expr;

    while (pathExpr[pos] != ')') {
      pos++;
      ch = pathExpr[pos];
      if ((ch == ',') || (ch == ')')) {
        expr = new String(pathExpr, startPos, pos-startPos);
        param = new XPath( namespaceTable, expr );
        functionParams.addElement(param); //todo je nach function Anzahl Parameter beachten!
      }
    }
    if (ch != ')') {
      parseError("Error while parsing function parameters, expected ')' or ','", pathExpr, pos);
    }
    return pos+1;
  }

  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append(functionName.getName()).append('(');
    for (int i = 0; i < functionParams.size(); i++) {
      if (i > 0) result.append(',');
      result.append(functionParams.elementAt(i).toString());
    }
    result.append(')');
    return result.toString();
  }
}
