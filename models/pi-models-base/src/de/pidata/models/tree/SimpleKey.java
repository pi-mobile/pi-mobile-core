/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.qnames.Base32;
import de.pidata.qnames.Key;
import de.pidata.qnames.NamespaceTable;
import de.pidata.models.types.SimpleType;

public class SimpleKey implements Key {

  private Object value;
  private String keyString = null;

  public SimpleKey( Object value ) {
    this.value = value;
  }

  /**
   * Creates a Comined key from its strign representation, see toKeyString()
   * @param keyString the strign to creatrre key from
   */
  public static SimpleKey fromKeyString( SimpleType type, String keyString, NamespaceTable namespaces ) {
    byte[] bytes = Base32.decode( keyString );
    Object value = type.createValue( new String(bytes), namespaces );
    SimpleKey key = new SimpleKey( value );
    key.keyString = keyString;
    return key; 
  }

  /**
   * Writes this key to a key string: Each key value is converted
   * to Base32. Then the Base32 strings are connected by a '_'
   * in the key values order.
   *
   * @return a string representation of this key  @param namespaces
   */
  public String toKeyString( NamespaceTable namespaces ) {
    if (keyString == null) {
      // note: value never is a QName (see QName)
      byte[] bytes = value.toString().getBytes();
      keyString = Base32.encode( bytes );
    }
    return keyString;
  }

  /**
   * Returns the value at keyValueIndex from this key
   *
   * @param keyValueIndex
   * @return the value at keyValueIndex from this key
   */
  public Object getKeyValue(int keyValueIndex) {
    return value;
  }

  /**
   * Returns the number of values (columns) this key consists of.
   *
   * @return the number of values (columns) this key consists of.
   */
  public int keyValueCount() {
    return 1;
  }

  public int hashCode() {
    return this.value.hashCode();
  }

  public boolean equals(Object obj) {
    if ((obj != null) && (obj instanceof SimpleKey)) {
      Object otherValue = ((SimpleKey) obj).value;
      if (value == null) return (otherValue == null);
      else return value.equals(otherValue);
    }
    else {
      return false;
    }
  }

  public String toString() {
    return "SimpleKey["+value+"]";
  }
}
