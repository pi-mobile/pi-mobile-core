/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.qnames.QName;

import java.util.Iterator;


public interface QNameIterator extends Iterator<QName>, Iterable<QName> {
  /**
   * Returns true if there is a further ID in this iteration, i.e. the next call to next()
   * will return a ID not null.
   *
   * @return true if there is a further ID in this iteration
   */
  public boolean hasNext();

  /**
   * Returns the next ID of this iterator and sets the internal pointer to the next ID.
   *
   * @return the next ID of this iteration
   * @throws java.lang.IllegalArgumentException if there is no next ID in this iterator
   */
  public QName next();
}
