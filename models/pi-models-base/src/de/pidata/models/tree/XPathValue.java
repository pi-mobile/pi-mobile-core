/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.qnames.Namespace;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;

public class XPathValue {

  public static Namespace NAMESPACE = Namespace.getInstance("de.pidata.models");

  protected Object value;
  protected NamespaceTable namespaceTable;

  /**
   * Used by validator implementation.
   *
   * @return
   */
  public NamespaceTable getNamespaceTable() {
    return namespaceTable;
  }

  public XPathValue( Object value, NamespaceTable namespaceTable ) {
    this.value = value;
    this.namespaceTable = namespaceTable;
  }

  public Object getValue( Model currentModel, int index, Context context, boolean readOnly ) {
    return this.value;
  }

  public QName getQNameValue( Model currentModel, int index, Context context ) {
    if (value instanceof String) {
      return QName.getInstance( (String) value, namespaceTable );
    }
    else if (value instanceof QName) {
      return (QName) value;
    }
    return null;
  }

  public String toString() {
    if (value instanceof String) {
      return "'"+value.toString()+"'";
    }
    else {
      return value.toString();
    }
  }
}
