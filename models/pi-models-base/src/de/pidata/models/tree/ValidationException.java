/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;
import de.pidata.qnames.Key;

public class ValidationException extends RuntimeException {

  private Type modelType;
  private Key modelKey;
  private QName attributeName;
  private int errorCode;
  public static final int NO_ERROR = 0;
  public static final int NEEDS_CONVERSION = -1;

  public static final int ERROR_WRONG_CLASS = 1;
  public static final int ERROR_WRONG_TYPE  = 2;
  public static final int ERROR_NOT_NULLABLE = 3;
  public static final int ERROR_ATTR_UNKNOWN = 4;
  public static final int ERROR_ANY_ATTR_UNKNOWN = 5;
  public static final int ERROR_ATTR_READONLY = 6;
  public static final int ERROR_KEY_ATTR_READONLY = 7;
  public static final int ERROR_TOO_BIG = 10;
  public static final int ERROR_TOO_SMALL  = 11;
  public static final int ERROR_NAN_NOT_ALLOW = 12;
  public static final int ERROR_TOO_SHORT = 13;
  public static final int ERROR_TOO_LONG  = 14;
  public static final int ERROR_FRACTION_TOO_LONG = 15;
  public static final int ERROR_ILLEGAL_VALUE = 16;
  public static final int MIXED_CONTENT_NOT_ALLOWED = 17;
  public static final int CONTENT_NOT_ALLOWED = 18;

  public ValidationException( Type modelType, Key modelKey, QName attributeName, int errorCode ) {
    super();
    this.modelType = modelType;
    this.modelKey = modelKey;
    this.attributeName = attributeName;
    this.errorCode = errorCode;
  }

  public Type getModelType() {
    return modelType;
  }

  public Key getModelKey() {
    return modelKey;
  }

  public QName getAttributeName() {
    return attributeName;
  }

  public int getErrorCode() {
    return errorCode;
  }

  /**
   * Returns the error message
   *
   * @return the error message
   */
  public String getMessage() {
    Type type = null;
    if (attributeName == null) {
      type = modelType;
    }
    else if (modelType instanceof ComplexType) {
      ComplexType cType = (ComplexType) modelType;
      int index = cType.indexOfAttribute( attributeName );
      if (index >= 0) type = cType.getAttributeType( index );
    }
    String msg = "Validation error, typeName=" + modelType.name() + ", key=" + modelKey + ", attrName=" + attributeName
               +", error=" + errorCode;
    if (type != null) msg = msg + ", msg=" + type.getErrorMessage( this.errorCode );
    return msg;
  }
}
