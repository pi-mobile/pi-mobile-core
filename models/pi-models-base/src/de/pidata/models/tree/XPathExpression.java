/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;

public class XPathExpression extends XPathValue {

  public static QName OR        = NAMESPACE.getQName("or" );
  public static QName AND       = NAMESPACE.getQName("and" );
  public static QName EQUAL     = NAMESPACE.getQName("=" );
  public static QName NOT_EQUAL = NAMESPACE.getQName("!=" );
  public static QName GREATER   = NAMESPACE.getQName(">" );
  public static QName LESS      = NAMESPACE.getQName("<" );
  public static QName GREATER_OR_EQUAL = NAMESPACE.getQName(">=" );
  public static QName LESS_OR_EQUAL    = NAMESPACE.getQName("<=" );
  public static QName PLUS      = NAMESPACE.getQName("+" );
  public static QName MINUS     = NAMESPACE.getQName("-" );
  public static QName MULTIPLY  = NAMESPACE.getQName("*" );

  protected XPathValue left;
  protected QName operator;
  protected XPathValue right;

  /**
   * Used by validator implementation.
   *
   * @return
   */
  public XPathValue getLeft() {
    return left;
  }

  /**
   * Used by validator implementation.
   *
   * @return
   */
  public XPathValue getRight() {
    return right;
  }

  public XPathExpression( NamespaceTable namespaceTable ) {
    super( null, namespaceTable );
  }

  public Object getValue( Model currentModel, int index, Context rootModel, boolean readOnly ) {
    return new Boolean(evaluate( currentModel, index, rootModel, readOnly ));
  }

  public QName getQNameValue( Model currentModel, int index, Context context ) {
    return null; // alwas is Boolean, never QName
  }

  private Object convert4Compare( Object value ) {
    if (value == null) {
      return null;
    }
    else if (value instanceof Integer) {
      return new Long( (long) ((Integer) value).intValue() );
    }
    else if (value instanceof Short) {
      return new Long( (long) ((Short) value).shortValue() );
    }
    else if (value instanceof Byte) {
      return new Long( (long) ((Byte) value).byteValue() );
    }
    else if (value instanceof Enum) {
      return value.toString();
    }
    else {
      return value;
    }
  }

  public boolean evaluate( Model model, int index, Context context, boolean readOnly ) {
    Object leftValue, rightValue;
    if (left != null) {
      leftValue = left.getValue(model, index, context, readOnly );
    }
    else {
      leftValue = null;
    }
    if (right != null) {
      rightValue = right.getValue(model, index, context, readOnly );
    }
    else {
      rightValue = null;
    }
    if ((leftValue != null) && (leftValue instanceof QName)) {
      rightValue = right.getQNameValue( model, index, context );
    }
    else if ((rightValue != null) && (rightValue instanceof QName)) {
      leftValue = left.getQNameValue( model, index, context );
    }

    //TODO Umgang mit Zahlen optimieren
    leftValue = convert4Compare( leftValue );
    rightValue = convert4Compare( rightValue );

    if (operator == null) {
      if (leftValue instanceof Number) {
        return (((Number) leftValue).longValue() == index);
      }
    }
    else if (operator == OR) {
      //TODO
      throw new RuntimeException("XPath operator OR not supported");
    }
    else if (operator == AND) {
      //TODO
      throw new RuntimeException("XPath operator AND not supported");
    }
    else if (operator == EQUAL) {
      return ( ((leftValue == null) && (rightValue == null))
               ||((leftValue != null) && leftValue.equals(rightValue)) );
    }
    else if (operator == NOT_EQUAL) {
      return ( ((leftValue == null) && (rightValue != null))
               ||((leftValue != null) && (!leftValue.equals(rightValue))) );
    }
    else if (operator == GREATER) {
      return ( ((Long)leftValue).longValue() > ((Long)rightValue).longValue() );
    }
    else if (operator == LESS) {
      return ( ((Long)leftValue).longValue() < ((Long)rightValue).longValue() );
    }
    else if (operator == GREATER_OR_EQUAL) {
      return ( ((Long)leftValue).longValue() >= ((Long)rightValue).longValue() );
    }
    else if (operator == LESS_OR_EQUAL) {
      return ( ((Long)leftValue).longValue() <= ((Long)rightValue).longValue() );
    }
    else if (operator == PLUS) {
      //TODO
      throw new RuntimeException("XPath operator + not supported");
    }
    else if (operator == MINUS) {
      //TODO
      throw new RuntimeException("XPath operator - not supported");
    }
    else if (operator == MULTIPLY) {
      //TODO
      throw new RuntimeException("XPath operator * not supported");
    }
    return false;
  }

  protected void parseError(String msg, char[] pathExpr, int pos) {
    String part;
    int start = pos - 30;
    if (start < 0) part = new String(pathExpr, 0, pos);
    else part = "..." + new String(pathExpr, start, pos-start);
    throw new IllegalArgumentException(msg+", pos="+pos+" part= "+part);
  }

  private int parseExpression( XPath xPath, char[] pathExpr, int pos, int len, boolean leftValue) {
    int start;
    XPath path;
    XPathLocationStep location;
    //XPathExpression expr;
    XPathFunction func;
    XPathValue value = null;

    char ch = pathExpr[pos];
    if ((ch >= '0') && (ch <= '9')) {
      start = pos;
      pos++;
      while ((pos < len) && (pathExpr[pos] >= '0') && (pathExpr[pos] <= '9')) {
        pos++;
      }
      value = new XPathValue( new Integer( new String( pathExpr, start, pos-start ) ), namespaceTable );
    }
    else if (ch == '(') {
      location = new XPathLocationStep( namespaceTable );
      start = pos+1;
      pos = findClosing( pathExpr, pos+1, len, '(', ')' );
      location.parse( xPath, pathExpr, start, pos );
      value = location;
    }
    else if (ch == '"') {
      pos++;
      start = pos;
      int nsPos = -1;
      while ((pos < len) && pathExpr[pos] != '"') {
        if (pathExpr[pos] == ':') {
          nsPos = pos;
        }
        pos++;
      }
      if (nsPos > 0) {
        QName qname =  QName.getInstance( pathExpr, start, pos, namespaceTable );
        value = new XPathValue( qname, namespaceTable );
      }
      else {
        value = new XPathValue( new String( pathExpr, start, pos - start ), namespaceTable );
      }
      pos++;
    }
    else if (ch == '\'') {
      pos++;
      start = pos;
      int nsPos = -1;
      while ((pos < len) && pathExpr[pos] != '\'') {
        if (pathExpr[pos] == ':') {
          nsPos = pos;
        }
        pos++;
      }
      if (nsPos > 0) {
        QName qname =  QName.getInstance( pathExpr, start, pos, namespaceTable );
        value = new XPathValue( qname, namespaceTable );
      }
      else {
        value = new XPathValue( new String( pathExpr, start, pos - start ), namespaceTable );
      }
      pos++;
    }
    else if ((ch == '.') || (ch == '@') || (ch == '*') || (ch == '$')) {
      location = new XPathLocationStep( namespaceTable );
      pos = location.parse( xPath, pathExpr, pos, len);
      value = location;
    }
    else if (ch == '/') {
      location = new XPathLocationStep( namespaceTable );
      pos = location.parse( xPath, pathExpr, ++pos, len );
      value = location;
    }

    else {
      start = pos;
      while ((pos < len) && (value == null)) {
        ch = pathExpr[pos];
        if (ch == '(') {
          func = new XPathFunction( NAMESPACE.getQName(pathExpr, start, pos), namespaceTable );
          pos++;
          pos = func.parse( pathExpr, pos, len );
          value = func;
        }
        else if ((ch == ' ') || (ch == ']')) {
          location = new XPathLocationStep( namespaceTable );
          pos = location.parse( xPath, pathExpr, start, pos );
          value = location;
        }
        else {
          pos++;
        }
      }
    }

    if (leftValue) left = value;
    else right = value;
    return pos;
  }

  private int findClosing( char[] pathExpr, int pos, int len, char opening, char closing ) {
    while (pos < len) {
      if (pathExpr[pos] == closing) {
        return pos;
      }
      else if (pathExpr[pos] == opening) {
        pos = findClosing( pathExpr, pos+1, len, opening,  closing );
      }
      pos++;
    }
    throw new IllegalArgumentException( "Closing '"+closing+"' missing in XPath expression" );
  }

  /**
   * Parses expression conatained in pathExpr beginning with pos. Stops parsing if end of
   * expression ']' is found or pos == len.
   * @param pathExpr the bufffer containing the expression
   * @param pos the start position for parsing
   * @param len the end position for parsion
   * @return the position after the last parsed character
   */
  public int parse( XPath xPath, char[] pathExpr, int pos, int len) {
    char ch;
    int start = pos;

    while (pathExpr[pos] == ' ') {
      pos++;
    }

    pos = parseExpression( xPath, pathExpr, pos, len, true );
    if (pathExpr[pos] == ']') return pos;
    while ((pos < len) && (pathExpr[pos] == ' ')) {
      pos++;
    }

    start = pos;
    if (pos < len) {
      ch = pathExpr[pos];
      if (ch == '=') {
        pos++;
        operator = NAMESPACE.getQName(pathExpr, start, pos);
      }
      else if ((ch == '!') || (ch == '<') || (ch == '>')) {
        ch = pathExpr[pos+1];
        if (ch == '=') pos += 2;
        else pos++;
        operator = NAMESPACE.getQName(pathExpr, start, pos);
      }
      else {
        while ((pos < len) && (operator == null)) {
          ch = pathExpr[pos];
          if ((ch == ' ') || (ch == ']')) {
            operator = NAMESPACE.getQName(pathExpr, start, pos);
            if (ch == ']') return pos;
          }
          pos++;
        }
      }
    }
    while ((pos < len) && (pathExpr[pos] == ' ')) {
      pos++;
    }

    pos = parseExpression( xPath, pathExpr, pos, len, false);

    return pos;
  }

  public String toString() {
    StringBuffer result = new StringBuffer();
    if (left != null) result.append(left.toString());
    if (operator != null) result.append(" ").append(operator.getName()).append(" ");
    if (right != null) result.append(right.toString());
    return result.toString();
  }
}
