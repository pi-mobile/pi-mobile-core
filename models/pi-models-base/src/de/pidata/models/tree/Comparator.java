/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

/**
 * Defines a rule for sorting a model's children
 */
public interface Comparator<CM extends Model> {

  /**
   * Compares models m1 and m2
   * @param m1 a model
   * @param m2 a model
   * @return -1 (m1 less m2), 0 (m1 equal m2) or 1 (m1 greater m2)
   * @throws IllegalArgumentException if models cannot be compared by this SortRole  
   */
  public int compare( CM m1, CM m2 );
}
