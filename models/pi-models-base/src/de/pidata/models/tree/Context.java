/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.log.Logger;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;

import java.util.ArrayList;
import java.util.List;

public class Context extends SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance("de.pidata.models.service");
  public static final QName ID_CONTEXT = NAMESPACE.getQName("Context");

  private List<Scope> scopeStack = new ArrayList<Scope>();
  private Root dataRoot;  //todo: normales Model oder Ersetzen durch Scope!
  private String callerID;
  private String programName;
  private String programVersion;
  private QName userID;

  /**
   * Creates a context for a remote calling client
   * @param callerID        the caller's device ID
   * @param userID          ID of the user currently logged in on calling device
   * @param programName     name of the program which issued the remote call
   * @param programVersion  name of the program which issued the remote call
   */
  public Context( String callerID, QName userID, String programName, String programVersion ) {
    super(ID_CONTEXT, new DefaultComplexType(ID_CONTEXT, Context.class.getName(), 0),
        null, null, null);
    dataRoot = new Root(NAMESPACE.getQName("PID-"+System.currentTimeMillis()));
    pushScope( new Scope() );
    this.callerID = callerID;
    this.userID = userID;
    this.programName = programName;
    this.programVersion = programVersion;
  }

  public Root getDataRoot () {
    return dataRoot;
  }

  public String getClientID() {
    return this.callerID;
  }

  public void setClientID( String clientID ) {
    this.callerID = clientID;
  }

  public String getCallerName() {
    return getClientID();
  }

  public String getProgramName() {
    return this.programName;
  }

  public String getProgramVersion() {
    return this.programVersion;
  }

  /**
   * Returns the ID of the user currently logged in on calling device, may be null
   * @return ID of the user currently logged in on calling device
   */
  public String getUserID() {
    if (this.userID == null) {
      return null;
    }
    else {
      return this.userID.toString();
    }
  }

  /**
   * Returns variableName's value
   * @param variableName the variable name
   * @return variableName's value
   */
  public Object get( QName variableName ) {
    Variable variable = findVariable( variableName );
    if (variable != null) {
      return variable.getValue();
    }
    return null;
  }

  private Variable findVariable( QName variableName ) {
    for (int i = scopeStack.size()-1; i >= 0; i--) {
      Scope scope = scopeStack.get( i );
      Variable variable = scope.get( variableName );
      if (variable != null) {
        return variable;
      }
    }
    return null;
  }

  /**
   * Sets variableName's value. If variableName does not exist it is
   * created, otherwise replaced.
   * @param variableName the variable name
   * @param value        variableName's value
   */
  public void set( QName variableName, Object value ) {
    Variable variable = findVariable( variableName );
    if (variable == null) {
      int lastScope = scopeStack.size()-1;
      if (lastScope >= 0) {
        Scope scope = scopeStack.get( lastScope );
        variable = new SimpleVariable( variableName );
        scope.add( variable );
      }
      else {
        throw new IllegalArgumentException( "Cannot set variable: scope stack is empty!" );
      }  
    }
    Object oldValue = variable.getValue();
    if (value == null) {
      if (oldValue != null) {
        variable.setValue( value );
        fireDataChanged( variableName, oldValue, value );
      }
    }
    else {
      if (!value.equals( oldValue )) {
        variable.setValue( value );
        fireDataChanged( variableName, oldValue, value );
      }
    }
  }

  /**
   * Adds scope to the end of scope stack. From now that scope will be the first to
   * be searched for variables in methods get() and set()
   * @param scope the scope to push
   */
  public void pushScope( Scope scope ) {
    this.scopeStack.add( scope );
  }

  /**
   * Removes the last scope from scope stack
   * @return the removed scope
   */
  public Scope popScope() {
    int lastScope = scopeStack.size()-1;
    Scope scope = this.scopeStack.get( lastScope );
    this.scopeStack.remove( lastScope );
    return scope;
  }

  /**
   * Returns the model referenced by the given modelPath string. The modelPath
   * may be absolute or relative to the current model.
   *
   * @param modelPath the reference string to be resolved, may also be null, empty, "." or ".."
   * @param currentModel the current model, may be null
   * @return the referenced model
   */
  public Model findModel( String modelPath, Model currentModel, NamespaceTable namespaceTable ) {
    //----- if reference starts with '*' find the type described by reference and
    //      create a new instance of that type
    if (modelPath.charAt(0) == '*') { // todo raus hier, in gui rein
      currentModel = dataRoot;
      ModelFactory factory;
      QName typeID;
      int pos = modelPath.indexOf(':');
      if (pos > 0) {
        Namespace ns = namespaceTable.getByPrefix( modelPath.substring( 1, pos ) );
        factory = ModelFactoryTable.getInstance().getFactory( ns );
        typeID = ns.getQName( modelPath.substring(pos+1));
      }
      else {
        typeID =  QName.getInstance( modelPath.substring(1), namespaceTable );
        factory = ModelFactoryTable.getInstance().getFactory( typeID.getNamespace() );
      }
      try {
        Type type = factory.getType(typeID);
        if (type == null) {
          throw new IllegalArgumentException("Factory does not know type name="+typeID);
        }
        Key localID = null;
        if ((type instanceof ComplexType) && ((ComplexType) type).keyAttributeCount() > 0) {
          throw new RuntimeException( "TODO" );
          //localID = SystemManager.getInstance().createKey(null);
        }
        return factory.createInstance(localID, type, null, null, null);
      }
      catch (Exception e) {
        Logger.error("Could not create model for type ID="+typeID, e);
        return null;
      }
    }
    else {
      XPath path = new XPath( namespaceTable, modelPath );
      return path.getModel( currentModel, this );
    }
  }

  public QName getProcessID () {
    return (QName) dataRoot.key();
  }

  public String getStoreName() {
    return getProcessID().getName(); // todo? + ".xml";
  }
}
