/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Base32;
import de.pidata.qnames.Key;
import de.pidata.qnames.NamespaceTable;
import de.pidata.models.types.SimpleType;
import de.pidata.qnames.QName;

public class CombinedKey implements Key {

  private Object[] values;
  private int hashCode = 0;
  private String keyString = null;

  public CombinedKey( Object[] values ) {
    this.values = values;
    for (int i= 0; i < values.length; i++) {
      if (values[i] != null) {
        hashCode += values[i].hashCode();
      }  
    }
  }

  /**
   * Creates a Comined key from its strign representation, see toKeyString()
   * @param keyString the strign to creatrre key from
   */
  public static CombinedKey fromKeyString( ComplexType type, String keyString, NamespaceTable namespaces ) {
    int start = 0;
    int pos;
    int i = 0;
    Object[] values = new Object[type.keyAttributeCount()];
    do {
      pos = keyString.indexOf( '_', start );
      byte[] bytes;
      if (pos >= 0) {
        bytes = Base32.decode( keyString.substring( start, pos ) );
      }
      else {
        bytes = Base32.decode( keyString.substring( start ) );
      }
      start = pos + 1;
      SimpleType keyValueType = type.getKeyAttributeType( i );
      values[i] = keyValueType.createValue( new String(bytes), namespaces );
    } while (start > 0);
    CombinedKey key = new CombinedKey( values );
    key.keyString = keyString;
    return key;
  }

  /**
   * Writes this key to a key string: Each key value is converted
   * to Base32. Then the Base32 strings are connected by a '_'
   * in the key values order.
   *
   * @return a string representation of this key  @param namespaces
   */
  public String toKeyString( NamespaceTable namespaces ) {
    if (keyString == null) {
      StringBuffer buf = new StringBuffer();
      for ( int i = 0; i < values.length; i++ ) {
        byte[] bytes;
        if (values[i] instanceof QName) {
          bytes = ((QName) values[i]).toString( namespaces ).getBytes();
        }
        else {
          bytes = values[i].toString().getBytes();
        }
        if (i > 0) buf.append( '_' );
        buf.append( Base32.encode( bytes ) );
      }
      keyString = buf.toString();
    }
    return keyString;
  }

  /**
   * Returns the value at keyValueIndex from this key
   *
   * @param keyValueIndex
   * @return the value at keyValueIndex from this key
   */
  public Object getKeyValue(int keyValueIndex) {
    return this.values[keyValueIndex];
  }

  /**
   * Returns the number of values (columns) this key consists of.
   *
   * @return the number of values (columns) this key consists of.
   */
  public int keyValueCount() {
    return values.length;
  }

  public int hashCode() {
    return this.hashCode;
  }

  public boolean equals(Object obj) {
    if (obj instanceof CombinedKey) {
      CombinedKey otherKey = (CombinedKey) obj;
      if (otherKey.values.length != values.length) {
        return false;
      }
      for (int i = 0; i < values.length; i++) {
        Object myValue = values[i];
        Object otherValue = otherKey.values[i];
        if (otherValue == null) {
          if (myValue != null) return false;
        }
        else {
          if (!otherValue.equals(myValue)) return false;
        }
      }
      return true;
    }
    else {
      return false;
    }
  }

  public String toString() {
    StringBuffer buf = new StringBuffer( "CombinedKey[" );
    for (int i = 0; i < values.length; i++) {
      if (i > 0) buf.append( "," );
      buf.append( values[i] );
    }
    buf.append( "]" );
    return buf.toString();
  }
}
