/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.qnames.QName;

/**
 * A ModelReference is a model containing a reference to a second model. Such a reference
 * may be definied by XML Schema "keyRef". PI-Mobile supports references within XPath expressions
 * by use of '^' instead of '/'.
 */
public interface ModelReference {

  /**
   * Returns the name of this reference
   * @return the name of this reference
   */
  public QName getName();

  /**
   * Returns the name of the type referenced
   * @return the name of the type referenced
   */
  public QName getRefTypeName();

  /**
   * Returns the name of the referencing attribute at index.
   * Order and type of the attributes is the same as defined by the
   * key of the referenced type
   *
   * @param index index of the referencing attribute
   * @return name of the referencing attribute at index
   */
  public QName getRefAttribute( int index );

  /**
   * Returns the model referenced by this model
   * @return the model referenced by this model
   */
  public Model refModel();
}
