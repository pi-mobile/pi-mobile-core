/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.models.types.ComplexType;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.Hashtable;

/**
 * A DefautlModel is a SequenceModel without a factory. Subclasses must overwrite
 * the clone() method!
 */
public class DefaultModel extends SequenceModel {

  public static ComplexType TYPE;

  public static Class VALUE_CLASS;
  static {
    try {
      VALUE_CLASS = Class.forName("de.pidata.models.tree.DefaultModel");
      DefaultComplexType type = new DefaultComplexType( BaseFactory.NAMESPACE.getQName( "DefaultModel" ), DefaultModel.class.getName(), 0 );
      TYPE = type;
    }
    catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
 }

  public DefaultModel( Key key, ComplexType type) {
    this(key, type, null, null, null);
  }

  public DefaultModel( Key key, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super(key, type, attributes, anyAttribs, children);
  }

  protected Model createClone(Key newKey, Object[] newAttributes, Hashtable newAnyAttribs, ChildList cList) {
    return new DefaultModel(newKey, (ComplexType) type, newAttributes, newAnyAttribs, cList );
  }

  // Manuelle Erweiterung
  public QName getTypeName() {

    if(this.type != null) {
      return this.type.name();
    }
    return null;
  }
}
