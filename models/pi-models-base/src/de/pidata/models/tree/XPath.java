/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.models.types.ComplexType;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.StringType;

public class XPath {
  public static final char DELIMITER = '/';

  private XPathLocationStep firstLocation;
  private boolean startWithRoot = false;
  private boolean startWithContex = false;
  private String value = null;
  private String pathExpression;
  private NamespaceTable namespaceTable;

  /**
   * Used by validator implementation.
   *
   * @return
   */
  public XPathLocationStep getFirstLocation() {
    return firstLocation;
  }

  /**
   * Creates the XPath from the given String. Avoids NullPointerException.
   *
   * @param namespaceTable
   * @param pathExpression
   * @return the XPath if the given String expression is not empty (null or length == 0); null otherwise
   */
  public static XPath fromString( NamespaceTable namespaceTable, String pathExpression ) {
    if (pathExpression != null && pathExpression.length() > 0) {
      return new XPath( namespaceTable, pathExpression );
    }
    return null;
  }
  /**
   * Setup and parse the XPath expression.
   * @param namespaceTable
   * @param pathExpression
   */
  public XPath( NamespaceTable namespaceTable, String pathExpression ) {
    if (namespaceTable.size() == 0) {
      throw new IllegalArgumentException( "NamespaceTable must not be empty" );
    }
    this.namespaceTable = namespaceTable;
    pathExpression = pathExpression.trim();
    this.pathExpression = pathExpression;
    
    if (pathExpression.equals( "$" )) {
      startWithContex = true;
    }
    else {
      int start = 0;
      int exprLen = pathExpression.length();
      char[] pathExpr = new char[exprLen];

      pathExpression.getChars(0, exprLen, pathExpr, 0);
      if (pathExpr[start] == '/') { //start with root
        startWithRoot = true;
        start++;
      }
      if (start >= exprLen) { // just root
        return;
      }
      if (pathExpr[start] == '\'') {
        int end = 1;
        while ((end < exprLen) && (pathExpr[end] != '\'')) {
          end++;
        }
        value = new String(pathExpr, start+1, (end-start)-1);
      }
      else {
        firstLocation = new XPathLocationStep( namespaceTable );
        start = firstLocation.parse( this, pathExpr, start, exprLen );
      }
    }  
  }

  public NamespaceTable getNamespaceTable() {
    return namespaceTable;
  }

  public String getPathExpression() {
    return pathExpression;
  }

  /**
   * Returns all Models identified by the path starting from startModel. If that
   * path does not exist from startModel a empty Model is returned. The result
   * does not contain duplicates.
   *
   * @param startModel the beginning Model for this path
   * @param context    the evaluation context, containing root model and variables
   * @return a Model of references to the Models found
   */
  public Model getModelList( Model startModel, Context context ) {
    Model result = SimpleReference.createContainer();
    Model startFrom = startModel;
    if (startWithRoot) {
      // Loop up to root
      startFrom = context.getDataRoot();
    }
    else if (startWithContex) {
      startFrom = context;
    }
    if (firstLocation == null) {
      SimpleReference.addReference( result, startFrom );
    }
    else {
      this.firstLocation.evaluate( startFrom, context, result, false );
    }
    return result;
  }

  /**
   * Returns the Model identified by the path starting from startModel. If that
   * path does not exist from startModel null is returned.
   *
   * @param startModel the beginning Model for this path
   * @param context    the evaluation context, containing root model and variables
   * @return the Model identifier by this path or null
   */
  public Model getModel( Model startModel, Context context ) {
    if (value != null) {
      return new SimpleModel(StringType.getDefString(), value);
    }
    else {
      try {
        Model result = getModelList( startModel, context );
        int size = result.childCount( SimpleReference.ID_SIMPLE_REFERENCE );
        if (size > 1) {
          throw new IllegalArgumentException( "Expression returned more than one Model: " + toString() );
        }
        else if (size == 1) {
          return ((SimpleReference) result.get( SimpleReference.ID_SIMPLE_REFERENCE, null )).refModel();
        }
        else {
          return null;
        }
      }
      catch (Exception ex) {
        throw new IllegalArgumentException( "Error processing XPath="+toString(), ex );
      }
    }
  }

  /**
   * Returns the simple value identified by the path starting from startModel. If that
   * path does not exist from startModel null is returned.
   *
   * @param startModel the beginning Model for this path
   * @param context    the evaluation context, containing root model and variables
   * @return the Model identifier by this path or null
   */
  public Object getValue( Model startModel, Context context ) {
    Object resultValue;
    if (value != null) {
      return value;
    }
    else {
      Model result = getModelList( startModel, context );
      int size = result.childCount( SimpleReference.ID_SIMPLE_REFERENCE );
      if (size > 1) {
        throw new IllegalArgumentException("Expression returned more than one Model!");
      }
      else if (size == 1) {
        resultValue = ((SimpleReference) result.get( SimpleReference.ID_SIMPLE_REFERENCE, null )).refModel();
        return resultValue = ((Model) resultValue).getContent();
      }
      else {
        return null;
      }
    }
  }

  /**
   * Returns the simple value identified by the path starting from startModel as string. If that
   * path does not exist from startModel null is returned.
   *
   * @param startModel the beginning Model for this path
   * @param context    the evaluation context, containing root model and variables
   * @return the Model identifier by this path or null
   */
  public String getStringValue(Model startModel, Context context) {
    Object value = getValue( startModel, context );
    if (value == null) {
      return null;
    }
    else if (value instanceof QName) {
      return ((QName) value).getName();
    }
    else {
      return value.toString();
    }
  }

  public String toString() {
    StringBuffer result = new StringBuffer();
    if (startWithRoot) result.append('/');
    if (firstLocation != null) result.append(firstLocation.toString());
    return result.toString();
  }

  public String toStringNoNS() {
    StringBuffer result = new StringBuffer("Path");
    String modelName;
    int index;
/*    for (int i = (path.length - 1); i >= 0; i--) {
      if (i < (path.length - 1)) {
        result.append("/");
      }
      //delete namespace
      modelName = ((QName) path[i]).getName();
      index = modelName.lastIndexOf('.');

      if (index != 0) {
        modelName = modelName.substring(index + 1);
      }
      result.append(modelName);
    } */
    return result.toString();
  }

  /**
   * Creates a new XPath by appending appendPath to this XPath
   * @param appendPath  the XPath to append to this XPath
   * @return  the appended XPath
   */
  public XPath append( XPath appendPath ) {
    //TODO optimieren: der neue XPath müssten ohne parsen konstuierbar sein
    return new XPath( getNamespaceTable(), getPathExpression() + "/" + appendPath.getPathExpression() );
  }
}
