/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.models.types.simple.DateObject;
import de.pidata.qnames.QName;

public class ComparatorDateTime implements Comparator<Model> {

  private QName dateAttrName;
  private QName timeAttrName;
  private XPath modelPath;

  /**
   * Creates a new date comparator
   * @param dateAttrName the attribute containing a date value to use for comparision
   * @param timeAttrName the attribute containing a time value to use for comparision, may be null
   */
  public ComparatorDateTime( QName dateAttrName, QName timeAttrName ) {
    this( null, dateAttrName, timeAttrName );
  }

  /**
   * Creates a new date comparator
   * @param modelPath    xpath to select model containing attributes
   * @param dateAttrName the attribute containing a date value to use for comparision
   * @param timeAttrName the attribute containing a time value to use for comparision, may be null
   */
  public ComparatorDateTime( XPath modelPath, QName dateAttrName, QName timeAttrName ) {
    if (dateAttrName == null) {
      throw new IllegalArgumentException( "attributeName must not be null" );
    }
    this.dateAttrName = dateAttrName;
    this.timeAttrName = timeAttrName;
    this.modelPath = modelPath;
  }

  /**
   * Compares models m1 and m2 by their time millis values of attribute given via constructor.
   * Null is treated as zero
   *
   * @param m1 a model
   * @param m2 a model
   * @return -1 (m1 less m2), 0 (m1 equal m2) or 1 (m1 greater m2)
   * @throws IllegalArgumentException if models cannot be compared by this SortRole
   */
  public int compare( Model m1, Model m2 ) {
    long[] value1 = fetchValues( m1 );
    long[] value2 = fetchValues( m2 );
    if (value1[0] == value2[0]) {
      if (value1[1] == value2[1]) return 0;
      else if (value1[1] > value2[1]) return 1;
      else return -1;
    }
    else if (value1[0] > value2[0]) return 1;
    else return -1;
  }

  private long[] fetchValues( Model model ) {
    long[] values = new long[2];
    if (modelPath != null) {
      model = modelPath.getModel( model, null );
      if (model == null) {
        values[0] = 0;
        values[1] = 0;
        return values;
      }
    }
    Object attr = model.get( dateAttrName );
    if (attr == null) values[0] = 0;
    else values[0] = ((DateObject) attr).getTime();
    if (timeAttrName == null) {
      values[1] = 0;
    }
    else {
      attr = model.get( timeAttrName );
      if (attr == null) values[1] = 0;
      else values[1] = ((DateObject) attr).getTime();
    }
    return values;
  }
}
