/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Relation;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.DefaultRelation;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.Collection;
import java.util.Hashtable;

public class DynamicModelFactory extends DefaultModelFactory {

  public DynamicModelFactory( Namespace targetNamespace, String schemaLocation, String version ) {
    super( targetNamespace, schemaLocation, version );
  }

  /**
   * Adds relation to this factory's root relations
   * @param rootRel the relation to be added
   * @implNote IMPORTANT: prevent creation of new type here as this is used for static setup too!
   */
  @Override
  public void addRootRelation( Relation rootRel ) {
    QName relName = rootRel.getRelationID();
    if (relName.getNamespace() != getTargetNamespace()) {
      throw new IllegalArgumentException( "Must not add root relation to a factory with different Namespace, "
          + "relationName="+relName+", factory namspace="+getTargetNamespace() );
    }
    // do not call getRootRelation here to avoid creating a new type
    Relation oldRel = this.rootRelations.get( relName );
    if (oldRel != null) {
      throw new IllegalArgumentException("Root relation is already defined: name="+rootRel.getRelationID());
    }
    this.rootRelations.put( relName, rootRel );
  }

  /**
   * Returns the root relation for the given relationName or null if not defined
   *
   * @param relationName name of the relation to be returned
   * @return the root relation for the given relationName or null
   */
  @Override
  public Relation getRootRelation( QName relationName ) {
    Relation relation = super.getRootRelation( relationName );
    if (relation == null) {
      DynamicType type = new DynamicType( relationName, DefaultModel.class.getName(), 0, null, false, true );
      addType( type );
      relation = new DefaultRelation( relationName, type, 0, Integer.MAX_VALUE, Collection.class, null );
      this.rootRelations.put( relationName, relation );
    }
    return relation;
  }

  /**
   * Returns the root relation for the given relationName only if it already exists.
   * Does not create a dynamic one.
   *
   * @param relationName
   * @return
   */
  public Relation getExistingRootRelation( QName relationName ) {
    return super.getRootRelation( relationName );
  }

  /**
   * Creates a new instance for the given type.
   *
   * @param key
   * @param type       the type definition describing the model to be created
   * @param attributes the attributes for the new instace - must be same order
   *                   than defined in type; may be null
   * @param anyAttribs Hastable containing attributes according to anyAttribute
   *                   definition of type, may be null
   * @param children   the initial list of children, may be null
   * @return a new model instance for the given type
   */
  @Override
  public Model createInstance( Key key, Type type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    DynamicType complexType = (DynamicType) type;
    int attrCount = complexType.definedAttributeCount();
    if (attributes != null && attributes.length > attrCount) {
      Object[] newAttr = new Object[attrCount];
      for (int i = 0; i < attrCount; i++) {
        newAttr[i] = attributes[i];
      }
      return new DefaultModel( key, (ComplexType) type, newAttr, anyAttribs, children );
    }
    else {
      return new DefaultModel( key, (ComplexType) type, attributes, anyAttribs, children );
    }
  }
}
