/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

/**
 * Interface for all classes containing a model. Whenever the model is replaced by another (or removed)
 * all registered EventListeners are notified.
 */
public interface ModelSource {

  /**
   * Returns the current data model of this data source
   * @return the current data model
   */
  public Model getModel();

  /**
   * Adds listener to this ModelSource's listener list
   *
   * @param listener the listener to be added
   */
  public void addListener( EventListener listener );

  /**
   * Removes listener from this ModelSource's listener list
   *
   * @param listener the listener to be removed
   */
  public void removeListener( EventListener listener );
}
