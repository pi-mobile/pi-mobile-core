/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

import java.util.Hashtable;

/**
 * <P> For each type of Model there exists a type definition and a model factory. The factory
 * splits the part which makes calls to the model loader from the part containing
 * the entire model. That allows a completely different creation of a tree than via
 * model loader</P>
 */
public interface ModelFactory {

  public static final Namespace NAMESPACE = Namespace.getInstance( "de.pidata.models" );

  /**
   * Creates a new model instance for the given simple type.
   * @param type    the new model's type
   * @param value   the new model's value
   * @return a new model instance for the given simple type
   */
  public Model createInstance(SimpleType type, Object value);

  /**
   * Creates a new instance for the given type.
   *
   * @param key     the new model's key or null if type's is a SimpleType or it's keyAttributeCount is 0
   * @param type    the type definition describing the model to be created
   * @param attributes  the attributes for the new instace - must be same order
   *                    than defined in type; may be null
   * @param anyAttribs  Hastable containing attributes according to anyAttribute
   *                    definition of type, may be null
   * @param children    the initial list of children, may be null
   * @return a new model instance for the given type
   */
  public Model createInstance( Key key, Type type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children);

  /**
   * Creates a new instance for the given type. Teh result's ID is the type name.
   *
   * @param type  the type definition describing the model to be created
   * @return a new model instance for the given type
   */
  public Model createInstance(ComplexType type);

  /**
   * Creates a new instance for the root relation definied by rootRelationName
   *
   * @param rootRelationName the name of a root relation defined by this factory
   * @return a new instance for rootRelationName
   */
  public Model createInstance(QName rootRelationName);

  /**
   * Returns this Factory's target namespace. There is a 1:1 assuziation between Factory and
   * targetNamespce.
   * @return this Factory's target namespace
   */
  public Namespace getTargetNamespace();

  /**
   * Returns the root relation for the given relationName or null if not defined
   * @param relationName namre of the relation to be returned
   * @return root relation
   */
  public Relation getRootRelation(QName relationName);

  /**
   * Returns the type for the given attribute name
   * @param attrName the attribute's name
   * @return the attribute's type or null if not found
   */
  public SimpleType getAttribute(QName attrName);

  /**
   * Returns the type definition for the given type name.
   * Hint: Use FactoryTable to get a type you do not know which factory supports it.
   * @param typeName the name of the type to be returned
   * @return the type definition for the given type name or null if type is unknown
   */
  public Type getType(QName typeName);

  /**
   * Returns this factory's schema loacation
   * @return this factory's schema loacation or null
   */
  public String getSchemaLocation();

  /**
   * Returns the version of this factory. This is the value of the schema's version attribute. 
   * @return the version of this factory
   */
  public String getVersion();

  /**
   * Adds the given Type to this Factory
   * @param type the type to be added
   * @throws IllegalArgumentException if type's namespace is different from factories
   *               targetNamespace or if type already exists for this factory
   */
  public void addType( Type type );

  /**
   * Adds relation to this factory's root relations
   * @param rootRel the relation to be added
   */
  public void addRootRelation(Relation rootRel);

  /**
   * Creates a relation for the given relationName and adds it to this
   * factory's root relations
   * @param relationName the relation name
   * @param type         the relations's type
   * @param minOccurs    the min occurence
   * @param maxOccurs    the max occurence
   * @param substitutionGroup the relations's substitutionGroup or null
   * @return the created relation
   */
  public Relation addRootRelation( QName relationName, Type type, int minOccurs, int maxOccurs, QName substitutionGroup );

  public QNameIterator typeNames();

  public QNameIterator relationNames();
}
