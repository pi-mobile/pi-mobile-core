/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.qnames.QName;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

public class ModelList<CM extends Model> extends ModelCollection<CM> implements List<CM> {

  /**
   *
   * @param relationID
   * @param childList
   *
   * @deprecated generator should use {{@link #ModelList(QName, SequenceModel)}} instead
   */
  @Deprecated
  public ModelList( QName relationID, ChildList childList ) {
    super( relationID, (SequenceModel) childList.getParent() );
  }

  public ModelList( QName relationID, SequenceModel owner ) {
    super( relationID, owner );
  }

  /**
   * Inserts all of the elements in the specified collection into this
   * list at the specified position (optional operation).  Shifts the
   * element currently at that position (if any) and any subsequent
   * elements to the right (increases their indices).  The new elements
   * will appear in this list in the order that they are returned by the
   * specified collection's iterator.  The behavior of this operation is
   * undefined if the specified collection is modified while the
   * operation is in progress.  (Note that this will occur if the specified
   * collection is this list, and it's nonempty.)
   *
   * @param index index at which to insert the first element from the
   *              specified collection
   * @param c     collection containing elements to be added to this list
   * @return <tt>true</tt> if this list changed as a result of the call
   * @throws UnsupportedOperationException if the <tt>addAll</tt> operation
   *                                       is not supported by this list
   * @throws ClassCastException            if the class of an element of the specified
   *                                       collection prevents it from being added to this list
   * @throws NullPointerException          if the specified collection contains one
   *                                       or more null elements and this list does not permit null
   *                                       elements, or if the specified collection is null
   * @throws IllegalArgumentException      if some property of an element of the
   *                                       specified collection prevents it from being added to this list
   * @throws IndexOutOfBoundsException     if the index is out of range
   *                                       (<tt>index &lt; 0 || index &gt; size()</tt>)
   */
  @Override
  public boolean addAll( int index, Collection<? extends CM> c ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns the element at the specified position in this list.
   *
   * @param index index of the element to return
   * @return the element at the specified position in this list
   * @throws IndexOutOfBoundsException if the index is out of range
   *                                   (<tt>index &lt; 0 || index &gt;= size()</tt>)
   */
  @Override
  public CM get( int index ) {
    Model child = owner.firstChild( relationID );
    int i = 0;
    while (child != null) {
      if (i == index) return (CM) child;
      child = child.nextSibling( relationID );
      i++;
    }
    return null;
  }

  /**
   * Replaces the element at the specified position in this list with the
   * specified element (optional operation).
   *
   * @param index   index of the element to replace
   * @param element element to be stored at the specified position
   * @return the element previously at the specified position
   * @throws UnsupportedOperationException if the <tt>set</tt> operation
   *                                       is not supported by this list
   * @throws ClassCastException            if the class of the specified element
   *                                       prevents it from being added to this list
   * @throws NullPointerException          if the specified element is null and
   *                                       this list does not permit null elements
   * @throws IllegalArgumentException      if some property of the specified
   *                                       element prevents it from being added to this list
   * @throws IndexOutOfBoundsException     if the index is out of range
   *                                       (<tt>index &lt; 0 || index &gt;= size()</tt>)
   */
  @Override
  public CM set( int index, CM element ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Inserts the specified element at the specified position in this list
   * (optional operation).  Shifts the element currently at that position
   * (if any) and any subsequent elements to the right (adds one to their
   * indices).
   *
   * @param index   index at which the specified element is to be inserted
   * @param element element to be inserted
   * @throws UnsupportedOperationException if the <tt>add</tt> operation
   *                                       is not supported by this list
   * @throws ClassCastException            if the class of the specified element
   *                                       prevents it from being added to this list
   * @throws NullPointerException          if the specified element is null and
   *                                       this list does not permit null elements
   * @throws IllegalArgumentException      if some property of the specified
   *                                       element prevents it from being added to this list
   * @throws IndexOutOfBoundsException     if the index is out of range
   *                                       (<tt>index &lt; 0 || index &gt; size()</tt>)
   */
  @Override
  public void add( int index, CM element ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Removes the element at the specified position in this list (optional
   * operation).  Shifts any subsequent elements to the left (subtracts one
   * from their indices).  Returns the element that was removed from the
   * list.
   *
   * @param index the index of the element to be removed
   * @return the element previously at the specified position
   * @throws UnsupportedOperationException if the <tt>remove</tt> operation
   *                                       is not supported by this list
   * @throws IndexOutOfBoundsException     if the index is out of range
   *                                       (<tt>index &lt; 0 || index &gt;= size()</tt>)
   */
  @Override
  public CM remove( int index ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns the index of the first occurrence of the specified element
   * in this list, or -1 if this list does not contain the element.
   * More formally, returns the lowest index <tt>i</tt> such that
   * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>,
   * or -1 if there is no such index.
   *
   * @param o element to search for
   * @return the index of the first occurrence of the specified element in
   * this list, or -1 if this list does not contain the element
   * @throws ClassCastException   if the type of the specified element
   *                              is incompatible with this list
   *                              (<a href="Collection.html#optional-restrictions">optional</a>)
   * @throws NullPointerException if the specified element is null and this
   *                              list does not permit null elements
   *                              (<a href="Collection.html#optional-restrictions">optional</a>)
   */
  @Override
  public int indexOf( Object o ) {
    Model child = owner.firstChild( relationID );
    int index = 0;
    while (child != null) {
      if (child == o) return index;
      child = child.nextSibling( relationID );
      index++;
    }
    return -1;
  }

  /**
   * Returns the index of the last occurrence of the specified element
   * in this list, or -1 if this list does not contain the element.
   * More formally, returns the highest index <tt>i</tt> such that
   * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>,
   * or -1 if there is no such index.
   *
   * @param o element to search for
   * @return the index of the last occurrence of the specified element in
   * this list, or -1 if this list does not contain the element
   * @throws ClassCastException   if the type of the specified element
   *                              is incompatible with this list
   *                              (<a href="Collection.html#optional-restrictions">optional</a>)
   * @throws NullPointerException if the specified element is null and this
   *                              list does not permit null elements
   *                              (<a href="Collection.html#optional-restrictions">optional</a>)
   */
  @Override
  public int lastIndexOf( Object o ) {
    return indexOf( o );
  }

  /**
   * Returns a list iterator over the elements in this list (in proper
   * sequence).
   *
   * @return a list iterator over the elements in this list (in proper
   * sequence)
   */
  @Override
  public ListIterator<CM> listIterator() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns a list iterator over the elements in this list (in proper
   * sequence), starting at the specified position in the list.
   * The specified index indicates the first element that would be
   * returned by an initial call to {@link ListIterator#next next}.
   * An initial call to {@link ListIterator#previous previous} would
   * return the element with the specified index minus one.
   *
   * @param index index of the first element to be returned from the
   *              list iterator (by a call to {@link ListIterator#next next})
   * @return a list iterator over the elements in this list (in proper
   * sequence), starting at the specified position in the list
   * @throws IndexOutOfBoundsException if the index is out of range
   *                                   ({@code index < 0 || index > size()})
   */
  @Override
  public ListIterator<CM> listIterator( int index ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns a view of the portion of this list between the specified
   * <tt>fromIndex</tt>, inclusive, and <tt>toIndex</tt>, exclusive.  (If
   * <tt>fromIndex</tt> and <tt>toIndex</tt> are equal, the returned list is
   * empty.)  The returned list is backed by this list, so non-structural
   * changes in the returned list are reflected in this list, and vice-versa.
   * The returned list supports all of the optional list operations supported
   * by this list.<p>
   * <p>
   * This method eliminates the need for explicit range operations (of
   * the sort that commonly exist for arrays).  Any operation that expects
   * a list can be used as a range operation by passing a subList view
   * instead of a whole list.  For example, the following idiom
   * removes a range of elements from a list:
   * <pre>{@code
   *      list.subList(from, to).clear();
   * }</pre>
   * Similar idioms may be constructed for <tt>indexOf</tt> and
   * <tt>lastIndexOf</tt>, and all of the algorithms in the
   * <tt>Collections</tt> class can be applied to a subList.<p>
   * <p>
   * The semantics of the list returned by this method become undefined if
   * the backing list (i.e., this list) is <i>structurally modified</i> in
   * any way other than via the returned list.  (Structural modifications are
   * those that change the size of this list, or otherwise perturb it in such
   * a fashion that iterations in progress may yield incorrect results.)
   *
   * @param fromIndex low endpoint (inclusive) of the subList
   * @param toIndex   high endpoint (exclusive) of the subList
   * @return a view of the specified range within this list
   * @throws IndexOutOfBoundsException for an illegal endpoint index value
   *                                   (<tt>fromIndex &lt; 0 || toIndex &gt; size ||
   *                                   fromIndex &gt; toIndex</tt>)
   */
  @Override
  public List<CM> subList( int fromIndex, int toIndex ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  public Object[] toArray( Object[] a ) {
    return super.toArray( a );
  }
}
