/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.log.Logger;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.complex.DefaultRelation;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.Collection;

public class DynamicType extends DefaultComplexType {

  public DynamicType( QName typeID, String modelClassName, int additionalKeyAttributeCount, Type parentType, boolean abstractType, boolean mixedType ) {
    super( typeID, modelClassName, additionalKeyAttributeCount, parentType, abstractType, mixedType );
    addAttributeType( AnyAttribute.ANY_ATTRIBUTE, AnyAttribute.ANY_ATTR_TYPE );

    Logger.info( this.getClass().getSimpleName() + " created: " + typeID + " [" + modelClassName + "] " + (parentType != null ? ("within " + parentType) : "" ) );
  }

  /**
   * Create array to store attributes defined by this type.
   * Needed by XML parser.
   * @return object array for attributes with matching size for this type
   */
  @Override
  public Object[] createAttributeArray() {
    return new Object[20];
  }

  public int definedAttributeCount() {
    return super.attributeCount();
  }

  public boolean isDefined( QName attributeID ) {
    return (super.indexOfAttribute( attributeID ) >= 0);
  }

  /**
   * Returns the default value for the attribute at index or null if
   * a default is not defined.
   *
   * @param index index of the attribute
   * @return default value for the attribute at index or null
   */
  @Override
  public Object getAttributeDefault( int index ) {
    return null;
  }

  /**
   * Returns the type definition for the attribute at index.
   *
   * @param index index of the attribute
   * @return the type definition for the attribute at index.
   */
  public SimpleType getAttributeType( int index) {
    if (index < definedAttributeCount()) {
      return super.getAttributeType( index );
    }
    else {
      return null;
    }
  }

  /**
   * Returns the ID (name) for the attribute at index.
   *
   * @param index index of the attribute
   * @return the ID (name) for the attribute at index.
   */
  @Override
  public QName getAttributeName( int index ) {
    if (index < definedAttributeCount()) {
      return super.getAttributeName( index );
    }
    else {
      return null;
    }
  }

  /**
   * Returns the index of the attribute identified by attributeID.
   * This may be used to store attributes in an array. The order
   * of attributes depends on the order in the type definition
   *
   * @param attributeID the attribute's ID
   * @return the index of attributeID or -1 if attributeID does
   * not exist
   */
  @Override
  public int indexOfAttribute( QName attributeID ) {
    int index = super.indexOfAttribute( attributeID );
    if (index < 0) {
      addAttributeType( attributeID, StringType.getDefString() );
      return super.indexOfAttribute( attributeID );
    }
    else {
      return index;
    }
  }

  /**
   * Returns the child relation identified by relationID or null if relationID is
   * not defined.
   *
   * @param relationID ID of the child relation for which type information is requested
   * @return the child relation identified by relationID
   */
  @Override
  public Relation getRelation( QName relationID ) {
    Relation relation = super.getRelation( relationID );
    if (relation == null) {
      Namespace namespace = relationID.getNamespace();
      ModelFactory modelFactory = ModelFactoryTable.getInstance().getFactory( namespace );
      if (modelFactory == null) {
        Logger.warn( "Missing ModelFactory for namespace="+namespace+" --> creating DynamicModelFactory" );
        modelFactory = new DynamicModelFactory( namespace, namespace.toString(), "1.0" );
      }
      Type type = modelFactory.getType( relationID );
      if (type == null) {
        type = new DynamicType( relationID, DefaultModel.class.getName(), 0, null, false, true );
        modelFactory.addType( type );
      }
      relation = new DefaultRelation( relationID, type, 0, Integer.MAX_VALUE, Collection.class, null );
      addRelation( relation );
    }
    return relation;
  }
}
