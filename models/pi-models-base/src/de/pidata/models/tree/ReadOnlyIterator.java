/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import java.util.Iterator;

public class ReadOnlyIterator implements ModelIterator<Model> {

  private ModelIterator sourceIter;

  public ReadOnlyIterator( ModelIterator sourceIter ) {
     this.sourceIter = sourceIter;
  }

  /**
   * Returns an iterator over a set of elements of type T.
   *
   * @return an Iterator.
   */
  @Override
  public Iterator<Model> iterator() {
    return this;
  }

  /**
   * Returns true if there is a further Model in this iteration, i.e. the next call to next()
   * will return a Model not null.
   *
   * @return true if there is a further Model in this iteration
   */
  public boolean hasNext() {
    return sourceIter.hasNext();
  }

  /**
   * Returns the next Model of this iterator and sets the internal pointer to the next Model.
   *
   * @return the next Model of this iteration
   * @throws IllegalArgumentException if there is no next Model in this iterator
   */
  public Model next() {
    return new ReadOnlyWrapper( sourceIter.next() );
  }

  /**
   * Removes from the underlying collection the last element returned
   * by this iterator (optional operation).  This method can be called
   * only once per call to {@link #next}.  The behavior of an iterator
   * is unspecified if the underlying collection is modified while the
   * iteration is in progress in any way other than by calling this
   * method.
   *
   * @throws UnsupportedOperationException if the {@code remove}
   *                                       operation is not supported by this iterator
   * @throws IllegalStateException         if the {@code next} method has not
   *                                       yet been called, or the {@code remove} method has already
   *                                       been called after the last call to the {@code next}
   *                                       method
   */
  @Override
  public void remove() {
    throw new IllegalArgumentException( "Cannot remove: This iterator is read only" );
  }

  public String toString() {
    return "ModeliteratorReadOnly, souureIter="+sourceIter;
  }
}
