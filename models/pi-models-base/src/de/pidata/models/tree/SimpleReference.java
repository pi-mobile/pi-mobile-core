/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.tree;

import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;

public class SimpleReference extends SequenceModel implements ModelReference {

  public static final QName ID_NAME = BaseFactory.NAMESPACE.getQName("name");
  public static final QName ID_TITLE = BaseFactory.NAMESPACE.getQName("title");
  public static final QName ID_SIMPLE_REFERENCE = BaseFactory.NAMESPACE.getQName( "simpleReference" );

  public static final ComplexType TYPE;
  public static final ComplexType CONTAINER_TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( ID_SIMPLE_REFERENCE, SimpleReference.class.getName(), 0 );
    TYPE = type;
    type.addAttributeType( ID_NAME, QNameType.getQName() );
    type.addAttributeType( ID_TITLE, StringType.getDefString() );

    type = new DefaultComplexType( BaseFactory.NAMESPACE.getQName( "refContainer" ), DefaultModel.class.getName(), 0 );
    type.addRelation( ID_SIMPLE_REFERENCE, TYPE, 0, Integer.MAX_VALUE );
    CONTAINER_TYPE = type;
  }

  private Model refModel;

  public SimpleReference( QName name, String title, Model refModel ) {
    super( null, TYPE, null, null, null );
    this.refModel = refModel;
    set( ID_NAME, name );
    set( ID_TITLE, title );
  }

  /**
   * Returns the name of this reference
   *
   * @return the name of this reference
   */
  public QName getName() {
    return (QName) get( ID_NAME );
  }

  public String getTitle() {
    return (String) get( ID_TITLE );
  }

  /**
   * Returns the name of the type referenced
   *
   * @return the name of the type referenced
   */
  public QName getRefTypeName() {
    return refModel.type().name();
  }

  /**
   * Returns the name of the referencing attribute at index.
   * Order and type of the attributes is the same as defined by the
   * key of the referenced type
   *
   * @param index index of the referencing attribute
   * @return name of the referencing attribute at index
   */
  public QName getRefAttribute( int index ) {
    Type type = refModel.type();
    if (type instanceof SimpleType) {
      return type.name();
    }
    else {
      return ((ComplexType) type).getKeyAttribute( index );
    }
  }

  /**
   * Returns the model referenced by this model
   *
   * @return the model referenced by this model
   */
  public Model refModel() {
    return refModel;
  }

  /**
   * Creates a container Model for SimpleReference models
   * @return container Model of type CONTAINER_TYPE
   */
  public static Model createContainer() {
    return new DefaultModel( null, CONTAINER_TYPE );
  }

  public static void addReference( Model result, Model refModel ) {
    int count = result.childCount( ID_SIMPLE_REFERENCE );
    SimpleReference reference = new SimpleReference( ID_SIMPLE_REFERENCE, ""+count, refModel );
    result.add( SimpleReference.ID_SIMPLE_REFERENCE, reference );
  }
}
