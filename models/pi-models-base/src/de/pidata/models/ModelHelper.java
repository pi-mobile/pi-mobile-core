/*
 * This file is part of PI-Mobile (https://gitlab.com/pi-mobile/pi-tool-apis).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models;

import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.tree.SimpleModel;

/**
 * Provides common methods which help to deal with mixed content models.
 */
public class ModelHelper {

  /**
   * Returns the plain text content of the given {@link Model}. Strips all tags.
   *
   * @param model
   * @return
   */
  public static String getContent( Model model ) {
    StringBuilder sb = new StringBuilder();
    ModelIterator valuePartIterator = model.iterator( null, null );
    while (valuePartIterator.hasNext()) {
      Model next = valuePartIterator.next();
      if (next instanceof SimpleModel) {
        Object content = next.getContent();
        if (content != null) {
          sb.append( content.toString() );
        }
      }
      else {
        sb.append( getContent( next ) );
      }
    }
    return sb.toString();
  }
}
