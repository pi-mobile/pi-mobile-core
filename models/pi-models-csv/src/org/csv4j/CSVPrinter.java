/*
 * CSVPrinter.java
 * Printer for CSV-streams. Write csv-tokens to streams. 
 * Created on Mai 23, 2005
 *
 * Copyright 2005 SoftMethod GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.csv4j;

import java.io.IOException;
import java.io.Writer;

/**
 * Printer for CSV-streams. Write csv-tokens to streams.
 *
 * @author Andreas Meyer (andreas.meyer@softmethod.de)
 *         <p/>
 *         Last modified: Felix, 2005.12.03, Bugfixing Comment-Char not escaped
 *         Last modified: Andi, 2005.12.20, Changing auto-qutoe mechanism
 */
public class CSVPrinter {

  /**
   * Enum for quote-style
   */
  public static class QuoteStyle {
    private QuoteStyle() {
    }

    public static final QuoteStyle QUOTE_ALWAYS = new QuoteStyle();
    public static final QuoteStyle QUOTE_STRINGS = new QuoteStyle();
    public static final QuoteStyle QUOTE_MINIMAL = new QuoteStyle();
  }

  /**
   * Enum for escape-style
   */
  public static class EscapeStyle {
    private EscapeStyle() {
    }

    public static final EscapeStyle ESCAPE_BACKSLASH = new EscapeStyle();
    public static final EscapeStyle ESCAPE_DOUBLE = new EscapeStyle();
    public static final EscapeStyle REPLACE_QUOTE = new EscapeStyle();
  }

  /**
   * Delimiter dividing tokens
   */
  private char tokDelim;

  /**
   * The chars that delimits a line
   */
  private String eolMarker = "\n";

  /**
   * Target-Writer
   */
  private Writer w;

  /**
   * Switch for the state-machine:
   * Are we at the start of a line?
   */
  private boolean atSOL = true;

  /**
   * Quoting style:
   * - quote always
   * - quote all strings (not numbers)
   * - quote only neccessary tokens (containing token delimiters,
   * quote-chars or comment-chars)
   */
  private QuoteStyle quoteStyle;

  /**
   * Escape style:
   * escape quote-char in quoted string ( "abc"def")
   * with
   * - backslash    ("abc\"def")
   * - double-quote ("abc""def")
   */
  private EscapeStyle escapeStyle;

  /**
   * The char used for quotes
   */
  private char quoteChar;

  /**
   * The char used for comments
   */
  private char commentChar;

  //alternate number matcher via regex (not used at the moment)
  //private static final Matcher MATCHER_NUMBER = Pattern.compile("^-?[0-9]+$").matcher("");

  /**
   * Constructor that inits the printer using a Writer as source.
   */
  public CSVPrinter( Writer w ) {
    this.w = w;

    // init defaults
    setQuoteChar( '\"' );
    setCommentChar( '#' );
    setTokenDelimiter( ',' );
    setEOL( "\n" );
    setQuoteStyle( QuoteStyle.QUOTE_STRINGS );
    setEscapeStyle( EscapeStyle.REPLACE_QUOTE );
  }

  /**
   * Close the writer and flush the stream
   */
  public void close() throws IOException {
    w.close();
  }

  /**
   * Setter for the EOL-Marker
   *
   * @param td The String that divides tokens
   */
  public void setEOL( String eol ) {
    this.eolMarker = eol;
  }

  /**
   * Returns the EOL-Marker.
   *
   * @returns The EOL-String
   */
  public String getEOL() {

    return eolMarker;
  }

  /**
   * Setter for token delimiter
   *
   * @param td The String that divides tokens
   */
  public void setTokenDelimiter( char td ) {
    this.tokDelim = td;
  }

  /**
   * Returns the Token-Delimiter.
   *
   * @returns The Toekn-Delimiter
   */
  public char getTokenDelimiter() {

    return tokDelim;
  }

  /**
   * Setter for comment char
   *
   * @param cc The char that inits a comment line
   */
  public void setCommentChar( char cc ) {
    commentChar = cc;
  }

  /**
   * Returns the Comment-Char
   *
   * @returns The EOL-String
   */
  public char getCommentChar() {

    return commentChar;
  }

  /**
   * Setter for quote char
   *
   * @param qc The char that encloses a quoted literal
   */
  public void setQuoteChar( char qc ) {
    quoteChar = qc;
  }

  /**
   * Setter for quote style
   *
   * @param qs The Quoting style to be used in the printer
   */
  public void setQuoteStyle( QuoteStyle qs ) {
    this.quoteStyle = qs;
  }

  /**
   * Returns the QuoteStyle
   *
   * @returns The QuoteStyle
   */
  public QuoteStyle getQuoteStyle() {

    return quoteStyle;
  }

  /**
   * Setter for escape style
   *
   * @param es The Escaping style to be used in the printer
   */
  public void setEscapeStyle( EscapeStyle es ) {
    this.escapeStyle = es;
  }

  /**
   * Returns the EscapeStyle
   *
   * @returns The EscapeStyle
   */
  public EscapeStyle getEscapeStyle() {

    return escapeStyle;
  }

  /**
   * Print a comment line that is normally ignored by CSV-Readers
   *
   * @param comt Comment-Text
   */
  public void printlnComment( String comt ) throws IOException {

    // felix, 2005.10.23 - Now also writes a trailing blank after the comment-char
    _write( commentChar + " " + comt + eolMarker );

    atSOL = true;
  }

  /**
   * Print a normal token to the stream
   *
   * @param s Token to write
   */
  public void print( String s ) throws IOException {

    // prevent NullPointerEx
    if (s == null) {
      s = "null";
    }

    // write no delimiter at start-of-line
    if (atSOL) {
      _write( escape( s ) );
      atSOL = false;
    }
    else {
      _write( tokDelim + escape( s ) );
    }
  }

  /**
   * Escape a string literal
   *
   * @param src Token to escape
   */
  private String escape( String src ) {

    // ignore empty source string
    if (src == null)
      return null;

    // check if we need quoting
    boolean needQuote = false;

    if (quoteStyle == QuoteStyle.QUOTE_ALWAYS) {
      needQuote = true;
    }
    else if (quoteStyle == QuoteStyle.QUOTE_STRINGS) {
      // check if token is an integer
      // 1. check if string is empty
      if (src.length() >= 1) {

          // 2. check if first char is a negative sign
        char c = src.charAt( 0 );
        if (c != '-' && !Character.isDigit( c )) {
          needQuote = true;
        }
        else {
          // 3. check if all other chars are digits
          for (int i = 1; i < src.length(); i++) {
            c = src.charAt( i );
            if (!Character.isDigit( c )) {
              needQuote = true;
              break;
            }
          }
        }
      }

      // alternate number matcher via regex
      //MATCHER_NUMBER.reset(src);
      //needQuote = !MATCHER_NUMBER.matches();
    }
    else if (quoteStyle == QuoteStyle.QUOTE_MINIMAL) {

      // check if src contain comment char
      if (src.indexOf( commentChar ) >= 0) {
        needQuote = true;
      }
      // check if src contain quote char
      else if (src.indexOf( quoteChar ) >= 0) {
        needQuote = true;
      }
      // check if src contain token delimiter
      else if (src.indexOf( tokDelim ) >= 0) {
        needQuote = true;
      }
    }

    // quote if needed
    if (needQuote) {
      StringBuffer sb = new StringBuffer();

      sb.append( quoteChar );
      for (int i = 0; i < src.length(); i++) {
        char c = src.charAt( i );

        if (c == quoteChar) {

          if (escapeStyle == EscapeStyle.ESCAPE_BACKSLASH) {
            sb.append( "\\" + quoteChar );
          }
          else if (escapeStyle == EscapeStyle.REPLACE_QUOTE) {
            sb.append( "'" );
          }
          else {
            sb.append( quoteChar );
            sb.append( quoteChar );
          }

        }
        else {
          sb.append( c );
        }
      }
      sb.append( quoteChar );

      // return quoted string
      return sb.toString();

    }

    // return unquoted string
    return src;

  }

  /**
   * Print a normal token to the stream and add a NEWLINE-char
   *
   * @param s Token to write
   */
  public void println( String s ) throws IOException {
    print( s );
    println();
  }

  /**
   * Print an empty line.
   */
  public void println() throws IOException {
    _write( eolMarker );
    atSOL = true;
  }

  /**
   * Print a token-line to the stream and add a NEWLINE-char
   *
   * @param sa Tokens to write to one line
   */
  public void println( String[] sa ) throws IOException {

    // ignore empty lines
    if (sa == null)
      return;

    // iterate array of tokens
    for (int i = 0; i < sa.length; i++) {
      print( sa[i] );
    }

    println();

  }

  /**
   * Print a Comment-Token-line to the stream and add a NEWLINE-char.
   * This makes it easy to print column-Headers.
   *
   * @param sa Tokens to write to one line
         * @author Felix Schupp <felix.schupp@softmethod.de>
         */
	public void printlnComment(String[] sa) throws IOException {
		
		// ignore empty lines
		if(sa == null)
			return;
		
	// Write the Comment-Char plus a trailing Space
	print(commentChar + " ");

		// iterate array of tokens
		for(int i=0;i<sa.length;i++) {
			print(sa[i]);
		}
		
		println();

	}


	private void _write(String msg) throws IOException {
		w.write(msg);
		w.flush();
	}
}
