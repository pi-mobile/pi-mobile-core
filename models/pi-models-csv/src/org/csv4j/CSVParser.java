/*
 * CSVParser.java
 * Parser for CSV-streams. Support for reading single tokens or a whole
 * line as Array. 
 * Created on Mai 19, 2005
 *
 * Copyright 2005 SoftMethod GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.csv4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Vector;

/**
 * Parser for CSV-streams. Support for reading single tokens or a whole
 * line as Array.
 *
 * @author Andreas Meyer (andreas.meyer@softmethod.de)
 */
public class CSVParser {

  private static final int STATE_LITERAL = 1;
  private static final int STATE_DELIMITER = 2;

  /**
   * Lexer that prepares the tokens
   */
  private CSVLexer lexer;

  /**
   * Switch for the state-machine:
   * Next token must be a literal or a delimiter?
   */
  private int curState;

  /**
   * Switch for the state-machine:
   * Are we at Begin Of Line?
   */
  private boolean atBOL = true;

  private String[] tokenCache;
  private int tokIndex = 0;

  /**
   * Constructor that inits the parser using a InputStream as source.
   */
  public CSVParser( InputStream is ) {
    lexer = new CSVLexer( new BufferedReader( new InputStreamReader( is ) ) );

    curState = STATE_LITERAL;

  }

  /**
   * Constructor that inits the parser using a InputStream as source
   * using a specified encoding
   */
  public CSVParser( InputStream is, String encoding ) throws UnsupportedEncodingException {
    lexer = new CSVLexer( new BufferedReader( new InputStreamReader( is, encoding ) ) );

    curState = STATE_LITERAL;

  }

  /**
   * Close the lexer and the underlying stream
   */
  public void close() throws IOException {

    lexer.close();
  }

  /**
   * Reads next token from lexer and parses that value.
   * Skipping delimiters or EOLs and reformatting empty tokens
   * is done here.
   *
   * @return Next parsed token
   */
  public String nextValue() throws IOException {

    // load cache if empty
    if (needCaching()) {
      loadCache();
    }

    String nextTok;

    if (tokIndex >= tokenCache.length) {
      loadCache();
      tokIndex = 0;
    }

    // return null if cache is not loadable
    if (tokenCache.length == 0)
      return null;

    nextTok = tokenCache[tokIndex++];

    return nextTok;
  }

  /**
   * Reads a whole line of tokens from lexer and parses all single values.
   * Skipping delimiters or EOLs and reformatting empty tokens
   * is done here.
   *
   * @return Next line of parsed tokens as array
   */
  public String[] getLine() throws IOException {
    loadCache();

    // return null if cache is not loadable
    if (tokenCache.length == 0)
      return null;

    return tokenCache;

  }

  public String getOrigLine() {
    return lexer.getOrigLine();
  }

  private boolean needCaching() {
    return (tokenCache == null);
  }

  private void loadCache() throws IOException {

    String nextTok;
    Vector list = new Vector();
    while ((nextTok = _nextValue()) != null) {

      // reformat empty token at eol
      if (nextTok.endsWith( "\n" )) {
        list.addElement( "" );
        break;

      }
      else {
        list.addElement( nextTok );

      }
    }

    //TODO Performance!!
    tokenCache = new String[list.size()];
    for (int i = list.size()-1; i >= 0; i--) {
      tokenCache[i] = (String) list.elementAt( i );
    }
  }

  private String _nextValue() throws IOException {

    // get next token from lexer
    int ttype = lexer.nextToken();

    // detect EOF
    if (ttype == CSVLexer.TT_EOF)
      return null;

    // determine what kind of token we want to get
    switch (curState) {

      // next token should be a literal
      case STATE_LITERAL:

        switch (ttype) {
          // next token is numeric literal; convert and return it
          case CSVLexer.TT_NUMBER:
            atBOL = false;
            curState = STATE_DELIMITER;
            return String.valueOf( (int) lexer.nval );

            // next token is string literal; return it
          case CSVLexer.TT_WORD:
            atBOL = false;
            curState = STATE_DELIMITER;
            return lexer.sval;

            // next token is quoted string literal; return it
          case CSVLexer.TT_QUOTED:
            atBOL = false;
            curState = STATE_DELIMITER;
            return lexer.sval;

            // next token is eol; this must be an empty token at eol
          case CSVLexer.TT_EOL:

            // detect empty line and skip
            if (atBOL) {
              return _nextValue();
            }
            else {
              atBOL = true;
              return "\n";
            }

            // next token is a delimiter; this must be an empty token at bol
            // or in the middle
          case CSVLexer.TT_DELIMITER:
            atBOL = false;
            return "";

        }

        // next token should be a delimiter
      case STATE_DELIMITER:

        switch (ttype) {
          // next token is a delimiter; skip it and return nextValue
          case CSVLexer.TT_DELIMITER:
            curState = STATE_LITERAL;
            return _nextValue();

            // next token is a eol; signal eol through null
          case CSVLexer.TT_EOL:
            atBOL = true;
            curState = STATE_LITERAL;
            return null;
        }

    }

    // reset token cache (read in next line)
    loadCache();

    // got sth else so return null
    throw new IOException( "unrecognised data format: state=" + curState + ", ttype=" + ttype + ", lineno=" + getLineNo() );
  }

  /**
   * Setter for comment char
   *
   * @param cc The char that inits a comment line
   */
  public void setCommentChar( char cc ) {
    lexer.setCommentChar( cc );
  }

  /**
   * Setter for quote char
   *
   * @param qc The char that encloses a quoted literal
   */
  public void setQuoteChar( char qc ) {
    lexer.setQuoteChar( qc );
  }

  /**
   * Setter for token delimiter
   *
   * @param td The String that divides tokens
   */
  public void setTokenDelimiter( char td ) {
    lexer.setTokenDelimiter( td );
  }

  public int getLineNo() {
    return lexer.lineno();
  }

  public void setReturnComments( boolean b ) {
    lexer.setReturnComments( b );
  }

}
