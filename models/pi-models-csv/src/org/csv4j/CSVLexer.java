/*
 * CSVLexer.java
 * Lexer for CSV-streams. 
 * Created on Mai 20, 2005
 *
 * Copyright 2005 SoftMethod GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.csv4j;

import java.io.IOException;
import java.io.Reader;
import java.io.StreamTokenizer;

/**
 * Lexer for CSV-streams.
 *
 * @author Andreas Meyer (andreas.meyer@softmethod.de)
 */
public class CSVLexer extends StreamTokenizer {

  public static final int TT_DELIMITER = -5;
  public static final int TT_QUOTED = -6;

  private char delimChar;
  private char cc;
  private char quoteChar;

  private Reader r;

  public int ttype;

  private StringBuffer lineBuffer;
  private boolean deleteLineBuffer;

  /**
   * Switch for comment handling:
   * Return Comment-lines?
   */
  private boolean returnComments = false;


  public CSVLexer( Reader r, char cc, char delim, char qc ) {
    super( r );

    this.r = r;

    this.cc = cc;
    this.delimChar = delim;
    this.quoteChar = qc;

    // set defaults
    init();
  }

  /**
   * Constructor that inits the lexer using a Reader as source.
   */
  public CSVLexer( Reader r ) {
    this( r, '#', ',', '"' );
  }

  /**
   * Close the underlying reader
   */
  public void close() throws IOException {

    r.close();
  }

  public int nextToken() throws IOException {

    if (deleteLineBuffer) {
      deleteLineBuffer = false;
      lineBuffer = new StringBuffer();
    }

    // read next token from superclass
    // prepare special csv-tokens
    // types:
    // TT_EOF       = -1
    // TT_EOL       = \n
    // TT_NUMBER    = -2
    // TT_WORD      = -3
    // TT_NOTHING   = -4
    // TT_DELIMITER = -5
    // TT_QUOTED    = -6

    ttype = super.nextToken();

    if (ttype == delimChar) {
      ttype = TT_DELIMITER;
      lineBuffer.append( delimChar );
    }
    else if (ttype == quoteChar) {
      ttype = TT_QUOTED;
      lineBuffer.append( quoteChar );
      lineBuffer.append( sval );
      lineBuffer.append( quoteChar );
    }
    else if (ttype == TT_NUMBER) {
      lineBuffer.append( nval );
    }
    else if (ttype == TT_WORD) {
      lineBuffer.append( sval );
    }
    else if (ttype == TT_EOF || ttype == TT_EOL) {
      deleteLineBuffer = true;
    }
    else {
      //lineBuffer.append(ttype);
    }

    return ttype;
  }

  public String getOrigLine() {
    return lineBuffer.toString();
  }

  public String toString() {
    StringBuffer ret = new StringBuffer();
    ret.append( "Token[" );

    switch (ttype) {
      case TT_DELIMITER:
        ret.append( "DELIMITER" );
        ret.append( "], line " + lineno() );
        break;

      default:
        ret.append( super.toString().substring( 6 ) );

    }
    return ret.toString();
  }

  /**
   * Setter for token delimiter
   *
   * @param td The String that divides tokens
   */
  void setTokenDelimiter( char td ) {
    delimChar = td;
    init();
  }

  /**
   * Setter for comment char
   *
   * @param cc The char that inits a comment line
   */
  void setCommentChar( char cc ) {
    this.cc = cc;
    init();
  }

  /**
   * Setter for quote char
   *
   * @param qc The char that encloses a quoted literal
   */
  public void setQuoteChar( char qc ) {
    quoteChar = qc;
    init();
  }

  public void setReturnComments( boolean b ) {
    returnComments = b;
    init();
  }


  private void init() {

    resetSyntax();

    // set base
    wordChars( 32, 255 );
    whitespaceChars( 0, 31 );

    // override comment if returnComments false
    if (!returnComments) {
      commentChar( cc );
    }

    // override delimiter
    ordinaryChar( delimChar );

    // set quoting char
    quoteChar( quoteChar );

    eolIsSignificant( true );

    lineBuffer = new StringBuffer();

  }


}
