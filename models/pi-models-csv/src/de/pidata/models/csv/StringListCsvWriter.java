package de.pidata.models.csv;

import de.pidata.log.Logger;
import de.pidata.messages.ErrorMessages;
import de.pidata.progress.Progress;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;
import org.csv4j.CSVPrinter;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;


public class StringListCsvWriter {

  private Storage storage;
  private CSVPrinter csvPrinter;
  private Charset charset = UTF_8;

  private Progress progress;


  public StringListCsvWriter( File directory, String filename, Progress progress ) throws IOException {
    this.storage = SystemManager.getInstance().getStorage( directory.getAbsolutePath() );
    this.progress = progress;
    init(filename);
  }

  public StringListCsvWriter( File file, Progress progress ) throws IOException {
    this.storage = SystemManager.getInstance().getStorage( file.getParentFile().getAbsolutePath() );
    this.progress = progress;
    init(file.getName());
  }

  public void setCharset( Charset charset ) {
    this.charset = charset;
  }

  private void init( String fileName ) throws IOException {
    OutputStream storageOut = this.storage.write( fileName, true, false );
    OutputStreamWriter out = new OutputStreamWriter( storageOut, charset );
    csvPrinter = new CSVPrinter( out );
    csvPrinter.setTokenDelimiter( ';' );
  }

  public void close() {
    if (csvPrinter != null) {
      try {
        csvPrinter.close();
      }
      catch (IOException e) {
        Logger.error( ErrorMessages.ERROR + ": Could not close CsvPrinter.", e );
      }
    }
  }

  public void append( String string ) throws IOException {
    writeLine( string );
  }

  public void append( List<String> stringList ) throws IOException {
    writeLine( stringList );
  }


  public void write( List<List<String>> csvStringList ) throws IOException{
    boolean isFirstLine = true;

    for(List<String> currentLine : csvStringList) {
      if (isFirstLine) {
        writeHeader(currentLine);
        isFirstLine = false;
      }
      else {
        writeLine( currentLine );
      }
    }
  }

  private void writeHeader(List<String> columnHeaders) throws IOException {
    csvPrinter.println( columnHeaders.toArray(new String[0]) );
  }


  private void writeLine( String valueLine ) throws IOException {
    csvPrinter.println( valueLine );
    if (progress != null) {
      progress.next();
    }
  }

  private void writeLine( List<String> valueArray ) throws IOException {
    csvPrinter.println( valueArray.toArray(new String[0]) );
    if (progress != null) {
      progress.next();
    }
  }
}
