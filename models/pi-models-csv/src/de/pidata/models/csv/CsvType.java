/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.csv;

import de.pidata.models.tree.DefaultModelFactory;
import de.pidata.models.tree.DynamicType;
import de.pidata.models.tree.ModelFactory;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.List;

public class CsvType extends DynamicType {

  /** the column names found by parsing the first line of the source */
  private List<QName> colNames;

  public CsvType( QName typeName, List<QName> colNames, boolean createRootRelation  ) {
    super( typeName, null, 0, null, false, false );
    this.colNames = colNames;

    Namespace ns = typeName.getNamespace();
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( ns );
    if (factory == null) {
      factory = new DefaultModelFactory( typeName.getNamespace(), typeName.getName() + ".xsd", "1.0" );
    }
    if (createRootRelation) {
      factory.addRootRelation( typeName, this, 0, Integer.MAX_VALUE, null );
    }

    initAttributes();
  }

  protected void initAttributes() {
    for (int i=0; i<colNames.size(); i++) {
      addAttributeType((QName) colNames.get(i), StringType.getDefString());
    }
  }

}
