/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.csv;

import de.pidata.log.Logger;
import de.pidata.models.tree.*;
import de.pidata.models.types.Type;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.progress.Progress;
import de.pidata.qnames.Key;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.NamespaceTable;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;
import de.pidata.system.base.ModelReader;
import de.pidata.system.base.Storage;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.SystemManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.*;

/**
 * Reading a source containing comma separated values (csv).
 * <p>
 * The first line may contain the column names. To use that column names as generic type attributes don't provide
 * a {@link CsvType} to the loadData() call.
 * <p>
 * The number of header lines also can be set after creating a new CsvReader.
 * <p>
 *
 * The column separator, string separator, string separator mask and line separator are set to
 * the default which typically will be used when exporting from Microsoft Excel to Csv. They can be set
 * separately to other values.
 */
public class CsvReader implements ModelReader {

  private static final boolean DEBUG = false;

  private static final int UTF_8_BOM = 0xFEFF;
  private final Progress progress;

  private InputStreamReader dataInput;
  private NamespaceTable namespaces;
  private int nextChar;

  /** the column names found by parsing the first line of the source */
  private final Map<Integer, QName> columnAttributes = new HashMap<Integer, QName>();

  private int headlines = 0;
  private int colSep = ',';
  private int stringSep = '"';
  private int stringSepMask = '"';
  private int lineSep = -1; // default: no special EOL character, user CR/LF instead
  private QName dateType = DateTimeType.TYPE_DATE;
  private DateFormat dateFormat = null;

  private CsvType csvRowType; // the type read from the CSV file
  private ComplexType childType; // the type to be used to add a row to the result container; should be a superset of csvRowType

  /**
   * Create csv data reader
   */
  public CsvReader( Progress progress ) {
    this.progress = progress;
  }

  /**
   * Create csv data reader with other than the default separators.
   * @param colSep column separator
   * @param stringSep string separator
   * @param stringSepMask character used to mask the string separator within strings
   * @param lineSep line separator
   */
  public CsvReader( Progress progress, int colSep, int stringSep, int stringSepMask, int lineSep ) {
    this.progress = progress;
    this.colSep = colSep;
    this.stringSep = stringSep;
    this.stringSepMask = stringSepMask;
    this.lineSep = lineSep;
  }

  public void setHeadlines( int headlines ) {
    this.headlines = headlines;
  }

  public void setColSep(int colSep) {
    this.colSep = colSep;
  }

  public void setStringSep(int stringSep) {
    this.stringSep = stringSep;
  }

  public void setStringSepMask(int stringSepMask) {
    this.stringSepMask = stringSepMask;
  }

  public void setLineSep(int lineSep) {
    this.lineSep = lineSep;
  }

  public void setDateFormat( QName dateType, DateFormat dateFormat ) {
    this.dateType = dateType;
    this.dateFormat = dateFormat;
  }

  public void setDateFormat( DateFormat dateFormat ) {
    this.dateFormat = dateFormat;
  }

  public Model loadData( InputStreamReader dataInput, NamespaceTable namespaces, Model container, QName childRelationName ) throws IOException {
    return loadData( dataInput, namespaces, container, null, childRelationName, null );
  }

  /**
   * Load data from any file containing CSV data. A dynamic Type will be created from the first line
   * within the file.
   *
   * @param dataInput  containing CSV data
   * @param namespaces
   * @return
   * @throws IOException
   */
  public Model loadData( InputStreamReader dataInput, NamespaceTable namespaces, QName rootRelation ) throws IOException {
    return loadData( dataInput, namespaces, null, rootRelation, null, null );
  }

  /**
   * Load data from an unknown csv source into the given parent model.
   *
   * @param dataInput
   * @param namespaces
   * @param container the container which will hold the data
   * @param rootRelation the root relation to be used to create a new container
   * @param childRelationName the relation name which will be used to add the data to the parent
   * @param csvType column header definition, if null first line of CSV file is expected to be column headers
   * @throws IOException
   */
  public Model loadData( InputStreamReader dataInput, NamespaceTable namespaces, Model container, QName rootRelation, QName childRelationName, CsvType csvType ) throws IOException {
    this.dataInput = dataInput;
    this.namespaces = namespaces;
    this.csvRowType = csvType;

    // first row might be read for generic child type mapping
    int lineNum = 0;

    nextChar = dataInput.read();
    while (nextChar == UTF_8_BOM) {
      nextChar = dataInput.read();
    }

    Model dataContainer = container;

    Namespace ns;
    if (childRelationName == null) {
      ns = namespaces.getDefaultNamespace();
      childRelationName = ns.getQName( "datarow" );
    }
    else {
      ns = childRelationName.getNamespace();
    }
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( ns );
    if (factory == null) {
      factory = new DynamicModelFactory( ns, rootRelation.getName() + ".xsd", "1.0" );
    }

    if (csvRowType == null) {
      // get CSV type from header, map to child type, nothing else
      List<QName> colNames = parseFirstLine( ns );
      csvRowType = new CsvType( childRelationName, colNames, false ); // TODO: create rootRelation when?
      lineNum = 1;
    }

    DefaultComplexType containerType;
    Relation childRelation = null;
    if (dataContainer != null) {
      containerType = ((DefaultComplexType) container.type());
      childRelation = containerType.getRelation( childRelationName );
    }
    else {
      if (rootRelation != null) {
        containerType = new DefaultComplexType( rootRelation, DefaultModel.class.getName(), 1 );
        containerType.addKeyAttributeType( 0, ns.getQName( "ID" ), QNameType.getQName() );
        dataContainer = new DefaultModel( rootRelation, containerType );
      }
      else {
        throw new IllegalArgumentException( "cannot create data container: no rootRelation given" );
      }
    }

    if (childRelation != null) {
      Type containerChildType = childRelation.getChildType();
      if (containerChildType != null) {
        childType = (ComplexType) containerChildType;
      }
      // TODO: Prüfen
//      if (!childType.equals( containerChildType )) {
//        throw new IllegalArgumentException( "dummy Root relation is already defined: name=" + childRelationName );
//      }
    }
    else {
      factory.addType( csvRowType );
      containerType.addRelation( childRelationName, csvRowType, 0, Integer.MAX_VALUE );
      lineNum = 1;

      childType = csvRowType;
    }

    buildTypeMap();

    // skip additional headlines
    while (nextChar > 0 && lineNum < headlines) {
      lineNum++;

      if (progress != null) {
        progress.setMessage( "Read line " + lineNum );
      }

      if (DEBUG) Logger.info( "skip headline nr. " + lineNum );
      while (nextChar > 0 && !reachedEOL( nextChar )) {
        nextChar = dataInput.read();
      }
      consumeEOL();
    }
    if (nextChar <= 0) {
      return dataContainer;
    }

    while (nextChar > 0) {
      lineNum++;

//      progress.setMessage( "Read line " + lineNum );

      if (DEBUG) Logger.info( "line nr. " + lineNum );
      dataContainer.add( childRelationName, parseLine( childType, factory ) );
    }

    return dataContainer;
  }

  private void buildTypeMap() {
    for (int i = 0; i < csvRowType.attributeCount(); i++) {
      QName csvColumnName = csvRowType.getAttributeName( i );
      int childAttributeIndex = childType.indexOfAttribute( csvColumnName );
      if (childAttributeIndex >= 0) {
        columnAttributes.put( Integer.valueOf( i ), csvColumnName );
      }
    }
  }

  /**
   * Get columns names from first line to get Attributes for the dynamic CSV row type.
   *
   * @param csvRowNamespace
   * @return
   * @throws IOException
   */
  private List<QName> parseFirstLine( Namespace csvRowNamespace ) throws IOException {
    List<QName> colNames = new ArrayList<>();

    StringBuffer str;
    while (nextChar > 0 && !reachedEOL( nextChar )) {
      if (nextChar == stringSep) {
        str = parseString();
      }
      else {
        str = parseDefault();
      }
      colNames.add( csvRowNamespace.getQName( str.toString() ) );
    }
    consumeEOL();

    return colNames;
  }

  /**
   * Build a model of the given type from one line of the source. For unknown source the factory is set to null
   * and a DefaultModel will be build.
   * @param type the type to use for building the model
   * @return
   * @throws IOException
   */
  private Model parseLine(ComplexType type, ModelFactory factory) throws IOException {
    int colNum = 0;
    StringBuffer str;
    Object[] attributes = new Object[type.attributeCount()];
    int      keyAttrCount = type.keyAttributeCount();
    boolean  simpleKey = false;
    Object[] keyAttrs = null;
    Key      key = null;

    if (keyAttrCount == 1) {
      simpleKey = true;
    }
    else if (keyAttrCount > 0) {
      keyAttrs = new Object[keyAttrCount];
    }
    while (nextChar > 0 && !reachedEOL( nextChar )) {
      if (nextChar == stringSep) {
        str = parseString();
      }
      else {
        str = parseDefault();
      }

      if (DEBUG) {
        if (colNum == 0) Logger.info("first field: " + str);
      }

      // get attribute for column; skip columns which are not found as attribute
      QName attrName = columnAttributes.get( Integer.valueOf( colNum ) );
      if (attrName != null) {
        int attrIndex = type.indexOfAttribute( attrName );
        SimpleType attrType = type.getAttributeType( attrIndex );
        int keyIndex = type.getKeyIndex( attrName );
        Object value = parseValue( str, attrType );

        //----- key attribute
        if (keyIndex >= 0) {
          attributes[attrIndex] = Key.KEY_ATTR;
          if (simpleKey) {
            if (attrType instanceof QNameType) {
              key = (QName) value;
            }
            else {
              key = new SimpleKey( value );
            }
          }
          else {
            keyAttrs[keyIndex] = value;
          }
        }
        //----- normal attribute
        else {
          attributes[attrIndex] = value;
        }
      }
      colNum++;
    }

    // last column might miss terminating colSep
    if (colNum < columnAttributes.size()) {
      for (int i = colNum; i < columnAttributes.size(); i++) {
        QName attrName = columnAttributes.get( Integer.valueOf( i ) );
        if (attrName != null) {
          int attrIndex = type.indexOfAttribute( attrName );
          SimpleType attrType = type.getAttributeType( attrIndex );
          Object value = attrType.createValue( "", this.namespaces );
          attributes[attrIndex] = value;
        }
      }
    }

    consumeEOL();

    return factory.createInstance(key, type, attributes, null, null);
  }

  protected Object parseValue( StringBuffer str, SimpleType attrType ) {
    if (attrType instanceof DateTimeType && dateFormat != null) {
      try {
        Date date = dateFormat.parse( str.toString() );
        return new DateObject( dateType, date.getTime() );
      }
      catch (ParseException e) {
        Logger.error( "Error parsing date=" + str + ": " + e.getMessage() );
        return null;
      }
    }
    else {
      return attrType.createValue( str.toString(), this.namespaces );
    }
  }

  private StringBuffer parseString() throws IOException {
    StringBuffer str = new StringBuffer();
    int c = nextChar;
    boolean strEnd = false;
    while (!strEnd) {
      c = dataInput.read();
      if (c == stringSepMask) {
        c = dataInput.read();
        if (c == stringSep) {
          str.append(stringSep);
        }
        else {
          strEnd = true;
        }
      }
      else {
        str.append((char)c);
      }
    }
    if (c > 0 && !reachedEOL( c )) {
      nextChar = dataInput.read();
    }
    else {
      nextChar = c;
    }
    return str;
  }

  private StringBuffer parseDefault() throws IOException {
    StringBuffer str = new StringBuffer();
    int c = nextChar;
    while ((c > 0) && (c != colSep) && !reachedEOL( c )) {
      str.append((char)c);
      c = dataInput.read();
    }
    if (c > 0 && !reachedEOL( c )) {
      nextChar = dataInput.read();
    }
    else {
      nextChar = c;
    }
    return str;
  }

  /**
   * Checks if EOL was hit either the configured lineSep or CR and/or LF.
   * @return true if EOL was hit and consumed
   */
  private boolean reachedEOL( int c ) {
    boolean isEOL = false;
    if (lineSep > 0 && c == lineSep) {
      isEOL = true;
    }
    else if (c == '\r') {
      isEOL = true;
    }
    else if (c == '\n') {
      isEOL = true;
    }
    return  isEOL;
  }

  /**
   * Reads the EOL characters, either the configured lineSep or CR and/or LF.
   * @throws IOException
   */
  private void consumeEOL() throws IOException {
    if (lineSep > 0 && nextChar == lineSep) {
      nextChar = dataInput.read();
    }
    else if (nextChar == '\r') {
      nextChar = dataInput.read();
      if (nextChar == '\n') {
        nextChar = dataInput.read();
      }
    }
    else if (nextChar == '\n') {
      nextChar = dataInput.read();
    }
  }

  /**
   * Special for use case: load any file once as generic type will be named according to path.
   * IMPORTANT: a rootRelation will be added so subsequent calls with the same path will lead to an internal error
   *
   * @param path           path pointing to a file containing CSV data
   * @param namespaces
   * @return
   * @throws IOException
   */
  public Model loadData( String path, NamespaceTable namespaces ) throws IOException {
    String dir = null;

    // index = path.lastIndexOf("/");
    int index = -1;
    int i = -1;
    do {
      i = path.indexOf( "/", i + 1 );
      if (i >= 0) index = i;
    } while (i >= 0);

    if (index >= 0) {
      dir = path.substring( 0, index );
      path = path.substring( index + 1 );
    }
    Storage storage = SystemManager.getInstance().getStorage( dir );
    InputStreamReader is = null;
    Model container;
    InputStream inputStream = storage.read( path );
    try {
      is = new InputStreamReader( inputStream );
      if (is == null) {
        throw new IOException( "Data resource not found, path=" + path );
      }

      container = loadData( is, namespaces, namespaces.getDefaultNamespace().getQName( path ) );
    }
    finally {
      StreamHelper.close( inputStream );
    }
    return container;
  }



}
