/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.csv;

import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.simple.*;
import de.pidata.progress.Progress;
import de.pidata.qnames.QName;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.Storage;
import org.csv4j.CSVPrinter;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;


public class CsvWriter {

  private Storage storage;
  private CSVPrinter csvPrinter;
  private Charset charset = UTF_8;

  private Progress progress;

  private ModelIterator<Model> lineIterator;
  private ComplexType lineType;

  public CsvWriter( Storage storage, Progress progress) {
    this.storage = storage;
    this.progress = progress;
  }

  public void setCharset( Charset charset ) {
    this.charset = charset;
  }

  public void write( String destination, Model model, QName relationID) throws IOException {
    if (relationID == null){
      throw new IllegalArgumentException( "RelationID must not be null" );
    }

    int tablerows = model.childCount( relationID );
    if (progress != null) {
      progress.createChild( tablerows );
    }

    this.lineIterator = model.iterator( relationID, null );
    write( destination );
  }

  public void write( String destination, ModelIterator<Model> modelIterator) throws IOException {
    if (modelIterator == null){
      throw new IllegalArgumentException( "ModelIterator must not be null" );
    }

    if (progress != null) {
      progress.createChild( 2 );
    }

    this.lineIterator = modelIterator;
    write( destination );
  }

  private void write( String destination ) throws IOException {
    OutputStream storageOut = null;
    try{
      storageOut = this.storage.write(destination, true, false);
      OutputStreamWriter out = new OutputStreamWriter( storageOut, charset );
      csvPrinter = new CSVPrinter( out );
      csvPrinter.setTokenDelimiter( ';' );
      writeCSV( lineIterator );
      storageOut.flush();
    }
    finally {
      StreamHelper.close(storageOut);
    }
  }

  public void init( String destination, ComplexType lineType ) throws IOException {
    OutputStream storageOut = this.storage.write( destination, true, false );
    OutputStreamWriter out = new OutputStreamWriter( storageOut );
    csvPrinter = new CSVPrinter( out );
    csvPrinter.setTokenDelimiter( ';' );

    this.lineType = lineType;
    writeHeader();
  }

  public void close() throws IOException {
    if (csvPrinter != null) {
      csvPrinter.close();
    }
  }

  public void append( Model model ) throws IOException {
    writeLine( model );
  }

  public void append( String string ) throws IOException {
    writeLine( string );
  }

  public void append( List<String> stringList ) throws IOException {
    writeLine( stringList );
  }

  private void writeCSV( ModelIterator lineIterator ) throws IOException{
    boolean isFirstLine = true;

    while (lineIterator.hasNext()) {
      Model modelLine = lineIterator.next();
      if (isFirstLine) {
        if (!(modelLine.type() instanceof ComplexType)) {
          throw new IllegalArgumentException( "Line type is not a ComplexType " );
        }
        lineType = (ComplexType) modelLine.type(); // TODO: tree might have multiple node types
        writeHeader();
        isFirstLine = false;
      }
      writeLine( modelLine );
    }
  }

  private void writeHeader() throws IOException {
    QName attrName;
    String[] columnHeaders;

    int fields = lineType.attributeCount();
    columnHeaders = new String[fields];
    for (int i = 0; i < fields; i++) {
      attrName = lineType.getAttributeName( i );
      columnHeaders[i] = attrName.getName();
    }
    csvPrinter.println( columnHeaders );
    if (progress != null) {
      progress.next();
    }
  }

  private void writeLine( Model modelLine ) throws IOException {
    SimpleType simpleType;
    QName attrName;
    String attributeValue = "";
    Object object;
    String[] valueLine;

    if (!lineType.equals( modelLine.type() )) {
      throw new IllegalArgumentException( "Line has type that differs from initially given ComplexType" );
    }

    if (modelLine.type() instanceof ComplexType) {
      int fields = lineType.attributeCount();
      valueLine = new String[fields];
      for (int i = 0; i < fields; i++) {
        simpleType = lineType.getAttributeType( i );
        attrName = lineType.getAttributeName( i );
        object = modelLine.get( attrName );
        if (object != null) {
          attributeValue = convertAttribute( object, simpleType );
          if (attributeValue != "null") {
            valueLine[i] = attributeValue;
          }
          else{
            valueLine[i] = "";
          }
        }
        else{
          valueLine[i] = "";
        }
      }
      csvPrinter.println( valueLine );
      if (progress != null) {
        progress.next();
      }
    }
  }

  private void writeLine( String valueLine ) throws IOException {
    csvPrinter.println( valueLine );
    if (progress != null) {
      progress.next();
    }
  }

  private void writeLine( List<String> valueArray ) throws IOException {
    csvPrinter.println( valueArray.toArray(new String[0]) );
    if (progress != null) {
      progress.next();
    }
  }

  private String convertAttribute(Object object, SimpleType simpleType) {
    if (simpleType == null) {
      throw new IllegalArgumentException("Arguments simpleType is wrong: simpleType == null");
    }

    if (object != null) {
      if (simpleType instanceof QNameType) {
        QName idValue = (QName) object;
        return idValue.getName();
      }
      else if (simpleType instanceof StringType) {
        return object.toString();
      }
      else if (simpleType instanceof DecimalType) {
        return object.toString();
      }
      else if (simpleType instanceof NumberType) {
        return String.valueOf(object);
      }
      else if (simpleType instanceof BooleanType) {
        return String.valueOf(object);
      }
      else if (simpleType instanceof DateTimeType) {
        QName type = ((DateTimeType) simpleType).getType();
        if (type == DateTimeType.TYPE_DATE) {
          return DateTimeType.toDateString((DateObject) object);
        }
        else if (type == DateTimeType.TYPE_DATETIME) {
          return DateTimeType.toDateTimeString((DateObject) object, true);
        }
        else if (type == DateTimeType.TYPE_TIME) {
          return DateTimeType.toTimeString((DateObject) object, true);
        }
      }
      else if (simpleType instanceof BinaryType) {
        return BinaryType.toBase64String( ((Binary) object).getBytes(), ((Binary) object).size() );
      }
      else {
        SimpleType baseType = (SimpleType) simpleType.getBaseType();
        if (baseType != null) {
          return convertAttribute(object, baseType);
        }
        else {
          throw new IllegalArgumentException("not implemented base type for attribute [" + simpleType.name() + "]");
        }
      }
    }
    return null;
  }

}
