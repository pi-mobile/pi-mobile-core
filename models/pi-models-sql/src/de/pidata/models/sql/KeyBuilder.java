/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.sql;

import de.pidata.models.types.SimpleType;
import de.pidata.qnames.Key;
import de.pidata.models.tree.Model;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.QName;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Stack;

public class KeyBuilder {

  private DBConnection dbConn;
  private Stack foreignKeyColumns = new Stack();
  private Stack foreignKeyValues = new Stack();

  public KeyBuilder( DBConnection dbConn ) {
    this.dbConn = dbConn;
  }

  /**
   * Add the model's keys and key values to the key stack.
   * @param model
   */
  public void pushKeys( Model model, String tableName ) throws IOException {
    ComplexType type = (ComplexType) model.type();
    int keyAttrCount = type.keyAttributeCount();
    Key key = model.key();
    for (int i = 0; i < keyAttrCount; i++) {
      QName attrName = type.getKeyAttribute(i);
      pushKeyColumn( tableName, attrName.getName(), type.getAttributeType(i), key.getKeyValue(i) );
    }
  }

  public void pushKeyColumn( String tableName, String attrName, SimpleType keyColType, Object keyColValue ) throws IOException {
    String colName = createParentKeyColName( attrName, tableName );
    foreignKeyColumns.push( colName );
    String value = dbConn.convertAttribute( keyColValue, keyColType );
    foreignKeyValues.push( value );
  }

  /**
   * Remove the model's keys and key values from the key stack.
   * @param model
   * @throws java.io.IOException
   */
  public void popKeys(Model model) {
    if (!foreignKeyColumns.isEmpty() && !foreignKeyValues.isEmpty()) {
      int count = ((ComplexType) model.type()).keyAttributeCount();
      for (int i = 0; i < count; i++) {
        foreignKeyColumns.pop();
        foreignKeyValues.pop();
      }
    }
  }

  /**
   * Adds comma separated list of key column names and values for current
   * stack position.
   * @param columnNames  buffer to add key column names
   * @param values               buffer to add key values
   */
  public void appendForeignKeys( StringBuffer columnNames, StringBuffer values ) {
    Enumeration enumeration = foreignKeyColumns.elements();
    while (enumeration.hasMoreElements()) {
      String column = (String) enumeration.nextElement();
      columnNames.append( column ).append(",");
    }
    enumeration = foreignKeyValues.elements();
    while (enumeration.hasMoreElements()) {
      String value = (String) enumeration.nextElement();
      values.append(value).append(",");
    }
  }

  /**
   * Appends key column names  and values to where clause
   * @param whereClause  buffer for where clause
   */
  private void appendForeignKeys( StringBuffer whereClause ) {
    if (foreignKeyColumns != null) {
      boolean first = true;
      Enumeration keyEnum   = foreignKeyColumns.elements();
      Enumeration valueEnum = foreignKeyValues.elements();
      while (keyEnum.hasMoreElements()) {
        String column = (String) keyEnum.nextElement();
        String value = (String) valueEnum.nextElement();
        if (first) {
          first = false;
        }
        else {
          whereClause.append(" AND ");
        }
        whereClause.append( column ).append("=").append(value);
      }
    }
  }

  /**
   * Columns named "ID or "name" are prepended by model's type name
   * @param colName   the colName to prepend if necessary
   * @param tableName name of the database table colName belongs to
   * @return the name to use for colName in children's primary key
   */
  public String createParentKeyColName( String colName, String tableName ) {
    if (colName.equals("ID") || colName.equals("name")) {
      return tableName + colName;
    }
    else {
      return colName;
    }
  }

  /**
   * Create a key condition for use in WHERE clause. The key is created from
   * given model's key and the parent model's key fields if needed
   * @param model  the model, if null only parent's key fields are added
   * @return the created condition for use in WHERE clause
   * @throws IOException
   */
  public StringBuffer createKeyCondition( Model model ) throws IOException {
    StringBuffer condition = new StringBuffer();
    appendForeignKeys( condition );
    if (model != null) {
      Key key = model.key();
      ComplexType modelType = (ComplexType) model.type();
      appendKey( condition, key, modelType );
    }
    return condition;
  }

  public void appendKey( StringBuffer condition, Key key, ComplexType modelType ) throws IOException {
    int keyAttrCount = key.keyValueCount();
    for (int i = 0; i < keyAttrCount; i++) {
      Object keyValue = key.getKeyValue(i);
      String colName = modelType.getKeyAttribute(i).getName();
      if (condition.length() > 0) condition.append(" AND ");
      String modelIDDB = dbConn.convertAttribute(keyValue, modelType.getKeyAttributeType(i) );
      condition.append(colName).append("=").append(modelIDDB).append("");
    }
  }

}
