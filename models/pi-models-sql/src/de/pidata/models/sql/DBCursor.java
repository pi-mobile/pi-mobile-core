/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.sql;

import de.pidata.models.types.SimpleType;
import de.pidata.qnames.QName;

import java.io.IOException;

public interface DBCursor {

  /**
   * Moves Cursor to the next data row
   * @return true if a next data row exists, otherwise false
   */
  public boolean next() throws IOException;

  /**
   * Returns string value for given columnName
   * @param columnName column*s name
   * @return String value
   */
  public String getString( String columnName ) throws IOException;

  /**
   * Returns long value for given columnName
   * @param columnName column*s name
   * @return long value
   */
  public long getLong( String columnName ) throws IOException;

  /**
   * Returns the value of the attribute identified by name. The
   * result data type is defined by type.
   *
   * @param name attribute's name
   * @param type the result type
   * @param defaultValue returned if attribute name is not defined,
   *                     most be valid for type.
   * @return the value of the attribute identified by name
   */
  public Object getAttributeValue( QName name, SimpleType type, Object defaultValue ) throws IOException;

  /**
   * Closes this DBCursor's resources
   */
  public void close();

}
