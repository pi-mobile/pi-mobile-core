/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.sql;

import de.pidata.log.Logger;
import de.pidata.system.base.NumberSequence;

import java.io.IOException;

/**
 * Simple NumberSequence implementation using a SQL database table for storing
 */
public class SQLTableSequence extends NumberSequence {

  private static final String SEQUENCE_TABLE = "_sequence_table";

  protected DBConnection conn;

  public SQLTableSequence( String name, DBConnection conn ) throws IOException {
    super( name, 0, 0, NumberSequence.KEY_TYPE_LONG );
    this.conn = conn;

    DBCursor cursor = null;
    try {
      if (!conn.existsTable( SEQUENCE_TABLE ) ) {
        createTable();
      }
      String colLast = getColNameLast();
      String colMin = getColNameMin();
      String colMax = getColNameMax();
      String colUpdate = getColNameUpdate();
      String sql = "SELECT "+colLast+","+colMin+","+colMax+","+colUpdate+" FROM "+SEQUENCE_TABLE+" WHERE name='"+name+"'";
      cursor = conn.query( sql );
      if (cursor.next()) {
        last = cursor.getLong( colLast );
        min = cursor.getLong( colMin );
        max = cursor.getLong( colMax );
        String updateStr = cursor.getString( colUpdate );
        updateRequested = "X".equals( updateStr );
        conn.finishSelect();
      }
      else {
        createSequence( name, 1, Long.MAX_VALUE );
      }
    }
    catch (IOException ex) {
      String msg = "Error initializing SQLTableSequence";
      Logger.error( msg, ex );
      throw new IOException(msg);
    }
    finally {
      if (cursor != null) cursor.close();
    }
  }

  public SQLTableSequence( String name, DBConnection conn, long initial, long max ) throws IOException {
    super(name, initial, max, NumberSequence.KEY_TYPE_LONG);
    this.conn = conn;
    if (!conn.existsTable( SEQUENCE_TABLE ) ) {
      createTable();
    }
    createSequence( name, initial, max );
  }

  private void createSequence( String name, long initial, long max ) throws IOException {
    try {
      String sql = "INSERT INTO "+SEQUENCE_TABLE+" (name,"+getColNameLast()+","+getColNameMin()+","+getColNameMax()+") "
                 + "VALUES('"+name+"',"+initial+","+initial+","+max+")";
      conn.execute( sql );
      conn.commit();
    }
    catch (Exception ex) {
      conn.rollback();
      Logger.error( "Could not insert row into table "+SEQUENCE_TABLE, ex );
    }
  }

  private void createTable() throws IOException {
    try {
      String sql = "CREATE TABLE "+SEQUENCE_TABLE+" ("
                 + "name VARCHAR(256) NOT NULL, "
                 + getColNameLast() + " INTEGER NOT NULL, "
                 + getColNameMin() + " INTEGER NOT NULL, "
                 + getColNameMax() + " INTEGER NOT NULL, "
                 + getColNameUpdate() + " VARCHAR(1), "
                 + "PRIMARY KEY (name) )";
      conn.execute( sql );
      conn.commit();
    }
    catch (Exception ex) {
      conn.rollback();
      Logger.error( "Could not create table "+SEQUENCE_TABLE, ex );
    }
  }

  private String getColNameLast() {
    return "last_value";
  }

  private String getColNameMax() {
    return "max_value";
  }

  private String getColNameMin() {
    return "min_value";
  }

  private String getColNameUpdate() {
    return "update_requested";
  }

  /**
   * Updates this sequence with a new min and max value and resets percent left to 100%
   *
   * @param min new min value
   * @param max new max value
   */
  public synchronized void update( long min, long max ) throws IOException {
    super.update( min, max );

    try {
      String updateStr;
      if (updateRequested) updateStr = "X";
      else updateStr = "-";
      String sql = "UPDATE "+SEQUENCE_TABLE+" SET "+getColNameMin()+"="+min+","+getColNameMax()+"="+max+","
                 +getColNameLast()+"="+last+","+getColNameUpdate()+"='"+updateStr+"' WHERE name='"+name+"'";;
      conn.execute( sql );
      conn.commit();
    }
    catch (IOException ex) {
      conn.rollback();
      String msg = "could not update sequence, name="+name;
      Logger.error( msg, ex );
      throw new IOException(msg);
    }
  }

  /**
   * Increments the sequence by count and returns the first new value
   *
   * @param count number of values needed
   * @return next sequence value or first value if count is greater than 1
   * @throws IllegalArgumentException if there is no next value in this sequence, i.e next is greater than max()
   */
  public synchronized long next( int count ) throws IOException {
    DBCursor cursor = null;
    long id = -1;
    try {
      String colLast = getColNameLast();
      String colMax = getColNameMax();
      String sql = "UPDATE "+SEQUENCE_TABLE+" SET "+colLast+"="+colLast+"+"+count;
      conn.execute( sql );

      sql = "SELECT "+colLast+","+colMax+" FROM "+SEQUENCE_TABLE+" WHERE name='"+name+"'";
      cursor = conn.query( sql );
      cursor.next();
      id = cursor.getLong( colLast );
      max = cursor.getLong( colMax );
      if (id >= max) {
        conn.rollback();
        throw new IllegalArgumentException( "Sequence '" + name + "' has not enough values for count="+count+", last="+last+", max=" + max );
      }
      cursor.close();   // Cursor must be closed before Commit - otherwise SQLite throws an exception!
      conn.commit();
    }
    catch (IOException ex) {
      if (cursor != null) cursor.close();
      conn.rollback();
      String msg = "readSequence could not create next value, name="+name;
      Logger.error(msg, ex);
      throw new IOException(msg);
    }
    last = id - count + 1;
    return last;
  }
}
