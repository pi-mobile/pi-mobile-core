/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.sql;

import de.pidata.log.Logger;
import de.pidata.models.tree.*;
import de.pidata.qnames.Key;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;

import java.io.IOException;

public class SQLReader {

  private static final boolean DEBUG = false;

  protected DBConnection dbConn;

  public SQLReader( DBConnection connection ) throws IOException {
    dbConn = connection;
  }

  /**
   * Loads parentModel's children by using the rules defined inside parentModel's type.
   *
   * @param keyBuilder class used to create SQL where clause
   * @param parentModel the model to which loaded children will be added
   */
  private void loadChildren( KeyBuilder keyBuilder, Model parentModel, boolean recursive)
  throws IOException {
    ComplexType nodeType = (ComplexType) parentModel.type();
    ComplexType childType;
    String sql;
    String table;
    QName relationID;

    StringBuffer condition = keyBuilder.createKeyCondition( null );

    QNameIterator iterator = nodeType.relationNames();
    while (iterator.hasNext()) {
      relationID = iterator.next();
      childType = (ComplexType) nodeType.getChildType(relationID);
      table = dbConn.getTableName( childType );

      sql = "SELECT * FROM " + table + " WHERE " + condition;

      fetchNodes( keyBuilder, parentModel, relationID, sql.toString(), recursive);
    }
  }

  /**
   * Read the children according to the given relation and add them to the parent model.
   * RelationID may be a root element; if so, the parent model must be a root model.
   * @param keyBuilder
   * @param parentModel
   * @param relationID
   * @param sql
   * @throws java.io.IOException
   */
  private int fetchNodes( KeyBuilder keyBuilder, Model parentModel, QName relationID, String sql, boolean recursive)
  throws IOException {
    ModelFactory modelFactory;
    Model model;
    DBCursor dbCursor = null;
    int depth = 0;
    int index;
    ChildList   childList = new ChildList();
    ComplexType type = (ComplexType) ((ComplexType) parentModel.type()).getChildType(relationID);

    modelFactory = ModelFactoryTable.getInstance().getFactory( type.name().getNamespace() );
    if (modelFactory == null) {
      Logger.warn("No factory found for type ID=" + type.name());
      return 0;
    }
//    QName idAttrName = modelFactory.getIdAttrName();

    if (DEBUG) Logger.debug(sql);
    try {
      if (DEBUG) Logger.debug("before executeQuery");
      dbCursor = dbConn.query( sql );
      if (DEBUG) Logger.debug("after executeQuery");
      index = 0;
      while (dbCursor.next()) {
        ComplexType nodeType = type;
        Object[] attribs = new Object[nodeType.attributeCount()];
        Key key = fetchAttributes(attribs, dbCursor, type);
        model = modelFactory.createInstance(key, type, attribs, null, null);;
        childList.add(relationID, model);
        if (DEBUG) {
          Logger.debug("fetched node depth="+depth+" index="+index);
          index++;
        }
      }
      if (DEBUG) Logger.debug("fetchNodes finished depth="+depth);
    }
    catch (Exception ex) {
      dbConn.rollback();
      throw new IOException("Could not fetch nodes, cause="+ex.getMessage() + ", sql="+sql);
    }
    finally {
      if (dbCursor != null) dbCursor.close();
    }

    model = childList.getFirstChild(null);
    while (model != null) {
      if (recursive) {
        //----- load model's children
        keyBuilder.pushKeys( model, dbConn.getTableName( (ComplexType) model.type() ) );
        loadChildren( keyBuilder, model, recursive );
        keyBuilder.popKeys( model );
      }
      Model next = model.nextSibling(null); // nextSibling is destroyed by parentModel.add()
      parentModel.add(relationID, model);
      model = next;
    }
    return childList.size();
  }

  public Key fetchAttributes(Object[] attributes, DBCursor dbCursor, ComplexType typeDef) throws IOException {
    SimpleType attributeType;
    QName   attributeID;
    int          keyAttrCount = typeDef.keyAttributeCount();
    Object[]     keyAttrs = null;
    Key          key = null;
    int          keyIndex;
    boolean      simpleKey = false;
    Object       value;

    if (keyAttrCount == 1) {
      simpleKey = true;
    }
    else if (keyAttrCount > 0) {
      keyAttrs = new Object[keyAttrCount];
    }

    for (int i = 0; i < attributes.length; i++) {
      attributeType = typeDef.getAttributeType( i );
      attributeID   = typeDef.getAttributeName( i );
      value = dbCursor.getAttributeValue( attributeID, attributeType, typeDef.getAttributeDefault( i ) );
      keyIndex = typeDef.getKeyIndex(attributeID);
      if (keyIndex >= 0) {
        attributes[i] = Key.KEY_ATTR;
        if (simpleKey) {
          if (attributeType instanceof QNameType) {
            key = (QName) value;
          }
          else {
            key = new SimpleKey( value );
          }
        }
        else {
          keyAttrs[keyIndex] = value;
        }
      }
      else {
        attributes[i] = value;
      }
    }
    if (keyAttrs != null) {
      key = new CombinedKey( keyAttrs );
    }
    return key;
  }

  public int loadData(Model parentModel, QName relationID)
  throws IOException {
    String sql = "SELECT * FROM " + relationID.getName();
    return loadData(sql, parentModel, relationID, Integer.MAX_VALUE, true);
  }

  public int loadData(String sql, Model parentModel, QName relationID)
  throws IOException {
    return loadData(sql, parentModel, relationID, Integer.MAX_VALUE, true);
  }

  public synchronized int loadData(String sql, Model parentModel, QName relationID, int maxCountResult)
  throws IOException {
    return loadData(sql, parentModel, relationID, maxCountResult, true);
  }

  /**
   * Loads data from dataResource.
   *
   * @param sql the sql statement
   * @param parentModel the parent model to which the new tree will be added
   * @param maxCountResult max number of records to load for the top level of the reuslt tree
   * @param recursive if true children of loaded models alre loeaded recursively
   * @return the number of records loaded for the top level of result tree or -1 if maxCountResult
   *         has been exceeded
   * @throws java.io.IOException
   */
  public synchronized int loadData(String sql, Model parentModel, QName relationID, int maxCountResult, boolean recursive)
  throws IOException {
    int count = 0;

    if (maxCountResult > 0 && maxCountResult < Integer.MAX_VALUE) {
      sql = dbConn.addLimit(sql, maxCountResult);
    }
    sql = dbConn.check(sql);
    count = fetchNodes( new KeyBuilder( dbConn ), parentModel, relationID, sql, recursive);
    dbConn.finishSelect();
    return count;
  }

  /**
   * Loads a single MOdel from dataResource.
   *
   * @param sql the sql statement
   * @param type the model's type
   * @return the Model loaded
   * @throws java.io.IOException
   */
  public synchronized Model loadModel(String sql, ComplexType type)
  throws IOException {
    int count = 0;
    sql = dbConn.addLimit(sql, 1);
    sql = dbConn.check(sql);
    Root root = new Root();
    count = fetchNodes( new KeyBuilder( dbConn ), root, type.name(), sql, true);
    dbConn.finishSelect();
    return root.get(null, null);
  }

  /**
   * Loads parentModel's children by using the rules defined inside parentModel's type.
   *
   * @param parentModel the model to which loaded children will be added
   */
  public synchronized void loadData(Model parentModel) throws IOException {
    loadChildren(null, parentModel, true);
    dbConn.finishSelect();
  }
}
