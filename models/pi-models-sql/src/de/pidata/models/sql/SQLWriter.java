/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.sql;

import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.tree.QNameIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.qnames.QName;
import de.pidata.system.base.Storage;
import de.pidata.stream.StreamHelper;

import java.io.IOException;
import java.io.InputStream;

public class SQLWriter {

  private static final boolean DEBUG = false;

  private DBConnection dbConn;
  private boolean working = false;

  public SQLWriter( DBConnection connection ) throws IOException {
    if (connection == null) {
      throw new IllegalArgumentException("JDBC connection must not be null!");
    }
    dbConn = connection;
  }

  public KeyBuilder createKeyBuilder() {
    return new KeyBuilder(dbConn);
  }

  public synchronized void write( Model model ) throws IOException {
    write(model, true);
  }

  /**
   * Removes all rows from binding relationName.getName() and then writes
   * all children of model to the database. Use DELETE CASCADE if children
   * of existing rows should also be deleted.
   * @param model      the parent model of the models to be written
   * @param relationName model's relation conatinig the models to be written
   * @throws IOException if DBConnection.supportsCascade()==false
   */
  public synchronized void replace(Model model, QName relationName ) throws IOException {
    if (!dbConn.supportsCascade()) {
      ComplexType modelType = (ComplexType) model.type();
      ComplexType childType = (ComplexType) modelType.getChildType( relationName );
      if (childType.relationCount() > 0) {
        throw new IOException( "SQLWrite.replace not working for DBConnection with supportsCascade()==false and child relation with children" );
      }
    }
    String sql = "DELETE FROM " + relationName.getName();
    try {
      if (DEBUG) Logger.debug("jdbc replace --> sql " + sql);
      dbConn.execute( sql );
    }
    catch (Exception e) {
      String msg = "Could not delete model sql="+sql;
      Logger.error(msg, e);
      dbConn.rollback();
      throw new IOException(msg);
    }
    write(model, false);
  }

  /**
   * Write a model to the database. if writeRoot is set to true then the model itself is stored. The key of the root
   * model is added to all children.
   * Id writeRoot is set to false, the model is not stored.
   * @param model
   * @param writeRoot
   * @throws IOException
   */
  public synchronized void write(Model model, boolean writeRoot) throws IOException{
    if (this.working) {
      throw new IllegalStateException("Must not write two Models at the same time!");
    }
    this.working = true;

    try {
      if (writeRoot) {
        writeRootDB(model);
      }
      else {
        writeChildrenDB( model, createKeyBuilder() );
      }
      try {
        if (DEBUG) Logger.debug("jdbc writeModel before commit");
        dbConn.commit();
        if (DEBUG) Logger.debug("jdbc writeModel commit successful");
      }
      catch (Exception e) {
        String msg = "Could not commit after write model";
        Logger.error(msg, e);
        dbConn.rollback();
        throw new IOException(msg);
      }
    }
    finally {
      this.working = false;
    }
  }

  /**
   * Update a model. No foreign keys are added.
   * @param model the model to update in the database
   * @return true if model already existed, false if the model was inserted into database
   * @throws IOException
   */
  public synchronized boolean update(Model model) throws IOException {
    String tableName = model.type().name().getName();
    return update( tableName, model );
  }

  /**
   * Update a model. No foreign keys are added.
   * @param tableName name of the table to update
   * @param model the model to update in the database
   * @return true if model already existed, false if the model was inserted into database
   * @throws IOException
   */
  public synchronized boolean update( String tableName, Model model ) throws IOException {
    return update( tableName, model, createKeyBuilder(), null );
  }

  /**
   * Updates only model's attributes. Model's children are left unchanged
   * in database.
   * If the key of the model's parent are used as foreign keys then the foreign key columns
   * and their values must be present here.
   * The foreign key values must be in the same order as the key columns. The format of the key values
   * must be already adjusted accorign to the column's type.
   * @param model the model whose attributes have changed
   * @param keyBuilder class beeing responsible for building the primary key
   * @return true if model already existed, false if the model was inserted into database
   */
  public synchronized boolean update( Model model, KeyBuilder keyBuilder ) throws IOException {
    String tableName = model.type().name().getName();
    return update( tableName, model, keyBuilder, null );
  }

  /**
   * Updates only model's attributes. Model's children are left unchanged
   * in database.
   * If the key of the model's parent are used as foreign keys then the foreign key columns
   * and their values must be present here.
   * The foreign key values must be in the same order as the key columns. The format of the key values
   * must be already adjusted accorign to the column's type.
   * @param tableName name of the table to update
   * @param model the model whose attributes have changed
   * @param keyBuilder class beeing responsible for building the primary key
   * @param attributes if not null only these attributes will be updated
   * @return true if model already existed, false if the model was inserted into database
   */
  public synchronized boolean update( String tableName, Model model, KeyBuilder keyBuilder, QName[] attributes ) throws IOException {
    StringBuffer sqlBuf = new StringBuffer();
    String       sql = null;
    DBCursor     dbCursor = null;
    boolean      exists = false;

    if (this.working) {
      throw new IllegalStateException("Must not write two Models at the same time!");
    }
    this.working = true;

    try {
      sqlBuf.append("SELECT * FROM ").append( tableName );
      StringBuffer condition = keyBuilder.createKeyCondition( model );
      if (condition.length() > 0) {
        sqlBuf.append( " WHERE " ).append( condition );
      }
      sql = sqlBuf.toString();
      dbCursor = dbConn.query( sql );
      exists = dbCursor.next();
      dbCursor.close();
      if (DEBUG) Logger.debug("jdbc updateModel: exists="+exists+", sql="+sql);

      if (exists) {
        sql = createUpdateStatement( model, tableName, keyBuilder, attributes );
      }
      else {
        sql = createInsertStatement( model, tableName, keyBuilder, attributes );
      }
      if (DEBUG) Logger.debug("jdbc updateModel --> sql " + sql);
      if (sql != null) {
        dbConn.execute( sql );
      }
      dbCursor.close();
      dbConn.commit();
    }
    catch (Exception e) {
      String msg = "Could not update model sql="+sql;
      Logger.error(msg, e);
      dbConn.rollback();
      throw new IOException(msg);
    }
    finally{
      if (dbCursor != null) dbCursor.close();
      this.working = false;
    }
    return exists;
  }

  public void executeSQLFile(Storage storage, String filename) throws IOException {
    if(!storage.exists(filename)) {
      Logger.info("file not exits [" + filename + "]");
      return;
    }
    StringBuffer strBuf = null;
    InputStream  in   = null;
    try {
      in = storage.read(filename);
      strBuf = new StringBuffer();
      int chr = ' ';
      boolean inStatement = false;
      do {
        if (!(chr == '\t' || chr == '\n' || chr == ' ' || chr == '#')) inStatement = true;
        if (chr == '#') {
          // fetch comment line
          while (chr > 0 && chr != '\n') {
            chr = in.read();
          }
        }
        if( inStatement) {
          if((char)chr == ';') {
            if (DEBUG) Logger.debug("executing SQL:\n" + strBuf);
            dbConn.execute( strBuf.toString() );
            strBuf.setLength(0);
            inStatement = false;
          }
          else {
            strBuf.append((char)chr);
          }
        }
        chr = in.read();
      } while(chr >= 0);
      dbConn.commit();
    }
    catch (Exception e) {
      String msg = "Error executing SQL from file:" + strBuf;
      Logger.error(msg, e);
      dbConn.rollback();
      throw new IOException(msg);
    }
    finally{
      StreamHelper.close( in );
    }
  }

  /**
   * Write a model and its descendants to the database.
   * @param model the base model
   */
  private void writeRootDB(Model model) throws IOException {

    if (!(model.type() instanceof ComplexType)) {
      throw new IllegalArgumentException("SQLWriter cannot write simple models!");
    }

    delete(model);

    // append the parent's keys and key values: model might be child within a data model tree  todo: funktioniert nur, solange das model 1. oder 2. Ebene des tree ist!
    KeyBuilder keyBuilder = createKeyBuilder();
    Model parent = model.getParent( false );
    if (parent != null) {
      keyBuilder.pushKeys( parent, dbConn.getTableName( (ComplexType) parent.type() ) );
    }
    writeDB( model, keyBuilder );
  }

  private void deleteChildren( Model model, KeyBuilder keyBuilder ) throws IOException {
    ComplexType complexType = (ComplexType) model.type();
    String sql = null;

    if (!dbConn.supportsCascade()) {
      keyBuilder.pushKeys( model, dbConn.getTableName( (ComplexType) model.type() ) );
      for (ModelIterator iter = model.iterator( null, null ); iter.hasNext(); ) {
        Model child = iter.next();
        deleteChildren( child, keyBuilder );
      }
      keyBuilder.popKeys( model );
    }

    // delete the child models
    try {
      for (QNameIterator iditer = complexType.relationNames(); iditer.hasNext();) {
        QName relationID = iditer.next();
        QName typeName = complexType.getChildType(relationID).name();

        StringBuffer deleteBuf = new StringBuffer( "DELETE FROM " ).append( typeName.getName() );
        StringBuffer condition = keyBuilder.createKeyCondition( null );
        if (condition.length() > 0) {
          deleteBuf.append( " WHERE " ).append( condition );
        }
        String deleteCmd = deleteBuf.toString();
        if (DEBUG) Logger.debug("jdbc deleteModel --> sql " + deleteCmd);
        dbConn.execute( deleteCmd );
      }
    }
    catch (Exception e) {
      String msg = "Could not delete model sql="+sql;
      Logger.error(msg, e);
      dbConn.rollback();
      throw new IOException(msg);
    }

  }

  /**
   * Write a model's children to the database.
   * The model itself is not written to the database. It just represents a container for the models
   * which should be written to the database.
   * @param model the container holding the models to be written to the database
   */
  private void writeChildrenDB( Model model, KeyBuilder keyBuilder ) throws IOException {
    ComplexType complexType = (ComplexType) model.type();
    QName relationID;
    Model childmodel;

    if (!(model.type() instanceof ComplexType)) {
      throw new IllegalArgumentException("SQLWriter cannot write simple models!");
    }

    deleteChildren( model, keyBuilder );

    // write the child models
    for (QNameIterator iditer = complexType.relationNames(); iditer.hasNext();) {
      relationID = iditer.next();

      for (ModelIterator miter = model.iterator(relationID, null); miter.hasNext();) {
        childmodel = miter.next();
        writeDB( childmodel, keyBuilder );
      }
    }
  }

  /**
   * Write a model and its descendants recursively to the database. The model's key values are appended while
   * descending the model tree.
   * @param model the model containing the data
   * @throws IOException
   */
  private void writeDB( Model model, KeyBuilder keyBuilder ) throws IOException {
    ComplexType complexType;
    QName relationID;
    Model childmodel;
    String sql = null;

    if (!(model.type() instanceof ComplexType)) {
      throw new IllegalArgumentException("SQLWriter cannot write simple models!");
    }
    complexType = (ComplexType) model.type();

    try {
      String tableName = model.type().name().getName();
//        sql = prepInsertStatement(model, id, tableName);
//        PreparedStatement stmt = prepStatement(sql);
//        if (DEBUG) Logger.debug("jdbc writeModel --> sql " + sql);
//        fillInsertStatement(stmt, model, id, tableName);
//        stmt.executeUpdate(sql);

      sql = createInsertStatement( model, tableName, keyBuilder, null );
      if (DEBUG) Logger.debug("jdbc writeModel --> sql " + sql);
      dbConn.execute( sql );
      if (DEBUG) Logger.debug("jdbc writeModel successful");
    }
    catch (Exception e) {
      String msg = "Could not insert model, sql="+sql;
      Logger.error(msg, e);
      dbConn.rollback();
      throw new IOException(msg);
    }

    // recursively write the models children
    keyBuilder.pushKeys( model, dbConn.getTableName( (ComplexType) model.type() ) );
    for (QNameIterator iditer = complexType.relationNames(); iditer.hasNext();) {
      relationID = iditer.next();

      for (ModelIterator miter = model.iterator(relationID, null); miter.hasNext();) {
        childmodel = miter.next();
        writeDB( childmodel, keyBuilder );
      }
    }
    keyBuilder.popKeys( model );
  }

  /**
   * Delete the given model form relation model.tyxpe().name().
   * For recursive deletion use FOREIGN KEY with DELETE CASCADE
   * when creating tables.
   * If DBConnection.supportsCascade() is false the cascade is
   * done by recursive deleting all children (bottom up).
   * @param model        the model to be deleted
   * @throws IOException
   */
  public void delete( Model model ) throws IOException {
    String tableName = dbConn.getTableName( (ComplexType) model.type() );
    delete( tableName, model );    
  }

  /**
   * Delete the given model form relation model.tyxpe().name().
   * For recursive deletion use FOREIGN KEY with DELETE CASCADE
   * when creating tables.
   * If DBConnection.supportsCascade() is false the cascade is
   * done by recursive deleting all children (bottom up).
   * @param model        the model to be deleted
   * @throws IOException
   */
  public void delete( String tableName, Model model ) throws IOException {
    String sql = null;
    KeyBuilder keyBuilder = createKeyBuilder();
    if (!dbConn.supportsCascade()) {
      keyBuilder.pushKeys( model, dbConn.getTableName( (ComplexType) model.type() ) );
      deleteChildren( model, keyBuilder );
      keyBuilder.popKeys( model );
    }
    try {
      StringBuffer deleteBuf = new StringBuffer( "DELETE FROM " ).append( tableName );
      StringBuffer condition = keyBuilder.createKeyCondition( model );
      if (condition.length() > 0) {
        deleteBuf.append( " WHERE " ).append( condition );
      }
      sql = deleteBuf.toString();
      if (DEBUG) Logger.debug("jdbc writeModel --> sql " + sql);
      dbConn.execute( sql );
      dbConn.commit();
    }
    catch (Exception e) {
      String msg = "Could not delete model sql="+sql;
      Logger.error(msg, e);
      dbConn.rollback();
      throw new IOException(msg);
    }
  }

  private String createInsertStatement( Model model, String tableName, KeyBuilder keyBuilder, QName[] attributes ) throws IOException{
    StringBuffer insert = new StringBuffer();
    StringBuffer columns = new StringBuffer();
    StringBuffer values = new StringBuffer();
    SimpleType simpleType;
    QName attributeID;
    Object object;
    ComplexType complexType = (ComplexType) model.type();
    keyBuilder.appendForeignKeys( columns, values );
    if (attributes == null) {
      for (int i = 0; i < complexType.attributeCount(); i++) {
        simpleType = complexType.getAttributeType(i);
        attributeID = complexType.getAttributeName(i);
        object = model.get(attributeID);
        if (object != null) {
          columns.append(attributeID.getName()).append(",");
          String str = dbConn.convertAttribute(object, simpleType );
          values.append(str).append( "," );
        }
      }
    }
    else {
      for (int nr = 0; nr < attributes.length; nr++) {
        int i = complexType.indexOfAttribute( attributes[nr] );
        simpleType = complexType.getAttributeType(i);
        attributeID = complexType.getAttributeName(i);
        object = model.get(attributeID);
        if (object != null) {
          columns.append(attributeID.getName()).append(",");
          String str = dbConn.convertAttribute(object, simpleType );
          values.append(str).append( "," );
        }
      }
    }
    columns.setLength(columns.length() - 1);
    values.setLength(values.length() - 1);
    insert.append("INSERT INTO ").append(tableName).append(" (").append(columns.toString());
    insert.append(") Values (").append(values.toString()).append(")");
    return insert.toString();
  }

  /**
   * Create update statement for the given model
   * @param model     the model to be updated in database
   * @param tableName the datebase binding for which the update statement is created
   * @return the update statement or null if nothing has to be updated
   */
  public String createUpdateStatement( Model model, String tableName, KeyBuilder keyBuilder, QName[] attributes ) throws IOException{
    StringBuffer update = new StringBuffer();
    StringBuffer condition = new StringBuffer();
    StringBuffer sets = new StringBuffer();
    SimpleType simpleType;
    QName attributeID;
    Object object;
    ComplexType complexType = (ComplexType) model.type();

    condition.append( keyBuilder.createKeyCondition( model ) );

    if (attributes == null) {
      for (int i = 0; i < complexType.attributeCount(); i++) {
        simpleType = complexType.getAttributeType(i);
        attributeID = complexType.getAttributeName(i);
        if (!attributeID.getName().equals("ID")) {
          object = model.get(attributeID);
          sets.append(attributeID.getName()).append("=");
          if (object == null) {
            sets.append("NULL,");
          }
          else {
            sets.append( dbConn.convertAttribute( object, simpleType ) ).append(",");
          }
        }
      }
    }
    else {
      for (int nr = 0; nr < attributes.length; nr++) {
        int i = complexType.indexOfAttribute( attributes[nr] );
        simpleType = complexType.getAttributeType(i);
        attributeID = complexType.getAttributeName(i);
        if (!attributeID.getName().equals("ID")) {
          object = model.get(attributeID);
          sets.append(attributeID.getName()).append("=");
          if (object == null) {
            sets.append("NULL,");
          }
          else {
            sets.append( dbConn.convertAttribute( object, simpleType ) ).append(",");
          }
        }
      }
    }
    if (sets.length() > 1) {
      sets.setLength(sets.length() - 1); // remove last comma
      update.append( "UPDATE " ).append( tableName ).append( " SET " ).append( sets );
      if (condition.length() > 0) {
        update.append( " WHERE " ).append( condition );
      }
      return update.toString();
    }
    else return null;
  }

  /**
   * resets the
   * involved db objects
   */
  private synchronized void resetDB() {
  }

}
