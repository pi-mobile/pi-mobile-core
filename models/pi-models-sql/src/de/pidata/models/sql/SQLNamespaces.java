/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.pidata.models.sql;

import de.pidata.log.Logger;
import de.pidata.models.tree.ModelFactory;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;

import java.io.IOException;
import java.util.Hashtable;

public class SQLNamespaces extends NamespaceTable {

  private DBConnection dbConn;
  private Hashtable  schemaversionTable;

  public SQLNamespaces( DBConnection conn, NamespaceTable parentNsTable ) throws IOException {
    super( parentNsTable );
    if (conn == null) {
      throw new IllegalArgumentException("JDBC connection must not be null!");
    }
    dbConn = conn;
    schemaversionTable = new Hashtable();
    if (!conn.existsTable( "namespace" ) ) {
      createTable();
    }
    readFromDB();
  }

  private void createTable() throws IOException {
    try {
      dbConn.execute( "CREATE TABLE namespace ("
                    + "  id   VARCHAR(3),"
                    + "  name VARCHAR(256),"
                    + "  schemaversion  VARCHAR(20),"
                    + "  PRIMARY KEY (id) )" );
      dbConn.commit();
    }
    catch (Exception ex) {
      dbConn.rollback();
      Logger.error( "Could not create table namespace", ex );
    }
  }

  /**
   * Read namespaces from database
   */
  public void readFromDB()  {
    DBCursor dbCursor = null;
    removeAll();
    try {
      dbCursor = dbConn.query("SELECT * FROM namespace");
      String id;
      String name;
      while (dbCursor.next()){
        id = dbCursor.getString("id");
        if (id == null || id.equals(" ")) id = "";
        name = dbCursor.getString("name");
        Namespace namespace = Namespace.getInstance(name);
        super.addNamespace( namespace, id );
        String schemaversion = dbCursor.getString("schemaversion");
        if ((schemaversion != null) && (schemaversion.length() > 0) && (!schemaversion.equals( "null" ))) {
          schemaversionTable.put( namespace, schemaversion );
        }
      }
    }
    catch (Exception ex) {
      Logger.error("Could not read database namespaces", ex);
      throw new IllegalArgumentException("Could not read database namespaces, message="+ex.getMessage());
    }
    finally {
      dbCursor.close();
    }
  }

  private String addToDB( Namespace namespace, String prefix ) {
    try {
      if (prefix.length() <= 0) prefix = " ";
      String schemaversion = null;
      ModelFactory factory = ModelFactoryTable.getInstance().getFactory( namespace );
      if (factory != null) {
        schemaversion = factory.getVersion();
      }
      if (schemaversion == null) {
        schemaversion = "-";
      }
      dbConn.execute( "INSERT INTO namespace (id,name,schemaversion) Values ('" + prefix + "','" + namespace.getUri() + "','"+schemaversion+"')" );
      dbConn.commit();
    }
    catch (Exception e) {
      Logger.error( "Could not add namespace prefix", e );
    }
    return prefix;
  }

  /**
   * Returns the schemaversion stored in database's Namespace table
   * @param namespace the namespace to return schemaversion for
   * @return namespace's schemaversion or null if not defined or namespace does not exist in database
   */
  public String getSchemaVersion( Namespace namespace ) {
    return (String) schemaversionTable.get( namespace );
  }

  public void setSchemaVersion( Namespace namespace, String version ) {
    String sql = "UPDATE namespace SET schemaversion='"+version+"' WHERE name='"+namespace.getUri()+"'";
    try {
      dbConn.execute( sql );
      dbConn.commit();
    }
    catch (Exception e) {
      Logger.error("Could not update schema version, sql="+sql, e);
    }
  }

  /**
   * Converts idValue to the value used for that id in this SQLNamespaces's database.
   * If idValues's namespace is not the database's default namespace the namespace
   * prefix followed by ':' is prepended to idValue.getName().
   * @param idValue the value to be converted
   * @return the converted value
   */
  public String convert(QName idValue) throws IOException{
    if (idValue == null) return null;
    Namespace namespace = idValue.getNamespace();
    String prefix = getPrefix(namespace);
    if (prefix == null) {
      prefix = createNewPrefix( namespace, false );
    }
    if (prefix.length() > 0) {
      return prefix + ":" + idValue.getName();
    }
    else {
      return idValue.getName();
    }
  }

  public QName getQName(String fullQName) throws IOException {
    if (fullQName == null) return null;
    String prefix, nameStr;
    prefix = SQLNamespaces.getPrefix(fullQName);
    nameStr = SQLNamespaces.getName(fullQName);
    Namespace namespaceDB = getByPrefix( prefix );
    if (namespaceDB == null) {
      throw new IOException("Namespace is not in DB. Prefix: " + prefix);
    }
    return namespaceDB.getQName(nameStr);
  }

  /**
   * Returns only the Namepart from QName-String
   * @param dbQName QName-String from DB
   * @return Namepart from QName-String
   */
  public static String getName(String dbQName) {
    String nameStr;
    int index = dbQName.indexOf(':');
    if (index >= 0) {
      //----- Remove namespace prefix if exists
      nameStr = dbQName.substring(index + 1);
    }
    else {
      nameStr = dbQName;
    }
    return nameStr;
  }

  /**
   * Returns only the Prefix from QName-String
   * @param dbQName QName-String from DB
   * @return Prefix from QName-String
   */
  public static String getPrefix(String dbQName) {
    String prefix;
    int index = dbQName.indexOf(':');
    if (index >= 0) {
      prefix = dbQName.substring(0, index);
    }
    else {
      prefix = "";
    }
    return prefix;
  }

  public void addNamespace( Namespace namespace, String prefix ) {
    super.addNamespace(namespace,prefix);
    addToDB( namespace, prefix );
  }
}
