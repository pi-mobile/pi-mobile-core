/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.sql;

import de.pidata.log.Logger;
import de.pidata.models.tree.Context;
import de.pidata.qnames.Key;
import de.pidata.models.tree.Model;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.simple.BooleanType;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.models.types.simple.DecimalType;
import de.pidata.models.types.simple.NumberType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;
import de.pidata.system.base.NumberSequence;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.SystemManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public abstract class DBConnection {

  public static String driverClassName;
  public int MAX_STRLEN;

  protected long reconnectMillis;
  protected long lastAccessTime;
  private SQLNamespaces sqlNamespaces;
  protected HashMap<ComplexType,String> typeTableMap = new HashMap<ComplexType,String>();

  public DBConnection( int reconnectMinutes ) {
    if (reconnectMinutes <= 0) reconnectMillis = -1;
    else reconnectMillis = reconnectMinutes * 60000;
  }

  public void addTypeMap( ComplexType type, String tableName ) {
    typeTableMap.put( type, tableName );
  }

  public String getTableName( ComplexType type ) {
    String tableName = typeTableMap.get( type );
    if (tableName == null) {
      tableName = type.name().getName();
    }
    return tableName;
  }

  /**
   *
   * @param propertySection the propertySection to use for readign properties, e.g. "database"
   * @return
   * @throws Exception
   */
  public static DBConnection createConnection( Context context, String propertySection ) throws Exception {
    if (propertySection == null) propertySection = "database";
    SystemManager sysMan = SystemManager.getInstance();
    String connClassName = sysMan.getProperty( propertySection+".connectionClass", null );
    if (connClassName == null) {
      String db_driver = sysMan.getProperty(propertySection+".drivername", null );
      if (db_driver == null) throw new IllegalArgumentException("database.drivername missing in both property files");
      connClassName = getConnectionClass( db_driver );
    }
    DBConnection conn = (DBConnection) Class.forName( connClassName ).newInstance();
    conn.connect( context, propertySection );
    return conn;
  }

  public static String getConnectionClass( String driverClassName ) {
    DBConnection.driverClassName = driverClassName;
    if (driverClassName.equals("oracle.jdbc.OracleDriver")) {
      return "de.pidata.models.jdbc.OracleConnection";
    }
    else if (driverClassName.equals("oracle.lite.poljdbc.POLJDBCDriver")) {
      return "de.pidata.models.jdbc.OracleLiteConnection";
    }
    else if (driverClassName.equals("de.slab.dtfsql.type4.Driver")) {
      return "de.pidata.models.jdbc.DtfConnection";
    }
    else if (driverClassName.toUpperCase().indexOf("ACCESS") >= 0) {
      return "de.pidata.models.jdbc.OdbcConnection";
    }
    else if (driverClassName.equals("com.ibm.db2.jcc.DB2Driver")) {
      return "de.pidata.models.jdbc.Db2eConnection";
    }
    else if (driverClassName.equals("org.postgresql.Driver")) {
      return "de.pidata.models.jdbc.PostreSQLConnection";
    }
    else if (driverClassName.equals("com.mysql.jdbc.Driver") || driverClassName.contains( "sqlserver" )) { // TODO: temp hack?
      return "de.pidata.models.jdbc.MySQLConnection";
    }
    else if (driverClassName.startsWith("org.apache.derby.jdbc")) {
      return "de.pidata.models.jdbc.JavaDBConnection";
    }
    else if (driverClassName.equals("org.sqlite.Driver") || driverClassName.equals( "org.sqlite.JDBC" )) {
      return "de.pidata.models.jdbc.SQLiteConnection";
    }
    throw new RuntimeException("Don't know which DBConnection class to use for driver class=" + driverClassName );
  }

  /**
   *
   *
   *
   * @param context
   * @param propertySection the propertySection to use for readign properties, e.g. "database"
   * @throws Exception
   */
  public abstract void connect( Context context, String propertySection ) throws Exception;

  /**
   * Closes current jdbc connection and tries to reconnect with same connection parameters
   * @return true if reconnect was successful
   */
  public abstract boolean reconnect();

  /**
   * Executes the commit on the jdbc connection.
   * @throws java.io.IOException
   */
  public abstract void commit() throws IOException;

  /**
   * Executes the rollback on the jdbc connection and catches possible SQLException in
   * empty catch block.
   */
  public abstract void rollback();

  public abstract void close();

  public SQLNamespaces getNamespaces() throws IOException {
    if (sqlNamespaces == null) {
      this.sqlNamespaces = new SQLNamespaces( this, null );
    }
    return this.sqlNamespaces;
  }

  public abstract DBCursor query( String sql ) throws IOException;

  /**
   * @deprecated  use KeyBuilder
   */
  @Deprecated
  public final String XcreateParentKeyColName(String colName, ComplexType modelType) {
    throw new RuntimeException( "Deprecated - use KeyBuilder" );
  }

  /**
   * @deprecated  use KeyBuilder
   */
  @Deprecated
  public final StringBuffer XcreateKeyCondition( Model model, boolean parentKey ) throws IOException {
    throw new RuntimeException( "Deprecated - use KeyBuilder" );
  }

  /**
   * @deprecated  use KeyBuilder
   */
  @Deprecated
  public final StringBuffer XcreateKeyCondition( Key key, ComplexType modelType, boolean parentKey ) throws IOException {
    throw new RuntimeException( "Deprecated - use KeyBuilder" );
  }

  /**
   * Return the string value for the given object by using his simpleType
   *
   * @param object the object to convert
   * @param simpleType the type of the object
   * @return String for SQL-Statment
   */
  public String convertAttribute( Object object, SimpleType simpleType )
  throws IOException {
    String idDB;
    if (simpleType == null) {
      throw new IllegalArgumentException("Arguments simpleType is wrong: simpleType == null");
    }
    if (object != null) {
      if (simpleType instanceof QNameType) {
        idDB = getNamespaces().convert((QName) object);
        return "'" + idDB + "'";
      }
      else if (simpleType instanceof StringType) {
        String str = (String) object;
        // replace all occurences of ' by two '
        if (str.indexOf('\'') >= 0) {
          StringBuffer buf = new StringBuffer(str);
          for (int i = buf.length() - 1; i >= 0; i--) {
            if (buf.charAt(i) == '\'') {
              buf.insert(i, '\'');
            }
          }
          str = buf.toString();
        }
        return "'" + str + "'";
      }
      /*
      else if (simpleType instanceof NumberType) {
        return object.toString();
      }
      */
      else if (simpleType instanceof NumberType) {
        if (simpleType instanceof DecimalType) {
          return object.toString();
        }
        else {
          return String.valueOf(object);
        }
      }
      else if (simpleType instanceof BooleanType) {
        return String.valueOf(object);
      }
      else if (simpleType instanceof DateTimeType) {
        return dateTime2SQL( (DateTimeType) simpleType, (DateObject) object );
      }
      else {
        SimpleType base = (SimpleType) simpleType.getBaseType();
        if (base != null) {
          return convertAttribute( object, base );
        }
        else {
          throw new IllegalArgumentException("Not implemented base type: " + simpleType.name());
        }
      }
    }
    return null;
  }

  /**
   * Convert the internal representation of a date/time value into the database specific
   * representaion. If using a String it must be surrounded by single quotes.
   *
   * @param dateTimeType
   * @param value   the date value
   * @return the database specific representation of the date/time value
   */
  public String dateTime2SQL( DateTimeType dateTimeType, DateObject value ) {
    String result = "";
    QName type = dateTimeType.getType();
    if (type == DateTimeType.TYPE_DATE) {
      result =  "'" + DateTimeType.toDateString( value) + "'";
    }
    else if (type == DateTimeType.TYPE_DATETIME) {
      result = "'" + DateTimeType.toDateTimeString( value, true) + "'";
    }
    else if (type == DateTimeType.TYPE_TIME) {
      result = "'" + DateTimeType.toTimeString( value, true) + "'";
    }
    return result;
  }

  /**
   * Returns the database specific type for an internal date/time type
   *
   * @param dateTimeType
   * @return the database specific type
   */
  public abstract String getDateTimeType( DateTimeType dateTimeType );

  /**
   * Creates a new Sequence
   * @param name the sequence's name
   * @param min  the min (starting) value for the new sequence
   * @param max  the max value for the new sequence
   * @return the new sequence object
   */
  public abstract NumberSequence createSequence( String name, long min, long max ) throws IOException;

  /**
   * Creates an sequence object for a sequence existing in database
   *
   *
   * @param name the sequence's name
   * @return the new sequence object
   */
  public abstract NumberSequence getSequence( String name ) throws IOException;

  /**
   * Add a limit constraint to the sql string.
   * @param sql
   * @param maxCountResult
   */
  public abstract String addLimit(String sql, int maxCountResult);

  /**
   * Check and modify sql statement according to database specific needs. The default
   * syntax corresponds to Oracle Lite, so there is less need to convert strings
   * in the limited environment of a mobile device!
   *
   * - replace function upper() with toupper() in dtF
   * - replace function lower() with tolower() in dtF
   *
   * @param sql
   * @return adjusted sql statement
   */
  public String check(String sql) {
    return sql;
  }

  /**
   * Called after a single or a sequence of SELECT statements. Some databases
   * (e.g. Oracle Lite) are creating locks while reading, so we have to issue a
   * commit after reading data !?
   */
  public void finishSelect() {
    // by default do nothing
  }

  /**
   * Creatres a date expression - Reason: on some drivers you cannot compare a date with
   * a timestamp!
   * @return
   */
  public String dateExpression( String columnName, String operation, DateObject date  ) {
    return columnName + " " + operation + " '" + DateTimeType.toDateString(date) + "'";
  }

  /**
   * Creatres a date expression - Reason: on some drivers you cannot compare a date with
   * a timestamp!
   * @return
   */
  public String dateTimeExpression( String columnName, String operation, DateObject date  ) {
    return columnName + " " + operation + " '" + DateTimeType.toDateTimeString(date) + "'";
  }

  public abstract void execute( String sql ) throws IOException;

  /**
   * Reads and executes patchScript. Autocommit is set to false and commit is executed after having
   * executed last sql statement. Parser expects each sql statement to be ended with ';'. If an
   * exception occurs rollback is called on database connection and IOException is thrown.
   *
   * @param patchScript  the stream to read patch script from
   * @throws IOException if script execution or reading from stream fails
   */
  public void patch( InputStream patchScript ) throws IOException {
    String sql;
    try {
      do {
        sql = StreamHelper.readUntil( patchScript, ';' );
        if (sql != null) {
          sql = sql.trim();
          if (sql.length() > 0) {
            sql = check(sql);
            Logger.info( "starting patch sql="+sql+";" );
            execute( sql );
          }
        }
      } while (sql != null);
      commit();
      Logger.info( "patch finished succesful" );
    }
    catch (Exception e) {
      rollback();
      String msg = "patch failed";
      Logger.error( msg, e );
      throw new IOException( msg+", ex="+e.getMessage() );
    }
  }

  /**
   * Returns true if table named tableName exists
   * @param tableName teh tabel to check
   * @return true if tableName exists
   */
  public abstract boolean existsTable( String tableName ) throws IOException;

  /**
   * Returns true if this DBConnection supports cascade definition for delete and update
   * like definded by CREATE TABLE ... ON ... CASCADE
   *
   * @return true if this DBConnection supports cascade definition
   */
  public boolean supportsCascade() {
    return true;
  }

  public void addColumn( String table, String name, String type, boolean nullable ) throws IOException {
    String sql = "ALTER TABLE "+table+" ADD "+name+" "+type;
    if (!nullable) sql = sql + " NOT NULL";
    execute( sql );
  }

  public void updateColumn( String table, String name, String type, boolean nullable ) throws IOException {
    String sql = "ALTER TABLE "+table+" UPDATE "+name+" "+type;
    if (!nullable) sql = sql + " NOT NULL";
    execute( sql );
  }
}
