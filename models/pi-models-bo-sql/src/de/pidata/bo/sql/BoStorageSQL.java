package de.pidata.bo.sql;

import de.pidata.bo.base.BoStorage;
import de.pidata.bo.base.Businessobject;
import de.pidata.log.Logger;
import de.pidata.models.config.ConfigFactory;
import de.pidata.models.config.Configurable;
import de.pidata.models.config.Configurator;
import de.pidata.models.config.Instance;
import de.pidata.models.config.Parameter;
import de.pidata.models.sql.DBConnection;
import de.pidata.models.sql.DBCursor;
import de.pidata.models.sql.KeyBuilder;
import de.pidata.models.sql.SQLNamespaces;
import de.pidata.models.tree.*;
import de.pidata.qnames.Key;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.NamespaceTable;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;
import de.pidata.system.base.NumberSequence;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;

public class BoStorageSQL implements BoStorage, Configurable {

  private static final boolean DEBUG = false;
  public static final QName DEFAULT_NAMESPACE = ConfigFactory.NAMESPACE.getQName( "defaultNamespace" );

  protected DBConnection conn;
  protected SQLNamespaces sqlNamespaces;

  protected Hashtable dbTables = new Hashtable(); // type name -> tabele name

  public BoStorageSQL() {
  }

  /**
   * Called by Configurator to initialize this object.
   *
   * @param configurator the calling configurator
   * @param instanceDef  the instance config entry for this object
   */
  public void init( Configurator configurator, Instance instanceDef ) throws Exception {
    Namespace defaultNamespace;
    Parameter defNS = instanceDef.getParameter( DEFAULT_NAMESPACE );
    if (defNS == null) {
      defaultNamespace = Businessobject.NAMESPACE;
    }
    else {
      defaultNamespace = Namespace.getInstance( defNS.getValue() );
    }
    init( DBConnection.createConnection( configurator.getContext(), "database" ), defaultNamespace );
  }

  public void init( DBConnection conn, Namespace defaultNamespace ) throws IOException {
    this.conn = conn;
    sqlNamespaces = new SQLNamespaces( conn, null );
    if (sqlNamespaces.size() == 0) {
      // Database is empty
      sqlNamespaces.addNamespace( defaultNamespace, "" );
      createTables();
    }
    Businessobject.initStorage( this );
  }

  public DBConnection getConnection() {
    return conn;
  }

  protected void createTables() throws IOException {
    Logger.info( "--- Creating database tables ---" );
    Storage patchDir = SystemManager.getInstance().getStorage( "sql" );
    runScript( patchDir, "schema.ddl" );
    runScript( patchDir, "schema_client.ddl" );
    runScript( patchDir, "initial.sql" );
    Logger.info( "--- database tables created ---" );
  }

  private void runScript( Storage patchDir, String filename ) throws IOException {
    runScript( conn, patchDir, filename );
  }

  public static void runScript( DBConnection conn, Storage patchDir, String filename ) throws IOException {
    if (patchDir.exists( filename )) {
      InputStream patchStream = patchDir.read( filename );
      conn.patch( patchStream );
    }
  }

  public NamespaceTable getNamespaces() {
    return sqlNamespaces;
  }

  protected String getTableName( QName boName ) {
    String tableName = (String) dbTables.get( boName );
    if (tableName == null) tableName = boName.getName().toLowerCase();
    return tableName;
  }

  protected String getTableName( QName boName, QName relName ) {
    String tableID = boName.toString()+"|"+relName.toString();
    String tableName = (String) dbTables.get( tableID );
    if (tableName == null) tableName = boName.getName().toLowerCase() + "_" + relName.getName().toLowerCase();
    return tableName;
  }

  protected String createKey4Select( ComplexType type, Key key ) throws IOException {
    StringBuffer sqlBuf = new StringBuffer();
    for (int i = 0; i < type.keyAttributeCount(); i++) {
      String keyAttrName = type.getKeyAttribute( i ).getName();
      Object keyValue = key.getKeyValue( i );
      String dbValue = conn.convertAttribute( keyValue, type.getKeyAttributeType( i ) );
      if (sqlBuf.length() > 0) sqlBuf.append( " AND " );
      sqlBuf.append( keyAttrName ).append( "=" ).append( dbValue );
    }
    return sqlBuf.toString();
  }

  protected String createParentKey4Select( ComplexType parentType, Key parentKey, QName relName, ComplexType childType, String grandParentKeySQL ) throws IOException {
    StringBuffer sqlBuf = new StringBuffer( grandParentKeySQL );
    for (int i = 0; i < parentType.keyAttributeCount(); i++) {
      String keyAttrName = parentType.getKeyAttribute( i ).getName();
      if (keyAttrName.toLowerCase().equals( "id" )) {
        keyAttrName = parentType.name().getName() + keyAttrName;
      }
      Object keyValue = parentKey.getKeyValue( i );
      String dbValue = conn.convertAttribute( keyValue, parentType.getKeyAttributeType( i ) );
      if (sqlBuf.length() > 0) sqlBuf.append( " AND " );
      sqlBuf.append( keyAttrName ).append( "=" ).append( dbValue );
    }
    return sqlBuf.toString();
  }

  protected void createParentKey4Insert( ComplexType parentType, Key parentKey, QName relName, ComplexType childType,
                                      StringBuffer columnBuf, StringBuffer valueBuf ) throws IOException {
    for (int i = 0; i < parentType.keyAttributeCount(); i++) {
      String keyAttrName = parentType.getKeyAttribute( i ).getName();
      if (keyAttrName.toLowerCase().equals( "id" )) {
        keyAttrName = parentType.name().getName() + keyAttrName;
      }
      Object keyValue = parentKey.getKeyValue( i );
      String dbValue = conn.convertAttribute( keyValue, parentType.getKeyAttributeType( i ) );

      if (columnBuf.length() > 0) {
        columnBuf.append( "," );
        valueBuf.append( "," );
      }
      columnBuf.append( keyAttrName );
      valueBuf.append( dbValue );
    }
  }

  protected Key createKey( ComplexType type, DBCursor idResult ) throws IOException {
    SimpleType keyType;
    int count = type.keyAttributeCount();
    if (count == 1) {
      keyType = type.getKeyAttributeType( 0 );
      if (keyType instanceof QNameType) {
        return sqlNamespaces.getQName( idResult.getString( type.getKeyAttribute( 0 ).getName() ));
      }
      else {
        Object value = idResult.getAttributeValue( type.getKeyAttribute( 0 ), type.getKeyAttributeType( 0 ), null );
        return new SimpleKey( value );
      }
    }
    else {
      Object[] keyValues = new Object[count];
      for (int i = 0; i < count; i++) {
        keyValues[i] = idResult.getAttributeValue( type.getKeyAttribute( i ), type.getKeyAttributeType( i ), null );
      }
      return new CombinedKey( keyValues );
    }
  }

  /**
   * Read the children according to the given relation and add them to the parent model.
   * RelationID may be a root element; if so, the parent model must be a root model.
   * @param parentModel
   * @param relationID
   * @param sql
   * @throws java.io.IOException
   */
  private int fetchNodes( Model parentModel, QName relationID, String sql, boolean recursive, String parentKeySQL )
  throws IOException {
    ModelFactory modelFactory;
    Model model;
    DBCursor dbCursor = null;
    int depth = 0;
    int index;
    ChildList childList = new ChildList();
    ComplexType type = (ComplexType) ((ComplexType) parentModel.type()).getChildType(relationID);

    modelFactory = ModelFactoryTable.getInstance().getFactory( type.name().getNamespace() );
    if (modelFactory == null) {
      Logger.warn("No factory found for type ID=" + type.name());
      return 0;
    }

    if (DEBUG) Logger.debug(sql);
    try {
      if (DEBUG) Logger.debug("fetchNodes --> sql " + sql);
      dbCursor = conn.query( sql );
      index = 0;
      while (dbCursor.next()) {
        ComplexType nodeType = type;
        Object[] attribs = new Object[nodeType.attributeCount()];
        Key key = fetchAttributes(attribs, dbCursor, type);
        model = modelFactory.createInstance(key, type, attribs, null, null);;
        childList.add(relationID, model);
        if (DEBUG) {
          Logger.debug("fetched node depth="+depth+" index="+index);
          index++;
        }
      }
      if (DEBUG) Logger.debug("fetchNodes finished depth="+depth);
    }
    catch (IOException ex) {
      conn.rollback();
      throw new IOException("Could not fetch nodes, cause="+ex.getMessage() + "sql="+sql);
    }
    finally {
      if (dbCursor != null) dbCursor.close();
    }

    model = childList.getFirstChild(null);
    while (model != null) {
      if (recursive) {
        if (model instanceof Businessobject) ((Businessobject) model).loading( true );
        loadChildren( model, parentKeySQL );
        if (model instanceof Businessobject) ((Businessobject) model).loading( false );
      }
      Model next = model.nextSibling(null); // nextSibling is destroyed by parentModel.add()
      if (model instanceof Businessobject) {
        Businessobject oldBo = (Businessobject) parentModel.get( relationID, model.key() );
        if (oldBo != null) {
          oldBo.updateFrom( (Businessobject) model );
        }
        else {
          parentModel.add(relationID, model);
        }
      }
      else {
        parentModel.add(relationID, model);
      }
      model = next;
    }
    return childList.size();
  }

  /**
   * Loads data from dataResource.
   *
   * @param sql the sql statement
   * @param parentModel the parent model to which the new tree will be added
   * @param maxCountResult max number of records to load for the top level of the reuslt tree
   * @param recursive if true children of loaded models alre loeaded recursively
   * @return the number of records loaded for the top level of result tree or -1 if maxCountResult
   *         has been exceeded
   * @throws java.io.IOException
   */
  private int loadData( String sql, Model parentModel, QName relationID, int maxCountResult, boolean recursive, String parentKeySQL )
  throws IOException {
    int count = 0;
    if (maxCountResult > 0 && maxCountResult < Integer.MAX_VALUE) {
      sql = conn.addLimit(sql, maxCountResult);
    }
    sql = conn.check(sql);
    count = fetchNodes( parentModel, relationID, sql, recursive, parentKeySQL );
    conn.finishSelect();
    return count;
  }

  /**
   * Test if a resultSet contains the specified column. Just catch the thrown exception...
   * @param resultSet
   * @param columnName
   * @return true if the column does not exist in the result set or the corresponding value is null
   */
  private boolean isNull( DBCursor resultSet, String columnName ) {
    Object obj = null;
    try {
      obj = resultSet.getString( columnName );
    }
    catch (IOException e) {
      return true;
    }
    return (obj == null);
  }

  private Key fetchAttributes(Object[] attributes, DBCursor dbCursor, ComplexType typeDef) throws IOException {
    SimpleType attributeType;
    QName   attributeID;
    int          keyAttrCount = typeDef.keyAttributeCount();
    Object[]     keyAttrs = null;
    Key          key = null;
    int          keyIndex;
    boolean      simpleKey = false;
    Object       value;

    if (keyAttrCount == 1) {
      simpleKey = true;
    }
    else if (keyAttrCount > 0) {
      keyAttrs = new Object[keyAttrCount];
    }

    for (int i = 0; i < attributes.length; i++) {
      attributeType = typeDef.getAttributeType( i );
      attributeID   = typeDef.getAttributeName( i );
      value = dbCursor.getAttributeValue( attributeID, attributeType, attributeType.createDefaultValue() );
      keyIndex = typeDef.getKeyIndex(attributeID);
      if (keyIndex >= 0) {
        attributes[i] = Key.KEY_ATTR;
        if (simpleKey) {
          if (attributeType instanceof QNameType) {
            key = (QName) value;
          }
          else {
            key = new SimpleKey( value );
          }
        }
        else {
          keyAttrs[keyIndex] = value;
        }
      }
      else {
        attributes[i] = value;
      }
    }
    if (keyAttrs != null) {
      key = new CombinedKey( keyAttrs );
    }
    return key;
  }

  /**
   * Create update statement for the given model
   * @param model     the model to be updated in database
   * @param tableName the datebase binding for which the update statement is created
   * @return the update statement or null if nothing has to be updated
   */
  private String createUpdateStatement( String parentIDSQL, String parentTypeSQL, Model model, String tableName) throws IOException{
    StringBuffer update = new StringBuffer();
    StringBuffer where = new StringBuffer();
    StringBuffer sets = new StringBuffer();
    SimpleType simpleType;
    QName attributeID;
    Object object;
    ComplexType complexType = (ComplexType) model.type();
    KeyBuilder keyBuilder = new KeyBuilder( conn );

    if (parentIDSQL != null) {
      where.append( "parentID=" );
      where.append( parentIDSQL );
      where.append( " AND parentType=" );
      where.append( parentTypeSQL );
      where.append( " AND " );
    }
    where.append( keyBuilder.createKeyCondition( model ) );


    for (int i = 0; i < complexType.attributeCount(); i++) {
      simpleType = complexType.getAttributeType(i);
      attributeID = complexType.getAttributeName(i);
      if (!attributeID.getName().equals("ID")) {
        object = model.get(attributeID);
        sets.append(attributeID.getName()).append("=");
        if (object == null) {
          sets.append("NULL,");
        }
        else {
          sets.append( conn.convertAttribute( object, simpleType ) ).append(",");
        }
      }
    }
    if (sets.length() > 1) {
      sets.setLength(sets.length() - 1); // remove last comma
      update.append("UPDATE ").append(tableName).append(" SET ").append(sets.toString());
      update.append(" WHERE ").append(where.toString());
      return update.toString();
    }
    else return null;
  }

  private String createInsertStatement( String parentKeyColumns, String parentKeyValues, Model model, String tableName) throws IOException{
    StringBuffer insert = new StringBuffer();
    StringBuffer columns = new StringBuffer();
    StringBuffer values = new StringBuffer();
    SimpleType simpleType;
    QName attributeID;
    Object object;
    ComplexType complexType = (ComplexType) model.type();

    if (parentKeyColumns != null) {
      columns.append( parentKeyColumns );
      values.append( parentKeyValues );
    }

    for (int i = 0; i < complexType.attributeCount(); i++) {
      simpleType = complexType.getAttributeType(i);
      attributeID = complexType.getAttributeName(i);
      object = model.get(attributeID);
      if (object != null) {
        if (columns.length() > 0) {
          columns.append( "," );
          values.append( "," );
        }
        columns.append( attributeID.getName() );
        String str = conn.convertAttribute( object, simpleType );
        values.append(str);
      }
    }
    insert.append("INSERT INTO ").append(tableName).append(" (").append(columns.toString());
    insert.append(") Values (").append(values.toString()).append(")");
    return insert.toString();
  }

  /**
   * @return true if model already existed, false if the model was inserted into database
   */
  public synchronized boolean exists( ComplexType boType, Key boID ) throws IOException {
    StringBuffer sqlBuf = new StringBuffer();
    StringBuffer condition = new StringBuffer();
    String       sql = null;
    DBCursor     dbCursor = null;
    boolean      exists = false;
    KeyBuilder   keyBuilder = new KeyBuilder( conn );

    try {
      sqlBuf.append("SELECT * FROM ").append(getTableName( boType.name() )).append(" WHERE ");
      keyBuilder.appendKey( condition, boID, boType );
      sqlBuf.append( condition ).append( " " );
      sql = sqlBuf.toString();
      if (DEBUG) Logger.debug("exists --> sql " + sql);
      dbCursor = conn.query( sql );
      exists = dbCursor.next();
      dbCursor.close();
      if (DEBUG) Logger.debug("jdbc updateModel: exists="+exists+", sql="+sql);
    }
    catch (Exception ex) {
      String msg = "Could not check if bo exists, id="+boID+" type="+(boType!=null?boType.name().toString():"null");
      Logger.error(msg, ex);
      throw new IOException( msg );
    }
    finally {
      if (dbCursor != null) dbCursor.close();
    }
    return exists;
  }

  /**
   * Delete the given model form relation model.tyxpe().name().
   * For recursive deletion use FOREIGN KEY with DELETE CASCADE
   * when creating tables.
   * @throws IOException
   */
  private void delete( String tableName, ComplexType modelType, String parentKeySQL, Key boID ) throws IOException {
    KeyBuilder   keyBuilder = new KeyBuilder( conn );
    String sql = null;
    StringBuffer sqlBuf = new StringBuffer( "DELETE FROM " );
    sqlBuf.append( tableName ).append( " WHERE " );
    StringBuffer condition = new StringBuffer();
    if (parentKeySQL != null) {
      condition.append( parentKeySQL );
    }
    keyBuilder.appendKey( condition, boID, modelType );
    sqlBuf.append( condition );
    sql = sqlBuf.toString();
    if (DEBUG) Logger.debug("delete --> sql " + sql);
    conn.execute( sql );
  }

  /**
   * Executes sql on this BOStorage's database without any checks. Avoid using this method!
   *
   * @param sql          the SQL to execute
   * @throws IOException exception thrown by DBConnection.execute()
   */
  public void execute( String sql) throws IOException {
    conn.execute( sql );
  }

  /**
   * Loads the bo of type having key id.
   * @param type the type
   * @param id   the key or null
   * @return the (first matching) bo  or null if not found
   * @throws IOException exception while accessing storage
   */
  public synchronized Businessobject load( ComplexType type, Key id ) throws IOException {
    Root dataRoot = new Root();
    loadBo( type.name(), id, dataRoot, type );
    Businessobject bo = (Businessobject) dataRoot.get( null, null );
    if (bo != null) {
      dataRoot.remove( bo.getParentRelationID(), bo );
    }  
    return bo;
  }

  /**
   * Loads BOs into conatiner. For all BOs already existing in container the existing BO
   * is updated from the loaded bo.
   * 
   * @param container    the conatiner to add loaded BOs to
   * @param relationName loaded BOs will be added to this relation
   * @param filterAttr   attribute to apply filterValeu to odr null for no filter
   * @param comparator
   * @param filterValue  the filter value (only applicable if filterAttr is not null)
   */
  public synchronized void load( Model container, QName relationName, QName filterAttr, int comparator, String filterValue ) {
    ComplexType containerType = (ComplexType) container.type();
    QName boName = containerType.getChildType( relationName ).name();
    StringBuffer sql = new StringBuffer( "SELECT * FROM " + getTableName( boName ) );
    if (filterAttr != null) {
      sql.append( " WHERE " ).append( filterAttr.getName() );
      if (filterValue == null) {
        switch (comparator) {
          case COMP_EQUAL: sql.append( " IS NULL" ); break;
          case COMP_NOT_EQUAL: sql.append( " IS NOT NULL" ); break;
          default: throw new RuntimeException( "Comparator not supported: "+comparator );
        }
        ;
      }
      else {
        switch (comparator) {
          case COMP_EQUAL: sql.append( "=" ).append( "'" + filterValue + "'" ); break;
          case COMP_NOT_EQUAL: sql.append( "<>" ).append( "'" + filterValue + "' OR " ).append( filterAttr.getName() ).append( " IS NULL"); break;
          default: throw new RuntimeException( "Comparator not supported: "+comparator );
        }
      }
    }
    try {
      loadData( sql.toString(), container, relationName, Integer.MAX_VALUE, true, "" );
    }
    catch (IOException e) {
      Logger.error("Could not load BOs, sql="+sql, e);
    }
  }
  
  public synchronized void store( Businessobject bo ) throws IOException {
    if (DEBUG) Logger.debug( "-> BO store, type name="+bo.type().name().getName());
    try {
      ComplexType boType = (ComplexType) bo.type();
      Key boID = bo.key();

      deleteChildren( boType, boID, "" );
      delete( getTableName( boType.name() ), boType, null, boID  );

      String tableName = getTableName( boType.name() );
      //TODO hier dürfen wir das so nicht machen --> feuert einen Event: bo.setChanged( new DateObject() );
      String sql = createInsertStatement( null, null, bo, tableName);
      if (DEBUG) Logger.debug("store --> sql " + sql);
      conn.execute( sql );

      storeChildren( bo, "", "" );
      conn.commit();
    }
    catch (Exception e) {
      conn.rollback();
      String msg = "Could not store bo";
      Logger.error(msg, e);
      throw new IOException( msg );
    }
    if (DEBUG) Logger.debug( "<- BO store, type name="+bo.type().name().getName());
  }

  private void storeChildren( Model model, String grandParentKeyColumns, String grandParentKeyValues ) throws IOException {
    ComplexType modelType = (ComplexType) model.type();
    Key modelKey = model.key();
    if (modelKey == null) {
      throw new IOException( "BoStorage does not support child models without key, type="+modelType.name() );
    }

    for (QNameIterator relNameIter = modelType.relationNames(); relNameIter.hasNext(); ) {
      QName relName = relNameIter.next();
      Relation rel = modelType.getRelation( relName );
      Type childType = rel.getChildType();
      if (childType instanceof ComplexType) {
        StringBuffer columnBuf = new StringBuffer( grandParentKeyColumns );
        StringBuffer valueBuf = new StringBuffer( grandParentKeyValues );
        createParentKey4Insert( modelType, modelKey, relName, (ComplexType) childType, columnBuf, valueBuf );
        String parentKeyColumns = columnBuf.toString();
        String parentKeyValues = valueBuf.toString();
        for (ModelIterator childIter = model.iterator( relName, null ); childIter.hasNext(); ) {
          Model child = childIter.next();
          String sql = createInsertStatement( parentKeyColumns, parentKeyValues, child, getTableName( childType.name() ) );
          if (DEBUG) Logger.debug("storeChildren --> sql " + sql);
          conn.execute( sql );
          storeChildren( child, parentKeyColumns, parentKeyValues );
        }
      }
      else {
        throw new IOException( "Unsupportet child type, name="+childType.name() );
      }
    }
  }

  public synchronized void delete( Businessobject bo ) throws IOException {
    ComplexType boType = (ComplexType) bo.type();
    Key boID = bo.key();

    delete( boType, boID );
  }

  public synchronized void delete( ComplexType boType, Key boID ) throws IOException {
    if (DEBUG) Logger.debug( "-> BO delete, type name="+boType.name().getName());
    try {
      deleteChildren( boType, boID, "" );
      delete( getTableName( boType.name() ), boType, null, boID  );
      conn.commit();
    }
    catch (Exception e) {
      conn.rollback();
      String msg = "Could not delete bo, id="+boID+" type="+(boType!=null?boType.name().toString():"null");
      Logger.error(msg, e);
      throw new IOException( msg );
    }
    if (DEBUG) Logger.debug( "<- BO delete, type name="+boType.name().getName());
  }

  private void deleteChildren( ComplexType modelType, Key boID, String grandParentKeySQL ) throws IOException {
    DBCursor idCursor = null;
    try {
      String sql;
      for (QNameIterator relNameIter = modelType.relationNames(); relNameIter.hasNext(); ) {
        QName relName = relNameIter.next();
        Relation rel = modelType.getRelation( relName );
        Type childType = rel.getChildType();
        String parentKeySQL = createParentKey4Select( modelType, boID, relName, (ComplexType) childType, grandParentKeySQL );
        if (childType instanceof ComplexType) {
          ComplexType cType = (ComplexType) childType;
          if (cType.keyAttributeCount() == 0) {
            throw new IOException( "Children without key are not supported, child type="+cType.name() );
          }
          StringBuffer sqlBuf = new StringBuffer( "SELECT ");
          for (int i = 0; i < cType.keyAttributeCount(); i++) {
            if (i > 0) sqlBuf.append( "," );
            sqlBuf.append( cType.getKeyAttribute( i ).getName() );
          }
          sqlBuf.append( " FROM ").append( getTableName( childType.name() ));
          sqlBuf.append( " WHERE ").append( parentKeySQL );
          sql = sqlBuf.toString();
          if (DEBUG) Logger.debug("deleteChildren --> sql " + sql);
          idCursor = conn.query( sql );
          while (idCursor.next()) {
            Key childID = createKey( cType, idCursor );
            deleteChildren( cType, childID, parentKeySQL );
            delete( getTableName( childType.name() ), cType, parentKeySQL, childID );
          }
        }
        else {
          throw new IOException( "Unsupportet child type, name="+childType.name() );
        }
      }
    }
    finally{
      if (idCursor != null) idCursor.close();
    }
  }

  private void loadBo( QName boName, Key boID, Model boListe, ComplexType boType ) throws IOException {
    String sql = "SELECT * FROM " + getTableName( boName ) + " WHERE " + createKey4Select( boType, boID );
    loadData( sql, boListe, boType.name(), 1, true, "" );
    Businessobject bo = (Businessobject) boListe.get( null, null );
  }

  private void loadChildren( Model model, String grandParentKeySQL ) throws IOException {
    ComplexType modelType = (ComplexType) model.type();
    Key modelKey = model.key();
    if (modelKey == null) {
      throw new IOException( "BoStorage does not support child models without key, type="+modelType.name() );
    }

    String sql;
    for (QNameIterator relNameIter = modelType.relationNames(); relNameIter.hasNext(); ) {
      QName relName = relNameIter.next();
      Relation rel = modelType.getRelation( relName );
      Type childType = rel.getChildType();
      String parentKeySQL = createParentKey4Select( modelType, modelKey, relName, (ComplexType) childType, grandParentKeySQL );
      if (childType instanceof ComplexType) {
        sql = "SELECT * FROM " + getTableName( childType.name() ) + " WHERE "+parentKeySQL;
        loadData( sql, model, relName, Integer.MAX_VALUE, true, parentKeySQL );
      }
      else {
        throw new IOException( "Unsupportet child type, name="+childType.name() );
      }
    }
  }

  /**
   * Creates a sequence object for a sequence existing in this BoStorage
   * @param sequenceName the sequence's name
   * @throws IOException if sequence does not exist in this BoStorage or cannot be read
   */
  public NumberSequence createSequence( String sequenceName ) throws IOException {
    return conn.getSequence( sequenceName );
  }
}
