package de.pidata.models.xml.binder;

public class TestXmlWriter {

  private void testEncoding() {
    // &#x1F3A8; ist richtig und das folgende ist falsch:  &#xd83c;&#xdfa8;
    XmlWriter writer = new XmlWriter( null, "ASCII" );
    String xml = writer.convertCDATA( "--\ud83c\udfa8\n++äöüßµ++" );
    System.out.println( xml );
    String xml2 = writer.convertString( "--\ud83c\udfa8\n++äöüßµ++" );
    System.out.println( xml2 );
  }

  public static void main(String[] args) {
    try {
      new TestXmlWriter().testEncoding();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
}
