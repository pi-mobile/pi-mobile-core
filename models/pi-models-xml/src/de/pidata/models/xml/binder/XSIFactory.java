/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.binder;

import de.pidata.models.tree.AbstractModelFactory;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.StringType;

import java.util.Hashtable;

public class XSIFactory extends AbstractModelFactory {

  protected static final int NUM_BASE_TYPES = 22;
  public static final Namespace NAMESPACE = Namespace.getInstance("http://www.w3.org/2001/XMLSchema-instance");
  public static final QName ID_NIL = NAMESPACE.getQName("nil");
  public static final QName ID_TYPE = NAMESPACE.getQName("type");
  public static final QName ID_SCHEMA_LOCATION = NAMESPACE.getQName("schemaLocation");
  public static final QName ID_NO_NAMESPACE_SCHEMA_LOCATION = NAMESPACE.getQName("noNamespaceSchemaLocation");

  public XSIFactory() {
    super( NAMESPACE, "http://www.w3.org/2001/XMLSchema-instance", "2001" );
    attributes = new Hashtable();
    attributes.put(ID_NIL, StringType.getDefString());
    attributes.put(ID_TYPE, StringType.getDefString());
    attributes.put(ID_SCHEMA_LOCATION, StringType.getDefString());
    attributes.put(ID_NO_NAMESPACE_SCHEMA_LOCATION, StringType.getDefString());
  }
}
