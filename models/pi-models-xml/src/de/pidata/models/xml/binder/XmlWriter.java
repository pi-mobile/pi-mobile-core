/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.binder;

import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Relation;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.*;
import de.pidata.qnames.NamespaceTable;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.qnames.QName;
import de.pidata.progress.ProgressListener;
import de.pidata.string.Helper;
import de.pidata.system.base.Storage;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.SystemManager;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Enumeration;

public class XmlWriter {

  public final static String XM_VERSION_10 = "1.0";
  public final static String XM_VERSION_11 = "1.1";

  private final int INDENT = 2;
  private Storage storage;
  private NamespaceTable namespaces;
  private int indexForNamespace; /** keeps the position within the buffer at which the namespace definitions will be inserted */
  private String encoding;
  private boolean writeUTF;
  private boolean prettyPrint = true;
  private boolean writeInvalidCharacters = true;
  private boolean withNamespaces;
  private boolean forcePrefix = false;
  private boolean forceEmptyAttributes = false;
  private String xmlVersion = XM_VERSION_11;

  public XmlWriter(Storage storage) {
    this(storage, SystemManager.getInstance().getProperty("comm.encoding", "UTF-8"));
  }

  public XmlWriter(Storage storage, String encoding) {
    this.storage = storage;
    this.encoding = encoding;
    this.writeUTF = (encoding.toUpperCase().startsWith("UTF-"));
  }

  public String getEncoding() {
    return encoding;
  }

  public boolean isWriteUTF() {
    return writeUTF;
  }

  public void setWriteUTF( boolean writeUTF ) {
    this.writeUTF = writeUTF;
  }

  public boolean isWriteInvalidCharacters() {
    return writeInvalidCharacters;
  }

  public void setWriteInvalidCharacters( boolean writeInvalidCharacters ) {
    this.writeInvalidCharacters = writeInvalidCharacters;
  }

  public boolean isPrettyPrint() {
    return prettyPrint;
  }

  public void setPrettyPrint( boolean prettyPrint ) {
    this.prettyPrint = prettyPrint;
  }

  public boolean isForcePrefix() {
    return forcePrefix;
  }

  public void setForcePrefix( boolean forcePrefix ) {
    this.forcePrefix = forcePrefix;
  }

  public boolean isForceEmptyAttributes() {
    return forceEmptyAttributes;
  }

  public void setForceEmptyAttributes( boolean forceEmptyAttributes ) {
    this.forceEmptyAttributes = forceEmptyAttributes;
  }

  public String getXmlVersion() {
    return xmlVersion;
  }

  public void setXmlVersion( String xmlVersion ) {
    this.xmlVersion = xmlVersion;
  }

  public void open() {
  }

  public void write( String destination, Model model, QName relationID ) throws IOException {
    StringBuilder buffer = writeXML(model, relationID);
    OutputStream storageOut = null;
    try {
      storageOut = this.storage.write(destination, true, false );
      OutputStreamWriter out = new OutputStreamWriter(storageOut);
      out.write( buffer.toString() );
      out.flush();
      storageOut.flush();
    }
    finally {
      StreamHelper.close( storageOut );
    }
  }

  public void write(String destination, Model model, QName relationID, boolean writeRoot) throws IOException {
    if (writeRoot) {
      write(destination, model, relationID);
    }
    else {
      /**
       * TODO: es kann sein das man nur Teile eines Models in XML schreiben will.
       * Dies kann der XMLWriter noch nicht.
       */
      throw new IllegalArgumentException("XmlWriter can only write with writeRoot is true !!!");
    }
  }

  public void close() {
  }

  public StringBuilder writeXML( Model model, boolean withHeader ) {
    return writeXML( model, model.getParentRelationID(), withHeader );
  }

  public void startXML( Model model ) {
    // build a new namespace table locally and get all known namespaces from the model;
    // will be extended as needed here but not in the original model
    this.namespaces = new NamespaceTable( model.namespaceTable() );
    this.indexForNamespace = -1;
  }

  public StringBuilder writeXML( Model model, QName relationID ) {
    return writeXML( model, relationID, true );
  }

  public StringBuilder writeXML( Model model, QName relationID, boolean withHeader ) {
    return writeXML( model, relationID, withHeader, true, 0, true );
  }

  /**
   * Write complete model structure as XML to StringBuilder
   *
   * @param model          the model to write to buffer
   * @param relationName   the relation to use for top level tag
   * @param withHeader     if true start with XML header (<?xml...)
   * @param withNamespaces if true insert namespace declarations (xmlns=...) into top level tag
   * @return
   */
  public StringBuilder writeXML( Model model, QName relationName, boolean withHeader, boolean withNamespaces, int indent, boolean isRoot ) {
    this.withNamespaces = withNamespaces;
    StringBuilder buffer = new StringBuilder();
    if (isRoot) {
      startXML( model );
    }
    if (withHeader) {
      // Use xml version 1.1 to enable characters like &#x8; (Backslash) and &#xb; (Line Tabulation)
      // Reason is validation goes wrong e.g. in STARC API instead.
      buffer.append( "<?xml version=\"" + xmlVersion + "\" encoding=\"" ).append( encoding ).append( "\" ?>" );
      if (prettyPrint) buffer.append( "\n" );
    }
    if (relationName == null) {
      relationName = model.type().name();
    }
    writeXML( buffer, model, relationName, model.type(), indent, isRoot );
    if (withNamespaces) {
      insertNamespaces( buffer );
    }
    return buffer;
  }

  /**
   * Creates XML-hierarchy for the given node.
   * All attributtes and children are validated by recursion
   *
   * @param buffer the written XML-hierarchy so far
   * @param model the current model in the ML-hierarchy
   * @param aRelationName is Relation from model to childmodel
   * @param indent the indent of each XML-tag
   * @param isRootNode the first node in the recursion is the rootnode
   */

  private void writeXML( StringBuilder buffer, Model model, QName aRelationName, Type modelType, int indent, boolean isRootNode ) {
    SimpleType simpleType;

    if (aRelationName == null) {
      throw new IllegalArgumentException( "RelationID must not be null" );
    }

    if (prettyPrint) {
      for (int j = 0; j < indent; j++) {
        buffer.append( " " );
      }
    }

    NamespaceTable nsTab = model.namespaceTable();
    if (nsTab.getOwner() == model) {
      namespaces.addNamespaces( nsTab );
    }

    String addPrefix = "";
    if (withNamespaces) {
      addPrefix = namespaces.getOrCreatePrefix( aRelationName );
      if (addPrefix.length() > 0) addPrefix += ":";
    }

    String name = aRelationName.getName();
    if (model.type() instanceof ComplexType) {
      if (appendTagStart( buffer, aRelationName, modelType, model, isRootNode, addPrefix )) {
        ComplexType nodeType = (ComplexType) model.type();
        if (model.firstChild( null ) != null) {
          appendChildren( buffer, indent, model, null, nodeType );
          // don't add additional characters to simple content
          if (model.childCount( ComplexType.CDATA ) > 0) {
            appendEndTagForSimpleContent( buffer, name, addPrefix );
          }
          else {
            appendEndTag( buffer, indent, name, addPrefix );
          }
        }
        else {
          String xmlValue = convertAttribute( model.getContent(), nodeType.getContentType() );
          if (xmlValue != null) buffer.append(xmlValue);
          appendEndTagForSimpleContent( buffer, name, addPrefix );
        }
      }
    }
    else {
      simpleType = (SimpleType) model.type();
      buffer.append("<").append( addPrefix ).append( name ).append( ">");
      String xmlValue = convertAttribute(model.getContent(), simpleType);
      if (xmlValue != null) buffer.append(xmlValue);
      buffer.append("</" ).append( addPrefix ).append( name ).append( ">");
      if (prettyPrint) buffer.append( "\n" );
    }
  }

  /**
   * Appends start tag with its attributes to buffer. Does not append tag's children.
   * If there is no content (childCount==0 and content is empty) tag is closed ( /> ) otherwise appendEndTag() has to be called to
   * close this tag.
   *
   * @param buffer        the buffer to append with XML
   * @param relationName  the relationName to use for start tag
   * @param relationType  the relationType, may be a parent type of model.type()
   * @param model         the model containing start tag attributes
   * @param isRootNode    true if this node is a root node, so keep buffer position for namespace declarations
   * @param addPrefix     optional prefix to write in front of tag name
   * @return              true if tag is left unclosed for children
   */
  public boolean appendTagStart( StringBuilder buffer, QName relationName, Type relationType, Model model, boolean isRootNode, String addPrefix ) {
    String name = relationName.getName();
    ComplexType complexType;
    String attributeValue;
    SimpleType simpleType;
    QName attrName;
    Object object;
    complexType = (ComplexType) model.type();

    buffer.append("<").append( addPrefix ).append( name );

    //----- Add default namespace declaration to element
    if (forcePrefix) {
      if (relationName.getNamespace() == namespaces.getDefaultNamespace()) {
        buffer.append( " xmlns=\"" + relationName.getNamespace().getUri() + "\"" );
      }
    }

    //----- Write xsi:type if current type inherits form required type
    if (complexType.name() != relationType.name()) {
      attributeValue = convertAttribute( complexType.name(), QNameType.getQName() );
      namespaces.addNamespace( XSIFactory.NAMESPACE, "xsi" );
      buffer.append( " xsi:type=\"" ).append( attributeValue ).append( "\"" );
    }

    //----- Write normal attributes
    for (int i = 0; i < complexType.attributeCount(); i++) {
      simpleType = complexType.getAttributeType(i);
      attrName = complexType.getAttributeName(i);
      object = model.get(attrName);
      if (object != null) {
        attributeValue = convertAttribute(object, simpleType);
        if (attributeValue != null) {
          buffer.append(" ");
          if (attrName.getNamespace() != relationName.getNamespace()) {
            buffer.append( namespaces.getOrCreatePrefix( attrName ) ).append( ":" );
          }
          else if (forcePrefix) {
            if ( attrName.getNamespace() != namespaces.getDefaultNamespace()) {
              buffer.append( namespaces.getOrCreatePrefix( attrName ) ).append( ":" );
            }
          }
          buffer.append( attrName.getName() ).append( "=\"" );
          buffer.append( attributeValue ).append( "\"" );
        }
      }
    }
    //----- Write any attributes
    AnyAttribute anyAttribute = complexType.getAnyAttribute();
    if (anyAttribute != null) {
      for (Enumeration nameEnum = model.anyAttributeNames(); nameEnum.hasMoreElements(); ) {
        attrName = (QName) nameEnum.nextElement();
        simpleType = AnyAttribute.findType(attrName);
        object = model.get(attrName);
        if (object != null) {
          attributeValue = convertAttribute(object, simpleType);
          if (attributeValue != null) {
            String nsPrefix = namespaces.getOrCreatePrefix( attrName );
            if (nsPrefix.length() > 0) nsPrefix += ":";
            buffer.append( " " ).append( nsPrefix ).append( attrName.getName() ).append( "=\"" );
            buffer.append( attributeValue ).append( "\"" );
          }
        }
      }
    }

    if (isRootNode) {
      indexForNamespace = buffer.length();
    }

    //----- write end of attributes
    boolean hasSimpleContent = !Helper.isNullOrEmpty( model.getContent() );
    boolean hasContent = ((model.firstChild(null) != null) || hasSimpleContent);
    if (hasContent) {
      buffer.append( ">" );
      // don't add additional characters to simple or mixed content
      if (prettyPrint && (model.childCount( ComplexType.CDATA ) == 0) && !hasSimpleContent) {
        buffer.append( "\n" );
      }
    }
    else {
      buffer.append("/>");
      if (prettyPrint) {
        buffer.append( "\n" );
      }
    }
    return hasContent;
  }

  /**
   * Append all children of model in relationName to buffer
   *
   * @param buffer       the buffer to append
   * @param indent       current indent (only valid if prettyPrint==true)
   * @param model        the model who's children will be appended to buffer
   * @param relationName if not null only write children of this relation
   * @param modelType    model's type like declared in ints parent node (model's real type might be derived from this modelType)
   */
  public void appendChildren( StringBuilder buffer, int indent, Model model, QName relationName, ComplexType modelType ) {
    ModelIterator mit = model.iterator( relationName, null );
    while (mit.hasNext()) {
      Model child = mit.next();
      QName relationID = child.getParentRelationID();
      if (relationID == DefaultComplexType.CDATA) {
        Object content = child.getContent();
        if (content != null) {
          buffer.append( convertCDATA( content.toString() ) );
        }
      }
      else {
        Type requiredType = null;
        // might be participant of a substitution group
        Type childType = child.type();
        Relation childRootRelation = ModelFactoryTable.findRootRelation( childType.name() );
        if (childRootRelation != null) {
          QName substGroup = childRootRelation.getSubstitutionGroup();
          if (substGroup != null) {
            requiredType = childType;
            relationID = childType.name();
          }
        }
        if (requiredType == null) {
          requiredType = modelType.getChildType( relationID );
        }
        writeXML( buffer, child, relationID, requiredType, indent + INDENT, false );
      }
    }
  }

  /**
   * Append close tag to buffer
   *
   * @param buffer    the XML buffer
   * @param indent    current indent (only valid if prettyPrint==true)
   * @param name      tag name
   * @param addPrefix optional prefix to write in front of tag name
   */
  public void appendEndTag( StringBuilder buffer, int indent, String name, String addPrefix ) {
    if (prettyPrint) {
      for (int i = 0; i < indent; i++) {
        buffer.append( " " );
      }
    }
    buffer.append( "</" ).append( addPrefix ).append( name ).append( ">" );
    if (prettyPrint) buffer.append( "\n" );
  }

  /**
   * Append close tag to buffer. Do not add any additional characters.
   *
   * @param buffer    the XML buffer
   * @param name      tag name
   * @param addPrefix optional prefix to write in front of tag name
   */
  public void appendEndTagForSimpleContent( StringBuilder buffer, String name, String addPrefix ) {
    buffer.append( "</" ).append( addPrefix ).append( name ).append( ">" );
    if (prettyPrint) buffer.append( "\n" );
  }

  /**
   * Build the namspace declarations and insert them into the buffer
   * on the root node.
   * @param buffer the buffer with the document
   */
  public void insertNamespaces( StringBuilder buffer ) {
    StringBuilder nsDeclaration = namespaces.buildNamespaceDeclaration( prettyPrint );
    buffer.insert( indexForNamespace, nsDeclaration );
  }

  /**
   * Return the string value for the given object by using his simpleType
   *
   * @param object the object to convert
   * @param simpleType the type of the object
   * @return the String value of the given object
   */
  private String convertAttribute(Object object, SimpleType simpleType) {
    if (simpleType == null) {
      throw new IllegalArgumentException("Arguments simpleType is wrong: simpleType == null");
    }

    if (object != null) {
      if (simpleType instanceof QNameType) {
        QName idValue = (QName) object;
        if (QNameType.getNCName().isAssignableFrom( simpleType )) {
          return convertString(idValue.getName());
        }
        else {
          String nsPrefix = namespaces.getOrCreatePrefix( idValue );
          if ("".equals( nsPrefix )) {
            return convertString( idValue.getName() );
          }
          else {
            return nsPrefix + ":" + convertString( idValue.getName() );
          }
        }
      }
      else if (simpleType instanceof StringType) {
        return convertString(object.toString());
      }
      else if (simpleType instanceof DecimalType) {
        return object.toString();
      }
      else if (simpleType instanceof NumberType) {
        return String.valueOf(object);
      }
      else if (simpleType instanceof BooleanType) {
        return String.valueOf(object);
      }
      else if (simpleType instanceof DateTimeType) {
        QName type = ((DateTimeType) simpleType).getType();
        if (type == DateTimeType.TYPE_DATE) {
          return DateTimeType.toDateString((DateObject) object);
        }
        else if (type == DateTimeType.TYPE_DATETIME) {
          return DateTimeType.toDateTimeString((DateObject) object, true);
          // TODO folgende Codezeile wäre richtig, ist aber inkompatibel mit dem Konstruktor von DateObject
          // in alten Clients: MMontage bis v75, MService bis v30
          // return DateTimeType.toDateString((DateObject) object) + "T" + DateTimeType.toTimeString((DateObject) object, true, 3, ':', true);
        }
        else if (type == DateTimeType.TYPE_TIME) {
          return DateTimeType.toTimeString((DateObject) object, true);
        }
      }
      else if (simpleType instanceof BinaryType) {
        return BinaryType.toBase64String( ((Binary) object).getBytes(), ((Binary) object).size() );
      }
      else if (simpleType instanceof DurationType){
        return object.toString();
      }
      else {
        SimpleType baseType = (SimpleType) simpleType.getBaseType();
        if (baseType != null) {
          return convertAttribute(object, baseType);
        }
        else {
          throw new IllegalArgumentException("not implemented base type for attribute [" + simpleType.name() + "]");
        }
      }
    }
    return null;
  }

  /**
   * Valid unicode characters for xml version 1.0 according to https://www.w3.org/TR/xml/#charsets are:
   * 	#x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]
   * 	any Unicode character, excluding the surrogate blocks, FFFE, and FFFF.
   * @param aktChar
   * @return
   */
  private boolean isValidChar( char aktChar ) {
    return (aktChar == 0x9 || aktChar == 0xa || aktChar == 0xd
        || (aktChar >= 0x20 && aktChar <= 0xd7ff)
        || (aktChar >= 0xe000 && aktChar != 0xfffe && aktChar != 0xffff));
  }

  private static int escapeCharacter( StringBuilder buf, int i, char aktChar ) {
    buf.setCharAt( i, '&' );
    String codeStr;
    if (Character.isHighSurrogate( aktChar )) {
      int unicode = Character.toCodePoint( aktChar, buf.charAt( i + 1 ) );
      buf.setCharAt( i +1, '#' );
      codeStr = "x" + Integer.toString( unicode, 16 ) + ";";
      buf.insert( i + 2, codeStr );
      i = i + 1 + codeStr.length();
    }
    else {
      codeStr = "#x" + Integer.toString( aktChar, 16 ) + ";";
      buf.insert( i + 1, codeStr );
      i += codeStr.length();
    }
    return i;
  }

  protected String convertString( String string ) {
    StringBuilder buf = new StringBuilder( string );
    char aktChar;
    for (int i = 0; i < buf.length(); i++) {
      aktChar = buf.charAt(i);
      if (aktChar < ' ') {
        if (writeInvalidCharacters || isValidChar( aktChar )) {
          buf.setCharAt( i, '&' );
          buf.insert( i + 1, "#x" + Integer.toString( aktChar, 16 ) + ";" );
        }
        else {
          // skip / remove invalid
          buf.deleteCharAt( i );
        }
      }
      else if (aktChar > 126) {
        if (writeInvalidCharacters || isValidChar( aktChar )) {
          if (!writeUTF) {
            i = escapeCharacter( buf, i, aktChar );
          }
        }
        else {
          // skip / remove invalid
          buf.deleteCharAt( i );
        }
      }
      else {
        switch (aktChar) {
          case '\"': {
            buf.setCharAt(i, '&');
            buf.insert(i+1, "quot;");
            break;
          }
          case '\'': {
            buf.setCharAt(i, '&');
            buf.insert(i+1, "apos;");
            break;
          }
          case '<': {
            buf.setCharAt(i, '&');
            buf.insert(i+1, "lt;");
            break;
          }
          case '>': {
            buf.setCharAt(i, '&');
            buf.insert(i+1, "gt;");
            break;
          }
          case '&': {
            buf.setCharAt(i, '&');
            buf.insert(i+1, "amp;");
            break;
          }
        }
      }
    }
    if (buf.length() == 0) {
      if (forceEmptyAttributes) {
        return "";
      }
      else {
        return null;
      }
    }
    else {
      return buf.toString();
    }
  }

  protected String convertCDATA( String string ) {
    StringBuilder buf = new StringBuilder( string );
    char aktChar;
    for (int i = 0; i < buf.length(); i++) {
      aktChar = buf.charAt(i);
      if ((aktChar > 126) && (!encoding.toUpperCase().startsWith("UTF-"))) {
        i = escapeCharacter( buf, i, aktChar );
      }
      else {
        switch (aktChar) {
          case '\"': {
            buf.setCharAt( i, '&' );
            buf.insert( i + 1, "quot;" );
            i += 5;
            break;
          }
          case '\'': {
            buf.setCharAt( i, '&' );
            buf.insert( i + 1, "apos;" );
            i += 5;
            break;
          }
          case '<': {
            buf.setCharAt( i, '&' );
            buf.insert( i + 1, "lt;" );
            i += 3;
            break;
          }
          case '>': {
            buf.setCharAt( i, '&' );
            buf.insert( i + 1, "gt;" );
            i += 3;
            break;
          }
          case '&': {
            buf.setCharAt( i, '&' );
            buf.insert( i + 1, "amp;" );
            i += 4;
            break;
          }
        }
      }
    }
    return buf.toString();
  }

  public int write( Model message, OutputStreamWriter os, ProgressListener progressListener ) throws IOException {
    QName relationID = message.getParentRelationID();
    if (relationID == null) {
      relationID = message.type().name();
    }
    StringBuilder buffer = writeXML( message, relationID );
    int sum = StreamHelper.buffer2Stream( buffer, os, Integer.MAX_VALUE, progressListener );
    return sum;
  }

  public static void write( Storage storage, String fileName, Model model ) throws IOException {
    QName relationID = model.getParentRelationID();
    if (relationID == null) {
      relationID = model.type().name();
    }
    XmlWriter xmlWriter = new XmlWriter( storage );
    xmlWriter.write( fileName, model, relationID );
  }
}
