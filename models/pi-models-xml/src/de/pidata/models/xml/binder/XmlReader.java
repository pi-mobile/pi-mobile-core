/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.binder;

import de.pidata.log.Logger;
import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.*;
import de.pidata.progress.ProgressListener;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Hashtable;
import java.util.List;
import java.util.ArrayList;

public class XmlReader {

  private static final boolean DEBUG = false;

  public static final Namespace NAMESPACE = Namespace.getInstance("de.pidata.models");
  private static final String ATTR_TARGETNAMESPACE = "targetNamespace";
  private static final String UNDEFINED = "undefined";

  private static final int INITIAL_STACK_SIZE = 16;
  private static final int EXTENT_STACK_SIZE = 8;
  private static final int INITIAL_NS_COUNT = 20;
  private static final int MAX_PREFIX_LENGTH = 5;
  private static final int MAX_DUMP_CHARS = 1024;
  private static final int SRCBUF_KEEP = 10;

  private static Hashtable entityMap;
  static {
    entityMap = new Hashtable();
    entityMap.put( "amp",   Character.valueOf( '&' ) );
    entityMap.put( "apos",  Character.valueOf( '\'' ) );
    entityMap.put( "gt",    Character.valueOf( '>' ) );
    entityMap.put( "lt",    Character.valueOf( '<' ) );
    entityMap.put( "quot",  Character.valueOf( '"' ) );
    entityMap.put( "auml",  Character.valueOf( (char) 0xE4 ) );
    entityMap.put( "ouml",  Character.valueOf( (char) 0xF6 ) );
    entityMap.put( "uuml",  Character.valueOf( (char) 0xFC ) );
    entityMap.put( "Auml",  Character.valueOf( (char) 0xC4 ) );
    entityMap.put( "Ouml",  Character.valueOf( (char) 0xD6 ) );
    entityMap.put( "Uuml",  Character.valueOf( (char) 0xDC ) );
    entityMap.put( "szlig", Character.valueOf( (char) 0x3B2 ) );
    new XSIFactory();
  }

  private InputStream dataStream;
  private boolean continuous = false;
  private Reader reader;
  private String encoding = "UTF-8";
  private char[] srcBuf;
  private int srcPos;
  private int srcCount;

  private int line;
  private int column;
  private int depth;
  private int parseIndex;

  /** whenever a new tag is found in srcBuf that tag is copied to txtBuf while parsing. e tag always begins at txtBuf[0] */
  private char[] txtBuf = new char[512];
  /** Pointer to the next positiopn to be used inside txtBuf */
  private int txtPos = 0;
  private int txtEnd = 0;
  private int[] attrStart = new int[16];
  private int[] attrNsPos = new int[16];
  private int[] attrEnd = new int[16];
  private int[] valueStart = new int[16];
  private int[] valueEnd = new int[16];
  private int attrCount;
  private Hashtable anyAttribs = null;

  private ProgressListener progressListener;

  private char[] entityBuf = new char[10];

  /**
   * Array of declared namespace prefixes. Attention: first character contains prefix's length
   */
  private NamespaceTable namespaces;
  private String xsiPrefix;
  private Type   xsiType;
  protected Namespace documentNamespace;

  protected List modelNameStack = new ArrayList();
  private int tempID = 0;

  public XmlReader() {
    srcBuf = new char[4096];
  }

  public String lastLine() {
    if (txtPos < 1) return null;
    else return new String(txtBuf, 0, txtPos-1);
  }

  /**
   * Looks for a namespace prefix matching the characters between startPos (included) and
   * endPos (excluded). nsPrefixes is searched from this.nsCounts[this.depth] backwards.
   *
   * @param startPos the position of first character of the prefix inside this.txtbuf
   * @param endPos   the position after the last character of the prefix inside this.txtbuf
   * @return the namespace identified by the prefix or null if not found
   */
  public final Namespace findNamespace(int startPos, int endPos) {
    return this.namespaces.getByPrefix( txtBuf, startPos, endPos );
  }

  private Namespace getDefaultNamespace() {
    Namespace namespace = this.namespaces.getDefaultNamespace();
    if (namespace == null) {
      namespace = this.documentNamespace;
    }
    return namespace;
  }

  private void setInput( Reader reader ) {
    this.reader = reader;

    line = 1;
    column = 0;
    //defaultNamespace = NAMESPACE_NONE;

    if (reader == null)
      return;

    srcPos = 0;
    srcCount = 0;
    depth = 0;
  }

  private void parseError(String message, Type parentType, Exception ex) throws IOException {
    if (parentType != null) {
      message = message + " in parentType [" + parentType.name() + "]";
    }
    message = message +" line="+line+", column="+column;
    if (DEBUG) Logger.info("");
    Logger.error( "XmlReader error: "+ message, ex );
    Logger.error("   last line: "+lastLine());
    dumpInput();
    throw new IOException(message);
  }

  private void parseError( String message ) throws IOException {
    parseError( message, null );
  }

  private void parseError( String message, Type parentType ) throws IOException {
    parseError( message, parentType, null );
  }

  private void back( int count ) throws IOException {
    txtPos -= count;
    if (txtPos < 0) {
      throw new IOException( "Illegal txtPos after back() txtPos="+txtPos );
    }
  }

  /**
   * Reads next character from srcBuf. If end of srcBuf is reaches srcBuf is refilled from reader.
   * Copies character to txtBuf[txtPos] if not CR, LF. Compresses multiple whitespace chars to a
   * single one when writing to txtBuf. txtPos is incremented if character was written to txtBuf.
   *
   * This method must not process entities - think of converting &lt; to '<' before looking for next tag.
   * Each caller has to decide if and when it should call readEntity().
   *
   * @return the character read
   * @throws java.io.IOException
   */
  private final char read() throws IOException {
    int character;
    char bufChar = 0;

    if (txtPos < txtEnd) {
      return txtBuf[txtPos++];
    }
    try {
      do {
        if ((srcBuf.length <= 1) || (reader == null)) {
          if (reader == null) character = dataStream.read();
          else character = reader.read();
          if ((this.progressListener != null) && ((this.parseIndex % 100) == 0)) {
            progressListener.updateProgress( null, parseIndex );
          }
        }
        else if (srcPos < srcCount) {
          character = srcBuf[srcPos++];
        }
        else {
          srcCount = reader.read( srcBuf, 0, srcBuf.length );
          if (srcCount <= 0) {
            character = -1;
          }
          else {
            character = srcBuf[0];
            if (DEBUG) Logger.debug( "---- XMLReader read(): " + new String( srcBuf, 0, srcCount ) );
            if (this.progressListener != null) {
              progressListener.updateProgress( null, parseIndex + srcCount );
            }
          }
          srcPos = 1;
        }

        if (character == -1) {
          throw new IOException( "Unexpected end of input stream." );
        }
        else {
          bufChar = (char) character;
        }
      } while (bufChar == 0);
    }
    catch (IOException ex) {
      if (DEBUG) Logger.error( "I/O error while readign from XML input", ex );
      throw ex;
    }
    catch (Exception ex) {
      Logger.error( "Error while reading from XML input", ex );
      throw new IOException( ex.getMessage() );
    }

    if (txtPos == txtBuf.length) {
      char[] bigger = new char[txtPos * 4 / 3 + 4];
      System.arraycopy( txtBuf, 0, bigger, 0, txtPos );
      txtBuf = bigger;
    }
    txtBuf[txtPos++] = bufChar;
    txtEnd++;
    parseIndex++;
    return (char) character;
  }

  private boolean compareString(String compStr, int startPos, int endPos) {
    int len = endPos - startPos;
    if (compStr.length() != len) return false;
    for (int i = 0; i < len; i++) {
      if (compStr.charAt(i) != txtBuf[startPos+i]) return false;
    }
    return true;
  }

  /**
   * Parses entity from txtBuf and replaces it by corresponding char.
   * Finally calls read() and so returns the character after the entity.
   *
   * It is important not to return the entity char to avoid it to be processed
   * by caller - thing of returning '<' (converted fom &lt;) to loop searching
   * for next tag.
   *
   * @return char after the entity char
   * @throws IOException
   */
  private void readEntity() throws IOException {
    int entityBufLen = 0;
    int entityStartPos = txtPos - 1;
    int character = '&';
    while (character != ';') {
      character = read();
      if (character == ';') {
        String str = new String( entityBuf, 0, entityBufLen );
        if (str.charAt( 0 ) == '#') {
          if (str.charAt( 1 ) == 'x') {
            txtBuf[entityStartPos] = (char) Integer.parseInt( str.substring( 2 ), 16 );
          }
          else {
            txtBuf[entityStartPos] = (char) Integer.parseInt( str.substring( 1 ) );
          }
        }
        else {
          Character entityChar = (Character) entityMap.get( str );
          if (entityChar == null) {
            throw new IOException( "Unknown entity: " + str );
          }
          else {
            txtBuf[entityStartPos] = entityChar.charValue();
          }
        }
      }
      else {
        entityBuf[entityBufLen] = (char) character;
        entityBufLen++;
      }
    }
    int i = entityStartPos + 1;
    while (txtPos < txtEnd) {
      txtBuf[i++] = txtBuf[txtPos++];
    }
    txtPos = entityStartPos + 1;
    txtEnd = txtPos;
  }

  private QName createQName(Namespace defaultNS, int startPos, int nsPos, int endPos) throws IOException {
    Namespace ns;
    if (nsPos < 0) {
      ns = defaultNS;
      if (ns == null) {
        parseError("Default namespace not defined");
      }
    }
    else {
      ns = findNamespace(startPos, nsPos);
      if (ns == null) {
        parseError("Unknown namespace prefix: "+new String(txtBuf, startPos, nsPos-startPos));
      }
      startPos = nsPos+1;
    }
    return ns.getQName(txtBuf, startPos, endPos);
  }

  private int parseQName() throws IOException {
    char c;
    int nsPos = -1;
    do {
      c = read(); // we do not expect to see entities here

      //--- tagName contains namespace prefix
      if (c == ':') {
        nsPos = txtPos-1;
      }
    }
    while ((c >= 'a' && c <= 'z')
           || (c >= 'A' && c <= 'Z')
           || (c >= '0' && c <= '9')
           || c == '_'
           || c == '-'
           || c == ':'
           || c == '.');

    return nsPos;
  }

  private Object createValue(Namespace defaultNS, QName attrName, SimpleType type, int startPos, int endPos) throws IOException {
    Namespace ns;

    if (type == null) {
      parseError( "Attribute type must not be null." );
    }

    if (type instanceof QNameType) {
      ns = defaultNS;
      int nsPos = -1;
      for (int i = startPos; i < endPos; i++) {
        if (txtBuf[i] == ':') {
          nsPos = i;
          break;
        }
      }
      if (nsPos < 0) {
        if (QNameType.getNCName().isAssignableFrom( type )) {
          return documentNamespace.getQName( txtBuf, startPos, endPos );
        }
        else {
          return ns.getQName( txtBuf, startPos, endPos );
        }
      }
      else if (QNameType.getNCName().isAssignableFrom( type )) {
        parseError( "Found ':' in NCName: " + new String( txtBuf, startPos, nsPos ) );
        return null;
      }
      else {
        ns = findNamespace(startPos, nsPos);
        if (ns == null) {
          parseError("Unknown namespace: " + new String(txtBuf, startPos, nsPos));
        }
        return ns.getQName(txtBuf, nsPos+1, endPos);
      }
    }
    else if (type instanceof StringType) {
      return type.createValue( new String(txtBuf, startPos, endPos-startPos ), null );
    }
    else if (type instanceof IntegerType) {
      if (endPos > startPos) {
        //TODO String-Erzeugung vermeiden
        return type.createValue(new String(txtBuf, startPos, endPos-startPos), null );
      }
      else {
        return null;
      }
    }
    else if (type instanceof BooleanType) {
      if (endPos > startPos) {
        if (compareString("true", startPos, endPos)) {
          return BooleanType.TRUE;
        }
        else {
          return BooleanType.FALSE;
        }
      }
      else {
        return null;
      }
    }
    else if (type instanceof DateTimeType) {
      //TODO String-Erzeugung vermeiden
      return type.createValue(new String(txtBuf, startPos, endPos-startPos), null );
    }
    else if (type instanceof DecimalType) {
      //TODO String-Erzeugung vermeiden
      return type.createValue(new String(txtBuf, startPos, endPos-startPos), null );
    }
    else if (type instanceof BinaryType) {
      //TODO String-Erzeugung vermeiden
      return type.createValue(new String(txtBuf, startPos, endPos-startPos), null );
    }
    else if (type instanceof DurationType) {
      //TODO String-Erzeugung vermeiden
      return type.createValue(new String(txtBuf, startPos, endPos-startPos), null );
    }
    else {
      SimpleType baseType = (SimpleType) type.getBaseType();
      if (baseType == null) {
        parseError("Unsupported SimpleType: "+type.name().getName());
        return null;
      }
      else {
        return createValue(defaultNS, attrName, baseType, startPos, endPos);
      }
    }
  }

  /**
   * Parse a value between "
   * Exits when having read trailing "
   */
  private void parseValue(char closeChar) throws IOException {
    char c;
    do {
      c = read();
      if (c == '&') {
        readEntity();
      }
    }
    while (c != closeChar);
  }

  /**
   * Reads whitespace and increments "line" if CR is read.
   * Starts reading with current char
   * @return the first character not beeing white space
   * @throws IOException
   */
  private char readWhitespace() throws IOException {
    char c = txtBuf[txtPos-1];
    while (c <= ' ') {
      if (c == '\n') line++;
      c = read();
    }
    return c;
  }

  private Key createAttributes( Object[] attributes, Namespace tagNS, ComplexType nodeType ) throws IOException {
    QName        attrName;
    int          attrIndex;
    SimpleType   attrType;
    Namespace    defaultValueNS;
    AnyAttribute anyAttr;
    int          keyAttrCount = nodeType.keyAttributeCount();
    boolean      simpleKey = false;
    Object[]     keyAttrs = null;
    Key          key = null;

    if (keyAttrCount == 1) {
      simpleKey = true;
    }
    else if (keyAttrCount > 0) {
      keyAttrs = new Object[keyAttrCount];
    }

    if (attributes == null) {
      if (keyAttrCount > 0) {
        throw new IllegalArgumentException("Key attributes must be defined for type="+nodeType.name());
      }
      attributes = null;
    }
    else {
      int attrDefCount = attributes.length;
      defaultValueNS = getDefaultNamespace();
      if (defaultValueNS == null) defaultValueNS = nodeType.name().getNamespace();
      for (int i = 0; i < attrDefCount; i++) {
        attributes[i] = UNDEFINED;
      }
      for (int i = 0; i < this.attrCount; i++) {
        attrName = createQName(tagNS, attrStart[i], attrNsPos[i], attrEnd[i]);
        if (DEBUG) System.out.print(attrName.getName()+ " ");
        attrIndex = nodeType.indexOfAttribute(attrName);
        //----- anyAttribute
        if (attrIndex < 0) {
          attrType = AnyAttribute.findType(attrName);
          if (attrType == null) {
            parseError( "Type not found for any attribute name=" + attrName + " in nodeType [" + nodeType.name() + "]" );
          }
          if (this.anyAttribs == null) {
            this.anyAttribs = new Hashtable();
          }
          this.anyAttribs.put(attrName, createValue(defaultValueNS, attrName, attrType, valueStart[i], valueEnd[i]));
        }
        else {
          attrType = nodeType.getAttributeType(attrIndex);
          Object value = createValue(defaultValueNS, attrName, attrType, valueStart[i], valueEnd[i]);
          int keyIndex = nodeType.getKeyIndex(attrName);
          //----- key attribute
          if (keyIndex >= 0) {
            attributes[attrIndex] = Key.KEY_ATTR;
            if (simpleKey) {
              if (attrType instanceof QNameType) {
                key = (QName) value;
              }
              else {
                key = new SimpleKey( value );
              }
            }
            else {
              keyAttrs[keyIndex] = value;
            }
          }
          //----- normal attribute
          else {
            attributes[attrIndex] = value;
          }
          if (attrName.getName().equals(ATTR_TARGETNAMESPACE)) {
            this.documentNamespace = Namespace.getInstance((String) value);
          }
        }
      }
      for (int i = 0; i < attrDefCount; i++) {
        if (attributes[i] == UNDEFINED) {
          attributes[i] = nodeType.getAttributeDefault(i);
        }
      }
    }

    //----- build combined key
    if (keyAttrs != null) {
      key = new CombinedKey( keyAttrs );
    }
    return key;
  }

  private Namespace storeNamespace( String nsName, String prefix ) {
    Namespace ns = Namespace.getInstance(nsName);
    storeNamespace( ns, prefix );
    return ns;
  }

  private void storeNamespace( Namespace ns, String prefix ) {
    if (this.namespaces.getOwner() != null) {
      namespaces = new NamespaceTable( namespaces );
    }
    namespaces.addNamespace( ns, prefix );
    if (ns == XSIFactory.NAMESPACE) {
      this.xsiPrefix = prefix;
      ModelFactoryTable.getInstance().getOrSetFactory( XSIFactory.NAMESPACE, XSIFactory.class );
    }
  }

  private Namespace processNsDecl(int attrIndex) {
    String nsName, prefix;

    if (attrNsPos[attrIndex] < 0) {
      if (compareString("xmlns", attrStart[attrIndex], attrEnd[attrIndex])) {
        nsName = new String(txtBuf, valueStart[attrIndex], valueEnd[attrIndex]-valueStart[attrIndex]);
        return storeNamespace( nsName, "" );
      }
    }
    else {
      if (compareString("xmlns", attrStart[attrIndex], attrNsPos[attrIndex])) {
        prefix = new String(txtBuf, attrNsPos[attrIndex]+1, attrEnd[attrIndex]-(attrNsPos[attrIndex]+1));
        nsName = new String(txtBuf, valueStart[attrIndex], valueEnd[attrIndex]-valueStart[attrIndex]);
        return storeNamespace(nsName, prefix);
      }
    }
    return null;
  }

  /**
   * Check if attribute is from XML Schema instance namespace. We have to process these
   * attributes here before processing tag and attributes.
   * @param attrIndex index of the attribute to check
   * @return true if attribute is from XML Schema instance namespace
   */
  private boolean prcessXSI( int attrIndex ) throws IOException {
    if ((this.xsiPrefix != null) && (attrNsPos[attrIndex] > 0)) {
       if (compareString( xsiPrefix, attrStart[attrIndex], attrNsPos[attrIndex])) {
         if (compareString( "type", attrNsPos[attrIndex]+1, attrEnd[attrIndex])) {
           QName typeName = (QName) createValue( getDefaultNamespace(), XSIFactory.ID_TYPE, QNameType.getQName(),
                                                 valueStart[attrIndex], valueEnd[attrIndex] );
           this.xsiType = ModelFactoryTable.getInstance().getFactory( typeName.getNamespace() ).getType( typeName );
           return true;
         }
       }
    }
    return false;
  }


  private int[] expandIntArray(int[] oldArray, int newSize) {
    int[] newArray = new int[newSize];
    System.arraycopy(oldArray, 0, newArray, 0, oldArray.length);
    return newArray;
  }

  private void checkAttrSizes() {
    if (attrCount >= attrStart.length) {
      int newSize = attrCount * 2;
      attrStart = expandIntArray(attrStart, newSize);
      attrNsPos = expandIntArray(attrNsPos, newSize);
      attrEnd = expandIntArray(attrEnd, newSize);
      valueStart = expandIntArray(valueStart, newSize);
      valueEnd = expandIntArray(valueEnd, newSize);
    }
  }

  private char parseAttributes( char endChar) throws IOException {
    attrCount = 0;
    char c = readWhitespace();

    while ((c != '>') && (c != endChar)) {
      checkAttrSizes();
      attrStart[attrCount] = txtPos-1;
      attrNsPos[attrCount] = parseQName(); // AnyAttribute haben einen NS-Prefix, andere nicht!
      attrEnd[attrCount] = txtPos-1;

      c = readWhitespace();
      if (c != '=') {
        parseError("Expected '=' but found '"+c+"'");
      }
      c = read();
      c = readWhitespace();
      if (c != '"') {
        parseError("Expected '\"' but found '"+c+"'");
      }
      valueStart[attrCount] = txtPos;
      parseValue('"');
      valueEnd[attrCount] = txtPos-1;

      c = txtBuf[txtPos-1];
      if (c != '"') {
        parseError("Expected '\"' but found '"+c+"'");
      }
      c = read();
      c = readWhitespace();
      Namespace ns = processNsDecl( attrCount );
      if (ns == null) {
        if (!prcessXSI(attrCount)) {
          attrCount++;
        }
      }
    }
    return c;
  }

  private ChildList readChildren( QName parentRelation, ComplexType parentType ) throws IOException {
    ChildList children = new ChildList();
    QName relation;
    do {
      // read character content
      char c;
      int valStart;
      Object value;
      Model node;

      if (DEBUG) System.out.print( ">" );
      if (parentType.isMixed()) {
        valStart = txtPos;
        c = read();
        c = readWhitespace();
        if (c == '<') {
          back( 1 );
        }
        else {
          parseValue( '<' );
          value = createValue( getDefaultNamespace(), null, StringType.getDefString(), valStart, txtPos - 1 );
          if (value != null) {
            ModelFactory nodeFactory = ModelFactoryTable.getInstance().getFactory( parentType.name().getNamespace() );
            if (nodeFactory == null) {
              parseError( "No factory found for node tag=" + parentRelation + ", namespace=" + parentType.name().getNamespace() );
            }
            node = nodeFactory.createInstance( StringType.getDefString(), value );
            if (node == null) {
              parseError( "Could not create character content node for typeID=" + parentType.name() );
            }
            children.add( ComplexType.CDATA, node );
          }
          else {
            parseError( "Error in mixed content for typeID=" + parentType.name() );
          }
          back(1);
        }
      }

      relation = nextTag( parentRelation, parentType, children );
    }
    while (relation != null);
    return children;
  }

  private void readHeader() throws IOException {
    char c;
    int startPos = this.txtPos-1;
    do {
      c = read(); // we do not expect to see entities here
    }
    while ((c >= 'a' && c <= 'z')
        || (c >= 'A' && c <= 'Z')
        || (c >= '0' && c <= '9')
        || c == '_'
        || c == '-'
        || c == ':'
        || c == '.');

    String tag = new String( txtBuf, startPos, this.txtPos-startPos-1 );
    if (!("?xml".equals(tag))) {
      throw new IOException("Header must start with '?xml'");
    }
    c = parseAttributes( '?' );
    for (int i = 0; i < this.attrCount; i++) {
      String attrName = new String(txtBuf, attrStart[i], attrEnd[i]-attrStart[i]);
      if ("encoding".equals(attrName)) {
        encoding = new String(txtBuf, valueStart[i], valueEnd[i]-valueStart[i]);
      }
    }
    while (c != '>') {
      c = read(); // we do not expect to see entities here
    }

  }

  private void readDocType() throws IOException {
    char c;
    do {
      c = read(); // we do not expect to see entities here
    } while (c != '>');
  }

  private void readProcessingInstruction() throws IOException {
    char c, c1;
    c = read();
    do {
      c1 = c;
      c = read(); // we do not expect ot see entities here
    } while ( !((c1=='?') && (c == '>')) );
  }

  private void readComment() throws IOException {
    char c, c1, c2;
    c = read(); // we do not convert entities in comment
    if (c != '-') {
      if (c=='D') {
        readDocType();
      }
      else if (c=='?') {
        readProcessingInstruction();
      }
      else {
        parseError("Expected '-', 'D' or '?' but found '"+c+"'");
      }
      return;
    }
    c = read();
    if (c != '-') {
      parseError("Expected '-' but found '"+c+"'");
    }
    c2 = read();
    c = read();
    do {
      c1 = c2;
      c2 = c;
      c = read(); // we do not convert entities in comments
    }
    while ( !((c1 == '-') && (c2 == '-') && (c == '>')) );
  }

  /**
   * Reads the next tag and creates the corresponding Model or returns null if
   * tag is a closing tag. Adds created models to parentChilds
   * @param  parentType the parent type for the new Model
   * @return the relation or null
   * @throws IOException
   */
  private QName nextTag(QName parentRelation, ComplexType parentType, ChildList parentChilds) throws IOException {
    char         c;
    QName        relation;
    Type         type;
    ComplexType  nodeType;
    Object[]     attributes;
    ChildList    children;
    int          tagStart, tagNsPos, tagEnd, valStart;
    boolean      foundTag = false;
    Object       value;
    Hashtable    anyAttribs;
    Model        node;

    // reset txtBuf to start of tag
    int i = 0;
    while (txtPos < txtEnd) {
      txtBuf[i++] = txtBuf[txtPos++];
    }
    txtPos = 0;
    txtEnd = i;

    //--- Loop until start tag begins
    while (!foundTag) {
      do {
        c = read();  // we do not expect entities here
      }
      while (c != '<');

      //--- Check for closing tag
      c = read();
      if (c == '/') {
        processCloseTag(parentRelation);
        return null;
      }
      else if (c == '?') {
        readHeader();
      }
      else if (c == '!') {
        readComment();
      }
      else {
        foundTag = true;
      }
    }

    //--- If file hat no header set up InputStreamReader now
    if (!continuous && (this.reader == null)) {
      if (encoding == null) setInput(new InputStreamReader(dataStream));
      else {
        try {
          setInput(new InputStreamReader(dataStream, encoding));
        }
        catch (Exception ex) {
          // catching UnsupportedEncodingException would be better, but CrEme just throws IllegalArgumentException
          Logger.warn("XmlReader: Exception (perhaps unsuported encoding: "+encoding+"), msg="+ex.getMessage());
          setInput(new InputStreamReader(dataStream));
        }
      }
    }    

    //--- Parse tag and attributes
    Integer tagID = Integer.valueOf( this.tempID++ );
    xsiType = null;
    tagStart = txtPos - 1;
    tagNsPos = parseQName();
    tagEnd = txtPos - 1;
    c = parseAttributes( '/' );
    if (namespaces.getOwner() == null) {
      // Mark new namespaceTable as created for this tag: Owning model is created too late
      namespaces.setOwner( tagID );
    }

    Namespace tagNsDef = getDefaultNamespace();
    if ((tagNsDef == null) && (parentRelation != null)) {
      tagNsDef = parentRelation.getNamespace();
    }
    relation = createQName(tagNsDef, tagStart, tagNsPos, tagEnd);
    if (parentType == null) {
      if (documentNamespace == null) {
        // namespace of the root tag defines document namespace
        documentNamespace = relation.getNamespace();
      }
      ModelFactory factory = ModelFactoryTable.getInstance().getFactory( relation.getNamespace() );
      if (factory == null) {
        parseError("Factory not found (perhaps missing in system.properties) for Namespace: "+relation.getNamespace());
      }
      Relation rel = factory.getRootRelation(relation);
      if (rel == null) {
        parseError( "Relation not found: " + relation, parentType );
      }
      type = rel.getChildType();
    }
    else {
      type = parentType.getChildType(relation);
    }
    if (DEBUG) {
      for (int sp = 0; sp < depth; sp++) System.out.print("  ");
      System.out.print("<"+relation+ " ");
    }
    if (type == null) {
      parseError( "Type not found, relation=" + relation, parentType );
    }

    if (xsiType != null) {
      if (!type.isAssignableFrom( xsiType )) {
        parseError( "xsi:type=" + xsiType.name() + " does not inherit from required type=" + type, parentType );
      }
      type = xsiType;
    }

    ModelFactory nodeFactory = ModelFactoryTable.getInstance().getFactory( type.name().getNamespace() );
    if (nodeFactory == null) {
      parseError( "No factory found for node tag=" + relation + ", namespace=" + type.name().getNamespace(), parentType );
    }

    if (type instanceof SimpleType) {
      if (c != '>') {
        if (c == ' ') c = readWhitespace();
        else if (c != '/') parseError("Expected '>' but found '"+c+"'");
      }
      if (c == '/') {
        c = read();
        if (c != '>') {
          parseError("Expected '>' but found '"+c+"'");
        }
        if (DEBUG) System.out.print("/>");
        value = null;
      }
      else {
        if (DEBUG) System.out.print(">");
        valStart = txtPos;
        parseValue('<');
        value = createValue(getDefaultNamespace(), null, (SimpleType) type, valStart, txtPos-1);
        c = read();
        if (c != '/') {
          parseError("Expected '/' but found '"+c+"'");
        }
        processCloseTag(relation);
      }
      node = nodeFactory.createInstance((SimpleType) type, value);
      if (node == null) {
        parseError( "Could not create node for relation=" + relation + " typeID=" + type.name(), parentType );
      }
      if (namespaces.getOwner() == tagID) {
        node.ownNamespaceTable( namespaces );
        namespaces = namespaces.getParent();
      }
      parentChilds.add(relation, node);
      return relation;
    }
    else {
      //--- Read attributes
      nodeType = (ComplexType) type;
      this.anyAttribs = null;
      attributes = nodeType.createAttributeArray();
      Key key = createAttributes(attributes, nodeType.name().getNamespace(), nodeType);
      anyAttribs = this.anyAttribs;

      //--- Read children
      if (c == '/') {
        c = read();
        if (c != '>') {
          parseError("Expected '>' but found '"+c+"'");
        }
        if (DEBUG) System.out.println("/>");
        value = null;
        children = null;
      }
      else {
        if (DEBUG) System.out.println(">");
        if (nodeType.isMixed()) {
          this.depth++;
          value = null;
          children = readChildren( relation, nodeType );
          this.depth--;
        }
        else {
          children = null;
          int contentStart = txtPos;
          c = read();
          c = readWhitespace();
          if (c == '<') {
            back( 1 );
            this.depth++;
            value = null;
            children = readChildren( relation, nodeType );
            this.depth--;
          }
          else {
            parseValue( '<' );
            value = createValue( getDefaultNamespace(), null, type.getContentType(), contentStart, txtPos - 1 );
          }
        }
      }

      //--- Build node
      node = null;
      try {
        node = nodeFactory.createInstance(key, nodeType, attributes, anyAttribs, children );
        if (value != null) {
          node.setContent( value );
        }
      }
      catch (Exception ex) {
        parseError( "Could not create node for relation=" + relation + " typeID=" + type.name() + " ex=" + ex.getMessage(), parentType, ex );
      }
      if (node == null) {
        parseError( "Could not create node for relation=" + relation + " typeID=" + type.name(), parentType );
      }
      if (namespaces.getOwner() == tagID) {
        node.ownNamespaceTable( namespaces );
        namespaces = namespaces.getParent();
      }
      parentChilds.add( relation, node );
      return relation;
    }
  }

  private void processCloseTag(QName parentRelation) throws IOException {
    int tagStart;
    int tagNsPos;
    int tagEnd;
    QName relation;
    tagStart = txtPos;
    tagNsPos = parseQName();
    tagEnd = txtPos - 1;
    Namespace tagNsDef = getDefaultNamespace();
    if ((tagNsDef == null) && (parentRelation != null)) {
      tagNsDef = parentRelation.getNamespace();
    }
    relation = createQName(tagNsDef, tagStart, tagNsPos, tagEnd);
    if (relation != parentRelation) {
      parseError( "Closing tag '" + relation + "' does not match opening tag '" + parentRelation + "'" );
    }
    if (DEBUG) {
      for (int sp = 0; sp < this.depth-1; sp++) System.out.print("  ");
      System.out.println("</"+relation+">");
    }
  }

  private void dumpInput() throws IOException {
    char c;
    int max = MAX_DUMP_CHARS;
    try {
      do {
        c = read();
        System.out.print(c);
        max--;
      }
      while ((c > 0) && (max > 0));
      if (c > 0) {
        System.out.print( "[...]" );
      }
      System.out.println();
    }
    catch (Exception ex) {
      // ignore further exceptions
    }
  }


  /**
   * Loads data from dataResource
   * Note: dataStream is NOT closed by loadData!
   *
   * @param reader 
   * @param progressListener
   * @throws java.io.IOException
   */
  public synchronized void includeData( Model parentNode, Reader reader, ProgressListener progressListener )
      throws IOException {
    this.progressListener = progressListener;

    //----- open stream and read until first start tag
    if (reader == null) {
      throw new IllegalArgumentException("loadData: reader must not be null!");
    }

    this.modelNameStack.clear();
    this.namespaces = parentNode.namespaceTable();
    this.depth = 0;
    this.tempID = 1;

    setInput( reader );
    this.srcCount = 0;

    ComplexType parentType = (ComplexType) parentNode.type();
    ChildList children = new ChildList();
    QName tagID = nextTag(null, parentType, children);

    //close data stream
    this.reader = null;

    if (children.size() != 1) {
      throw new IllegalArgumentException("XML file must have exactly 1 root element");
    }
    parentNode.add( tagID, children.getFirstChild( null ) );
  }

  /**
    * Loads data from dataResource, calls loadData(dataStream,progressListener,false)
    * Note: dataStream is NOT closed by loadData!
    *
    * @param dataStream the fully qualified data resource name
    * @throws java.io.IOException
    */
  public Model loadData( InputStream dataStream, ProgressListener progressListener )
  throws IOException {
    return loadData( dataStream, progressListener, false, null );
  }

  /**
   * Loads data from dataResource, calls loadData(dataStream,progressListener,false)
   * Note: dataStream is NOT closed by loadData!
   *
   * @param dataStream the fully qualified data resource name
   * @throws java.io.IOException
   */
  public Model loadData( Reader dataStream, ProgressListener progressListener, Namespace defaultNS )
      throws IOException {
    setInput( dataStream );
    return loadData( null, progressListener, false, defaultNS );
  }

  public synchronized Model loadData( InputStream dataStream, ProgressListener progressListener, boolean continuous ) throws IOException {
    return loadData( dataStream, progressListener, continuous, null );
  }

  /**
   * Loads data from dataResource
   * Note: dataStream is NOT closed by loadData!
   *
   * @param dataStream the fully qualified data resource name
   * @param progressListener
   * @param continuous       if true no wrapping reader is created to avoid reading additional characters from
   *                         dataStream after end of XML document
   * @throws java.io.IOException
   */
  public synchronized Model loadData( InputStream dataStream, ProgressListener progressListener, boolean continuous, Namespace defaultNS )
  throws IOException {
    this.progressListener = progressListener;
    this.continuous = continuous;
//    DefaultComplexType rootType = (DefaultComplexType) root.type();
    ChildList children;

    //----- open stream and read until first start tag
    if (DEBUG) Logger.debug("XmlReader.loadData URL=" + dataStream);
    if ((dataStream == null) && (reader == null)) {
      throw new IllegalArgumentException("loadData: either dataStream or reader (see setInput) must not be null!");
    }

    this.modelNameStack.clear();
    this.namespaces = new NamespaceTable();
    if (ModelFactoryTable.getInstance().getFactory( XmlFactory.NAMESPACE ) == null) {
       new XmlFactory();
    }
    storeNamespace( XmlFactory.NAMESPACE, "xml" ); // implicit Namespace prefix "xml", e.g. for attribute "xml:lang"
    this.depth = 0;
    if (defaultNS != null) {
      storeNamespace( defaultNS, "" );
    }
    this.tempID = 1;

    this.dataStream = dataStream;
    this.srcCount = 0;

    children = new ChildList();
    nextTag(null, null, children);

    //close data stream
    this.reader = null;

    if (children.size() != 1) {
      throw new IllegalArgumentException("XML file must have exactly 1 root element");
    }
    Model rootNode = children.getFirstChild(null);
    return rootNode;
  }

  public static Model loadData( Storage storage, String fileName ) throws IOException {
    InputStream is = null;
    try {
      is = storage.read( fileName );
      if (is == null) {
        throw new IOException( "Data resource not found, path=" + storage.getPath( fileName ) );
      }
    }
    catch (IOException ex) {
      throw new IOException( "Data resource not found, path=" + storage.getPath( fileName ) );
    }
     try {
      XmlReader reader = new XmlReader();
      return reader.loadData(is, null);
    }
    finally {
      StreamHelper.close( is );
    }
  }

  public static Model loadData( String path ) throws IOException {
    String dir = null;

    int index = path.lastIndexOf("/");
    int indexBS = path.lastIndexOf("\\");
    if (indexBS > index) {
      index = indexBS;
    }

    if (index >= 0) {
      dir = path.substring(0, index);
      path = path.substring(index+1);
    }
    Storage storage = SystemManager.getInstance().getStorage(dir);
    return loadData( storage, path );
  }
}
