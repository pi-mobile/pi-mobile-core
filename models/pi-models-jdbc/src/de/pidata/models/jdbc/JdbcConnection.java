/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.jdbc;

import de.pidata.log.Logger;
import de.pidata.models.sql.DBConnection;
import de.pidata.models.sql.DBCursor;
import de.pidata.models.tree.Context;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.qnames.QName;
import de.pidata.system.base.SystemManager;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Properties;

public abstract class JdbcConnection extends DBConnection {

  private String db_driver;
  private String db_url;
  private String db_user;
  private String db_pass;
  private Connection jdbcConn;

  protected JdbcConnection( int reconnectMinutes ) throws Exception {
    super( reconnectMinutes );
  }

/*  protected JdbcConnection( Connection jdbcConn, int reconnectMinutes ) {
    super( reconnectMinutes );
    this.jdbcConn = jdbcConn;
  }
*/
  /**
   *
   * @param context
   * @param propertySection the propertySection to use for readign properties, e.g. "database"
   * @throws Exception
   */
  public void connect( Context context, String propertySection ) throws Exception {
    getParameters( propertySection );
    this.jdbcConn = jdbcConnect( this.db_driver, this.db_url, this.db_user, this.db_pass );
    lastAccessTime = System.currentTimeMillis();
  }

  public static Connection jdbcConnect( String db_driver, String db_url, String db_user, String db_pass ) throws ClassNotFoundException, SQLException {
    Class.forName(db_driver);

    Properties dbProps = new Properties();
    //wegen jdk 1.1.8 kompatibilitaet, statt setProperty... wird put
    dbProps.put("user", db_user);
    dbProps.put("password", db_pass);
    Logger.info("DataAccess database.url = " + db_url);

    Connection jdbcConn = DriverManager.getConnection(db_url, dbProps);
    jdbcConn.setAutoCommit(false);
    DatabaseMetaData md = jdbcConn.getMetaData();
    String dbName = md.getDatabaseProductName();
    Logger.info("Database product name = " + dbName);

    return jdbcConn;
  }

  private void getParameters( String propertySection ) throws Exception {
    SystemManager sysMan = SystemManager.getInstance();
    db_driver = sysMan.getProperty( propertySection+".drivername", null );
    if (db_driver == null) throw new IllegalArgumentException( propertySection+".drivername missing in both property files" );
    db_url = sysMan.getProperty( propertySection+".url", null );
    if (db_url == null) throw new IllegalArgumentException( propertySection+".url missing in both property files" );
    db_user = sysMan.getProperty( propertySection+".user", null );
    db_pass = sysMan.getProperty( propertySection+".pass", null );
  }

  public Connection getConnection() {
    if (reconnectMillis > 0) {
      if ((System.currentTimeMillis() - reconnectMillis) > this.lastAccessTime) {
        reconnect();
      }
      this.lastAccessTime = System.currentTimeMillis();
    }
    return jdbcConn;
  }

  /**
   * Closes current jdbc connection and tries to reconnect with same connection parameters
   * @return true if reconnect was successful
   */
  public boolean reconnect() {
    // TODO: First commit an open transaction, or maybe we can copy SavePoints between transactions?
    close();
    try {
      jdbcConn = jdbcConnect( this.db_driver, this.db_url, this.db_user, this.db_pass );
      return true;
    }
    catch (Exception ex) {
      Logger.error( "Error while JDBC reconnect", ex );
      return false;
    }
  }
  protected Statement createStatement() throws IOException {
    Statement stmt = null;
    try {
      stmt = getConnection().createStatement();
//TODO      if (maxCountResult < Integer.MAX_VALUE) {
//        stmt.setMaxRows(maxCountResult+1);
//      }
    }
    catch (SQLException ex) {
      Logger.error("Can't create JDBC statement", ex);
      throw new IOException("Can't create JDBC statement, message="+ex.getMessage());
    }
    return stmt;
  }


  public DBCursor query( String sql ) throws IOException {
    return new JdbcCursor( this, sql );
  }


  public void execute( String sql ) throws IOException {
    Statement stmt = null;
    try {
      stmt = createStatement();
      stmt.execute( sql );
    }
    catch (Exception ex) {
      Logger.error( "Error executing statement, sql="+sql, ex );
      throw new IOException( "Error executing sql, msg="+ex.getMessage() );
    }
    finally {
      try {
        stmt.close();
      }
      catch (SQLException e) {
        Logger.error( "Error closing statement", e );
      }
    }
  }

  /**
   * Executes the commit on the jdbc connection.
   * @throws IOException
   */
  public void commit() throws IOException {
    try {
      getConnection().commit();
    }
    catch (Exception ex) {
      Logger.error("Can't commit", ex);
      throw new IOException("Can't commit, message="+ex.getMessage());
    }
  }

  /**
   * Executes the rollback on the jdbc connection and catches possible SQLException in
   * empty catch block.
   */
  public void rollback() {
    try {
      Connection con = getConnection();
      if (con != null) {
        con.rollback();
      }
    }
    catch (Exception e) {
      Logger.error("Can't rollback", e);
    }
  }

  /**
   * Closes given ResultSet and Statement if not null, and catches
   * possible SQLExceptions (empty catch block)
   * @param stmt       Statement to close or null
   * @param resultSet  ResultSet to close or null
   */
  public void closeResources( Statement stmt, ResultSet resultSet ) {
    if (resultSet != null) {
      try {
        resultSet.close();
      }
      catch (SQLException e) {
        //do nothing
      }
    }
    if (stmt != null) {
      try {
        stmt.close();
      }
      catch (SQLException e) {
        //do nothing
      }
    }
  }

  public void close() {
    if (jdbcConn != null) {
      try {
        jdbcConn.close();
      }
      catch (SQLException e) {
        //do nothing
      }
      jdbcConn = null;
    }
  }

  /**
  * Convert a column in a result set to the internal representation.
  *
   * @param dateTimeType
   * @param defaultValue
   * @param resultSet
   * @param columnName
   * @return
  * @throws SQLException
  */
  public Object getDateTime( DateTimeType dateTimeType, Object defaultValue, ResultSet resultSet, String columnName) throws SQLException {
    Object result = null;

    QName type = dateTimeType.getType();
    if (type == DateTimeType.TYPE_DATE) {
      Date date = resultSet.getDate(columnName);
      result = new DateObject( DateTimeType.TYPE_DATE, date.getTime());
    }
    else if (type == DateTimeType.TYPE_DATETIME ) {
      Timestamp dateTime = resultSet.getTimestamp(columnName);
      result = new DateObject( DateTimeType.TYPE_DATETIME, dateTime.getTime());
    }
    else if (type == DateTimeType.TYPE_TIME ) {
      Time time = resultSet.getTime(columnName);
      result = new DateObject( DateTimeType.TYPE_TIME, time.getTime());
    }
    return result;
  }

  /**
   * Returns true if table named tableName exists
   *
   * @param tableName teh tabel to check
   * @return true if tableName exists
   */
  public boolean existsTable( String tableName ) throws IOException {
    Statement stmt = null;
    ResultSet result = null;
    String[] tableTypes = new String[1];
    tableTypes[0] = "TABLE";
    try {
      stmt = getConnection().createStatement();
      // Tut nicht: ResultSet result = getConnection().getMetaData().getTables( null, null, tableName, tableTypes );
      result = stmt.executeQuery( "SELECT count(*) FROM "+tableName );
      return result.next();
    }
    catch (SQLException e) {
      return false;
    }
    finally {
      closeResources( stmt, result );
    }
  }
}
