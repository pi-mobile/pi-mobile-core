/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.jdbc;

import de.pidata.models.sql.SQLTableSequence;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.simple.BooleanType;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.qnames.QName;
import de.pidata.system.base.NumberSequence;

import java.io.IOException;
import java.sql.*;

public class SQLiteConnection extends JdbcConnection {

  public SQLiteConnection() throws Exception {
    super( -1 );
    MAX_STRLEN = 4000;
  }

  /**
   * Return the string value for the given object by using his simpleType
   *
   * @param object     the object to convert
   * @param simpleType the type of the object
   * @return String for SQL-Statment
   */
  public String convertAttribute( Object object, SimpleType simpleType ) throws IOException {
    if (simpleType == null) {
      throw new IllegalArgumentException("Arguments simpleType is wrong: simpleType == null");
    }
    if (object != null) {
      if (simpleType instanceof BooleanType) {
        if (((Boolean) object).booleanValue()) return "1";
        else return "0";
      }
    }
    return super.convertAttribute( object, simpleType );
  }

  /**
   * Returns the database specific type for an internal date/time type
   *
   *
   * @param dateTimeType@return the database specific type
   */
  public String getDateTimeType( DateTimeType dateTimeType ) {
    QName type = dateTimeType.getType();
    if (type == DateTimeType.TYPE_DATE) {
      return "DATE";
    }
    else if (type == DateTimeType.TYPE_TIME) {
      return "TIME";
    }
    else if (type == DateTimeType.TYPE_DATETIME) {
      return "DATETIME";
    }
    else {
      throw new IllegalArgumentException("not implemented base type for attribute [" + dateTimeType.name() + "]");
    }

  }

  /**
   * Convert a column in a result set to the internal representation.
   *
   *
   * @param dateTimeType
   * @param defaultValue
   * @param resultSet
   * @param columnName
   * @return
   * @throws java.sql.SQLException
   */
  public Object getDateTime( DateTimeType dateTimeType, Object defaultValue, ResultSet resultSet, String columnName ) throws SQLException {
    Timestamp timeStamp = resultSet.getTimestamp( columnName );
    if (timeStamp == null) return null;
    else return new DateObject( dateTimeType.getType(), timeStamp.getTime() );
  }

  public String dateTime2SQL( DateTimeType dateTimeType, DateObject value ) {
    if (value == null) {
      return "null";
    }
    else {
      long timeStamp = value.getTime();
      return Long.toString( timeStamp );
    }
  }

  /**
   * Creates a new Sequence
   *
   * @param name the sequence's name
   * @param min  the min (starting) value for the new sequence
   * @param max  the max value for the new sequence
   * @return the new sequence object
   */
  public NumberSequence createSequence( String name, long min, long max ) throws IOException {
    return new SQLTableSequence( name, this, min, max );
  }

  /**
   * Returns a sequence object for a sequence existing in database
   *
   * @param name the sequence's name
   * @return the sequence object
   */
  public NumberSequence getSequence( String name ) throws IOException {
    return new SQLTableSequence( name, this );
  }

  /**
   * Add a limit constraint to the sql string.
   *
   * @param sql
   * @param maxCountResult
   */
  public String addLimit( String sql, int maxCountResult ) {
    return sql + " LIMIT "+maxCountResult;
  }
}
