/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.jdbc;

import de.pidata.models.types.SimpleType;
import de.pidata.models.types.simple.BooleanType;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.qnames.QName;
import de.pidata.system.base.NumberSequence;

import java.io.IOException;

public class JavaDBConnection extends JdbcConnection {

  public JavaDBConnection() throws Exception {
    super( -1 );
    MAX_STRLEN = 4000;
  }

  public String getDateTimeType( DateTimeType dateTimeType ) {
    QName type = dateTimeType.getType();
    if (type == DateTimeType.TYPE_DATE) {
      return "DATE";
    }
    else if (type == DateTimeType.TYPE_TIME) {
      return "TIMESTAMP(3)";
    }
    else if (type == DateTimeType.TYPE_DATETIME) {
      return "TIMESTAMP(3)";
    }
    else {
      throw new IllegalArgumentException("not implemented base type for attribute [" + dateTimeType.name() + "]");
    }
  }

  /**
   * Return the string value for the given object by using his simpleType
   *
   * @param object     the object to convert
   * @param simpleType the type of the object
   * @return String for SQL-Statment
   */
  public String convertAttribute( Object object, SimpleType simpleType ) throws IOException {
    if (simpleType == null) {
      throw new IllegalArgumentException("Arguments simpleType is wrong: simpleType == null");
    }
    if (object != null) {
      if (simpleType instanceof BooleanType) {
        if (((Boolean) object).booleanValue()) return "'1'";
        else return "'0'";
      }
    }
    return super.convertAttribute( object, simpleType );
  }

  /**
   * Convert the internal representation of a date/time value into the database specific
   * representaion. If using a String it must be surrounded by single quotes.
   *
   * @param dateTimeType
   * @param value   the date value
   * @return the database specific representation of the date/time value
   */
  public String dateTime2SQL( DateTimeType dateTimeType, DateObject value ) {
    String result = "";
    QName type = dateTimeType.getType();
    if (type == DateTimeType.TYPE_DATE) {
      result = "'" + DateTimeType.toDateString( value ) + "'";
    }
    else if (type == DateTimeType.TYPE_DATETIME) {
      result = "'" + DateTimeType.toDateTimeString( value, true) + "'";
    }
    else if (type == DateTimeType.TYPE_TIME) {
      // JavaDB does not allow Millis for data type TIME
      result = "'" + DateTimeType.toTimeString( value, false) + "'";
    }
    return result;
  }

  /**
   * Creates a new Sequence
   *
   * @param name the sequence's name
   * @param min  the min (starting) value for the new sequence
   * @param max  the max value for the new sequence
   * @return the new sequence object
   */
  public NumberSequence createSequence( String name, long min, long max ) throws IOException {
    //TODO sequence support
    throw new IllegalArgumentException( "sequence currently not supported" );
  }

  /**
   * Creates an sequence object for a sequence existing in database
   *
   *
   * @param name the sequence's name
   * @return the new sequence object
   */
  public NumberSequence getSequence( String name ) throws IOException {
    return new JavaDBSequence( name, this );
  }

  public String addLimit(String sql, int maxCountResult) {
    //TODO
    return sql;
  }

  /**
   * Check and modify sql statement according to database specific needs. The default
   * syntax corresponds to Oracle Lite, so there is less need to convert strings
   * in the limited environment of a mobile device!
   * <p/>
   * - replace function upper() with toupper() in dtF
   * - replace function lower() with tolower() in dtF
   *
   * @param sql
   * @return adjusted sql statement
   */
  public String check( String sql ) {
    String sqlUpper = sql.toUpperCase();
    if (sqlUpper.startsWith( "ALTER TABLE" )) {
      int posModify = sqlUpper.indexOf( "MODIFY" );
      if (posModify > 0) {
        // Beispiel sql: alter table einzelnachweis modify bezeichnung VARCHAR(80);
        //   wird zu:    alter table einzelnachweis alter bezeichnung set data type VARCHAR(80);
        StringBuffer modifiedSQL = new StringBuffer();
        modifiedSQL.append( "ALTER TABLE " );
        modifiedSQL.append( sql.substring( "ALTER TABLE ".length(), posModify ));
        modifiedSQL.append( "ALTER " );
        int endModify = posModify+"MODIFY".length();
        int startCol = endModify + 1;
        while (sqlUpper.charAt( startCol ) == ' ') startCol++;
        int posType = sqlUpper.indexOf( ' ', startCol );
        modifiedSQL.append( sql.substring( startCol, posType ));
        modifiedSQL.append( " SET DATA TYPE " );
        modifiedSQL.append( sql.substring( posType ));
        return modifiedSQL.toString();
      }
    }
    return super.check( sql );
  }
}
