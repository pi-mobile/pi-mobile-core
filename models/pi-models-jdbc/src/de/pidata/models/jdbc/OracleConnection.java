/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.jdbc;

import de.pidata.log.Logger;
import de.pidata.models.tree.Context;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.qnames.QName;
import de.pidata.system.base.NumberSequence;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * database.drivername=oracle.jdbc.OracleDriver
 * database.url=jdbc:oracle:thin:@gandalf:1521:xe
 */
public class OracleConnection extends JdbcConnection{

  public OracleConnection() throws Exception {
    super( -1 );
    MAX_STRLEN = 4000;
  }

  /**
   *
   * @param context
   * @param propertySection the propertySection to use for readign properties, e.g. "database"
   * @throws Exception
   */
  public void connect( Context context, String propertySection ) throws Exception {
    super.connect( context, propertySection );
    setDateFormat();
  }

  protected void setDateFormat() {
    try {
      String sql;
      Statement stmt = createStatement();
      // set date/time format to ISO 8601
      sql = "ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY-MM-DD'";
      stmt.executeQuery(sql);
      sql = "ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'YYYY-MM-DD HH24:MI:SS.FF'";
      stmt.executeQuery(sql);
      sql = "ALTER SESSION SET NLS_TIMESTAMP_TZ_FORMAT = 'YYYY-MM-DD HH24:MI:SS.FF TZH:TZM'";
      stmt.executeQuery(sql);
      commit();
    }
    catch (Exception e) {
      Logger.error( "Could not set date format", e );
    }
  }

  /**
   * Closes current jdbc connection and tries to reconnect with same connection parameters
   *
   * @return true if reconnect was successful
   */
  public boolean reconnect() {
    boolean success = super.reconnect();
    if (success) {
      setDateFormat();
    };
    return success;
  }

  /**
   * Convert the internal representation of a date/time value into the database specific
   * representaion. If using a String it must be surrounded by single quotes.
   *
   * @param dateTimeType
   * @param value   the date value
   * @return the database specific representation of the date/time value
   */
  public String dateTime2SQL( DateTimeType dateTimeType, DateObject value ) {
    String result = "";
    QName type = dateTimeType.getType();
    if (type == DateTimeType.TYPE_DATE) {
      result = "'" + DateTimeType.toDateString( value ) + "'";
    }
    else if (type == DateTimeType.TYPE_DATETIME) {
      result = "'" + DateTimeType.toDateTimeString( value, true) + "'";
    }
    else if (type == DateTimeType.TYPE_TIME) {
      result = "'" + DateTimeType.toDateTimeString( value, true) + "'";
    }
    return result;
  }

  public String getDateTimeType( DateTimeType dateTimeType ) {
    QName type = dateTimeType.getType();
    if (type == DateTimeType.TYPE_DATE) {
      return "DATE";
    }
    else if (type == DateTimeType.TYPE_TIME) {
      return "TIMESTAMP(3)";
    }
    else if (type == DateTimeType.TYPE_DATETIME) {
      return "TIMESTAMP(3)";
    }
    else {
      throw new IllegalArgumentException("not implemented base type for attribute [" + dateTimeType.name() + "]");
    }
  }

  public int readSequence( String sequenceName ) throws IOException {
    ResultSet resultSet = null;
    Statement stmt = null;
    int id = -1;
    try {
      String sql = "SELECT "+sequenceName+".nextval FROM DUAL";
      stmt = createStatement();
      resultSet = stmt.executeQuery(sql);
      resultSet.next();
      id = resultSet.getInt(1);
      resultSet.close();
      stmt.close();
    }
    catch (SQLException ex) {
      String msg = "readSequence konnte keine ID erzeugen";
      Logger.error(msg, ex);
      try {
        if (resultSet != null) resultSet.close();
        if (stmt != null) stmt.close();
      }
      catch (SQLException e1) {
        // do nothing
      }
      throw new IOException(msg);
    }

    return id;
  }

  /**
   * Creates a new Sequence
   *
   * @param name the sequence's name
   * @param min  the min (starting) value for the new sequence
   * @param max  the max value for the new sequence
   * @return the new sequence object
   */
  public NumberSequence createSequence( String name, long min, long max ) throws IOException {
    //TODO sequence support
    throw new IllegalArgumentException( "sequence currently not supported" );
  }

  /**
   * Creates an sequence object for a sequence existing in database
   *
   *
   * @param name the sequence's name
   * @return the new sequence object
   */
  public NumberSequence getSequence( String name ) throws IOException {
    //TODO sequence support
    throw new IllegalArgumentException( "sequence currently not supported" );
  }

  public String addLimit(String sql, int maxCountResult) {
    int posCol = sql.toUpperCase().indexOf("SELECT") + 6;
    return "SELECT --+FIRST_ROWS(" + maxCountResult + ") " + sql.substring(posCol);
  }

}
