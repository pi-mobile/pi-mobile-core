/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.jdbc;

import de.pidata.log.Logger;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.qnames.QName;
import de.pidata.system.base.NumberSequence;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class PostreSQLConnection extends JdbcConnection {

  public PostreSQLConnection() throws Exception {
    super( -1 );
    MAX_STRLEN = 4000;
  }

  public String getDateTimeType( DateTimeType dateTimeType ) {
    QName type = dateTimeType.getType();
    if (type == DateTimeType.TYPE_DATE) {
      return "DATE";
    }
    else if (type == DateTimeType.TYPE_TIME) {
      return "TIMESTAMP(3)";
    }
    else if (type == DateTimeType.TYPE_DATETIME) {
      return "TIMESTAMP(3)";
    }
    else {
      throw new IllegalArgumentException("not implemented base type for attribute [" + dateTimeType.name() + "]");
    }
  }

  public int readSequence( String sequenceName ) throws IOException {
    ResultSet resultSet = null;
    Statement stmt = null;
    int id = -1;
    try {
      String sql = "SELECT nextval('"+sequenceName+"')";
      stmt = createStatement();
      resultSet = stmt.executeQuery(sql);
      resultSet.next();
      id = resultSet.getInt(1);
      resultSet.close();
      stmt.close();
    }
    catch (SQLException ex) {
      String msg = "readSequence konnte keine ID erzeugen";
      Logger.error(msg, ex);
      try {
        if (resultSet != null) resultSet.close();
        if (stmt != null) stmt.close();
      }
      catch (SQLException e1) {
        Logger.error( msg, ex );
      }
      throw new IOException(msg);
    }

    return id;
  }

  /**
   * Creates a new Sequence
   *
   * @param name the sequence's name
   * @param min  the min (starting) value for the new sequence
   * @param max  the max value for the new sequence
   * @return the new sequence object
   */
  public NumberSequence createSequence( String name, long min, long max ) throws IOException {
    //TODO sequence support
    throw new IllegalArgumentException( "sequence currently not supported" );
  }

  /**
   * Creates an sequence object for a sequence existing in database
   *
   *
   * @param name the sequence's name
   * @return the new sequence object
   */
  public NumberSequence getSequence( String name ) throws IOException {
    //TODO sequence support
    throw new IllegalArgumentException( "sequence currently not supported" );
  }

  public String addLimit(String sql, int maxCountResult) {
    //TODO
    return sql;
  }
}
