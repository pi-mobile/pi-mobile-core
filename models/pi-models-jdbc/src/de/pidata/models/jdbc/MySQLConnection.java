/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.jdbc;

import de.pidata.models.sql.DBConnection;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.qnames.QName;
import de.pidata.system.base.NumberSequence;

import java.io.IOException;

public class MySQLConnection extends JdbcConnection {

  public MySQLConnection() throws Exception {
    // TODO: Make reconnect configurable. We do not want a fix reconnect time.
    super( 60 );
    MAX_STRLEN = 4000;
  }

  public String getDateTimeType( DateTimeType dateTimeType ) {
    QName type = dateTimeType.getType();
    if (type == DateTimeType.TYPE_DATE) {
      return "DATE";
    }
    else if (type == DateTimeType.TYPE_TIME) {
      return "TIME";
    }
    else if (type == DateTimeType.TYPE_DATETIME) {
      return "DATETIME";
    }
    else {
      throw new IllegalArgumentException("not implemented base type for attribute [" + dateTimeType.name() + "]");
    }
  }

  /**
   * Creates a new Sequence
   * @param name the sequence's name
   * @param min  the min (starting) value for the new sequence
   * @param max  the max value for the new sequence
   * @return the new sequence object
   */
  public NumberSequence createSequence( String name, long min, long max ) throws IOException {
    throw new IllegalArgumentException( "Create sequence currently not supported for MySQL" );
  }

  /**
   * Creates an sequence object for a sequence existing in database
   *
   *
   * @param name the sequence's name
   * @return the new sequence object
   */
  public NumberSequence getSequence( String name ) throws IOException {
    return new MySqlSequence( name, this );
  }

  public String addLimit(String sql, int maxCountResult) {
    //TODO
    return sql;
  }

  /**
   * Returns the last number from a table with autoincrement.
   *
   * @param dataTable
   * @return
   */
  public int getLastId( String dataTable) {
    StringBuilder lastIdSql = new StringBuilder();
    // TODO: Create function which will get the last index from a table for different SLQ dialects.
    // targetDbConnection.getLastId(dataTable);

    // MySql: SELECT id FROM fap_dng AS lastid ORDER BY id DESC LIMIT 1;
    if (DBConnection.driverClassName.equals("com.mysql.jdbc.Driver") ) {
      lastIdSql.append( "SELECT id AS lastid  FROM ").append( dataTable ).append(" ORDER BY id DESC LIMIT 1;") ;
    }
    // SQL Server
    else {
      lastIdSql.append( "SELECT ident_current('").append( dataTable ).append("') AS lastid;" );
    }
    throw new RuntimeException( "TODO: finish implementation" );
  }
}
