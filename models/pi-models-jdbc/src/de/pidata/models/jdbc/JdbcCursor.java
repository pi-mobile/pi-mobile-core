/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.jdbc;

import de.pidata.log.Logger;
import de.pidata.models.sql.DBCursor;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.simple.BooleanType;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.models.types.simple.DecimalObject;
import de.pidata.models.types.simple.DecimalType;
import de.pidata.models.types.simple.IntegerType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcCursor implements DBCursor {

  private String sql;
  private Statement stmt;
  private ResultSet resultSet;
  private JdbcConnection conn;


  public JdbcCursor( JdbcConnection conn, String sql ) throws IOException {
    this.conn = conn;
    this.sql = sql;
    this.stmt = conn.createStatement();
    try {
      this.resultSet = stmt.executeQuery( sql );
    }
    catch (SQLException ex) {
      Logger.error("Can't execute query, sql="+sql, ex);
      throw new IOException("Can't execute query, message="+ex.getMessage());
    }
  }

  /**
   * Moves Cursor to the next data row
   * @return true if a next data row exists, otherwise false
   */
  public boolean next() throws IOException {
    try {
      return resultSet.next();
    }
    catch (SQLException e) {
      throw new IOException( "Error in ResultSet.next, msg="+e.getMessage() );
    }
  }

  /**
   * Test if a resultSet contains the specified column. Just catch the thrown exception...
   * @param columnName
   * @return true if the column does not exist in the result set or the corresponding value is null
   */
  private boolean isNull( String columnName ) {
    Object obj = null;
    try {
      obj = resultSet.getString(columnName);
    }
    catch (SQLException e) {
      return true;
    }
    return (obj == null);
  }

  /**
   * Returns string value for given columnName
   *
   * @param columnName column*s name
   * @return String value
   */
  public String getString( String columnName ) throws IOException {
    try {
      return resultSet.getString( columnName );
    }
    catch (SQLException ex) {
      String msg = "Can't read column name="+columnName;
      Logger.error(msg, ex);
      throw new IOException( msg+", message="+ex.getMessage());
    }
  }

  /**
   * Returns long value for given columnName
   *
   * @param columnName column*s name
   * @return long value
   */
  public long getLong( String columnName ) throws IOException {
    try {
      return resultSet.getLong( columnName );
    }
    catch (SQLException ex) {
      String msg = "Can't read column name="+columnName;
      Logger.error(msg, ex);
      throw new IOException( msg+", message="+ex.getMessage());
    }
  }

  /**
   * Returns the value of the attribute identified by name. The
   * result data type is defined by type.
   *
   * @param name attribute's name
   * @param type the result type
   * @param defaultValue returned if attribute name is not defined,
   *                     most be valid for type.
   * @return the value of the attribute identified by name
   */
  public Object getAttributeValue( QName name, SimpleType type, Object defaultValue)
  throws IOException {
    Object result = null;
    String columnName = name.getName();

    if (type == null) {
      throw new IllegalArgumentException("Attribute type must not be null.");
    }

    try {
      if (isNull( columnName )) {
        return defaultValue;
      }

      if (type instanceof QNameType) {
        String value = resultSet.getString(columnName);
        if (value == null) result = null;
        else if (value.length() == 0) result = null;
        else result = conn.getNamespaces().getQName(value);
      }
      else if (type instanceof StringType) {
        result = resultSet.getString(columnName);
      }
      else if (type instanceof IntegerType) {
        Class valueClass = type.getValueClass();
        if (valueClass.getName().endsWith("Integer")) {
          result = new Integer((int)resultSet.getInt(columnName));
        }
        if (valueClass.getName().endsWith("Long")) {
          result = new Long(resultSet.getInt(columnName));
        }
        if (valueClass.getName().endsWith("Short")) {
          result = new Short((short) resultSet.getInt(columnName));
        }
        if (valueClass.getName().endsWith("Byte")) {
          result = new Byte((byte) resultSet.getInt(columnName));
        }
      }
      else if (type instanceof DecimalType) {
        String strValue = resultSet.getString(columnName);
        if ((strValue == null) || (strValue.length() == 0)) result = null;
        else result = new DecimalObject(strValue);
      }
      else if (type instanceof BooleanType) {
        boolean value = resultSet.getBoolean(columnName);
        if (value) result = BooleanType.TRUE;
        else result = BooleanType.FALSE;
      }
      else if (type instanceof DateTimeType) {
        result = conn.getDateTime( (DateTimeType) type, defaultValue, resultSet, columnName );
      }
      else {
        SimpleType base = (SimpleType) type.getBaseType();
        if (base == null) {
          throw new IllegalArgumentException("not implemented base type");
        }
        result = getAttributeValue( name, base, defaultValue );
      }
    }
    catch (SQLException ex) {
      String msg = "Could not get value from result set, column=" + columnName;
      Logger.error(msg, ex);
      conn.rollback();
      throw new IOException(msg);
    }
    return result;
  }

  /**
   * Closes this DBCursor's resources, i.e. ResultSet and Statement if not null, and catches
   * possible SQLExceptions (empty catch block)
   */
  public void close() {
    conn.closeResources( stmt, resultSet );
  }
}
