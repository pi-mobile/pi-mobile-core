/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.jdbc;

import de.pidata.log.Logger;
import de.pidata.system.base.NumberSequence;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySqlSequence extends NumberSequence {

  protected JdbcConnection conn;

  public MySqlSequence( String name, MySQLConnection conn ) throws IOException {
    super( name, 0, 0, NumberSequence.KEY_TYPE_LONG );
    this.conn = conn;

    ResultSet resultSet = null;
    Statement stmt = null;
    try {
      stmt = conn.createStatement();
      String colMin = getColNameMin();
      String colMax = getColNameMax();
      String colUpdate = getColNameUpdate();
      String sql = "SELECT "+name+","+colMin+","+colMax+","+colUpdate+" FROM sequence";
      resultSet = stmt.executeQuery(sql);
      resultSet.next();
      last = resultSet.getInt( name );
      min = resultSet.getInt( colMin );
      max = resultSet.getInt( colMax );
      String updateStr = resultSet.getString( colUpdate );
      updateRequested = "X".equals( updateStr );
      resultSet.close();
      stmt.close();
      conn.commit();
    }
    catch (SQLException ex) {
      String msg = "Error initializing MySqlSequence";
      Logger.error(msg, ex);
      try {
        if (resultSet != null) resultSet.close();
        if (stmt != null) stmt.close();
      }
      catch (SQLException e1) {
        Logger.error( msg, ex );
      }
      throw new IOException(msg);
    }
  }

  private String getColNameMax() {
    String colMax = name + "_max";
    return colMax;
  }

  private String getColNameMin() {
    String colMin = name + "_min";
    return colMin;
  }

  private String getColNameUpdate() {
    String colMin = name + "_update";
    return colMin;
  }

  /**
   * Updates this sequence with a new min and max value and resets percent left to 100%
   *
   * @param min new min value
   * @param max new max value
   */
  public synchronized void update( long min, long max ) throws IOException {
    super.update( min, max );

    ResultSet resultSet = null;
    Statement stmt = null;
    try {
      stmt = conn.createStatement();
      String updateStr;
      if (updateRequested) updateStr = "X";
      else updateStr = "-";
      String sql = "UPDATE sequence SET "+getColNameMin()+"="+min+","+getColNameMax()+"="+max+","
                 +name+"="+last+","+getColNameUpdate()+"='"+updateStr+"'";
      stmt.executeUpdate( sql );
      stmt.close();
      conn.commit();
    }
    catch (SQLException ex) {
      String msg = "could not update sequence, name="+name;
      Logger.error(msg, ex);
      try {
        if (resultSet != null) resultSet.close();
        if (stmt != null) stmt.close();
      }
      catch (SQLException e1) {
        Logger.error( msg, ex );
      }
      throw new IOException(msg);
    }
  }

  /**
   * Increments the sequence by count and returns the first new value
   *
   * @param count number of values needed
   * @return next sequence value or first value if count is greater than 1
   * @throws IllegalArgumentException if there is no next value in this sequence, i.e next is greater than max()
   */
  public synchronized long next( int count ) throws IOException {
    ResultSet resultSet = null;
    Statement stmt = null;
    int id = -1;
    try {
      stmt = conn.createStatement();
      String sql = "UPDATE sequence SET "+name+"=LAST_INSERT_ID("+name+"+"+count+")";
      stmt.executeUpdate( sql );
      sql = "SELECT LAST_INSERT_ID()";
      resultSet = stmt.executeQuery(sql);
      resultSet.next();
      id = resultSet.getInt(1);
      resultSet.close();
      stmt.close();
      if (id >= max) {
        conn.rollback();
        throw new IllegalArgumentException( "Sequence '" + name + "' has not enough values for count="+count+", last="+last+", max=" + max );
      }
      conn.commit();
    }
    catch (SQLException ex) {
      String msg = "readSequence could not create next value, name="+name;
      Logger.error(msg, ex);
      try {
        if (resultSet != null) resultSet.close();
        if (stmt != null) stmt.close();
      }
      catch (SQLException e1) {
        Logger.error( msg, ex );
      }
      throw new IOException(msg);
    }
    last = id - count + 1;
    return last;
  }
}
