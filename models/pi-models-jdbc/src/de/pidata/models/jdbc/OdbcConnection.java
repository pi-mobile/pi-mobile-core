/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.jdbc;

import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.qnames.QName;
import de.pidata.system.base.NumberSequence;

import java.io.IOException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;

/**
 * database.drivername=sun.jdbc.odbc.JdbcOdbcDriver
 * database.url=jdbc:odbc:<data source name>
 * private static final String accessDBURLPrefix = "jdbc:odbc:Driver={Microsoft Access Driver (*.mdb)};DBQ=";
    private static final String accessDBURLSuffix = ";DriverID=22;READONLY=false}";
 *
 *
 */
public class OdbcConnection extends JdbcConnection{

  public OdbcConnection() throws Exception {
    super( -1 );
    MAX_STRLEN = 4096;
    throw new RuntimeException("not really implemented - many problems!");
  }

  /**
   * Convert a column in a result set to the internal representation.
   *
   *
   * @param dateTimeType
   * @param defaultValue
   * @param resultSet
   * @param columnName
   * @return
   * @throws java.sql.SQLException
   */
  public Object getDateTime( DateTimeType dateTimeType, Object defaultValue, ResultSet resultSet, String columnName) throws SQLException {
    Object result = null;
    Object colValue = resultSet.getObject(columnName);

    QName type = dateTimeType.getType();
    if ( type == DateTimeType.TYPE_DATE ) {
      Date date = Date.valueOf(colValue.toString());
      result = new DateObject( DateTimeType.TYPE_DATE, date.getTime());
    }
    else if (type == DateTimeType.TYPE_DATETIME ) {
      Timestamp dateTime = Timestamp.valueOf(colValue.toString());
      result = new DateObject( DateTimeType.TYPE_DATETIME, dateTime.getTime());
    }
    else if (type == DateTimeType.TYPE_TIME ) {
      Time time = Time.valueOf(colValue.toString());
      result = new DateObject( DateTimeType.TYPE_TIME, time.getTime());
    }
    return result;
  }

  /**
   * Returns the database specific type for an internal date/time type
   *
   *
   * @param dateTimeType@return the database specific type
   */
  public String getDateTimeType( DateTimeType dateTimeType ) {
    QName type = dateTimeType.getType();
    if (type == DateTimeType.TYPE_DATE) {
      return "DATE";
    }
    else if (type == DateTimeType.TYPE_TIME) {
      return "TIME";
    }
    else if (type == DateTimeType.TYPE_DATETIME)  {
      return "DATETIME";
    }
    else {
      throw new IllegalArgumentException("not implemented base type for attribute [" + dateTimeType.name() + "]");
    }
  }

  /**
   * Creates a new Sequence
   *
   * @param name the sequence's name
   * @param min  the min (starting) value for the new sequence
   * @param max  the max value for the new sequence
   * @return the new sequence object
   */
  public NumberSequence createSequence( String name, long min, long max ) throws IOException {
    //TODO sequence support
    throw new IllegalArgumentException( "sequence currently not supported" );
  }

  /**
   * Creates an sequence object for a sequence existing in database
   *
   *
   * @param name the sequence's name
   * @return the new sequence object
   */
  public NumberSequence getSequence( String name ) throws IOException {
    //TODO sequence support
    throw new IllegalArgumentException( "sequence currently not supported" );
  }

  /**
   * Add a limit constraint to the sql string.
   *
   * @param sql
   * @param maxCountResult
   */
  public String addLimit(String sql, int maxCountResult) {
    return sql;
  }
}
