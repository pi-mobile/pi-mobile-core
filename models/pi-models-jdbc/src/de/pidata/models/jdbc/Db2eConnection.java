/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.jdbc;

import de.pidata.log.Logger;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.qnames.QName;
import de.pidata.system.base.NumberSequence;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * database.drivername=com.ibm.db2e.jdbc.DB2eDriver
 * database.url=jdbc:db2e:crmauto
 */
public class Db2eConnection extends JdbcConnection{

  public Db2eConnection() throws Exception {
    super( -1 );
    MAX_STRLEN = 4096;
  }

  /**
   * Convert the internal representation of a date/time value into the database specific
   * representaion. If using a String it must be surrounded by single quotes.
   *
   * @param dateTimeType
   * @param value   the date value
   * @return the database specific representation of the date/time value
   */
  public String dateTime2SQL( DateTimeType dateTimeType, DateObject value ) {
    String result = "";
    QName type = dateTimeType.getType();
    if (type == DateTimeType.TYPE_DATE) {
      result =  "'" + DateTimeType.toDateString( value ) + "'";
    }
    else if (type == DateTimeType.TYPE_DATETIME) {
      result = "'" + DateTimeType.toDateString( value ) + "-" + DateTimeType.toTimeString((DateObject) value, true, 3, '.', false) + "000'";
    }
    else if (type == DateTimeType.TYPE_TIME) {
      result = "'" + DateTimeType.toTimeString( value ) + "'";
    }
    return result;
  }

  public String getDateTimeType( DateTimeType dateTimeType ) {
    QName type = dateTimeType.getType();
    if (type == DateTimeType.TYPE_DATE) {
      return "DATE";
    }
    else if (type == DateTimeType.TYPE_TIME) {
      return "TIME";
    }
    else if (type == DateTimeType.TYPE_DATETIME) {
      return "TIMESTAMP";
    }
    else {
      throw new IllegalArgumentException("not implemented base type for attribute [" + dateTimeType.name() + "]");
    }
  }

  public int readSequence( String sequenceName ) throws IOException {
    ResultSet resultSet = null;
    Statement stmt = createStatement();
    String sql;
    int id = -1;
    try {
      sql = "SELECT nextval FROM "+sequenceName;
      resultSet = stmt.executeQuery(sql);
      resultSet.next();
      id = resultSet.getInt(1);

      sql = "UPDATE "+sequenceName+" SET nextval = nextval + 1";
      stmt = createStatement();
      stmt.executeUpdate(sql);

      commit();
    }
    catch (SQLException ex) {
      String msg = "readSequence konnte keine ID erzeugen";
      Logger.error(msg, ex);
      rollback();
      try {
        if (resultSet != null) resultSet.close();
        if (stmt != null) stmt.close();
      }
      catch (SQLException e1) {
        // do nothing
      }
      throw new IOException(msg);
    }
    finally {
      closeResources(stmt, resultSet);
    }

    return id;
  }

  /**
   * Creates a new Sequence
   *
   * @param name the sequence's name
   * @param min  the min (starting) value for the new sequence
   * @param max  the max value for the new sequence
   * @return the new sequence object
   */
  public NumberSequence createSequence( String name, long min, long max ) throws IOException {
    //TODO sequence support
    throw new IllegalArgumentException( "sequence currently not supported" );
  }

  /**
   * Creates an sequence object for a sequence existing in database
   *
   *
   * @param name the sequence's name
   * @return the new sequence object
   */
  public NumberSequence getSequence( String name ) throws IOException {
    //TODO sequence support
    throw new IllegalArgumentException( "sequence currently not supported" );
  }

  public String addLimit(String sql, int maxCountResult) {
    return sql;  // TODO!
  }

  /**
   * Creatres a date expression - Reason: on some drivers you cannot compare a date with
   * a timestamp!
   * @return
   */
  public String dateExpression( String columnName, String operation, DateObject date  ) {
    return columnName + operation + "'" + DateTimeType.toDateString(date) + "'";
  }

    /**
   * Creatres a date expression - Reason: on some drivers you cannot compare a date with
   * a timestamp!
   * @return
   */
  public String dateTimeExpression( String columnName, String operation, DateObject date  ) {
    return columnName + operation + "'" + DateTimeType.toDateString(date) + "-" + DateTimeType.toTimeString(date, true, 3, '.', false) + "000" + "'";
  }
}
