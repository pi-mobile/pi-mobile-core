/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.binding;

import de.pidata.log.Logger;
import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.simple.IntegerType;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.StringType;

/**
 * Binding to a single model attribute
 */
public class AttributeBinding extends Binding {

  /** attribute name */
  private QName attributeName;

  /**
   * Creates a binding for a value binding.
   * Pathlist is set false, meaning modelPath is expected to return max. a single model, so Binding's model is
   * the first entry of XPath result container.
   * @param context          context to use for accessing global models - needed if path start with "/"
   * @param modelSource      data source (optional). If not null this binding also keeps relative to
   *                         modelSource's selected model
   * @param modelPath        the path to the model (may be null)
   * @param attributeName    name of the attribute this binding is connected to
   */
  public AttributeBinding( Context context, ModelSource modelSource, XPath modelPath, QName attributeName ) {
    super( context );
    this.attributeName = attributeName;
    setModelSource( modelSource, modelPath );
  }

  public AttributeBinding( Context context, ModelSource modelSource, String modelPath, QName attributeName, NamespaceTable namespaceTable ) {
    this(context, modelSource, XPath.fromString( namespaceTable, modelPath ), attributeName);
  }

  public QName getAttributeName() {
    return attributeName;
  }

  /**
   * Returns this binding's value type or null if model or valueID not defined
   * @return this binding's value type or null
   */
  private Type doGetValueType( Model model ) {
    if ((model != null) && (attributeName != null)) {
      if (model == context) {
        // Context variables do not have a entry in context's type
        return StringType.getDefString();
      }
      ComplexType type = (ComplexType) model.type();
      int i = type.indexOfAttribute(this.attributeName );
      if ((i < 0) && (model instanceof Transient))  {
        type = ((Transient) model).transientType();
        if (type != null) {
          i = type.indexOfAttribute(this.attributeName );
        }
      }
      if (i < 0) {
        throw new IllegalArgumentException("Model type="+model.type().name()+" does not have attribute or transient, attributeName="+this.attributeName );
      }
      SimpleType valueType = type.getAttributeType(i);
      return valueType;
    }
    else {
      return null;
    }
  }

  /**
   * Returns this binding's value type or null if model or valueID not defined
   * @return this binding's value type or null
   */
  public Type getValueType() {
    Model model = getModel();
    return doGetValueType( model );
  }

  public boolean isReadOnly() {
    if (getModel() == null) {
      return false;
    }
    else {
      return getModel().isReadOnly( attributeName );
    }
  }

  @Override
  protected Object doFetchModelValue( Model model ) {
    Object value = null;
    if (model != null) {
      try {
        value = model.get( attributeName );
      }
      catch (Exception ex) {
        Logger.error( "Could not fetch model value, modelSource=" + getModelSource() + ", path=" + getModelPath() + ", attributeName=" + attributeName, ex );
      }
    }
    if (value instanceof Enum) {
      return value.toString();
    }
    else {
      return value;
    }
  }

  /**
   * Stores value in attributeID of model
   *
   * @param value the value to be stored
   * @param model the model for storing
   */
  public void doStoreModelValue( Object value, Model model ) {
    if ((attributeName != null) && (model != null)) {
      BindingListener listener = this.bindingListener;
      this.bindingListener = null;
      try {
        Type valueType = doGetValueType( model );
        if (value != null) {
          Class valueClass = valueType.getValueClass();
          if (valueClass.isEnum()) {
            value = ((SimpleType) valueType).createValue( value.toString(), model.namespaceTable() );
          }
          else if ((value instanceof Number) && (valueType instanceof IntegerType) && (!value.getClass().equals( valueClass ))) {
            // when using e.g. progress controller we might get the wrong number type
            long val = ((Number) value).longValue();
            value = ((IntegerType) valueType).createValue( val );
          }
        }
        model.set( attributeName, value);
      }
      catch (Exception ex) {
        Logger.error( "Error setting attibute ID="+attributeName+" of model type="+model.type().name(), ex );
      }
      finally {
        this.bindingListener = listener;
      }
    }
  }

  public void storeModelValue( Object value ) {
    if (isReadOnly()) {
      throw new IllegalArgumentException( "Cannot store model value, binding is read only!");
    }
    doStoreModelValue( value, getModel() );
    fetchModelValue(); // save new value in this.value and fire event if changed
  }

  /**
   * Stores value but uses dataContext instead of binding's model;
   * If dataContext is null storeModelValeu( value ) is called.
   * In this case path is used starting vom dataContext. ModelSource is ignored.
   *
   * @param value       the value to be stored
   * @param dataContext the context model for storing or null
   */
  @Override
  public void storeModelValue( Object value, Model dataContext ) {
    if (dataContext == null) {
      storeModelValue( value );
    }
    else {
      Model model;
      XPath modelPath = getModelPath();
      if (modelPath == null) {
        model = dataContext;
      }
      else {
        model = modelPath.getModel( dataContext, context );
      }
      doStoreModelValue( value, model );
    }
  }

  /**
   * An event occurred.
   * We are only interested in events related to the binding itself. Events from child models
   * are ignored.
   * @param eventSender the sender of this event
   * @param eventID the id of the event
   * @param source the instance where the event occurred on, may be a child of eventSender
   * @param modelID the id of the affected model
   * @param oldValue the old value of the model
   * @param newValue the new value of the model
   */
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue) {
    if (EventSender.isSource( source, getModel() ) && ((attributeName == null) || (modelID == attributeName))) {
      fetchModelValue();
    }
    else {
      super.eventOccured( eventSender, eventID, source, modelID, oldValue, newValue );
    }
  }

  @Override
  public String toString() {
    return getModelPath()+"@"+attributeName;
  }
}
