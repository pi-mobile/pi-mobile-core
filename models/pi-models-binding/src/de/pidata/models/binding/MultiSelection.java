/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.binding;

import de.pidata.models.tree.EventListener;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class MultiSelection extends Selection {

  private List<Model> selectedValues = new ArrayList<Model>();

  public MultiSelection( QName tableRelationID, boolean emptyAllowed ) {
    super( tableRelationID, emptyAllowed );
  }

  /**
   * Returns the size of this selection, i.e. the count of selected values.
   *
   * @return the size of this selection
   */
  public int size() {
    return this.selectedValues.size();
  }

  @Override
  public int selectedValueCount() {
    return selectedValues.size();
  }

  public Model getSelectedValue( int index ) {
    if (index >= selectedValues.size()) {
      return null;
    }
    else {
      return (Model) this.selectedValues.get( index );
    }
  }

  @Override
  public boolean isMultiSelect() {
    return true;
  }

  /**
   * Creates a List and fills it with the current selected Items.
   * @return a new List<Model>
   */
  public List<Model> toList() {
    List<Model> selectedElements = new ArrayList<Model>();
    if(selectedValueCount() > 0) {
      for (int i = 0; i < selectedValueCount(); i++) {
        selectedElements.add( getSelectedValue( i ) );
      }
    }
    return selectedElements;
  }

  /**
   * Select the given value. For types MAX_ONE and SINGLE an already existing selection
   * is cleared before value is selected.
   *
   * @param value the value to select
   * @throws IllegalArgumentException if value's is not child of selection's binding
   */
  public Model select( Model value ) {
    checkValid( value );
    if (value == null) {
      this.selectedValues.clear();
      fireDataRemoved( null, null, null );
      return null;
    }
    else {
      if (!selectedValues.contains( value )) {
        this.selectedValues.add( value );
        fireDataAdded( value );
      }
      return value;
    }
  }

  public boolean isSelected( Model value ) {
    return selectedValues.contains( value );
  }

  public Model getSelectedPrev() {
    Model current;
    if (selectedValues.size() < 1) {
      current = null;
    }
    else {
      current = selectedValues.get( selectedValues.size()-1 );
    }
    Model prev = null;
    for (Model child : valueIter()) {
      if (child == current) {
        return prev;
      }
      prev = child;
    }
    return prev;
  }

  public Model getSelectedNext() {
    Model current;
    if (selectedValues.size() < 1) {
      current = null;
    }
    else {
      current = selectedValues.get( selectedValues.size()-1 );
    }
    boolean isNext;
    if (current == null) {
      isNext = true;
    }
    else {
      isNext = false;
    }
    for (Model child : valueIter()) {
      if (isNext) {
        return child;
      }
      if (child == current) {
        isNext = true;
      }
    }
    return current;
  }

  public void modelChanged( Binding source, Model newModel ) {
    fireTableChanged( EventListener.ID_MODEL_DATA_CHANGED, null, null, null );
    Model table = getListBinding().getModel();
    if (table == null) {
      clear();
    }
    else {
      for (int i = selectedValues.size()-1; i >= 0; i--) {
        Model model = selectedValues.get( i );
        if (!isValidSelection( model )) {
          deSelect( model );
        }
      }
      if (!emptyAllowed) {
        Model initalSelection = table.get( this.tableRelationID, null );
        if (initalSelection != null) {
          select(initalSelection);
        }
      }
    }
  }

  public boolean isValidSelection( Model value ) {
    if (value == null) {
      return emptyAllowed;
    }
    else {
      int index = getListBinding().getModel().indexOfChild( this.tableRelationID, value );
      return (index >= 0);
    }
  }

  protected void checkValid( Model value ) {
    if (value == null) {
      if (!emptyAllowed) {
        throw new IllegalArgumentException( "Value must not be null" );
      }
    }
    else {
      int index = getListBinding().getModel().indexOfChild( this.tableRelationID, value );
      if (index < 0) {
        // TODO: PRU FXTable runs into this very often
       // throw new IllegalArgumentException( "Value '" + value + "' must be a member of selection's binding" );
      }
    }
  }

  /**
   * Deselect the given value
   *
   * @param value the value to deselect, or null to deselect all
   * @return false if tried to deselect last item and seletion type is SINGLE or MIN_ONE
   */
  public boolean deSelect( Model value ) {
    if ((size() <= 1) && (!emptyAllowed)) {
      return false;
    }
    if (value == null) {
      selectedValues.clear();
      fireDataRemoved( null, null, null );
      return true;
    }
    else {
      int index = selectedValues.indexOf( value );
      this.selectedValues.remove( value );
      Model next;
      if ((index >= 0) && (index < selectedValues.size())) {
        next = (Model) selectedValues.get( index );
      }
      else {
        next = null;
      }
      fireDataRemoved( null, value, next );
      return true;
    }
  }

  public void deSelect( QName selectionID ) {
    Model value = getListBinding().getModel().get( this.tableRelationID, selectionID );
    deSelect( value );
  }

  /**
   * Removes all values from current selection
   */
  public void clear() {
    if (!emptyAllowed) {
      throw new IllegalArgumentException( "Must not clear a selection with emptyAllowed==false" );
    }
    this.selectedValues.clear();
    fireDataRemoved( null, null, null );
  }

  /**
   * Called when model list has changed
   *
   * @param eventID      see eventID at EventListner.eventOccured()
   * @param relationName the affected relation
   * @param oldValue     see oldValue at EventListner.eventOccured()
   * @param newValue     see newValue at EventListner.eventOccured()
   */
  public void listChanged( int eventID, QName relationName, Model oldValue, Model newValue ) {
    if (eventID == EventListener.ID_MODEL_DATA_ADDED) {
      fireTableChanged( eventID, relationName, oldValue, newValue );
      if (!emptyAllowed && (selectedValues.size() == 0)) {
        // select first added row if empty selection is not allowed
        select( newValue );
      }
    }
    else if (eventID == EventListener.ID_MODEL_DATA_REMOVED) {
      fireTableChanged( eventID, relationName, oldValue, newValue );
      if ((oldValue == null) || (selectedValues.contains( oldValue ))) {
        if (emptyAllowed || (selectedValues.size() > 1)) {
          deSelect( oldValue );
        }
        else {
          if (newValue == null) {
            Model newSelection = getListBinding().getModel().get( this.tableRelationID, null );
            if (emptyAllowed || (newSelection != null)) {
              select( newSelection );
            }
            else {
              // avoid firing an event if we (temporarily) have no valid data
              this.selectedValues.clear();
            }
          }
          else {
            select( newValue );
          }
        }
      }
    }
    else {
      modelChanged( getListBinding(), newValue );
    }
  }

  public void valueChanged( Binding source, Object newValue ) {
    // do nothing
  }
}
