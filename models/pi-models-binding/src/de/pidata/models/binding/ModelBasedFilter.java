/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.binding;

import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.QName;

public class ModelBasedFilter implements Filter, EventListener {

  private ModelSource dataSource;

  private FilterListener filterListener;

  @Override
  public void init(ModelSource dataSource) {
    this.dataSource = dataSource;
    if(dataSource!=null){
      dataSource.addListener( this );
    }
  }

  /**
   * Returns true if model matches this filter
   *
   * @param model the model to be matched
   * @return true if model matches this filter
   */
  public boolean matches( Model model ) {
    Model filterRules = dataSource.getModel();
    if (filterRules == null) {
      return true;
    }
    else {
      ComplexType filterType = (ComplexType) filterRules.type();
      for (int i = 0; i < filterType.attributeCount(); i++) {
        QName attrName = filterType.getAttributeName( i );
        Object filterValue = filterRules.get( attrName );
        if (filterValue != null) {
          if (!filterValue.equals( model.get( attrName) )) {
            return false;
          }
        }
      }
      return true;
    }
  }

  @Override
  public void setFilterListener( FilterListener filterListener ) {
    this.filterListener = filterListener;
  }

  @Override
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {
    if (filterListener != null) {
      filterListener.filterChanged( this );
    }
  }
}
