/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.binding;

import de.pidata.log.Logger;
import de.pidata.models.tree.*;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

public abstract class Binding implements EventListener {

  /** Context to be used to access global models */
  protected Context context;

  /** the controller to which this binding belongs */
  protected BindingListener bindingListener;

  /** reference to a controller if this binding depends on another controller's data */
  private ModelSource modelSource = null;

  /** path to the model */
  private XPath modelPath;

  /** current model */
  private Model model = null;

  /** current value */
  private Object value = null;

  /**
   * Creates a binding for a model binding.
   * Must call setModelSource() after immediately after initialization!
   *
   * @param context          context to use for accessing global models - needed if path start with "/"
   */
  protected Binding( Context context ) {
    this.context = context;
  }

  public Context getContext() {
    return context;
  }

  /**
   * Sets modelSource and path and fetches model if modelSource is not null
   * @param modelSource         data source (optional). If not null this binding also keeps relative to modelSource's selected model
   * @param modelPath          the path to the model (may be null)
   */
  public void setModelSource( ModelSource modelSource, XPath modelPath ) {
    if (this.modelSource != null) {
      this.modelSource.removeListener( this );
    }

    this.modelSource = modelSource;
    this.modelPath = modelPath;
    if (modelSource == null) {
      throw new IllegalArgumentException( "ModelSource must not be null" );
    }
    refresh();
    modelSource.addListener( this );
    if (bindingListener != null) {
      bindingListener.modelSourceChanged( this, modelSource );
    }
  }

  /**
   * Fetches this binding's model be evaluating modelSource and modelPath
   */
  public void refresh() {
    Model newModel;
    if (this.modelSource != null) {
      newModel = fetchModel();
    }
    else {
      newModel = null;
    }
    setModel( newModel );
  }

  /**
    * Removes this binding as listener from modelSource and model.
    * Clears model, bindingListener and modelSource (set to null).
    */
  public void disconnect() {
    if (model != null) {
      this.model.removeListener( this );
      this.model = null;
    }
    if (modelSource != null) {
      modelSource.removeListener( this );
      modelSource = null;
    }
    this.bindingListener = null;
  }

  public void setBindingListener( BindingListener listener ) {
    this.bindingListener = listener;
    if (bindingListener != null) {
      Model model = getModel();
      this.value = doFetchModelValue( model );
      bindingListener.modelChanged( this, model );
      bindingListener.valueChanged( this, this.value );
    }
  }

  public BindingListener getBindingListener() {
    return bindingListener;
  }

  public XPath getModelPath() {
    return modelPath;
  }

  public Model getModel() {
    return model;
  }

  public void setModel( Model newModel ) {
    Model oldModel = this.model;
    if (newModel != oldModel) {
      if (oldModel != null) {
        oldModel.removeListener( this );
      }
      this.model = newModel;
      if (this.model != null) {
        this.model.addListener( this );
      }
      if (bindingListener != null) {
        bindingListener.modelChanged( this, this.model );
      }
      fetchModelValue();
    }
  }

  /**
   * Fetches model but uses dataContext instead of binding's model.
   *
   * @param dataContext the context model
   */
  public Model getModel( Model dataContext ) {
    XPath modelPath = getModelPath();
    if (modelPath == null) {
      return dataContext;
    }
    else {
      return modelPath.getModel( dataContext, context );
    }
  }

  public ModelSource getModelSource() {
    return modelSource;
  }

  /**
   * Evalute the path and set model to the found model.
   * TODO: Änderungen am Pfad (aushängen/einhängen von Teilbäumen) wird nicht berücksichtigt!
   */
  protected Model fetchModel() {
    try {
      Model currentModel = modelSource.getModel();
      if (modelPath != null) {
        return modelPath.getModel( currentModel, context );
      }
      else {
        return currentModel;
      }
    }
    catch (Exception ex) {
      Logger.error( "Binding could not fetch model, datasource="+ modelSource +", path="+modelPath, ex);
    }
    return null;
  }

  /**
   * Returns this binding's value type or null if model or valueID not defined
   * @return this binding's value type or null
   */
  public abstract Type getValueType();

  public abstract boolean isReadOnly();

  protected abstract Object doFetchModelValue( Model model );

  /**
   * Reads model value into this Binding. If value has changed BindingListener.valueChanged() is called.
   */
  public Object fetchModelValue() {
    Object oldValue = this.value;
    Model model = getModel();
    this.value = doFetchModelValue( model );
    if ((this.value != oldValue) && (bindingListener != null)) {
      bindingListener.valueChanged( this, this.value );
    }
    return this.value;
  }

  /**
   * Fetches value but uses dataContext instead of binding's model.
   * In this case path is used starting vom dataContext. ModelSource is ignored.
   *
   * @param dataContext the context model for storing
   */
  public Object fetchModelValue( Model dataContext ) {
    Model model = getModel( dataContext );
    return doFetchModelValue( model );
  }

  public abstract void storeModelValue( Object value );

  /**
   * Stores value but uses dataContext instead of binding's model.
   * In this case path is used starting vom dataContext. ModelSource is ignored.
   *
   * @param value        the value to be stored
   * @param dataContext  the context model for storing
   */
  public abstract void storeModelValue( Object value, Model dataContext );

  /**
   * An event occurred.
   * We are only interested in events related to the binding itself. Events from child models
   * are ignored.
   * @param eventSender the sender of this event
   * @param eventID the id of the event
   * @param source the instance where the event occurred on, may be a child of eventSender
   * @param modelID the id of the affected model
   * @param oldValue the old value of the model
   * @param newValue the new value of the model
   */
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue) {
    if (source == modelSource) {
      refresh();
    }
  }

  @Override
  public String toString() {
    return ""+getModelPath();
  }
}
