/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.binding;

import de.pidata.models.tree.Filter;
import de.pidata.models.tree.FilterListener;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelSource;

public class InstanceFilter implements Filter {

  private Class filterClass;

  public InstanceFilter( Class filterClass ) {
    this.filterClass = filterClass;
  }

  @Override
  public void init( ModelSource dataSource ) {
    // do nothing
  }

  /**
   * Returns true if model matches this filter
   *
   * @param model the model to be matched
   * @return true if model matches this filter
   */
  @Override
  public boolean matches( Model model ) {
    if (model == null) {
      return false;
    }
    else {
      return this.filterClass.isAssignableFrom( model.getClass() );
    }
  }

  @Override
  public void setFilterListener( FilterListener filterListener ) {
    throw new IllegalArgumentException( "Not supported" );
  }
}
