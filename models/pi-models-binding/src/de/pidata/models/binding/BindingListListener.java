/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.binding;

import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

public interface BindingListListener extends BindingListener {

  /**
   * Called when model list has changed
   * @param eventID       see eventID at EventListner.eventOccured()
   * @param relationName  the affected relation
   * @param oldValue      see oldValue at EventListner.eventOccured()
   * @param newValue      see newValue at EventListner.eventOccured()
   */
  public void listChanged( int eventID, QName relationName, Model oldValue, Model newValue );
}
