/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.binding;

import de.pidata.models.tree.*;

public class SimpleModelSource implements ModelSource {

  private EventSender eventSender = new EventSender( this );
  private Model model;
  private Context context;

  public SimpleModelSource( Context context, Model model ) {
    this.context = context;
    this.model = model;
  }

  public void setModel( Model model ) {
    Model oldModel = this.model;
    this.model = model;
    eventSender.fireEvent( EventListener.ID_MODEL_DATA_CHANGED, this, null, oldModel, this.model );
  }

  /**
   * Returns the current data model of this data source
   *
   * @return the current data model
   */
  public Model getModel() {
    return this.model;
  }

  /**
   * Returns this ModelSource's context, e.g. to access global data structures
   *
   * @return this ModelSource's context
   */
  public Context getContext() {
    return this.context;
  }

  /**
   * Adds listener to this ModelSource's listener list
   *
   * @param listener the listener to be added
   */
  public void addListener(EventListener listener) {
    this.eventSender.addListener(listener);
  }

  /**
   * Removes listener from this ModelSource's listener list
   *
   * @param listener the listener to be removed
   */
  public void removeListener(EventListener listener) {
    this.eventSender.removeListener(listener);
  }
}
