/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.binding;

import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelSource;

/**
 * <P>Interface for all classes using Models or values from Models.</P>
 *
 * <P>Classes implementing this Interface may use Binding to attach to their Model
 * and so can attach to runtime changing Models.</P>
 */
public interface BindingListener {

  /**
   * Called by Binding after it's value has changed
   *
   * @param source   the event source
   * @param newValue the new value
   */
  public void valueChanged( Binding source, Object newValue );

  /**
   * Called by Binding after it's model has changed
   *
   * @param source   the event source
   * @param newModel the new model
   */
  public void modelChanged( Binding source, Model newModel );

  /**
   * Called by Binding after it's modelSource has been changed
   *
   * @param source
   * @param modelSource the new modelSource, never null
   */
  void modelSourceChanged( Binding source, ModelSource modelSource );
}
