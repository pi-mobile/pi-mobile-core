/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.binding;

import de.pidata.log.Logger;
import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Type;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;

/**
 * Binding to a list of models, e.g. all model in a specific model relation
 */
public class ModelListBinding extends Binding implements FilterListener {

  private QName relationName;
  private Filter filter;

  /**
   * Creates a binding for a model binding.
   * while
   * @param context          context to use for accessing global models - needed if path start with "/"
   * @param modelSource      data source (optional). If not null this binding also keeps relative to
   *                         modelSource's selected model
   * @param modelPath        the path to the model (may be null)
   * @param relationName     the name of the relation to use withing the model located by modelPath
   */
  public ModelListBinding( Context context, ModelSource modelSource, XPath modelPath, QName relationName, Filter filter ) {
    super( context );
    this.relationName = relationName;
    this.filter = filter;
    if (filter != null) {
      filter.setFilterListener( this );
    }
    setModelSource( modelSource, modelPath );
  }

  /**
   * Creates a binding for a model binding.
   * while
   * @param context          context to use for accessing global models - needed if path start with "/"
   * @param modelSource      data source (optional). If not null this binding also keeps relative to
   *                         modelSource's selected model
   * @param modelPath        the path to the model (may be null)
   * @param relationName     the name of the relation to use withing the model located by modelPath
   */
  public ModelListBinding( Context context, ModelSource modelSource, String modelPath, QName relationName, Filter filter, NamespaceTable namespaceTable ) {
    this( context, modelSource, XPath.fromString( namespaceTable, modelPath), relationName, filter );
  }

  public void setBindingListener( BindingListener listener ) {
    if (!(listener instanceof BindingListListener)) {
      throw new IllegalArgumentException( "Binding listener must be instance of BindingListListener" );
    }
    super.setBindingListener( listener );
  }

  /**
   * Evalute the path and set model to the found model.
   */
  protected Model fetchModel() {
    ModelSource modelSource = getModelSource();
    XPath modelPath = getModelPath();
    try {
      Model currentModel = modelSource.getModel();
      if (modelPath != null) {
        return modelPath.getModel( currentModel, context );
      }
      else {
        return currentModel;
      }
    }
    catch (Exception ex) {
      Logger.error( "Binding could not fetch model, datasource=" + modelSource + ", path=" + modelPath, ex );
    }
    return null;
  }

  /**
   * Returns this binding's value type or null if model or valueID not defined
   *
   * @return this binding's value type or null
   */
  public Type getValueType() {
    Model model = getModel();
    if (model == null) {
      return null;
    }
    else if (relationName == null) {
      throw new IllegalArgumentException( "getValueType not supported for ModelList" );
    }
    else {
      ComplexType modelType = (ComplexType) model.type();
      return modelType.getChildType( relationName );
    }
  }

  public boolean isReadOnly() {
    return true;
  }

  @Override
  protected Object doFetchModelValue( Model model ) {
    return model;
  }

  /**
   * Throws IllegalArgumentException
   *
   * @param value
   */
  public void storeModelValue( Object value ) {
    throw new IllegalArgumentException( "Cannot store model value, binding is read only!");
  }

  /**
   * Throws IllegalArgumentException
   *
   * @param value       the value to be stored
   * @param dataContext the context model for storing
   */
  @Override
  public void storeModelValue( Object value, Model dataContext ) {
    throw new IllegalArgumentException( "Cannot store model value, binding is read only!");
  }

  public ModelIterator modelIterator() {
    Model table = getModel();
    if (table == null) return ModelIteratorChildList.EMPTY_ITER;
    else return table.iterator( this.relationName, this.filter );
  }

  /**
   * An event occurred.
   * We are only interested in events related to the binding itself. Events from child models
   * are ignored.
   * @param eventSender the sender of this event
   * @param eventID the id of the event
   * @param source the instance where the event occurred on, may be a child of eventSender
   * @param modelID the id of the affected model
   * @param oldValue the old value of the model
   * @param newValue the new value of the model
   */
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {
    if (EventSender.isSource( source, getModel() ) && ((relationName == null) || (modelID == relationName))) {
      if (bindingListener != null) {
        if (((oldValue == null) || (oldValue instanceof Model)) && ((newValue == null) || (newValue instanceof Model))) {
          if ((eventID != EventListener.ID_MODEL_DATA_REMOVED) && (this.relationName == null)) {
            // special case: relationName null means only real children, not transient ones
            // A real child's parent is listBinding's model, a transient child has a different parent
            // This is not valid for removed event: in that case parent and relationName have already been cleared.
            if (((oldValue instanceof Model) && ((Model) oldValue).getParent( false ) != getModel())
                || ((newValue instanceof Model) && ((Model) newValue).getParent( false ) != getModel())) {
              return;
            }
          }
          ((BindingListListener) bindingListener).listChanged( eventID, modelID, (Model) oldValue, (Model) newValue );
        }
      }
    }
    else {
      super.eventOccured( eventSender, eventID, source, modelID, oldValue, newValue );
    }
  }

  @Override
  public void filterChanged( Filter filter ) {
    if (bindingListener != null) {
      ((BindingListListener) bindingListener).listChanged( EventListener.ID_MODEL_DATA_CHANGED, relationName, null, null );
    }
  }

  public Filter getFilter() {
    return filter;
  }
}
