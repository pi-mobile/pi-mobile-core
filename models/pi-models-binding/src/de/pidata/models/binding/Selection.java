/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.binding;

import de.pidata.models.tree.*;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

/**
 * Holds a list of models selected from another model's (binding) children
 */
public abstract class Selection implements BindingListListener {

  public static final Namespace NAMESPACE    = Namespace.getInstance("de.pidata.models");
  
  private ModelListBinding listBinding;
  protected QName     tableRelationID;
  protected boolean   emptyAllowed;
  private EventSender eventSender = new EventSender( this );

  public Selection( QName tableRelationID, boolean emptyAllowed  ) {
    this.emptyAllowed = emptyAllowed;
    this.tableRelationID = tableRelationID;
  }

  /**
   * Sets this controller's binding and fetches value
   * @param listBinding
   */
  public void setListBinding( ModelListBinding listBinding) {
    this.listBinding = listBinding;
    listBinding.setBindingListener( this );
  }

  public ModelListBinding getListBinding() {
    return listBinding;
  }

  public void refresh() {
    listBinding.refresh();
  }

  /**
   * Called by Binding after it's modelSource has been changed
   *
   * @param source
   * @param modelSource the new modelSource, never null
   */
  @Override
  public void modelSourceChanged( Binding source, ModelSource modelSource ) {
    // do nothing - only valid for ValueController
  }

  public QName getTableRelationID() {
    return tableRelationID;
  }

  public boolean isEmptyAllowed() {
    return emptyAllowed;
  }

  public abstract int selectedValueCount();

  public abstract Model getSelectedValue( int index );

  public abstract boolean isMultiSelect();

  /**
   * Called by binding to tell this BindingListener to store its
   * value in the binding.
   * Note: This happens e.g. before a dialog executes a operation to
   * make sure all dialog values are available to that operation.
   *
   * Selection's implementation does nothing, because a selection
   * always stores changes immediately to the binding.
   */
  public void saveValue() {
    // do nothing
  }

  /**
   * Returns the number of values available for selection.
   * @return the number of values available for selection.
   */
  public int valueCount() {
    if (getListBinding() == null) return 0;
    else {
      Model table = getListBinding().getModel();
      if (table == null) return 0;
      else return table.childCount(this.tableRelationID);
    }
  }

  /**
   * Returns an iterator over all Models available for selection
   * @return an iterator over all Models available for selection
   */
  public ModelIterator<Model> valueIter() {
    if (getListBinding() == null) {
      return ModelIteratorChildList.EMPTY_ITER;
    }
    else {
      return getListBinding().modelIterator();
    }
  }

  /**
   * Returns the model identified by key from this selection's table.
   * If valuePath is not null for each entry within this selection's tabele valuePath is evaluated
   * before checking key.
   *
   * @param valueKey   key of the value to return
   * @param valuePath  optional path form selection's table entries to value model, may be null
   * @return  the matching model or null
   */
  public Model getValue( Key valueKey, XPath valuePath ) {
    if (listBinding != null) {
      Model table = listBinding.getModel();
      if (table != null) {
        if (valuePath == null) {
          return table.get(this.tableRelationID, valueKey);
        }
        else {
          for (Model model : valueIter()) {
            Model valueModel = valuePath.getModel( model, null );
            if ((valueModel != null) && valueKey.equals( valueModel.key() )) {
              return model;
            }
          }
        }
      }
    }
    return null;
  }

  public int indexOf(Model selModel) {
    if (listBinding != null) {
      Model table = listBinding.getModel();
      if (table != null) {
        return table.indexOfChild(this.tableRelationID, selModel);
      }
    }
    return -1;
  }

  /**
   * Retrieves the value at index or null if index is out of range.
   * @param index the index of the value to be selected
   * @return the value at index or null if index is out of range
   */
  public Model getValue(int index) {
    int i = 0;
    for (Model child : valueIter()) {
      if (i == index) return child;
      i++;
    }
    return null;
  }

  /**
   * Returns the first Model in selection having value for attribute attrName
   * @param attrName name of the attribute to look at
   * @param value        value required fopr attrName
   * @return the model or null if none is matching
   */
  public Model findValue( QName attrName, Object value ) {
    for (Model child : valueIter()) {
      Object childValue = child.get( attrName );
      if (value == null) {
        if (childValue == null) return child;
      }
      else {
        if (value.equals( childValue )) return child;
      }
    }
    return null;
  }

  /**
   * Select the given value. For types MAX_ONE and SINGLE an already existing selection
   * is cleared before value is selected.
   * @param value the value to select
   * @return the selected value
   * @throws IllegalArgumentException if value's is not child of selection's binding
   */
  public abstract Model select(Model value);

  public abstract boolean isSelected(Model value);

  public void selectAll() {
    if (isMultiSelect()) {
      for (Model value : valueIter()) {
        select( value );
      }
    }
    else {
      throw new IllegalArgumentException( "SelectAll only allowed for multiSelect" );
    }
  }

  /**
   * Returns previous of current selection. If previous does not exist
   * current selection is returned
   * @return previous of current selection or current selection
   */
  public abstract Model getSelectedPrev();

  /**
   * Returns next of current selection. If next does not exist
   * current selection is returned
   * @return next of current selection or current selection
   */
  public abstract Model getSelectedNext();

  public abstract boolean isValidSelection( Model value );

  protected abstract void checkValid(Model value) throws IllegalArgumentException;

  /**
   * Deselect the given value
   * @param value the value to deselect, or null to deselect all
   * @return false if tried to deselect last item and seletion type is SINGLE or MIN_ONE
   */
  public abstract boolean deSelect(Model value);

  public abstract void deSelect(QName selectionID);

  /**
   * Removes all values from current selection
   */
  public abstract void clear();

  /**
   * Adds listener to this Model's listener list
   *
   * @param listener the listener to be added
   */
  public void addListener(EventListener listener) {
    this.eventSender.addListener( listener );
  }

  /**
   * Removes listener from this Model's listener list
   *
   * @param listener the listener to be removed
   */
  public void removeListener(EventListener listener) {
    this.eventSender.removeListener( listener );
  }

  protected void fireTableChanged( int eventID, QName relationID, Model oldValue, Model newValue ) {
    this.eventSender.fireEvent( eventID, listBinding, relationID, oldValue, newValue );
  }

  protected void fireDataAdded(Model newValue) {
    this.eventSender.fireEvent( EventListener.ID_MODEL_DATA_ADDED, this, null, null, newValue );
  }

  protected void fireDataChanged(Model oldValue, Model newValue) {
    this.eventSender.fireEvent( EventListener.ID_MODEL_DATA_CHANGED, this, null, oldValue, newValue);
  }

  /**
   *
   * @param relationID the relation from which oldValue was removed, if null
   *                   all realtions are affected
   * @param oldValue   the removed child, if null all children of relationID
   *                   have been removed
   * @param nextValue  the next child after removed child
   */
  protected void fireDataRemoved(QName relationID, Model oldValue, Model nextValue ) {
    this.eventSender.fireEvent( EventListener.ID_MODEL_DATA_REMOVED, this, relationID, oldValue, nextValue );
  }
}
