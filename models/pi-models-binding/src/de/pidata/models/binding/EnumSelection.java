/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.binding;

import de.pidata.models.simpleModels.SimpleStringTable;
import de.pidata.models.simpleModels.StringMap;
import de.pidata.models.tree.*;
import de.pidata.models.types.Type;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;
import de.pidata.string.Helper;

public class EnumSelection extends SingleSelection {

  private Context context;
  private ModelSource modelSource;
  private Binding enumAttributeBinding;
  private NamespaceTable namespaceTable;
  private Filter filter;

  public EnumSelection( Context context, NamespaceTable namespaceTable, ModelSource modelSource, QName tableRelationID, Binding enumAttributeBinding, Filter filter, boolean emptyAllowed ) {
    super( tableRelationID, emptyAllowed );
    this.context = context;
    this.namespaceTable = namespaceTable;
    this.modelSource = modelSource;
    this.enumAttributeBinding = enumAttributeBinding;
    this.filter = filter;
  }

  @Override
  public void refresh() {
    enumAttributeBinding.refresh();
    Binding binding = super.getListBinding();
    if (binding == null) {
      initModelListBinding();
    }
    else {
      binding.refresh();
    }
    super.refresh();
  }

  private void initModelListBinding() {
    Model model = modelSource.getModel();
    initEnumModel( model );
    ModelListBinding modelListBinding = new ModelListBinding( context, modelSource, new XPath( namespaceTable, "." ), tableRelationID, filter );
    setListBinding( modelListBinding );
  }

  private void initEnumModel( Model model ) {
    if (model instanceof SimpleStringTable) {
      Type valueType = enumAttributeBinding.getValueType();
      if (valueType != null) {
        Class valueClass = valueType.getValueClass();
        if (valueClass.isEnum()) {
          for (Object constant : valueClass.getEnumConstants()) {
            String enumValue = constant.toString();
            String name = "";
            if (constant instanceof Enum) {
              name = ((Enum) constant).name();
            }
            StringMap entry;
            if (Helper.isNullOrEmpty( name ) || enumValue.equals( name )) {
              entry = new StringMap( enumValue );
            }
            else {
              entry = new StringMap( name, enumValue );
            }
            ((SimpleStringTable) model).addStringMapEntry( entry );
          }
        }
      }
    }
  }

  /**
   * Returns the first Model in selection having value for attribute attrName
   *
   * @param attrName name of the attribute to look at
   * @param value    value required fopr attrName
   * @return the model or null if none is matching
   */
  @Override
  public Model findValue( QName attrName, Object value ) {
    if (getListBinding() == null) {
      initModelListBinding();
    }
    return super.findValue( attrName, value );
  }

  @Override
  public void modelChanged( Binding source, Model newModel ) {
    // may only occur on initial set binding and in that case we do not want to happen anything
  }
}
