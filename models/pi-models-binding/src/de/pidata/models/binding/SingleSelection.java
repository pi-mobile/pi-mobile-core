/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.binding;

import de.pidata.models.tree.*;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;

public class SingleSelection extends Selection implements ModelSource {

  private Model selectedValue = null;

  public SingleSelection( QName tableRelationID, boolean emptyAllowed  ) {
    super( tableRelationID, emptyAllowed );
  }

  public SingleSelection( Context context, NamespaceTable namespaceTable, ModelSource modelSource, QName tableRelationID, Filter filter, boolean emptyAllowed  ) {
    super( tableRelationID, emptyAllowed );
    ModelListBinding binding = new ModelListBinding( context, modelSource, new XPath(namespaceTable,"."), tableRelationID, filter );
    setListBinding( binding );
  }

  /**
   * Returns the current data model of this data source
   *
   * @return the current data model
   */
  public Model getModel() {
    return getSelectedValue(0);
  }

  @Override
  public int selectedValueCount() {
    // TODO: create new methods with correct and consistent implementation!
    return 1;
  }

  @Override
  public Model getSelectedValue( int index ) {
    // TODO: create new methods with correct and consistent implementation!
    if (index != 0) {
      throw new IndexOutOfBoundsException( "For SingleSelection index must be 0, index="+index );
    }
    return selectedValue;
  }

  @Override
  public boolean isMultiSelect() {
    return false;
  }

  /**
   * Select the given value. If value == this..selectedValue nothing happens, otherwise
   * existing selection is cleared and fireDateRemoved() is called. Then value is made the
   * new selection and fireDataAdded() is called. 
   *
   * @param value the value to select
   * @return the selected value
   * @throws IllegalArgumentException if value's is not child of selection's binding
   */
  public Model select( Model value ) {
    if (value != this.selectedValue) {
      checkValid( value );
      Model oldValue = this.selectedValue;
      this.selectedValue = value;
      fireDataChanged( oldValue, value );
    }
    return value;
  }

  public boolean isSelected( Model value ) {
    return (value == this.selectedValue);
  }

  public Model getSelectedPrev() {
    Model prev = null;
    for (Model child : valueIter()) {
      if (child == selectedValue) {
        return prev;
      }
      prev = child;
    }
    return prev;
  }

  public Model getSelectedNext() {
    boolean isNext;
    if (selectedValue == null) {
      isNext = true;
    }
    else {
      isNext = false;
    }
    for (Model child : valueIter()) {
      if (isNext) {
        return child;
      }
      if (child == selectedValue) {
        isNext = true;
      }
    }
    return selectedValue;
  }

  public void modelChanged( Binding source, Model newModel ) {
    fireTableChanged( EventListener.ID_MODEL_DATA_CHANGED, null, null, null );
    Model table = getListBinding().getModel();
    if (table == null) {
      clear();
    }
    else {
      if (!isValidSelection( selectedValue )) {
        Model initalSelection;
        if (isEmptyAllowed()) {
          initalSelection = null;
        }
        else {
           initalSelection = table.get( this.tableRelationID, null );
        }
        if (initalSelection == null) {
          clear();
        }
        else {
          select( initalSelection );
        }
      }
    }
  }

  public boolean isValidSelection( Model value ) {
    if (value == null) {
      return emptyAllowed;
    }
    else {
      Model listModel =  this.getListBinding().getModel();
      if (listModel == null) {
        return false;
      }
      else {
        int index = listModel.indexOfChild( this.tableRelationID, value );
        return (index >= 0);
      }
    }
  }

  protected void checkValid( Model value ) {
    if (value == null) {
      if ((!emptyAllowed) && (this.getListBinding().getModel().childCount( this.tableRelationID ) > 0)) {
        throw new IllegalArgumentException( "Value must not be null" );
      }
    }
    else {
      if (!isValidSelection( value )) {
        throw new IllegalArgumentException( "Value '" + value + "' must be a member of selection's binding" );
      }
    }
  }

  /**
   * Deselect the given value
   *
   * @param value the value to deselect, or null to deselect all
   * @return false if tried to deselect last item and seletion type is SINGLE or MIN_ONE
   */
  public boolean deSelect( Model value ) {
    if (!emptyAllowed) {
      return false;
    }
    this.selectedValue = null;
    fireDataRemoved( null, value, null );
    return true;
  }

  public void deSelect( QName selectionID ) {
    Model value = this.getListBinding().getModel().get( this.tableRelationID, selectionID );
    deSelect( value );
  }

  /**
   * Removes all values from current selection
   */
  public void clear() {
    if ((selectedValue != null) && (!emptyAllowed) && (valueCount() > 0)) {
      throw new IllegalArgumentException( "Must not clear a selection with emptyAllowed==false" );
    }
    this.selectedValue = null;
    fireDataRemoved( null, null, null );
  }

  /**
   * Called when model list has changed
   *
   * @param eventID      see eventID at EventListner.eventOccured()
   * @param relationName the affected relation
   * @param oldValue     see oldValue at EventListner.eventOccured()
   * @param newValue     see newValue at EventListner.eventOccured()
   */
  public void listChanged( int eventID, QName relationName, Model oldValue, Model newValue ) {
    if (eventID == EventListener.ID_MODEL_DATA_ADDED) {
      fireTableChanged( eventID, relationName, oldValue, newValue );
      if (!emptyAllowed && (this.selectedValue == null)) {
        // select first added row if empty selection is not allowed
        select( newValue );
      }
    }
    else if (eventID == EventListener.ID_MODEL_DATA_REMOVED) {
      fireTableChanged( eventID, relationName, oldValue, newValue );
      if ((oldValue == null) || (oldValue == getSelectedValue(0) )) {
        if (emptyAllowed) {
          deSelect( oldValue );
        }
        else {
          if (newValue == null) {
            Model newSelection = getListBinding().getModel().get( this.tableRelationID, null );
            if (emptyAllowed || (newSelection != null)) {
              select( newSelection );
            }
            else {
              // avoid firing an event if we (temporarily) have no valid data
              this.selectedValue = null;
            }
          }
          else {
            select( newValue );
          }
        }
      }
    }
    else {
      modelChanged( getListBinding(), newValue );
    }
  }

  public void valueChanged( Binding source, Object newValue ) {
    // do nothing
  }
}
