/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.binding;

import de.pidata.models.tree.Context;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelSource;
import de.pidata.models.tree.XPath;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

/**
 * Binding to a single model.
 */
public class ModelBinding extends Binding {

  private QName relationName;

  /**
   * Creates a binding for a model binding.
   * while
   * @param context          context to use for accessing global models - needed if path start with "/"
   * @param modelSource      data source (optional). If not null this binding also keeps relative to
   *                         modelSource's selected model
   * @param modelPath        the path to the model (may be null)
   * @param relationName     the name of the relation to use withing the model located by modelPath;
   *                         that relation should have a max. size of 1
   */
  public ModelBinding(Context context, ModelSource modelSource, XPath modelPath, QName relationName ) {
    super( context );
    this.relationName = relationName;
    setModelSource( modelSource, modelPath );
  }

  /**
   * Returns this binding's value type or null if model or valueID not defined
   *
   * @return this binding's value type or null
   */
  public Type getValueType() {
    Model model = getModel();
    if (model == null) {
      return null;
    }
    else {
      if (relationName == null) {
        return model.type();
      }
      else {
        ComplexType type = (ComplexType) model.type();
        return type.getChildType( relationName );
      }
    }
  }

  public boolean isReadOnly() {
    return true;
  }

  protected Object doFetchModelValue( Model contextModel ) {
    if (relationName == null) {
      return contextModel;
    }
    else if (contextModel == null) {
      return null;
    }
    else {
      return contextModel.get( relationName, null );
    }
  }

  /**
   * Throws IllegalArgumentException
   *
   * @param value
   */
  public void storeModelValue( Object value ) {
    throw new IllegalArgumentException( "Cannot store model value, binding is read only!");
  }

  /**
   * Throws IllegalArgumentException
   *
   * @param value        the value to be stored
   * @param dataContext  the context model for storing
   */
  public void storeModelValue( Object value, Model dataContext ) {
    throw new IllegalArgumentException( "Cannot store model value, binding is read only!");
  }
}
