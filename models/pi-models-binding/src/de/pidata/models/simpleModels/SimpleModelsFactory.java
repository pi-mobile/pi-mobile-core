// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.models.simpleModels;

import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.*;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.util.Hashtable;

public class SimpleModelsFactory extends de.pidata.models.tree.AbstractModelFactory {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.pidata.de/res/simpleModels.xsd" );

  public static final QName ID_SIMPLESTRINGTABLE = NAMESPACE.getQName("simpleStringTable");

  public static final ComplexType STRINGMAP_TYPE = new DefaultComplexType( NAMESPACE.getQName( "stringMap" ), StringMap.class.getName(), 1 );
  public static final ComplexType SIMPLESTRINGTABLE_TYPE = new DefaultComplexType( NAMESPACE.getQName( "simpleStringTable" ), SimpleStringTable.class.getName(), 0 );
  static {
    DefaultComplexType type;
    type = (DefaultComplexType) STRINGMAP_TYPE;
    type.addKeyAttributeType( 0, StringMap.ID_ID, StringType.getDefString() );
    type.addAttributeType( StringMap.ID_VALUE, StringType.getDefString() );

    type = (DefaultComplexType) SIMPLESTRINGTABLE_TYPE;
    type.addAttributeType( SimpleStringTable.ID_ID, QNameType.getInstance() );
    type.addRelation( SimpleStringTable.ID_STRINGMAPENTRY, STRINGMAP_TYPE, 1, Integer.MAX_VALUE );

  }
  public SimpleModelsFactory() {
    super( NAMESPACE, "http://www.pidata.de/res/simpleModels.xsd", "1.0" );
    addType( STRINGMAP_TYPE );
    addType( SIMPLESTRINGTABLE_TYPE );
    addRootRelation( ID_SIMPLESTRINGTABLE, SIMPLESTRINGTABLE_TYPE, 1, 1);
  }

  public Model createInstance(Key key, Type typeDef, Object[] attributes, Hashtable anyAttribs, ChildList children) {
    Class modelClass = typeDef.getValueClass();
    if (modelClass == StringMap.class) {
      return new StringMap(key, attributes, anyAttribs, children);
    }
    if (modelClass == SimpleStringTable.class) {
      return new SimpleStringTable(key, attributes, anyAttribs, children);
    }
    return super.createInstance(key, typeDef, attributes, anyAttribs, children);
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
