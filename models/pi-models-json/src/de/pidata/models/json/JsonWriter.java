/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.json;

import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.tree.QNameIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.simple.*;
import de.pidata.qnames.QName;

import java.util.Enumeration;
import java.util.List;

public class JsonWriter {

  private final int INDENT = 2;

  public StringBuilder writeJson( Model model, QName relationName ) {
    StringBuilder buffer = new StringBuilder();
    writeJson( buffer, model, relationName, 0 );
    return buffer;
  }

  public StringBuilder writeJson( ModelIterator<Model> modelIter ) {
    StringBuilder buffer = new StringBuilder();
    if (modelIter == null) {
      buffer.append( "[]" );
    }
    else {
      writeChildList( buffer, 0, modelIter );
    }
    return buffer;
  }

  public StringBuilder writeJson( List<Model> modelList ) {
    StringBuilder buffer = new StringBuilder();
    if (modelList == null) {
      buffer.append( "[]" );
    }
    else {
      buffer.append( "[" );
      boolean firstChild = true;
      for (Model child : modelList) {
        if (firstChild) {
          firstChild = false;
        }
        else {
          buffer.append( ",\n" );
        }
        appendIndent( buffer, 0 );
        writeJson( buffer, child, child.getParentRelationID(), INDENT );
      }
      buffer.append( "]" );
    }
    return buffer;
  }

  private void appendIndent( StringBuilder buffer, int indent ) {
    for (int j = 0; j < indent; j++) {
      buffer.append(" ");
    }
  }

  private boolean appendAttribute( StringBuilder buffer, QName attrName, String attributeValue, boolean first, int indent ) {
    if (first) {
      first = false;
    }
    else {
      buffer.append( ",\n" );
    }
    appendIndent( buffer, indent );
    buffer.append("\"").append(attrName.getName() + "\": \"");
    buffer.append(attributeValue + "\"");
    return first;
  }

  private void writeJson( StringBuilder buffer, Model model, QName relationName, int indent ) {
    ComplexType complexType;
    SimpleType simpleType;
    QName attrName;
    String attributeValue = "";
    Object object;


    String name = relationName.getName();
    if (model.type() instanceof ComplexType) {
      complexType = (ComplexType) model.type();

      appendIndent( buffer, indent );
      buffer.append( "{\n");
      indent += INDENT;

      //----- Write normal attributes
      boolean first = true;
      for (int i = 0; i < complexType.attributeCount(); i++) {
        simpleType = complexType.getAttributeType(i);
        attrName = complexType.getAttributeName(i);
        object = model.get(attrName);
        if (object != null) {
          attributeValue = convertAttribute(object, simpleType);
          if (attributeValue != null) {
            first = appendAttribute( buffer, attrName, attributeValue, first, indent );
          }
        }
      }
      //----- Write any attributes
      AnyAttribute anyAttribute = complexType.getAnyAttribute();
      if (anyAttribute != null) {
        for (Enumeration nameEnum = model.anyAttributeNames(); nameEnum.hasMoreElements(); ) {
          attrName = (QName) nameEnum.nextElement();
          simpleType = anyAttribute.findType(attrName);
          object = model.get(attrName);
          if (object != null) {
            attributeValue = convertAttribute(object, simpleType);
            if (attributeValue != null) {
              first = appendAttribute( buffer, attrName, attributeValue, first, indent );
            }
          }
        }
      }

      //----- write child models
      for (QNameIterator childRelIter = complexType.relationNames(); childRelIter.hasNext();) {
        QName childRelName = childRelIter.next();
        Relation childRel = complexType.getRelation( childRelName );
        if (first) {
          first = false;
        }
        else {
          buffer.append( ",\n" );
        }
        appendIndent( buffer, indent );
        buffer.append( "\"" ).append( childRelName.getName() ).append("\": ");
        if (childRel.getMaxOccurs() > 1) {
          ModelIterator<Model> childIter = model.iterator( childRelName, null );
          writeChildList( buffer, indent, childIter );
        }
        else {
          Model child = model.firstChild( childRelName );
          if (child == null) {
            buffer.append( "[]" );
          }
          else {
            writeJson( buffer, child, relationName, indent + INDENT );
          }
        }
      }
      buffer.append( "\n" );
      appendIndent( buffer, indent - INDENT );
      buffer.append( "}\n" );
    }
    else {
      simpleType = (SimpleType) model.type();
      String xmlValue = convertAttribute( model.getContent(), simpleType );
      if (xmlValue != null) {
        // FIXME?: String lists do not need a name.
        // buffer.append( "\"" ).append( name ).append( "\": \"" );
        buffer.append( "\"" ).append( xmlValue ).append( "\"" );
      }
    }
  }

  private void writeChildList( StringBuilder buffer, int indent, ModelIterator<Model> childIter ) {
    buffer.append( "[" );
    boolean firstChild = true;
    for (Model child : childIter) {
      if (firstChild) {
        firstChild = false;
      }
      else {
        buffer.append( ",\n" );
      }
      appendIndent( buffer, indent );
      writeJson( buffer, child, child.getParentRelationID(), indent + INDENT );
    }
    buffer.append( "]" );
  }

  /**
   * Return the string value for the given object by using his simpleType
   *
   * @param object the object to convert
   * @param simpleType the type of the object
   * @return the String value of the given object
   */
  private String convertAttribute(Object object, SimpleType simpleType) {
    if (simpleType == null) {
      throw new IllegalArgumentException("Arguments simpleType is wrong: simpleType == null");
    }

    if (object != null) {
      if (simpleType instanceof QNameType) {
        QName idValue = (QName) object;
        return convertString(idValue.getName());
      }
      else if (simpleType instanceof StringType) {
        return convertString((String) object);
      }
      else if (simpleType instanceof DecimalType) {
        return object.toString();
      }
      else if (simpleType instanceof NumberType) {
        return String.valueOf(object);
      }
      else if (simpleType instanceof BooleanType) {
        return String.valueOf(object);
      }
      else if (simpleType instanceof DateTimeType) {
        QName type = ((DateTimeType) simpleType).getType();
        if (type == DateTimeType.TYPE_DATE) {
          return DateTimeType.toDateString((DateObject) object);
        }
        else if (type == DateTimeType.TYPE_DATETIME) {
          return DateTimeType.toDateTimeString((DateObject) object, true);
          // TODO folgende Codezeile wäre richtig, ist aber inkompatibel mit dem Konstruktor von DateObject
          // in alten Clients: MMontage bis v75, MService bis v30
          // return DateTimeType.toDateString((DateObject) object) + "T" + DateTimeType.toTimeString((DateObject) object, true, 3, ':', true);
        }
        else if (type == DateTimeType.TYPE_TIME) {
          return DateTimeType.toTimeString((DateObject) object, true);
        }
      }
      else if (simpleType instanceof BinaryType) {
        return BinaryType.toBase64String( ((Binary) object).getBytes(), ((Binary) object).size() );
      }
      else {
        SimpleType baseType = (SimpleType) simpleType.getBaseType();
        if (baseType != null) {
          return convertAttribute(object, baseType);
        }
        else {
          throw new IllegalArgumentException("not implemented base type for attribute [" + simpleType.name() + "]");
        }
      }
    }
    return null;
  }

  private String convertString( String string ) {
    StringBuffer buf = new StringBuffer();
    char aktChar;
    for (int i = 0; i < string.length(); i++) {
      aktChar = string.charAt(i);
      switch (aktChar) {
        case '"': {
          buf.append( "\\\"" );
          break;
        }
        case '\b': {
          buf.append( "\\b" );
          break;
        }
        case '\f': {
          buf.append( "\\f" );
          break;
        }
        case '\n': {
          buf.append( "\\n" );
          break;
        }
        case '\r': {
          buf.append( "\\r" );
          break;
        }
        case '\t': {
          buf.append( "\\t" );
          break;
        }
        case '\\': {
          buf.append( "\\\\" );
          break;
        }
        default: {
          if (aktChar < ' ') {
            buf.append( "\\u" );
            String charHexStr = Integer.toHexString( aktChar );
            for (int x = 0; x < (4 - charHexStr.length()); x++) {
              buf.append( "0" );
            }
            buf.append( charHexStr );
          }
          else {
            buf.append( aktChar );
          }
        }
      }
    }
    return buf.toString();
  }
}
