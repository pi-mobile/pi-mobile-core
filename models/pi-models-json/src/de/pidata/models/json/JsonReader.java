/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.json;

import de.pidata.parser.Parser;
import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.*;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.io.IOException;
import java.io.InputStream;

public class JsonReader extends Parser {

  private boolean ignoreAdditionalAttributes;

  public JsonReader() {
    this.ignoreAdditionalAttributes = false;
  }

  public JsonReader( boolean ignoreAdditionalAttributes ) {
    this.ignoreAdditionalAttributes = ignoreAdditionalAttributes;
  }

  @Override
  protected void parseError( String message ) throws JsonParseError {
    int start = inputPos - 30;
    int end = inputPos;
    dumpRemainingMsg();
    if (start <= 0) {
      throw new JsonParseError( message + ", input=" + inputBuffer.substring( 0, end ) );
    }
    else {
      throw new JsonParseError( message + ", input=.." + inputBuffer.substring( start, end ) );
    }
  }

  @Override
  public void doParse() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  public synchronized void loadDataList( InputStream dataStream, Model parentModel, QName relationName, String encoding ) throws IOException {
    initBuffer( dataStream, encoding );
    if (nextChar() >= 0) {
      back();
      parseArray( parentModel, relationName.getName() );
    }
  }

  public synchronized Model loadData( InputStream dataStream, Type type, String encoding ) throws IOException {
    initBuffer( dataStream, encoding );
    if (nextChar() == -1) {
      return null;
    }
    back();

    if (type instanceof SimpleType) {
      return parseSimpleModel( (SimpleType) type );
    }
    else {
      ComplexType modelType = (ComplexType) type;
      ModelFactory modelFactory = ModelFactoryTable.getInstance().getFactory( modelType.name().getNamespace() );
      Model result = modelFactory.createInstance( modelType );
      int ch;
      do {
        skipUntil( '"' );
        parseMember( result );
        ch = skipWhitespace( false );
        while ((ch != ',') && (ch != '}')) {
          ch = skipWhitespace( false );
          if (ch == -1) {
            endOfInputError( "Unexpected end of input while parsing array" );
          }
        }
      } while (ch != '}');
      return result;
    }
  }

  private void parseValue( Model parentModel, String name ) throws IOException {
    Type type = parentModel.type();
    if (type instanceof SimpleType) {
      parseError( "Cannot read a member for simple type="+type.name() );
    }
    ComplexType modelType = (ComplexType) type;

    int attributeIndex = getAttributeIndex( modelType, name );
    if (attributeIndex >= 0) {
      // child is an Attribute
      QName attributeName = modelType.getAttributeName( attributeIndex );
      SimpleType attributeType = modelType.getAttributeType( attributeIndex );
      Object value = parseAttributeValue( attributeName, attributeType );
      parentModel.set( attributeName, value );
    }
    else {
      // child is a Relation
      Relation childRel = getChildRelation( modelType, name );
      if (childRel == null) {
        if (modelType instanceof DefaultComplexType) {
          parseUndefinedChild( parentModel, (DefaultComplexType) modelType, name );
        }
        else {
          parseError( "Unknown Attribute or Relation name=" + name + " for type=" + modelType.name() );
        }
      }
      else {
        Type childType = childRel.getChildType();
        parseValueChild( parentModel, childRel.getRelationID(), childType );
      }
    }
  }

  private void parseUndefinedChild( Model parentModel, DefaultComplexType parentType, String name ) throws IOException {
    QName id = QName.getInstance( parentModel.getParentRelationID().getNamespace(), name );
    int ch = skipWhitespace( false );
    if (ch == '[') {
      back();
      parseArray( parentModel, name );
    }
    else if (ch == '{') {
      back();
      Type childType = new DefaultComplexType( id, DefaultModel.class.getName(), 0 );
      if (!ignoreAdditionalAttributes) {
        parentType.addRelation( id, childType, 0, Integer.MAX_VALUE );
      }
      parseValueChild( parentModel, id, childType );
    }
    else {
      back();
      SimpleType attributeType = StringType.getDefString();
      Object value = parseAttributeValue( id, attributeType );
      if (!ignoreAdditionalAttributes) {
        parentType.addAttributeType( id, attributeType );
        parentModel.set( id, value );
      }
    }
  }

  private Object parseAttributeValue( QName attributeName, SimpleType type ) throws IOException {
    int ch;
    while (true) {
      ch = nextChar();
      if (ch == -1) {
        endOfInputError( "Unexpected end of input while parsing string" );
      }
      switch (ch) {
        case '{' : {
          parseError( "Simple value cannot be an object, but found '{" );
          return null;
        }
        case '[' : {
          parseError( "Simple value cannot be an array, but found '[" );
          return null;
        }
        case '"' : {
          String strValue = parseString();
          return createValue( type, strValue, attributeName.getNamespace() );
        }
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9' : {
          StringBuilder buf = new StringBuilder(ch);
          while ((ch >= '0') && (ch <= '9')) {
            buf.append( (char) ch );
            ch = nextChar();
          }
          if (ch == -1) {
            endOfInputError( "Unexpected end of input while parsing string" );
          }
          back();
          String bufString = buf.toString();
          if (type instanceof NumberType) {
            try {
              return Integer.valueOf( bufString );
            }
            catch (NumberFormatException e) {
              return bufString;
            }
          }
          else {
            return bufString;
          }
        }
        case 't' :
        case 'T' :  {
          parseFixedString( "rue", true );
          if (type instanceof BooleanType) {
            return Boolean.TRUE;
          }
          else {
            return "true";
          }
        }
        case 'f' :
        case 'F' : {
          parseFixedString( "alse", true );
          if (type instanceof BooleanType) {
            return Boolean.FALSE;
          }
          else {
            return "false";
          }
        }
        case 'n' :
        case 'N' : {
          parseFixedString( "ull", true );
          return null;
        }
      }
    }
  }

  private Model parseSimpleModel( SimpleType type ) throws IOException {
    int ch;
    ModelFactory modelFactory = ModelFactoryTable.getInstance().getFactory( type.name().getNamespace() );
    while (true) {
      ch = nextChar();
      if (ch == -1) {
        endOfInputError( "Unexpected end of input while parsing string" );
      }
      switch (ch) {
        case '{' : {
          parseError( "Simple value cannot be an object, but found '{" );
        }
        case '[' : {
          parseError( "Simple value cannot be an array, but found '[" );
        }
        case '"' : {
          String strValue = parseString();
          Object value = createValue( type, strValue, type.name().getNamespace() );
          return modelFactory.createInstance( type, value );
        }
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9' : {
          StringBuilder buf = new StringBuilder();
          while (true) {
            ch = nextChar();
            if (ch == -1) {
              endOfInputError( "Unexpected end of input while parsing string" );
            }
            else if ((ch >= '0') && (ch <= '9')) {
              buf.append( (char) ch );
            }
            else {
              Integer value = Integer.valueOf( buf.toString() );
              return modelFactory.createInstance( type, value );
            }
          }
        }
        case 't' :
        case 'T' : {
          parseFixedString( "rue", true );
          return modelFactory.createInstance( type, Boolean.TRUE );
        }
        case 'f' :
        case 'F' :  {
          parseFixedString( "alse", true );
          return modelFactory.createInstance( type, Boolean.FALSE );
        }
        case 'n' :
        case 'N' :  {
          parseFixedString( "ull", true );
          return modelFactory.createInstance( type, null );
        }
      }
    }
  }

  private void parseSimpleChildValue( Model parentModel, QName relationName, SimpleType childType ) throws IOException {
    int ch;
    ModelFactory modelFactory = ModelFactoryTable.getInstance().getFactory( childType.name().getNamespace() );
    while (true) {
      ch = nextChar();
      if (ch == -1) {
        endOfInputError( "Unexpected end of input while parsing string" );
      }
      switch (ch) {
        case '{' : {
          parseError( "Simple value cannot be an object, but found '{" );
          return;
        }
        case '[' : {
          // It might be a simple type array. Try to parse it.
          try {
            back();
            parseArray( parentModel, relationName.getName() );
          }
          catch (IOException e) {
            parseError( "Simple value cannot be an array, but found '[" );
          }
          return;
        }
        case '"' : {
          String strValue = parseString();
          Object value = createValue( childType, strValue, relationName.getNamespace() );
          parentModel.add( relationName, modelFactory.createInstance( childType, value ) );
          return;
        }
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9' : {
          StringBuilder buf = new StringBuilder();
          while (true) {
            ch = nextChar();
            if (ch == -1) {
              endOfInputError( "Unexpected end of input while parsing string" );
            }
            else if ((ch >= '0') && (ch <= '9')) {
              buf.append( (char) ch );
            }
            else {
              Integer value = Integer.valueOf( buf.toString() );
              parentModel.add( relationName, modelFactory.createInstance( childType, value ) );
              back();
              return;
            }
          }
        }
        case 't' :
        case 'T' :  {
          parseFixedString( "rue", true );
          parentModel.add( relationName, modelFactory.createInstance( childType, Boolean.TRUE ) );
          return;
        }
        case 'f' :
        case 'F' : {
          parseFixedString( "alse", true );
          parentModel.add( relationName, modelFactory.createInstance( childType, Boolean.FALSE ) );
          return;
        }
        case 'n' :
        case 'N' :  {
          parseFixedString( "ull", true );
          parentModel.add( relationName, modelFactory.createInstance( childType, null ) );
          return;
        }
      }
    }
  }

  private void parseArray( Model parentModel, String name ) throws IOException {
    int ch;
    do {
      ch = skipWhitespace( false );
      if (ch == -1) {
        endOfInputError( "Unexpected end of input while parsing array" );
      }
    } while (ch != '[');
    // Check empty array
    ch = skipWhitespace( false );
    if (ch != ']') {
      back();
      do {
        parseValue( parentModel, name );
        ch = skipWhitespace( false );
        while ((ch != ',') && (ch != ']')) {
          ch = skipWhitespace( false );
          if (ch == -1) {
            endOfInputError( "Unexpected end of input while parsing array" );
          }
        }
      } while (ch != ']');
    }
  }

  private void parseObject( Model parentModel, QName childRelName, Type childType ) throws IOException {
    int ch;
    while (true) {
      ch = nextChar();
      if (ch == -1) {
        endOfInputError( "Unexpected end of input while parsing array" );
        return;
      }
      else if (ch == '"') {
        parseError( "Expected start of object '{', but found '\"'" );
        return;
      }
      else if (ch == '[') {
        back();
        parseArray( parentModel, childRelName.getName() );
        return;
      }
      else if (ch == '{') {
        Model child;
        ModelFactory modelFactory = ModelFactoryTable.getInstance().getFactory( childType.name().getNamespace() );
        Type supportedType = modelFactory.getType( childType.name() );
        if (supportedType == null) {
          if (childType instanceof ComplexType) {
            child = new DefaultModel( null, (ComplexType) childType );
          }
          else {
            child = new SimpleModel( childType );
          }
        }
        else {
          child = modelFactory.createInstance( (ComplexType) childType );
          parentModel.add( childRelName, child );
        }
        do {
          skipUntil( '"' );
          parseMember( child );
          ch = skipWhitespace( false );
          while ((ch != ',') && (ch != '}')) {
            ch = skipWhitespace( false );
            if (ch == -1) {
              endOfInputError( "Unexpected end of input while parsing array" );
            }
          }
        } while (ch != '}');
        return;
      }
      else if ((ch == 'n') || (ch == 'N')) {
        parseFixedString( "ull", true );
        // ignore null child
        return;
      }
    }
  }

  private void parseMember( Model model ) throws IOException {
    String name = parseString();
    int ch;
    do {
      ch = skipWhitespace( false );
      if (ch == -1) {
        endOfInputError( "Unexpected end of input while parsing array" );
      }
    } while (ch != ':');
    parseValue( model, name );
  }

  private void parseValueChild( Model parentModel, QName childRelName, Type childType ) throws IOException {
    if (childType instanceof SimpleType) {
      parseSimpleChildValue( parentModel, childRelName, (SimpleType) childType );
    }
    else {
      parseObject( parentModel, childRelName, childType );
    }
  }

  private Object createValue( SimpleType type, String strValue, Namespace ns ) throws IOException {
    if (type instanceof QNameType) {
      return ns.getQName( strValue );
    }
    else if (type instanceof StringType) {
      return strValue;
    }
    else if (type instanceof IntegerType) {
      return Integer.valueOf( strValue );
    }
    else if (type instanceof BooleanType) {
      return Boolean.valueOf( strValue );
    }
    else if (type instanceof DateTimeType) {
      return type.createValue( strValue, null );
    }
    else if (type instanceof DecimalType) {
      return type.createValue( strValue, null );
    }
    else if (type instanceof BinaryType) {
      return type.createValue( strValue, null );
    }
    else if (type instanceof DurationType) {
      return type.createValue( strValue, null );
    }
    else {
      SimpleType baseType = (SimpleType) type.getBaseType();
      if (baseType == null) {
        parseError( "Unsupported SimpleType: "+type.name().getName() );
        return null;
      }
      else {
        return createValue( baseType, strValue, ns );
      }
    }
  }

  private int getAttributeIndex( ComplexType modelType, String name ) {
    for (int i = 0; i < modelType.attributeCount(); i++) {
      if (modelType.getAttributeName( i ).getName().equals( name )) {
        return i;
      }
    }
    return -1;
  }

  private Relation getChildRelation( ComplexType modelType, String name ) {
    for (QNameIterator relNameIter = modelType.relationNames(); relNameIter.hasNext();) {
      QName relName = relNameIter.next();
      if (relName.getName().equals( name )) {
        return modelType.getRelation( relName );
      }
    }
    return null;
  }


  private String parseString() throws IOException {
    int ch;
    StringBuilder buf = new StringBuilder();
    while (true) {
      ch = nextChar();
      if (ch == -1) {
        endOfInputError( "Unexpected end of input while parsing string" );
      }
      else if (ch == '\\') {
        buf.append( parseEscaped() );
      }
      else if (ch == '"') {
        return buf.toString();
      }
      else {
        buf.append( (char) ch );
      }
    }
  }

  private char parseEscaped() throws IOException {
    int ch = nextChar();
    if (ch == -1) {
      endOfInputError( "Unexpected end of input while parsing string" );
    }
    switch (ch) {
      case '"' : return '"';
      case 'b' : return '\b';
      case 'f' : return '\f';
      case 'n' : return '\n';
      case 'r' : return '\r';
      case 't' : return '\t';
      case '\\' : return '\\';
      case '/' : return '/';
      case 'u' : return parseUnicode();
      default: {
        parseError( "Unknown escape sequence starting with char='"+ch+"'" );
        return 0;
      }
    }
  }

  private char parseUnicode() throws IOException {
    StringBuilder buf = new StringBuilder();
    for (int i = 0; i < 4; i++) {
      int ch = nextChar();
      if (ch == -1) {
        endOfInputError( "Unexpected end of input while parsing string" );
      }
      // allow all HEX digits
      if (((ch >= '0') && (ch <= '9')) || ((ch >= 'a') && (ch <= 'f')) || ((ch >= 'A') && (ch <= 'F'))) {
        buf.append( (char) ch );
      }
      else {
        parseError( "Illegal character, expected digit, but char='" + ch + "'" );
      }
    }
    int charNumber = Integer.parseInt( buf.toString(), 16 );

    // The & \u0026 does not need an encoding and should be given as '&'
    // Some json libraries encode it although there is no need.
    if (charNumber == 38) { // \u0026 => 38
      return '&';
    }
    else {
      return (char) charNumber;
    }
  }
}
