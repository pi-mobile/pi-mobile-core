/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.bo.cache;

import de.pidata.bo.base.BoStorage;
import de.pidata.bo.base.Businessobject;
import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;
import de.pidata.system.base.NumberSequence;

import java.io.IOException;
import java.util.Hashtable;

public class BoCache implements BoStorage {

  private BoStorage boStorage;
  private Hashtable boTypeCaches = new Hashtable();

  public BoCache() {
    this( Businessobject.storage() );
  }

  public BoCache( BoStorage boStorage ) {
    if (boStorage == null) {
      throw new RuntimeException( "boStorage must not be null for BoCache" ); 
    }
    this.boStorage = boStorage;
    Businessobject.initStorage( this );
  }

  public NamespaceTable getNamespaces() {
    return boStorage.getNamespaces();
  }

  private Hashtable getTypeCache( ComplexType boType ) {
    Hashtable typeCache = (Hashtable) boTypeCaches.get( boType.name() );
    if (typeCache == null) {
      typeCache = new Hashtable();
      boTypeCaches.put( boType.name(), typeCache );
    }
    return typeCache;
  }

  public Businessobject load( ComplexType type, Key id ) throws IOException {
    Hashtable typeCache = getTypeCache( type );
    Businessobject bo = (Businessobject) typeCache.get( id );
    if (bo == null) {
      bo = boStorage.load( type, id );
      if (bo != null) {
        typeCache.put( id, bo );
      }
    }
    return bo;
  }

  /**
   * Loads BOs into conatiner. For all BOs already existing in container the existing BO
   * is updated from the loaded bo.
   *
   * @param container    the conatiner to add loaded BOs to
   * @param relationName loaded BOs will be added to this relation
   * @param filterAttr   attribute to apply filterValeu to odr null for no filter
   * @param comparator
   * @param filterValue  the filter value (only applicable if filterAttr is not null)
   */
  public void load( Model container, QName relationName, QName filterAttr, int comparator, String filterValue ) throws IOException {
    Root root = new Root();
    boStorage.load( root, relationName, filterAttr, comparator, filterValue );
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( relationName.getNamespace() );
    ComplexType boType = (ComplexType) factory.getRootRelation( relationName ).getChildType();
    Hashtable typeCache = getTypeCache( boType );
    for (ModelIterator resultIter = root.iterator( relationName, null ); resultIter.hasNext(); ) {
      Businessobject bo = (Businessobject) resultIter.next();
      Businessobject cacheBo = (Businessobject) typeCache.get( bo.key() );
      if (cacheBo == null) {
        root.remove( relationName, bo );
        typeCache.put( bo.key(), bo );
      }
      else {
        cacheBo.updateFrom( bo );
        if (cacheBo.getParent( false ) == null) {
          bo = cacheBo;
        }
        else {
          root.remove( relationName, bo );
        }
      }
      container.add( relationName, bo );
    }
  }

  public void store( Businessobject bo ) throws IOException {
    boStorage.store( bo );
    ComplexType boType = (ComplexType) bo.type();
    Hashtable typeCache = getTypeCache( boType );
    Businessobject cacheBo = (Businessobject) typeCache.get( bo.key() );
    if ((cacheBo != null) && (cacheBo != bo)) {
      cacheBo.updateFrom( bo );
    }
  }

  public void delete( Businessobject bo ) throws IOException {
    ComplexType boType = (ComplexType) bo.type();
    Hashtable typeCache = getTypeCache( boType );
    typeCache.remove( bo.key() );
    boStorage.delete( bo );
  }

  public void delete( ComplexType boType, Key id ) throws IOException {
    Hashtable typeCache = getTypeCache( boType );
    typeCache.remove( id );
    boStorage.delete( boType, id );
  }

  /**
   * Creates a sequence object for a sequence existing in this BoStorage
   *
   * @param name the sequence's name
   * @throws java.io.IOException if sequence does not exist in this BoStorage or cannot be read
   */
  public NumberSequence createSequence( String name ) throws IOException {
    return boStorage.createSequence( name );
  }

  /**
   * @return true if model already existed, false if the model was inserted into database
   */
  public boolean exists( ComplexType boType, Key boID ) throws IOException {
    return boStorage.exists( boType, boID );
  }

  public BoStorage getBaseStorage() {
    return boStorage;
  }
}
