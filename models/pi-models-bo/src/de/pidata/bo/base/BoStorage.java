/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.bo.base;

import de.pidata.qnames.Key;
import de.pidata.models.tree.Model;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;
import de.pidata.system.base.NumberSequence;

import java.io.IOException;

public interface BoStorage {

  public static final QName INSTANCE_NAME = Businessobject.NAMESPACE.getQName( "BoStorage" );
  public static final int COMP_EQUAL = 1;
  public static final int COMP_NOT_EQUAL = 2;

  public NamespaceTable getNamespaces();

  /**
   * Loads the bo of type having key id.
   * @param type the type
   * @param id   the key or null
   * @return the (first matching) bo  or null if not found
   * @throws IOException exception while accessing storage
   */
  public Businessobject load( ComplexType type, Key id ) throws IOException;

  /**
   * Loads BOs into conatiner. For all BOs already existing in container the existing BO
   * is updated from the loaded bo.
   *
   * @param container    the conatiner to add loaded BOs to
   * @param relationName loaded BOs will be added to this relation
   * @param filterAttr   attribute to apply filterValeu to odr null for no filter
   * @param comparator
   * @param filterValue  the filter value (only applicable if filterAttr is not null)
   */
  public void load( Model container, QName relationName, QName filterAttr, int comparator, String filterValue ) throws IOException;

  public void store( Businessobject bo ) throws IOException;

  public void delete( Businessobject bo ) throws IOException;

  public void delete( ComplexType boType, Key id ) throws IOException;

  /**
   * Creates a sequence object for a sequence existing in this BoStorage
   * @param name the sequence's name
   * @throws IOException if sequence does not exist in this BoStorage or cannot be read
   */
  public NumberSequence createSequence( String name ) throws IOException;

  /**
   * @return true if model already existed, false if the model was inserted into database
   */
  boolean exists( ComplexType boType, Key boID ) throws IOException;
}
