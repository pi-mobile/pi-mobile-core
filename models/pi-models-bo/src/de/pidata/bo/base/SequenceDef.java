// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)
// Sat Jan 17 10:58:30 CET 2009

package de.pidata.bo.base;

import de.pidata.models.tree.ChildList;
import de.pidata.qnames.Key;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.IntegerType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.StringType;

import java.util.Hashtable;

public class SequenceDef extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.pidata.de/res/bo.xsd" );

  public static final QName ID_NAME = NAMESPACE.getQName("name");
  public static final QName ID_MAX = NAMESPACE.getQName("max");
  public static final QName ID_MIN = NAMESPACE.getQName("min");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "sequenceDef" ), SequenceDef.class.getName(), 0 );
    TYPE = type;
    type.addAttributeType( ID_NAME, StringType.getDefString());
    type.addAttributeType( ID_MIN, IntegerType.getDefLong());
    type.addAttributeType( ID_MAX, IntegerType.getDefLong());
  }

  public SequenceDef() {
    super( null, TYPE, null, null, null );
  }

  public SequenceDef(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, TYPE, attributeNames, anyAttribs, childNames);
  }

  protected SequenceDef(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the attribute name.
   *
   * @return The attribute name
   */
  public String getName() {
    return (String) get( ID_NAME );
  }

  /**
   * Set the attribute name.
   *
   * @param name new value for attribute name
   */
  public void setName( String name ) {
    set( ID_NAME, name );
  }

  /**
   * Returns the attribute min.
   *
   * @return The attribute min
   */
  public Long getMin() {
    return (Long) get( ID_MIN );
  }

  /**
   * Set the attribute min.
   *
   * @param min new value for attribute min
   */
  public void setMin( Long min ) {
    set( ID_MIN, min );
  }

  /**
   * Returns the attribute max.
   *
   * @return The attribute max
   */
  public Long getMax() {
    return (Long) get( ID_MAX );
  }

  /**
   * Set the attribute max.
   *
   * @param max new value for attribute max
   */
  public void setMax( Long max ) {
    set( ID_MAX, max );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  public SequenceDef( String name, long min, long max ) {
    super( null, TYPE, null, null, null );
    setName( name );
    setMin( new Long(min) );
    setMax( new Long(max) );
  }
}
