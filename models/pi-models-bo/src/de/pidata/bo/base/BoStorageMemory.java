/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.bo.base;

import de.pidata.qnames.Key;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.tree.Root;
import de.pidata.models.tree.SimpleKey;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.NamespaceTable;
import de.pidata.models.types.Relation;
import de.pidata.qnames.QName;
import de.pidata.system.base.NumberSequence;

import java.io.IOException;

public class BoStorageMemory extends Root implements BoStorage {

  public BoStorageMemory() {
    Businessobject.initStorage( this );
  }

  public NamespaceTable getNamespaces() {
    return null;
  }

  public Businessobject load( ComplexType type, Key id ) throws IOException {
    return (Businessobject) get( type.name(), new SimpleKey(id) );
  }

  public void load( Model container, QName relationName, QName filterAttr, int comparator, String filterValue ) throws IOException {
    if ((filterAttr != null) && (comparator != COMP_EQUAL)) {
      throw new RuntimeException( "Comparator not supported: "+comparator );
    }
    Relation relation = ((ComplexType) container.type()).getRelation( relationName );
    for (ModelIterator boIter = iterator( relation.getChildType().name(), null ); boIter.hasNext(); ) {
      Businessobject bo = (Businessobject) boIter.next();
      if ((filterAttr == null) || ((bo.get(filterAttr) == null) && (filterValue == null))
          || (filterValue.equals( bo.get(filterAttr).toString() ))) {
        Businessobject oldBo = (Businessobject) container.get( relationName, bo.key() );
        if (oldBo != null) {
          oldBo.updateFrom( bo );
        }
        else {
          container.add( relationName, bo.clone( null, true, true ) );
        }
      }
    }
  }

  public void store( Businessobject bo ) throws IOException {
    add( bo.type().name(), bo.clone( null, true, false ) );
  }

  public void delete( Businessobject bo ) throws IOException {
    Businessobject delBo = (Businessobject) get( bo.type().name(), bo.key() );
    if (delBo != null) {
      remove( bo.type().name(), delBo );
    }
  }

  public void delete( ComplexType boType, Key id ) throws IOException {
    QName typeName = boType.name();
    Businessobject delBo = (Businessobject) get( typeName, new SimpleKey( id ) );
    if (delBo != null) {
      remove( typeName, delBo );
    }
  }

  public NumberSequence createSequence( String name ) throws IOException {
    throw new IOException( "Sequence currently not supported for BoStorageMemory" );
  }

  public boolean exists( ComplexType boType, Key boID ) throws IOException {
    Businessobject bo = (Businessobject) get( type.name(), new SimpleKey( boID ) );
    return (bo != null);
  }
}
