/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.bo.xml;

import de.pidata.system.base.NumberSequence;
import de.pidata.system.base.Storage;
import de.pidata.stream.StreamHelper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class BoSequenceXml extends NumberSequence {

  protected Storage xmlStorage;

  /**
   * Loads the sequence from xmlStorage
   * @param name        name of a sequence existing in xmlStorage
   * @param xmlStorage  the storage to load sequence from and store it when changed
   */
  public BoSequenceXml( String name, Storage xmlStorage ) throws IOException {
    super( name, 0, 0, NumberSequence.KEY_TYPE_LONG );
    this.xmlStorage = xmlStorage;
    String fileName = getFileName();
    if (!xmlStorage.exists( fileName )) {
      throw new IllegalArgumentException( "Sequence file is missing, name="+name+", storage="+xmlStorage.getName() );
    }
    readFile( fileName );
  }

  /**
   * Creates a new sequence and stores it in xmlStorage
   * @param name        name of the sequence to be created in xmlStorage
   * @param xmlStorage  the storage to use for sequence now and when it changes
   * @param min         min (strting) value for the new sequence
   * @param max         max value for the new sequence
   * @param keyType     key type to use, one of constants NumberSequence.KEY_TYPE_*
   */
  public BoSequenceXml( String name, Storage xmlStorage, long min, long max, int keyType ) throws IOException {
    super( name, min, max, keyType );
    this.xmlStorage = xmlStorage;
    writeFile( getFileName() );
  }

  /**
   * Updates this sequence with a new min and max value and resets percent left to 100%
   *
   * @param min new min value
   * @param max new max value
   */
  public void update( long min, long max ) throws IOException {
    super.update( min, max );
    writeFile( getFileName() );
  }

  public void setUpdateRequested( boolean updateRequested ) throws IOException {
    super.setUpdateRequested( updateRequested );
    writeFile( getFileName() );
  }

  /**
   * Increments the sequence by count and returns the first new value
   *
   * @param count number of values needed
   * @return next sequence value or first value if count is greater than 1
   * @throws IllegalArgumentException if there is no next value in this sequence, i.e next is greater than max()
   */
  public long next( int count ) throws IOException {
    String fileName;
    fileName = getFileName();
    if (xmlStorage.exists( fileName )) {
      readFile( fileName );
    }
    if ((this.last+count) >= max) {
      throw new IllegalArgumentException( "Sequence '" + name + "' has not enough values for count="+count+", last="+last+", max=" + max );
    }
    long first = last + 1;
    last += count;
    writeFile( fileName );
    return first;
  }

  private void writeFile( String fileName ) throws IOException {
    OutputStream out = null;
    try {
      out = xmlStorage.write( fileName, true, false );
      OutputStreamWriter pw = new OutputStreamWriter( out );
      pw.write( "" + last + "\n" );
      pw.write( "" + min + "\n" );
      pw.write( "" + max + "\n" );
      if (updateRequested) pw.write( "X\n" );
      else pw.write( "-\n" );
      pw.flush();
    }
    finally {
      StreamHelper.close( out );
    }
  }

  private void readFile( String fileName ) throws IOException {
    InputStream is = null;
    try {
      is = xmlStorage.read( fileName );
      String line = StreamHelper.readLine( is );
      last = Long.parseLong( line );
      line = StreamHelper.readLine( is );
      min = Long.parseLong( line );
      line = StreamHelper.readLine( is );
      max = Long.parseLong( line );
      line = StreamHelper.readLine( is );
      updateRequested = "X".equals(line);
    }
    finally {
      StreamHelper.close( is );
    }
  }

  private String getFileName() {
    String fileName;
    if (name == null) fileName = "default.seq";
    else fileName = name.toLowerCase()+ ".seq";
    return fileName;
  }
}
