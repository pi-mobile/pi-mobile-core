/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.bo.xml;

import de.pidata.bo.base.BoStorage;
import de.pidata.bo.base.Businessobject;
import de.pidata.models.tree.AbstractModelFactory;
import de.pidata.qnames.Key;
import de.pidata.models.tree.Model;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.models.xml.binder.XmlWriter;
import de.pidata.system.base.NumberSequence;
import de.pidata.system.base.Storage;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.SystemManager;

import java.io.IOException;
import java.io.InputStream;

public class BoStorageXml implements BoStorage {

  public static char ID_DELMIITER = '-';

  private Storage xmlStorage;
  private NamespaceTable namespaces;

  public BoStorageXml() {
    this( SystemManager.getInstance().getStorage( "data" ) );
  }

  public BoStorageXml( Storage xmlStorage ) {
    this.xmlStorage = xmlStorage;
    loadNamespaces();
    Businessobject.initStorage( this );
  }

  public NamespaceTable getNamespaces() {
    return namespaces;
  }

  private void loadNamespaces() {
    namespaces = new NamespaceTable();
    InputStream is = null;
    try {
      if (xmlStorage.exists( "namespaces" )) {
        is = xmlStorage.read( "namespaces" );
        if (is != null) {
          String prefix, namespace;
          String line = StreamHelper.readLine( is );
          int pos = line.indexOf( '=' );
          if (pos == 0) {
            prefix = "";
          }
          else {
            prefix = line.substring( 0, pos );
          }
          namespace = line.substring( pos+1 );
          namespaces.addNamespace( Namespace.getInstance( namespace ), prefix );
        }
      }  
    }
    catch (IOException e) {
      // do nothing
    }
    finally {
      StreamHelper.close( is );
    }
  }

  private String createFileName( QName typeName, Key id ) {
    return typeName.getName() + ID_DELMIITER + id.toKeyString( namespaces ) + ".xml";
  }

  private Businessobject readBo( String fileName ) throws IOException {
    InputStream is = null;
    try {
      if (xmlStorage.exists( fileName )) {
        is = xmlStorage.read( fileName );
        XmlReader reader = new XmlReader();
        return (Businessobject) reader.loadData(is, null);
      }
      else {
        return null;
      }
    }
    finally {
      StreamHelper.close( is );
    }
  }

  /**
   * Loads the bo of type having key id.
   * @param type the type
   * @param id   the key or null
   * @return the (first matching) bo  or null if not found
   * @throws IOException exception while accessing storage
   */
  public Businessobject load( ComplexType type, Key id ) throws IOException {
    String boName = type.name().getName();
    if (id == null) {
      String[] boFiles = xmlStorage.list(); //TODO optimieren durch typeName*-*.xml
      int i = 0;
      while ((i < boFiles.length) && (id == null)) {
        int posExt = boFiles[i].lastIndexOf( '.' );
        if ((posExt > 0) && "xml".equals(boFiles[i].substring( posExt+1 ))) {
          int posID = boFiles[i].lastIndexOf( BoStorageXml.ID_DELMIITER );
          String typeName = boFiles[i].substring( 0, posID );
          if (typeName.equals( boName )) {
            id = AbstractModelFactory.createKey( type, boFiles[i].substring( posID+1, posExt ), namespaces );
          }
        }
        i++;
      }
      if (id == null) return null;
    }
    String fileName = createFileName( type.name(), id );
    return readBo( fileName );
  }

  /**
   * Loads BOs into conatiner. For all BOs already existing in container the existing BO
   * is updated from the loaded bo.
   *
   * @param container    the conatiner to add loaded BOs to
   * @param relationName loaded BOs will be added to this relation
   * @param filterAttr   attribute to apply filterValeu to odr null for no filter
   * @param comparator
   * @param filterValue  the filter value (only applicable if filterAttr is not null)
   */
  public void load( Model container, QName relationName, QName filterAttr, int comparator, String filterValue ) throws IOException {
    ComplexType containerType = (ComplexType) container.type();
    String boName = containerType.getChildType( relationName ).name().getName();
    ComplexType type = (ComplexType) containerType.getChildType( relationName );

    String[] boFiles = xmlStorage.list(); //TODO optimieren durch typeName*-*.xml
    for (int i = 0; i < boFiles.length; i++) {
      int posExt = boFiles[i].lastIndexOf( '.' );
      if ((posExt > 0) && "xml".equals(boFiles[i].substring( posExt+1 ))) {
        int posID = boFiles[i].lastIndexOf( BoStorageXml.ID_DELMIITER );
        String typeName = boFiles[i].substring( 0, posID );
        if (typeName.equals( boName )) {
          Key id = AbstractModelFactory.createKey( type, boFiles[i].substring( posID+1, posExt ), namespaces );
          Businessobject bo = load( type, id );

          boolean accept;
          if (filterAttr == null) {
            accept = true;
          }
          else {
            Object boValue = bo.get(filterAttr);
            if (boValue == null) {
              switch (comparator) {
                case COMP_EQUAL: accept = (filterValue == null); break;
                case COMP_NOT_EQUAL: accept = (filterValue != null); break;
                default: throw new RuntimeException( "Comparator not supported: "+comparator );
              }
            }
            else {
              String strValue;
              if (boValue instanceof QName) {
                strValue = ((QName) boValue).toString( namespaces );
              }
              else {
                strValue = boValue.toString();
              }
              switch (comparator) {
                case COMP_EQUAL: accept = filterValue.equals( strValue ); break;
                case COMP_NOT_EQUAL: accept = !(filterValue.equals( strValue )); break;
                default: throw new RuntimeException( "Comparator not supported: "+comparator );
              }
            }
          }
          
          if (accept) {
            Businessobject oldBo = (Businessobject) container.get( relationName, bo.key() );
            if (oldBo != null) {
              oldBo.updateFrom( bo );
            }
            else {
              container.add( relationName, bo );
            }  
          }
        }
      }
    }
  }

  public void store( Businessobject bo ) throws IOException {
    String fileName = createFileName( bo.type().name(), bo.key() );
    XmlWriter writer = new XmlWriter( xmlStorage );
    writer.write( fileName, bo, bo.type().name(), true );
  }

  public void delete( Businessobject bo ) throws IOException {
    String fileName = createFileName( bo.type().name(), bo.key() );
    xmlStorage.delete( fileName );
  }

  public void delete( ComplexType boType, Key id ) throws IOException {
    String fileName = createFileName( boType.name(), id );
    xmlStorage.delete( fileName );
  }

  /**
   * @return true if model already existed, false if the model was inserted into database
   */
  public boolean exists( ComplexType boType, Key boID ) throws IOException {
    String fileName = createFileName( boType.name(), boID );
    return xmlStorage.exists( fileName );
  }

  /**
   * Creates a sequence object for a sequence existing in this BoStorage
   * @param sequenceName the sequence's name
   * @throws IOException if sequence does not exist in this BoStorage or cannot be read
   */
  public NumberSequence createSequence( String sequenceName ) throws IOException {
    return new BoSequenceXml( sequenceName, this.xmlStorage );
  }
}
