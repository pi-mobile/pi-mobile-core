# PI-Mobile History

In many parts the history of PI-Mobile is the history of PI-Data, so you might alternatively read these german pages:

- [PI-Data Histroie](http://www.pi-data.de/dark/historie.html) 
- [PI-Data Produkt PI-Mobile](http://www.pi-data.de/dark/pi-mobile.html) 

The roots of PI-Mobile are in year 2000 when we started with our first framework for platform neutral application development in Java.
The company Wearix no longer exists, but the experience collected with a team of 20 developers went into the architecture of PI-Mobile. 

The first and still active application ["Mobile Montage"](http://www.pi-data.de/dark/mobile-montage.html) basing on PI-Mobile is in productive use since beginning of 2004.
When writing these lines in 2020 we just started test phase of a new version of Mobile Montage with a redesigned modern UI.

From 2000 until 2008 we had a lot of diploma thesis all within the context of mobile application development.
What SAP and the other big software players learned the next 10 years we already knew in 2001.

Marketing and sales was not our strength, so we never managed to get much customers but especially [SchwörerHaus](https://www.schwoererhaus.de/), for with we developed "Mobile Montage" were very happy.
Currently SchwörerHaus uses 3 Apps and one App interface developed by PI-Data and based on PI-Mobile.
The apps were launched in 2004, 2006 and 2019.   
