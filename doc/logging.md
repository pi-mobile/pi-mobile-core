# Logging

PI-Mobile provides a very lightweight logging mechanism.

The main component is the static [Logger](../core/pi-core-basics/src/de/pidata/log/Logger.java) class which works in two roles:
- keep track of the registered loggers as kind of a `LogManager`
- employ the registered loggers with logging requests

## Usage

The `Logger` is initialized during PI-Mobile system creation, e.g. `DesktopSystem`, `ServerSystem`, ...

To handle application specific logging instantiate and register one or more loggers which must implement the interface `LoggerInterface`, e.g.

```
DefaultLogger logger = new DefaultLogger( Level.DEBUG );
Logger.addLogger( loggger );
```

```
String filename = "createTheFilename";
int logfileExpireDays = 1;
int logfilePurgeDays = 30;
FilebasedRollingLogger logger = new FilebasedRollingLogger( logfileName, logfileExpireDays, logfilePurgeDays, Level.DEBUG );
Logger.addLogger( logger );
```

Request a log entry by calling one of the static methods, e.g

```
Logger.info( "here comes the message" ) ;

Logger.error( "here comes the message", ex ) ;
```

## Integrate with java.util.logging

To attach the default Java logging a `DefaultHandler` is provided which forwards all log requests to the PI-Mobile logger.

Usage:

```
java.util.logging.Logger Logger rootLogger = LogManager.getLogManager().getLogger( "" );
Handler[] handlers = rootLogger.getHandlers(); 
// enable console logging via PI-Mobile handler
for (Handler handler : handlers) {
  if (handler instanceof ConsoleHandler) {
    rootLogger.removeHandler( handler );
  }
}
rootLogger.addHandler( new DefaultHandler() );
```

### Implement more Handlers

The following prototype implementations demonstrate how new handlers can be defined and used to benefit from the lightweight PI-Mobile logging::
- [ConsolHandler](../core/pi-core-basics/src/de/pidata/log/ConsoleHandler.java)
- [FilebasedRollingHandler](../core/pi-core-files/src/de/pidata/file/FilebasedRollingHandler.java)
- [DefaultFormatter](../core/pi-core-basics/src/de/pidata/log/DefaultFormatter.java)

Change the entries in the existing `logging.properties` or create a new one as needed.

**IMPORTANT:** do not configure java.util.logging if it is not needed for any other purpose, e.g. third party library. 
It will just add huge overhead to logging.

### Use

To use the lightweight PI-Mobile logging change the handlers entry to use the provided handler, e.g.

```
handlers = de.pidata.file.FilebasedRollingHandler, de.pidata.log.ConsoleHandler
```

To have the default logging entries look like the PI-Mobile entries use the provided Formatter, e.g.

```
java.util.logging.FileHandler.formatter = de.pidata.log.DefaultFormatter
java.util.logging.ConsoleHandler.formatter = de.pidata.log.DefaultFormatter
```

### Known Problem

If the handlers are not recognized check and try the following:

- The desired handler must be in the system classpath.
- Check if the configuration is loaded.
- As the OpenJDK implementation does not take the context class into account further actions might be necessary, see https://bugs.openjdk.java.net/browse/JDK-6448699.
Try to force the `LogManager` to reconfigure:
  
```
LogManager.getLogManager().readConfiguration( getClass().getResourceAsStream( "path/to/logging.properties" );
```
