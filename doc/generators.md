# PI-Mobile Generators

Each of the generators displays a short usage information if called without parameters.
For detailed description see the documentation inside the generator class.
For examples on how to use the generators see the sample run configurations provided within the IntelliJ IDEA project.

## Generate Java Code
- [Schema2Class](../tools/pi-tools-generator/src/de/pidata/models/generator/Schema2Class.java): generates Java model classes (.java) from XML Schema

## Generate C++ Code
- [Schema2cpp](../tools/pi-tools-generator/src/de/pidata/models/cppgenerator/Schema2cpp.java): generates C++ model code (.cpp, .h) from XML Schema

## Generate XML Schema
- [Xml2Schema](../tools/pi-tools-generator/src/de/pidata/tools/Xml2Schema.java): generates XML Schema (.xsd) describing a given XML file
- [Java2Schema](../tools/pi-tools-generator/src/de/pidata/tools/Java2Schema.java): generates XML Schema (.xsd) describing the model represented by a given Java model class file 
