# PI-Mobile-Core

This project contains all PI-Mobile core modules. 
These modules provide the platform independent part of PI-Mobile.

## License

This project is licensed under GPL v3 (see file LICENCE).

## Main features

- Extended model view controller (MVC) pattern for user interface
- UI implementations for JavaFX, Android and iOS (with help of [Google j2objc](https://developers.google.com/j2objc))
- Support for lightweight mobile Apps as well as rich featured desktop applications 
- Lightweight server application (servlet) for connection to backend systems, e.g. SAP
- XML Schema based data type definition
- One of the fastest and least memory consuming XML parser/binder
- Background data synchronization between client and server
- Lightweight [logging](doc/logging.md) 

Lots of [code generators](doc/generators.md) to make life easy: 
- Generate model classes and database DDL
- Generate service interfaces from WSDL description
- Generate XML Schema and WSDL from UML (Enterprise Architect)
- Generated SAP interface 

## Examples

For examples how to use PI-Mobile see our example projects:
- [PI-Mobile-FXSample](https://gitlab.com/pi-mobile/pi-mobile-FXsamples) contains several JavaFX examples.

If you want to see a **big project using PI-Mobile** take a look at [PI-Rail](https://gitlab.com/pi-rail). 

## Philosophy and Architecture

Goal of this chapter is to make you understand the basic ideas and architecture auf the PI-Mobile framework.

The motivation of PI-Mobile is to **fulfil the Java promise "write once, run anywhere"**.
 
The roots of PI-Mobile are in year 2000 when we started with our first framework for platform neutral application development in Java.
**PI-Mobile itself was started in 2005** and replaced a first version of 2003 with same API.
The oldest still active application ["Mobile Montage"](http://www.pi-data.de/dark/mobile-montage.html) is in productive use since beginning of 2004.
In the early 2000s performance really mattered, so some design decisions in PI-Mobile were driven by performance. 
More about history can be found on our [history page](doc/history.md).

The UI concept is based on the MVC (Model View Controller) pattern:
- Models are defined in XML Schema files. The [Schema2Class](tools/pi-tools-generator/src/de/pidata/models/generator/Schema2Class.java) Generator creates Java classes from the .xsd files.
- Generated model classes can be extended by manual code and transient attributes.
- The dialog structure of controller and view is defined in a specific XML file. In that file also the binding between ui and model is defined. 
- The visual part of the UI is created using native UI designer tools like JavaFX SceneBuilder. By giving an UI element (e.g. a TextField) an ID it can be attached to its view.

Extensive use of XML is a central feature of PI-Mobile:
- The parser/binder is very fast.
- For each XML file a XML Schema is required - simple parsing like SAX is not supported.
- XML Reader and Writer are available for InputStreams and so also for files
- A simple SQL OR-Mapper is also available 

## Source Code Structure

The PI-Mobile framework is splitted in this project (pi-mobile-core) and one additional project for each UI platform
- [pi-mobile-fx](https://gitlab.com/pi-mobile/pi-mobile-fx) for Desktop using JavaFX
- [pi-mobile.android](https://gitlab.com/pi-mobile/pi-mobile-android) for Android SmartPhones and Tablets
- A project for Apple iOS is coming soon

Each project itself is splittet in multiple modules having partially dependecies to each other and for the UI platforms also to modules of pi-mobile-core.

The IntelliJ IDEA projects are expecting that all projects have been checked out to a directory having the same name as the repository and all below one common parent directory:
```
.
+-- pi-mobile-android
|   +-- pi-android-base
|   |   +-- ...
|   +-- pi-android-bluetooth
|   |   +-- ...
|   +-- LICENSE
|   +-- README.md
+-- pi-mobile-core
|   +-- ...
+-- pi-mobile-fx
|   +-- ...
+-- pi-rail-base
|   +-- ...
+-- pi-rail-client
|   +-- ...
+-- pi-rail-fx
    +-- ...
```           

 