/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.connect.ip;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by pru on 15.05.16.
 */
public class IPHelper {

  private static final Pattern IPV4_PATTERN = Pattern.compile("^(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}$");

  public static boolean isIPv4Address( String input ) {
    return IPV4_PATTERN.matcher(input).matches();
  }

  /**
   * Get IP address from first non-localhost interface
   *
   * @param useIPv4 true=return ipv4, false=return ipv6
   * @return address or empty string
   */
  public static InetAddress getMyIP( boolean useIPv4 ) {
    try {
      List<NetworkInterface> interfaces = Collections.list( NetworkInterface.getNetworkInterfaces() );
      for (NetworkInterface intf : interfaces) {
        List<InetAddress> addrs = Collections.list( intf.getInetAddresses() );
        for (InetAddress addr : addrs) {
          if (!addr.isLoopbackAddress()) {
            if (useIPv4) {
              String sAddr = addr.getHostAddress().toUpperCase();
              boolean isIPv4 = isIPv4Address( sAddr );
              if (isIPv4) {
                return addr;
              }
            }
            else {
              return addr;
            }
          }
        }
      }
    }
    catch (Exception ex) {
      // for now eat exceptions
    }
    return null;
  }

  public static String getMyIPAddress( boolean useIPv4 ) {
    InetAddress addr = getMyIP( useIPv4 );
    if (addr != null) {
      return "";
    }
    else {
      String sAddr = addr.getHostAddress().toUpperCase();
      boolean isIPv4 = isIPv4Address( sAddr );
      if (isIPv4) {
        return sAddr;
      }
      else {
        int delim = sAddr.indexOf( '%' ); // drop ip6 port suffix
        if (delim < 0) {
          return sAddr;
        }
        else {
          return sAddr.substring( 0, delim );
        }
      }
    }
  }

  /**
   * Get address from first non-localhost interface starting with prefix
   * @return  address or empty string
   */
  public static String getMyIPAddress( String prefix ) {
    try {
      java.util.List<NetworkInterface> interfaces = Collections.list( NetworkInterface.getNetworkInterfaces() );
      for (NetworkInterface intf : interfaces) {
        java.util.List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
        for (InetAddress addr : addrs) {
          if (!addr.isLoopbackAddress()) {
            String ip = addr.getHostAddress().toUpperCase();
            if ((prefix == null) || ip.startsWith( prefix )) {
              return addr.getHostName().toUpperCase();
            }
          }
        }
      }
    }
    catch (Exception ex) {
      // for now eat exceptions
    }
    return null;
  }

}
