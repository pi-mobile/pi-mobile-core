/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.connect.socket;

import de.pidata.connect.base.AbstractConnection;
import de.pidata.connect.base.ConnectionController;
import de.pidata.connect.base.ConnectionListener;
import de.pidata.connect.stream.*;
import de.pidata.log.Logger;
import de.pidata.stream.StreamHelper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by pru on 28.04.17.
 */
public class SocketConnection extends AbstractConnection implements StreamConnection, ConnectionController, Runnable {

  public static final int READ_LOOP_IDLE_SLEEP_MILLIS = 50;
  public static final int TIMEOUT_MILLIS = -1;

  //--- List of connection steps
  public static final String STEP_CREATE_SOCKET = "Create Socket";
  public static final String STEP_OPEN_STREAM   = "Open Socket Stream";
  public static final String STEP_START_RECEIVE = "Start Receive";

  protected String connectionURL;
  protected String host;
  protected int port;
  protected StreamHandler streamHandler;
  private StreamReceiver streamReceiver;
  protected Socket socket = null;
  private InputStream inputStream;
  private OutputStream outputStream;
  protected ResourceBundle resourceBundle;

  public SocketConnection( String connectionURL, String host, int port, StreamHandler streamHandler ) {
    this.connectionURL = connectionURL;
    this.host = host;
    this.port = port;
    this.streamHandler = streamHandler;
    resourceBundle = ResourceBundle.getBundle( "connectionSteps", Locale.GERMAN );
  }

  /**
   * Returns unique ID for this connection, e.g. URL plus counter
   *
   * @return unique ID for this connection
   */
  @Override
  public String getConnectionID() {
    return connectionURL;
  }

  /**
   * Called by connectionListener to get all connection steps in order.
   * Implementation has to call connectionListener.addStep() in correct
   * order for each step.
   *
   * The list of steps are exact that steps passed when establishing a
   * connection. While connection each registered listener will be called
   * on success or failure of these steps.
   *
   * @param connectionListener listener to add steps to
   */
  @Override
  public void getConnectionSteps( ConnectionListener connectionListener ) {
    connectionListener.addStep( this, STEP_CREATE_SOCKET );
    connectionListener.addStep( this, STEP_OPEN_STREAM );
    connectionListener.addStep( this, STEP_START_RECEIVE );
  }

  @Override
  public String connect() throws IOException {
    try {
      resetConnectionSteps();
      startConnectionStep( STEP_CREATE_SOCKET, host+":"+port );
      socket = new Socket( host, port );
      socket.setSoTimeout( 5000 );
      Logger.info( "Socket created for "+host+":"+port );
      Runtime.getRuntime().addShutdownHook( new Thread( this ) );
    }
    catch (UnknownHostException e) {
      errorConnectionStep( e.getMessage(), resourceBundle.getString( "socket.connectUsb" ) );
      throw new IOException( "failed to connect to Dongle (Unknown host)", e );
    }
    catch (Exception ex) {
      errorConnectionStep( ex.getMessage(), resourceBundle.getString( "socket.connectUsb" ) );
      throw new IOException( "failed to connect to Dongle ", ex );
    }
    return null;
  }

  /**
   * Shutdown hook
   */
  public void run() {
    close();
  }

  public void start() throws IOException {
    InetAddress address = socket.getInetAddress();
    Logger.info( "SocketConnection["+getConnectionID()+"]: connect to address=" + address.getHostAddress() + ", name=" + address.getHostName() );
    startConnectionStep( STEP_OPEN_STREAM );
    inputStream = socket.getInputStream();
    startConnectionStep( STEP_START_RECEIVE );
    streamReceiver = new StreamReceiver( this, READ_LOOP_IDLE_SLEEP_MILLIS, TIMEOUT_MILLIS );
    streamReceiver.startReceive( address.getHostAddress(), inputStream, streamHandler );
    successConnectionStep( null );
    setStateConnected();
  }

  @Override
  public OutputStream getOutputStream() throws IOException {
    if (outputStream == null) {
      outputStream = socket.getOutputStream();
    }
    return outputStream;
  }

  @Override
  public void disconnect() {
    close();
  }

  @Override
  public boolean isConnected() {
    return (socket != null);
  }

  /**
   * Schließt die Verbindung --> STATE_CLOSED
   */
  @Override
  public void close() {
    if (streamReceiver != null) {
      streamReceiver.stop();
      streamReceiver = null;
    }
    StreamHelper.close( inputStream );
    inputStream = null;
    StreamHelper.close( outputStream );
    outputStream = null;
    try {
      if (socket != null) {
        socket.close();
      }
    }
    catch (Exception ex) {
      Logger.error( "Exception closing IP socket", ex );
    }
    socket = null;
    setStateDisconnected();
  }

  public static void main( String[] args ) {
    SocketConnection socketConnection = null;
    try {
      int port = Integer.parseInt( args[1] );
      String connID = "Test";
      MessageHandler msgHandler = new MessageHandler() {
        @Override
        public void connected( StreamHandler requestHandler ) {
          Logger.info( "Connected" );
        }

        @Override
        public void disconnected( StreamHandler requestHandler ) {
          Logger.info( "Disconnected" );
        }

        @Override
        public void received( StringBuilder messageBuffer, StreamHandler requestHandler ) {
          Logger.info( "Received: "+messageBuffer.toString() );
        }
      };
      MessageSplitter messageSplitter = new MessageSplitter( connID, null, "\n", msgHandler );
      socketConnection = new SocketConnection( "Test", args[0], port, messageSplitter );
      socketConnection.connect();
      socketConnection.start();
      OutputStream outputStream = socketConnection.outputStream;

      while (true) {
        outputStream.write( "ping\n".getBytes() );
        outputStream.flush();
        Thread.sleep( 1000 );
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    finally {
      if (socketConnection != null) {
        socketConnection.close();
      }
    }
  }
}
