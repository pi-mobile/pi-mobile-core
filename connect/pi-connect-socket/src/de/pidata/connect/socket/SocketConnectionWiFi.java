/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.connect.socket;

import de.pidata.connect.stream.StreamHandler;
import de.pidata.log.Logger;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by pru on 28.04.17.
 */
public class SocketConnectionWiFi extends SocketConnection {


  public SocketConnectionWiFi( String connectionURL, String host, int port, StreamHandler streamHandler ) {
    super(connectionURL, host, port, streamHandler);
  }


  @Override
  public String connect() throws IOException {
    try {
      resetConnectionSteps();
      startConnectionStep( STEP_CREATE_SOCKET, host+":"+port );
      socket = new Socket( host, port );
      socket.setSoTimeout( 5000 );
      Logger.info( "Socket created for "+host+":"+port );
      Runtime.getRuntime().addShutdownHook( new Thread( this ) );
    }
    catch (UnknownHostException e) {
      errorConnectionStep( e.getMessage(), resourceBundle.getString( "socket.connectWiFi" ) );
      throw new IOException( "failed to connect to Dongle-Wifi (Unknown host)", e );
    }
    catch (Exception ex) {
      errorConnectionStep( ex.getMessage(), resourceBundle.getString( "socket.connectWiFi" ) );
      throw new IOException( "failed to connect to Dongle-Wifi", ex );
    }
    return null;
  }


}
