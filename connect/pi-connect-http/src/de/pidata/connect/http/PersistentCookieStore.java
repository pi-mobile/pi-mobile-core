package de.pidata.connect.http;

import de.pidata.log.Logger;

import java.io.*;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;


public class PersistentCookieStore implements CookieStore {

  private static final String COOKIE_PREFS = "CookiePrefs";

  private final HashMap<String, ConcurrentHashMap<String, HttpCookie>> cookieMap;
  private final Preferences cookiePrefs;


  public PersistentCookieStore() {
    cookiePrefs = Preferences.userRoot().node( this.getClass().getName() + "_" + COOKIE_PREFS );
    cookieMap = new HashMap<String, ConcurrentHashMap<String, HttpCookie>>();

    try {
      for (String uriName : cookiePrefs.childrenNames()) {
        Preferences uriPref = cookiePrefs.node( uriName );

        for (String cookieName : uriPref.keys()) {
          String encodedCookie = uriPref.get( cookieName, null );
          if (encodedCookie != null) {
            HttpCookie decodedCookie = decodeCookie( encodedCookie );
            if (decodedCookie != null) {
              if (!cookieMap.containsKey( uriName )) {
                cookieMap.put( uriName, new ConcurrentHashMap<String, HttpCookie>() );
              }
              cookieMap.get( uriName ).put( cookieName, decodedCookie );
            }
          }
        }
      }
    }
    catch (BackingStoreException e) {
      Logger.error( "Unable to load persistent cookie store", e );
    }
  }

  @Override
  public void add( URI uri, HttpCookie cookie ) {
    String name = getCookieToken( uri, cookie );

    // Save cookie into local store, or remove if expired
    if (!cookie.hasExpired()) {
      if (!cookieMap.containsKey( uri.getHost() )) {
        cookieMap.put( uri.getHost(), new ConcurrentHashMap<String, HttpCookie>() );
      }
      cookieMap.get( uri.getHost() ).put( name, cookie );
    }
    else {
      if (cookieMap.containsKey( uri.toString() )) {
        cookieMap.get( uri.getHost() ).remove( name );
      }
    }

    // Save cookie into persistent store
    Preferences uriPref = cookiePrefs.node( uri.getHost() );
    uriPref.put( name, encodeCookie( new SerializableHttpCookie( cookie ) ) );
  }

  protected String getCookieToken( URI uri, HttpCookie cookie ) {
    return cookie.getName() + cookie.getDomain();
  }

  @Override
  public List<HttpCookie> get( URI uri ) {
    ArrayList<HttpCookie> ret = new ArrayList<HttpCookie>();
    if (cookieMap.containsKey( uri.getHost() ))
      ret.addAll( cookieMap.get( uri.getHost() ).values() );
    return ret;
  }

  @Override
  public boolean removeAll() {
    try {
      // Clear persistent cookies
      cookiePrefs.clear();
      for (String uriName : cookiePrefs.childrenNames()) {
        Preferences uriPref = cookiePrefs.node( uriName );
        uriPref.removeNode();
      }
    }
    catch (BackingStoreException e) {
      Logger.error( "Unable to clear persistent cookies", e );
    }
    cookieMap.clear();
    return true;
  }


  @Override
  public boolean remove( URI uri, HttpCookie cookie ) {

    boolean success = false;

    if (uri == null && cookie == null) {
      return false;
    }
    else if (uri != null && cookie == null) {
      // Remove the whole node
      if (cookieMap.containsKey( uri.getHost() )) {
        cookieMap.remove( uri.getHost() );

        // Remove persistent cookie
        try {
          cookiePrefs.node( uri.getHost() ).removeNode();
          success = true;
        }
        catch (BackingStoreException e) {
          Logger.error( "Unable to delete persistent node", e );
        }
      }
    }
    // Try to remove with given uri
    else {
      String name = getCookieToken( uri, cookie );

      if (uri != null) {
        if (cookieMap.containsKey( uri.getHost() ) && cookieMap.get( uri.getHost() ).containsKey( name )) {
          cookieMap.get( uri.getHost() ).remove( name );

          // Remove persistent cookie
          cookiePrefs.node( uri.getHost() ).remove( name );
          success = true;
        }
      }
      // Try to remove without given uri
      else {
        for (String key : cookieMap.keySet()) {
          ConcurrentHashMap<String, HttpCookie> cookies = cookieMap.get( key );
          if (cookies.containsKey( name )) {
            cookies.remove( name );
            success = true;
            break;
          }
        }

        try {
          for (String uriName : cookiePrefs.childrenNames()) {
            Preferences uriPref = cookiePrefs.node( uriName );
            for (String cookieName : uriPref.keys()) {
              if (cookieName.equals( name )) {
                uriPref.remove( cookieName );
                success = true;
                break;
              }
            }
          }
        }
        catch (BackingStoreException e) {
          Logger.error( "Unable to load persistent cookie store", e );
          success = false;
        }
      }
    }

    return success;

  }

  @Override
  public List<HttpCookie> getCookies() {
    ArrayList<HttpCookie> ret = new ArrayList<HttpCookie>();
    for (String key : cookieMap.keySet()) {
      ret.addAll( cookieMap.get( key ).values() );
    }

    return ret;
  }

  @Override
  public List<URI> getURIs() {
    ArrayList<URI> ret = new ArrayList<URI>();
    for (String key : cookieMap.keySet()) {
      try {
        ret.add( new URI( key ) );
      }
      catch (URISyntaxException e) {
        Logger.error( e.getLocalizedMessage(), e );
      }
    }

    return ret;
  }

  /**
   * Serializes Cookie object into String
   *
   * @param cookie cookie to be encoded, can be null
   * @return cookie encoded as String
   */
  protected String encodeCookie( SerializableHttpCookie cookie ) {
    if (cookie == null)
      return null;
    ByteArrayOutputStream os = new ByteArrayOutputStream();
    try {
      ObjectOutputStream outputStream = new ObjectOutputStream( os );
      outputStream.writeObject( cookie );
    }
    catch (IOException e) {
      Logger.error( "IOException in encodeCookie", e );
      return null;
    }

    return byteArrayToHexString( os.toByteArray() );
  }

  /**
   * Returns cookie decoded from cookie string
   *
   * @param cookieString string of cookie as returned from http request
   * @return decoded cookie or null if exception occured
   */
  protected HttpCookie decodeCookie( String cookieString ) {
    byte[] bytes = hexStringToByteArray( cookieString );
    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream( bytes );
    HttpCookie cookie = null;
    try {
      ObjectInputStream objectInputStream = new ObjectInputStream( byteArrayInputStream );
      cookie = ((SerializableHttpCookie) objectInputStream.readObject()).getCookie();
    }
    catch (IOException e) {
      Logger.error( "IOException in decodeCookie", e );
    }
    catch (ClassNotFoundException e) {
      Logger.error( "ClassNotFoundException in decodeCookie", e );
    }

    return cookie;
  }

  /**
   * Using some super basic byte array &lt;-&gt; hex conversions so we don't have to rely on any
   * large Base64 libraries. Can be overridden if you like!
   *
   * @param bytes byte array to be converted
   * @return string containing hex values
   */
  protected String byteArrayToHexString( byte[] bytes ) {
    StringBuilder sb = new StringBuilder( bytes.length * 2 );
    for (byte element : bytes) {
      int v = element & 0xff;
      if (v < 16) {
        sb.append( '0' );
      }
      sb.append( Integer.toHexString( v ) );
    }
    return sb.toString().toUpperCase( Locale.US );
  }

  /**
   * Converts hex values from strings to byte array
   *
   * @param hexString string of hex-encoded values
   * @return decoded byte array
   */
  protected byte[] hexStringToByteArray( String hexString ) {
    int len = hexString.length();
    byte[] data = new byte[len / 2];
    for (int i = 0; i < len; i += 2) {
      data[i / 2] = (byte) ((Character.digit( hexString.charAt( i ), 16 ) << 4) + Character.digit( hexString.charAt( i + 1 ), 16 ));
    }
    return data;
  }
}


