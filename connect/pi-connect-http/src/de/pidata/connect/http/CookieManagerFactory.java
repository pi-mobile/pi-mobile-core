package de.pidata.connect.http;

import java.net.CookieManager;

public class CookieManagerFactory {

  public static CookieManager getCookieManager() {
    return new CookieManager();
  }

   public static CookieManager getPersistentCookieManager() {
    return new CookieManager( new PersistentCookieStore(), null);
  }


}
