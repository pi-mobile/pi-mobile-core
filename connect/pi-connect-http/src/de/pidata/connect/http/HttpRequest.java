package de.pidata.connect.http;

import de.pidata.log.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;

public class HttpRequest {

  private HttpURLConnection httpConnection;
  public HttpRequest( HttpURLConnection httpConnection ) {
    this.httpConnection = httpConnection;
  }

  public String getConnectionID() {
    return httpConnection.getURL().toString()+"@"+System.currentTimeMillis();
  }

  public int getResponseCode() throws IOException {
    return httpConnection.getResponseCode();
  }

  public OutputStream getOutputStream() throws IOException {
    return httpConnection.getOutputStream();
  }

  public String getEncoding() {
    String encoding = httpConnection.getContentEncoding();
    if (encoding == null) encoding = "UTF-8";
    return encoding;
  }

  public InputStream getInputStream() throws IOException {
    return httpConnection.getInputStream();
  }

  public InputStream getErrorStream() throws IOException {
    return httpConnection.getErrorStream();
  }

  public static String readString(InputStream inputStream, String encoding) throws IOException {
    ByteArrayOutputStream result = new ByteArrayOutputStream();
    byte[] buffer = new byte[1024];
    int length;
    while ( (length = inputStream.read( buffer ) ) > 0) {
      result.write(buffer, 0, length);
    }
    return result.toString(encoding);
  }

  public String readRawString() {
    String resultString = "";
    try (InputStream inputStream = getInputStream()) {
      resultString = readString( inputStream, "UTF-8" );
    } catch (IOException ex) {
      Logger.error( "Error reading raw String from server, returning empty String", ex );
    }

    return resultString;
  }

  public String getResponseHeader( String name ) {
    return httpConnection.getHeaderField( name );
  }

  /**
   * Closes this connection.
   */
  public void close() {
    httpConnection.disconnect();
  }
}
