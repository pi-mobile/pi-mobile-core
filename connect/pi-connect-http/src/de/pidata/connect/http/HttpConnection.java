/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.connect.http;

import de.pidata.connect.base.AbstractConnection;
import de.pidata.connect.base.ConnectionListener;

import java.io.IOException;
import java.io.OutputStream;
import java.net.*;
import java.util.Iterator;
import java.util.Map;

public class HttpConnection extends AbstractConnection implements AutoCloseable {

  public static final String CONTENT_TYPE_JSON = "application/json; charset=utf-8";
  public static final String CONTENT_TYPE_XML  = "application/xml; charset=utf-8";
  private final CookieManager cookieManager;

  private Map<String, String> requestHeaders;
  private Proxy proxy = null;

  private String lastRequestConnID = null;

  public HttpConnection() {

    // Use the already created CookieManager if available. We might have problems with
    // missing cookies due to several instances of CookieManager.
    if (CookieHandler.getDefault() instanceof CookieManager) {
      cookieManager = (CookieManager) CookieHandler.getDefault();
    }
    // The default might be is com.sun.webkit.CookieManager. We do not want to use it, because there ist no available CookieStore
    else {
      cookieManager = CookieManagerFactory.getPersistentCookieManager();
      CookieHandler.setDefault( cookieManager );

    }
  }

  /**
   * Returns unique ID for this connection, e.g. URL plus counter
   *
   * @return unique ID for this connection
   */
  @Override
  public String getConnectionID() {
    return lastRequestConnID;
  }

  /**
   * Called by connectionListener to get all connection steps in order.
   * Implementation has to call connectionListener.addStep() in correct
   * order for each step.
   * <p>
   * The list of steps are exact that steps passed when establishing a
   * connection. While connection each registered listener will be called
   * on success or failure of these steps.
   *
   * @param connectionListener listener to add steps to
   */
  @Override
  public void getConnectionSteps( ConnectionListener connectionListener ) {
    //TODO: connectionSteps
  }

  public synchronized HttpRequest call( URL destination, String requestMethod, String requestString) throws IOException {
    return call(destination, CONTENT_TYPE_JSON, requestMethod, false, requestString);
  }

  public synchronized HttpRequest call( URL destination, String contentType, String requestMethod, boolean followRedirects, String resquestBody ) throws IOException {
    // Logger.debug("HttpConnection "+requestMethod+": " + destination);

    HttpURLConnection httpConnection = createUrlConnection( destination );

    setStateConnected();
    httpConnection.setDoOutput(true);
    httpConnection.setRequestMethod(requestMethod);
    httpConnection.setRequestProperty("Content-Type", contentType );
    byte[] bytes = resquestBody.getBytes("UTF-8");
    httpConnection.setRequestProperty( "Content-Length", Integer.toString( bytes.length ) );
    applyRequestProperties( httpConnection );
    httpConnection.setInstanceFollowRedirects(followRedirects);
    try (OutputStream outputStream = httpConnection.getOutputStream()) {
      outputStream.write( bytes );
      outputStream.flush();
    }
    return new HttpRequest( httpConnection );
  }

  private HttpURLConnection createUrlConnection( URL destination ) throws IOException {
    HttpURLConnection httpConnection;
    if (proxy == null) {
      httpConnection = (HttpURLConnection) destination.openConnection();
    }
    else {
      httpConnection = (HttpURLConnection) destination.openConnection(proxy);
    }
    this.lastRequestConnID = httpConnection.getURL().toString()+"@"+System.currentTimeMillis();
    return httpConnection;
  }

  public synchronized HttpRequest call( URL destination, String requestMethod) throws IOException {
    // Logger.debug("HttpConnection "+requestMethod+": " + destination);

    HttpURLConnection httpConnection = createUrlConnection( destination );

    setStateConnected();
    httpConnection.setRequestMethod( requestMethod );
    httpConnection.setRequestProperty("Accept", "application/json; charset=utf-8");
    // httpConnection.setRequestProperty( "Content-Length", "0" );
    applyRequestProperties( httpConnection );
    return new HttpRequest( httpConnection );
  }

  private void applyRequestProperties( HttpURLConnection httpConnection ) {
    if (requestHeaders != null) {
      Iterator<Map.Entry<String, String>> iterator = requestHeaders.entrySet().iterator();
      while (iterator.hasNext()) {
        Map.Entry<String, String> stringStringEntry = iterator.next();
        httpConnection.setRequestProperty( stringStringEntry.getKey(), stringStringEntry.getValue() );
      }
    }
  }

  public synchronized void setRequestHeaders( Map<String, String> requestHeaders ) {
    this.requestHeaders = requestHeaders;
  }

  /**
   * Closes this connection.
   */
  @Override
  public void close() {
    // httpConnection.disconnect();
    setStateDisconnected();
  }

  public CookieManager getCookieManager() {
    return cookieManager;
  }

  public void setProxy( String server, int port ) {
    this.proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(server, port));
  }
}
