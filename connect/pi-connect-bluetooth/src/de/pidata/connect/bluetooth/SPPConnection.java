/*
 * This file is part of PI-Mobile Core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.connect.bluetooth;

import de.pidata.log.Logger;
import de.pidata.connect.base.ConnectionController;

import javax.bluetooth.*;
import javax.bluetooth.UUID;
import java.io.IOException;
import java.util.*;

public class SPPConnection implements ConnectionController, DiscoveryListener {

  private static UUID SPP = new UUID(0x1101);

  //object used for waiting
  private Object lock = new Object();
  //vector containing the devices discovered
  private Vector vecDevices = new Vector();
  private String connectionURL = null;
  private LocalDevice localDevice;
  private DiscoveryAgent agent;
  private RemoteDevice remoteDevice;
  private boolean connected = false;


  public SPPConnection( String connectionURL ) {
    this.connectionURL = connectionURL;

    //display local device address and name
    try {
      localDevice = LocalDevice.getLocalDevice();
      Logger.debug( "SPPConnection init, local device address=" + localDevice.getBluetoothAddress()
                           + ", name=" + localDevice.getFriendlyName() );
    }
    catch (Exception ex) {
      Logger.error( "Could not initialize Bluetooth", ex );
    }
  }

  public void listDevices() throws IOException {
    //find devices
    agent = localDevice.getDiscoveryAgent();
    Logger.info( "Starting device inquiry..." );

    try {
      agent.startInquiry( DiscoveryAgent.GIAC, this );
    }
    catch (Exception e) {
      Logger.error( "Error starting device inquiry", e );
    }
    try {
      synchronized (lock) {
        lock.wait();
      }
    }
    catch (InterruptedException e) {
      // do nothing
    }
    Logger.info( "Device Inquiry Completed. " );
    //print all devices in vecDevices
    int deviceCount = vecDevices.size();

    if (deviceCount <= 0) {
      Logger.debug( "No Devices Found ." );
    }
    else {
      //print bluetooth device addresses and names in the format [ No. address (name) ]
      Logger.debug( "Bluetooth Devices: " );
      for (int i = 0; i < deviceCount; i++) {
        remoteDevice = (RemoteDevice) vecDevices.elementAt( i );
        System.out.println( (i + 1) + ". " + remoteDevice.getBluetoothAddress() + " (" + remoteDevice.getFriendlyName( true ) + ")" );
      }
    }
  }

  public RemoteDevice getRemoteDevice() {
    return remoteDevice;
  }

  public String getConnectionURL() {
    return connectionURL;
  }

  public String connect() {
    try {

      if(remoteDevice == null) {
        return null;
      }

      //check for spp service
      UUID[] uuidSet = new UUID[]{SPP};

      System.out.println( "\nSearching for service..." );
      agent.searchServices( null, uuidSet , remoteDevice, this );
      try {
        synchronized (lock) {
          lock.wait();
        }
      }
      catch (InterruptedException e) {
        Logger.error( "Error connecting", e );
      }
      if (connectionURL == null || connectionURL.equals( "" ) ) {
        Logger.info( "Device does not support Simple SPP Service." );
      }
      else {
        Logger.info( "Connected to Simple SPP Service on: "+remoteDevice.getFriendlyName(true));
        return localDevice.getBluetoothAddress();
      }
    }
    catch (Exception ex) {
      Logger.error( "Could not connect via bluetooth", ex );
    }
    return null;
  }

  public void disconnect() {
    //To change body of implemented methods use File | Settings | File Templates.
  }

  public boolean isConnected() {
    return connected;
  }

  //methods of DiscoveryListener
  public void deviceDiscovered( RemoteDevice btDevice, DeviceClass cod ) {
    //add the device to the vector
    if (!vecDevices.contains( btDevice )) {
      vecDevices.addElement( btDevice );
    }
  }

  //	 implement this method since services are not being discovered
  public void servicesDiscovered( int transID, ServiceRecord[] servRecord ) {
    if (servRecord != null && servRecord.length > 0) {
      connectionURL = servRecord[0].getConnectionURL( 0, false );
      connected = true;
    }
    synchronized (lock) {
      lock.notify();
    }
  }

  //	 implement this method since services are not being discovered
  public void serviceSearchCompleted( int transID, int respCode ) {
    synchronized (lock) {
      lock.notify();
    }

    switch (respCode) {
      case 1:
        Logger.info( "serviceSearchCompleted: SERVICE_SEARCH_COMPLETED" );
        break;
      case 2:
        Logger.info( "serviceSearchCompleted: SERVICE_SEARCH_TERMINATED" );
        break;
      case 3:
        Logger.info( "serviceSearchCompleted: SERVICE_SEARCH_ERROR" );
        break;
      case 4:
        Logger.info( "serviceSearchCompleted: SERVICE_SEARCH_NO_RECORDS" );
        break;
      case 6:
        Logger.info( "serviceSearchCompleted: SERVICE_SEARCH_DEVICE_NOT_REACHABLE" );
        break;
      default:
        Logger.info( "serviceSearchCompleted: Unknown serviceSearch result." );
    }
  }

  public void inquiryCompleted( int discType ) {
    synchronized (lock) {
      lock.notify();
    }
  }

}
