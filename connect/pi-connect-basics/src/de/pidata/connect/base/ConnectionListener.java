/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.connect.base;

/**
 * Created by cga on 26.10.17.
 */

public interface ConnectionListener {

  /**
   * Called by Connection in reply to getConnectionSteps()
   */
  void addStep( Connection connection, String stepName );

  /**
   * Called by Connection whenever a connection step changes its state.
   *
   * @param connection the connection to which the connection state belongs
   * @param stepName   name of the step for which the state has changed
   * @param stepState  the new step state
   * @param detailInfo optional detail information
   */
  void stepStateChanged( Connection connection, String stepName, ConnectionStepState stepState, String detailInfo );

  /**
   * Called by Connection whenever a connection step changes its state.
   *
   * @param connection the connection to which the connection state belongs
   * @param stepName   name of the step for which the state has changed
   * @param stepState  the new step state
   * @param detailInfo optional detail information
   * @param errorHandling optional error Handling information
   */
  void stepStateChanged( Connection connection, String stepName, ConnectionStepState stepState, String detailInfo, String errorHandling );
  /**
   * Called by Connection to reset all steps to ConnectionStepState.waiting
   *
   * @param connection
   */
  void resetConnectionSteps( Connection connection );

  /**
   * Called by Connection after connection has been established successfully
   */
  void connected( Connection connection );

  /**
   * Called by Connection when an connection has been disconnected/closed
   */
  void disconnected( Connection connection );
}
