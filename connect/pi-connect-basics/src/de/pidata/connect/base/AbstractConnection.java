/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.connect.base;

import de.pidata.log.Logger;

import java.util.LinkedList;
import java.util.List;

public abstract class AbstractConnection implements Connection {

  private static final boolean DEBUG = false;

  protected List<ConnectionListener> connectionListeners = new LinkedList<ConnectionListener>();
  protected String currentStep;
  private ConnectionStepState currentState = ConnectionStepState.progress;
  private boolean stateConnected;
  private String lastDetailInfo;
  private String lastErrorHandling;

  public String getCurrentStep() {
    return currentStep;
  }

  public ConnectionStepState getCurrentState() {
    return currentState;
  }

  public String getLastDetailInfo() {
    return lastDetailInfo;
  }

  public String getLastErrorHandling() {
    return lastErrorHandling;
  }

  /**
   * Adds listener to this connection
   *
   * @param listener the new listener
   */
  public synchronized void addConnectionListener( ConnectionListener listener ) {
    if (DEBUG) {
      Logger.info( getClass().getName() + ".addConnectionListener="+listener.getClass().getName()+", #listener="+connectionListeners.size() );
    }
    connectionListeners.add( listener );
  }

  /**
   * Removes listener from this connection
   *
   * @param listener the listener to be removed
   */
  public synchronized void removeConnectionListener( ConnectionListener listener ) {
    if (DEBUG) {
      Logger.info( getClass().getName() + ".removeConnectionListener="+listener.getClass().getName()+", #listener="+connectionListeners.size() );
    }
    connectionListeners.remove( listener );
  }

  /**
   * resets current step information and
   * notifies listener
   */
  protected synchronized void resetConnectionSteps() {
    stateConnected = false;
    currentStep = null;
    lastDetailInfo = null;
    lastErrorHandling = null;
    this.currentState = ConnectionStepState.waiting;
    ConnectionListener[] listenerArray = new ConnectionListener[connectionListeners.size()];
    connectionListeners.toArray( listenerArray );
    for (ConnectionListener listener : listenerArray) {
      listener.resetConnectionSteps( this );
    }
  }

  /**
   * success current step and notify listeners that newStep is progressing
   *
   * @param newStep
   * @param detailInfo
   */
  protected synchronized void startConnectionStep( String newStep, String detailInfo ) {
    if ((this.currentStep != null) && (currentState != ConnectionStepState.success)) {
      successConnectionStep( null );
    }
    this.currentStep = newStep;
    this.currentState = ConnectionStepState.progress;
    this.lastDetailInfo = detailInfo;
    this.lastErrorHandling = null;
    if (DEBUG) {
      Logger.info( getClass().getName() + ".startConnectionStep set progress Name=" + this.currentStep + ", #listener=" + connectionListeners.size() );
    }
    notifyStepStateChanged( detailInfo, ConnectionStepState.progress, null );
  }

  /**
   * success current step and notify listeners that newStep is progressing
   *
   * @param currentStep
   */
  protected void startConnectionStep( String currentStep ) {
    startConnectionStep( currentStep, null );
  }

  /**
   * success current step and notifies listeners that current step has succeeded
   *
   * @param detailInfo
   */
  protected synchronized void successConnectionStep( String detailInfo ) {
    this.currentState = ConnectionStepState.success;
    this.lastDetailInfo = detailInfo;
    this.lastErrorHandling = null;
    if (DEBUG) {
      Logger.info( getClass().getName() + ".successConnectionStep set success Name=" + this.currentStep + ", #listener=" + connectionListeners.size() );
    }
    notifyStepStateChanged( detailInfo, ConnectionStepState.success, null );
  }

  /**
   * fails current step and notifies listeners that current step has failed
   *
   * @param detailInfo
   */
  protected synchronized void errorConnectionStep( String detailInfo ) {
    errorConnectionStep( detailInfo, null );
  }

  /**
   * fails current step and notifies listeners that current step has failed
   *
   * @param detailInfo
   * @param errorHandling
   */
  protected synchronized void errorConnectionStep( String detailInfo, String errorHandling ) {
    this.currentState = ConnectionStepState.error;
    this.lastDetailInfo = detailInfo;
    this.lastErrorHandling = errorHandling;
    notifyStepStateChanged( detailInfo, ConnectionStepState.error, errorHandling );
  }

  /**
   * notifies connectionListeners that state of current step has changed
   *
   * @param detailInfo
   * @param errorHandling
   */
  private void notifyStepStateChanged( String detailInfo, ConnectionStepState connectionStepState, String errorHandling ) {
    ConnectionListener[] listenerArray = new ConnectionListener[connectionListeners.size()];
    connectionListeners.toArray( listenerArray );
    for (ConnectionListener listener : listenerArray) {
      listener.stepStateChanged( this, this.currentStep, connectionStepState, detailInfo, errorHandling );
    }
  }

  /**
   * set state is connected and notify listeners
   */
  protected synchronized void setStateConnected() {
    if (!stateConnected) {
      stateConnected = true;
      //Logger.info( getClass().getName() + ".setStateConnected, #listener=" + connectionListeners.size() );
      successConnectionStep( "" );
      ConnectionListener[] listenerArray = new ConnectionListener[connectionListeners.size()];
      connectionListeners.toArray(listenerArray);
      for (ConnectionListener listener : listenerArray) {
        listener.connected( this );
      }
    }
  }

  /**
   * set state is disconnected and notify listeners
   */
  protected synchronized void setStateDisconnected() {
    stateConnected = false;
    Logger.info( getClass().getName() + ".setStateDisconnected, #listener=" + connectionListeners.size() );
    ConnectionListener[] listenerArray = new ConnectionListener[connectionListeners.size()];
    connectionListeners.toArray(listenerArray);
    for (ConnectionListener listener : listenerArray) {
      listener.disconnected( this );
    }
  }
}
