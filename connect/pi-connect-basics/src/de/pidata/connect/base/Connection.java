/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.connect.base;

/**
 * Created by pru on 20.05.16.
 */
public interface Connection {

  /**
   * Returns unique ID for this connection, e.g. URL plus counter
   * @return unique ID for this connection
   */
  public String getConnectionID();

  /**
   * Adds listener to this connection
   *
   * @param listener the new listener
   */
  public void addConnectionListener( ConnectionListener listener );

  /**
   * Removes listener from this connection
   *
   * @param listener the listener to be removed
   */
  public void removeConnectionListener( ConnectionListener listener );

  /**
   * Called by connectionListener to get all connection steps in order.
   * Implementation has to call connectionListener.addStep() in correct
   * order for each step.
   *
   * The list of steps are exact that steps passed when establishing a
   * connection. While connection each registered listener will be called
   * on success or failure of these steps.
   *
   * @param connectionListener listener to add steps to
   */
  void getConnectionSteps( ConnectionListener connectionListener );

  /**
   * Closes this connection.
   */
  public void close();
}
