package de.pidata.connect.nfc;

public interface NfcScanListener {

  void scannedSerialNumber(String serialNumber);

}
