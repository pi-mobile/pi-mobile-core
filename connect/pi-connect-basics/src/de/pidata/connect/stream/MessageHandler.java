/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.connect.stream;

import de.pidata.connect.stream.StreamHandler;

/**
 * Created by pru on 17.05.16.
 */
public interface MessageHandler {

  /**
   * Called by server after connection is established
   * @param requestHandler the calling requestHandler
   */
  public void connected( StreamHandler requestHandler );

  /**
   * Called by server after disconnect
   * @param requestHandler the calling requestHandler
   */
  public void disconnected( StreamHandler requestHandler );

  /**
   * Invoked whenever a complete message has been received.
   * @param messageBuffer the buffer containing one message without start an end characters
   * @param requestHandler the calling requestHandler
   */
  public void received( StringBuilder messageBuffer, StreamHandler requestHandler );
}
