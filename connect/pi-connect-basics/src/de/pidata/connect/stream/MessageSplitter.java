/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.connect.stream;

import java.io.IOException;

/**
 * RequestHandler splitting stream into multiple messages.
 * Messages are identified by a starting character sequence and a ending character sequence.
 * These sequences must not occur within the message itself.
 */
public class MessageSplitter implements StreamHandler {

  private String connectionID;
  private StreamReceiver receiver;
  private StringBuilder parseBuffer;
  private StringBuilder receiveBuffer;
  private String startString;
  private String endString;
  private MessageHandler messageHandler;

  /**
   * Create message splitter fpor messages beeing surrounded by startString and endString.
   * One of startString and endString may be null, but not both.
   * @param connectionID    String uniquely identifying the connection this message splitter is working for
   * @param startString     the starting character sequence
   * @param endString       the end character sequence
   * @param messageHandler  the message handler to call whenever a complete message has beeen received
   */
  public MessageSplitter( String connectionID, String startString, String endString, MessageHandler messageHandler ) {
    if ((startString == null) && (endString == null)) {
      throw new IllegalArgumentException( "At least one of startString and endString must not be null!" );
    }
    this.connectionID = connectionID;
    this.startString = startString;
    this.endString = endString;
    this.messageHandler = messageHandler;
  }

  @Override
  public String getConnectionID() {
    return connectionID;
  }

  public void setStartEnd( String startString, String endString ) {
    this.startString = startString;
    this.endString = endString;
  }

  public String getStartString() {
    return startString;
  }

  public String getEndString() {
    return endString;
  }

  /**
   * Returns current Serviceprovider
   *
   * @return current Serviceprovider or null if not connected
   */
  @Override
  public StreamReceiver getStreamReceiver() {
    return receiver;
  }

  public MessageHandler getMessageHandler() {
    return messageHandler;
  }

  /**
   * Returns client's human readable name if available
   *
   * @return clientName or null if not connected
   */
  @Override
  public String getCallerName() {
    if (receiver == null) {
      return null;
    }
    else {
      return receiver.getCallerName();
    }
  }

  /**
   * Called by stream receiver when entering receive loop
   * @param receiver the stream receiver which will invoke received()
   */
  @Override
  public void connected( StreamReceiver receiver ) {
    this.receiver = receiver;
    this.parseBuffer = new StringBuilder();
    this.receiveBuffer = null;
    messageHandler.connected( this );
  }

  /**
   * Called by server after disconnect
   */
  @Override
  public void disconnected() {
    messageHandler.disconnected( this );
    this.receiver = null;
  }

  /**
   * Returns true if buffer ends with chars, otherwise false.
   * Also returns false if buffer's length is less than chars' length
   *
   * @param buffer buffer to examine
   * @param chars  end chars looking for
   * @return true if buffer ends with chars
   */
  private boolean isEndOf( StringBuilder buffer, String chars ) {
    if (chars == null) {
      return true;
    }
    int bufLen = buffer.length();
    int charLen = chars.length();
    if (bufLen < charLen) {
      return false;
    }
    int start = bufLen - charLen;
    for (int i = 0; i < charLen; i++) {
      if (chars.charAt( i ) != buffer.charAt( start + i )) {
        return false;
      }
    }
    return true;
  }

  /**
   * Called by server whenever a character is received
   *  @param ch the character received
   */
  @Override
  public void received( char ch ) throws IOException {
    if (receiveBuffer == null) {
      if (startString == null) {
        receiveBuffer = new StringBuilder();
        receiveBuffer.append( ch );
      }
      else {
        parseBuffer.append( ch );
        if (isEndOf( parseBuffer, startString )) {
          receiveBuffer = new StringBuilder();
          parseBuffer = new StringBuilder();
        }
      }
    }
    else {
      receiveBuffer.append( ch );
      if (endString == null) {
        if (isEndOf( receiveBuffer, startString )) {
          receiveBuffer.delete( receiveBuffer.length() - startString.length(), receiveBuffer.length() );
          messageHandler.received( receiveBuffer, this );
          receiveBuffer = new StringBuilder();
        }
      }
      else if (isEndOf( receiveBuffer, endString )) {
        receiveBuffer.delete( receiveBuffer.length() - endString.length(), receiveBuffer.length() );
        messageHandler.received( receiveBuffer, this );
        receiveBuffer = null;
      }
    }
  }

  /**
   * Called by stream receiver whenever an error occurs while receiving
   *
   * @param ex the exception
   * @return true if stream receiver should retry, false to abort connection
   */
  @Override
  public boolean processError( Exception ex ) {
    return false;
  }
}
