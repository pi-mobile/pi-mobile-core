/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.connect.stream;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public interface StreamConnectionRequestReply extends StreamConnection {

  /**
   * Call destiantion without message (GET)
   * @param destination
   * @param requestMethod
   * @return http response code
   * @throws IOException
   */
  public int call(URL destination, String requestMethod) throws IOException;

  /**
   * Call destination with message (POST / PUT)
   *
   * @param destination
   * @param requestMethod
   * @param message  @return
   * @return http response code
   * @throws IOException
   */
  public int call(URL destination, String requestMethod, String message) throws IOException;

  public InputStream getInputStream() throws IOException;

}
