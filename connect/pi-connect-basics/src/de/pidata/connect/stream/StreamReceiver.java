/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.connect.stream;

import de.pidata.log.Logger;
import de.pidata.stream.StreamHelper;

import java.io.InputStream;

/**
 * Implements a loop receiving data from an input stream and splitting that data into separate messages.
 */
public class StreamReceiver implements Runnable {

  protected InputStream inStream = null;
  protected int readLoopIdleSleepMillis;
  protected long timeoutMillis;
  protected StreamHandler streamHandler;
  protected StreamConnection connection;
  protected String callerName;
  protected boolean running;

  /**
   *
   * @param connection               the connection this receiver works on
   * @param readLoopIdleSleepMillis  Time to sleep if nothing is received
   * @param timeoutMillis            timeout after which connection is closed if nothing is received, -1 for not timeout
   */
  public StreamReceiver( StreamConnection connection, int readLoopIdleSleepMillis, int timeoutMillis ) {
    this.connection = connection;
    this.readLoopIdleSleepMillis = readLoopIdleSleepMillis;
    this.timeoutMillis = timeoutMillis;
  }

  public StreamConnection getConnection() {
    return connection;
  }

  public String getCallerName() {
    return callerName;
  }

  /**
   * Starts receiving data from inStream and calls StreamHandler.processMessage whenever
   * stopChar occurs in iunput stream.
   * @param streamHandler the handler for response processing
   */
  public void startReceive( String callerName, InputStream inStream, StreamHandler streamHandler ) {
    this.callerName = callerName;
    this.inStream = inStream;
    this.streamHandler = streamHandler;
    Thread receiveThread = new Thread( this );
    receiveThread.start();
  }

  public boolean isRunning() {
    return running;
  }

  public void stop() {
    running = false;
  }

  /**
   * Called when leaving run loop after streams have been closed
   */
  protected void closeConnection() {
    Logger.info( "StreamReceiver["+connection.getConnectionID()+"]: closing connection..." );
    StreamHelper.close( inStream );
    inStream = null;
    if (streamHandler != null) {
      streamHandler.disconnected();
    }
    streamHandler = null;
    if (connection != null) {
      connection.close();
    }
    connection = null;
  }

  @Override
  public void run() {
    running = true;
    long lastactive = System.currentTimeMillis();
    Logger.info( "StreamReceiver["+connection.getConnectionID()+"]: started receive thread" );
    streamHandler.connected( this );
    while (running) {
      try {
        while (running) {
          if (inStream.available() > 0) {
            int ch = inStream.read();
            if (ch >= 0) {
              streamHandler.received( (char) ch );
            }
            lastactive = System.currentTimeMillis();
          }
          else {
            if ((timeoutMillis > 0 ) && (lastactive + timeoutMillis < System.currentTimeMillis())) {
              Logger.info( "StreamReceiver[" + connection.getConnectionID() + "] timeout" );
              running = false;
            }
            else {
              Thread.sleep( readLoopIdleSleepMillis );
            }
          }
        }
      }
      catch (Exception ex) {
        Logger.error( "StreamReceiver[" + connection.getConnectionID() + "]: Exception while communication", ex );
        if (!streamHandler.processError( ex )) {
          running = false;
        }
      }
    }
    closeConnection();
  }
}
