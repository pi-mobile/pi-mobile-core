/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.connect.stream;

import java.io.IOException;

/**
 * Interface for processing data arriving on a stream connection.
 * If multiple connections are allowed for each connection a new
 * instance of this interface has to be created.
 */
public interface StreamHandler {

  /**
   * Called by stream receiver when entering receive loop
   * @param receiver the stream receiver which will invoke received()
   */
  public void connected( StreamReceiver receiver );

  /**
   * Called by server after disconnect
   */
  public void disconnected();

  /**
   * Returns String uniquely identifying the connection this stream handler is working for
   * @return the connection ID
   */
  public String getConnectionID();

  /**
   * Returns current StreamReceiver
   * @return current StreamReceiver or null if not connected
   */
  public StreamReceiver getStreamReceiver();

  /**
   * Returns client's human readable name if available
   * @return clientName or null if not connected
   */
  public String getCallerName();

  /**
   * Called by server whenever a character is received
   * @param ch the charcter received
   */
  public void received( char ch ) throws IOException;

  /**
   * Called by stream receiver whenever an error occurs while receiving
   * @param ex the exception
   * @return true if stream receiver should retry, false to abort connection
   */
  public boolean processError( Exception ex );
}
