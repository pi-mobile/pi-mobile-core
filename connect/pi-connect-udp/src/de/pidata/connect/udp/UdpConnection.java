/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.connect.udp;

import de.pidata.log.Logger;
import de.pidata.worker.WorkerThread;

import java.io.IOException;
import java.net.*;

/**
 * Created by pru on 02.09.15.
 */
public class UdpConnection implements Runnable {

  private static final boolean DEBUG = false;

  private DatagramSocket socket;
  private boolean active = false;
  private int receivePort;
  private InetReceiveListener listener;
  private WorkerThread receiveThread;

  public UdpConnection( int receivePort, InetReceiveListener receiveListener ) {
    this.receivePort = receivePort;
    this.listener = receiveListener;
    receiveThread = new WorkerThread( this, "Udp-Receive-"+receivePort );
    receiveThread.start();
  }

  public boolean isActive() {
    return active;
  }

  public synchronized boolean isConnected() {
    return (socket != null);
  }

  public static String getHexCodes( byte[] buffer ) {
    StringBuilder buf = new StringBuilder();
    for (int i = 0; i < buffer.length; i++) {
      String hexStr = Integer.toHexString( buffer[i] );
      if (hexStr.length() > 2) hexStr = hexStr.substring(hexStr.length()-2);
      else if (hexStr.length() < 2) hexStr = "0" + hexStr;
      buf.append(" 0x").append( hexStr );
    }
    return buf.toString();
  }

  private synchronized void doCloseSocket() {
    if (this.socket != null) {
      this.socket.disconnect();
      this.socket.close();
      this.socket = null;
      Logger.info( "UdpConnection stopped, port=" + receivePort );
    }
  }

  public void close() {
    if (active) {
      active = false;
      doCloseSocket();
      if (receiveThread != null) {
        receiveThread.interrupt();
        receiveThread = null;
      }
    }
  }

  public InetReceiveListener getListener() {
    return listener;
  }

  public synchronized boolean socketActive() {
    return (this.socket != null);
  }

  /**
   * When an object implementing interface <code>Runnable</code> is used
   * to create a thread, starting the thread causes the object's
   * <code>run</code> method to be called in that separately executing
   * thread.
   * <p/>
   * The general contract of the method <code>run</code> is that it may
   * take any action whatsoever.
   *
   * @see Thread#run()
   */
  @Override
  public void run() {
    this.active = true;
    byte[] buf = new byte[8192];
    while (active) {
      DatagramSocket currentSocket;
      synchronized (this) {
        if (this.socket == null) {
          try {
            this.socket = new DatagramSocket( null );
            this.socket.setReuseAddress( true );
            this.socket.bind( new InetSocketAddress( receivePort ) );
            this.socket.setSoTimeout( 5000 );
            Logger.info( "UdpConnection started listening on UDP port=" + receivePort );
          }
          catch (Exception e) {
            doCloseSocket();
            Logger.error( "Error opening socket", e );
          }
        }
        currentSocket = this.socket;
      }
      if (currentSocket != null) {
        //Response
        DatagramPacket packet = new DatagramPacket(buf, buf.length);
        try {
          currentSocket.receive(packet);
          byte[] responseData = new byte[packet.getLength()];
          System.arraycopy( packet.getData(), 0, responseData, 0, packet.getLength() );
          if (listener == null) {
            Logger.info( "No listener, UDP Service data=" + getHexCodes(responseData) );
          }
          else {
            listener.receivedData( packet.getAddress(), responseData, null, packet.getPort() );
          }
        }
        catch (SocketTimeoutException tex) {
          // report only if in DEBUG mode
          if (DEBUG) Logger.error( "Socket timeout in UDP run loop", tex );
        }
        catch (SocketException se) {
          // report only if in DEBUG mode
          if (DEBUG) Logger.error( "Socket error in UDP run loop", se );
          doCloseSocket();
        }
        catch (Exception e) {
          if (active) {
            InetAddress addr = null;
            try {
              addr = packet.getAddress();
            }
            catch (Exception ex) {
              // fo nothing
            }
            Logger.error( "Error in UDP run loop, addr="+addr, e );
          }
        }
      }
      if (currentSocket == null) {
        // Wait a moment before retry open socket
        try {
          Thread.sleep( 1000 );
        }
        catch (InterruptedException e) {
          active = false;
        }
      }
    }
    doCloseSocket();
  }

  public void send( InetAddress destAddress, int destPort, byte[] message ) throws IOException {
    DatagramSocket socket;
    synchronized (this) {
      socket = this.socket;
    }
    if (socket == null) {
      throw new IOException( "Socket is not active" );
    }
    else {
      DatagramPacket packet = new DatagramPacket( message, message.length, destAddress, destPort );
      socket.send( packet );
    }
  }
}
