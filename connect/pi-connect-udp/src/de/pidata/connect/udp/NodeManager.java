/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.connect.udp;

import java.util.HashMap;

/**
 * Created by pru on 02.09.15.
 */
public class NodeManager {

  private HashMap<NodeAddress,Node> nodeMap = new HashMap<NodeAddress, Node>();

  private static NodeManager instance;

  public static NodeManager getInstance() {
    if (instance == null) {
      instance = new NodeManager();
    }
    return instance;
  }

  private NodeManager() {
  }

  public synchronized void addModule( Node module ) {
    nodeMap.put( module.getAddress(), module );
  }

  public synchronized Node getModule( NodeAddress address ) {
    return nodeMap.get( address );
  }

  public synchronized void removeModule( Node xBeeWiFi ) {
    nodeMap.remove( xBeeWiFi.getAddress() );
  }


}
