/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.comm.client.base;

import de.pidata.log.Logger;
import de.pidata.system.base.SystemManager;

import java.util.Hashtable;

public abstract class AbstractHttpConnection extends AbstractStreamConnection {

  protected String protocol;
  protected String host;
  protected int    port;
  protected String path;
  protected int timeout = 120000;  // 2 min

  /**
   * Wird nach dem Konstruktor aufgerufen, um die Parameter zu übergeben. Es wird
   * sichergestellt, dass init() genau einmal je Instanz aufgerufen wird.
   *
   * @param props Liste mit Parametern, wenn null wird SystemManager.getProperty() verwendet
   * @throws Exception
   */
  public void init( Hashtable props ) throws Exception {
    String protocol;
    String host;
    int port = 80;
    String path;
    String proxy;
    int proxyPort = 80;
    if (props == null) {
      SystemManager sysMan = SystemManager.getInstance();
      protocol = sysMan.getProperty("protocol", "http");
      host = sysMan.getProperty( "serverName", null );
      if (protocol == null) {
        // compatibility with old key word
        host = sysMan.getProperty( "serverName", null );
      }
      port = sysMan.getPropertyInt( "port", 80 );
      path = sysMan.getProperty( "path", null );
      proxy = sysMan.getProperty( "proxy", null );
      proxyPort = sysMan.getPropertyInt( "proxyPort", 80 );
      int timeout = sysMan.getPropertyInt( "comm.timeout", 120000 ); // default 2 min
      setTimeout( timeout );
    }
    else {
      protocol = (String) props.get("protocol");
      if (protocol == null) protocol = "http";
      host = (String) props.get("host");
      String tmp = (String) props.get("port");
      if (tmp != null) port = Integer.parseInt( tmp );
      path = (String) props.get("path");
      proxy = (String) props.get("proxy");
      tmp = (String) props.get("proxyPort");
      if (tmp != null) proxyPort = Integer.parseInt( tmp );
    }
    init( protocol, host, port, path );
    if (proxy != null) {
       setProxy( proxy, proxyPort );
    }
  }

  /**
   * Set proxy to use for next call of open()
   * @param proxy     proxy server name
   * @param proxyPort proxy server's port to use
   */
  protected abstract void setProxy( String proxy, int proxyPort );

  public void init(String protocol, String host, int port, String path) throws Exception {
    this.protocol = protocol;
    this.host = host;
    this.port = port;
    this.path = path;
    String message = null;
    if (!verifyUrlComp(protocol)) {
      message = "Falsches PROTOKOLL!";
    }
    if (!verifyUrlComp(host)) {
      message = "Kein HOST, keine Verbindung!";
    }
    if (message != null) {
      throw new IllegalArgumentException(message);
    }
    Logger.info("HttpConnection11 init: "+protocol+"://"+host+":"+port+path);
  }

  /**
     * Builds the URL string from its components:protocol+host+port
     * @return URL string
     */
    public String getURL() {
      return (protocol + "://" + host + ":" + port + path);
    }

  /**
     * checks the URL components
     * @param urlComponent
     * @return true if the component is not null and not empty
     */
    private boolean verifyUrlComp(String urlComponent) {
      return (urlComponent != null && urlComponent.length() != 0);
    }

  /**
   * Returns this Connections timeout
   * @return timeout value in ms
   */
  public int getTimeout() {
    return timeout;
  }

  /**
   * Sets this Connections timeout. New timeout value is set in next call to send().
   * @param timeout value in ms
   */
  public void setTimeout(int timeout) {
    this.timeout = timeout;
  }
}
