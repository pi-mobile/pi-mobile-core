/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.comm.client.base;

public abstract class ChannelSender extends Sender {
  private double progressMax = 1.0;
  private double progress= 0.0;

  protected String getMimeType( String repeatName, int repeatIndex ) {
    String contentType = "text/xml; " + SoapSender.MIME_ATTR_CHARSET + "=" + encoding.toLowerCase() + ";";
    if (repeatName != null) {
      contentType = contentType + " " + SoapSender.MIME_ATTR_REPEAT + "="+ repeatName + "; " + SoapSender.MIME_ATTR_INDEX+"=" + repeatIndex + ";";
    }
    return contentType;
  }

  /**
   * Called to set an optional max value. this allows prograss messages
   * like 8 of 25
   *
   * @param maxValue the max value, following calls to setProgress mus not
   *                 exceed this value
   */
  public void setMaxValue( double maxValue ) {
    this.progressMax = maxValue;
  }

  /**
   * Updates progress indicator with message and per cent value.
   *
   * @param newMessage the message to display or null
   * @param progress   progress in per cent or -1 for aborted with error
   */
  public void updateProgress( String newMessage, double progress ) {
    this.progress = progress;
  }

  /**
   * Updates progress indicator with message and per cent value.
   *
   * @param progress progress in per cent or -1 for aborted with error
   */
  public void updateProgress( double progress ) {
    updateProgress( "", progress );
  }

  public void showError( String errorMessage ) {
    updateProgress( errorMessage, progressMax );
  }

  public void showInfo( String errorMessage ) {
    // TODO
  }

  public void showWarning( String warningMessage ) {
    throw new UnsupportedOperationException();
  }

  public void resetColor() {
    throw new UnsupportedOperationException();
  }

  public void hideError() {
    updateProgress( "", progress );
  }
  //noops
}