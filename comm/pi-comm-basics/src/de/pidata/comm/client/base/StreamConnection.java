/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.comm.client.base;

import de.pidata.models.tree.Context;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;

/**
 * Created by pru on 28.01.16.
 */
public interface StreamConnection {
  /**
   * Wird nach dem Konstruktor aufgerufen, um die Parameter zu übergeben. Es wird
   * sichergestellt, dass init() genau einmal je Instanz aufgerufen wird.
   *
   * @param props Liste mit Parametern, wenn null wird SystemManager.getProperty() verwendet
   * @throws Exception
   */
  void init( Hashtable props ) throws Exception;

  /**
   * Opens a connection
   *
   * @param serviceName name of service to be called
   * @param operation   name of operation within service
   * @param mimeType    type of data beeing transferred
   * @param context
   * @return the stream containing server'*s reply
   * @throws IOException
   */
  OutputStream open( String serviceName, String operation, String mimeType, Context context ) throws IOException;

  /**
     * Schaltet von STATE_SENDING auf STATE_RECEIVING um und liefert den InputStream.
     * Macht mindestens flush() auf OutputStream.
     * @return den InputStream
     * @throws IOException
     */
  InputStream getInputStream() throws IOException;

  /**
     * Schließt die Verbindung --> STATE_CLOSED
     */
  void close();
}
