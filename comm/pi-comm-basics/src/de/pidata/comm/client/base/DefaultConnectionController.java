/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.comm.client.base;

import de.pidata.log.Logger;
import de.pidata.connect.base.ConnectionController;
import de.pidata.system.base.SystemManager;

import java.io.IOException;

public class DefaultConnectionController implements ConnectionController {

  private boolean connected = false;
  private boolean clientIDset = false;
  private String clientID = null;

  public DefaultConnectionController() {
    this(false);
  }

  public DefaultConnectionController(boolean clientIDset) {
    this.clientIDset = clientIDset;
  }

  /**
   * Connects and returns clientID
   *
   * @return
   * @throws IOException
   */
  @Override
  public String connect() throws IOException {
    if (!clientIDset) {
      clientID = SystemManager.getInstance().getProperty("clientId", null );
      if ((clientID == null) || (clientID.length() == 0)) {
        Logger.error("ClientId not defined in system.properties --> exiting...", null);
        System.exit(1);
      }
      int pos = clientID.indexOf(":");
      if (pos >= 0) {
        clientID = clientID.substring( pos );
      }
      SystemManager.getInstance().setClientID( clientID );
      Logger.info("ClientId='"+clientID+"'");
      clientIDset = true;
    }
    connected = true;
    return clientID;
  }

  public void disconnect() {
    connected = false;
  }

  public boolean isConnected() {
    return connected;
  }
}
