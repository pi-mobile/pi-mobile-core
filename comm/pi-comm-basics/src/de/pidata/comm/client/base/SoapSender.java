/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.comm.client.base;

import de.pidata.comm.soap.Body;
import de.pidata.comm.soap.Envelope;
import de.pidata.comm.soap.Fault;
import de.pidata.comm.soap.SoapFactory;
import de.pidata.connect.stream.StreamHandler;
import de.pidata.log.Logger;
import de.pidata.service.base.ServiceException;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.Model;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.models.xml.binder.XmlWriter;
import de.pidata.qnames.QName;
import de.pidata.stream.StreamHelper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Hashtable;

public class SoapSender extends ChannelSender {

  public static final String HEADER_SOAP_ACTION = "soapaction";
  public static final String HEADER_CONTENT_TYPE = "content-type";
  public static final String HEADER_SENDER_ID = "sender-id";
  public static final String HEADER_MAX_LOGS = "max-logs";
  public static final String HEADER_USER_AGENT = "user-agent";
  public static final String HEADER_FROM = "from";
  public static final String MIME_ATTR_REPEAT = "repeat";
  public static final String MIME_ATTR_INDEX = "index";
  public static final String MIME_ATTR_CHARSET = "charset";

  public static final String PROP_HEADER_MAX_LOGS = "comm.maxLogsPerEnvelope";

  public static final int NONE = 0;
  public static final int SERVICE_OPERATION = 1;
  public static final int OPERATION = 2;

  protected StreamConnection streamConnection;
  protected int SOAP_ACTION_FORMAT = SERVICE_OPERATION;

  public SoapSender() {
  }

  /**
   * Wird nach dem Konstruktor aufgerufen, um die Parameter zu übergeben. Es wird
   * sichergestellt, dass init() genau einmal je Instanz aufgerufen wird.
   *
   * @param props Liste mit Parametern, niemals null
   * @throws Exception
   */
  public void init( Hashtable props ) throws Exception {
    //TODO encoding
    throw new RuntimeException( "TODO" );
  }

  public void init( StreamConnection channel, String encoding) {
    this.encoding = encoding;
    this.streamConnection = channel;
  }

  /**
   * <P> Überträgt den Envelope an die in diesem Sender codierte
   * bzw. konfigurierte Zieladresse (SOAP, API-Call, ...).</P>
   *
   * @param serviceName
   * @param operationName
   * @param caller
   */
  public synchronized void invoke( String serviceName, QName operationName, Context caller, Envelope request, String repeatName, OutputStream repeatBuffer ) throws IOException {
    String contentType = getMimeType( repeatName, -1 );
    OutputStream outStream = streamConnection.open( serviceName, operationName.getName(), contentType, caller );
    try {
      invokeInternal( serviceName, operationName, request, repeatBuffer, outStream );
    }
    finally{
      streamConnection.close();
    }
  }

  private void invokeInternal( String serviceName, QName operationName, Envelope request, OutputStream repeatBuffer, OutputStream outStream ) throws IOException {
    OutputStreamWriter writer;
    int sum;
    setState( Sender.STATE_SEND);
    if (debug) Logger.debug("Open connection, this="+this);
    try {
      writer = new OutputStreamWriter( outStream, this.encoding );
    }
    catch (IOException ex) {
      // catching UsupportedEncodingException would be better, but CrEme just throws IOException
      Logger.warn("HttpChannel11: IOException (perhaps unsuported encoding), msg="+ex.getMessage());
      writer = new OutputStreamWriter( outStream );
    }

    //----- Create XML message and store it in repeat buffer file
    XmlWriter xmlWriter = new XmlWriter(null, encoding);
    StringBuilder xmlBuf = xmlWriter.writeXML( request, SoapFactory.ENVELOPE_TYPE.name() );
    if (repeatBuffer != null) {
      OutputStreamWriter repeatWriter = new OutputStreamWriter( repeatBuffer );
      StreamHelper.buffer2Stream( xmlBuf, repeatWriter, Integer.MAX_VALUE, null );
      repeatWriter.flush();
      repeatBuffer.close();
    }
    if (debug) {
      Logger.debug( "Calling service name="+serviceName+", opName="+operationName
                           +" msg=" + xmlBuf.toString() );
    }

    //----- begin transmission
    sum = StreamHelper.buffer2Stream( xmlBuf, writer, Integer.MAX_VALUE, this );
    setState(Sender.STATE_FLUSH);
    writer.flush();
    outStream.flush();
    if (debug) Logger.debug("Finished send, #bytes=" + sum);
  }

  public synchronized void doSend(OutputStream outStream, Envelope message) throws IOException {
    int sum;
    try {
      XmlWriter xmlWriter = new XmlWriter(null);
      OutputStreamWriter writer = new OutputStreamWriter(outStream);
      if (debug) Logger.debug(xmlWriter.writeXML(message, message.type().name()).toString());
      sum = xmlWriter.write(message, writer, null);
      writer.flush();
      outStream.flush();
    }
    catch (Exception e) {
      streamConnection.close();
      Logger.error("could not send ", e);
      throw new IOException(e.getMessage());
    }
  }

  private Envelope doReceive() throws IOException, ServiceException {
    Envelope response = null;
    setState( Sender.STATE_TRY_RECEIVE);
    try {
      XmlReader xmlReader = new XmlReader();
      InputStream inStream = streamConnection.getInputStream();
      response = (Envelope) xmlReader.loadData( inStream, this );
      if (debug) {
        XmlWriter xmlWriter = new XmlWriter(null, encoding);
        Logger.debug( "Response:\n" + xmlWriter.writeXML(response, response.type().name()).toString() );
      }
    }
    catch (Exception ex) {
      Logger.error("Exception while reading/parsing HTTP response stream.", ex);
      throw new IOException("Exception while reading/parsing HTTP response: "+ex.getMessage());
    }
    if (response != null) {
      Body body = response.getBody();
      if (body != null) {
        Fault fault = body.getFault();
        if (fault != null) {
          throw new ServiceException( fault.getFaultcodeStr(), fault.getFaultstring() );
        }
      }
    }
    return response;

  }

  public synchronized Envelope invokeSynchron( String serviceName, QName operationName, Context caller, Envelope request, String repeatName, OutputStream repeatBuffer ) throws IOException, ServiceException {
    Envelope response = null;
    String contentType = getMimeType( repeatName, -1 );
    try {
      OutputStream outStream = streamConnection.open( serviceName, operationName.getName(), contentType, caller );
      invokeInternal( serviceName, operationName, request, repeatBuffer, outStream );
      response = doReceive();
    }
    finally {
      streamConnection.close();
    }
    return response;
  }


  /**
   * Called to repeat the message named repeatName from repeatIndex.
   * Repeated Data start with following line: repeatName[repeatIndex]
   *
   * @param caller
   * @param repeatName   name of the message to repeat, typically a file name
   * @param bufStream    buffer delivering the bytes to be repeated
   * @param repeatIndex  the index of the byte within message to begin from
   * @return the reply envelope or null if repeat buffer has no data after repeatIndex
   * @throws IOException
   */
  public Model repeat( Context caller, String repeatName, InputStream bufStream, int repeatIndex ) throws IOException, ServiceException {
    if (debug) Logger.debug( "Begin with repeat, name=" + repeatName + ", index=" + repeatIndex );
    bufStream.skip( repeatIndex );
    byte[] buf = new byte[256];
    int count;
    int sum = 0;
    count = bufStream.read( buf );
    if (count > 0) {
      StreamConnection conn = null;
      try {
        conn = getConnection();
        String mimeType = getMimeType( repeatName, repeatIndex );
        OutputStream sendStream = conn.open( Sender.SYSTEM_SERVICE, Sender.OP_REPEAT, mimeType, caller );
        while (count > 0) {
          sendStream.write( buf, 0, count );
          count = bufStream.read( buf );
          sum += count;
          setProgress( sum );
        }
        setState(Sender.STATE_FLUSH);
        sendStream.flush();
        if (debug) Logger.debug("Finished send, #bytes=" + sum);
        return doReceive();
      }
      finally {
        if (conn != null) conn.close();
      }
    }
    else {
      // repeat buffer is empty, so we have to do classic repeat, i.e. create ne message from existing logs
      return null;
    }
  }

  /**
     * Setzt den Channel dieses Senders.
     *
   * @param channel
   */
    public void setConnection( StreamConnection channel ) {
        this.streamConnection = channel;
    }

    /**
     * Liefert den Channel dieses Senders.
     * Wird zur Erzeugung von Trace-Einträgen verwendet
     *
     * @return das Ziel dieses Senders als ChannelClient.
     */
    public StreamConnection getConnection() {
        return this.streamConnection;
    }


  /**
   * Set some configuration information, e.g. a SOPA sender might have different handling
   * of SOAP action.
   * The contents of the configString depends on the sender which is uesd!
   *
   * @param configString string containing the configuration information
   */
  public void setConfiguration(String configString) {
    SOAP_ACTION_FORMAT = SERVICE_OPERATION;
    if (configString != null) {
      if (configString.equals("NONE"))
        SOAP_ACTION_FORMAT = NONE;
      else if (configString.equals("OPERATION"))
        SOAP_ACTION_FORMAT = OPERATION;
    }
  }

}
