/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.comm.client.base;

import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

public abstract class AbstractStreamConnection implements StreamConnection {

  protected Model stateModel;
  protected QName stateID;

  /**
   * Sets Model and valueID where to put state information
   * @param stateModel
   * @param stateID
   */
  public void setStateModel( Model stateModel, QName stateID) {
    this.stateModel = stateModel;
    this.stateID = stateID;
  }

  public void setState(QName state) {
    if (this.stateModel != null) {
      this.stateModel.set(stateID, state);
    }
  }
}
