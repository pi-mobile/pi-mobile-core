/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.comm.client.base;

import de.pidata.comm.soap.Envelope;
import de.pidata.log.Logger;
import de.pidata.service.base.ServiceException;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.Model;
import de.pidata.progress.ProgressListener;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.system.base.SystemManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class Sender implements ProgressListener {
  public static final Namespace NAMESPACE = Namespace.getInstance( "de.pidata.comm" );

  public static final String SYSTEM_SERVICE = "System";
  public static final String OP_REPEAT = "repeat";

  public static final QName STATE_INIT = NAMESPACE.getQName("Init");
  public static final QName STATE_TRY_CONNECT = NAMESPACE.getQName("Verb.Aufbau");
  public static final QName STATE_CONNECT = NAMESPACE.getQName("Verbunden");
  public static final QName STATE_SEND = NAMESPACE.getQName("Senden");
  public static final QName STATE_FLUSH = NAMESPACE.getQName("Ende Senden");
  public static final QName STATE_RECEIVE = NAMESPACE.getQName("Empfang");
  public static final QName STATE_TRY_RECEIVE = NAMESPACE.getQName("Warte Empf.");
  public static final QName STATE_IDLE = NAMESPACE.getQName( "Bereit" );
  public static final QName STATE_BUSY = NAMESPACE.getQName( "Arbeite" );
  public static final QName STATE_PROCESS = NAMESPACE.getQName( "Akt.Daten" );
  public static final QName STATE_DISCONNECTED = NAMESPACE.getQName( "Keine Verb." );
  public static final QName STATE_RECOVERING = NAMESPACE.getQName( "Statusabgl." );
  public static final QName STATE_AUTO_OFF = NAMESPACE.getQName( "Autom.aus" );
  public static final QName STATE_SERVER_ERROR = NAMESPACE.getQName( "Server ERROR" );

  protected boolean debug = false;

  protected String encoding = "UTF-8";
  protected Model stateModel;
  protected QName stateID;
  protected boolean sending = true;

  public void setDebug( boolean debug ) {
    this.debug = debug;
  }

  /**
   * Sets Model and valueID where to put state information
   * @param stateModel
   * @param stateID
   */
  public void setStateModel(Model stateModel, QName stateID) {
    this.stateModel = stateModel;
    this.stateID = stateID;
  }

  public void setState(QName state) {
    if (state == STATE_FLUSH) {
      sending = false;
    }
    if (this.stateModel != null) {
      this.stateModel.set(stateID, state);
    }
  }

  public void setEncoding( String encoding ) {
    this.encoding = encoding;
  }

  /**
   * Called to tell a progress to a listener
   *
   * @param progress the progress, must be <= maxValue if given
   */
  public void setProgress( int progress ) {
    if (this.stateModel != null) {
      String msg;
      if (sending) msg = ">"+Integer.toString(progress);
      else msg = "<"+Integer.toString(progress);
      setState( NAMESPACE.getQName(msg) );
    }
  }

  /**
   * Called to set an optional max value. this allows prograss messages
   * like 8 of 25
   *
   * @param maxValue the max value, following calls to setProgress mus not
   *                 exceed this value
   */
  public void setMaxValue( int maxValue ) {
    //TODO
  }

  /**
   * Setzt die StreamConnection dieses Senders.
   *
   * @param connection
   */
  public abstract void setConnection( StreamConnection connection );

  /**
   * Liefert die StreamConnection dieses Senders.
   * Wird zur Erzeugung von Trace-Einträgen verwendet
   *
   * @return die StreamConnection dieses Senders.
   */
  public abstract StreamConnection getConnection();

  /**
   * Set some configuration information, e.g. a SOPA sender might have different handling
   * of SOAP action.
   * The contents of the configString depends on the sender which is uesd!
   * @param configString string containing the configuration information
   */
  public abstract void setConfiguration( String configString);

  /**
   * <P> Überträgt den Envelope an die in diesem Sender codierte
   * bzw. konfigurierte Zieladresse (SOAP, API-Call, ...).</P>
   *
   * @param serviceName
   * @param operationName
   * @param caller
   * @param request der zu sendende Envelope @return der Antwort-Envelope oder null bei asynchronem Aufruf @throws IOException
   * @param reapeatBuffer  buffer to store Message for repeat after communitation interrupt, may be null
   */
  public abstract void invoke( String serviceName, QName operationName, Context caller, Envelope request, String repeatname, OutputStream reapeatBuffer ) throws IOException;

  public abstract Envelope invokeSynchron( String serviceName, QName name, Context caller, Envelope request, String repeatName, OutputStream repeatBuffer ) throws IOException, ServiceException;

  /**
   * Called to repeat the message named repeatName from repeatIndex.
   * Repeated Data start with following line: repeatName[repeatIndex]
   *
   * @param caller
   * @param repeatName   name of the message to repeat, typically a file name
   * @param bufStream    buffer delivering the bytes to be repeated
   * @param repeatIndex  the index of the byte within message to begin from
   * @return the reply envelope or null if repeat buffer has no data after repeatIndex
   * @throws IOException
   */
  public abstract Model repeat( Context caller, String repeatName, InputStream bufStream, int repeatIndex ) throws IOException, ServiceException;

  public static Sender initComm() {
    try {
      // HTTP Sender for 1.1 Platform
      SystemManager sysMan = SystemManager.getInstance();
      String encoding = sysMan.getProperty( "comm.encoding", null );
      if (encoding == null) encoding = "ASCII";
      String commClass = sysMan.getProperty("comm.class", null );
      //if (commClass == null) commClass = "de.pidata.comm.client.client11.Sender11";
      if (commClass == null) {
        String errorStr = "System property 'comm.class' not set";
        Logger.warn(errorStr);
        throw new IllegalArgumentException(errorStr);
      }
      String configString = sysMan.getProperty("comm.configuration", null );
      Sender sender = new SoapSender(); // TODO parametrierbar machen
      AbstractStreamConnection connection = (AbstractStreamConnection) Class.forName(commClass).newInstance();
      connection.init( null );
      sender.setConnection( connection );
      sender.setEncoding( encoding );
      sender.setConfiguration( configString );

      String commDebug = sysMan.getProperty("comm.debug", null );
      if ((commDebug != null) && (commDebug.toUpperCase().equals("TRUE"))) {
        sender.setDebug( true );
      }
      return sender;
    }
    catch (Exception e) {
      Logger.error( "Could not create client Sender", e );
      throw new RuntimeException( "Could not create client Sender. Error Message: '" + e.getMessage() + "'" );
    }
  }

  public static boolean isStateActive( QName state ) {
    if (state == STATE_IDLE) return false;
    if (state == STATE_AUTO_OFF) return false;
    if (state == STATE_DISCONNECTED) return false;
    if (state == STATE_SERVER_ERROR) return false;
    return true;
  }
}
