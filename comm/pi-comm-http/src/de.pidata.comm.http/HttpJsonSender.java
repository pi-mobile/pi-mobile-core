/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.comm.http;

import de.pidata.connect.http.HttpConnection;
import de.pidata.connect.http.HttpRequest;
import de.pidata.log.Logger;
import de.pidata.models.json.JsonReader;
import de.pidata.models.json.JsonRemoteException;
import de.pidata.models.json.JsonWriter;
import de.pidata.service.base.ServiceException;
import de.pidata.models.tree.*;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.QName;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static de.pidata.connect.http.HttpConnection.CONTENT_TYPE_JSON;

public class HttpJsonSender {

  public static final boolean DEBUG = false;
  protected String protocol;
  protected String host;
  protected int    port;
  private HttpConnection httpConnection;

  private boolean ignoreAdditionalAttibutes = true;

  public HttpJsonSender( String protocol, String host, int port ) {
    this.protocol = protocol;
    this.host = host;
    this.port = port;

    httpConnection = new HttpConnection();
  }

  public URI getHostURI (String path) {
    try {
      return new URI( protocol, null, host, port, path, null, null );
    }
    catch (URISyntaxException e) {
      throw new IllegalArgumentException( "invalid host or path [" + host + " : " + path + "]", e );
    }
  }

  private URL createURL( String servicePath ) throws MalformedURLException {
    try {
      URI uri = new URI( protocol, null, host, port, servicePath, null, null );
      URL url = uri.toURL();
      return url;
    }
    catch (URISyntaxException e) {
      throw new IllegalArgumentException( "invalid servicePath [" + servicePath + "]", e );
    }
  }

  private URL createURL( String servicePath, String query ) throws MalformedURLException {
    try {
      URI uri = new URI( protocol, null, host, port, servicePath, query, null );
      URL url = uri.toURL();
      return url;
    }
    catch (URISyntaxException e) {
      throw new IllegalArgumentException( "invalid servicePath [" + servicePath + "]", e );
    }
  }

  private String readRawString( HttpRequest httpRequest ) {
    return httpRequest.readRawString();
  }

  private String readStringResult( HttpRequest httpRequest ) {
    try {
      JsonReader reader = new JsonReader();
      SimpleModel simpleModel = (SimpleModel) reader.loadData( httpRequest.getInputStream(), StringType.getDefString(), httpRequest.getEncoding() );
      if (simpleModel == null) {
        return null;
      }
      else {
        return (String) simpleModel.getContent();
      }
    }
    catch (Exception ex) {
      //TODO dreckiger workaround
      Logger.error( "Error parsing string result, returning empty String", ex );
      return "";
    }
  }

  private String readErrorStream( HttpRequest httpRequest ) throws IOException {
    InputStream errorStream = httpRequest.getErrorStream();
    if (errorStream == null) {
      return "NO MESSAGE";
    }
    else {
      BufferedReader br = new BufferedReader( new InputStreamReader( errorStream ) );
      String line;
      StringBuilder response = new StringBuilder();

      while ((line = br.readLine()) != null) {
        response.append(line);
      }
      return response.toString();
    }
  }

  public Model invokeGet( String servicePath, Type resultType ) throws IOException, ServiceException {
    URL url = createURL( servicePath );

    HttpRequest httpRequest = httpConnection.call( url, "GET" );
    int responseCode = httpRequest.getResponseCode();
    if (responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
      handleStatusCodes_400( responseCode, httpRequest );
      // The return will not be reached, though the function will throw detailed ServiceExceptions
      return null;
    }
    else {
      JsonReader reader = new JsonReader(this.ignoreAdditionalAttibutes);
      return reader.loadData( httpRequest.getInputStream(), resultType, "UTF-8" );
    }
  }

  public Model invokeGet( String servicePath, String query, Type resultType ) throws IOException, ServiceException {
    URL url = createURL( servicePath, query );

    HttpRequest httpRequest = httpConnection.call( url, "GET" );
    int responseCode = httpRequest.getResponseCode();
    if (responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
      handleStatusCodes_400( responseCode, httpRequest );
      // The return will not be reached, though the function will throw detailed ServiceExceptions
      return null;
    }
    else {
      JsonReader reader = new JsonReader(this.ignoreAdditionalAttibutes);
      return reader.loadData( httpRequest.getInputStream(), resultType, "UTF-8" );
    }
  }

  public String invokeGetRaw( String servicePath ) throws IOException, ServiceException {
    return invokeGetRaw( servicePath, null );
  }

  public InputStream invokeGetInputStream( String servicePath ) throws IOException, ServiceException {
    return invokeGetInputStream( servicePath, null );
  }

  public String invokeGetRaw( String servicePath, String query ) throws IOException, ServiceException {
    URL url = query != null ? createURL( servicePath,query ) : createURL( servicePath );

    HttpRequest httpRequest = httpConnection.call( url, "GET" );
    int responseCode = httpRequest.getResponseCode();
    if (responseCode != HttpURLConnection.HTTP_OK) {
      throw new ServiceException( Integer.toString( responseCode ), readErrorStream( httpRequest ) );
    }
    else {
      String rawString = httpRequest.readRawString();
      return rawString;
    }
  }

  public InputStream invokeGetInputStream( String servicePath, String query ) throws IOException, ServiceException {
    URL url = query != null ? createURL( servicePath,query ) : createURL( servicePath );
    HttpRequest httpRequest = httpConnection.call( url, "GET" );
    int responseCode = httpRequest.getResponseCode();
    if (responseCode != HttpURLConnection.HTTP_OK) {
      throw new ServiceException( Integer.toString( responseCode ), readErrorStream( httpRequest ) );
    }
    else {
      return httpRequest.getInputStream();
    }
  }

  public Model invokeGetAsChildren( String servicePath, Type resultType ) throws IOException, ServiceException {
    DefaultComplexType resultParentType = new DefaultComplexType( BaseFactory.NAMESPACE.getQName( "" + System.currentTimeMillis() ), DefaultModel.class.getName(), 0 );
    resultParentType.addRelation( resultType.name(), resultType, 0, Integer.MAX_VALUE );
    DefaultModel resultParent = new DefaultModel( null, resultParentType );
    invokeGet( servicePath, resultParent, resultType.name() );
    return resultParent;
  }

  public <T extends Model> List<T> invokeGetAsList( String servicePath, Type resultType ) throws IOException, ServiceException {
    DefaultComplexType resultParentType = new DefaultComplexType( BaseFactory.NAMESPACE.getQName( "" + System.currentTimeMillis() ), DefaultModel.class.getName(), 0 );
    resultParentType.addRelation( resultType.name(), resultType, 0, Integer.MAX_VALUE );
    Model resultParent = new DefaultModel( null, resultParentType );
    invokeGet( servicePath, resultParent, resultType.name() );
    List<T> modelList = new ArrayList<>();
    for (Model child : resultParent.iterator( resultType.name(), null )) {
      modelList.add( (T) child );
    }
    return modelList;
  }

  public <T extends Model> List<T> invokeGetAsList( String servicePath, String query, Type resultType ) throws IOException, ServiceException {
    DefaultComplexType resultParentType = new DefaultComplexType( BaseFactory.NAMESPACE.getQName( "" + System.currentTimeMillis() ), DefaultModel.class.getName(), 0 );
    resultParentType.addRelation( resultType.name(), resultType, 0, Integer.MAX_VALUE );
    Model resultParent = new DefaultModel( null, resultParentType );
    invokeGet( servicePath, query, resultParent, resultType.name() );
    List<T> modelList = new ArrayList<>();
    for (Model child : resultParent.iterator( resultType.name(), null )) {
      modelList.add( (T) child );
    }
    return modelList;
  }


  public void invokeGet( String servicePath, String query, Model resultParent, QName resultRelationID ) throws IOException, ServiceException {
    URL url = createURL( servicePath, query );

    HttpRequest httpRequest = httpConnection.call( url, "GET" );
    int responseCode = httpRequest.getResponseCode();
    if (responseCode == HttpURLConnection.HTTP_INTERNAL_ERROR) {
      JsonReader reader = new JsonReader();
      reader.loadData( httpRequest.getInputStream(), JsonRemoteException.createType(), "UTF-8" );
    }
    else if (responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
      handleStatusCodes_400( responseCode, httpRequest );
    }
    else {
      JsonReader reader = new JsonReader(this.ignoreAdditionalAttibutes);
      reader.loadDataList( httpRequest.getInputStream(), resultParent, resultRelationID, "UTF-8" );
    }
  }

  public void invokeGet( String servicePath, Model resultParent, QName resultRelationID ) throws IOException, ServiceException {
    URL url = createURL( servicePath );

    HttpRequest httpRequest = httpConnection.call( url, "GET" );
    int responseCode = httpRequest.getResponseCode();
    if (responseCode == HttpURLConnection.HTTP_INTERNAL_ERROR) {
      JsonReader reader = new JsonReader();
      reader.loadData( httpRequest.getInputStream(), JsonRemoteException.createType(), "UTF-8" );
    }
    else if (responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
      handleStatusCodes_400( responseCode, httpRequest );
    }
    else {
      JsonReader reader = new JsonReader(this.ignoreAdditionalAttibutes);
      reader.loadDataList( httpRequest.getInputStream(), resultParent, resultRelationID, "UTF-8" );
    }
  }

  private void handleStatusCodes_400( int responseCode, HttpRequest httpRequest ) throws ServiceException, IOException {
    if (responseCode == HttpURLConnection.HTTP_NOT_FOUND) {
      throw new ServiceException( Integer.toString( responseCode ), "Resource not found: " + httpRequest.getConnectionID() );
    }
    else {
      throw new ServiceException( Integer.toString( responseCode ), readErrorStream( httpRequest ) );
    }
  }


  public void invokeDelete( String servicePath, Model resultParent, QName resultRelationID ) throws IOException, ServiceException {
    URL url = createURL( servicePath );

    HttpRequest httpRequest = httpConnection.call( url, "DELETE" );
    int responseCode = httpRequest.getResponseCode();
    if (responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
      handleStatusCodes_400( responseCode, httpRequest );
    }

    JsonReader reader = new JsonReader(this.ignoreAdditionalAttibutes);
    reader.loadDataList( httpRequest.getInputStream(), resultParent, resultRelationID, "UTF-8" );
  }

  public HttpResult invokeDelete( String servicePath ) throws IOException, ServiceException {
    URL url = createURL( servicePath );

    HttpRequest httpRequest = httpConnection.call( url, "DELETE" );
    int responseCode = httpRequest.getResponseCode();
    if (responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
      handleStatusCodes_400( responseCode, httpRequest );
    }
    return new HttpResult( responseCode, readStringResult( httpRequest ) );
  }

  /**
   * Sends a file's content via POST.
   *
   * @param servicePath      The relative path on the server.
   * @param file             A file conteining the request body.
   * @param contentType      The content type.
   * @param followredirects  If false and a redirect occurs, the location will be stored in the result's rawText.
   * @param rawResult        If true, the result will NOT be parsed into a Model.
   * @param resultParent     The parent of the resulting Model.
   * @param resultRelationID @see Model.getParentRelationID()
   * @return
   * @throws IOException
   * @throws ServiceException
   */
  public HttpResult invokePost( String servicePath, File file, String contentType, boolean followredirects, boolean rawResult, Model resultParent, QName resultRelationID ) throws IOException, ServiceException {
    try (InputStream inputStream = new FileInputStream(file)) {
      String body = HttpRequest.readString( inputStream, "UTF-8" );
      return invokePost( servicePath, null, contentType, followredirects, rawResult, resultParent, resultRelationID, body );
    }
  }

  /**
   * Sends a file's content via POST.
   *
   * @param servicePath      The relative path on the server.
   * @param query            Query parameters (yes, they are allowed on POST)
   * @param file             A file conteining the request body.
   * @param contentType      The content type.
   * @param followredirects  If false and a redirect occurs, the location will be stored in the result's rawText.
   * @param rawResult        If true, the result will NOT be parsed into a Model.
   * @param resultParent     The parent of the resulting Model.
   * @param resultRelationID @see Model.getParentRelationID()
   * @return
   * @throws IOException
   * @throws ServiceException
   */
  public HttpResult invokePost( String servicePath, String query, File file, String contentType, boolean followredirects, boolean rawResult, Model resultParent, QName resultRelationID ) throws IOException, ServiceException {
    try (InputStream inputStream = new FileInputStream(file)) {
      String body = HttpRequest.readString( inputStream, "UTF-8" );
      return invokePost( servicePath, query, contentType, followredirects, rawResult, resultParent, resultRelationID, body );
    }
  }

  /**
   * Sends a Model as JSON via POST.
   *
   * @param servicePath      The relative path on the server.
   * @param request          The Model to send.
   * @param followredirects  If false and a redirect occurs, the location will be stored in the result's rawText.
   * @param rawResult        If true, the result will NOT be parsed into a Model.
   * @param resultParent     The parent of the resulting Model.
   * @param resultRelationID @see Model.getParentRelationID()
   * @return
   * @throws IOException
   * @throws ServiceException
   */
  public HttpResult invokePost( String servicePath, Model request, boolean followredirects, boolean rawResult, Model resultParent, QName resultRelationID ) throws IOException, ServiceException {
    JsonWriter writer = new JsonWriter();
    String body = writer.writeJson( request, request.getParentRelationID() ).toString();

    return invokePost( servicePath, null, CONTENT_TYPE_JSON, followredirects, rawResult, resultParent, resultRelationID, body );
  }

  /**
   * The core invokePost().
   *
   * @param servicePath      The relative path on the server.
   * @param contentType      The content type.
   * @param followredirects  If false and a redirect occurs, the location will be stored in the result's rawText.
   * @param rawResult        If true, the result will NOT be parsed into a Model.
   * @param resultParent     The parent of the resulting Model.
   * @param resultRelationID @see Model.getParentRelationID()
   * @param body             The request body.
   * @return an object containing the respoonseCode and the raw response String.
   * @throws IOException
   * @throws ServiceException
   */
  private HttpResult invokePost( String servicePath, String query, String contentType, boolean followredirects, boolean rawResult, Model resultParent, QName resultRelationID, String body ) throws IOException, ServiceException {
    URL url = createURL( servicePath, query );
    if (DEBUG) {
      Logger.debug( "to URL " + url.toString() );
    }
    HttpRequest httpRequest = httpConnection.call( url, contentType, "POST", followredirects, body );
    int responseCode = httpRequest.getResponseCode();
    if (responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
      handleStatusCodes_400( responseCode, httpRequest );
    }

    HttpResult result;

    if (responseCode == HttpURLConnection.HTTP_SEE_OTHER) {
      result = new HttpResult( responseCode, httpRequest.getResponseHeader( "Location" ) );
    }
    else if (rawResult) {
      result = new HttpResult( responseCode, readRawString( httpRequest ) );
    }
    else if (resultParent != null) {
      JsonReader reader = new JsonReader(this.ignoreAdditionalAttibutes);
      reader.loadDataList( httpRequest.getInputStream(), resultParent, resultRelationID, "UTF-8" );
      result = new HttpResult( responseCode );
    }
    else {
      result = new HttpResult( responseCode, readStringResult( httpRequest ) );
    }

    return result;
  }

  /**
   * Sends a JSON object to the server and receives another one.
   * Currently, only text/* type responses are supported.
   *
   * @param servicePath The relative path on the server.
   * @param request     The model that will be serialized from JSON.
   * @return an object containing the respoonseCode usually an empty Sting.
   * @throws IOException
   * @throws ServiceException
   */
  public HttpResult invokePost( String servicePath, Model request ) throws IOException, ServiceException {
    return invokePost( servicePath, request, false, false, null, null );
  }

  /**
   * invokePostRaw() is for special cases, when you send JSON, but receive something else. e.g. a HTML page.
   * Currently, only text/* type responses are supported.
   *
   * @param servicePath     The relative path on the server.
   * @param followredirects if false and a redirect occurs, the location will be stored in the result's rawText
   * @param request         The model that will be serialized as JSON.
   * @return an object containing the respoonseCode and the raw response String.
   * @throws IOException
   * @throws ServiceException
   */
  public HttpResult invokePostRaw( String servicePath, boolean followredirects, Model request ) throws IOException, ServiceException {
    return invokePost( servicePath, request, followredirects, true, null, null );
  }

  /**
   * invokePostRaw() is for special cases, when you send JSON, but receive something else. e.g. a HTML page.
   * Currently, only text/* type responses are supported.
   *
   * @param servicePath     The relative path on the server.
   * @param query            Query parameters (yes, they are allowed on POST)
   * @param file             A file conteining the request body.
   * @param contentType      The content type.
   * @param followredirects if false and a redirect occurs, the location will be stored in the result's rawText
   * @return an object containing the respoonseCode and the raw response String.
   * @throws IOException
   * @throws ServiceException
   */
  public HttpResult invokePostRaw( String servicePath, String query, File file, String contentType, boolean followredirects ) throws IOException, ServiceException {
    return invokePost( servicePath, query, file, contentType, followredirects, true, null, null );
  }

  /**
   * sends an empty message.
   *
   * @param servicePath     The relative path on the server.
   * @return the raw response String.
   * @throws IOException
   * @throws ServiceException
   */
  public String invokePost( String servicePath ) throws IOException, ServiceException {
    URL url = createURL( servicePath );

    HttpRequest httpRequest = httpConnection.call( url, "POST", " " );
    int responseCode = httpRequest.getResponseCode();
    if (responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
      handleStatusCodes_400( responseCode, httpRequest );
      // The return will not be reached, though the function will throw detailed ServiceExceptions
      return null;
    }
    else {
      return readStringResult( httpRequest );
    }
  }

  public List<HttpCookie> invokePostForCookies( String servicePath, Model request ) throws IOException, ServiceException {
    URL url = createURL( servicePath );

    JsonWriter writer = new JsonWriter();
    StringBuilder requestStr = writer.writeJson( request, request.getParentRelationID() );

    HttpRequest httpRequest = httpConnection.call( url, "POST", requestStr.toString() );
    int responseCode = httpRequest.getResponseCode();
    if (responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
      handleStatusCodes_400( responseCode, httpRequest );
      // The return will not be reached, though the function will throw detailed ServiceExceptions
      return null;
    }
    else {
      return httpConnection.getCookieManager().getCookieStore().getCookies();
    }
  }

  public <T extends Model> List<T> invokePostReturnAsList( String servicePath, Model request, Type resultType ) throws IOException, ServiceException {
    URL url = createURL( servicePath );

    DefaultComplexType resultParentType = new DefaultComplexType( BaseFactory.NAMESPACE.getQName( "" + System.currentTimeMillis() ), DefaultModel.class.getName(), 0 );
    resultParentType.addRelation( resultType.name(), resultType, 0, Integer.MAX_VALUE );
    Model resultParent = new DefaultModel( null, resultParentType );

    JsonWriter writer = new JsonWriter();
    StringBuilder requestStr = writer.writeJson( request, request.getParentRelationID() );

    HttpRequest httpRequest = httpConnection.call( url, "POST", requestStr.toString() );
    int responseCode = httpRequest.getResponseCode();
    if (responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
      handleStatusCodes_400( responseCode, httpRequest );
      // The return will not be reached, though the function will throw detailed ServiceExceptions
      return null;
    }
    else {
      JsonReader reader = new JsonReader(this.ignoreAdditionalAttibutes);
      reader.loadDataList( httpRequest.getInputStream(), resultParent, resultType.name(), "UTF-8" );

      List<T> modelList = new ArrayList<>();
      for (Model child : resultParent.iterator( resultType.name(), null )) {
        modelList.add( (T) child );
      }
      return modelList;
    }
  }

  public Model invokePost( String servicePath, Model request, Type resultType ) throws IOException, ServiceException {
    URL url = createURL( servicePath );
    

    JsonWriter writer = new JsonWriter();
    StringBuilder requestStr = writer.writeJson( request, request.getParentRelationID() );

    HttpRequest httpRequest = httpConnection.call( url, "POST", requestStr.toString() );
    int responseCode = httpRequest.getResponseCode();
    if (responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
      handleStatusCodes_400( responseCode, httpRequest );
      // The return will not be reached, though the function will throw detailed ServiceExceptions
      return null;
    }
    else {
      JsonReader reader = new JsonReader(this.ignoreAdditionalAttibutes);
      return reader.loadData( httpRequest.getInputStream(), resultType, "UTF-8" );
    }
  }

  public Model invokePost( String servicePath, Model request, Type resultType, boolean folowRedirect ) throws IOException, ServiceException {
    URL url = createURL( servicePath );

    JsonWriter writer = new JsonWriter();
    StringBuilder requestStr = writer.writeJson( request, request.getParentRelationID() );

    HttpRequest httpRequest = httpConnection.call( url, CONTENT_TYPE_JSON, "POST", folowRedirect, requestStr.toString() );
    int responseCode = httpRequest.getResponseCode();
    if (responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
      handleStatusCodes_400( responseCode, httpRequest );
      // The return will not be reached, though the function will throw detailed ServiceExceptions
      return null;
    }
    else {
      JsonReader reader = new JsonReader(this.ignoreAdditionalAttibutes);
      return reader.loadData( httpRequest.getInputStream(), resultType, "UTF-8" );
    }
  }


  public void invokePut( String servicePath, Model request ) throws IOException, ServiceException {
    URL url = createURL( servicePath );
    

    JsonWriter writer = new JsonWriter();
    StringBuilder requestStr = writer.writeJson( request, request.getParentRelationID() );

    HttpRequest httpRequest = httpConnection.call( url, "PUT", requestStr.toString() );
    int responseCode = httpRequest.getResponseCode();
    if (responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
      handleStatusCodes_400( responseCode, httpRequest );
    }
  }

  public Model invokePut( String servicePath, Model request, Type resultType ) throws IOException, ServiceException {
    URL url = createURL( servicePath );
    

    JsonWriter writer = new JsonWriter();
    StringBuilder requestStr = writer.writeJson( request, request.getParentRelationID() );

    HttpRequest httpRequest = httpConnection.call( url, "PUT", requestStr.toString() );
    int responseCode = httpRequest.getResponseCode();
    if (responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
      handleStatusCodes_400( responseCode, httpRequest );
      // The return will not be reached, though the function will throw detailed ServiceExceptions
      return null;
    }
    else {
      JsonReader reader = new JsonReader(this.ignoreAdditionalAttibutes);
      return reader.loadData( httpRequest.getInputStream(), resultType, "UTF-8" );
    }
  }

  public Model invokePutAsList( String servicePath, ModelIterator modelIterator, Type resultType ) throws IOException, ServiceException {
    URL url = createURL( servicePath );
    

    JsonWriter writer = new JsonWriter();
    StringBuilder requestStr = writer.writeJson( modelIterator );

    HttpRequest httpRequest = httpConnection.call( url, "PUT", requestStr.toString() );
    int responseCode = httpRequest.getResponseCode();
    if (responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
      handleStatusCodes_400( responseCode, httpRequest );
      // The return will not be reached, though the function will throw detailed ServiceExceptions
      return null;
    }
    else {
      JsonReader reader = new JsonReader(this.ignoreAdditionalAttibutes);
      return reader.loadData( httpRequest.getInputStream(), resultType, "UTF-8" );
    }
  }

  public Model invokePutAsList( String servicePath, List<Model> modelList, Type resultType ) throws IOException, ServiceException {
    URL url = createURL( servicePath );
    

    JsonWriter writer = new JsonWriter();
    StringBuilder requestStr = writer.writeJson( modelList );

    HttpRequest httpRequest = httpConnection.call( url, "PUT", requestStr.toString() );
    int responseCode = httpRequest.getResponseCode();
    if (responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
      handleStatusCodes_400( responseCode, httpRequest );
      // The return will not be reached, though the function will throw detailed ServiceExceptions
      return null;
    }
    else {
      JsonReader reader = new JsonReader(this.ignoreAdditionalAttibutes);
      return reader.loadData( httpRequest.getInputStream(), resultType, "UTF-8" );
    }
  }

  public void setRequestHeaders( Map<String, String> requestHeaders ) {
    httpConnection.setRequestHeaders( requestHeaders );
  }

  public void setProxy( String server, int port ) {
   httpConnection.setProxy(server, port);
  }

  public boolean isIgnoreAdditionalAttibutes() {
    return ignoreAdditionalAttibutes;
  }

  public void setIgnoreAdditionalAttibutes( boolean ignoreAdditionalAttibutes ) {
    this.ignoreAdditionalAttibutes = ignoreAdditionalAttibutes;
  }
}
