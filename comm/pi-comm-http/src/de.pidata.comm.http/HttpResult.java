/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.comm.http;

import de.pidata.models.tree.Model;

public class HttpResult {
    int responseCode;
    String rawText;
    Model model;

    public HttpResult(int responseCode) {
        this.responseCode = responseCode;
        this.rawText = null;
        this.model = null;
    }

    public HttpResult(int responseCode, String rawText) {
        this.responseCode = responseCode;
        this.rawText = rawText;
        this.model = null;
    }

    public HttpResult(int responseCode, Model model) {
        this.responseCode = responseCode;
        this.rawText = null;
        this.model = model;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getRawText() {
        return rawText;
    }

    public Model getModel() {
        return model;
    }
}
