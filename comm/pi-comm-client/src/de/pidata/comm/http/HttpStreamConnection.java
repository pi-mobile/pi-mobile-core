/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.comm.http;

import de.pidata.comm.client.base.AbstractHttpConnection;
import de.pidata.comm.client.base.Sender;
import de.pidata.comm.client.base.SoapSender;
import de.pidata.log.Logger;
import de.pidata.models.tree.Context;
import de.pidata.qnames.QName;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.SystemManager;

import java.io.*;
import java.net.*;
import java.util.Properties;

public class HttpStreamConnection extends AbstractHttpConnection {

  private static final boolean DEBUG = false;

  private HttpURLConnection httpConnection;
  private OutputStream outStream;
  private InputStream inStream = null;

  public HttpStreamConnection() {
    //----- HTTPS ermöglichen: Keystore mit nicht-signiertem key setzen
    SystemManager sysMan = SystemManager.getInstance();
    String keyStore = sysMan.getProperty( "comm.keystore", null );
    if (keyStore != null) {
      Properties sysProps = System.getProperties();
      sysProps.put( "javax.net.ssl.trustStore", keyStore );
      sysProps.put( "javax.net.ssl.trustStorePassword", sysMan.getProperty( "comm.keystorepasswd", "" ) );
      sysProps.put( "java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol" );  // only needed for CrEme
      System.setProperties( sysProps );
    }
  }

  /**
   * Set proxy to use for next call of open()
   *
   * @param proxy     proxy server name
   * @param proxyPort proxy server's port to use
   */
  protected void setProxy( String proxy, int proxyPort ) {
    throw new RuntimeException( "TODO" );
  }

  private URL createURL( String servicePath ) throws MalformedURLException {
    try {
      URI uri = new URI( protocol, null, host, port, servicePath, null, null );
      URL url = uri.toURL();
      return url;
    }
    catch (URISyntaxException e) {
      throw new IllegalArgumentException( "invalid servicePath [" + servicePath + "]", e );
    }
  }

  /**
   * Opens a connection
   *
   * @param serviceName   name of service to be called
   * @param operationName name of operation within service
   * @param mimeType      type of data beeing transferred
   * @param caller
   * @return the stream containing server'*s reply
   * @throws IOException
   */
  public OutputStream open( String serviceName, String operationName, String mimeType, Context caller ) throws IOException {
    setState( Sender.STATE_INIT );
    URL destination = createURL( path );
    httpConnection = (HttpURLConnection) destination.openConnection();
    httpConnection.setDoOutput(true);
    httpConnection.setRequestMethod( "POST" );
    httpConnection.setRequestProperty( SoapSender.HEADER_CONTENT_TYPE, "text/xml" );
    httpConnection.setRequestProperty( SoapSender.HEADER_SOAP_ACTION, serviceName + "/" + operationName ); // needed at least by MMontage

    //httpConnection.setRequestProperty( "Content-Length", "4" );
    // Das darf hier nicht drin stehen, sonst geht HTTPS nicht: httpConnection.setRequestProperty( "Host", "localhost:50001" );

    String senderID = caller.getCallerName();
    if (senderID == null) {
      senderID = caller.getClientID();
    }
    httpConnection.setRequestProperty( SoapSender.HEADER_SENDER_ID, senderID );  // needed at least by MMontage
    SystemManager sysMan = SystemManager.getInstance();

    String programName = null;
    String programVersion = null;
    String userID = null;
    programName = caller.getProgramName();
    programVersion = caller.getProgramVersion();
    userID = caller.getUserID();
    if (programName == null) {
      programName = sysMan.getProgramName();
      programVersion = sysMan.getProgramVersion();
    }
    httpConnection.setRequestProperty( SoapSender.HEADER_USER_AGENT, programName + "/" + programVersion );

    int maxLogsPerEnvelope = SystemManager.getInstance().getPropertyInt( SoapSender.PROP_HEADER_MAX_LOGS, Integer.MAX_VALUE );
    httpConnection.setRequestProperty( SoapSender.HEADER_MAX_LOGS, Integer.toString( maxLogsPerEnvelope ) );

    if (userID == null) {
      QName sysUserID = sysMan.getUserID();
      if (sysUserID != null) {
        userID = sysUserID.getName();
      }
    }
    if (userID == null) {
      httpConnection.setRequestProperty( SoapSender.HEADER_FROM, "" );
    }
    else {
      httpConnection.setRequestProperty( SoapSender.HEADER_FROM, userID );
    }

    setState( Sender.STATE_TRY_CONNECT );
    this.outStream = httpConnection.getOutputStream();
    setState( Sender.STATE_CONNECT );
    return outStream;
  }

  /**
   * Closes the current connection's up stream and returns the down stream.
   *
   * @return the down stream
   * @throws IOException
   */
  public InputStream getInputStream() throws IOException {
    this.outStream.close();
    this.outStream = null;
    setState( Sender.STATE_TRY_RECEIVE );
    //if (DEBUG) Logger.debug("Finished send, #bytes=" + sum);
    try {
      inStream = httpConnection.getInputStream();
      int httpStatus = this.httpConnection.getResponseCode();

      if (httpStatus != 200) {
        String errorMsg = "Http status code not ok: " + httpStatus;
        if (inStream != null) {
          dumpStream( inStream );
        }
        throw new IOException( errorMsg );
      }
    }
    catch (Exception ex) {
      int rc = httpConnection.getResponseCode();
      StringBuilder buf = new StringBuilder();
      StreamHelper.stream2Buffer( httpConnection.getErrorStream(), buf, 10000, false );
      Logger.error( buf.toString() );
      httpConnection.disconnect();
      Logger.error( "Exception while reading/parsing HTTP response stream, rc="+rc, ex );
      throw new IOException( "Exception while reading/parsing HTTP response, rc="+rc+", msg: " + ex.getMessage() );
    }
    return inStream;
  }

  /**
   * Schließt die Verbindung --> STATE_CLOSED
   */
  public void close() {
    StreamHelper.close( outStream );
    StreamHelper.close( inStream );
    try {
      httpConnection.disconnect();
    }
    catch (Exception ex) {
      // do nothing
    }
  }

  protected void dumpStream( InputStream inStream ) {
    InputStreamReader reader = new InputStreamReader( inStream );
    BufferedReader br = new BufferedReader( reader );
    String line;
    try {
      StringBuffer buf = new StringBuffer();
      buf.append( "REPLY MESSAGE DUMP : ---------------------" );
      while ((line = br.readLine()) != null) {
        buf.append( line );
      }
      Logger.debug( buf.toString() );
    }
    catch (IOException e) {
      Logger.error( "Could not dump input", e );
    }
  }

}
