// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.comm.soap;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.SequenceModel;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.complex.AnyElement;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.util.Hashtable;

public class Detail extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://schemas.xmlsoap.org/soap/envelope/" );


  public Detail() {
    super( null, SoapFactory.DETAIL_TYPE, null, null, null );
  }

  public Detail(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, SoapFactory.DETAIL_TYPE, attributeNames, anyAttribs, childNames);
  }

  protected Detail(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
