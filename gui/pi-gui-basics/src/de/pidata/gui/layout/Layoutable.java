/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.layout;

import de.pidata.gui.component.base.*;

public interface Layoutable {

  /**
   * Returns size limit for direction
   * @param direction the direction
   * @return this component's size limit in one direction
   */
  public SizeLimit getSizeLimit( Direction direction );

  /**
   * Returns grid position, cell span and alignment for direction
   * @param direction the direction
   * @return the LayoutInfo containing grid position, cell span and alignment for direction
   */
  public LayoutInfo getLayoutInfo( Direction direction );

  /**
   * Sets pixel positon relative to layout grid's origin for direction.
   * This method is called by layouter.
   * @param direction the direction
   * @param position  the new pixel position
   */
  public void setPos( Direction direction, short position );

  /**
   * Returns the pixel position for direction relative to layout grid's origin.
   * @param direction the direction
   * @return the layout position and size of this component
   */
  public short getPos( Direction direction);

  /**
   * Returns the components parent container for layout.
   * The parent container is responsible for layout and position of the component.
   * @return the parent container
   */
  public Container getLayoutParent();

  /**
   * Sets parent container for layout
   * @param parent the parent container
   */
  public void setLayoutParent( Container parent );

  /**
   * Called for painting this component
   * This method has to be implemented for each individual component.
   * @param graph the graphics context to use for painting
   */
  public void doPaint( ComponentGraphics graph);

  /**
    * Returns true if all parents of this Component are visible. If only
    * one of the parents is invisible false is returned.
    * @return true if all parents are visible
    */
  public boolean isVisible();
}
