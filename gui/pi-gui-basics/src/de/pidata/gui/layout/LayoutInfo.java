/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.layout;

/**
 * Stores a position in one direction (either row or column!) inside a container's layout grid.
 *
 */
public class LayoutInfo {

  public static final byte ALIGN_MIN = 0;
  public static final byte ALIGN_CENTER = 1;
  public static final byte ALIGN_MAX = 2;
  public static final byte ALIGN_GROW = 3;

  private byte cellNumber = 0;
  private byte cellSpan   = 1;
  private byte alignment  = ALIGN_MIN;

  /**
   * Construct a component which will be positioned by the layouter
   */
  public LayoutInfo() {
  }

  /**
   * Construct a component which we know will be positioned at cell and span cellSpan cells
   * @param cell
   * @param cellSpan
   */
  public LayoutInfo( byte cell, byte cellSpan, byte alignment ) {
    this.cellNumber = cell;
    this.cellSpan = cellSpan;
    this.alignment = alignment;
  }

  /**
   * Set the components position in the grid
   * @param cell
   */
  public void setPosition(byte cell) {
    this.cellNumber = cell;
  }

  /**
   * Set the cellSpan of the component
   * @param cellSpan
   */
  public void setSpan(byte cellSpan) {
    this.cellSpan = cellSpan;
  }

  /**
   * Sets alignment
   * @param alignment one of constants ALIGN_*
   */
  public void setAlignment( byte alignment ) {
    this.alignment = alignment;
  }

  /**
   * Return the components posiiton in the grid
   * @return the components posiiton in the grid
   */
  public byte getCellNumber() {
    return cellNumber;
  }

  /**
   * Return the components cellSpan
   * @return the components cellSpan
   */
  public byte getCellSpan() {
    return cellSpan;
  }

  /**
   * sets the position and span
   * @param cell
   * @param cellSpan
   */
  public void setGrid(byte cell, byte cellSpan) {
    this.setPosition(cell);
    this.setSpan(cellSpan);
  }

  /**
   * Returns alignment for direction
   * @param direction the direction
   * @return one of constants ALIGN_*
   */
  public byte getAlignment() {
    return alignment;
  }


}
