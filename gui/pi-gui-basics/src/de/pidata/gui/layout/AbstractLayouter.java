/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.layout;

import de.pidata.gui.component.base.*;
import de.pidata.log.Logger;

import java.util.Vector;


public abstract class AbstractLayouter implements Layouter {

  private static final boolean DEBUG = false;
  private static final String ME = "AbstractLayouter";

  private Container target;
  private final Direction direction;

  /** Hier werden die Größen der Zellen in einer Richtung festgehalten.
   * Die Anzahl der Einträge entspricht der Zeile/Spaltenanzahl.
   */
  private Vector layoutCells;
  private Vector longSpanComps = new Vector();

  private short gap;

  public AbstractLayouter (Direction direction) {
    this.direction = direction;
    this.layoutCells = new Vector();
    //todo: Stadard-Wert konfigurierbar
    ComponentFactory compFactory = Platform.getInstance().getComponentFactory();
    if (direction == Direction.X) this.gap = compFactory.getGapX();
    else this.gap = compFactory.getGapY();
  }

  public AbstractLayouter (Direction direction, int cellCount) {
    this.direction = direction;
    this.layoutCells = new Vector(cellCount);
    //todo: Stadard-Wert konfigurierbar
    ComponentFactory compFactory = Platform.getInstance().getComponentFactory();
    if (direction == Direction.X) this.gap = compFactory.getGapX();
    else this.gap = compFactory.getGapY();
  }

  public void setGap (short gap) {
    this.gap = gap;
  }

  /**
   * Sets the target for this layouter. Do not call directly. This method
   * is called by Container when its layouter is set.
   * @param target the target for this layouter
   */
  public void setTarget (Container target) {
    this.target = target;
  }

  /**
   * Returns this layouters target
   * @return this layouters target
   */
  public Container getTarget () {
    return this.target;
  }

  /**
   * Returns this layouters direction
   * @return this layouters direction
   */
  public Direction getDirection () {
    return direction;
  }

  /**
   * Returns the number of cells this Container uses for layouting its components
   * @return the number of cells this Container uses for layouting its components
   */
  public int cellCount () {
    return this.layoutCells.size();
  }

  /**
   * Creates the array for keeping the sizes for all cells
   * @param cellcount
   */
  protected void addLayoutCells (int cellcount) {
    for (int i=0; i<cellcount; i++) {
      this.layoutCells.addElement(new LayoutCell());
    }
  }

  private LayoutCell getLayoutCell ( int index) {
    int maxIndex = cellCount() - 1;
    // if cell is not present, create it
    if (index > maxIndex) {
      addLayoutCells(index - maxIndex);
    }
    return (LayoutCell)this.layoutCells.elementAt(index) ;
  }

  /**
   * Returns the visible content size. This is the current size of the container
   * minus the size of the scroller in the opposite direction if it exists.
   * @return the visible content size
   */
  public short visibleContentSize (short targetSize) {
    Scroller scroller = target.getScroller(direction.getOpposite());
    int scrollerSize = 0;

    // get size of scroller if visible
    if ((scroller != null) && (scroller.getComponentState().isVisible())) {
      scrollerSize = scroller.getSizeLimit(direction).getCurrent();
    }

    return (short) (targetSize - scrollerSize);
  }

  /**
   * Calculate minimum, preferred and maximum sizes for this layouter's
   * layout cells and set target's sizeLimit
   * @param sizeLimit
   */
  public void calcSizes (SizeLimit sizeLimit) {
    if (DEBUG) Logger.info(ME + ".calcSizes() -->");

    Container container = (Container) getTarget();
    PaintState paintState = container.getPaintState(direction);

    if (paintState.hasPaintStateReached(PaintState.STATE_SIZES_CALC)) {
      return;
    }

    this.layoutCells.removeAllElements();
    this.longSpanComps.removeAllElements();
    calcCellSizes(container);
    if (longSpanComps.size() > 0) {
      distributeLongSpanComps();
    }
    setContainerSize(sizeLimit);

    paintState.doPaintState(PaintState.STATE_SIZES_CALC);
    if (DEBUG) Logger.info(ME + ".calcSizes() <--");
  }

  /**
   * Find the space for Layoutables which span over more than one cell.
   * If there is enough space within the spanned cells, do nothing.
   * If there is an empty cell, set its size to the missing space.
   * Otherwise resize the last cell.
   */
  private void distributeLongSpanComps () {
    Layoutable layoutable;
    SizeLimit compSizeLimit;
    LayoutCell cell;
    int cellIndex;
    int cellSpan;
    int sumCellMin;
    int sumCellPref;
    int sumCellMax;
    int delta;
    int minSize, prefSize, maxSize;
    SizeLimit cellSizeLimit;
    boolean emptyCellFound = false;
    int emptyCellIndex = -1;

    for (int i=0; i<longSpanComps.size(); i++) {
      layoutable = (Layoutable) longSpanComps.elementAt(i);
      compSizeLimit = layoutable.getSizeLimit(direction);
      cellIndex = layoutable.getLayoutInfo(direction).getCellNumber();
      cellSpan = layoutable.getLayoutInfo(direction).getCellSpan();
      sumCellMin = 0;
      sumCellPref = 0;
      sumCellMax = 0;
      for (int cellNum=0; cellNum<cellSpan; cellNum++) {
        cell = getLayoutCell(cellIndex + cellNum);
        cellSizeLimit = cell.getSizeLimit();
        sumCellMin += (cellSizeLimit.getMin() + gap);
        sumCellPref += (cellSizeLimit.getPref() + gap);
        sumCellMax += (cellSizeLimit.getMax() + gap);
        if (!cell.isUsed() && !emptyCellFound) {
          emptyCellIndex = cellIndex + cellNum;
        }
      }
      if (cellSpan > 0 && !emptyCellFound) {
        sumCellMin -= gap;
        sumCellPref -= gap;
        sumCellMax -= gap;
      }

      if (sumCellMin > Short.MAX_VALUE) sumCellMin = Short.MAX_VALUE;
      if (sumCellPref > Short.MAX_VALUE) sumCellPref = Short.MAX_VALUE;
      if (sumCellMax > Short.MAX_VALUE) sumCellMax = Short.MAX_VALUE;

      cell = findFillCell(cellIndex, cellSpan, emptyCellIndex);
      minSize = cell.getSizeLimit().getMin();
      prefSize = cell.getSizeLimit().getPref();
      maxSize = cell.getSizeLimit().getMax();

      delta = compSizeLimit.getMin() - sumCellMin;
      if (delta > 0) {
        minSize += delta;
        if (minSize > Short.MAX_VALUE) minSize = Short.MAX_VALUE;
      }
      // if Layoutable wants as much as possible, adjust to min
      if (compSizeLimit.getPref() < Short.MAX_VALUE) {
        delta = compSizeLimit.getPref() - sumCellPref;
      }
      else {
        delta = compSizeLimit.getMin() - sumCellPref;
      }
      if (delta > 0) {
        prefSize += delta;
        if (prefSize > Short.MAX_VALUE) prefSize = Short.MAX_VALUE;
      }
      delta = compSizeLimit.getMax() - sumCellMax;
      if (delta > 0) {
        maxSize += delta;
        if (maxSize > Short.MAX_VALUE) maxSize = Short.MAX_VALUE;
      }

      if (minSize > prefSize) prefSize = minSize;
      if (prefSize > maxSize) maxSize = prefSize;
      cell.getSizeLimit().set(minSize, prefSize, maxSize);
    }
  }

  /**
   * Find the cell which can be filled with the oversize of a multispan field.
   * @param cellIndex
   * @param cellSpan
   * @param emptyCellIndex
   * @return the cell to use for filling
   */
  private LayoutCell findFillCell(int cellIndex, int cellSpan, int emptyCellIndex) {
    LayoutCell cell = null;
    SizeLimit fillSizeLimit;

    // if empty cell found, fill it
    if (emptyCellIndex >= 0) {
      cell = getLayoutCell(emptyCellIndex);
    }
    else {
      // if no empty cell was found, try to find the last cell which can grow
      int fillIndex = cellIndex + cellSpan - 1;
      while (fillIndex >= cellIndex && cell == null) {
        fillSizeLimit = getLayoutCell(fillIndex).getSizeLimit();
        if (fillSizeLimit.getMax() > fillSizeLimit.getPref()) {
          cell = getLayoutCell(fillIndex);
        }
        else {
          fillIndex--;
        }
      }
    }
    // if no matching cell was found, add difference to last cell of
    if (cell == null) {
      cell = getLayoutCell(cellIndex + cellSpan - 1);
    }
    return cell;
  }

  /**
   * Collects all Layoutables and sets their sizes onto the used cells
   * @param container
   */
  private void calcCellSizes( Container container ) {
    Layoutable layoutable;
    SizeLimit compSizeLimit;
    int cellIndex;

    // get all managed Layoutables sizes into the cells sizes
    int compCount = container.childCount();
    for (int i=0; i < compCount; i++) {
      layoutable = container.getChild(i);
      compSizeLimit = layoutable.getSizeLimit(direction);

      if ((layoutable instanceof Container) && !((Container) layoutable).doesLayout( direction )) {
        calcCellSizes( (Container) layoutable );
      }
      else {
        if (layoutable.getLayoutInfo(direction).getCellSpan() > 1) {
          // queue for later distribution
          longSpanComps.addElement( layoutable );
        }
        else {
          cellIndex = calcCellNumber( layoutable );
          getLayoutCell( cellIndex ).setCellSizes( compSizeLimit );
        }
      }
    }
  }

  /**
   * Add all previous cells sizes into the offset.
   */
  private void calcCellOffsets () {
    int numCells = cellCount();
    int offset;
    LayoutCell prevCell;
    getLayoutCell(0).setOffset((short) 0);
    for (int i = 1; i < numCells; i++) {
      prevCell = getLayoutCell(i-1);
      offset = prevCell.getOffset() + prevCell.getSizeLimit().getCurrent();
      offset += gap;
      getLayoutCell(i).setOffset((short) offset);
    }
  }

  /**
   * Determine the cell number in which the Layoutable will be placed.
   * If cell number exceeds the current managed cells, add new cells.
   * @param Layoutable
   * @return the cell number
   */
  private int calcCellNumber (Layoutable layoutable) {
    return layoutable.getLayoutInfo(direction).getCellNumber();
  }

  /**
   * Set size and position of all components managed by this layouter.
   */
  private void setCurrentSize (Container container, short containerOffset) {
    int compCount = container.childCount();
    int compIndex;
    Layoutable layoutable;
    SizeLimit compSizeLimit;
    short cellIndex;
    short cellSpan;
    short cellSpanSize;
    short cellPos;
    int compPos;
    short compSize;
    short compSizePref;
    short compAlign;

    // reference components within cells
    for (compIndex = 0; compIndex < compCount; compIndex++) {
      layoutable = container.getChild(compIndex);
      compSizeLimit = layoutable.getSizeLimit(direction);

      cellIndex = layoutable.getLayoutInfo(direction).getCellNumber();
      cellSpan = layoutable.getLayoutInfo(direction).getCellSpan();
      cellSpanSize = getSpanCurrentSize(cellIndex, cellSpan);

      //get cell position relative to inner container area
      cellPos = getLayoutCell(cellIndex).getOffset();
      compSizePref = compSizeLimit.getPref();

      // adjust layoutable in cell according to alignment
      compAlign = layoutable.getLayoutInfo( direction ).getAlignment();
      if(compAlign == LayoutInfo.ALIGN_GROW) {
        compSize = cellSpanSize;
      }
      else {
        compSize = Tools.min(cellSpanSize, compSizePref);
      }
      switch(compAlign) {
        case LayoutInfo.ALIGN_MAX:
          compPos = cellSpanSize - compSize;
          break;
        case LayoutInfo.ALIGN_CENTER:
          compPos = (cellSpanSize - compSize) / 2;
          break;
        case LayoutInfo.ALIGN_MIN:
        case LayoutInfo.ALIGN_GROW:
        default:
          compPos = 0;
      }
      // later on painting is done relative to outer upper-left corner of container,
      // so we have to add layout offset
      try {
        layoutable.setPos(direction, (short) (cellPos + compPos + containerOffset));
        compSizeLimit.setCurrentFit(compSize);

        if (layoutable instanceof Container) {
          Container layoutContainer = (Container) layoutable;
          if (!layoutContainer.doesLayout(direction)) {
            layoutContainer.getPaintState( direction ).doPaintState( PaintState.STATE_BOUNDS_SET );
            setCurrentSize( layoutContainer, (short) -cellPos );
            layoutContainer.getPaintState( direction ).doPaintState( PaintState.STATE_LAYOUTED );
          }
        }
      }
      catch (Exception ex) {
        // We must continue, even if an exception occurs, otherwise the GUI might freeze
        // due to a loop producing the same exception over and over again
        Logger.error("Error while layouting", ex);
      }
    }
  }

  /**
   * Adjust sizes for scroller and set the containers size
   * @param sizeLimit size of container
   */
  protected void setContainerSize (SizeLimit sizeLimit) {

    doLayoutContainerSize(sizeLimit);

    setSpanSizes(sizeLimit, (short) 0, (short) cellCount());

    target.calcSizeLimit(sizeLimit, direction);

    setChildContainerSizes(target);
  }


  abstract void doLayoutContainerSize (SizeLimit sizeLimit);

  private void setChildContainerSizes (Container container) {
    int compCount = container.childCount();
    int compIndex;
    Layoutable layoutable;
    SizeLimit compSizeLimit;
    short start;
    short span;

    // reference components within cells
    for (compIndex = 0; compIndex < compCount; compIndex++) {
      layoutable = container.getChild(compIndex);
      if (layoutable instanceof Container) {
        Container layoutContainer = (Container) layoutable;
        if (!layoutContainer.doesLayout( direction )) {
          compSizeLimit = layoutContainer.getSizeLimit( direction );

          start = layoutContainer.getLayoutInfo( direction ).getCellNumber();
          span = layoutContainer.getLayoutInfo( direction ).getCellSpan();
          setSpanSizes( compSizeLimit, start, span );

          layoutContainer.getPaintState( direction ).doPaintState( PaintState.STATE_SIZES_CALC );

          setChildContainerSizes( layoutContainer );
        }
      }
    }
  }

  /**
   * Calculate and set sizes for given cell span.
   * @param sizeLimit
   * @param cellNum
   * @param cellSpan
   */
  private void setSpanSizes (SizeLimit sizeLimit, short cellNum, short cellSpan) {
    int minSize = 0;
    int prefSize = 0;
    int maxSize = 0;
    SizeLimit cellSizeLimit;
    for (int i=cellNum; i < (cellNum + cellSpan); i++) {
      cellSizeLimit = getLayoutCell(i).getSizeLimit();
      minSize += (cellSizeLimit.getMin() + gap);
      prefSize += (cellSizeLimit.getPref() + gap);
      maxSize += (cellSizeLimit.getMax()+ gap);
    }
    if (cellSpan > 0) {
      minSize -= gap;
      prefSize -= gap;
      maxSize -= gap;
    }
    sizeLimit.set(minSize, prefSize, maxSize);
  }

  /**
   * Calculate and set current size for given cell span.
   * @param cellNum
   * @param cellSpan
   */
  private short getSpanCurrentSize (short cellNum, short cellSpan) {
    short currentSize = 0;
    SizeLimit cellSizeLimit;
    for (int i=cellNum; i < (cellNum + cellSpan); i++) {
      cellSizeLimit = getLayoutCell(i).getSizeLimit();
      currentSize += cellSizeLimit.getCurrent() + gap;
    }
    if (cellSpan > 0) currentSize -= gap;
    return currentSize;
  }


  /**
   * Distributes the remaining size delta over all cells.
   * Loops over the cells to layout until all space is distributed or all
   * cells reached maximum/minimum size.
   * @param delta  the remaining space to distribute over sizes.
   */
  protected void distributeSize (int delta) {
    int numCells = cellCount();
    SizeLimit cellSizes;
    int maxAllowedSize;
    int minAllowedSize;
    int currentCellSize;
    boolean changed = true;
    int cellIndex;
    int changeSize;
    int cellDelta;

    if (numCells == 0) {
      return;
    }

    if (delta > 0) {
      while ((delta > 0) && (changed)) {
        changed = false;
        changeSize = delta / numCells;
        if (changeSize == 0) changeSize = 1;

        for (cellIndex = 0; ((cellIndex < numCells) && (delta > 0)); cellIndex++) {
          cellSizes = getLayoutCell(cellIndex).getSizeLimit();
          maxAllowedSize = cellSizes.getMax();
          currentCellSize = cellSizes.getCurrent();
          if (currentCellSize < maxAllowedSize) {
            cellDelta = changeSize;
            if (currentCellSize+cellDelta > maxAllowedSize) {
              cellDelta = maxAllowedSize - currentCellSize;
            }
            cellSizes.setCurrent((short) (currentCellSize + cellDelta));
            delta -= cellDelta;
            changed = true;
          }
        }
      }
    }
    else if (delta < 0) {
      while ((delta < 0) && (changed)) {
        changed = false;
        changeSize = delta / numCells;
        if (changeSize == 0) changeSize = -1;

        for (cellIndex = 0; ((cellIndex < numCells) && (delta < 0)); cellIndex++) {
          cellSizes = getLayoutCell(cellIndex).getSizeLimit();
          minAllowedSize = cellSizes.getMin();
          currentCellSize = cellSizes.getCurrent();
          if (currentCellSize > minAllowedSize) {
            cellDelta = changeSize;
            if (currentCellSize+cellDelta < minAllowedSize) {
              cellDelta = minAllowedSize - currentCellSize;
            }
            cellSizes.setCurrent((short) (currentCellSize + cellDelta));
            delta -= cellDelta;
            changed = true;
          }
        }
      }
    }
  }

  /**
   * Use container dialogController's current size, calculate apropriate sizes for conatainer's
   * children and set children's current sizes.
   */
  public void doLayout(short origin, short targetSize) {
    Container container = getTarget();
    PaintState paintState;

    paintState = container.getPaintState(direction);
    if (!paintState.hasPaintStateReached(PaintState.STATE_BOUNDS_SET)) {
      return;
    }

    // Set the components start size to pref
    short prefSize = prefSizeChilds();

    // determine the size for distributing the components;
    // also decide if scroller is necessasry
    short layoutSize = target.getLayoutSize( direction, targetSize, minSizeChilds(), prefSize );

    // loop until all space is distributed
    // or all components reached maximum height/width
    distributeSize(layoutSize - prefSize);

    // get the offsets for positioning
    calcCellOffsets();

    // set calculated position and size for all managed components of this layouter
    target.setChildrenSize(direction, layoutSize);
    setCurrentSize(target, origin);

    paintState.doPaintState(PaintState.STATE_LAYOUTED);
  }

  /**
   * Sets the cell's current size to pref and returns the needed size for all cells.
   * @return the needed size for all pref sizes
   */
  private short prefSizeChilds() {
    int sumSizes = 0;
    short prefSize;
    SizeLimit cellSize;
    int numCells = cellCount();
    for (int i = 0; i < numCells; i++) {
      cellSize = getLayoutCell(i).getSizeLimit();
      prefSize = cellSize.getPref();
      if (prefSize < Short.MAX_VALUE) {
        cellSize.setCurrentFit(prefSize);
      }
      else {
        cellSize.setCurrentFit(cellSize.getMin());
      }
      sumSizes += cellSize.getCurrent();
      sumSizes += gap;
    }
    if (numCells > 0) sumSizes -= gap;
    if (sumSizes > Short.MAX_VALUE) {
      return Short.MAX_VALUE;
    }
    else {
      return (short) sumSizes;
    }
  }

  /**
   * Adds the minimum sizes of all cells.
   * @return the minimum size necessary for all cells
   */
  private short minSizeChilds() {
    int numCells = cellCount();
    short minSizeChilds;
    // collect sizes of the components
    minSizeChilds = 0;
    for (int i = 0; i < numCells; i++) {
      minSizeChilds += getLayoutCell(i).getSizeLimit().getMin();
      minSizeChilds += gap;
    }
    if (numCells > 0) minSizeChilds -= gap;
    return minSizeChilds;
  }

}