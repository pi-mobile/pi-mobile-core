/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.layout;

import de.pidata.gui.component.base.Container;
import de.pidata.gui.component.base.Direction;
import de.pidata.gui.component.base.SizeLimit;

/**
 * Layouter is used by a Container for layout calculation. Methods should only
 * be called by the Container itself.
 *
 * Layouter calculates only in one direction; the container has to
 * use the appropriate Layouter for X and Y.
 *
 * When creating a container, the X and Y layouter have to be created first and given
 * to the containers constructor.
 *
 * When adding or removing compnents, the container must undo paintState
 * STATE_SIZES_CALC. Anay Layouter should rebuild all cached values when
 * entering paint state STATE_SIZES_CALC.
 */
public interface Layouter{

  /**
   * Sets the target for this layouter. Do not call directly. This method
   * is called by target when its layouter is set.
   * @param target the target for this layouter
   */
  public void setTarget(Container target);

  /**
   * Returns this layouter's target
   * @return this layouter's target
   */
  public Container getTarget();

  /**
   * Calculate minimum, preferred and maximum sizes for this layouter's
   * layout cells and set target's widthLimit / heightLimit
   * @param sizeLimit
   */
  public void calcSizes(SizeLimit sizeLimit);

  /**
   * Use container dialogController's current size, calculate sizes for container's
   * children and set children's current sizes.
   * @param origin     the layout origin = min value for children's position
   * @param targetSize the size to use for layout
   */
  public void doLayout(short origin, short targetSize);

  /**
   * Returns the direction for which this layoter ist used
   * @return the direction for which this layouter is used (DIRECTION_X / DIRECTION_Y)
   */
  Direction getDirection();

  void setGap (short gap);
}
