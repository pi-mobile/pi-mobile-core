/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.layout;

import de.pidata.gui.component.base.Direction;
import de.pidata.gui.component.base.SizeLimit;

/**
 * Setzt alle Komponenten in einer Richtung auf dieselbe Position.
 */
public class StackLayouter extends AbstractLayouter{

  private static final boolean DEBUG = false;
  private static final String ME = "FlowLayouter";

  public StackLayouter(Direction direction) {
    super(direction);
  }

  public void doLayoutContainerSize (SizeLimit sizeLimit) {
    // no action needed
  }
}
