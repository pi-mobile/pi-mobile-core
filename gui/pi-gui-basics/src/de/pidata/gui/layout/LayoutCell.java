/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.layout;

import de.pidata.gui.component.base.SizeLimit;
import de.pidata.gui.component.base.Tools;


/**
 * LayoutCell is used to keep the layout information for one cell managed by a layouter.
 */
public class LayoutCell {

  private SizeLimit sizeLimit;
  private short offset;
  private boolean isUsed;

  public LayoutCell () {
    this.sizeLimit = new SizeLimit();
    this.offset = 0;
    isUsed = false;
  }

  public SizeLimit getSizeLimit () {
    return sizeLimit;
  }

  public short getOffset () {
    return offset;
  }

  public void setOffset (short offset) {
    this.offset = offset;
  }

  public boolean isUsed () {
    return isUsed;
  }

  public void setUsed (boolean used) {
    isUsed = used;
  }

  /**
   * Calculate cell sizes. Use the maximum size of all components in the same cell.
   * @param compSizeLimit size of currently processed component
   */
  protected void setCellSizes (SizeLimit compSizeLimit) {
    short minSize = Tools.max( sizeLimit.getMin(), compSizeLimit.getMin());
    short prefSize = Tools.max( sizeLimit.getPref(), compSizeLimit.getPref());
    short maxSize = Tools.max( sizeLimit.getMax(), compSizeLimit.getMax());

    sizeLimit.set(minSize, prefSize, maxSize);

    setUsed(true);
  }
}
