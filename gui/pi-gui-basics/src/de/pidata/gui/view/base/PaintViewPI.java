/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.PaintController;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIPaintAdapter;
import de.pidata.gui.view.figure.Figure;
import de.pidata.gui.view.figure.ShapePI;
import de.pidata.qnames.QName;
import de.pidata.rect.Rect;
import de.pidata.rect.SimpleRect;

import java.util.ArrayList;
import java.util.List;

public class PaintViewPI extends AbstractViewPI {

  private UIPaintAdapter paintAdapter;
  private PaintController paintController;
  private List<Figure> figureList = new ArrayList<Figure>();
  private Rect bounds = new SimpleRect( 0,0, 1, 1 );

  public PaintViewPI( QName componentID, QName moduleID ) {
    super( componentID, moduleID );
  }

  public Controller getController() {
    return paintController;
  }

  public void setController( PaintController paintController ) {
    this.paintController = paintController;
  }

  public UIAdapter getUIAdapter() {
    return paintAdapter;
  }

  /**
   * Called to attach this view's to it's UIAdapter and UI component
   *  @param uiContainer the dialog to look for this view's dialog component
   *
   */
  public void attachUIComponent( UIContainer uiContainer ) {
    if (paintAdapter == null) {
      paintAdapter = uiContainer.getUIFactory().createPaintAdapter( uiContainer, this );
    }
  }

  /**
   * Called to create UIAdapter for matching component in uiContainer.
   * The view must not store any link to the created UIAdapter.
   * This kind of UIAdapters is used for rendering table a tree cells and
   * makes it possible to process events from within such a cell by this view.
   *
   * @param uiContainer the container to look for this view's UI component
   * @return the UIAdapter
   */
  @Override
  public UIAdapter attachUIRenderComponent( UIContainer uiContainer ) {
    if (paintAdapter == null) {
      paintAdapter = uiContainer.getUIFactory().createPaintAdapter( uiContainer, this );
    }
    return paintAdapter;
  }

  public void detachUIComponent() {
    if (paintAdapter != null) {
      paintAdapter.detach();
      paintAdapter = null;
    }
  }

  public Object getValue() {
    if (paintAdapter != null) {
      return paintAdapter.getValue();
    }
    return null;
  }

  public void updateValue( Object value ) {
    if (paintAdapter != null) {
      if (value instanceof Figure) {
        Figure figure = (Figure) value;

        if (figureCount() > 1) {
          for (Figure f : figureList) {
            removeFigure( f );
          }
          addFigure( figure );
        }
        else if (figureCount() == 0) {
          addFigure( figure );
        }
        else {
          Figure currentFigure = figureList.get( 0 );
          if (currentFigure == figure) {
            figureModified( figure, null );
          }
          else {
            removeFigure( currentFigure );
            addFigure( figure );
          }
        }
      }
      else if (value == null) {
        for (Figure figure : figureList) {
         removeFigure( figure );
        }
      }
      else {
        paintAdapter.setValue( value );
      }
    }
  }

  /**
   * Returns a Rect representing the complete Area occupied by this PaintView and providing
   * size change events whenever this PaintView's size changes.
   * This makes it is possible to align Figures to the bounds of this PaintView.

   * @return this PaintView's bounds
   */
  public Rect getBounds() {
    return bounds;
  }

  public void addFigure( Figure figure ) {
    figureList.add( figure );
    figure.setPaintViewPI( this );
    figureAdded( figure );
  }

  public void removeFigure( Figure figure ) {
    figureList.remove( figure );
    figureRemoved( figure );
    figure.setPaintViewPI( null );
  }

  public int figureCount() {
    return figureList.size();
  }

  public Figure getFigure( int index ) {
    return figureList.get( index );
  }

  public void figureModified( Figure figure, ShapePI shapePI ) {
    if (paintAdapter != null) {
      paintAdapter.figureModified( figure, shapePI );
    }
  }

  public void figureAdded( Figure figure ) {
    if (paintAdapter != null) {
      paintAdapter.figureAdded( figure );
    }
  }

  public void figureRemoved( Figure figure ) {
    if (paintAdapter != null) {
      paintAdapter.figureRemoved( figure );
    }
  }

  public void shapeRemoved( Figure figure, ShapePI shapePI ) {
    if (paintAdapter != null) {
      paintAdapter.shapeRemoved( figure, shapePI );
    }
  }
}
