/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.ImageController;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIImageAdapter;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

public class ImageViewPI extends AbstractViewPI {

  private UIImageAdapter  imageAdapter;
  private ImageController imageController;

  public ImageViewPI( QName componentID, QName moduleID ) {
    super( componentID,moduleID );
  }

  public Controller getController() {
    return imageController;
  }

  public void setController( ImageController imageController ) {
    this.imageController = imageController;
  }

  public UIAdapter getUIAdapter() {
    return imageAdapter;
  }

  /**
   * Called to attach this view's to it's UIAdapter and UI component
   *  @param uiContainer the dialog to look for this view's dialog component
   *
   */
  public void attachUIComponent( UIContainer uiContainer ) {
    if (imageAdapter == null) {
      imageAdapter = uiContainer.getUIFactory().createImageAdapter( uiContainer, this );
    }
  }

  /**
   * Called to create UIAdapter for matching component in uiContainer.
   * The view must not store any link to the created UIAdapter.
   * This kind of UIAdapters is used for rendering table a tree cells and
   * makes it possible to process events from within such a cell by this view.
   *
   * @param uiContainer the container to look for this view's UI component
   * @return the UIAdapter
   */
  @Override
  public UIAdapter attachUIRenderComponent( UIContainer uiContainer ) {
    return uiContainer.getUIFactory().createImageAdapter( uiContainer, this );
  }

  public void detachUIComponent() {
    if (imageAdapter != null) {
      imageAdapter.detach();
      imageAdapter = null;
    }
  }

  /**
   * Updates the value at (posX,posY). The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX      horizontal position, starting from 0
   * @param posY      vertical position, starting from 0
   * @param value     the new value
   */
  public void updateValue( short posX, short posY, Object value ) {
    if (imageAdapter != null) {
      if (value == null) {
        imageAdapter.setImage( null );
      }
      else if (value instanceof QName) {
        imageAdapter.setImageResource( (QName) value );
      }
      else if (value instanceof ComponentBitmap) {
        imageAdapter.setImage( (ComponentBitmap) value );
      }
      else {
        throw new IllegalArgumentException( "Unsupported value class="+value.getClass() );
      }
    }
  }

  public boolean isZoomEnabled(){
    return imageAdapter.isZoomEnabled();
  }

  public void setZoomFactor( double zoomFactor){
    imageAdapter.setZoomFactor(zoomFactor);
  }

  public double getZoomFactor (){
    return imageAdapter.getZoomFactor();
  }

  public void onZoomFactorChanged( double newZoomFactor, Model dataContext ){
    imageController.onZoomChanged( newZoomFactor, dataContext );
  }
}
