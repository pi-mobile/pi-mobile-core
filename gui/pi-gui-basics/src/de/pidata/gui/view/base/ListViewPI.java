/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.controller.base.ColumnInfo;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.SelectionController;
import de.pidata.gui.renderer.StringRenderer;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIListAdapter;
import de.pidata.log.Logger;
import de.pidata.models.tree.EventListener;
import de.pidata.models.tree.EventSender;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

/**
 * A ListViewPI represents a List visible by a platform specific UI component.
 *
 * A ListViewPI consists of multiple rows. Each row has the same structure in UI. The ListController
 * maintains this ListViewPI's array of data rows.
 *
 * A data row always is a Model instance.
 */
public class ListViewPI extends AbstractViewPI implements EventListener {

  public static final boolean DEBUG = false;

  private QName rowCompID;
  private QName rowColorID;
  private QName colorColumnID;
  private ComponentColor selectedRowColor = null;
  private ComponentColor selectedRowFontColor = null;

  protected UIListAdapter  uiListAdapter;
  private SelectionController listCtrl;

  private QName displayValueID;

  public ListViewPI( QName componentID, QName moduleID, QName rowCompID, QName rowColorID, QName colorColumnID ) {
    super( componentID, moduleID );
    this.rowCompID = rowCompID;
    this.rowColorID = rowColorID;
    this.colorColumnID = colorColumnID;
  }

  public Controller getController() {
    return listCtrl;
  }

  public void setController( SelectionController listController ) {
    this.listCtrl = listController;
  }

  /**
   * Returns cell renderer for columnInfo
   * @param columnInfo columnInfo or null for default renderer
   * @param cellValue value to be rendered
   * @return a String rendered from cellValue
   */
  public String render( ColumnInfo columnInfo, Object cellValue ) {
    if (columnInfo == null) {
      return StringRenderer.get( getInputMode() ).render( cellValue );
    }
    else {
      Controller renderCtrl = columnInfo.getRenderCtrl();
      if (renderCtrl == null) {
        return StringRenderer.get( getInputMode() ).render( cellValue );
      }
      else {
        return renderCtrl.render( cellValue );
      }
    }
  }

  /**
   * Returns this list view's controller
   * @return the controller
   */
  public SelectionController getListCtrl() {
    return listCtrl;
  }

  /**
   * Returns the UIAdapter connected to this view. The UI adapter is null
   * if the view is not attached to the UI.
   *
   * @return this view's UIAdapter
   */
  public UIAdapter getUIAdapter() {
    return uiListAdapter;
  }

  /**
   * Returns ID of the attribute used for displaying the row value.
   *
   * @return display value ID
   */
  public QName getDisplayValueID() {
    return displayValueID;
  }

  /**
   * Returns ID of the attribute used for styling the row, e.g. background color.
   *
   * @return row style value ID
   */
  public QName getRowColorID() {
    return rowColorID;
  }

  /**
   * Returns the ID of the column which should be clored. If null, the whole row is colored.
   *
   * @return color column ID
   */
  public QName getColorColumnID() {
    return colorColumnID;
  }

  /**
   * Returns true if this list's selection allows multi select.
   *
   * @return true if multi select allowed
   */
  public boolean isMultiSelect() {
    return listCtrl.getSelection().isMultiSelect();
  }

  /**
   * Called to attach this view's to it's UIAdapter and UI component
   *  @param uiContainer the dialog to look for this view's dialog component
   *
   */
  public void attachUIComponent( UIContainer uiContainer ) {
    if (uiListAdapter == null) {
      uiListAdapter = uiContainer.getUIFactory().createListAdapter( uiContainer, this );
    }
  }

  /**
   * Called to create UIAdapter for matching component in uiContainer.
   * The view must not store any link to the created UIAdapter.
   * This kind of UIAdapters is used for rendering table a tree cells and
   * makes it possible to process events from within such a cell by this view.
   *
   * @param uiContainer the container to look for this view's UI component
   * @return the UIAdapter
   */
  @Override
  public UIAdapter attachUIRenderComponent( UIContainer uiContainer ) {
    return uiContainer.getUIFactory().createListAdapter( uiContainer, this );
  }

  /**
   * Called to detach this view from it's UIAdapter and UI component
   */
  public void detachUIComponent() {
    if (uiListAdapter != null) {
      uiListAdapter.detach();
      uiListAdapter = null;
    }
  }

  /**
   * Sets ID of the attribute used for displaying the row value.
   *
   * @param displayValueID the value ID to use for display
   */
  public void setDisplayValueID( QName displayValueID ) {
    this.displayValueID = displayValueID;
  }

  /**
   * Returns the ID of the view definition to be used for rendering a list row
   * @return view definition ID
   */
  public QName getRowCompID() {
    return rowCompID;
  }

  public void setSelectedRowColor( ComponentColor backgroundColor, ComponentColor fontColor ) {
    this.selectedRowColor = backgroundColor;
    this.selectedRowFontColor = fontColor;
    if (uiListAdapter != null) {
      uiListAdapter.repaint();
    }
  }

  public ComponentColor getSelectedRowColor() {
    return selectedRowColor;
  }

  public ComponentColor getSelectedRowFontColor() {
    return selectedRowFontColor;
  }

  private Object getKey( Object value ) {
    if (value == null) return null;
    else if (value instanceof Model) return ((Model) value).key();
    else return value;
  }

  /**
   * Updates the value at (posX,posY). The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX      horizontal position, starting from 0
   * @param posY      vertical position, starting from 0
   * @param value     the new value
   */
  public synchronized void updateValue( short posX, short posY, Object value ) {
    throw new IllegalArgumentException( "No longer supported by list views" );
  }

  /**
   * Sets item at index selected/deselected
   * @param displayRow  item index
   * @param selected    selected yes/no
   */
  public void setSelected( Model displayRow, boolean selected ) {
    if (uiListAdapter != null) {
      uiListAdapter.setSelected( displayRow, selected );
    }
  }

  /**
   * Returns the count of display rows
   * @return the count of display rows
   */
  public synchronized int getDisplayRowCount() {
    if (uiListAdapter == null) {
      return 0;
    }
    else {
      return uiListAdapter.getDisplayRowCount();
    }
  }

  /**
   * Returns the value to display for cell at valueID of row
   *
   * @param row     the data row to be displayed
   * @param valueID ID of the attribute to be returned
   * @return the value to display for valueID
   */
  public Object getCellValue( Model row, QName valueID ) {
    if (valueID == null) {
      valueID = this.displayValueID;
    }
    return super.getCellValue( row, valueID );
  }

  /**
   * Called by UI adapter whenever its selection changed
   * @param selectedRow the row which has been selected or deselected
   * @param selected    true if row has been selected, false if is has been deselected
   */
  public void onSelectionChanged( Model selectedRow, boolean selected ) {
    if (DEBUG) Logger.info( "ListViewPI  compID=" + getComponentID() + " onSelectionChanged"
                            + ", selected="+selected+", row key=" + getKey(selectedRow) );
    if ((listCtrl != null) && (!listCtrl.isReadOnly())) {
      listCtrl.onSelectionChanged( selectedRow, selected );
    }
  }

  public void onNextSelected() {
    if (listCtrl != null) {
      listCtrl.onNextSelected();
    }
  }

  public void onPrevSelected() {
    if (listCtrl != null) {
      listCtrl.onPrevSelected();
    }
  }

  /**
   * An event occurred.
   * @param eventSender the sender of this event
   * @param eventID the id of the event
   * @param source the instance where the event occurred on, may be a child of eventSender
   * @param modelID the id of the affected model
   * @param oldValue the old value of the model
   * @param newValue the new value of the model
   */
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {
    if (uiListAdapter != null) {
      uiListAdapter.updateRow( (Model) eventSender.getOwner() );
    }
  }

  private void removeAllListeners() {
    if (uiListAdapter != null) {
      for (int i = 0; i < uiListAdapter.getDisplayRowCount(); i++) {
        Model displayRow = uiListAdapter.getDisplayRow( i );
        if (displayRow != null) {
          displayRow.removeListener( this );
        }
      }
    }
  }

  /**
   * This method is called when this view's controller is closing,
   * i.e. controller's dialog is closing.
   * The view has to calls detachUIComponent() and release all its
   * resources, e.g. remove itself as listener.
   */
  public synchronized void closing() {
    removeAllListeners();
    super.closing();
  }

  /**
   * Called by list controller to remove all rows from view
   * Does nothing if UI is not attached.
   */
  public void removeAllDisplayRows() {
    if (uiListAdapter != null) {
      removeAllListeners();
      uiListAdapter.removeAllDisplayRows();
    }
  }

  /**
   * Called by list controller to insert newRow into displayed list before beforeRow.
   * Does nothing if UI is not attached.
   *
   * @param newRow    the row to be inserted
   * @param beforeRow the row before which newRow is inserted
   */
  public void insertRow( Model newRow, Model beforeRow ) {
    if (uiListAdapter != null) {
      uiListAdapter.insertRow( newRow, beforeRow );
      if (newRow != null) {
        newRow.addListener( this );
      }
    }
  }

  /**
   * Called by list controller to remove row from display list
   * Does nothing if UI is not attached.
   *
   * @param removedRow the row to remove
   * @return true if display row list contained row
   */
  public void removeRow( Model removedRow ) {
    removedRow.removeListener( this );
    if (uiListAdapter != null) {
      uiListAdapter.removeRow( removedRow );
    }
  }

  /**
   * Called by list controller to update changedRow in display list.
   * Does nothing if UI is not attached.
   *
   * @param changedRow the row to be updated in display list
   */
  public void updateRow( Model changedRow ) {
    if (uiListAdapter != null) {
      uiListAdapter.updateRow( changedRow );
    }
  }
}
