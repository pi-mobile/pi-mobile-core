/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.ModuleController;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIFragmentAdapter;
import de.pidata.qnames.QName;

public class ModuleViewPI extends AbstractViewPI {

  private UIFragmentAdapter uiFragmentAdapter;
  private ModuleController moduleController;

  public ModuleViewPI( QName componentID, QName moduleID ) {
    super( componentID, moduleID );
  }

  @Override
  public Controller getController() {
    return moduleController;
  }

  public void setController( ModuleController moduleGroup ) {
    this.moduleController = moduleGroup;
  }

  /**
   * Returns the UIAdapter connected to this view. The UI adapter is null
   * if the view is not attached to the UI.
   *
   * @return this view's UIAdapter
   */
  @Override
  public UIAdapter getUIAdapter() {
    return uiFragmentAdapter;
  }

  /**
   * Returns the UIContainer connected to this view, which is this ModuleView's
   * UI Adapter. The UI container is null if the view is not attached to the UI.
   *
   * @return this view's UIContainer
   */
  public UIContainer getUIContainer() {
    return uiFragmentAdapter;
  }

  /**
   * Called to attach this view's to it's UIAdapter and UI component
   * In case of a child view within a complex parent view
   * (e.g. Table or Tree) a dataContext may be necessary to identify
   * the UI component.
   *  @param uiContainer the container to look for this view's UI component
   *
   */
  @Override
  public synchronized void attachUIComponent( UIContainer uiContainer ) {
    if (uiFragmentAdapter == null) {
      uiFragmentAdapter = uiContainer.getUIFactory().createFragmentAdapter( uiContainer, this );
      ModuleGroup moduleGroup = moduleController.getModuleGroup();
      uiFragmentAdapter.moduleChanged( moduleGroup );
    }
  }

  /**
   * Called to create UIAdapter for matching component in uiContainer.
   * The view must not store any link to the created UIAdapter.
   * This kind of UIAdapters is used for rendering table a tree cells and
   * makes it possible to process events from within such a cell by this view.
   *
   * @param uiContainer the container to look for this view's UI component
   * @return the UIAdapter
   */
  @Override
  public UIAdapter attachUIRenderComponent( UIContainer uiContainer ) {
    ModuleGroup module = moduleController.getCurrentModule();
    for (Controller childCtrl : module.controllerIterator()) {
      ViewPI viewPI = childCtrl.getView();
      viewPI.attachUIRenderComponent( uiContainer );
    }
    return null;
  }

  /**
   * Used in case of Popups to avoid Fragment transaction.
   * 
   * @param uiFragmentAdapter
   */
  public void setFragmentAdapter( UIFragmentAdapter uiFragmentAdapter ) {
    this.uiFragmentAdapter = uiFragmentAdapter;
  }

  /**
   * Called to detach this view from it's UIAdapter and UI component
   */
  @Override
  public synchronized void detachUIComponent() {
    if (uiFragmentAdapter != null) {
      uiFragmentAdapter.detach();
      uiFragmentAdapter = null;
    }
  }

  public synchronized void moduleChanged( ModuleGroup moduleGroup ) {
    if (uiFragmentAdapter != null) {
      uiFragmentAdapter.moduleChanged( moduleGroup );
    }
  }

  /**
   * Called by UIAdapter after having finished loading fragment.
   * Now we can attach UI
   */
  public void onFragmentLoaded( ModuleGroup moduleGroup ) {
    moduleGroup.activateModule( getUIContainer() );
  }

  public void onFragmentDestroyed( ModuleGroup moduleGroup ) {
    moduleGroup.deactivateModule();
    moduleController.moduleDeactivated( moduleGroup );
  }
}
