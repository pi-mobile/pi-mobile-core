/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.guidef.Node;
import de.pidata.gui.guidef.NodeType;
import de.pidata.models.tree.EventListener;
import de.pidata.models.tree.EventSender;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

import java.util.ArrayList;

/**
 * Created by pru on 14.07.16.
 */
public class TreeNodePI implements EventListener {

  private TreeViewPI treeViewPI;
  private Model nodeModel;
  private TreeNodePI parentNode;
  private QName displayValueID;
  private QName iconID;
  private QName editFlagID;
  private QName enabledFlagID;
  private QName childRelationID;
  private QName cellViewDefID;
  private ArrayList<TreeNodePI> childNodeList = new ArrayList<TreeNodePI>();
  private boolean childrenLoaded = false;

  public TreeNodePI( TreeViewPI treeViewPI, TreeNodePI parentNode, Model nodeModel, QName displayValueID, QName iconID, QName editFlagID, QName enabledFlagID, QName childRelationID ) {
    this.treeViewPI = treeViewPI;
    this.nodeModel = nodeModel;
    this.parentNode = parentNode;
    this.displayValueID = displayValueID;
    this.iconID = iconID;
    this.editFlagID = editFlagID;
    this.enabledFlagID = enabledFlagID;
    this.childRelationID = childRelationID;
    if (this.nodeModel != null) {
      this.nodeModel.addListener( this );
    }
  }

  public TreeViewPI getTreeViewPI() {
    return treeViewPI;
  }

  public Model getNodeModel() {
    return nodeModel;
  }

  public void setNodeModel( Model nodeModel, QName displayValueID, QName iconID, QName editFlagID, QName enabledFlagID, QName childRelationID ) {
    if (this.nodeModel != null) {
      this.nodeModel.removeListener( this );
    }
    this.nodeModel = nodeModel;
    this.displayValueID = displayValueID;
    this.iconID = iconID;
    this.editFlagID = editFlagID;
    this.enabledFlagID = enabledFlagID;
    this.childRelationID = childRelationID;
    if (this.nodeModel != null) {
      this.nodeModel.addListener( this );
    }
  }

  public TreeNodePI getParentNode() {
    return parentNode;
  }

  public QName getDisplayValueID() {
    return displayValueID;
  }

  public QName getEditFlagID() {
    return editFlagID;
  }

  public QName getEnabledFlagID() {
    return enabledFlagID;
  }

  public QName getCellViewDefID() {
    return cellViewDefID;
  }

  public QName getChildRelationID() {
    return childRelationID;
  }

  public QName getIconID() {
    return iconID;
  }

  @Override
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {
    if (source == this.nodeModel) {
      treeViewPI.updateNode( this );
    }
  }

  /**
   * Use this as initial request of loading children from lazy loading.
   */
  public void loadChildren() {
    if (childrenLoaded) {
      return;
    }
    synchronized (childNodeList) {
      childrenLoaded = true;
      int i = 0;
      for (Model childModel : nodeModel.iterator( getChildRelationID(), null )) {
        NodeType nodeTypeDef = treeViewPI.getTreeCtrl().getNodeType( childModel.type().name() );
        // don't call setChildModelAt() here, avoid events
        loadChildAt( i, childModel, nodeTypeDef.getValueID(), nodeTypeDef.getIconValueID(), nodeTypeDef.getEditFlagID(), nodeTypeDef.getEnabledFlagID(), nodeTypeDef.getRelationID() );
        i++;
      }
    }
    treeViewPI.updateChildList( this );
  }

  public boolean refreshChildList() {
    if (!childrenLoaded) {
      return false;
    }
    synchronized (childNodeList) {
      boolean changed = false;
      int i = 0;
      for (Model childModel : nodeModel.iterator( getChildRelationID(), null )) {
        TreeNodePI childNode = findNode( childModel );
        if (childNode == null) {
          NodeType nodeTypeDef = treeViewPI.getTreeCtrl().getNodeType( childModel.type().name() );
          // don't call setChildModelAt() here, avoid events
          loadChildAt( i, childModel, nodeTypeDef.getValueID(), nodeTypeDef.getIconValueID(), nodeTypeDef.getEditFlagID(), nodeTypeDef.getEnabledFlagID(), nodeTypeDef.getRelationID() );
          changed = true;
        }
        else {
          int oldIndex = childNodeList.indexOf( childNode );
          if (oldIndex > i) {
            childNodeList.set( oldIndex, childNodeList.get( i ) );
            childNodeList.set( i, childNode );
            changed = true;
          }
          else if (oldIndex < i) {
            throw new IllegalArgumentException( "Internal error: duplicate node" );
          }
        }
        i++;
      }
      return changed;
    }
  }

  public Iterable<TreeNodePI> childNodeIter() {
    // use a copy of the current list as lazy loading will modify original
    return new ArrayList<TreeNodePI>( childNodeList );
  }

  public TreeNodePI setChildModelAt( int index, Model childModel, QName displayValueID, QName iconID, QName editFlagID, QName enabledFlagID, QName childRelationID ) {
    synchronized (childNodeList) {
      childrenLoaded = true;
      TreeNodePI childNode = loadChildAt( index, childModel, displayValueID, iconID, editFlagID, enabledFlagID, childRelationID );
      treeViewPI.updateChildList( this );
      return childNode;
    }
  }

  private TreeNodePI loadChildAt( int index, Model childModel, QName displayValueID, QName iconID, QName editFlagID, QName enabledFlagID, QName childRelationID ) {
    while (index >= childNodeList.size()) {
      childNodeList.add( null );
    }
    TreeNodePI childNode = childNodeList.get( index );
    if (childNode == null) {
      childNode = new TreeNodePI( treeViewPI, this, childModel, displayValueID, iconID, editFlagID, enabledFlagID, childRelationID );
      childNodeList.set( index, childNode );
    }
    else {
      childNode.setNodeModel( childModel, displayValueID, iconID, editFlagID, enabledFlagID, childRelationID );
    }
    return childNode;
  }

  public TreeNodePI findNode( Model nodeModel ) {
    if (nodeModel == null) {
      return null;
    }
    if (getNodeModel() == nodeModel) {
      return this;
    }
    synchronized (childNodeList) {
      for (TreeNodePI childNode : childNodeList) {
        TreeNodePI foundNode = childNode.findNode( nodeModel );
        if (foundNode != null) {
          return foundNode;        }

      }
      return null;
    }
  }

  public void removeChild( TreeNodePI treeNodePI ) {
    childNodeList.remove( treeNodePI );
    treeViewPI.updateChildList( this );
  }

  public boolean isLeaf() {
    return !(nodeModel.iterator( getChildRelationID(), null ).hasNext());
  }

  public TreeNodePI getFirstChild() {
    loadChildren();
    if (childNodeList.size() > 0) {
      return childNodeList.get( 0 );
    }
    return null;
  }

  @Override
  public String toString() {
    if (nodeModel != null) {
      return nodeModel.toString();
    }
    return super.toString();
  }
}
