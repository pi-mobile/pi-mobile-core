/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.controller.base.ColumnInfo;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.renderer.Renderer;
import de.pidata.gui.renderer.StringRenderer;
import de.pidata.models.tree.Model;

/**
 * This is a Helper class for table controls. It combines row model and column info and provides the string
 * value rendered by ColumInfo.getRenderer( rowModel ).
 *
 * In order to support table filtering two TableCellPI have to be equal if thir renderString is equal. To enable
 * this behavior toString() returns renderString() and hashCode() and equals() only look at the render String.
 */
public class TableCellPI {

  private Model model;
  private ColumnInfo columnInfo;

  public TableCellPI( Model model, ColumnInfo columnInfo ) {
    this.model = model;
    this.columnInfo = columnInfo;
  }

  public Model getModel() {
    return model;
  }

  public ColumnInfo getColumnInfo() {
    return columnInfo;
  }

  /**
   * Returns cell renderer for columnInfo
   * @return the cell renderer, default is StringRenderer
   * @param cellValue
   */
  public String render( Object cellValue ) {
    Renderer renderer;
    if (columnInfo == null) {
      return StringRenderer.getDefault().render( cellValue );
    }
    else {
      Controller renderCtrl = columnInfo.getRenderCtrl();
      if (renderCtrl == null) {
        return StringRenderer.getDefault().render( cellValue );
      }
      else {
        return renderCtrl.render( cellValue );
      }
    }
  }

  /**
   * String representation of this cell, i.e. String returnded by calling this cell's ColumnInfo.getRenderer()
   * with this cell's model.
   *
   * @return the render String
   */
  public String getRenderString() {
    String renderValue = render( columnInfo.getCellValue( model ) );
    if (renderValue == null) {
      return "";
    }
    else {
      return renderValue;
    }
  }

  /**
   * Return the hash code of this TableCellPI's renderString()
   *
   * @return the hash code
   */
  @Override
  public int hashCode() {
    return getRenderString().hashCode();
  }

  /**
   * Two TabelCells are equal if their renderString() is equal
   *
   * @param obj the object to compare with this TableCellPI
   * @return true if renderString() is equal
   */
  @Override
  public boolean equals( Object obj ) {
    if (obj == null) {
      return false;
    }
    else if (obj instanceof TableCellPI) {
      return getRenderString().equals( obj.toString() );
    }
    else {
      return false;
    }
  }

  /**
   * Returns renderString()
   * 
   * @return renderString()
   */
  @Override
  public String toString() {
    return getRenderString();
  }
}
