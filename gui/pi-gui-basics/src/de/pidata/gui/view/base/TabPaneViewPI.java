/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.TabPaneController;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITabPaneAdapter;
import de.pidata.qnames.QName;

import java.util.List;

public class TabPaneViewPI extends AbstractViewPI {

  private UITabPaneAdapter uiTabPaneAdapter;
  private TabPaneController tabPaneController;

  public TabPaneViewPI( QName componentID, QName moduleID ) {
    super( componentID, moduleID );
  }

  public Controller getController() {
    return tabPaneController;
  }

  public void setController( TabPaneController buttonController ) {
    this.tabPaneController = buttonController;
  }

  public UIAdapter getUIAdapter() {
    return uiTabPaneAdapter;
  }

  /**
   * Called to attach this view's to it's UIAdapter and UI component
   *  @param uiContainer the container to look for this view's UI component
   *
   */
  public void attachUIComponent( UIContainer uiContainer ) {
    if (uiTabPaneAdapter == null) {
      uiTabPaneAdapter = uiContainer.getUIFactory().createTabPaneAdapter( uiContainer, this );
    }
  }

  /**
   * Called to create UIAdapter for matching component in uiContainer.
   * The view must not store any link to the created UIAdapter.
   * This kind of UIAdapters is used for rendering table a tree cells and
   * makes it possible to process events from within such a cell by this view.
   *
   * @param uiContainer the container to look for this view's UI component
   * @return the UIAdapter
   */
  @Override
  public UIAdapter attachUIRenderComponent( UIContainer uiContainer ) {
    return uiContainer.getUIFactory().createTabPaneAdapter( uiContainer, this );
  }

  public void detachUIComponent() {
    if (uiTabPaneAdapter != null) {
      uiTabPaneAdapter.detach();
      uiTabPaneAdapter = null;
    }
  }


  public String getSelectedTab() {
    if (uiTabPaneAdapter != null) {
      return uiTabPaneAdapter.getSelectedTab();
    }
    return null;
  }

  public void setSelectedTab( String tabId ) {
    if (uiTabPaneAdapter != null) {
      uiTabPaneAdapter.setSelectedTab(tabId);
    }
  }

  public void setSelectedTab( int index ) {
    if (uiTabPaneAdapter != null) {
      uiTabPaneAdapter.setSelectedTab(index);
    }
  }

  public void setTabDisabled( String tabId, boolean disabled ) {
    if (uiTabPaneAdapter != null) {
      uiTabPaneAdapter.setTabDisabled( tabId, disabled );
    }
  }

  public List<String> getTabs() {
    if (uiTabPaneAdapter != null) {
      return uiTabPaneAdapter.getTabs();
    }
    return null;
  }

  public void tabChanged( String id ) {
    tabPaneController.tabChanged(id);
  }

  public void setTabText( String tabId, String text ) {
    if (uiTabPaneAdapter != null) {
      uiTabPaneAdapter.setTabText( tabId, text );
    }
  }
}
