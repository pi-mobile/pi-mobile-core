/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITextAdapter;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

public class HTMLEditorViewPI extends TextViewPI {

  public HTMLEditorViewPI( QName componentID, QName moduleID ) {
    super( componentID, moduleID );
  }

  @Override
  protected UITextAdapter createTextAdapter( UIContainer uiContainer ) {
    return uiContainer.getUIFactory().createHTMLEditorAdapter( uiContainer, this );
  }
}
