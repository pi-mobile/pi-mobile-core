/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.controller.base.SearchListener;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UICodeEditorAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITextAdapter;
import de.pidata.parser.Parser;
import de.pidata.qnames.QName;

import java.util.List;
import java.util.Map;

/**
 * @author dsc
 */
public class CodeEditorViewPI extends TextViewPI {

  public CodeEditorViewPI( QName componentID, QName moduleID ) {
    super( componentID, moduleID );
  }

  @Override
  protected UITextAdapter createTextAdapter( UIContainer uiContainer ) {
    return uiContainer.getUIFactory().createCodeEditorAdapter( uiContainer, this );
  }

  public void showLinenumbers( boolean show ) {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null && uiAdapter instanceof UICodeEditorAdapter) {
      ((UICodeEditorAdapter)uiAdapter).showLinenumbers( show );
    }
  }

  public void addLineMarker( String marker, List<Integer> markLinesList, ComponentColor markerColor ) {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null && uiAdapter instanceof UICodeEditorAdapter) {
      ((UICodeEditorAdapter)uiAdapter).addLineMarker( marker, markLinesList, markerColor );
    }
  }

  public void setAutoindent( boolean autoindent ) {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null && uiAdapter instanceof UICodeEditorAdapter) {
      ((UICodeEditorAdapter)uiAdapter).setAutoindent( autoindent );
    }
  }

  /**
   * Sets the parser to use for syntax highlighting.
   *
   * Note: If a Parser exists pattern map is ignored.
   *
   * @param parser the parser to use or null toi remove the parser
   */
  public void setCodeParser( Parser parser ) {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null && uiAdapter instanceof UICodeEditorAdapter) {
      ((UICodeEditorAdapter)uiAdapter).setCodeParser( parser );
    }
  }

  /**
   * Sets the pattern map to use for syntax highlighting
   *
   * Note: If a Parser exists pattern map is ignored.
   *
   * @param patternMap the pattern map to use for highlighting
   */
  public void setCodePatternMap( Map<String, String> patternMap ) {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null && uiAdapter instanceof UICodeEditorAdapter) {
      ((UICodeEditorAdapter)uiAdapter).setCodePatternMap( patternMap );
    }
  }

  public void searchAll( String searchStr ) {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null && uiAdapter instanceof UICodeEditorAdapter) {
      ((UICodeEditorAdapter)uiAdapter).searchAll( searchStr );
    }
  }

  public void searchNext( String searchStr ) {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null && uiAdapter instanceof UICodeEditorAdapter) {
      ((UICodeEditorAdapter)uiAdapter).searchNext( searchStr );
    }
  }

  public void searchPrev( String searchStr ) {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null && uiAdapter instanceof UICodeEditorAdapter) {
      ((UICodeEditorAdapter)uiAdapter).searchPrev( searchStr );
    }
  }

  public void refreshLineMarkers() {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null && uiAdapter instanceof UICodeEditorAdapter) {
      ((UICodeEditorAdapter)uiAdapter).refreshLineMarkers();
    }
  }

  public void setSearchRegex( boolean searchRegex ) {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null && uiAdapter instanceof UICodeEditorAdapter) {
      ((UICodeEditorAdapter)uiAdapter).setSearchRegex( searchRegex );
    }
  }

  public void initListeners() {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null && uiAdapter instanceof UICodeEditorAdapter) {
      ((UICodeEditorAdapter)uiAdapter).initListeners();
    }
  }

  public void addSearchListener( SearchListener searchListener ) {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null && uiAdapter instanceof UICodeEditorAdapter) {
      ((UICodeEditorAdapter)uiAdapter).addSearchListener( searchListener );
    }
  }

  public void replaceNext( String replaceStr ) {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null && uiAdapter instanceof UICodeEditorAdapter) {
      ((UICodeEditorAdapter)uiAdapter).replaceNext( replaceStr );
    }
  }

  public void replaceAll( String replaceStr ) {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null && uiAdapter instanceof UICodeEditorAdapter) {
      ((UICodeEditorAdapter)uiAdapter).replaceAll( replaceStr );
    }
  }
}
