/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.guidef.InputMode;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.models.tree.Model;
import de.pidata.qnames.Key;
import de.pidata.qnames.QName;

public abstract class AbstractViewPI implements ViewPI {

  private QName componentID;
  private QName moduleID;
  private InputMode inputMode;
  private Object format;

  protected AbstractViewPI( QName componentID, QName moduleID ) {
    this.componentID = componentID;
    this.moduleID = moduleID;
  }

  public QName getComponentID() {
    return componentID;
  }

  /**
   *
   * @return
   */
  public QName getModuleID() {
    return moduleID;
  }

  public void setInputMode( InputMode inputMode ) {
    this.inputMode = inputMode;
  }

  public InputMode getInputMode() {
    return inputMode;
  }

  public void setFormat( Object format ) {
    this.format = format;
  }

  public Object getFormat() {
    return format;
  }

  public void setVisible( boolean visible ) {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null) {
      uiAdapter.setVisible( visible );
    }
  }

  public boolean isVisible() {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null) {
      return uiAdapter.isVisible();
    }
    return false;
  }

   public boolean isFocused() {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null) {
      return uiAdapter.isFocused();
    }
    return false;
  }

  public void setEnabled( boolean enabled ) {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null) {
      uiAdapter.setEnabled( enabled );
    }
  }

  public boolean isEnabled() {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null) {
      return uiAdapter.isEnabled();
    }
    return false;
  }

  /**
   * Set this view's background color
   *
   * @param color new background color
   */
  @Override
  public void setBackground( ComponentColor color ) {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null) {
      uiAdapter.setBackground( color );
    }
  }

  /**
   * Set this view's background to its initial background
   */
  @Override
  public void resetBackground() {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null) {
      uiAdapter.resetBackground();
    }
  }

  public void onFocusChanged( UIAdapter uiAdapter, boolean hasFocus, Model dataContext ) {
    // We must not process focus events from UI renderer:
    if (uiAdapter == getUIAdapter()) {
      Controller ctrl = getController();
      if (ctrl != null) {
        ctrl.onFocusChanged( getComponentID(), hasFocus );
      }
    }
  }

  /**
   * Toggles focus, selected, enabled and visible state of this component at position (posX, posY) or for
   * entry identified by key. It depends on the type of component what is meant by posX, posY and key.
   * Many components will just ignore posX and posY values or key.
   * The default implementaion calls the componentListener.
   *
   * @param posX     the x position of the source event
   * @param posY     the y position of the source event
   * @param focus    set focus on/off
   * @param selected set selected if true, deselected if false, do not change if null
   * @param enabled  set enabled if true, disabled if false, do not change if null
   * @param visible  set visible if true, invisible if false, do not change if null
   */
  public void setState( short posX, short posY, Boolean focus, Boolean selected, Boolean enabled, Boolean visible ) {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null) {
      uiAdapter.setState( posX, posY, focus, selected, enabled, visible );
    }
  }

  public void repaint() {
    UIAdapter uiAdapter = getUIAdapter();
    if (uiAdapter != null) {
      uiAdapter.repaint();
    }
  }

  /**
   * Sets the info text for given info type. It depends on component's implementation
   * how that info text is presented
   *
   * @param infoType the type of info, one of the constants INFO_*
   * @param infoText the info text
   */
  public void setInfoText( int infoType, String infoText ) {
    //TODO implement in subclasses
  }

  /**
   * Returns the value to display for cell at valueID of row
   *
   * @param row     the data row to be displayed
   * @param valueID ID of the attribute to be returned
   * @return the value to display for valueID
   */
  public Object getCellValue( Model row, QName valueID ) {
    Object value;
    if (row == null) {
      value = null;
    }
    else if (valueID == null) {
      Key key = row.key();
      if (key == null) value = null;
      else {
        value = key.getKeyValue( 0 );
        if (value instanceof QName) {
          value = ((QName) value).getName();
        }
      }
    }
    else {
      value = row.get( valueID );
    }
    return value;
  }

  public boolean processCommand( QName cmd, char inputChar, int index ) {
    UIAdapter view = getUIAdapter();
    if (view != null) {
      return  view.processCommand( cmd, inputChar, index );
    }
    return false;  //To change body of implemented methods use File | Settings | File Templates.
  }

  public void performOnClick() {
    UIAdapter view = getUIAdapter();
    if (view != null) {
      view.performOnClick();
    }
  }

  @Override
  public void showContextMenu( double mouseDownX, double mouseDownY ) {
    getController().showContextMenu( mouseDownX, mouseDownY );
  }

  /**
   * This method is called when this view's controller is closing,
   * i.e. controller's dialog is closing.
   * The view has to calls detachUIComponent() and release all its
   * resources, e.g. remove itself as listener.
   */
  public void closing() {
    detachUIComponent();
  }
}
