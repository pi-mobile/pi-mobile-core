package de.pidata.gui.view.base;

import de.pidata.gui.controller.base.ColumnInfo;

public interface ColumnView {

  void onColumnChanged( ColumnInfo columnInfo );

}
