/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.ButtonController;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UIButtonAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

public class ButtonViewPI extends AbstractViewPI {

  private UIButtonAdapter uiButtonAdapter;
  private ButtonController buttonController;

  public ButtonViewPI( QName componentID, QName moduleID ) {
    super( componentID, moduleID );
  }

  public Controller getController() {
    return buttonController;
  }

  public void setController( ButtonController buttonController ) {
    this.buttonController = buttonController;
  }

  public UIAdapter getUIAdapter() {
    return uiButtonAdapter;
  }

  /**
   * Called to attach this view's to it's UIAdapter and UI component
   *  @param uiContainer the container to look for this view's UI component
   *
   */
  public void attachUIComponent( UIContainer uiContainer ) {
    if (uiButtonAdapter == null) {
      uiButtonAdapter = uiContainer.getUIFactory().createButtonAdapter( uiContainer, this );
    }
  }

  /**
   * Called to create UIAdapter for matching component in uiContainer
   * 
   * @param uiContainer  the container to look for this view's UI component
   * @return the UIAdapter
   */
  @Override
  public UIAdapter attachUIRenderComponent( UIContainer uiContainer ) {
    return uiContainer.getUIFactory().createButtonAdapter( uiContainer, this );
  }

  public void detachUIComponent() {
    if (uiButtonAdapter != null) {
      uiButtonAdapter.detach();
      uiButtonAdapter = null;
    }
  }

  public void updateValue( Object value ) {
    if (uiButtonAdapter != null) {
      if (value instanceof QName) {
        updateIcon( value );
      }
      else {
        if (value == null) {
          uiButtonAdapter.setLabel( "" );
        }
        else {
          uiButtonAdapter.setLabel( value.toString() );
        }
      }
    }
  }

  public void updateText( Object value ) {
    if (uiButtonAdapter != null){
      uiButtonAdapter.setText( value );
    }
  }

  public void updateIcon( Object value ) {
    if(uiButtonAdapter != null){
      uiButtonAdapter.setGraphic( value );
    }
  }

  public String getText() {
    if (uiButtonAdapter != null) {
      return uiButtonAdapter.getText();
    }
    return null;
  }

  public void onClick( UIButtonAdapter source, Model dataContext ) {
    if (buttonController != null) {
      buttonController.press( dataContext );
    }
  }
}
