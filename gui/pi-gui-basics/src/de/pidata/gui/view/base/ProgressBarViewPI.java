/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.ProgressBarController;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIProgressBarAdapter;
import de.pidata.models.tree.Model;
import de.pidata.models.types.simple.DecimalObject;
import de.pidata.qnames.QName;

/**
 * <add description here>
 *
 * @author Doris Schwarzmaier
 */
public class ProgressBarViewPI extends AbstractViewPI {

  private UIProgressBarAdapter uiProgressBarAdapter;
  private ProgressBarController progressBarController;

  public ProgressBarViewPI( QName componentID, QName moduleID ) {
    super( componentID, moduleID );
  }

  public Controller getController() {
    return progressBarController;
  }

  public void setController( ProgressBarController progressBarController ) {
    this.progressBarController = progressBarController;
  }

  public UIAdapter getUIAdapter() {
    return uiProgressBarAdapter;
  }

  /**
   * Called to attach this view's to it's UIAdapter and UI component
   *  @param uiContainer the dialog to look for this view's dialog component
   *
   */
  public void attachUIComponent( UIContainer uiContainer ) {
    if (uiProgressBarAdapter == null) {
      uiProgressBarAdapter = uiContainer.getUIFactory().createProgressAdapter( uiContainer, this );
      if( progressBarController.hasError() ) {
        uiProgressBarAdapter.showError( progressBarController.getErrorMessage() );
      }
    }
  }

  /**
   * Called to create UIAdapter for matching component in uiContainer.
   * The view must not store any link to the created UIAdapter.
   * This kind of UIAdapters is used for rendering table a tree cells and
   * makes it possible to process events from within such a cell by this view.
   *
   * @param uiContainer the container to look for this view's UI component
   * @return the UIAdapter
   */
  @Override
  public UIAdapter attachUIRenderComponent( UIContainer uiContainer ) {
    return uiContainer.getUIFactory().createProgressAdapter( uiContainer, this );
  }

  public void detachUIComponent() {
    if (uiProgressBarAdapter != null) {
      uiProgressBarAdapter.detach();
      uiProgressBarAdapter = null;
    }
  }

  public Object getValue() {
    if (uiProgressBarAdapter != null) {
      return uiProgressBarAdapter.getValue();
    }
    return DecimalObject.ZERO;
  }

  public void updateValue( Object value ) {
    if (uiProgressBarAdapter != null) {
      if (value == null) {
        uiProgressBarAdapter.setValue( DecimalObject.ZERO );
      }
      else {
        uiProgressBarAdapter.setValue( value );
      }
    }
  }

  public void setMaxValue( int maxValue ) {
    if (uiProgressBarAdapter != null) {
      uiProgressBarAdapter.setMaxValue( maxValue );
    }
  }

  public void setMinValue( int minValue ) {
    if (uiProgressBarAdapter != null) {
      uiProgressBarAdapter.setMinValue( minValue );
    }
  }

  public void setProgress( double progress ) {
    if (uiProgressBarAdapter != null) {
      uiProgressBarAdapter.setProgress( progress );
    }
  }

  public void setProgress( double progress, String message ) {
    if (uiProgressBarAdapter != null) {
      uiProgressBarAdapter.setProgress( progress );
      uiProgressBarAdapter.setProgressMessage( message );
    }
  }

  public void showError( String errorMessage ) {
    if (uiProgressBarAdapter != null) {
      uiProgressBarAdapter.showError( errorMessage );
    }
  }

  public void hideError() {
    if (uiProgressBarAdapter != null) {
      uiProgressBarAdapter.hideError(  );
    }
  }

  public void showInfo( String infoMessage ) {
    if (uiProgressBarAdapter != null) {
      uiProgressBarAdapter.showInfo( infoMessage );
    }
  }

  public void showWarning (String warningMessage){
    if(uiProgressBarAdapter != null){
      uiProgressBarAdapter.showWarning (warningMessage);
    }
  }

  public void resetColor (){
    if(uiProgressBarAdapter != null){
      uiProgressBarAdapter.resetColor ();
    }
  }

  public void onValueChanged( UIProgressBarAdapter source, Number value, Model dataContext ) {
    if (uiProgressBarAdapter != null) { // process only if activated
      if (source == uiProgressBarAdapter) {
        dataContext = null;
      }
      progressBarController.storeValue( value, dataContext );
    }
  }
}
