/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.guidef.InputMode;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

public interface ViewPI {

  int INFO_TOOLTIP = 1;
  int INFO_ERROR = 2;

  QName getComponentID();

  /**
   * Returns the module ID which shall be used for this view. Implementations must decide how to handle this.
   * For example, a separate UI defining file with a name corresponding to the given module ID might be loaded
   * to find additional UI components by ID.
   *
   * @return the module ID given for this view
   * @deprecated used by selection/table/tree only; use {@link ListViewPI#getRowCompID()}/{@link TreeViewPI#getRowCompID()} instead
   */
  @Deprecated
  QName getModuleID();

  /**
   * Returns this view's controller
   *
   * @return this view's controller
   */
  Controller getController();

  /**
   * Returns the UIAdapter connected to this view. The UI adapter is null
   * if the view is not attached to the UI.
   *
   * @return this view's UIAdapter
   */
  UIAdapter getUIAdapter();

  /**
   * Returns this View's current input mode
   * @return this View's current input mode
   */
  InputMode getInputMode();

  /**
   * Sets this View's current input mode
   * @param inputMode this View's new input mode
   */
  void setInputMode( InputMode inputMode );

  /**
   * Returns this View's current input format
   * @return this View's current input format
   */
  Object getFormat();

  /**
   * Sets this View's current input format
   * @param format this View's new input format
   */
  void setFormat( Object format );

  /**
   * Toggles focus, selected, enabled and visible state of this component at position (posX, posY) or for
   * entry identified by key. It depends on the type of component what is meant by posX, posY and key.
   * Many components will just ignore posX and posY values or key.
   * The default implementaion calls the componentListener.
   * @param posX      the x position of the source event
   * @param posY      the y position of the source event
   * @param focus     set focus on/off
   * @param selected  set selected if true, deselected if false, do not change if null
   * @param enabled   set enabled if true, disabled if false, do not change if null
   * @param visible   set visible if true, invisible if false, do not change if null
   */
  void setState( short posX, short posY, Boolean focus, Boolean selected, Boolean enabled, Boolean visible );

  void setEnabled( boolean enabled );

  boolean isEnabled();

  void setVisible( boolean visible );

  boolean isVisible();

  boolean isFocused();

  /**
   * Set this view's background color
   * @param color new background color
   */
  void setBackground( ComponentColor color );

  /**
   * Set this view's background to its initial background
   */
  void resetBackground();

  void repaint();

  /**
   * Sets the info text for given info type. It depends on component's implementation
   * how that info text is presented
   *
   * @param infoType the type of info, one of the constants INFO_*
   * @param infoText the info text
   */
  void setInfoText( int infoType, String infoText );

  /**
   * Returns the value to display for cell at valueID of row
   *
   * @param row     the data row to be displayed
   * @param valueID ID of the attribute to be returned
   * @return the value to display for valueID
   */
  Object getCellValue( Model row, QName valueID );

  /**
   * Called by view whenever it gains or looses input focus.
   * @param uiAdapter
   * @param hasFocus true if view gained input focus @return if false focus switch is not allowed by ComponentListener
   * @param dataContext calling UIAdapter's dataContext
   */
  void onFocusChanged( UIAdapter uiAdapter, boolean hasFocus, Model dataContext );  // TODO ist das Aufgabe vom Controller oder vom View ?

  /**
   * Called to attach this view's to it's UIAdapter and UI component
   * In case of a child view within a complex parent view
   * (e.g. Table or Tree) a dataContext may be necessary to identify
   * the UI component.
   *
   * @param uiContainer the container to look for this view's UI component
   */
  void attachUIComponent( UIContainer uiContainer );

  /**
   * Called to create UIAdapter for matching component in uiContainer.
   * The view must not store any link to the created UIAdapter.
   * This kind of UIAdapters is used for rendering table a tree cells and
   * makes it possible to process events from within such a cell by this view.
   *
   * @param uiContainer  the container to look for this view's UI component
   * @return the UIAdapter
   */
  UIAdapter attachUIRenderComponent( UIContainer uiContainer );

  /**
   * Called to detach this view from it's UIAdapter and UI component
   */
  void detachUIComponent();

  /**
   * This method is called when this view's controller is closing,
   * i.e. controller's dialog is closing.
   * The view has to calls detachUIComponent() and release all its
   * resources, e.g. remove itself as listener.
   */
  void closing();

  boolean processCommand( QName cmd, char inputChar, int index );

  /**
   * send onClick request from Controller -> ViewPI -> UIAdapter -> View
   */
  void performOnClick();

  void showContextMenu( double mouseDownX, double mouseDownY );
}
