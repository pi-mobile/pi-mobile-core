/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.TreeController;
import de.pidata.gui.renderer.StringRenderer;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITreeAdapter;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

import java.util.HashMap;

/**
 * Created by pru on 12.07.2016.
 */
public class TreeViewPI extends AbstractViewPI {

  protected TreeController treeController;
  protected TreeNodePI rootNode = new TreeNodePI( this, null, null, null, null, null, null, null );
  protected UITreeAdapter uiTreeAdapter;
  protected HashMap<QName,Controller> rendererTable = new HashMap<QName,Controller>();
  private QName colorColumnID;
  private QName rowColorID;
  private QName rowCompID;

  public TreeViewPI( QName componentID, QName rowCompID, QName rowColorID, QName colorColumnID ) {
    super( componentID, rowCompID );
    this.rowCompID = rowCompID;
    this.rowColorID = rowColorID;
    this.colorColumnID = colorColumnID;
  }

  public void setController( TreeController treeController ) {
    this.treeController = treeController;
  }

  @Override
  public Controller getController() {
    return treeController;
  }

  public TreeController getTreeCtrl() {
    return treeController;
  }

  /**
   * Returns the UIAdapter connected to this view
   *
   * @return this view's UIAdapter
   */
  @Override
  public UIAdapter getUIAdapter() {
    return uiTreeAdapter;
  }

  /**
   * Called to attach this view's to it's UIAdapter and UI component
   *  @param uiContainer the container to look for this view's UI component
   *
   */
  @Override
  public void attachUIComponent( UIContainer uiContainer ) {
    if (uiTreeAdapter == null) {
      this.uiTreeAdapter = uiContainer.getUIFactory().createTreeAdapter( uiContainer, this );
    }
  }

  /**
   * Called to create UIAdapter for matching component in uiContainer.
   * The view must not store any link to the created UIAdapter.
   * This kind of UIAdapters is used for rendering table a tree cells and
   * makes it possible to process events from within such a cell by this view.
   *
   * @param uiContainer the container to look for this view's UI component
   * @return the UIAdapter
   */
  @Override
  public UIAdapter attachUIRenderComponent( UIContainer uiContainer ) {
    return uiContainer.getUIFactory().createTreeAdapter( uiContainer, this );
  }

  /**
   * Called to detach this view from it's UIAdapter and UI component
   */
  @Override
  public void detachUIComponent() {
    this.uiTreeAdapter = null;
  }

  /**
   * Adds a renderer for the given valueID
   * @param valueID  valueID for which renderer is used
   * @param renderer the renderer
   */
  public void addRenderer( QName valueID, Controller renderer ) {
    rendererTable.put( valueID, renderer );
  }

  public String render( Object value, QName valueID ) {
    if (valueID == null) {
      return StringRenderer.get( getInputMode() ).render( value );
    }
    else {
      Controller renderer = rendererTable.get( valueID );
      if (renderer == null) {
        return StringRenderer.get( getInputMode() ).render( value );
      }
      else {
        return renderer.render( value );
      }
    }
  }

  public void setRootModel( Model rootModel, QName displayValueID, QName iconID, QName editFlagID, QName enabledFlagID, QName childRelationID ) {
    rootNode.setNodeModel( rootModel, displayValueID, iconID, editFlagID, enabledFlagID, childRelationID );
    if (uiTreeAdapter != null) {
      uiTreeAdapter.setRootNode( rootNode );
    }
  }

  public TreeNodePI getRootNode() {
    return rootNode;
  }

  public TreeNodePI getSelectedNode() {
    if (uiTreeAdapter == null) {
      return null;
    }
    else {
      return uiTreeAdapter.getSelectedNode();
    }
  }

  public void onSelectionChanged( TreeNodePI selected ) {
    if (treeController != null) {
      treeController.onSelectionChanged( selected );
    }
  }

  public void setSelectedModel( Model nodeModel ) {
    if (uiTreeAdapter != null) {
      TreeNodePI selectedNode = rootNode.findNode( nodeModel );
      uiTreeAdapter.setSelectedNode( selectedNode );
    }
  }

  /**
   * Returns the ID of the UI module which should be used as row stencil.
   * 
   * @return row stencil UI ID
   */
  public QName getRowCompID() {
    return rowCompID;
  }

  /**
   * Returns the ID of the column which should be colored. If null, the whole row is colored.
   *
   * @return color column ID
   */
  public QName getColorColumnID() {
    return colorColumnID;
  }

  /**
   * Returns ID of the attribute used for styling the row, e.g. background color.
   *
   * @return row style value ID
   */
  public QName getRowColorID() {
    return rowColorID;
  }

  /**
   * Returns true if this table allows (inplace) editing
   *
   * @return true if table is editable
   */
  public boolean isEditable() {
    TreeController treeController = getTreeCtrl();
    if (treeController != null) {
      return !treeController.isReadOnly();
    }
    return false;
  }

  public void editNode( TreeNodePI treeNode ) {
    if (uiTreeAdapter != null) {
      uiTreeAdapter.editNode( treeNode );
    }
  }

  public void onStopEdit( TreeNodePI editedNode, QName valueID, Object newValue ) {
    TreeController treeController = getTreeCtrl();
    if (treeController != null) {
      treeController.onStopEdit( editedNode, valueID, newValue );
    }
  }

  public void updateNode( TreeNodePI treeNodePI ) {
    if (uiTreeAdapter != null) {
      uiTreeAdapter.updateNode( treeNodePI );
    }
  }

  public void updateChildList( TreeNodePI treeNodePI ) {
    if (uiTreeAdapter != null) {
      uiTreeAdapter.updateChildList( treeNodePI );
    }
  }

  public void setExpanded( TreeNodePI treeNodePI, boolean expand ) {
    if (uiTreeAdapter != null) {
      uiTreeAdapter.setExpanded(treeNodePI, expand);
    }
  }

}
