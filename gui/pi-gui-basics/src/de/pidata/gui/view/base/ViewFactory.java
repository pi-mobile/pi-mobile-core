/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.controller.base.ListController;
import de.pidata.qnames.QName;

public class ViewFactory {

  public ButtonViewPI createButtonView( QName componentID ) {
    return new ButtonViewPI( componentID, null );
  }

  public TextViewPI createTextView( QName componentID ) {
    return new TextViewPI( componentID, null );
  }

  public DateViewPI createDateView( QName componentID ) {
    return new DateViewPI( componentID, null );
  }

  public TextEditorViewPI createTextEditorView( QName componentID ) {
    return new TextEditorViewPI( componentID, null );
  }

  public CodeEditorViewPI createCodeEditorView( QName componentID ) {
    return new CodeEditorViewPI( componentID, null );
  }

  public WebViewPI createWebView( QName componentID ) {
    return new WebViewPI( componentID, null );
  }

  public HTMLEditorViewPI createHTMLEditorView( QName componentID ) {
    return new HTMLEditorViewPI( componentID, null );
  }

  public NodeViewPI createNodeView( QName componentID ) {
    return new NodeViewPI( componentID, null );
  }

  public TabPaneViewPI createTabPaneView( QName componentID ) {
    return new TabPaneViewPI( componentID, null );
  }

  public ListViewPI createListView( QName componentID, QName rowCompID, ListController controller ) {
    return new ListViewPI( componentID, null, rowCompID, null, null );
  }

  public TableViewPI createTableView( QName componentID, QName rowCompID, QName rowColorID, QName colorColumnID ) {
    return new TableViewPI( componentID, null, rowCompID, rowColorID, colorColumnID );
  }

  public ImageViewPI createImageView( QName componentID ) {
    return new ImageViewPI( componentID, null );
  }

  public FlagViewPI createFlagView( QName componentID, boolean isTristate ) {
    return new FlagViewPI( componentID, null, isTristate );
  }

  public PaintViewPI createPaintView( QName componentID ) {
    return new PaintViewPI( componentID, null );
  }

  public ProgressBarViewPI createProgressView( QName componentID ) {
    return new ProgressBarViewPI( componentID, null );
  }

  public TabGroupViewPI createTabGroupViewPI( QName componentID ) {
    return new TabGroupViewPI( componentID, null );
  }

  public ModuleViewPI createModuleView( QName componentID ) {
    return new ModuleViewPI( componentID, null );
  }

  public TreeViewPI createTreeView( QName treeID, QName rowColorID, QName colorColumnID ) {
    return new TreeViewPI( treeID, null, rowColorID, colorColumnID );
  }

  public TreeTableViewPI createTreeTableView( QName tableID, QName rowCompID, QName rowColorID, QName colorColumnID ) {
    return new TreeTableViewPI( tableID, rowCompID, rowColorID, colorColumnID );
  }

  //------------------------------------------------------------
  // old: with obsolete modulID
  //------------------------------------------------------------
  public ButtonViewPI createButtonView( QName componentID, QName moduleID ) {
    return new ButtonViewPI( componentID, moduleID );
  }

  public TextViewPI createTextView( QName componentID, QName moduleID ) {
    return new TextViewPI( componentID, moduleID );
  }

  public DateViewPI createDateView( QName componentID, QName moduleID ) {
    return new DateViewPI( componentID, moduleID );
  }

  public TextEditorViewPI createTextEditorView( QName componentID, QName moduleID ) {
    return new TextEditorViewPI( componentID, moduleID );
  }

  public CodeEditorViewPI createCodeEditorView( QName componentID, QName moduleID ) {
    return new CodeEditorViewPI( componentID, moduleID );
  }

  public WebViewPI createWebView( QName componentID, QName moduleID ) {
    return new WebViewPI( componentID, moduleID );
  }

  public HTMLEditorViewPI createHTMLEditorView( QName componentID, QName moduleID ) {
    return new HTMLEditorViewPI( componentID, moduleID );
  }

  public NodeViewPI createNodeView( QName componentID, QName moduleID ) {
    return new NodeViewPI( componentID, moduleID );
  }

  public TabPaneViewPI createTabPaneView( QName componentID, QName moduleID ) {
    return new TabPaneViewPI( componentID, moduleID );
  }

  public ListViewPI createListView( QName componentID, QName moduleID, QName rowCompID, ListController controller ) {
    return new ListViewPI( componentID, moduleID, rowCompID, null, null );
  }

  public TableViewPI createTableView( QName componentID, QName moduleID, QName rowCompID, QName rowColorID, QName colorColumnID ) {
    return new TableViewPI( componentID, moduleID, rowCompID, rowColorID, colorColumnID );
  }

  public ImageViewPI createImageView( QName componentID, QName moduleID ) {
    return new ImageViewPI( componentID, moduleID );
  }

  public FlagViewPI createFlagView( QName componentID, QName moduleID, boolean isTristate ) {
    return new FlagViewPI( componentID, moduleID, isTristate );
  }

  public PaintViewPI createPaintView( QName componentID, QName moduleID ) {
    return new PaintViewPI( componentID, moduleID );
  }

  public ProgressBarViewPI createProgressView( QName componentID, QName moduleID ) {
    return new ProgressBarViewPI( componentID, moduleID );
  }

  public TabGroupViewPI createTabGroupViewPI( QName componentID, QName moduleID ) {
    return new TabGroupViewPI( componentID, moduleID );
  }

  public ModuleViewPI createModuleView( QName componentID, QName moduleID ) {
    return new ModuleViewPI( componentID, moduleID );
  }

  public TreeViewPI createTreeView( QName treeID, QName moduleID, QName rowColorID, QName colorColumnID ) {
    return new TreeViewPI( treeID, moduleID, rowColorID, colorColumnID );
  }
}
