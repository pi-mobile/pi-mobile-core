/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.TextController;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITextAdapter;
import de.pidata.gui.ui.base.UIValueAdapter;
import de.pidata.qnames.QName;

public class TextViewPI extends AbstractViewPI {

  protected UIValueAdapter uiValueAdapter;
  private TextController textController;

  private boolean hideInput = false;
  private String savedText = null;
  private boolean listenTextChanges;

  public TextViewPI( QName componentID, QName moduleID ) {
    super( componentID, moduleID );
  }

  public Controller getController() {
    return textController;
  }

  public void setController( TextController textController ) {
    this.textController = textController;
  }

  public UIAdapter getUIAdapter() {
    return uiValueAdapter;
  }

  /**
   * Called to attach this view's to it's UIAdapter and UI component
   *  @param uiContainer the dialog to look for this view's dialog component
   *
   */
  public void attachUIComponent( UIContainer uiContainer ) {
    if (uiValueAdapter == null) {
      UITextAdapter uiTextAdapter = createTextAdapter( uiContainer );
      this.uiValueAdapter = uiTextAdapter;
      uiTextAdapter.setHideInput( hideInput );
      uiTextAdapter.setListenTextChanges( listenTextChanges );
    }
    setSavedValue();
  }

  /**
   * Called to create UIAdapter for matching component in uiContainer.
   * The view must not store any link to the created UIAdapter.
   * This kind of UIAdapters is used for rendering table a tree cells and
   * makes it possible to process events from within such a cell by this view.
   *
   * @param uiContainer the container to look for this view's UI component
   * @return the UIAdapter
   */
  @Override
  public UIAdapter attachUIRenderComponent( UIContainer uiContainer ) {
    return createTextAdapter( uiContainer );
  }

  protected UITextAdapter createTextAdapter( UIContainer uiContainer ) {
    return uiContainer.getUIFactory().createTextAdapter( uiContainer, this );
  }

  private void setSavedValue() {
    if (savedText != null) {
      uiValueAdapter.setValue( savedText );
    }
  }

  public void detachUIComponent() {
    if (uiValueAdapter != null) {
      uiValueAdapter.detach();
      uiValueAdapter = null;
    }
  }

  public void setHideInput( boolean hideInput ) {
    this.hideInput = hideInput;
  }

  public String getValue() {
    if (uiValueAdapter != null) {
      Object value = uiValueAdapter.getValue();
      if (value == null) {
        return "";
      }
      else {
        return value.toString();
      }
    }
    return "";
  }

  public void updateValue( String value ) {
    this.savedText = value;
    if (uiValueAdapter != null) {
      if (value == null) {
        uiValueAdapter.setValue( "" );
      }
      else {
        uiValueAdapter.setValue( value );
      }
    }
  }

  public void setCursorPos( short posX, short posY ) {
    if (uiValueAdapter instanceof UITextAdapter) {
      ((UITextAdapter) uiValueAdapter).setCursorPos( posX, posY );
    }
  }

  /**
   * May be called by UI to write complete value to model
   */
  public void saveValue() {
    textController.saveValue();
  }

  public void setListenTextChanges( boolean listenTextChanges ) {
    this.listenTextChanges = listenTextChanges;
    if (uiValueAdapter != null) {
      ((UITextAdapter) uiValueAdapter).setListenTextChanges( listenTextChanges );
    }
  }

  public void textChanged( String oldValue, String newValue ) {
    textController.textChanged( oldValue, newValue );
  }

  public void keyPressed( QName keyCode ) {
    textController.keyPressed( keyCode );
  }

  public void select( int fromPos, int toPos ) {
    ((UITextAdapter) getUIAdapter()).select( fromPos, toPos );
  }

  public void selectAll() {
    ((UITextAdapter) getUIAdapter()).selectAll();
  }

  public void setTextColor( ComponentColor color ) {
    if (uiValueAdapter instanceof UITextAdapter) {
      ((UITextAdapter) uiValueAdapter).setTextColor(color);
    }
  }
}
