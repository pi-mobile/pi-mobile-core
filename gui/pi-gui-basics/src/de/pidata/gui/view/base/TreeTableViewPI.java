/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.InputMode;
import de.pidata.gui.renderer.StringRenderer;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITableAdapter;
import de.pidata.gui.ui.base.UITreeTableAdapter;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

public class TreeTableViewPI extends TreeViewPI implements ColumnView {

  public TreeTableViewPI( QName componentID, QName rowCompID, QName rowColorID, QName colorColumnID ) {
    super( componentID, rowCompID, rowColorID, colorColumnID );
  }

  @Override
  public void detachUIComponent() {
    if (uiTreeAdapter instanceof SettingsHandler) {
      ((SettingsHandler)uiTreeAdapter).saveSettings();
    }
    super.detachUIComponent();
  }

  /**
   * Called to attach this view's to it's UIAdapter and UI component
   *  @param uiContainer the container to look for this view's UI component
   *
   */
  @Override
  public void attachUIComponent( UIContainer uiContainer ) {
    if (uiTreeAdapter == null) {
      this.uiTreeAdapter = uiContainer.getUIFactory().createTreeTableAdapter( uiContainer, this );
    }
    if (uiTreeAdapter instanceof SettingsHandler) {
      ((SettingsHandler) uiTreeAdapter).loadSettings();
    }
  }

  /**
   * Returns cell renderer for columnInfo
   * @param columnInfo columnInfo or null for default renderer
   * @param cellValue
   * @return the cell renderer, default is StringRenderer
   */
  public String render( ColumnInfo columnInfo, Object cellValue ) {
    if (columnInfo == null) {
      return StringRenderer.get( InputMode.string, Integer.valueOf( -1 ) ).render( cellValue );
    }
    else {
      Controller renderCtrl = columnInfo.getRenderCtrl();
      if (renderCtrl == null) {
        return StringRenderer.get( getInputMode() ).render( cellValue );
      }
      else {
        return renderCtrl.render( cellValue );
      }
    }
  }

  public ColumnInfo getColumnByCompID( QName columnID ) {
    ValueController selectionController = getTreeCtrl();
    if (selectionController != null) {
      TreeTableController tableController = (TreeTableController) selectionController;
      return tableController.getColumnByBodyCompID( columnID );
    }
    else {
      return null;
    }
  }

  /**
   * Return value for given cell
   * @param row         row to which cell belongs
   * @param columnInfo  column to which cell belongs
   * @return the cell value
   */
  public Object getCellValue( Model row, ColumnInfo columnInfo ) {
    ValueController selectionController = getTreeCtrl();
    if (selectionController != null) {
      TreeTableController tableController = (TreeTableController) selectionController;
      try {
        return columnInfo.getCellValue( row );
      }
      catch (Exception ex) {
        Logger.error( "Error getting column value for column name=" + columnInfo.getColName(), ex );
        return null;
      }
    }

    return null;
  }

  /**
   * Notifies this table view that editing has been started on given cell
   *
   * @param editNode    data node on which editing has been started
   * @param columnInfo  column in which editing has been started
   */
  public void onStartEdit( TreeNodePI editNode, ColumnInfo columnInfo ) {
    TreeController treeCtrl = getTreeCtrl();
    if (treeCtrl != null && treeCtrl instanceof TableController) {
      TreeTableController treeTableController = (TreeTableController) treeCtrl;
      //TODO? treeTableController.onStartEdit( editNode.getNodeModel(), columnInfo );
    }
  }

  /**
   * Notifies this table view that editing has stopped on given cell. Table now
   * has to store the newValue in the editedRow.
   *
   * @param editNode    data node on which editing has been stopped
   * @param valueID     ID of modified value
   * @param newValue    new value for the given cell
   */
  public void onStopEdit( TreeNodePI editNode, QName valueID, Object newValue ) {
    TreeController treeCtrl = getTreeCtrl();
    if (treeCtrl != null && treeCtrl instanceof TreeTableController) {
      TreeTableController treeTableController = (TreeTableController) treeCtrl;
      treeTableController.onStopEdit( editNode, valueID, newValue );
    }
  }

  public void onSelectedCell( TreeNodePI selectedNode, ColumnInfo columnInfo ) {
    TreeController treeCtrl = getTreeCtrl();
    if (treeCtrl != null && treeCtrl instanceof TreeTableController) {
      TreeTableController tableController = (TreeTableController) treeCtrl;
      tableController.onSelectedCell( selectedNode, columnInfo );
    }
  }

  public void onDoubleClickCell( TreeNodePI selectedNode, ColumnInfo columnInfo ) {
    TreeController treeCtrl = getTreeCtrl();
    if (treeCtrl != null && treeCtrl instanceof TreeTableController) {
      TreeTableController tableController = (TreeTableController) treeCtrl;
      tableController.onDoubleClickCell( selectedNode, columnInfo );
    }
  }

  public void onRightClickCell( TreeNodePI selectedNode, ColumnInfo columnInfo ) {
    TreeController treeCtrl = getTreeCtrl();
    if (treeCtrl != null && treeCtrl instanceof TreeTableController) {
      TreeTableController tableController = (TreeTableController) treeCtrl;
      tableController.onRightClickCell( selectedNode, columnInfo );
    }
  }

  @Override
  public void onColumnChanged( ColumnInfo columnInfo ) {
    if (uiTreeAdapter != null) {
      ((UITreeTableAdapter) uiTreeAdapter).columnChanged( columnInfo );
    }
  }
}