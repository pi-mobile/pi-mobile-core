/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.TabControllerGroup;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITabGroupAdapter;
import de.pidata.qnames.QName;

public class TabGroupViewPI extends AbstractViewPI {

  private UITabGroupAdapter uiTabGroupAdapter;
  private TabControllerGroup tabControllerGroup;

  public TabGroupViewPI( QName componentID, QName moduleID ) {
    super( componentID, moduleID );
  }

  public Controller getController() {
    return tabControllerGroup;
  }


  public void setController( TabControllerGroup tabControllerGroup ) {
    this.tabControllerGroup = tabControllerGroup;
  }

  public UIAdapter getUIAdapter() {
    return uiTabGroupAdapter;
  }

  public void updateValue( Object value ) {
    if (uiTabGroupAdapter != null) {
      uiTabGroupAdapter.setValue( value );
    }
  }

  public void attachUIComponent( UIContainer uiContainer ) {
    if (uiTabGroupAdapter == null) {
      uiTabGroupAdapter = uiContainer.getUIFactory().createTabGroupAdapter( uiContainer, this );
    }
  }

  /**
   * Called to create UIAdapter for matching component in uiContainer.
   * The view must not store any link to the created UIAdapter.
   * This kind of UIAdapters is used for rendering table a tree cells and
   * makes it possible to process events from within such a cell by this view.
   *
   * @param uiContainer the container to look for this view's UI component
   * @return the UIAdapter
   */
  @Override
  public UIAdapter attachUIRenderComponent( UIContainer uiContainer ) {
    return uiContainer.getUIFactory().createTabGroupAdapter( uiContainer, this );
  }

  public void detachUIComponent() {
    if (uiTabGroupAdapter != null) {
      uiTabGroupAdapter.detach();
      uiTabGroupAdapter = null;
    }
  }
}