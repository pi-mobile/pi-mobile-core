/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.controller.base.ColumnInfo;
import de.pidata.gui.controller.base.SelectionController;
import de.pidata.gui.controller.base.SettingsHandler;
import de.pidata.gui.controller.base.TableController;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITableAdapter;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

import java.util.HashMap;
import java.util.List;

public class TableViewPI extends ListViewPI implements ColumnView {

  private boolean autoScroll = false;

  public TableViewPI( QName componentID, QName moduleID, QName rowCompID, QName rowColorID, QName colorColumnID ) {
    super( componentID, moduleID, rowCompID, rowColorID, colorColumnID );
  }

  public boolean isAutoScroll() {
    return autoScroll;
  }

  public void setAutoScroll( boolean autoScroll ) {
    this.autoScroll = autoScroll;
  }

  /**
   * Called to attach this view's to it's UIAdapter and UI component
   *  @param uiContainer the dialog to look for this view's dialog component
   *
   */
  public void attachUIComponent( UIContainer uiContainer ) {
    if (uiListAdapter == null) {
      this.uiListAdapter = uiContainer.getUIFactory().createTableAdapter( uiContainer, this );
    }
    if (uiListAdapter instanceof SettingsHandler) {
      ((SettingsHandler) uiListAdapter).loadSettings();
    }
  }

  /**
   * Called to detach this view from it's UIAdapter and UI component
   */
  public void detachUIComponent() {
    if (uiListAdapter instanceof SettingsHandler) {
      ((SettingsHandler)uiListAdapter).saveSettings();
    }
    super.detachUIComponent();
  }

  /**
   * Return number of columns this tabel view has
   * @return number of columns
   */
  public int getColumnCount() {
    return ((TableController) getListCtrl()).columnCount();
  }

  public ColumnInfo getColumn( short index ) {
    SelectionController selectionController = getListCtrl();
    if (selectionController != null && selectionController instanceof TableController) {
      TableController tableController = (TableController) selectionController;
      return tableController.getColumn( index );
    }
    return null;
  }

  /**
   * Return value for given cell
   * @param row         row to which cell belongs
   * @param columnInfo  column to which cell belongs
   * @return the cell value
   */
  public Object getCellValue( Model row, ColumnInfo columnInfo ) {
    SelectionController selectionController = getListCtrl();
    if (selectionController != null && selectionController instanceof TableController) {
      TableController tableController = (TableController) selectionController;
      try {
        return columnInfo.getCellValue( row );
      }
      catch (Exception ex) {
        Logger.error( "Error getting column value for column name=" + columnInfo.getColName(), ex );
        return null;
      }
    }
    return null;
  }

  /**
   * Tells this view to edit cell for rowModel in column colName
   *
   * @param rowModel row model to edit
   * @param colName  name of column to edit
   */
  public void editCell( Model rowModel, QName colName ) {
    if (uiListAdapter != null) {
      ColumnInfo columnInfo = ((TableController) getListCtrl()).getColumnByName( colName );
      ((UITableAdapter) uiListAdapter).editCell( rowModel, columnInfo );
    }
  }

  public void onSelectedCell( Model selectedRow, ColumnInfo columnInfo ) {
    SelectionController selectionController = getListCtrl();
    if (selectionController != null && selectionController instanceof TableController) {
      TableController tableController = (TableController) selectionController;
      tableController.onSelectedCell( selectedRow, columnInfo );
    }
  }

  public void onDoubleClickCell( Model selectedRow, ColumnInfo columnInfo ) {
    SelectionController selectionController = getListCtrl();
    if (selectionController != null && selectionController instanceof TableController) {
      TableController tableController = (TableController) selectionController;
      tableController.onDoubleClickCell( selectedRow, columnInfo );
    }
  }

  public void onRightClickCell( Model selectedRow, ColumnInfo columnInfo ) {
    SelectionController selectionController = getListCtrl();
    if (selectionController != null && selectionController instanceof TableController) {
      TableController tableController = (TableController) selectionController;
      tableController.onRightClickCell( selectedRow, columnInfo );
    }
  }

  /**
   * Returns true if this table allows (inplace) editing
   *
   * @return true if table is editable
   */
  public boolean isEditable() {
    SelectionController selectionController = getListCtrl();
    if (selectionController != null && selectionController instanceof TableController) {
      TableController tableController = (TableController) selectionController;
      return tableController.isEditable();
    }
    return false;
  }

  /**
   * Returns true if this table allows selecting row(s)
   *
   * @return true if table is editable
   */
  public boolean isSelectable(){
    SelectionController selectionController = getListCtrl();
    if(selectionController!=null && selectionController instanceof TableController){
      TableController tableController = (TableController) selectionController;
      return tableController.isSelectable();
    }
    return false;
  }

  /**
   * Returns column info to which component with bodyCompID belongs
   * @param bodyCompID  body component ID
   * @return the column info
   */
  public ColumnInfo getColumnByCompID( QName bodyCompID ) {
    SelectionController selectionController = getListCtrl();
    if (selectionController != null && selectionController instanceof TableController) {
      TableController tableController = (TableController) selectionController;
      return tableController.getColumnByBodyCompID( bodyCompID );
    }
    else {
      return null;
    }
  }

  /**
   * Notifies this table view that editing has been started on given cell
   *
   * @param editRow     data row on which editing has been started
   * @param columnInfo  column in which editing has been started
   */
  public void onStartEdit( Model editRow, ColumnInfo columnInfo ) {
    SelectionController selectionController = getListCtrl();
    if (selectionController != null && selectionController instanceof TableController) {
      TableController tableController = (TableController) selectionController;
      tableController.onStartEdit( editRow, columnInfo );
    }
  }

  /**
   * Notifies this table view that editing has stopped on given cell. Table now
   * has to store the newValue in the editedRow.
   *
   * @param editedRow   data row on which editing has been stopped
   * @param columnInfo  column in which editing has been stopped
   * @param newValue    new value for the given cell
   */
  public void onStopEdit( Model editedRow, ColumnInfo columnInfo, Object newValue ) {
    SelectionController selectionController = getListCtrl();
    if (selectionController != null && selectionController instanceof TableController) {
      TableController tableController = (TableController) selectionController;
      tableController.onStopEdit( editedRow, columnInfo, newValue );
    }
  }

  /**
   * Called by UITableAdapter if row filtering has been activated/deactivated
   *
   * @param filterActive true if row filtering is active
   */
  public void onFiltered( boolean filterActive ) {
    SelectionController selectionController = getListCtrl();
    if (selectionController != null && selectionController instanceof TableController) {
      TableController tableController = (TableController) selectionController;
      tableController.onFiltered( filterActive );
    }
  }

  public HashMap<QName, Object> getFilteredValues() {
    if (uiListAdapter != null) {
      return ((UITableAdapter) uiListAdapter).getFilteredValues();
    }
    return new HashMap<QName, Object>(  );
  }

  public void unselectValues (HashMap<QName, Object> unselectedValuesMap ){
    if (uiListAdapter != null){
      ((UITableAdapter) uiListAdapter).unselectValues(unselectedValuesMap);
    }
  }

  public void finishedUpdateAllRows() {
    if (uiListAdapter != null) {
      ((UITableAdapter) uiListAdapter).finishedUpdateAllRows();
    }
  }

  public List<Model> getVisibleRows (){
    return ((UITableAdapter) uiListAdapter).getVisibleRowModels();
  }

  @Override
  public void onColumnChanged( ColumnInfo columnInfo ) {
    if (uiListAdapter != null) {
      ((UITableAdapter) uiListAdapter).columnChanged( columnInfo );
    }
  }
}