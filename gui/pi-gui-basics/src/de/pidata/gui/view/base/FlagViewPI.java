/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.base;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.FlagController;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIFlagAdapter;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

public class FlagViewPI extends AbstractViewPI {

  private UIFlagAdapter flagAdapter;
  private FlagController flagController;
  private Object         format;
  private boolean        isTristate;

  public FlagViewPI( QName componentID, QName moduleID, boolean isTristate ) {
    super( componentID, moduleID );
    this.isTristate = isTristate;
  }

  public void setController( FlagController flagController ) {
    this.flagController = flagController;
  }

  public Controller getController() {
    return flagController;
  }

  public UIAdapter getUIAdapter() {
    return flagAdapter;
  }

  /**
   * Called to attach this view's to it's UIAdapter and UI component
   *  @param uiContainer the dialog to look for this view's dialog component
   *
   */
  public void attachUIComponent( UIContainer uiContainer ) {
    if (flagAdapter == null) {
      flagAdapter = uiContainer.getUIFactory().createFlagAdapter( uiContainer, this );
    }
  }

  /**
   * Called to create UIAdapter for matching component in uiContainer.
   * The view must not store any link to the created UIAdapter.
   * This kind of UIAdapters is used for rendering table a tree cells and
   * makes it possible to process events from within such a cell by this view.
   *
   * @param uiContainer the container to look for this view's UI component
   * @return the UIAdapter
   */
  @Override
  public UIAdapter attachUIRenderComponent( UIContainer uiContainer ) {
    return uiContainer.getUIFactory().createFlagAdapter( uiContainer, this );
  }

  public void detachUIComponent() {
    if (flagAdapter != null) {
      flagAdapter.detach();
      flagAdapter = null;
    }
  }

  public Object getValue() {
    if (flagAdapter != null) {
      return flagAdapter.getValue();
    }
    else {
      return null;
    }
  }

  public void updateValue( Boolean value ) {
    if (flagAdapter != null) {
      flagAdapter.setValue( value );
    }
  }

  public void onCheckChanged( UIFlagAdapter source, Boolean checkedValue, Model dataContext ) {
    if (flagController != null) {
      if (source == flagAdapter) {
        dataContext = null;
      }
      flagController.onCheckChanged( checkedValue, dataContext );
    }
  }

  public void updateLabel( Object value ) {
    if (flagAdapter != null) {
      flagAdapter.setLabelValue( value );
    }
  }

  public Object getLabelValue() {
    if (flagAdapter != null) {
      return flagAdapter.getLabelValue();
    }
    return null;
  }

  public boolean isTristate() {
    return isTristate;
  }

  public void setTristate( boolean tristate ) {
    isTristate = tristate;
  }
}
