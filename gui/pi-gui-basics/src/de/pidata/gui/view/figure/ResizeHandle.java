/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.gui.ui.base.UIShapeAdapter;
import de.pidata.gui.view.base.PaintViewPI;
import de.pidata.rect.*;

/**
 * Created by pru on 09.06.17.
 */
public class ResizeHandle implements FigureHandle, PosSizeEventListener {

  private Figure owner;
  protected UIShapeAdapter uiShapeAdapter;
  private HandlePosition handlePosition;
  private DeltaRect bounds;

  public ResizeHandle( Figure owner, HandlePosition handlePosition, DeltaRect bounds ) {
    this.handlePosition = handlePosition;
    this.owner = owner;
    this.bounds = bounds;
    bounds.getEventSender().addListener( this );
  }

  /**
   * Returns the type of this shape - used for rendering on UI
   *
   * @return the shape type
   */
  @Override
  public ShapeType getShapeType() {
    return ShapeType.rect;
  }

  @Override
  public Rect getBounds() {
    return bounds;
  }

  /**
   * Returns this shape's style: color, line type, font, ...
   *
   * @return shape style, never null
   */
  @Override
  public ShapeStyle getShapeStyle() {
    return ShapeStyle.STYLE_BLACK;
  }

  @Override
  public Figure getOwner() {
    return owner;
  }

  /**
   * Returns number of positions this shape uses for drawing.
   * A a rectangle has one position (top left)
   *
   * @return number of positions: 1
   */
  @Override
  public int posCount() {
    return 1;
  }

  /**
   * Returns position at index, for details see posCount()
   *
   * @param index the position's index
   * @return position at index
   */
  @Override
  public Pos getPos( int index ) {
    if (index == 0) {
      return getBounds();
    }
    else {
      throw new IndexOutOfBoundsException( "Index must be 0, but is="+index );
    }
  }

  /**
   * Returns the text this shape shows or null if none.
   *
   * @return this shape's text
   */
  @Override
  public String getText() {
    return null;
  }

  @Override
  public HandlePosition getHandlePosition() {
    return handlePosition;
  }

  @Override
  public Figure getFigure() {
    return owner;
  }

  @Override
  public void attachUI( UIShapeAdapter uiShapeAdapter ) {
    this.uiShapeAdapter = uiShapeAdapter;
  }

  @Override
  public void detachUI() {
    this.uiShapeAdapter = null;
  }

  @Override
  public UIShapeAdapter getUIAdapter() {
    return uiShapeAdapter;
  }

  @Override
  public void posChanged( Pos pos, double oldX, double oldY ) {
    PaintViewPI paintViewPI = owner.getPaintView();
    if (paintViewPI != null) {
      paintViewPI.figureModified( owner, this );
    }
  }

  @Override
  public void sizeChanged( Rect rect, double oldWidth, double oldHeight ) {
    PaintViewPI paintViewPI = owner.getPaintView();
    if (paintViewPI != null) {
      paintViewPI.figureModified( owner, this );
    }
  }

  @Override
  public void rotationChanged( PosDir posDir, Rotation oldRotation ) {
    PaintViewPI paintViewPI = owner.getPaintView();
    if (paintViewPI != null) {
      paintViewPI.figureModified( owner, this );
    }
  }
}
