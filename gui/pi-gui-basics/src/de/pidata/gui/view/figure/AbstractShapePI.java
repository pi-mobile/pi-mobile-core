/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.ui.base.UIShapeAdapter;
import de.pidata.gui.view.base.PaintViewPI;
import de.pidata.rect.*;

/**
 * Created by pru on 14.06.17.
 */
public abstract class AbstractShapePI implements ShapePI, PosSizeEventListener, StyleEventListener {

  private Figure figure;
  private UIShapeAdapter uiShapeAdapter;
  private Rect bounds;
  private ShapeStyle shapeStyle;

  public AbstractShapePI( Figure figure, Rect bounds, ShapeStyle shapeStyle ) {
    if (figure == null) {
      throw new IllegalArgumentException( "Figure must not be null" );
    }
    if (bounds == null) {
      throw new IllegalArgumentException( "Bounds must not be null" );
    }
    if (shapeStyle == null) {
      throw new IllegalArgumentException( "ShapeStyle must not be null" );
    }
    this.figure = figure;
    this.bounds = bounds;
    this.shapeStyle = shapeStyle;
    bounds.getEventSender().addListener( this );
    shapeStyle.getEventSender().addListener( this );
  }

  @Override
  public Figure getFigure() {
    return figure;
  }

  @Override
  public Rect getBounds() {
    return bounds;
  }

  public ShapeStyle getShapeStyle() {
    return shapeStyle;
  }

  public void setShapeStyle( ShapeStyle shapeStyle ) {
    if (shapeStyle == null) {
      throw new IllegalArgumentException( "ShapeStyle must not be null" );
    }
    shapeStyle.getEventSender().removeListener( this );
    this.shapeStyle = shapeStyle;
    shapeStyle.getEventSender().addListener( this );
    appearanceChanged();
  }

  /**
   * Returns the text this shape shows or null if none.
   *
   * @return this shape's text
   */
  @Override
  public String getText() {
    return null;
  }

  protected PaintViewPI getPaintViewPI() {
    return getFigure().getPaintView();
  }

  @Override
  public void attachUI( UIShapeAdapter uiShapeAdapter ) {
    this.uiShapeAdapter = uiShapeAdapter;
  }

  @Override
  public void detachUI() {
    this.uiShapeAdapter = null;
  }

  @Override
  public UIShapeAdapter getUIAdapter() {
    return uiShapeAdapter;
  }

  @Override
  public void posChanged( Pos pos, double oldX, double oldY ) {
    PaintViewPI paintViewPI = getPaintViewPI();
    if (paintViewPI != null) {
      paintViewPI.figureModified( getFigure(), this );
    }  
  }

  @Override
  public void sizeChanged( Rect rect, double oldWidth, double oldHeight ) {
    PaintViewPI paintViewPI = getPaintViewPI();
    if (paintViewPI != null) {
      paintViewPI.figureModified( getFigure(), this );
    }
  }

  @Override
  public void rotationChanged( PosDir posDir, Rotation oldRotation ) {
    PaintViewPI paintViewPI = getPaintViewPI();
    if (paintViewPI != null) {
      paintViewPI.figureModified( getFigure(), this );
    }
  }

  @Override
  public void colorChanged( ShapeStyle shapeStyle, ComponentColor oldBorderColor, ComponentColor oldFillColor, boolean oldIsTransparent ) {
    appearanceChanged();
  }

  @Override
  public void fontChanged( ShapeStyle shapeStyle, String oldFontName, TextAlign oldTextAlign ) {
    appearanceChanged();
  }

  @Override
  public void strokeChanged( ShapeStyle shapeStyle, StrokeType oldStrokeType, double oldStrokeWidth ) {
    appearanceChanged();
  }

  /**
   * Unspecified change; force repaint.
   *
   * @param owner
   */
  @Override
  public void styleChanged( ShapeStyle owner ) {
    appearanceChanged();
  }

  public void appearanceChanged() {
    PaintViewPI paintViewPI = getPaintViewPI();
    if (paintViewPI != null) {
      paintViewPI.figureModified( getFigure(), this );
    }
  }

}
