/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.rect.LineRect;
import de.pidata.rect.Pos;

/**
 * Created by pru on 11.06.17.
 */
public class LineShapePI extends AbstractShapePI {

  private Pos startPos;
  private Pos endPos;

  public LineShapePI( Figure figure, Pos startPos, Pos endPos ) {
    this( figure, startPos, endPos, ShapeStyle.STYLE_BLACK );
  }

  public LineShapePI( Figure figure, Pos startPos, Pos endPos, ShapeStyle style ) {
    super( figure, new LineRect( startPos, endPos ), style );
    this.startPos = startPos;
    this.endPos = endPos;
  }

  /**
   * Returns the type of this shape - used for rendering on UI
   *
   * @return the shape type
   */
  @Override
  public ShapeType getShapeType() {
    return ShapeType.line;
  }

  public Pos getStartPos() {
    return startPos;
  }

  public Pos getEndPos() {
    return endPos;
  }

  /**
   * Returns number of positions this shape uses for drawing.
   * A line has a start and end position
   *
   * @return number of positions: 2
   */
  @Override
  public int posCount() {
    return 2;
  }

  /**
   * Returns position at index, for details see posCount()
   *
   * @param index 0 for startPos, 1 for endPos
   * @return position at index
   */
  @Override
  public Pos getPos( int index ) {
    switch (index) {
      case 0: return getStartPos();
      case 1: return getEndPos();
      default: throw new IndexOutOfBoundsException( "Index must be 0 or 1, but is="+index );
    }
  }

}
