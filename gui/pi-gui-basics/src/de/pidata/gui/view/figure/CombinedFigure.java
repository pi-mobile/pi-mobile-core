/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.rect.*;

import java.util.ArrayList;
import java.util.List;

public class CombinedFigure extends AbstractFigure {

  private List<ShapePI> shapeList = new ArrayList<ShapePI>();
  private boolean sizeDirty = true;

  public CombinedFigure() {
    super( new SimpleRectDir( 0,0, 0, 0, Rotation.NONE ) );
  }

  private void calcSize() {
    double height = 0;
    double width = 0;
    for (ShapePI shape : shapeList) {
      Rect shapeBounds = shape.getBounds();
      double w = shapeBounds.getX() + shapeBounds.getWidth();
      if (width < w) {
        width = w;
      }
      double h = shapeBounds.getX() + shapeBounds.getWidth();
      if (height < h) {
        height = h;
      }
    }
    sizeDirty = false;
  }

  @Override
  public Rect getBounds() {
    if (sizeDirty) {
      calcSize();
    }
    return super.getBounds();
  }

  @Override
  public int shapeCount() {
    return shapeList.size();
  }

  @Override
  public ShapePI getShape( int index ) {
    return shapeList.get( index );
  }

  public void addShape( ShapePI shapePI ) {
    this.shapeList.add( shapePI );
    sizeDirty = true;
  }

  public void removeShape( ShapePI shapePI ) {
    this.shapeList.remove( shapePI );
    sizeDirty = true;
  }
}
