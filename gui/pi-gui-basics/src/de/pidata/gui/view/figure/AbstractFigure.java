/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.gui.ui.base.UIPaintAdapter;
import de.pidata.gui.view.base.PaintViewPI;
import de.pidata.models.types.simple.Binary;
import de.pidata.rect.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by pru on 15.06.17.
 */
public abstract class AbstractFigure implements Figure, Binary {

  public static final double HANDLE_SIZE = 8;
  
  private PaintViewPI paintViewPI;
  private FigureHandle[] resizeHandles;
  protected FigureConnector[] figureConnectors;
  protected Rect figureBounds;
  protected SimpleRect oldBounds;
  private int selectionMode = 0;
  private boolean modifiable = true;

  protected AbstractFigure( Rect figureBounds ) {
    this.figureBounds = figureBounds;
  }

  public void setPaintViewPI( PaintViewPI paintViewPI ) {
    this.paintViewPI = paintViewPI;
  }

  public PaintViewPI getPaintView() {
    return paintViewPI;
  }

  public Rect getBounds() {
    return figureBounds;
  }

  public boolean isModifiable() {
    return modifiable;
  }

  public void setModifiable( boolean modifiable ) {
    this.modifiable = modifiable;
  }

  /**
   * Creates 8 rectangles as resize handles around this figure.
   */
  protected void createHandles() {
    resizeHandles = new FigureHandle[8];
    RectRelatedRect baseRect = new RectRelatedRect( figureBounds, 0, -10, 0, -10, HANDLE_SIZE, HANDLE_SIZE );
    resizeHandles[0] = new ResizeHandle( this, HandlePosition.TOP_LEFT, new DeltaRect( baseRect ) );
    baseRect = new RectRelatedRect( figureBounds, 0.5, -5, 0, -10, HANDLE_SIZE, HANDLE_SIZE );
    resizeHandles[1] = new ResizeHandle( this, HandlePosition.TOP_CENTER, new DeltaRect( baseRect ) );
    baseRect = new RectRelatedRect( figureBounds, 1.0, 2, 0, -10, HANDLE_SIZE, HANDLE_SIZE );
    resizeHandles[2] = new ResizeHandle( this, HandlePosition.TOP_RIGHT, new DeltaRect( baseRect ) );
    baseRect = new RectRelatedRect( figureBounds, 0, -10, 0.5, -5, HANDLE_SIZE, HANDLE_SIZE );
    resizeHandles[3] = new ResizeHandle( this, HandlePosition.MIDDLE_LEFT, new DeltaRect( baseRect ) );
    baseRect = new RectRelatedRect( figureBounds, 1.0, 2, 0.5, -5, HANDLE_SIZE, HANDLE_SIZE );
    resizeHandles[4] = new ResizeHandle( this, HandlePosition.MIDDLE_RIGHT, new DeltaRect( baseRect ) );
    baseRect = new RectRelatedRect( figureBounds, 0, -10, 1., 2, HANDLE_SIZE, HANDLE_SIZE );
    resizeHandles[5] = new ResizeHandle( this, HandlePosition.BOTTOM_LEFT, new DeltaRect( baseRect ) );
    baseRect = new RectRelatedRect( figureBounds, 0.5, -5, 1.0, 2, HANDLE_SIZE, HANDLE_SIZE );
    resizeHandles[6] = new ResizeHandle( this, HandlePosition.BOTTOM_CENTER, new DeltaRect( baseRect ) );
    baseRect = new RectRelatedRect( figureBounds, 1.0, 2, 1.0, 2, HANDLE_SIZE, HANDLE_SIZE );
    resizeHandles[7] = new ResizeHandle( this, HandlePosition.BOTTOM_RIGHT, new DeltaRect( baseRect ) );
  }

  protected void updateBounds( double deltaX, double deltaY, double deltaWidth, double deltaHeight ) {
    if (oldBounds == null) {
      oldBounds = new SimpleRect( figureBounds );
    }
    //respect minimum size when updating Bounds
    double newX = oldBounds.getX() + deltaX;
    double newY = oldBounds.getY() + deltaY;
    double newWidth = oldBounds.getWidth() + deltaWidth;
    if (newWidth < getMinWidth()) {
      //respect minWidth
      newWidth = getMinWidth();
      if (deltaX != 0) {
        //correct x if resizing on left side
        newX = oldBounds.getX() + oldBounds.getWidth() - newWidth;
      }
    }
    double newHeight = oldBounds.getHeight() + deltaHeight;
    if (newHeight < getMinHeight()) {
      //respect minHeight
      newHeight = getMinHeight();
      if (deltaY != 0) {
        //correct y if resizing on right side
        newY = oldBounds.getY() + oldBounds.getHeight() - newHeight;
      }
    }
    figureBounds.setX( newX );
    figureBounds.setY( newY );
    figureBounds.setWidth( newWidth );
    figureBounds.setHeight( newHeight );
    paintViewPI.figureModified( this, null );
  }

  protected void updateBoundsNew( double deltaX, double deltaY, double deltaWidth, double deltaHeight ) {
    if (oldBounds == null) {
      oldBounds = new SimpleRect( figureBounds );
    }
    double newDeltaX = deltaX;
    double newDeltaY = deltaY;
    double newDeltaW = deltaWidth;
    double newDeltaH = deltaHeight;
    boolean resizing = deltaWidth != 0 || deltaHeight != 0; //only if width/height changes this is a sign for resizing and not moving!
    if (deltaX != 0) {
      //updating left edge
      double minDeltaLeft = getMinDeltaLeft( resizing );
      double maxDeltaLeft = getMaxDeltaLeft( resizing );
      if (deltaX < minDeltaLeft) {
        newDeltaX = minDeltaLeft;
      }
      else if (deltaX > maxDeltaLeft) {
        newDeltaX = maxDeltaLeft;
      }

      //correct new deltaWidth
      if (deltaWidth != 0) {
        newDeltaW = -newDeltaX;
      }
    }
    else if (deltaWidth != 0) {
      //updating right edge (dx == O)
      double minDeltaRight = getMinDeltaRight( resizing );
      double maxDeltaRight = getMaxDeltaRight( resizing );
      if (deltaWidth < minDeltaRight) {
        newDeltaW = minDeltaRight;
      }
      else if (deltaWidth > maxDeltaRight) {
        newDeltaW = maxDeltaRight;
      }
      //no further correction necessary
    }
    if (deltaY != 0) {
      //updating top edge
      double minDeltaTop = getMinDeltaTop( resizing );
      double maxDeltaTop = getMaxDeltaTop( resizing );
      if (deltaY < minDeltaTop) {
        newDeltaY = minDeltaTop;
      }
      else if (deltaY > maxDeltaTop) {
        newDeltaY = maxDeltaTop;
      }

      //correct new deltaHeight
      if (deltaHeight != 0) {
        newDeltaH = -newDeltaY;
      }
    }
    else if (deltaHeight != 0) {
      //updating bottom edge (dy == 0)
      double minDeltaBottom = getMinDeltaBottom( resizing );
      double maxDeltaBottom = getMaxDeltaBottom( resizing );
      if (deltaHeight < minDeltaBottom) {
        newDeltaH = minDeltaBottom;
      }
      else if (deltaWidth > maxDeltaBottom) {
        newDeltaH = maxDeltaBottom;
      }
      //no further correction necessary
    }
    figureBounds.setX( oldBounds.getX() + newDeltaX );
    figureBounds.setY( oldBounds.getY() + newDeltaY );
    figureBounds.setWidth( oldBounds.getWidth() + newDeltaW );
    figureBounds.setHeight( oldBounds.getHeight() + newDeltaH );
    paintViewPI.figureModified( this, null );
  }

  /**
   * Returns minimum delta Value for left edge (dx of top/bottom left corner)
   * if there is no minimum null will be returned
   * @param resizing
   * @return
   */
  protected double getMinDeltaLeft( boolean resizing ) {
    //default:
    //there is no minimum for left edge
    return 0;
  }

  /**
   * Returns maximum delta Value for left edge (dx of top/bottom left corner)
   * if there is no maximum null will be returned
   * @param resizing
   * @return
   */
  protected double getMaxDeltaLeft( boolean resizing ) {
    //default:
    //when resizing respect minWidth
    //when moving there is no maximum for left edge
    if(resizing) {
      double currentWidth = figureBounds.getWidth();
      double minWidth = getMinWidth();
      return currentWidth - minWidth;
    }
    else {
      return 0;
    }
  }

  /**
   * Returns minimum delta Value for right edge (dx of top/bottom right corner)
   * if there is no minimum null will be returned
   * @param resizing
   * @return
   */
  protected double getMinDeltaRight( boolean resizing ) {
    //default:
    //when resizing respect minWidth
    //when moving there is no maximum for right edge
    if(resizing) {
      double currentWidth = figureBounds.getWidth();
      double minWidth = getMinWidth();
      return currentWidth - minWidth;
    }
    else {
      return 0;
    }
  }

  /**
   * Returns maximum delta Value for right edge (dx of top/bottom right corner)
   * if there is no maximum null will be returned
   * @param resizing
   * @return
   */
  protected double getMaxDeltaRight( boolean resizing ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns minimum delta Value for top edge (dy of top left/right corner)
   * if there is no minimum null will be returned
   * @param resizing
   * @return
   */
  protected double getMinDeltaTop(boolean resizing){
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns maximum delta Value for top edge (dy of top left/right corner)
   * if there is no maximum null will be returned
   * @param resizing
   * @return
   */
  protected double getMaxDeltaTop(boolean resizing){
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns minimum delta Value for bottom edge (dy of bottom left/right corner)
   * if there is no minimum null will be returned
   * @param resizing
   * @return
   */
  protected double getMinDeltaBottom(boolean resizing){
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns maximum delta Value for bottom edge (dy of bottom left/right corner)
   * if there is no maximum null will be returned
   * @param resizing
   * @return
   */
  protected double getMaxDeltaBottom(boolean resizing){
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  protected double getMinWidth() {
    return 0;
  }

  protected double getMinHeight() {
    return 0;
  }

  /**
   * Called by UI when this shape is selected. Shows/hides resize handles
   *
   * @param button
   * @param x
   * @param y
   * @param shapePI
   */
  @Override
  public void onMousePressed( int button, double x, double y, ShapePI shapePI ) {
    oldBounds = new SimpleRect( figureBounds );
    if (shapePI instanceof FigureHandle) {
      // do nothing
    }
    else if (shapePI instanceof FigureConnector) {
      ((FigureConnector) shapePI).mousePressed( button, x, y, shapePI );
    }
    else if (modifiable) {
      if (paintViewPI != null) {
        UIPaintAdapter uiPaintAdapter = (UIPaintAdapter) paintViewPI.getUIAdapter();
        if (selectionMode == 0) {
          showResizeHandles( uiPaintAdapter );
        }
        else {
          selectionMode = 0;
          uiPaintAdapter.setSelectionHandles( null );
        }
      }
    }
  }

  protected void showResizeHandles( UIPaintAdapter uiPaintAdapter ) {
    selectionMode = 1;
    if (resizeHandles == null) {
      createHandles();
    }
    uiPaintAdapter.setSelectionHandles( resizeHandles );
  }

  /**
   * Called by UI while handle is being moved.
   *
   * @param button
   * @param deltaX  horizontal delta to start point of move
   * @param deltaY  vertical delta to start point of move
   * @param shapePI
   */
  @Override
  public void onMouseDragging( int button, double deltaX, double deltaY, ShapePI shapePI ) {
    if (shapePI instanceof FigureHandle) {
      HandlePosition handlePosition = ((FigureHandle) shapePI).getHandlePosition();
      switch (handlePosition) {
        case TOP_LEFT:
          updateBounds( deltaX, deltaY, -deltaX, -deltaY );
          break;
        case TOP_CENTER:
          updateBounds( 0, deltaY, 0, -deltaY );
          break;
        case TOP_RIGHT:
          updateBounds( 0, deltaY, deltaX, -deltaY );
          break;
        case MIDDLE_LEFT:
          updateBounds( deltaX, 0, -deltaX, 0 );
          break;
        case MIDDLE_RIGHT:
          updateBounds( 0, 0, deltaX, 0 );
          break;
        case BOTTOM_LEFT:
          updateBounds( deltaX, 0, -deltaX, deltaY );
          break;
        case BOTTOM_CENTER:
          updateBounds( 0, 0, 0, deltaY );
          break;
        case BOTTOM_RIGHT: {
          updateBounds( 0, 0, deltaX, deltaY );
          break;
        }
      }
    }
    else if (shapePI instanceof FigureConnector) {
      ((FigureConnector) shapePI).mouseDragging( button, deltaX, deltaY, shapePI );
    }
    else {
      showResizeHandles( (UIPaintAdapter) paintViewPI.getUIAdapter() );
      updateBounds( deltaX, deltaY, 0, 0 );
    }
  }

  @Override
  public void onMouseReleased( int button, double x, double y, ShapePI shapePI ) {
    oldBounds = null;
    if (shapePI instanceof FigureHandle) {
      // do nothing
    }
    else if (shapePI instanceof FigureConnector) {
      ((FigureConnector) shapePI).mouseReleased( button, x, y, shapePI );
    }
    else {
      // do nothing
    }
  }

  //--------------------------------------------------------

  @Override
  public int size() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public byte[] getBytes() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void readBytes( InputStream byteStream ) throws IOException {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void writeBytes( OutputStream out ) throws IOException {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }
}
