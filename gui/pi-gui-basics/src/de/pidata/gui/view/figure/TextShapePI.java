/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.rect.Pos;
import de.pidata.rect.Rect;

public class TextShapePI extends RectangularShapePI {

  private String text;

  public TextShapePI( Figure figure, Rect bounds, ShapeStyle style, String text ) {
    super( figure, bounds, style );
    this.text = text;
  }

  /**
   * Returns the type of this shape - used for rendering on UI
   *
   * @return the shape type
   */
  @Override
  public ShapeType getShapeType() {
    return ShapeType.text;
  }

  /**
   * Returns number of positions this shape uses for drawing
   * A Text has one position (top left)
   *
   * @return number of positions
   */
  @Override
  public int posCount() {
    return 1;
  }

  /**
   * Returns position at index, for details see posCount()
   *
   * @param index the position's index
   * @return position at index
   */
  @Override
  public Pos getPos( int index ) {
    if (index == 0) {
      return getBounds();
    }
    else {
      throw new IndexOutOfBoundsException( "Index must be 0, but is="+index );
    }
  }

  public String getText() {
    return text;
  }

  public void setText( String text ) {
    this.text = text;
    appearanceChanged();
  }
}
