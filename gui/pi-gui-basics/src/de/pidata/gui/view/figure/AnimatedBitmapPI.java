/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.qnames.QName;
import de.pidata.rect.Rect;

/**
 *
 *
 * @author dsc
 */
public class AnimatedBitmapPI extends BitmapPI {

  private AnimationType animationType;

  public AnimatedBitmapPI( Figure figure, Rect bounds, QName bitmapID, ShapeStyle style ) {
    this( figure, bounds, bitmapID, AnimationType.RUNNING_TO_RIGHT, style);
  }

  public AnimatedBitmapPI( Figure figure, Rect bounds, QName bitmapID, AnimationType animationType, ShapeStyle style ) {
    this( figure, bounds, bitmapID, BitmapAdjust.SCALE, animationType, style);
  }

  public AnimatedBitmapPI( Figure figure, Rect bounds, QName bitmapID, BitmapAdjust bitmapAdjust, AnimationType animationType, ShapeStyle style ) {
    super( figure, bounds, bitmapID, bitmapAdjust, style);
    this.animationType = animationType;
  }

  public AnimationType getAnimationType() {
    return animationType;
  }

  public void setAnimationType( AnimationType animationType ) {
    if (animationType != this.animationType) {
      this.animationType = animationType;
      appearanceChanged();
    }
  }
}
