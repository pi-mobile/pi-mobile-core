/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure.path;

import de.pidata.rect.Pos;
import de.pidata.rect.Rect;

/**
 * Class describing an arc path element.
 *
 * @author dsc
 */
public class ArcPathElement extends PathElement {

  private Rect bounds;
  private double startAngle;
  private double sweepAngle;

  /**
   * Creates a path element describing an arc on the ellipse defined by it's bounds
   * starting at the given start angle and sweeping over the sweep angle.
   *
   * @param startAngle
   * @param sweepAngle
   */
  public ArcPathElement( Rect bounds, double startAngle, double sweepAngle ) {
    super( PathElementType.arc );
    this.bounds = bounds;
    this.startAngle = startAngle;
    this.sweepAngle = sweepAngle;
  }

  public double getStartAngle() {
    return startAngle;
  }

  public double getSweepAngle() {
    return sweepAngle;
  }

  /**
   * Returns the end position of the arc on the circle defined by it's bounds.
   *
   * @return
   */
  @Override
  public Pos getPos() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns the rectangle defining the ellipse on which the arc is defined.
   *
   * @return
   */
  public Rect getBounds() {
    return bounds;
  }
}
