/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.Platform;
import de.pidata.qnames.QName;

/**
 * ShapeStyle defines color, line style, font, ... for a Shape
 *
 * A ShapeStyle can be used by multiple Shapes, so be careful when changing ShapeStyle's attributes.
 */
public class ShapeStyle {

  public static final double DEFAULT_STROKE_WIDTH = 1.0;
  public static final StrokeType DEFAULT_STROKE_TYPE = StrokeType.CENTERED;
  public static final TextAlign DEFAULT_TEXT_ALIGN = TextAlign.LEFT;

  public static final ComponentColor COLOR_TRANSPARENT = Platform.getInstance().getColor( ComponentColor.TRANSPARENT );
  public static final ComponentColor COLOR_TEMP_TRANSPARENT = Platform.getInstance().getColor( ComponentColor.BLACK );
  private boolean isFillColorTransparent;

  public static final ShapeStyle STYLE_WHITE = new ShapeStyle( ComponentColor.WHITE );
  public static final ShapeStyle STYLE_BLACK = new ShapeStyle( ComponentColor.BLACK );

  protected StyleEventSender eventSender;

  private ComponentColor borderColor;
  private ComponentColor fillColor;
  private ComponentColor shadowColor;
  private String fontName;
  private TextAlign textAlign = DEFAULT_TEXT_ALIGN;
  private double strokeWidth = DEFAULT_STROKE_WIDTH;
  private StrokeType strokeType = DEFAULT_STROKE_TYPE;
  private double shadowRadius;

  protected ShapeStyle() {
  }

  public ShapeStyle( ComponentColor color ) {
    this( color, color );
  }

  public ShapeStyle( ComponentColor color, double strokeWidth ) {
    this( color, color, strokeWidth );
  }

  public ShapeStyle( ComponentColor borderColor, ComponentColor fillColor ) {
    this( borderColor, fillColor, DEFAULT_STROKE_WIDTH );
  }

  public ShapeStyle( ComponentColor borderColor, ComponentColor fillColor, double strokeWidth ) {
    this.borderColor = borderColor;
    this.fillColor = fillColor;
    if (COLOR_TRANSPARENT.equals( fillColor )) {
      isFillColorTransparent = true;
    }
    this.strokeWidth = strokeWidth;
  }

  public ShapeStyle( ComponentColor color, String fontName ) {
    this( color, fontName, DEFAULT_TEXT_ALIGN );
  }

  public ShapeStyle( ComponentColor color, String fontName, TextAlign textAlign ) {
    this( color, color );
    this.fontName = fontName;
    this.textAlign = textAlign;
  }

  public ShapeStyle( QName colorID, double strokeWidth ) {
    this( colorID, colorID, strokeWidth );
  }

  public ShapeStyle( QName borderColorID, QName fillColorID ) {
    this( borderColorID, fillColorID, DEFAULT_STROKE_WIDTH );
  }

  public ShapeStyle( QName colorID ) {
    this( colorID, colorID );
  }
  
  public ShapeStyle( QName borderColorID, QName fillColorID, double strokeWidth ) {
    Platform platform = Platform.getInstance();
    if (borderColorID == null) {
      this.borderColor = null;
    }
    else {
      this.borderColor = platform.getColor( borderColorID );
    }
    if (fillColorID == null) {
      this.fillColor = null;
    }
    else {
      this.fillColor = platform.getColor( fillColorID );
    }
    if (COLOR_TRANSPARENT.equals( fillColor )) {
      isFillColorTransparent = true;
    }
    this.strokeWidth = strokeWidth;
  }

  public ShapeStyle( QName colorID, String fontName ) {
    this( colorID, fontName, DEFAULT_TEXT_ALIGN );
  }

  public ShapeStyle( QName colorID, String fontName, TextAlign textAlign ) {
    this( colorID, colorID );
    this.fontName = fontName;
    this.textAlign = textAlign;
  }

  /**
   * Adds shadow parameters. Does not fire change events.
   *
   * @param shadowColor
   * @param shadowRadius
   */
  public void addShadow( ComponentColor shadowColor, double shadowRadius ) {
    this.shadowColor = shadowColor;
    this.shadowRadius = shadowRadius;
  }

  /**
   * Adds shadow parameters. Does not fire change events.
   *
   * @param shadowColorID
   * @param shadowRadius
   */
  public void addShadow( QName shadowColorID, double shadowRadius ) {
    this.shadowColor = Platform.getInstance().getColor( shadowColorID );
    this.shadowRadius = shadowRadius;
  }

  public ComponentColor getBorderColor() {
    return borderColor;
  }

  public void setBorderColor( ComponentColor borderColor ) {
    ComponentColor oldBorderColor = this.borderColor;
    this.borderColor = borderColor;
    fireColorChanged( oldBorderColor, fillColor, isFillColorTransparent );
  }

  public void setBorderColor( QName borderColorID ) {
    ComponentColor oldBorderColor = this.borderColor;
    Platform platform = Platform.getInstance();
    this.borderColor = platform.getColor( borderColorID );
    fireColorChanged( oldBorderColor, fillColor, isFillColorTransparent );
  }

  public ComponentColor getFillColor() {
    return fillColor;
  }

  public void setFillColor( ComponentColor fillColor ) {
    ComponentColor oldFillColor = this.fillColor;
    boolean oldIsTransparent = isFillColorTransparent;
    this.fillColor = fillColor;
    isFillColorTransparent = (COLOR_TRANSPARENT.equals( fillColor ));
    fireColorChanged( borderColor, oldFillColor, oldIsTransparent);
  }

  public void setFillColor( QName fillColorID ) {
    ComponentColor oldFillColor = this.fillColor;
    Platform platform = Platform.getInstance();
    this.fillColor = platform.getColor( fillColorID );
    boolean oldIsTransparent = isFillColorTransparent;
    isFillColorTransparent = (COLOR_TRANSPARENT.equals( fillColor ));
    fireColorChanged( borderColor, oldFillColor, oldIsTransparent );
  }

  public void setColor( ComponentColor color ) {
    ComponentColor oldBorderColor = this.borderColor;
    ComponentColor oldFillColor = this.fillColor;
    boolean oldIsTransparent = isFillColorTransparent;
    this.borderColor = color;
    this.fillColor = color;
    fireColorChanged( oldBorderColor, oldFillColor, oldIsTransparent );
  }

  public void setColor( QName colorID ) {
    ComponentColor oldBorderColor = this.borderColor;
    ComponentColor oldFillColor = this.fillColor;
    boolean oldIsTransparent = isFillColorTransparent;
    Platform platform = Platform.getInstance();
    this.borderColor = platform.getColor( colorID );
    this.fillColor = platform.getColor( colorID );
    fireColorChanged( oldBorderColor, oldFillColor, oldIsTransparent );
  }

  /**
   * Needed as Android does not start drawing if fill color is transparent.
   *
   * @return
   */
  public boolean isFillColorTransparent() {
    return isFillColorTransparent;
  }

  public String getFontName() {
    return fontName;
  }

  public void setFontName( String fontName ) {
    String oldFontName = this.fontName;
    this.fontName = fontName;
    fireFontChanged( oldFontName, textAlign );
  }

  public TextAlign getTextAlign() {
    return textAlign;
  }

  public void setTextAlign( TextAlign textAlign ) {
    TextAlign oldTextAlign = this.textAlign;
    this.textAlign = textAlign;
    fireFontChanged( fontName, oldTextAlign );
  }

  public double getStrokeWidth() {
    return strokeWidth;
  }

  public void setStrokeWidth( double strokeWidth ) {
    double oldStrokeWidth = this.strokeWidth;
    this.strokeWidth = strokeWidth;
    fireStrokeChanged( strokeType, oldStrokeWidth );
  }

  public StrokeType getStrokeType() {
    return strokeType;
  }

  public void setStrokeType( StrokeType strokeType ) {
    StrokeType oldStrokeType = this.strokeType;
    this.strokeType = strokeType;
    fireStrokeChanged( oldStrokeType, strokeWidth );
  }

  public ComponentColor getShadowColor() {
    return shadowColor;
  }

  public void setShadowColor( ComponentColor shadowColor ) {
    if (shadowColor != this.shadowColor) {
      this.shadowColor = shadowColor;
      fireStyleChanged();
    }
  }

  public void setShadowColor( QName colorID ) {
    ComponentColor newColor = Platform.getInstance().getColor( colorID );
    if (newColor != shadowColor) {
      this.shadowColor = newColor;
      fireStyleChanged();
    }
  }

  public double getShadowRadius() {
    return shadowRadius;
  }

  public void setShadowRadius( double shadowRadius ) {
    if (shadowRadius != this.shadowRadius) {
      this.shadowRadius = shadowRadius;
      fireStyleChanged();
    }
  }

  /**
   * Removes shadow. Fires events.
   */
  public void removeShadow() {
    if (shadowColor != null || shadowRadius > 0) {
      this.shadowColor = null;
      this.shadowRadius = 0;
      fireStyleChanged();
    }
  }

  public StyleEventSender getEventSender() {
    if (eventSender == null) {
      eventSender = new StyleEventSender( this );
    }
    return eventSender;
  }

  private boolean equalColor( ComponentColor oldColor, ComponentColor newColor ) {
    if (oldColor == null) {
      return (newColor == null);
    }
    else {
      return oldColor.equals( newColor );
    }
  }

  protected void fireColorChanged( ComponentColor oldBorderColor, ComponentColor oldFillColor, boolean oldIsTransparent ) {
    if (eventSender != null) {
      if (!equalColor( oldBorderColor, borderColor) || !equalColor( oldFillColor, fillColor) || (oldIsTransparent != isFillColorTransparent)) {
        eventSender.fireColorChanged( oldBorderColor, oldFillColor, oldIsTransparent );
      }
    }
  }

  protected void fireFontChanged( String oldFontName, TextAlign oldTextAlign ) {
    if (eventSender != null) {
      if (!fontName.equals( oldFontName ) || oldTextAlign != textAlign) {
        eventSender.fireFontChanged( oldFontName, oldTextAlign );
      }
    }
  }

  protected void fireStrokeChanged( StrokeType oldStrokeType, double oldStrokeWidth ) {
    if (eventSender != null) {
      if (oldStrokeType != strokeType || (Double.compare( oldStrokeWidth, strokeWidth ) != 0)) {
        eventSender.fireStrokeChanged( oldStrokeType, oldStrokeWidth );
      }
    }
  }

  protected void fireStyleChanged() {
    if (eventSender != null) {
      eventSender.fireStyleChanged();
    }
  }

  /**
   * Creates a copy of the given style.
   *
   * @param shapeStyle
   * @return
   */
  public static ShapeStyle createCopy( ShapeStyle shapeStyle ) {
    ShapeStyle newShapeStyle = new ShapeStyle();
    newShapeStyle.borderColor = shapeStyle.borderColor;
    newShapeStyle.fillColor = shapeStyle.fillColor;
    newShapeStyle.isFillColorTransparent = shapeStyle.isFillColorTransparent;
    newShapeStyle.shadowColor = shapeStyle.shadowColor;
    newShapeStyle.fontName = shapeStyle.fontName;
    newShapeStyle.textAlign = shapeStyle.textAlign;
    newShapeStyle.strokeWidth = shapeStyle.strokeWidth;
    newShapeStyle.strokeType = shapeStyle.strokeType;
    newShapeStyle.shadowRadius = shapeStyle.shadowRadius;
    return newShapeStyle;
  }

}
