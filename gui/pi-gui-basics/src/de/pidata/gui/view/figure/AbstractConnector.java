/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.gui.ui.base.UIShapeAdapter;
import de.pidata.rect.PosDir;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by pru on 15.06.17.
 */
public abstract class AbstractConnector implements FigureConnector {
  
  protected Figure owner;
  protected UIShapeAdapter uiShapeAdapter;
  protected PosDir anchor;
  protected List<Figure> connectionList = new LinkedList<Figure>();

  public AbstractConnector( PosDir anchor, Figure owner ) {
    this.anchor = anchor;
    this.owner = owner;
  }

  @Override
  public Figure getFigure() {
    return owner;
  }

  @Override
  public void attachUI( UIShapeAdapter uiShapeAdapter ) {
    this.uiShapeAdapter = uiShapeAdapter;
  }

  @Override
  public void detachUI() {
    this.uiShapeAdapter = null;
  }

  @Override
  public UIShapeAdapter getUIAdapter() {
    return uiShapeAdapter;
  }

  /**
   * Returns anchor point for given connectionClass or null if owner does
   * not support conenction of connectionClass
   *
   * @param connectionClass the class which will be used for a new connection
   * @return the anchor position
   */
  @Override
  public PosDir getAnchor( Class connectionClass ) {
    return anchor;
  }

  @Override
  public List<Figure> getConnectionList() {
    return connectionList;
  }
}
