/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.rect.Pos;
import de.pidata.rect.Rect;
import de.pidata.rect.RectRelatedPos;
import de.pidata.rect.RelativeRect;

public class EllipseShapePI extends RectangularShapePI {

  private Pos centerPos;

  public EllipseShapePI( Figure figure, Rect bounds ) {
    this( figure, bounds, ShapeStyle.STYLE_BLACK );
  }

  public EllipseShapePI( Figure figure, Rect bounds, ShapeStyle shapeStyle ) {
    super( figure, bounds, shapeStyle );
  }

  public EllipseShapePI( Figure figure, Pos centerPos, double radius, ShapeStyle shapeStyle ) {
    super( figure, createBounds( centerPos, radius, radius ), shapeStyle );
    this.centerPos = centerPos;
  }

  public EllipseShapePI( Figure figure, Pos centerPos, double radiusX, double radiusY, ShapeStyle shapeStyle ) {
    super( figure, createBounds( centerPos, radiusX, radiusY ), shapeStyle );
    this.centerPos = centerPos;
  }

  private static Rect createBounds( Pos centerPos, double radiusX, double radiusY ) {
    return new RelativeRect( centerPos, -radiusX, -radiusY, 2 * radiusX, 2 * radiusY, 0 );
  }

  /**
   * Returns the type of this shape - used for rendering on UI
   *
   * @return the shape type
   */
  @Override
  public ShapeType getShapeType() {
    return ShapeType.ellipse;
  }

  /**
   * Returns number of positions this shape uses for drawing.
   * A ellipse has one position (center).
   *
   * @return number of positions
   */
  @Override
  public int posCount() {
    return 1;
  }

  /**
   * Returns position at index, for details see posCount()
   *
   * @param index the position's index
   * @return position at index
   */
  @Override
  public Pos getPos( int index ) {
    if (index == 0) {
      return getCenterPos();
    }
    else {
      throw new IndexOutOfBoundsException( "Index must be 0, but is=" + index );
    }
  }

  private Pos getCenterPos() {
    if (centerPos == null) {
      centerPos = RectRelatedPos.createCenter( getBounds() );
    }
    return centerPos;
  }
}
