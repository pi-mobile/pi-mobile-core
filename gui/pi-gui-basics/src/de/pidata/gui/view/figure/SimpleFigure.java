/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.rect.*;

/**
 * Created by pru on 09.06.17.
 */
public class SimpleFigure extends AbstractFigure {
  
  private ShapePI shapePI;

  public SimpleFigure( ShapeType shapeType, Pos startPos, Pos endPos, ShapeStyle shapeStyle ) {
    super( new LineRect( startPos, endPos ) );
    if (shapeType == ShapeType.line) {
      shapePI = new LineShapePI( this, startPos, endPos, shapeStyle );
    }
    else {
      throw new RuntimeException( "TODO - unsupported ShapeType="+shapeType );
    }
  }

  public SimpleFigure( ShapeType shapeType, Rect figureBounds, ShapeStyle shapeStyle ) {
    super( figureBounds );
    if (shapeType == ShapeType.rect) {
      shapePI = new RectanglePI( this, getBounds(), shapeStyle );
    }
    else if (shapeType == ShapeType.ellipse) {
      shapePI = new EllipseShapePI( this, getBounds(), shapeStyle );
    }
    else {
      throw new RuntimeException( "TODO - unsupported ShapeType="+shapeType );
    }

    //--- Connectors
    figureConnectors = new FigureConnector[1];
    RectRelatedRect baseRect = new RectRelatedRect( getBounds(), 1.0, -8, 0.5, -5, 10, 10 );
    DeltaRect shapeRect = new DeltaRect( baseRect );
    RectRelatedPos anchor = new RectRelatedPos( shapeRect, 0.5, 0, 0.5, 0  );
    figureConnectors[0] = new SimpleConnector( this, shapeRect, anchor );
  }

  public SimpleFigure( ShapeType shapeType, Rect figureBounds, ShapeStyle textStyle, String text ) {
    super( figureBounds );
    if (shapeType == ShapeType.text) {
      shapePI = new TextShapePI( this, figureBounds, textStyle, text );
    }
    else {
      throw new RuntimeException( "TODO - unsupported ShapeType="+shapeType );
    }
  }

  public SimpleFigure( ShapeType shapeType, Rect figureBounds, ShapeStyle lineStyle, Pos[] positions ) {
    super( figureBounds );
    if (shapeType == ShapeType.path) {
      shapePI = new PolyLineShapePI( this, figureBounds, lineStyle, positions );
    }
    else {
      throw new RuntimeException( "TODO - unsupported ShapeType="+shapeType );
    }
  }

  @Override
  public int shapeCount() {
    if (figureConnectors == null) {
      return 1;
    }
    else {
      return figureConnectors.length + 1;
    }
  }

  @Override
  public ShapePI getShape( int index ) {
    if (index == 0) {
      return shapePI;
    }
    else {
      return figureConnectors[ index - 1 ];
    }
  }

  public int getFigureConnectorCount() {
    if (figureConnectors == null) {
      return 0;
    }
    else {
      return figureConnectors.length;
    }
  }

  public FigureConnector getFigureConnector( int index ) {
    return figureConnectors[index];
  }
}
