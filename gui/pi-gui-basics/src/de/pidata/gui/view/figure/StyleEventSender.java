/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.log.Logger;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author dsc
 */
public class StyleEventSender {

  private ShapeStyle owner;
  private List<StyleEventListener> listenerList;

  public StyleEventSender( ShapeStyle owner ) {
    this.owner = owner;
  }

  public Object getOwner() {
    return owner;
  }

  public void addListener( StyleEventListener listener)  {
    if (this.listenerList == null) {
      this.listenerList = new LinkedList<StyleEventListener>();
    }
    this.listenerList.add(listener);
  }

  public void removeListener( StyleEventListener listener ) {
    if (this.listenerList != null) {
      this.listenerList.remove(listener);
    }
  }

  public void fireColorChanged( ComponentColor oldBorderColor, ComponentColor oldFillColor, boolean oldIsTransparent ) {
    if (this.listenerList != null) {
      // Copying listeners prevents problems if listeners are added or removed while send event loop
      StyleEventListener[] listenerArr = new StyleEventListener[listenerList.size()];
      listenerList.toArray(listenerArr);
      StyleEventListener listener;
      for (int i = 0; i < listenerArr.length; i++) {
        listener = listenerArr[i];
        try {
          listener.colorChanged( owner, oldBorderColor, oldFillColor, oldIsTransparent );
        }
        catch (Exception ex) {
          Logger.error( "Error while event processing", ex );
        }
      }
    }
  }

  public void fireFontChanged( String oldFontName, TextAlign oldTextAlign ) {
    if (this.listenerList != null) {
      // Copying listeners prevents problems if listeners are added or removed while send event loop
      StyleEventListener[] listenerArr = new StyleEventListener[listenerList.size()];
      listenerList.toArray(listenerArr);
      StyleEventListener listener;
      for (int i = 0; i < listenerArr.length; i++) {
        listener = listenerArr[i];
        try {
          listener.fontChanged( owner, oldFontName, oldTextAlign );
        }
        catch (Exception ex) {
          Logger.error( "Error while event processing", ex );
        }
      }
    }
  }

  public void fireStrokeChanged( StrokeType oldStrokeType, double oldStrokeWidth ) {
    if (this.listenerList != null) {
      // Copying listeners prevents problems if listeners are added or removed while send event loop
      StyleEventListener[] listenerArr = new StyleEventListener[listenerList.size()];
      listenerList.toArray(listenerArr);
      StyleEventListener listener;
      for (int i = 0; i < listenerArr.length; i++) {
        listener = listenerArr[i];
        try {
          listener.strokeChanged( owner, oldStrokeType, oldStrokeWidth );
        }
        catch (Exception ex) {
          Logger.error( "Error while event processing", ex );
        }
      }
    }
  }

  public void fireStyleChanged() {
    if (this.listenerList != null) {
      // Copying listeners prevents problems if listeners are added or removed while send event loop
      StyleEventListener[] listenerArr = new StyleEventListener[listenerList.size()];
      listenerList.toArray(listenerArr);
      StyleEventListener listener;
      for (int i = 0; i < listenerArr.length; i++) {
        listener = listenerArr[i];
        try {
          listener.styleChanged( owner );
        }
        catch (Exception ex) {
          Logger.error( "Error while event processing", ex );
        }
      }
    }
  }
}
