/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.gui.ui.base.UIShapeAdapter;
import de.pidata.rect.Pos;
import de.pidata.rect.Rect;

/**
 * A ShapePI is the common parent of shapes like rectangle, ellipse or line.
 * A ShapePI must always be part of a Figure in order to get visible on UI.
 */
public interface ShapePI {

  /**
   * Returns the type of this shape - used for rendering on UI
   *
   * @return the shape type
   */
  public ShapeType getShapeType();

  public Figure getFigure();

  public Rect getBounds();

  /**
   * Returns number of positions this shape uses for drawing:
   * <UL>
   *   <LI>a rectangle has one position (top left)</LI>
   *   <LI>a line has a start and end position</LI>
   *   <LI>a polyline has one positions for start, each bend and end</LI>
   *   <LI>an ellipse has one position (center)</LI>
   * </UL>
   *
   * @return number of positions
   */
  public int posCount();

  /**
   * Returns position at index, for details see posCount()
   *
   * @param index the position's index
   * @return position at index
   */
  public Pos getPos( int index );

  /**
   * Returns this shape's style: color, line type, font, ...
   *
   * @return shape style, never null
   */
  public ShapeStyle getShapeStyle();

  /**
   * Returns the text this shape shows or null if none.
   *
   * @return this shape's text
   */
  public String getText();

  public void attachUI( UIShapeAdapter uiShapeAdapter );

  public void detachUI();

  public UIShapeAdapter getUIAdapter();
}
