/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.gui.component.base.ComponentColor;

/**
 *
 * @author dsc
 */
public class AnimationStyle extends ShapeStyle {

  public static final AnimationStyle DEFAULT_ANIMATION_STYLE = new AnimationStyle();

  private int animationDelay = 50;
  private double offsetRatio = 0.25;

  /**
   * Creates a style providing default values for animation.
   */
  private AnimationStyle() {
    super( ComponentColor.TRANSPARENT );
  }

  public AnimationStyle( int animationDelay, double offsetRatio ) {
    super( ComponentColor.TRANSPARENT );
    this.animationDelay = animationDelay;
    this.offsetRatio = offsetRatio;

    // prevent standstill
    if (this.offsetRatio <= 0) {
      this.offsetRatio = 1;
    }
  }

  public int getAnimationDelay() {
    return animationDelay;
  }

  public double getOffsetRatio() {
    return offsetRatio;
  }
}
