/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.rect.*;

import java.util.List;

public class CircleGradientStyle extends ShapeStyle implements PosSizeEventListener {

  private Pos centerPos;
  private double radius;
  private List<ComponentColor> colorList;
  private List<Double> offsetList;
  private GradientMode gradientMode;

  public CircleGradientStyle( Pos centerPos, double radius, List<ComponentColor> colorList, List<Double> offsetList, GradientMode gradientMode ) {
    if (colorList.size() != offsetList.size()) {
      throw new IllegalArgumentException( "ColorList and offsetList must have same size" );
    }
    this.centerPos = centerPos;
    centerPos.getEventSender().addListener( this );

    this.radius = radius;
    this.colorList = colorList;
    this.offsetList = offsetList;
    this.gradientMode = gradientMode;
  }

  public Pos getCenterPos() {
    return centerPos;
  }

  public double getRadius() {
    return radius;
  }

  public void setRadius( double radius ) {
    if (radius != this.radius) {
      this.radius = radius;
      fireStyleChanged();
    }
  }

  public List<ComponentColor> getColorList() {
    return colorList;
  }

  public void setColor( int index, ComponentColor color ) {
    ComponentColor oldColor = colorList.get( index );
    if (color != oldColor) {
      colorList.set( index, color );
      fireStyleChanged();
    }
  }

  public List<Double> getOffsetList() {
    return offsetList;
  }

  public GradientMode getGradientMode() {
    return gradientMode;
  }

  @Override
  public void posChanged( Pos pos, double oldX, double oldY ) {
    fireStyleChanged();
  }

  @Override
  public void sizeChanged( Rect rect, double oldWidth, double oldHeight ) {
    fireStyleChanged();
  }

  @Override
  public void rotationChanged( PosDir posDir, Rotation oldRotation ) {
    fireStyleChanged();
  }
}
