/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.rect.Pos;
import de.pidata.rect.Rect;

/**
 * Created by pru on 09.06.17.
 */
public class RectanglePI extends RectangularShapePI {

  private double cornerRadius = 0;

  public RectanglePI( Figure figure, Rect bounds ) {
    this( figure, bounds, ShapeStyle.STYLE_BLACK );
  }

  public RectanglePI( Figure figure, Rect bounds, ShapeStyle style ) {
    super( figure, bounds, style );
  }

  public RectanglePI (Figure figure, Rect bounds, double cornerRadius, ShapeStyle style){
    super(figure, bounds, style);
    this.cornerRadius = cornerRadius;
  }

  /**
   * Returns the type of this shape - used for rendering on UI
   *
   * @return the shape type
   */
  @Override
  public ShapeType getShapeType() {
    return ShapeType.rect;
  }

  /**
   * Returns number of positions this shape uses for drawing.
   * A a rectangle has one position (top left)
   *
   * @return number of positions: 1
   */
  @Override
  public int posCount() {
    return 1;
  }

  /**
   * Returns position at index, for details see posCount()
   *
   * @param index the position's index
   * @return position at index
   */
  @Override
  public Pos getPos( int index ) {
    if (index == 0) {
      return getBounds();
    }
    else {
      throw new IndexOutOfBoundsException( "Index must be 0, but is="+index );
    }
  }

  public double getCornerRadius() {
    return cornerRadius;
  }
}
