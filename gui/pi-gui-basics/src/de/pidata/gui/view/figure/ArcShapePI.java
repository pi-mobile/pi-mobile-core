/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.rect.Rect;

/**
 *
 * @author dsc
 */
public class ArcShapePI extends EllipseShapePI {

  private double startAngle;
  private double sweepAngle;
  private boolean useCenter = true;

  public ArcShapePI( Figure figure, Rect bounds, double startAngle, double sweepAngle, boolean useCenter ) {
    this( figure, bounds, startAngle, sweepAngle );
    this.useCenter = useCenter;
  }

  public ArcShapePI( Figure figure, Rect bounds, double startAngle, double sweepAngle ) {
    super( figure, bounds );
    this.startAngle = startAngle;
    this.sweepAngle = sweepAngle;
  }

  public ArcShapePI( Figure figure, Rect bounds, double startAngle, double sweepAngle, boolean useCenter, ShapeStyle shapeStyle ) {
    this( figure, bounds, startAngle, sweepAngle, shapeStyle );
    this.useCenter = useCenter;
  }

  public ArcShapePI( Figure figure, Rect bounds, double startAngle, double sweepAngle, ShapeStyle shapeStyle ) {
    super( figure, bounds, shapeStyle );
    this.startAngle = startAngle;
    this.sweepAngle = sweepAngle;
  }

  /**
   * Returns the type of this shape - used for rendering on UI
   *
   * @return the shape type
   */
  @Override
  public ShapeType getShapeType() {
    return ShapeType.arc;
  }

  public boolean getUseCenter() {
    return useCenter;
  }

  public double getStartAngle() {
    return startAngle;
  }

  public void setStartAngle( double startAngle ) {
    this.startAngle = startAngle;
    appearanceChanged();
  }

  public double getSweepAngle() {
    return sweepAngle;
  }

  public void setSweepAngle( double sweepAngle ) {
    this.sweepAngle = sweepAngle;
    appearanceChanged();
  }
}
