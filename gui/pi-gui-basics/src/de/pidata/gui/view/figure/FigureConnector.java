/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.rect.PosDir;

import java.util.List;

/**
 * Created by pru on 11.06.17.
 */
public interface FigureConnector extends ShapePI {

  /**
   * Returns anchor point for given connectionClass or null if owner does
   * not support conenction of connectionClass
   *
   * @param connectionClass the class which will be used for a new connection
   * @return the anchor position
   */
  PosDir getAnchor( Class connectionClass );

  List<Figure> getConnectionList();

  void mousePressed( int button, double x, double y, ShapePI shapePI );

  /**
   * Called by UI while handle is being moved.
   * @param deltaX  horizontal delta to start point of move
   * @param deltaY  vertical delta to start point of move
   */
  void mouseDragging( int button, double deltaX, double deltaY, ShapePI shapePI );

  void mouseReleased( int button, double x, double y, ShapePI shapePI );
}
