/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.gui.view.figure.path.LinePathElement;
import de.pidata.gui.view.figure.path.PathElement;
import de.pidata.gui.view.figure.path.PathStartElement;
import de.pidata.rect.Pos;
import de.pidata.rect.Rect;

/**
 *
 * @author dsc
 */
public class PathShapePI extends RectangularShapePI {

  private PathElement[] pathElementDefinition;

  public PathShapePI( Figure figure, Rect bounds, ShapeStyle style, Pos[] positions ) {
    super( figure, bounds, style );

    // if no other path information is given, create lines
    pathElementDefinition = new PathElement[positions.length];
    pathElementDefinition[0] = new PathStartElement( positions[0] );
    for (int i=1; i<positions.length; i++) {
      pathElementDefinition[i] = new LinePathElement( positions[i] );
    }
  }

  public PathShapePI( Figure figure, Rect bounds, ShapeStyle style, PathElement[] pathElementDefinition ) {
    this( figure, bounds, style );
    this.pathElementDefinition = pathElementDefinition;
  }

  protected PathShapePI( Figure figure, Rect bounds, ShapeStyle style ) {
    super( figure, bounds, style );
  }

  /**
   * Returns the type of this shape - used for rendering on UI
   *
   * @return the shape type
   */
  @Override
  public ShapeType getShapeType() {
    return ShapeType.path;
  }


  /**
   * Returns number of positions this shape uses for drawing:
   * <UL>
   * <LI>a rectangle has one position (top left)</LI>
   * <LI>a line has a start and end position</LI>
   * <LI>a polyline has one positions for start, each bend and end</LI>
   * <LI>an ellipse has one position (center)</LI>
   * </UL>
   *
   * @return number of positions
   */
  @Override
  public int posCount() {
    return pathElementDefinition.length;
  }

  /**
   * Returns position at index, for details see posCount()
   *
   * @param index the position's index
   * @return position at index
   */
  @Override
  public Pos getPos( int index ) {
    return pathElementDefinition[index].getPos();
  }

  /**
   * Returns the array containing the path elements defining the path.
   *
   * @return
   */
  public PathElement[] getPathElementDefinition() {
    return pathElementDefinition;
  }

}
