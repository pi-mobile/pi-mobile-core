/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.gui.view.base.PaintViewPI;
import de.pidata.rect.Rect;

/**
 * A Figure is an element of a drawing which can be selected, resized, rotated, ... by the user.
 *
 * A Figure is composed of one or more Shape objects.
 *
 * The pattern for this class and its partner classes is inspired by JHotDraw.
 */
public interface Figure {

  /**
   * Sets the PaintView used to make this Figure visible.
   *
   * @param paintViewPI the PaintView for showing this Figure on UI
   */
  void setPaintViewPI( PaintViewPI paintViewPI );

  PaintViewPI getPaintView();

  Rect getBounds();

  int shapeCount();

  ShapePI getShape( int index );

  void onMousePressed( int button, double x, double y, ShapePI shapePI );

  /**
   * Called by UI while handle is being moved.
   * @param deltaX  horizontal delta to start point of move
   * @param deltaY  vertical delta to start point of move
   */
  void onMouseDragging( int button, double deltaX, double deltaY, ShapePI shapePI );

  void onMouseReleased( int button, double x, double y, ShapePI shapePI );
}
