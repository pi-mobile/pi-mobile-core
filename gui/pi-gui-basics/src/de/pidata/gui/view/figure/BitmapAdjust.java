/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

/**
 * Depends on {@link AnimationType}:
 * If animation takes place in the horizontal, this describes the vertical alignment.
 *
 * @author dsc
 */
public enum BitmapAdjust {
  /** image will be scaled to fill the opposite to the animation direction */
  SCALE,
  /** image will be centered opposite to the animation direction */
  CENTER,
  /** image will be scaled and centered opposite to the animation direction */
  FITCENTER,
  /** image will be put on the upper left corner of the animation bounds */
  NONE
}
