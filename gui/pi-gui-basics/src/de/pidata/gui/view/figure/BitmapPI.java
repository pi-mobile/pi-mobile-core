/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.view.figure;

import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.Platform;
import de.pidata.qnames.QName;
import de.pidata.rect.Pos;
import de.pidata.rect.Rect;

import java.util.Objects;

/**
 *
 * @author dsc
 */
public class BitmapPI extends RectangularShapePI {

  private QName bitmapID;
  private ComponentBitmap bitmap;
  private BitmapAdjust bitmapAdjust = BitmapAdjust.SCALE;

  public BitmapPI( Figure figure, Rect bounds, QName bitmapID, ShapeStyle style ) {
    this( figure, bounds, bitmapID, AnimationType.RUNNING_TO_RIGHT, style);
  }

  public BitmapPI( Figure figure, Rect bounds, QName bitmapID, AnimationType animationType, ShapeStyle style ) {
    this( figure, bounds, bitmapID, BitmapAdjust.SCALE, style);
  }

  public BitmapPI( Figure figure, Rect bounds, QName bitmapID, BitmapAdjust bitmapAdjust, ShapeStyle style ) {
    super( figure, bounds, style);
    this.bitmapID = bitmapID;
    this.bitmap = null;
    this.bitmapAdjust = bitmapAdjust;
  }

  public BitmapPI( Figure figure, Rect bounds, ComponentBitmap bitmap, ShapeStyle style ) {
    super( figure, bounds, style);
    this.bitmapID = null;
    this.bitmap = bitmap;
  }

  /**
   * Returns the type of this shape - used for rendering on UI
   *
   * @return the shape type
   */
  @Override
  public ShapeType getShapeType() {
    return ShapeType.bitmap;
  }

  /**
   * Returns number of positions this shape uses for drawing:
   * <UL>
   * <LI>a rectangle has one position (top left)</LI>
   * <LI>a line has a start and end position</LI>
   * <LI>a polyline has one positions for start, each bend and end</LI>
   * <LI>an ellipse has one position (center)</LI>
   * </UL>
   *
   * @return number of positions
   */
  @Override
  public int posCount() {
    return 1;
  }

  /**
   * Returns position at index, for details see posCount()
   *
   * @param index the position's index
   * @return position at index
   */
  @Override
  public Pos getPos( int index ) {
    if (index == 0) {
      return getBounds();
    }
    else {
      throw new IndexOutOfBoundsException( "Index must be 0, but is="+index );
    }
  }

  public QName getBitmapID() {
    return bitmapID;
  }

  public void setBitmapID( QName bitmapID ) {
    if (!Objects.equals( bitmapID, this.bitmapID )) {
      this.bitmapID = bitmapID;
      this.bitmap = null;
      appearanceChanged();
    }
  }

  public BitmapAdjust getBitmapAdjust() {
    return bitmapAdjust;
  }

  public ComponentBitmap getBitmap() {
    if (this.bitmapID == null) {
      return this.bitmap;
    }
    else {
      return Platform.getInstance().getBitmap( this.bitmapID );
    }
  }
}
