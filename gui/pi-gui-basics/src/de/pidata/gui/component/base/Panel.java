/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.gui.layout.Layoutable;
import de.pidata.gui.layout.Layouter;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;

public class Panel extends AbstractContainer {
  private static final boolean DEBUG = false;
  private static final String ME = "Panel";

  private Layouter[] layouter = new Layouter[2];

  private Border border = null;
  private Scroller[] scroller = new Scroller[2];
  private int lastScrollPosX = 0;
  private int lastScrollPosY = 0;

  public Panel(Layouter layouterX, Layouter layouterY, boolean scrollHor, boolean scrollVert, short borderSize) {
    super();
    this.layouter[Direction.X.getIndex()] = layouterX;

    // if there is no own layouter, get it from parent later
    if (layouterX != null) {
      layouterX.setTarget(this);
    }

    this.layouter[Direction.Y.getIndex()] = layouterY;

    // if there is no own layouter, get it from parent later
    if (layouterY != null) {
      layouterY.setTarget(this);
    }

    if (scrollHor) {
      scroller[Direction.X.getIndex()] = createScroller(Direction.X);
    }
    if (scrollVert) {
      scroller[Direction.Y.getIndex()] = createScroller(Direction.Y);
    }

    if (borderSize > 0) {
      setBorder( new EmptyBorder( borderSize, borderSize, borderSize, borderSize ) );
    }  
  }

  /**
   * Updates the value at posX, posY. The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX  horizontal position, starting from 0
   * @param posY  vertical position, starting from 0
   * @param valueType
   * @param value the new value
   */
  public void updateValue( short posX, short posY, Type valueType, Object value ) {
    // do nothing
  }

  /**
   * Inserts the value before posX, posY. The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX  horizontal position, starting from 0, to insert after last give posX = last+1
   * @param posY  vertical position, starting from 0, to insert after last give posY = last+1
   * @param valueType
   * @param value the value to be inserted
   */
  public void insertValue( short posX, short posY, SimpleType valueType, Object value ) {
    // do nothing
  }

  /**
   * Sets the cursor position to (posX,posY). The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX horizontal position, starting from 0
   * @param posY vertical position, starting from 0
   */
  public void setCursorPos( short posX, short posY ) {
    // do nothing
  }

  public Scroller createScroller (Direction direction) {
    Scroller scroller = Platform.getInstance().getComponentFactory().createScrollbar(direction);
    scroller.setLayoutParent(this);
    scroller.getComponentState().setVisible(false);
    return scroller;
  }

  public Border getBorder() {
    return border;
  }

  public void setBorder (Border border) {
    this.border = border;
  }

  public Scroller getScroller (Direction direction) {
    if (scroller == null) return null;
    else return scroller[direction.getIndex()];
  }

  /**
   * This method is called whenever a state change occured. Each sub class
   * has to decide what to do, e.g. repaint its content
   */
  public void stateChanged( int oldState, int newState ) {
    // do nothing
  }

  /**
   * Sends a paint event
   */
  public void repaint() {
    Scroller scrollerX = getScroller(Direction.X);
    Scroller scrollerY = getScroller(Direction.Y);
    if (scrollerX != null) {
      if (scrollerX.getValue() != this.lastScrollPosX) {
        this.lastScrollPosX = moveContent(scrollerX, this.lastScrollPosX);
      }
    }
    if (scrollerY != null) {
      if (scrollerY.getValue() != this.lastScrollPosY) {
        this.lastScrollPosY = moveContent(scrollerY, this.lastScrollPosY);
      }
    }
    super.repaint();
  }

  /**
   * Move all components within the container according to the moved value of the scroll bubble.
   */
  protected int moveContent(Scroller scroller, int lastValue) {
    Direction direction = scroller.getDirection();
    Layoutable component;
    int newValue = scroller.getValue();

    int deltaPos;
    if (lastValue >= 0) deltaPos = lastValue - newValue;
    else deltaPos = -newValue;

    if (deltaPos != 0) {
      //----- move all components of the container
      short compPos;
      int cellCount = childCount();
      for (int i=0; i<cellCount; i++) {
        component = getChild(i);
        compPos = (short) (component.getPos(direction) + deltaPos);
        component.setPos(direction, compPos);
      }
    }
    return newValue;
  }


  /**
   * Called for painting this component
   * @param compGraph the graphics context to use for painting
   */
  public void doPaintImpl (ComponentGraphics compGraph) {
    short width = this.getSizeLimit( Direction.X ).getCurrent();
    short height = this.getSizeLimit( Direction.Y ).getCurrent();
    Position translate = new Position((short) 0,(short) 0);
    Rect oldClip = new Rect();
    compGraph.getClip(oldClip);
    Scroller scrollerX = getScroller(Direction.X);
    Scroller scrollerY = getScroller(Direction.Y);

    if (!allowPaint()) {
      return;
    }

    //----- Draw background

    // paint border if exists
    if (border != null) {
      compGraph.paintBackground(getBackground(), border.getLeft(),  border.getTop(),
                                width - (border.get2BorderSize(Direction.X)), height - (border.get2BorderSize(Direction.Y)));
      border.doPaint(compGraph, width, height);
    }
    else {
      compGraph.paintBackground(getBackground(), 0,  0, width, height);
    }
    // paint scroller if exist
    if (scrollerX != null && scrollerX.isVisible()) {
      doPaintScroller(scrollerX, compGraph, height);
    }
    if (scrollerY != null && scrollerY.isVisible()) {
      doPaintScroller(scrollerY, compGraph, width);
    }

    adaptContentGraphics(compGraph, translate);
    doPaintChildren( this, compGraph );

    compGraph.setClip(oldClip);
    compGraph.translate(translate.getX(), translate.getY());
  }

  /**
   * Translate and clip graph for containers content
   */
  public void adaptContentGraphics (ComponentGraphics compGraph, Position translation) {
    short width = this.getSizeLimit( Direction.X ).getCurrent();
    short height = this.getSizeLimit( Direction.Y ).getCurrent();
    Scroller scrollerX = getScroller(Direction.X);
    Scroller scrollerY = getScroller(Direction.Y);

    // adjust graphics: no action for border, but clip graphics for scroller
    // because children's position is relative to upper left edge of the container!
    if (scrollerX != null && scrollerX.isVisible()) {
      height -= scrollerX.getSizeLimit(Direction.Y).getCurrent();
    }
    if (scrollerY != null && scrollerY.isVisible()) {
      width -= scrollerY.getSizeLimit(Direction.X).getCurrent();
    }

    compGraph.clipRect((short) 0, (short) 0, width, height);
  }

  /**
   * paint scroller in one direction
   * @param scroller
   * @param compGraph
   * @param size
   */
  private void doPaintScroller (Scroller scroller, ComponentGraphics compGraph, short size) {
    short scrollerWidth = scroller.getSizeLimit(Direction.X).getCurrent();
    short scrollerHeight = scroller.getSizeLimit(Direction.Y).getCurrent();
    short offsetX = 0;
    short offsetY = 0;

    if (scroller.getDirection() == Direction.X) {
      offsetY = (short) (size - scrollerHeight);
      if (border != null) {
        offsetX = border.getLeft();
      }
    } else {
      offsetX = (short) (size - scrollerWidth);
      if (border != null) {
        offsetY = border.getTop();
      }
    }
    compGraph.translate( offsetX, offsetY);
    Rect oldClip = new Rect();
    compGraph.getClip(oldClip);
    compGraph.clipRect((short) 0, (short) 0, scrollerWidth, scrollerHeight);

    scroller.doPaint(compGraph);

    compGraph.setClip(oldClip);
    compGraph.translate((short) -offsetX,  (short) -offsetY);
  }

  /**
   * Calculates this component's min, max and pref sizes and
   * stores them in sizeLimit
   */
  public void calcSizes( Direction direction, SizeLimit sizeLimit ) {
    Layouter layouter = this.getLayouter(direction);
    if (layouter != null) {
      layouter.calcSizes(sizeLimit);
    }
/* TODO   Border border = getBorder();
    if (border != null) {
      int bSize = border.get2BorderSize( direction );
      int min =  sizeLimit.getMin() + bSize; 
      int pref = sizeLimit.getPref() + bSize;
      int max  = sizeLimit.getMax() + bSize;
      sizeLimit.set( min, pref, max );
    } */

    getPaintState(direction).doPaintState(PaintState.STATE_SIZES_CALC);
  }

  /**
   * Gets the layouter of this panel. If the panel does not have
   * an own layouter, this is null. We will use the layouter of its parent.
   * The layouter which is responsible for this panel has to layout all its subsequent
   * containers.
   * @param direction
   * @return the layouter for this panel or null
   */
  public Layouter getLayouter(Direction direction) {
    return this.layouter[direction.getIndex()];
  }

  /**
   * delegate layout calculation to layouter
   * @param direction
   */
  public void doLayout( Direction direction ) {
    Layouter layouter = this.getLayouter(direction);
    Border border = getBorder();
    short offset = 0;
    short contentSize = getSizeLimit(direction).getCurrent();

    if (border != null) {
      offset = border.getOffset(direction);
      contentSize -= border.get2BorderSize(direction);
    }
    if (direction == Direction.X) offset -= this.lastScrollPosX;
    else offset -= this.lastScrollPosY;

    if (layouter != null) {
      layouter.doLayout(offset, contentSize);
      doScrollerLayout(direction);
    }
    getPaintState(direction).doPaintState(PaintState.STATE_LAYOUTED);
  }

  public void doScrollerLayout(Direction direction) {
    PaintState paintState = getPaintState(direction);
    if (!paintState.hasPaintStateReached(PaintState.STATE_BOUNDS_SET)) {
      return;
    }

    short childrenSize = getChildrenSize(direction);
    short visibleContentSize = clientSize(direction);
    Scroller scroller = getScroller(direction);
    if ((scroller != null)) {
      if (visibleContentSize > childrenSize) visibleContentSize = childrenSize;
      scroller.setValueConstraints(0, childrenSize, visibleContentSize, 10, visibleContentSize / 2);
    }
    Scroller scrollerX = getScroller(Direction.X);
    Scroller scrollerY = getScroller(Direction.Y);
    doOneScrollerLayout( scrollerX );
    doOneScrollerLayout( scrollerY );
  }

  /**
   * Set one scrollers sizes and position in both directions.
   * @param scroller
   */
  private void doOneScrollerLayout( Scroller scroller) {
    //---- set horizontal scroller
    if ((scroller != null) && scroller.getComponentState().isVisible()) {
      Direction scrollDir = scroller.getDirection();
      Direction scrollDirOpp = scrollDir.getOpposite();
      short containerSize = getSizeLimit(scrollDir).getCurrent();
      short containerSizeOpp = getSizeLimit(scrollDirOpp).getCurrent();
      Scroller scrollerOpp = getScroller(scrollDirOpp);
      short scrollbarSize;

      short borderOffset = 0;
      Border border = getBorder();
      if (border != null) {
        borderOffset = border.getOffset(scrollDirOpp);
        containerSize -= borderOffset; // todo: warum nur einmal?
      }

      scrollbarSize = containerSize;
      if ((scrollerOpp != null) && scrollerOpp.getComponentState().isVisible()) {
        scrollbarSize -= scrollerOpp.getSizeLimit(scrollDir).getPref();
      }

      scroller.setPos(scrollDir, borderOffset);
      scroller.getSizeLimit(scrollDir).setCurrent(scrollbarSize);
      scroller.doLayout(scrollDir);

      scroller.setPos(scrollDirOpp, (short) (containerSizeOpp - scroller.getSizeLimit(scrollDirOpp).getPref()));
      scroller.getSizeLimit(scrollDirOpp).setCurrent(scroller.getSizeLimit(scrollDirOpp).getPref());

      if ((scrollerOpp != null) && scrollerOpp.getComponentState().isVisible()) {
        scrollerOpp.doLayout(scrollDirOpp);
      }
    }
  }

  /**
   * Determine if component does layout for itself.
   * This method returns true for all normal components, just for container without
   * own layouter this may return false.
   * @param direction
   * @return true; only panel without own layouter may return false.
   */
  public boolean doesLayout (Direction direction) {
    return layouter[direction.getIndex()] != null;
  }

  /**
   * Determine layout size and toggles visibility of scroller if necessary
   * @param direction
   * @param targetSize      the size of target's painting area
   * @param minSizeChilds
   * @param prefSizeChilds
   * @return the size available for layouting target's children
   */
  public short getLayoutSize( Direction direction, short targetSize, short minSizeChilds, short prefSizeChilds ) {
    short layoutSize;
    Scroller scrollerThisDir = getScroller(direction);
    Scroller scrollerOppDir = getScroller(direction.getOpposite());
    boolean scrollerBefore = false;
    boolean scrollerNew = false;

    layoutSize = targetSize;

    // need space for scroller in the opposite direction,
    // so there is less space for the children
    if (scrollerOppDir != null && scrollerOppDir.getComponentState().isVisible()) {
      layoutSize -= scrollerOppDir.getMinScrollerWidth();
    }

    // decide if scroller in actual direction is necessary
    if(scrollerThisDir != null) {
      scrollerBefore = scrollerThisDir.getComponentState().isVisible();
      if (layoutSize < minSizeChilds) {
        // not enough space for minimum size: show scrollbar; minimum size may be minimum scrollbar
        layoutSize = minSizeChilds;
        scrollerThisDir.getComponentState().setVisible(true);
      } else {
        // enough space: show preferred sizes, no scrollbar needed
        scrollerThisDir.getComponentState().setVisible(false);
        scrollerThisDir.setValueFit(0);
      }
      scrollerNew = scrollerThisDir.getComponentState().isVisible();
    }

    // if scroller has newly been added, layout in opposite direction has to be done
    if (scrollerBefore != scrollerNew) {
      if (getPaintState(direction.getOpposite()).hasPaintStateReached(PaintState.STATE_LAYOUTED)) {
        getPaintState(direction.getOpposite()).undoPaintState(PaintState.STATE_LAYOUTED);
        doLayout(direction.getOpposite());
      }
    }

    return layoutSize;
  }

  public Component findComponentAt(Position pos){
    short x = pos.getX();
    short y = pos.getY();
    short xOffset = x;
    short yOffset = y;
    Scroller scroller;
    Popup activePopup;

    // check if scroller is hit
    scroller = getScroller(Direction.X);
    if (scroller != null && scroller.isVisible()) {
      if ( containsPos( scroller, Direction.X, x ) && containsPos( scroller, Direction.Y, yOffset )) {
        pos.translate(scroller.getPos(Direction.X), scroller.getPos(Direction.Y));
        return scroller;
      }
    }
    scroller = getScroller(Direction.Y);
    if (scroller != null && scroller.isVisible()) {
      if ( containsPos( scroller, Direction.X, xOffset ) && containsPos( scroller, Direction.Y, y)) {
        pos.translate(scroller.getPos(Direction.X), scroller.getPos(Direction.Y));
        return scroller;
      }
    }

    //search in container
    return super.findComponentAt(pos);
  }
}
