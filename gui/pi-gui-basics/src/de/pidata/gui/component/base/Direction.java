/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

/**
 * Created by IntelliJ IDEA.
 * User: Doris
 * Date: 09.06.2004
 * Time: 15:38:06
 * To change this template use File | Settings | File Templates.
 */
public class Direction {
  public static final Direction X = new Direction(0);
  public static final Direction Y = new Direction(1);

  private int index;

  private Direction(int index) {
    this.index = index;
  }

  public int getIndex() {
    return index;
  }

  public Direction getOpposite () {
    if (index == Direction.X.getIndex()) {
      return Direction.Y;
    } else {
      return Direction.X;
    }
  }
}
