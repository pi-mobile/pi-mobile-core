/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.gui.event.InputManager;
import de.pidata.gui.event.TextWrapListener;
import de.pidata.gui.guidef.InputMode;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;


public class TextLine extends AbstractComponent {

  private static final boolean DEBUG = false;

  public static final int CURSOR_TOP = 5;
  public static final int CURSOR_BOTTOM = 6;
  public static final int CURSOR_LEFT = 7;
  public static final int CURSOR_RIGHT = 8;

  //----- Layout constants
  public static final int CURSOR_SIZE = 6;
//  public static final int DIR_CURSOR_WIDTH = 7;
//  public static final int DIR_CURSOR_HEIGHT = 21;
  public static final int DIR_CURSOR_WIDTH = 5;
  public static final int DIR_CURSOR_HEIGHT = 14;
  public static final int DEFAULT_PREF_SIZE = 200;

  private static final char[] EMPTY_CHARS = new char[0];

  /** preferrred width */
  private short prefWidth = -1;

  private int textOriginY;

  protected int borderWidth = 0;

  private QName lineID;

//  private String buffer;
  protected char[] chars;
  protected short cursorPos = 0;
  protected int startOffset;
  protected int length;
  protected int charsVisible;

  protected ComponentColor colorNormal;
  protected ComponentColor colorFocus;
  protected ComponentColor colorDisabled;
  protected TextWrapListener wrapListener;

  /** Creates a new TextField.
   */
  public TextLine(QName lineID, String buffer, int charsVisible) {
    this.lineID = lineID;
    setChars( buffer );
    this.charsVisible = charsVisible;
    this.startOffset = 0;
    this.length = -1;
    Platform platform = Platform.getInstance();
    colorNormal = platform.getColor(ComponentColor.WHITE);
    colorFocus  = platform.getColor(ComponentColor.YELLOW);
    colorDisabled = platform.getColor(ComponentFactory.COLOR_BACKGROUND_PANEL);
  }

  public ComponentColor getColorNormal() {
    return colorNormal;
  }

  public void setColorNormal( ComponentColor colorNormal ) {
    this.colorNormal = colorNormal;
  }

  public ComponentColor getColorFocus() {
    return colorFocus;
  }

  public void setColorFocus( ComponentColor colorFocus ) {
    this.colorFocus = colorFocus;
  }

  public ComponentColor getColorDisabled() {
    return colorDisabled;
  }

  public void setColorDisabled( ComponentColor colorDisabled ) {
    this.colorDisabled = colorDisabled;
  }

  public TextWrapListener getWrapListener() {
    return wrapListener;
  }

  public void setWrapListener(TextWrapListener wrapListener) {
    this.wrapListener = wrapListener;
  }

  /**
   * Updates the value at posX, posY. The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX  horizontal position, starting from 0
   * @param posY  vertical position, starting from 0
   * @param valueType
   * @param value the new value
   */
  public void updateValue( short posX, short posY, Type valueType, Object value ) {
    setChars( value );
    repaint();
  }

  private synchronized void setChars( Object value ) {
    if (value == null) {
      chars = EMPTY_CHARS;
    }
    else {
      String buffer = value.toString();
      chars = new char[buffer.length()];  //TODO optimieren!
      buffer.getChars( 0, buffer.length(), chars, 0 );
      if (startOffset >= chars.length) {
        startOffset = 0;
      }
      if (cursorPos > chars.length) {
        cursorPos = (short) chars.length;
      }
    }
  }

  /**
   * Inserts the value before posX, posY. The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX  horizontal position, starting from 0, to insert after last give posX = last+1
   * @param posY  vertical position, starting from 0, to insert after last give posY = last+1
   * @param valueType
   * @param value the value to be inserted
   */
  public void insertValue( short posX, short posY, SimpleType valueType, Object value ) {
    //TODO Not yet implemented
    throw new RuntimeException("TODO");
  }

  public void setCursorPos( short posX, short posY ) {
    this.cursorPos = posX;
    repaint();
  }

  public QName getFormat() {
    return null;
  }

  public void setRange(int startOffset, int length) {
    this.startOffset = startOffset;
    this.length = length;
    repaint();
  }

  public int getStartOffset() {
    return startOffset;
  }

  public void setStartOffset( int startOffset ) {
    if (startOffset != this.startOffset) {
      this.startOffset = startOffset;
      repaint();
    }
  }

  public int getLength() {
    if (chars.length == 0) return 0;
    if (length == -1) {
      return (chars.length - startOffset);
    }
    else return length;
  }

  /**
   * Returns first offset of char buffer after this text line
   * @return first offset of char buffer after this text line
   */
  public int getEndOffset() {
    return startOffset + getLength();
  }

  public short getCursorPos() {
    return cursorPos;
  }

  /**
   * This method is called whenever a state change occured. Each sub class
   * has to decide what to do, e.g. repaint its content
   */
  public void stateChanged( int oldState, int newState ) {
    repaint();
  }

  /**
   * draws cursor and direction cursors if needed
   *
   * @param graph  graphic context
   */
  protected void paintCursor(ComponentGraphics graph) {
    if (!getComponentState().hasFocus()) {
      return;
    }
    if ((cursorPos < startOffset) || (cursorPos > (startOffset+getLength()))) {
      return;
    }
    short x = (short) (calcStringPosX() + 1);
    short y = 0;
    short rowHeight = getSizeLimit(Direction.Y).getCurrent();
    ComponentFont font = getFont();
    x += (short) (font.getCharsWidth( this, chars, startOffset, (cursorPos - startOffset)) - 1 );

    // line betwenn top and bottom cursor
    graph.setColor( Platform.getInstance().getColor( ComponentColor.DARKGRAY ));
    graph.paintLine(x, y, x, (short) (y+rowHeight));
    graph.setColor( Platform.getInstance().getColor( ComponentColor.LIGHTGRAY ));
    graph.paintLine((short) (x+1), y, (short) (x+1), (short) (y+rowHeight));
    graph.paintLine((short) (x-1), y, (short) (x-1), (short) (y+rowHeight));
  }

  /**
   * Calculates this component's min, max and pref sizes and
   * stores them in heightLimits
   */
  public void calcSizes(Direction direction, SizeLimit sizeLimit) {
    if (direction == Direction.X) {
      ComponentFont compFont = this.getCompFont();
      int charWidth = compFont.getMaxCharWidth( this );
      if (charsVisible > 0) {
        int minSize = charWidth*charsVisible;
        sizeLimit.set( minSize, minSize, Short.MAX_VALUE);
      }
      else {
        int minSize = charWidth*3;
        int prefSize = charWidth*30;
        sizeLimit.set( minSize, prefSize, Short.MAX_VALUE);
      }
    }
    else {
      ComponentFont compFont = this.getCompFont();
      short stringSize = (short) compFont.getHeight(this);
      sizeLimit.set(stringSize, stringSize, stringSize);
    }

    this.getPaintState(direction).doPaintState(PaintState.STATE_SIZES_CALC);
  }

  public void doPaintImpl(ComponentGraphics graph) {
    if (!allowPaint()) {
      return;
    }
    if (getComponentState().isEnabled()) {
      if (getComponentState().hasFocus()) {
        setBackground( colorFocus );
      }
      else {
        setBackground( colorNormal );
      }
    }
    else {
      setBackground( colorDisabled );
    }

    int width  = getSizeLimit(Direction.X).getCurrent();
    int height = getSizeLimit(Direction.Y).getCurrent();
    ComponentFont font = getFont();
    int textWidth = width;

    if (!isVisible()) {
      return;
    }
    if ((width > 0) && (height > 0)) {
      graph.paintBackground( getBackground(), 0, 0, width, height);
      int charCount = getLength();
      if (charCount < 0) charCount = 0;
      if (charCount > 0) {
        synchronized(this) {
          if (wrapListener != null) {
            // we must prevent WrapListener (e.g. TextEditor) from changing our
            // buffer while text wrapping
            int len = charCount;
            while (font.getCharsWidth( this, chars, startOffset, charCount ) > textWidth) {
              charCount--;  //todo: optimieren!
            }
            if (charCount < len) {
              wrapListener.doLineBreak(this, startOffset+charCount);
            }
          }
          int start = startOffset;
          // wrapListener might have modified startOffset
          charCount++; // add last (truncated) char for rendering
          if (start + charCount > chars.length) {
            charCount = chars.length - start;
          }

          int x = calcStringPosX();
          this.paintCursor(graph);
          graph.paintString( this.chars, start, charCount, x, 0, this.getForeground(), font );
        }
      }
      else {
        this.paintCursor(graph);
      }
    }
  }

  protected short calcStringPosX() {
    short x = 0;
    if (inputMode == InputMode.numeric) {
      int width  = getSizeLimit(Direction.X).getCurrent();
      ComponentFont font = getFont();
      x = (short) (width - font.getCharsWidth( this, chars, 0, chars.length ));
    }
    return x;
  }

  /**
   * Calculates the position of the cursor in the text buffer from given paint position
   * @param firstVisibleChar the first character visible in the component
   * @param posX the paint position
   * @return the cursor position in the text buffer
   */
  public short calcCursorPos (int firstVisibleChar, int posX) {
    ComponentFont compFont = this.getCompFont();
    int charIndex = firstVisibleChar;
    int posIndex  = 0;
    while ((posIndex < posX) && (charIndex < (startOffset+getLength()))) {
      posIndex += compFont.getCharWidth(this, chars[charIndex]);
      charIndex++;
    }
    if (charIndex < (startOffset+getLength())) charIndex--;
    return (short) charIndex;
  }

  /**
   * Calculates the last visible character position in buffer if entry fields width is fieldWidth
   * @param firstVisibleChar the first character to show
   * @param fieldWidth the entr field's width
   * @return the last visible character position in buffer
   */
  public int calcLastVisibleChar( int firstVisibleChar, int fieldWidth) {
    ComponentFont compFont = this.getCompFont();
    int charIndex = firstVisibleChar;
    int width     = 0;
    int bufLen    = chars.length;
    while ((width < fieldWidth) && (charIndex < bufLen)) {
      width += compFont.getCharWidth(this, chars[charIndex]);
      charIndex++;
    }
    return charIndex;
  }

  /**
   * Moves the visible part of buffer until cursorPos is visible
   * @param fieldWidth the available space for the entry field
   * @return the buffer index of the last visible character
   */
/*  public int adjustVisibleArea( int firstVisibleChar, int fieldWidth ) {
    if (DEBUG) Logger.debug( "-> TextField.adjustVisibleArea");
    ComponentFont compFont = this.getCompFont();
    int lastCharIndex = calcLastVisibleChar( firstVisibleChar, fieldWidth);
    int scrollWidth;
    int scrollCount;
    int dx;

    //----- Scroll to right if necessary
    if (cursorPos > lastCharIndex-SCROLL_LIMIT) {
      if (cursorPos > buffer.length()-SCROLL_LIMIT) scrollCount = buffer.length()-lastCharIndex;
      else scrollCount = cursorPos - (lastCharIndex - SCROLL_LIMIT);
      if (scrollCount > 0) {
        scrollWidth = compFont.getCharsWidth(this, buffer.getChars(), cursorPos-scrollCount, scrollCount);
        dx = 0;
        scrollCount = 0;
        while (dx < scrollWidth) {
          firstVisibleChar++;
          dx += compFont.getCharWidth(this, buffer.getChars()[firstVisibleChar]);
        }
        lastCharIndex = calcLastVisibleChar(firstVisibleChar, fieldWidth);
      }
    }
    //----- Scroll to left if necessary
    else if (cursorPos < firstVisibleChar+SCROLL_LIMIT) {
      if (cursorPos < SCROLL_LIMIT) firstVisibleChar = 0;
      else {
        scrollCount = firstVisibleChar - (cursorPos - SCROLL_LIMIT);
        firstVisibleChar -= scrollCount;
      }
      lastCharIndex = calcLastVisibleChar(firstVisibleChar, fieldWidth);
    }
    if (DEBUG) Logger.debug( "<- TextField.adjustVisibleArea");
    return lastCharIndex;
  }
 */
  /**
   * Call the action valid for the mouse event.
   * The action is defined in the controller.
   * @param state
   * @param lastPointedComponent
   * @param posInComp
   */
  public void processMouseEvent(int state, Component lastPointedComponent, Position posInComp) {
    if (state == InputManager.STATE_SELECTED || state == InputManager.STATE_LOCAL_MENU ){
      if (lastPointedComponent == this) {
        short cursorPos = calcCursorPos( startOffset, posInComp.getX() );
        this.setState( cursorPos, (short) 0, Boolean.TRUE, Boolean.TRUE, null, null );
        if (listener != null) {
          listener.selected( this.getID(), cursorPos, (short) -1, componentState.isSelected() );
        }
      }
    }
    else if (state == InputManager.STATE_SCROLL) {
      processScroller( posInComp );
    }
  }

  /**
   * Called by Component whenever a input command arrives.
   *
   * @param compID
   * @param cmd       the command, one of the constants InputManager.CMD_*
   * @param inputChar optional input character for the command
   * @param index     optional index parameter for the command @return true if the command has been processed, otherwise false
   */
  public boolean command( QName compID, QName cmd, char inputChar, int index ) {
    if (listener != null) {
      listener.command( getID(), cmd, inputChar, index );
    }
    return true;
  }
}
