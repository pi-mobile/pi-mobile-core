/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.gui.event.ComponentListener;
import de.pidata.gui.event.InputManager;
import de.pidata.gui.event.TextWrapListener;
import de.pidata.gui.guidef.InputMode;
import de.pidata.log.Logger;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.QName;

public class TextArea extends AbstractContainer implements TextWrapListener {

  private static final boolean DEBUG = false;
  private static final String ME = "TextArea";

  private TextLine renderLine;
  private TextLine editLine;
  private Scroller scrollerY;

  private String data[];
  private int rowCount = 0;
  private short selectedLine = 0;
  private int cursorPosX = 0;
  private int minLinesVisible = 0;
  private int charsVisible;
  private int maxLinesVisible = -1;
  private short renderIndex;
  private Border textBorder;

  public TextArea( int charsVisible, int rowsVisible, Border textBorder ) {
    super();
    this.charsVisible = charsVisible;
    if (rowsVisible > 1) {
      this.minLinesVisible = rowsVisible;
      this.maxLinesVisible = rowsVisible;
    }
    this.textBorder = textBorder;

    Platform platform = Platform.getInstance();

    renderLine = platform.getComponentFactory().createTextLine(null, null, this.charsVisible );
    renderLine.setLayoutParent(this);
    renderLine.getComponentState().setVisible(false);
    renderLine.setWrapListener( this );

    editLine = platform.getComponentFactory().createTextLine(null, null, this.charsVisible );
    editLine.setLayoutParent(this);
    editLine.getComponentState().setVisible(true);
    editLine.setBackground( Platform.getInstance().getColor(ComponentColor.YELLOW) );
    editLine.setWrapListener( this );
    add( editLine );

    scrollerY = platform.getComponentFactory().createScrollbar(Direction.Y);
    scrollerY.setLayoutParent(this);
    scrollerY.getComponentState().setVisible(true); // TODO: nur bei Bedarf?

    data = new String[minLinesVisible];
  }

  /**
   * Sets a selection listener for this component. SelectionID may be used as identifier
   * if listener wants to listen on multiple components. Listener is called whenever
   * setSelected is called on this component.
   *
   * @param listener the selection listener
   */
  public void setComponentListener( ComponentListener listener ) {
    super.setComponentListener( listener );
    editLine.setComponentListener( listener );
  }

  /**
   * Sets this Controller's current input mode
   *
   * @param inputMode this Controller's new input mode
   */
  public void setInputMode( InputMode inputMode ) {
    super.setInputMode( inputMode );
    editLine.setInputMode( inputMode );
  }

  /**
   * Tells the component the count of available values, e.g. for displaying a scroll bar
   *
   * @param sizeX horizontal count
   * @param sizeY vertical count
   */
  public void setDataCount(short sizeX, short sizeY) {
    this.rowCount = sizeY;
    this.scrollerY.setMaxValue( sizeY );
    if (isVisible()) {
      doScrollerLayout( Direction.Y );
    }
  }

  /**
   * Inserts the value before posX, posY. The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX      horizontal position, starting from 0, to insert after last give posX = last+1
   * @param posY      vertical position, starting from 0, to insert after last give posY = last+1
   * @param valueType
   * @param value     the value to be inserted
   */
  public void insertValue(short posX, short posY, SimpleType valueType, Object value) {
    //TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

    /**
   * Updates the value at posX, posY. The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
     * @param posX      horizontal position, starting from 0, meaning character position
     * @param posY      vertical position, starting from 0, meaning visible line number
     * @param valueType
     * @param value     the new value
     */
  public void updateValue(short posX, short posY, Type valueType, Object value) {
    if(posY < data.length) {
      if (value == null) data[posY] = "";
      else data[posY] = value.toString();
      if (posY == renderIndex) {
        renderLine.updateValue( (short) 0, posY, StringType.getDefString(), data[posY]);
      }
      repaint();
    }
  }

  /**
   * Sets the cursor position to (posX,posY). The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX horizontal position, starting from 0
   * @param posY vertical position, starting from 0
   */
  public void setCursorPos( short posX, short posY ) {
    int cursorPosY = selectedLine + scrollerY.getValue(); 
    if ((posY != cursorPosY) || (posX != cursorPosX)) {
      selectedLine = (short) (posY - scrollerY.getValue());
      cursorPosX = posX;
      editLine.setCursorPos( posX, (short) 0 );
      if (isVisible()) {
        scrollRowToVisible( posY );
        repaint();
      }
    }  
  }

  public boolean setFocus( boolean focus ) {
    boolean ok = super.setFocus( focus );
    if (ok) editLine.setFocus( focus );
    return ok;
  }

  /**
   * Toggles focus, selected, enabled and visible state of this component at position (posX, posY) or for
   * entry identified by key. It depends on the type of component what is meant by posX, posY and key.
   * Many components will just ignore posX and posY values or key.
   * The default implementaion calls the componentListener.
   * @param posX      the x position of the source event
   * @param posY      the y position of the source event
   * @param focus     set focus on/off
   * @param selected  set selected if true, deselected if false, do not change if null
   * @param enabled   set enabled if true, disabled if false, do not change if null
   * @param visible   set visible if true, invisible if false, do not change if null
   */
  public void setState( short posX, short posY, Boolean focus, Boolean selected, Boolean enabled, Boolean visible ) {
    super.setState( posX, posY, focus, selected, enabled, visible );
    if ((posY >= 0) && (selected != null)) {
      if (selected.booleanValue()) {
        this.selectedLine = posY;
      }
      else {
        this.selectedLine = -1;
      }
    }
  }


  /**
   * This method is called whenever a state change occured. Each sub class
   * has to decide what to do, e.g. repaint its content
   */
  public void stateChanged( int oldState, int newState ) {
    repaint();
  }

  /**
   * Get the size of the area which can be used while painting children.
   * @param dir direction
   * @return useable size within given direction
   */
  public short clientSize(Direction dir) {
    short visSize = getSizeLimit(dir).getCurrent();
    if (dir == Direction.X) {
      if (scrollerY.isVisible()) {
        visSize = (short) (visSize - scrollerY.getSizeLimit(dir).getCurrent());
      }
    }
    if (textBorder != null) {
      if (dir == Direction.X) visSize -= (textBorder.getLeft() + textBorder.getRight());
      else visSize -= (textBorder.getTop() + textBorder.getBottom());
    }
    return visSize;
  }

  /**
   * Calculates this component's min, max and pref sizes in one direction and
   * stores them in sizeLimit
   *
   * @param direction
   * @param sizeLimit
   */
  public void calcSizes(Direction direction, SizeLimit sizeLimit) {
    SizeLimit rowSize = editLine.getSizeLimit(direction);

    int areaSizeMin;
    int areaSizePref;
    int areaSizeMax;

    if (getPaintState(direction).hasPaintStateReached(PaintState.STATE_SIZES_CALC)) {
      return;
    }

    if (direction == Direction.X) {
      SizeLimit scrollerSize = scrollerY.getSizeLimit( Direction.X );
      areaSizeMin = rowSize.getMin() + scrollerSize.getMin();
      areaSizePref = rowSize.getPref() + scrollerSize.getPref();
      areaSizeMax = rowSize.getMax() + scrollerSize.getMax();
      if (textBorder != null) {
        int dx = textBorder.getLeft() + textBorder.getRight();
        areaSizeMin += dx;
        areaSizePref += dx;
        areaSizeMax += dx;    
      }
    }
    else {
      areaSizeMin = rowSize.getMin() * minLinesVisible;
      if (textBorder != null) {
        areaSizeMin += textBorder.getTop() + textBorder.getBottom();
      }
      if (minLinesVisible > 0) {
        areaSizePref = areaSizeMin;
      }
      else {
        areaSizePref = Short.MAX_VALUE;
      }
      if (maxLinesVisible > 0) {
        areaSizeMax = rowSize.getMin() * minLinesVisible;
        if (textBorder != null) {
          areaSizeMax += textBorder.getTop() + textBorder.getBottom();
        }
      }
      else {
        areaSizeMax = Short.MAX_VALUE;
      }
    }

    sizeLimit.set(areaSizeMin, areaSizePref, areaSizeMax);

    getPaintState(direction).doPaintState(PaintState.STATE_SIZES_CALC);
  }

  private void moveLine( TextLine textLine, short lineNum ) {
    short x = 0;
    short lineHeight = textLine.getSizeLimit(Direction.Y).getCurrent();
    int linePos = lineNum * lineHeight;
    if (textBorder != null) {
      linePos += textBorder.getTop();
      x += textBorder.getLeft();
    }
    if (linePos > Short.MAX_VALUE) {
      linePos = Short.MAX_VALUE;
    }
    textLine.setPos(Direction.X, x );
    textLine.setPos(Direction.Y, (short) linePos);

    try {
      if (lineNum < data.length) {
        textLine.updateValue( (short) 0, lineNum, StringType.getDefString(), data[lineNum] );
      }
      else {
        textLine.updateValue( (short) 0, lineNum, StringType.getDefString(), null );
      }
    }
    catch (Exception ex) {
      Logger.error( "Error initializing textline #"+lineNum, ex );
    }
  }

  private int calcRowPos (int rowNum) {
    short rowHeight = 0;
    rowHeight = renderLine.getSizeLimit(Direction.Y).getCurrent();
    int rowPos = rowNum * rowHeight; // todo + border usw.
    if (rowPos > Short.MAX_VALUE ) {
      rowPos = Short.MAX_VALUE;
    }
    return rowPos;
  }

  public void scrollRowToVisible(int rowIndex) {
    int firstVisibleRow = scrollerY.getValue();
    int visibleRowCount = scrollerY.getValueLength();
    if (rowIndex < firstVisibleRow) {
      scrollerY.setValue( rowIndex );
    }
    else if (rowIndex >= (firstVisibleRow + visibleRowCount)) {
      scrollerY.setValue( rowIndex + 1 - visibleRowCount);
    }

    Container parent = getLayoutParent();
    if (parent != null) {
      int size = renderLine.getSizeLimit(Direction.Y).getCurrent();
      int pos = calcRowPos( rowIndex - scrollerY.getValue() );
      parent.scrollToVisible(Direction.Y, pos + getPos(Direction.Y), size);
    }
  }

  public void doScrollerLayout(Direction direction) {
    PaintState paintState = getPaintState(direction);
    if (!paintState.hasPaintStateReached(PaintState.STATE_BOUNDS_SET)) {
      return;
    }

    short currentSize = getSizeLimit(direction).getCurrent();
    if (direction == Direction.X) {
      short scrollerWidth = this.scrollerY.getSizeLimit(direction).getPref();
      scrollerY.getSizeLimit(direction).setCurrentFit( scrollerWidth );
      scrollerY.setPos( direction, (short) (currentSize - scrollerWidth) );
    }
    else {
      int length = visibleRows();
      short posY = 0;

      if (listener != null) {
        listener.setVisibleRange( getID(), (short) 0, (short) scrollerY.getValue(), Short.MAX_VALUE, (short) (scrollerY.getValue() + length) );
      }

      if (length > rowCount) length = rowCount;
      scrollerY.setValueConstraints( 0, rowCount, length, 1, (length / 2));
      scrollerY.getSizeLimit(direction).setCurrent(currentSize);
      scrollerY.setPos( direction, posY );
      scrollerY.doLayout(direction);
    }
  }

  private int visibleRows() {
    short lineSize = renderLine.getSizeLimit( Direction.Y ).getCurrent();
    short height = getSizeLimit( Direction.Y ).getCurrent();
    if (textBorder != null) {
       height -= (textBorder.getTop() + textBorder.getBottom());
    }
    int length;
    if (height <= 0) length = 0;
    else length = height / lineSize;

    if (length > data.length) {
      String[] newData = new String[length];
      System.arraycopy( this.data, 0, newData, 0, this.data.length );
      this.data = newData;
    }
    return length;
  }

  private void doPaintScroller (Scroller scroller, ComponentGraphics compGraph, short size) {
    short scrollerWidth = scroller.getSizeLimit(Direction.X).getCurrent();
    short scrollerHeight = scroller.getSizeLimit(Direction.Y).getCurrent();
    short offsetX = 0;
    short offsetY = scroller.getPos(Direction.Y);

    if (scroller.getDirection() == Direction.X) {
      offsetY = (short) (size - scrollerHeight);
    } else {
      offsetX = (short) (size - scrollerWidth);
    }
    compGraph.translate( offsetX, offsetY);
    Rect oldClip = new Rect();
    compGraph.getClip(oldClip);
    compGraph.clipRect((short) 0, (short) 0, scrollerWidth, scrollerHeight);

    scroller.doPaint(compGraph);

    compGraph.setClip(oldClip);
    compGraph.translate((short) -offsetX,  (short) -offsetY);
  }

  /**
   * Called for painting this component
   *
   * @param graph the graphics context to use for painting
   */
  public void doPaintImpl(ComponentGraphics graph) {
    Rect outerClip = null;
    if (!allowPaint()) {
      return;
    }
    if (DEBUG) Logger.debug( "-> TextArea.doPaintImpl");

    doScrollerLayout( scrollerY.getDirection() );

    //----- Draw background
    short width = getSizeLimit( Direction.X ).getCurrent();
    short height = getSizeLimit( Direction.Y ).getCurrent();
    graph.paintBackground(getBackground(), 0,  0, width, height);
    if (textBorder != null) {
      textBorder.doPaint( graph, width - this.scrollerY.getSizeLimit(Direction.X).getCurrent(), height );
      outerClip = new Rect();
      graph.getClip( outerClip );
      graph.setClip( new Rect( textBorder.getLeft(), textBorder.getTop(), width - textBorder.getRight(), height - textBorder. getBottom() ) );
    }

    TextLine paintLine;
    Rect oldClip = new Rect();
    renderLine.getComponentState().setVisible(true);
    int paintRowCount = visibleRows();
    if (paintRowCount > rowCount) paintRowCount = rowCount;
    for (renderIndex = 0; renderIndex <= paintRowCount; renderIndex++) {
      if (renderIndex == selectedLine) {
        paintLine = editLine;
      }
      else {
        paintLine = renderLine;
      }
      moveLine( paintLine, renderIndex );
      graph.getClip( oldClip );
      short posX = paintLine.getPos( Direction.X );
      short posY = paintLine.getPos( Direction.Y );
      graph.translate( posX, posY );
      graph.setClip( new Rect( 0, 0, paintLine.getSizeLimit( Direction.X ).getCurrent(), paintLine.getSizeLimit( Direction.Y ).getCurrent()) );
      paintLine.doPaint( graph );
      graph.translate( (short) -posX, (short) -posY );
      graph.setClip( oldClip );
    }
    renderLine.getComponentState().setVisible(false);

    if (textBorder != null) {
      graph.setClip( outerClip );
    }
    doPaintScroller(scrollerY, graph, width);

    if (DEBUG) Logger.debug( "<- TextArea.doPaintImpl");
  }

 /**
   * Calculate the position of the children.
   * This method is empty for normal components, but needs implementaion
   * for containers.
   */
  public void doLayout (Direction direction) {
    short currentSize = getSizeLimit(direction).getCurrent();
    PaintState paintState = getPaintState(direction);
    if (!paintState.hasPaintStateReached(PaintState.STATE_BOUNDS_SET)) {
      return;
    }

    if (direction == Direction.X) {
      short scrollerWidth = this.scrollerY.getSizeLimit(direction).getPref();
      int borderWidth = 0;
      if (textBorder != null) {
        borderWidth = (textBorder.getTop() + textBorder.getBottom());
      }
      renderLine.getSizeLimit(direction).setCurrentFit((short) (currentSize - scrollerWidth - borderWidth));
      editLine.getSizeLimit(direction).setCurrentFit((short) (currentSize - scrollerWidth - borderWidth));
    }
    else {
      short lineSize = renderLine.getSizeLimit(direction).getPref();
      renderLine.getSizeLimit(direction).setCurrent(lineSize);
      editLine.getSizeLimit(direction).setCurrent(lineSize);
    }

    doScrollerLayout(direction);

    paintState.doPaintState(PaintState.STATE_LAYOUTED);
  }

  /**
   * Called by TextLine if text is too large to fit in Textline
   *
   * @param textLine the line to be wrapped
   * @param wrapPos  teh last character which can be displayed
   */
  public void doLineBreak( TextLine textLine, int wrapPos ) {
    if (textLine == renderLine) {
      listener.setVisibleRange( getID(), (short) -1, this.renderIndex, (short) wrapPos, (short) 0 );
    }
    else { // editLine
      listener.setVisibleRange( getID(), (short) -1, this.selectedLine, (short) wrapPos, (short) 0 );
    }
  }

  private short calcRowAt( short y ) {
    if (textBorder != null) {
      y = (short) (y - textBorder.getTop());
      if (y < 0) y = 0;
    }
    short lineHeight = renderLine.getSizeLimit(Direction.Y).getCurrent();
    return (short) (y / lineHeight);
  }

  /**
   * Call the action valid for the mouse event.
   * The action is defined in the controller.
   *
   * @param state
   * @param lastPointedComponent
   * @param posInComp
   */
  public void processMouseEvent( int state, Component lastPointedComponent, Position posInComp ) {
    if (state == InputManager.STATE_SELECTED || state == InputManager.STATE_LOCAL_MENU ){
      if (lastPointedComponent == this) {
        short selLine = calcRowAt( posInComp.getY() );
        if (selLine != this.selectedLine) {
          this.selectedLine = selLine;
          moveLine( editLine, selectedLine );
          repaint();
        }
        short cursorX = editLine.calcCursorPos( 0, posInComp.getX() );
        this.setState( cursorX, selectedLine, Boolean.TRUE, Boolean.TRUE, null, null );
        if (listener != null) {
          listener.selected( this.getID(), cursorX, selectedLine, componentState.isSelected() );
        }
      }
    }
    else if (state == InputManager.STATE_SCROLL) {
      processScroller( posInComp );
    }
  }

  public Component findComponentAt(Position pos){
    short x = pos.getX();
    short y = pos.getY();
    short xOffset = x;

    if (scrollerY != null && scrollerY.isVisible()) {
      if (containsPos( scrollerY, Direction.X, xOffset ) && containsPos( scrollerY, Direction.Y, y )) {
        pos.translate( scrollerY.getPos(Direction.X), scrollerY.getPos(Direction.Y) );
        return scrollerY;
      }
    }

    //search in container
    return super.findComponentAt(pos);
  }
}
