/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.gui.event.InputManager;
import de.pidata.gui.layout.LayoutInfo;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.QName;


/**
 * This component describes a piece of text on the window.
 */
public class Label extends AbstractComponent {

  public static QName FORMAT_NUMBER = ComponentFactory.NAMESPACE.getQName("number");
  public static QName FORMAT_SAP_NUMBER = ComponentFactory.NAMESPACE.getQName("sapNumber");
  public static QName FORMAT_NUM_COLOR = ComponentFactory.NAMESPACE.getQName("numColor");

  // the text shown on the screen
  private String caption;
  private QName format;
  private int charsVisible = -1;

  public Label() {
  }

  /**
   * Create a Label
   * @param caption      initial caption, used for size calculation if charsvisible is -1
   * @param format       format rule
   * @param charsVisible Number of characters for width calculation, if -1 size of caption is used
   */
  public Label( String caption, String format, int charsVisible ) {
    if ((format != null) && (format.length() > 0)) {
      this.format = ComponentFactory.NAMESPACE.getQName(format);
    }
    setCaption( caption );
    this.charsVisible = charsVisible;

    if (this.format == FORMAT_SAP_NUMBER) {
      getLayoutInfo( Direction.X ).setAlignment( LayoutInfo.ALIGN_MAX );
    }
    setForeground(Platform.getInstance().getColor(ComponentFactory.COLOR_LABEL_TEXT));
  }

  private void setCaption( String caption ) {
    if ((caption != null) && (format == FORMAT_SAP_NUMBER)) {
      this.caption = StringType.removeLeadingZeros( caption );
    }
    else {
      this.caption = caption;
    }
  }

  /**
   * Replaces this label's text. posX and posY are ignored.
   *
   * @param posX  ignored
   * @param posY  ignored
   * @param valueType
   * @param value the new label text (String)
   */
  public void updateValue( short posX, short posY, Type valueType, Object value ) {
    setLabelText( (String) value );
  }

  /**
   * Inserts the value before posX, posY. The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX  horizontal position, starting from 0, to insert after last give posX = last+1
   * @param posY  vertical position, starting from 0, to insert after last give posY = last+1
   * @param valueType
   * @param value the value to be inserted
   */
  public void insertValue( short posX, short posY, SimpleType valueType, Object value ) {
    //TODO
    throw new RuntimeException("TODO");
  }

  /**
   * Sets the cursor position to (posX,posY). The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX horizontal position, starting from 0
   * @param posY vertical position, starting from 0
   */
  public void setCursorPos( short posX, short posY ) {
    // do nothing
  }

  public QName getFormat() {
    return format;
  }

  /**
   * This method is called whenever a state change occured. Each sub class
   * has to decide what to do, e.g. repaint its content
   */
  public void stateChanged( int oldState, int newState ) {
    // do nothing
  }

  /**
   * Called for painting this component
   * Der graphics context wird bereits zugeschnitten hier angeliefert.
   * @param graph the graphics context to use for painting
   */
  public void doPaintImpl(ComponentGraphics graph) {
    ComponentColor background;
    short sizeX = getSizeLimit(Direction.X).getCurrent();
    short sizeY = getSizeLimit(Direction.Y).getCurrent();
    short x;
    short emptySpace;

    if (!allowPaint()) {
      return;
    }
    background = getBackground();
    if ((format == FORMAT_NUM_COLOR) || (format == FORMAT_NUMBER)) {
      int value = 0;
      if ((this.caption != null) && (this.caption.length() > 0)) {
        //TODO parseInt im CharBuffer einbauen
        value = Integer.parseInt(this.caption);
      }
      if (format == FORMAT_NUM_COLOR) {
        Platform platform =  Platform.getInstance();
        //TODO Schwellwerte parametrierbar machen
        if (value < 0) background = platform.getColor(ComponentColor.RED);
        else if (value < 5) background = platform.getColor(ComponentColor.YELLOW);
        else background = platform.getColor(ComponentColor.GREEN);
      }
    }

    // set label position according to alignment
    ComponentFont font = getFont();
    emptySpace = (short) (sizeX - font.getStringWidth( this, this.caption ));
    switch (getLayoutInfo(Direction.X).getAlignment()) {
      case LayoutInfo.ALIGN_MAX:
        x = emptySpace;
        break;
      case LayoutInfo.ALIGN_CENTER:
        x = (short) (emptySpace / 2);
        break;
      case LayoutInfo.ALIGN_MIN:
      case LayoutInfo.ALIGN_GROW:
      default:
        x = 0;
    }

    graph.paintBackground( background, 0, 0, sizeX, sizeY);
    graph.paintString( this.caption, x, 0, this.getForeground(), font);
  }

  public String getLabelText() {
    return this.caption;
  }

  public void setLabelText(String caption) {
    setCaption( caption );
    this.repaint();
  }

  /**
   * Calculates this component's min, max and pref sizes and
   * stores them in heightLimits
   */
  public void calcSizes(Direction direction, SizeLimit sizeLimit) {
    ComponentFont compFont = this.getCompFont();
    short stringSize = 0;
    short maxSize = 0;

    if(direction == Direction.X) {
      if (charsVisible > 0) {
        int charWidth = compFont.getMaxCharWidth( this );
        stringSize = (short) (charWidth * charsVisible);
      }
      else {
        stringSize = (short) compFont.getStringWidth( this, this.caption );
      }
      maxSize = Short.MAX_VALUE;
    }
    else {
      stringSize = (short) compFont.getHeight(this);
      maxSize = stringSize;
    }
    sizeLimit.set(stringSize, stringSize, maxSize);

    this.getPaintState(direction).doPaintState(PaintState.STATE_SIZES_CALC);
  }

  /**
   * Calculates the position of the cursor in the text buffer from given paint position
   * @param posX the paint position
   * @return the cursor position in the text buffer
   */
  public short calcCursorPos( int posX ) {
    ComponentFont compFont = this.getCompFont();
    int charIndex = 0;
    int posIndex  = 0;
    while ((posIndex < posX) && (charIndex < caption.length())) {
      posIndex += compFont.getCharWidth(this, caption.charAt( charIndex ));
      charIndex++;
    }
    if (charIndex < (caption.length())) charIndex--;
    return (short) charIndex;
  }

  /**
   * Call the action valid for the mouse event.
   * The action is defined in the controller.
   * @param state
   * @param lastPointedComponent
   * @param posInComp
   */
  public void processMouseEvent(int state, Component lastPointedComponent, Position posInComp) {
    if (state == InputManager.STATE_SELECTED || state == InputManager.STATE_LOCAL_MENU ){
      if (lastPointedComponent == this) {
        short cursorPos = calcCursorPos( posInComp.getX() );
        this.setState( cursorPos, (short) 0, Boolean.TRUE, Boolean.TRUE, null, null );
        if (listener != null) {
          listener.selected( this.getID(), cursorPos, (short) -1, componentState.isSelected() );
        }
      }
    }
    else if (state == InputManager.STATE_SCROLL) {
      processScroller( posInComp );
    }
  }
}
