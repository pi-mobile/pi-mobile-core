/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;


public class Tools {

  /**
   * Returns the greater of two short values
   * @param value1
   * @param value2
   * @return value1 if value2 is less, else value2
   */
 public static short max(short value1, short value2) {
    if (value1 > value2) return value1;
    else return value2;
  }

 /**
   * Returns the less of two short values
   * @param value1
   * @param value2
   * @return value1 if value2 is greater, else value2
   */
  public static short min(short value1, short value2) {
    if (value1 < value2) return value1;
    else return value2;
  }

}
