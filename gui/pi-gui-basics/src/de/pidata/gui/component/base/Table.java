/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.gui.event.ComponentListener;
import de.pidata.gui.event.InputManager;
import de.pidata.gui.event.TableComp;
import de.pidata.gui.layout.Layoutable;
import de.pidata.gui.layout.Layouter;
import de.pidata.gui.ui.base.UITableAdapter;
import de.pidata.gui.view.base.TableViewPI;
import de.pidata.log.Logger;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.simple.StringType;

import java.util.Vector;

public class Table extends AbstractContainer implements TableComp {

  private static final boolean DEBUG = false;
  private static final String ME = "Table";

  private short minVisibleRows = 2; //todo: konfig am binding selbst

  private Panel headRow;
  private Panel masterRow;
  private Panel editors = null;

  private Layouter layouterX;
  private Scroller scrollerY;

  /** will be set if inline editing is active; will be set to null when editing is finished */
  private Component editComp = null;
  private Popup editPopup = new Popup();

  private ComponentColor activeRowFocusColor;
  private ComponentColor activeRowSelColor;
  private ComponentColor normalRowColor;

  private short tableGap = 5;  //todo: konfigurierbar

  private Vector data = new Vector();
  private int selectedRow = -1;
  private short visibleRows;

  public Table(Layouter layouterX, Panel header, Panel body, Panel edit, short visibleRows) {
    super();
    Platform platform = Platform.getInstance();
    Component child;

    this.layouterX = layouterX;
    this.layouterX.setGap(tableGap);
    this.visibleRows = visibleRows;

    this.headRow = header;
    headRow.setLayoutParent(this);
    ComponentColor headBkg = platform.getColor(ComponentFactory.COLOR_TABLE_HEADER_BACKGROUND);
    ComponentColor headFg = platform.getColor(ComponentFactory.COLOR_TABLE_HEADER_FONT);
    headRow.setBackground(headBkg);
    for (int i = 0; i < headRow.childCount(); i++) {
      child = (Component) headRow.getChild(i);
      child.setBackground(headBkg);
      child.setForeground(headFg);
      //TODO wozu - kollidieret mit manuellem Alignment? child.setAlignment(Direction.X, ALIGN_GROW);
    }
    this.activeRowFocusColor = platform.getColor(ComponentFactory.COLOR_TABLE_ACTIVE_ROW);
    this.activeRowSelColor = platform.getColor(ComponentFactory.COLOR_TABLE_SELECTED_ROW);
    this.normalRowColor = platform.getColor(ComponentFactory.COLOR_TABLE_NORMAL_ROW);

    masterRow = body;
    masterRow.setLayoutParent(this);
    masterRow.getComponentState().setVisible(false);
    for (int i = 0; i < masterRow.childCount(); i++) {
      child = (Component) masterRow.getChild(i);
      //TODO wozu - kollidieret mit manuellem Alignment, z.B. für Decimals? child.setAlignment(Direction.X, ALIGN_GROW);
      child.setBackground(this.normalRowColor);
    }

    this.add(header);
    scrollerY = platform.getComponentFactory().createScrollbar(Direction.Y);
    scrollerY.setLayoutParent(this);
    scrollerY.getComponentState().setVisible(true);

    masterRow.setBackground(platform.getColor(ComponentFactory.COLOR_TABLE_BACKGROUND));

    if (edit != null) {
      editors = edit;
      editors.setLayoutParent(this);
      editors.getComponentState().setVisible(false);
    }

    editPopup.setBackground(platform.getColor(ComponentFactory.COLOR_TABLE_ACTIVE_ROW));
    setBackground(platform.getColor(ComponentFactory.COLOR_TABLE_BACKGROUND));
 }

  /**
   * Sets a component listener for this component. SelectionKey may be used as identifier
   * if listener wants to listen on multiple components. Listener is called whenever
   * a input event occurs on this component.
   * @param listener  the component listener
   */
  public void setComponentListener( ComponentListener listener ) {
    super.setComponentListener( listener );
    for (int i = 0; i < headRow.childCount(); i++) {
      Component headerComp = (Component) headRow.getChild(i);
      headerComp.setComponentListener( listener );
    }
  }

  /**
   * Tells the component the count of available values, e.g. for displaying a scroll bar
   *
   * @param sizeX ignored
   * @param sizeY row count
   */
  public void setDataCount( short sizeX, short sizeY ) {
    doScrollerLayout( Direction.Y );
    repaint();
  }

  /**
   * Inserts the value before posX, posY. The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX  horizontal position, starting from 0, to insert after last give posX = last+1
   * @param posY  vertical position, starting from 0, to insert after last give posY = last+1
   * @param valueType
   * @param value the value to be inserted
   */
  public void insertValue( short posX, short posY, SimpleType valueType, Object value ) {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  /**
   * Updates the value at posX, posY. The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX  horizontal position, starting from 0
   * @param posY  vertical position, starting from 0
   * @param valueType
   * @param value the new value
   */
  public void updateValue( short posX, short posY, Type valueType, Object value ) {
    if (posY < data.size()) {
      Object[] rowData = (Object[]) data.elementAt( posY );
      rowData[posX] = value;
      repaint();
    }
  }

  /**
   * Sets the cursor position to (posX,posY). The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX horizontal position, starting from 0
   * @param posY vertical position, starting from 0
   */
  public void setCursorPos( short posX, short posY ) {
    if (isVisible()) {
      scrollRowToVisible( posY );
      repaint();
    }  
  }

  /**
   * Toggles focus, selected, enabled and visible state of this component at position (posX, posY) or for
   * entry identified by key. It depends on the type of component what is meant by posX, posY and key.
   * Many components will just ignore posX and posY values or key.
   * The default implementaion calls the componentListener.
   * @param posX      the x position of the source event
   * @param posY      the y position of the source event
   * @param focus     set focus on/off
   * @param selected  set selected if true, deselected if false, do not change if null
   * @param enabled   set enabled if true, disabled if false, do not change if null
   * @param visible   set visible if true, invisible if false, do not change if null
   */
  public void setState( short posX, short posY, Boolean focus, Boolean selected, Boolean enabled, Boolean visible ) {
    super.setState( posX, posY, focus, selected, enabled, visible );
    if (selected != null) {
      if (selected.booleanValue()) {
        this.selectedRow = posY;
      }
      else {
        this.selectedRow = -1;
      }
    }
  }

  /**
   * This method is called whenever a state change occured. Each sub class
   * has to decide what to do, e.g. repaint its content
   */
  public void stateChanged( int oldState, int newState ) {
    repaint();
  }

  public Panel getHeadRow() {
    return headRow;
  }

  public Panel getMasterRow() {
    return masterRow;
  }

  public Component getEditComp() {
    return editComp;
  }

  /**
   * Get the size of the area which can be used while painting children.
   * @param dir direction
   * @return useable size within given direction
   */
  public short clientSize(Direction dir) {
    short visSize = getSizeLimit(dir).getCurrent();
    if (dir == Direction.X) {
      if (scrollerY.isVisible()) {
        visSize = (short) (visSize - scrollerY.getSizeLimit(dir).getCurrent());
      }
    }
    else {
      visSize = (short) (visSize - headRow.getSizeLimit(dir).getPref());
    }
    return visSize;
  }

  public void scrollRowToVisible(int rowIndex) {
    int firstVisibleRow = scrollerY.getValue();
    int visibleRowCount = scrollerY.getValueLength();
    if (rowIndex < firstVisibleRow) {
      scrollerY.setValue( rowIndex );

    }
    else if (rowIndex >= (firstVisibleRow + visibleRowCount - 1)) {
      scrollerY.setValue( rowIndex + 1 - visibleRowCount);
    }

    Container parent = getLayoutParent();
    if (parent != null) {
      int size = masterRow.getSizeLimit(Direction.Y).getCurrent();
      int pos = calcRowPos( rowIndex - scrollerY.getValue() );
      parent.scrollToVisible(Direction.Y, pos + getPos(Direction.Y), size);
    }
  }

  /**
   * Calculates this component's min, max and pref sizes in one direction and
   * stores them in sizeLimit
   *
   * @param direction
   * @param sizeLimit
   */
  public void calcSizes (Direction direction, SizeLimit sizeLimit) {
    SizeLimit headSize = headRow.getSizeLimit(direction);
    SizeLimit rowSize = masterRow.getSizeLimit(direction);
    int tableSizeMin;
    int tableSizePref;
    int tableSizeMax;

    if (getPaintState(direction).hasPaintStateReached(PaintState.STATE_SIZES_CALC)) {
      return;
    }

    if (direction == Direction.X) {
      tableSizeMin = Tools.max(headSize.getMin(), rowSize.getMin()) + scrollerY.getSizeLimit( direction ).getMin();
      tableSizePref = Tools.max(headSize.getPref(), rowSize.getPref()) + scrollerY.getSizeLimit( direction ).getPref();
      tableSizeMax = Tools.max(headSize.getMax(), rowSize.getMax());
    }
    else {
      short minBody = Tools.max((short) (minVisibleRows * rowSize.getPref()), scrollerY.getSizeLimit(direction).getMin());
      tableSizeMin = (short) (headSize.getPref() + minBody);
      if (visibleRows == Short.MAX_VALUE) {
        tableSizePref = Short.MAX_VALUE;
        tableSizeMax = Short.MAX_VALUE;
      }
      else {
        int temp = headSize.getPref() + this.visibleRows * rowSize.getPref();
        if (temp > Short.MAX_VALUE) tableSizePref = Short.MAX_VALUE;
        else tableSizePref = (short) temp;
        tableSizeMax = tableSizePref;
        tableSizeMin = tableSizePref;    
      }
    }

    sizeLimit.set(tableSizeMin, tableSizePref, tableSizeMax);

    getPaintState(direction).doPaintState(PaintState.STATE_SIZES_CALC);
  }

  public int rowCount () {
    if (listener == null) {
      return 0;
    }
    else {
      return ((UITableAdapter) listener).getDisplayRowCount();
    }
  }

  public int columnCount() {
    return masterRow.childCount();
  }

  public int firstVisibleRow() {
    return scrollerY.getValue();
  }

  /**
   * Returns the number of rows completely displayable in this binding. A further row
   * may be displayed partial.
   * @return the number of rows displayable in this binding
   */
  public int visibleRowCount() {
    short tableHeight = getSizeLimit(Direction.Y).getCurrent();
    short headHeight = headRow.getSizeLimit(Direction.Y).getCurrent();
    short rowHeight = masterRow.getSizeLimit(Direction.Y).getCurrent();
    if (rowHeight == 0) return 1;
    else return ((tableHeight - headHeight) / rowHeight) + 1;
  }

  /**
   * Calculate the position of the children.
   * This method is empty for normal components, but needs implementaion
   * for containers.
   */
  public void doLayout (Direction direction) {
    short currentSize = getSizeLimit(direction).getCurrent();
    PaintState paintState = getPaintState(direction);
    if (!paintState.hasPaintStateReached(PaintState.STATE_BOUNDS_SET)) {
      return;
    }

    if (direction == Direction.X) {
      short scrollerWidth = this.scrollerY.getSizeLimit(direction).getPref();
      short headSize = (short) (currentSize - scrollerWidth);
      headRow.getSizeLimit(direction).setCurrentFit(headSize);
      masterRow.getSizeLimit(direction).setCurrentFit(headSize);
      //----- copy headRow layout to masterRow
      Layoutable headChild, bodyChild;
      for (int i = 0; i < headRow.childCount(); i++) {
        headChild = headRow.getChild(i);
        bodyChild = masterRow.getChild(i);
        bodyChild.getSizeLimit(direction).setCurrentFit(headChild.getSizeLimit(direction).getCurrent());
        bodyChild.setPos(direction, headChild.getPos(direction));
      }
    }
    else {
      short headSize = headRow.getSizeLimit(direction).getPref();
      headRow.getSizeLimit(direction).setCurrent(headSize);
      short bodySize = masterRow.getSizeLimit(direction).getPref();
      masterRow.getSizeLimit(direction).setCurrent( bodySize );
    }

    doScrollerLayout(direction);

    paintState.doPaintState(PaintState.STATE_LAYOUTED);
  }

  public void doScrollerLayout(Direction direction) {
    //-- Make shure data at least as much rows as length
    for (int i = data.size(); i < visibleRows; i++) {
      data.addElement( new Object[columnCount()] );
    }

    PaintState paintState = getPaintState(direction);
    if (!paintState.hasPaintStateReached(PaintState.STATE_BOUNDS_SET)) {
      return;
    }

    short currentSize = getSizeLimit(direction).getCurrent();
    if (direction == Direction.X) {
      short scrollerWidth = this.scrollerY.getSizeLimit(direction).getPref();
      scrollerY.getSizeLimit(direction).setCurrentFit( scrollerWidth );
      scrollerY.setPos( direction, (short) (currentSize - scrollerWidth) );
    }
    else {
      int count = rowCount();
      int visibleRows = visibleRowCount();
      int length = visibleRows - 1;  // last row may be clipped
      if (length > count) length = count;

      if (count < minVisibleRows) {
        count = minVisibleRows;
        length = minVisibleRows;
      }
      if (length <= 0) length = 1;
      scrollerY.setValueConstraints( 0, count, length, 1, (length / 2));
      short headSize = headRow.getSizeLimit(direction).getPref();
      scrollerY.getSizeLimit(direction).setCurrent( (short) (currentSize - headSize) );
      scrollerY.setPos( direction, headSize );
      scrollerY.doLayout(direction);

      if (listener != null) {
        short firstRow = (short) scrollerY.getValue();
        listener.setVisibleRange( getID(), (short) 0, firstRow, (short) columnCount(), (short) (firstRow + visibleRows) );
      }
    }
  }

  private void doPaintScroller (Scroller scroller, ComponentGraphics compGraph, short size) {
    short scrollerWidth = scroller.getSizeLimit(Direction.X).getCurrent();
    short scrollerHeight = scroller.getSizeLimit(Direction.Y).getCurrent();
    short offsetX = 0;
    short offsetY = scroller.getPos(Direction.Y);

    if (scroller.getDirection() == Direction.X) {
      offsetY = (short) (size - scrollerHeight);
    } else {
      offsetX = (short) (size - scrollerWidth);
    }
    compGraph.translate( offsetX, offsetY);
    Rect oldClip = new Rect();
    compGraph.getClip(oldClip);
    compGraph.clipRect((short) 0, (short) 0, scrollerWidth, scrollerHeight);

    scroller.doPaint(compGraph);

    compGraph.setClip(oldClip);
    compGraph.translate((short) -offsetX,  (short) -offsetY);
  }

  /**
   * Called for painting this component
   * @param graph the graphics context to use for painting
   */
  public void doPaintImpl (ComponentGraphics graph) {
    if (!allowPaint()) {
      return;
    }
    if (DEBUG) Logger.debug( "-> Table.doPaintImpl");

    doScrollerLayout( scrollerY.getDirection() );

    //----- Draw background
    short width = getSizeLimit( Direction.X ).getCurrent();
    short height = getSizeLimit( Direction.Y ).getCurrent();
    graph.paintBackground(getBackground(), 0,  0, width, height);

    //short firstVisibleRow = (short) scrollerY.getValue();
    masterRow.getComponentState().setVisible(true);
    int paintRowCount = visibleRowCount();
    int firstVisibleRow = scrollerY.getValue();
    if (paintRowCount > (rowCount() - firstVisibleRow)) {
      paintRowCount = rowCount() - firstVisibleRow;
    }
    for (short i=0; i < paintRowCount; i++) {
      moveMasterRow (i);
      short posX = masterRow.getPos( Direction.X );
      short posY = masterRow.getPos( Direction.Y );
      graph.translate( posX, posY );
      masterRow.doPaint(graph);
      graph.translate( (short) -posX, (short) -posY );
    }
    masterRow.getComponentState().setVisible(false);

    doPaintScroller(scrollerY, graph, width);

    // paint header + editcomp
    doPaintChildren( this, graph );
    
    // fill area right of headRow above scroller with background color of headRow
    Border hb = headRow.getBorder();
    int x = headRow.getPos( Direction.X ) + headRow.getSizeLimit( Direction.X ).getCurrent() - hb.getRight();
    int y = headRow.getPos( Direction.Y ) + hb.getTop();
    int w = width - x - hb.getRight();
    int h = headRow.getSizeLimit( Direction.Y ).getCurrent() - hb.getTop() - hb.getBottom();
    graph.paintBackground( headRow.getBackground(), x, y, w, h );

    if (DEBUG) Logger.debug( "<- Table.doPaintImpl");
  }


  public void repaint() {
    if (scrollerY != null) doScrollerLayout( scrollerY.getDirection() );
    super.repaint();
  }

  public void initData(int visibleRowIndex, Panel masterRow ) {
    if (visibleRowIndex < data.size()) {
      Object[] row = (Object[]) data.elementAt( visibleRowIndex );
      int colCount = row.length;
      for (int colIndex = 0; colIndex < colCount; colIndex++) {
        ((Component) masterRow.getChild( colIndex )).updateValue( (short) 0, (short) 0, StringType.getDefString(), row[colIndex] );
      }
    }
  }

  private void moveMasterRow( short rowNum ) {
    int   rowPos = 0;
    ComponentColor background;

    rowPos = calcRowPos(rowNum);
    masterRow.setPos(Direction.Y, (short) rowPos);

    try {
      initData(rowNum, masterRow);
    }
    catch (Exception ex) {
      Logger.error( "Error initializing table row #"+rowNum, ex );
    }

    if (rowNum == this.selectedRow) {
      if (getComponentState().hasFocus()) {
        background = activeRowFocusColor;
      }
      else {
        background = activeRowSelColor;
      }
    }
    else {
      background = normalRowColor;
    }
    for (int i = 0; i < masterRow.childCount(); i++) {
      ((Component) masterRow.getChild(i)).setBackground(background);
    }
  }

  private int calcRowPos (int rowNum) {
    short headHeight = 0;
    short rowHeight = 0;
    if (headRow != null) {
      headHeight = headRow.getSizeLimit(Direction.Y).getCurrent();
    }
    rowHeight = masterRow.getSizeLimit(Direction.Y).getCurrent();
    int rowPos = headHeight + (rowNum * rowHeight); // todo + border usw.
    if (rowPos > Short.MAX_VALUE ) {
      rowPos = Short.MAX_VALUE;
    }
    return rowPos;
  }

  public void showEditor ( Component editorComp, int rowNum, int colNum) {
    resetEditComp();
    if (editorComp != null) {
      editComp = editorComp;
      editComp.setVisible(true);
      editPopup.add(editComp);

      // position editor on selected cell
      moveMasterRow( (short) selectedRow );
      Layoutable cellComp = masterRow.getChild(colNum);

      short editWidth = calcEditSize(cellComp, Direction.X);
      short editHeight = calcEditSize(cellComp, Direction.Y);

      editComp.getSizeLimit(Direction.X).setCurrentFit(editWidth);
      editComp.getSizeLimit(Direction.Y).setCurrentFit(editHeight);

      Position editPos = calcEditPos( cellComp, editComp.getSizeLimit( Direction.Y ).getCurrent() );

      editPopup.showPopup(this, editPos, editWidth, editHeight);
      editComp.setState( (short) -1, (short) -1, Boolean.TRUE, null, null, null );
    }
  }

  private Position calcEditPos( Layoutable cellComp, int editorHeight) {
    int x = calcPosInWindow( cellComp, Direction.X );
    int dy = (editorHeight - cellComp.getSizeLimit( Direction.Y ).getCurrent()) / 2;
    int y = calcPosInWindow( cellComp, Direction.Y ) - dy;

    return new Position((short) x, (short) y);
  }

  private short calcEditSize( Layoutable cellComp, Direction direction ) {
    SizeLimit renderSize = cellComp.getSizeLimit(direction);
    return renderSize.getCurrent();
  }

  public void resetEditComp () {
    if (editComp != null) {
      editComp.setVisible(false);
      if (editPopup.isVisible()) {
        editPopup.closePopup();
      }
      editPopup.removeAll();
      editPopup.getPaintState(Direction.X).undoPaintState(PaintState.STATE_SIZES_CALC);
      editPopup.getPaintState(Direction.Y).undoPaintState(PaintState.STATE_SIZES_CALC);
      editComp = null;
    }
  }

  public int findColumn (Position posInComp) {
    int childCount = masterRow.childCount();
    Layoutable child;
    short x = posInComp.getX();
    short y = posInComp.getY();
    for (int i=0; i <childCount; i++) {
      child = masterRow.getChild(i);
      if (containsPos( child, Direction.X, x ) && containsPos( child, Direction.Y, y )) {
        return i;
      }
    }
    return -1;
  }

  /**
   * Call the action valid for the mouse event.
   * The action is defined in the controller.
   * @param state
   * @param lastPointedComponent
   * @param posInComp
   */
  public void processMouseEvent(int state, Component lastPointedComponent, Position posInComp) {
    if (state == InputManager.STATE_SELECTED || state == InputManager.STATE_LOCAL_MENU ){
      if (lastPointedComponent == this) {

        Component comp = super.findComponentAt(posInComp) ;
        int selectedRow = -1;
        int colNum = -1;

        // no hit in master row
        if (comp == this) {
          short rowHeight = getMasterRow().getSizeLimit(Direction.Y).getCurrent();
          int firstVisibleRow = scrollerY.getValue();
          selectedRow = findRow(posInComp);
          if ((firstVisibleRow + selectedRow) <= rowCount()) {
            int y = calcRowPos(selectedRow);
            posInComp.translate((short) 0, (short) y);
            colNum = findColumn(posInComp);
            if (colNum >= 0) {
              Layoutable child = masterRow.getChild(colNum);
              scrollToVisible(Direction.X, child.getPos(Direction.X), child.getSizeLimit(Direction.X).getCurrent());
              scrollToVisible(Direction.Y, y, rowHeight);
            }
          }
        }

        this.setState( (short) colNum, (short) selectedRow, Boolean.TRUE, Boolean.TRUE, null, null );
        if (listener != null) {
          listener.selected( this.getID(), (short) colNum, (short) selectedRow, componentState.isSelected() );
        }
      }
    }
    else if (state == InputManager.STATE_DOUBLECLICK ){
      if (listener != null) {
        listener.command( getID(), InputManager.CMD_OPEN, ' ', 0 );
      }
    }
    else if (state == InputManager.STATE_SCROLL) {
      processScroller( posInComp );
    }
  }

  /**
   * Find row number at given position.
   * @param posInComp
   * @return the row number
   */
  private int findRow(Position posInComp) {
    short rowHeight = getMasterRow().getSizeLimit(Direction.Y).getCurrent();
    short headHeight = getHeadRow().getSizeLimit(Direction.Y).getCurrent();
    int selectedRow = (posInComp.getY() - headHeight) / rowHeight;
    return selectedRow;
  }

  public Component findComponentAt(Position pos){
    short x = pos.getX();
    short y = pos.getY();
    short xOffset = x;

    if (scrollerY != null && scrollerY.isVisible()) {
      if ( containsPos( scrollerY, Direction.X, xOffset ) && containsPos( scrollerY, Direction.Y, y )) {
        pos.translate((short)scrollerY.getPos(Direction.X), (short)scrollerY.getPos(Direction.Y));
        return scrollerY;
      }
    }

    //search in container
    return super.findComponentAt( pos );
  }
}
