/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.gui.guidef.DialogDef;
import de.pidata.models.tree.Model;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

public interface GuiBuilder {

  public static final Namespace NAMESPACE = ComponentFactory.NAMESPACE;

  public static final QName ID_COLS = NAMESPACE.getQName("cols");
  public static final QName ID_ROWS = NAMESPACE.getQName("rows");

  public static final QName ID_SCROLLNONE = NAMESPACE.getQName("none");
  public static final QName ID_SCROLLBOTH = NAMESPACE.getQName("both");
  public static final QName ID_SCROLLHOR = NAMESPACE.getQName("horizontal");
  public static final QName ID_SCROLLVERT = NAMESPACE.getQName("vertical");

  public static final QName ID_GRIDLAYOUTER = NAMESPACE.getQName("GridLayouter");
  public static final QName ID_STACKLAYOUTER = NAMESPACE.getQName("StackLayouter");
  public static final QName ID_FLOWLAYOUTER = NAMESPACE.getQName("FlowLayouter");

  public static final QName ID_ALIGNLEFT = NAMESPACE.getQName("left");
  public static final QName ID_ALIGNRIGHT = NAMESPACE.getQName("right");
  public static final QName ID_ALIGNTOP = NAMESPACE.getQName("top");
  public static final QName ID_ALIGNBOTTOM = NAMESPACE.getQName("bottom");
  public static final QName ID_ALIGNCENTER = NAMESPACE.getQName("center");
  public static final QName ID_ALIGNGROW = NAMESPACE.getQName("grow");
  
  public Dialog createDialog( DialogDef dialogDef );

  void setModuleContainer( Model moduleContainer );

  void addShapeClass( QName typeID, Class shapeClass );
}
