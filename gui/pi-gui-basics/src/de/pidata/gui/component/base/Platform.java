/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.connect.bluetooth.SPPConnection;
import de.pidata.connect.stream.MessageSplitter;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.event.InputManager;
import de.pidata.gui.guidef.*;
import de.pidata.gui.layout.Layouter;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIFactory;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.config.Configurator;
import de.pidata.models.config.DeviceTable;
import de.pidata.models.service.ServiceManager;
import de.pidata.models.tree.*;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.string.Helper;
import de.pidata.system.base.BackgroundSynchronizer;
import de.pidata.system.base.ModelReader;
import de.pidata.system.base.Storage;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.SystemManager;

import java.io.InputStream;
import java.util.*;

public abstract class Platform extends Thread {

  public static final QName NAVKEYS_KEYBOARD = ComponentFactory.NAMESPACE.getQName("keyboard");
  public static final QName NAVKEYS_2_WAY = ComponentFactory.NAMESPACE.getQName("2-way");
  public static final QName NAVKEYS_4_WAY = ComponentFactory.NAMESPACE.getQName("4-way");

  public static boolean loadServices = true;

  private static Platform instance;

  private Configurator configurator;
  private Hashtable borders = new Hashtable();
  private DialogController initialDlgCtrl;
  private Application app;
  private int startupProgress = 0;

  private WeakHashMap<QName,ComponentBitmap> imageCache = new WeakHashMap<QName,ComponentBitmap>();

  public static Platform getInstance() {
    return instance;
  }

  /**
   * Switch focus to the given component if possible
   * @param toView the destination component for focus switch
   */
  public static void switchFocusTo( ViewPI toView ) {
    UIAdapter uiAdapter = toView.getUIAdapter();
    if (uiAdapter != null) {
      uiAdapter.setState( (short) -1, (short) -1, Boolean.TRUE, null, null, null );
    }
  }

  protected Platform() {
    if (instance != null) {
      throw new IllegalArgumentException( "Must not create a second instance of Platform!" );
    }
    instance = this;
  }

  protected void init( Context context, String[] args ) throws Exception {
    new ServiceManager();
    loadConfig( context );
    loadGui( context, args );
  }

  protected abstract void initComm( Context context ) throws Exception;

  public void loadServices( Context context ) throws Exception {
    if (loadServices) {
      //----- Create Synchronizer
      String syncModeStr = SystemManager.getInstance().getProperty("sync.mode", null );
      if ((syncModeStr != null) && (!syncModeStr.toUpperCase().equals( "NONE" ))) {
        BackgroundSynchronizer syncronizer = BackgroundSynchronizer.getInstance();
        if (syncronizer == null) {
          syncronizer = (BackgroundSynchronizer) Class.forName( "de.pidata.service.client.SystemSynchronizer" ).newInstance();
        }
      }
      initComm(context);
    }
  }

  protected void loadConfig( Context context ) throws Exception {
    Storage configStorage = SystemManager.getInstance().getStorage( null );
    InputStream configStream = null;
    if (configStorage.exists( "config.xml" )) {
      try {
        configStream = configStorage.read( "config.xml" );
        setConfigurator( Configurator.loadConfig( configStream, context ) );
      }
      finally {
        StreamHelper.close( configStream );
      }
    }
    else {
      Logger.info( "Config file not found: "+configStorage.getPath( "config.xml" ) );
    }
  }

  protected void loadGui( Context context, String[] args ) throws Exception {
    String guiFile;
    ControllerBuilder builder;
    if (args.length > 0) {
      guiFile = args[0] + ".xml";
    }
    else {
      guiFile = SystemManager.getInstance().getProgramName() + ".xml";
    }
    loadGui( context, guiFile );
    startGui( context );
  }

  public void setStartupProgress( int percent ) {
    this.startupProgress = percent;
    Logger.info( "startup progress: " + percent + "%" );
  }

  public int getStartupProgress() {
    return startupProgress;
  }

  public Configurator getConfigurator() {
    return configurator;
  }

  protected void setConfigurator(  Configurator configurator ) {
    this.configurator = configurator;
  }

  /**
   * Returns this platform's name
   * @return this platform's name
   */
  public abstract String getPlatformName();

  /**
   * Returns the name of the current device
   * @return name of the current device
   */
  public String getDevice() {
    return System.getProperty("os.name").toLowerCase();
  }

  public Storage getGuiStorage() {
    SystemManager sysMan = SystemManager.getInstance();
    String path = "gui";
    String version = sysMan.getProgramVersion();
    if (version != null) {
      path = path + "_" + version;
    }
    Storage storage = sysMan.getStorage( path );
    if (!storage.exists( sysMan.getProgramName()+".xml" )) {
      storage = sysMan.getStorage( "gui" );
    }
    return storage;
  }

  /**
   * Returns the platform specific statup file name for programName.
   * For example a Windows desktop platfrom appends programName with ".bat"
   * @param programName the program name
   * @return the platform specific statup file name for programName
   */
  public abstract String getStartupFile( String programName );

  /**
   * Returns the sorage wher startfiles are placed on this platform
   * @return the sorage wher startfiles are placed on this platform 
   */
  public abstract Storage getStartupFileStorage();

  public void addToImageCache( QName imageID, ComponentBitmap bitmap ) {
    imageCache.put( imageID, bitmap );
  }

  /**
   * Creates ComponentBitmap with platform dependant Graphics of size width x height
   * @param width  the new ComponentBitmap's width
   * @param height the new ComponentBitmap's height
   * @return a platform depandant ComponentBitmap
   */
  public abstract ComponentBitmap createBitmap(int width, int height);

  /**
   * Loads a bitmap from the given file system path.
   *
   * @param path the path of a image file (gif or jpeg)
   * @return the bitmap loaded
   */
  public abstract ComponentBitmap loadBitmapFile( String path );

  public abstract ComponentBitmap loadBitmap( InputStream imageStream);

  public abstract ComponentBitmap loadBitmapResource( QName bitmapID );

  public abstract ComponentBitmap loadBitmapAsset( QName bitmapID );

  /**
   * Returns bitmap for the given ID: If path starts with '@' bitmap is
   * fetched form resources. Otherwise bitmap ist fetched from file system.
   * If not present in file system it is fetched from assets.
   *
   * In any case image is cached by platform (key is bitmapID).
   *
   * @param bitmapID the ID addressing the bitmap
   * @return the bitmap loaded
   */
  public final ComponentBitmap getBitmap( QName bitmapID ) {
    if (bitmapID == null) {
      return null;
    }
    String imagePath = bitmapID.getName();
    ComponentBitmap componentBitmap = imageCache.get( bitmapID );
    if (componentBitmap == null) {
      if (imagePath.startsWith( "@" )) {
        componentBitmap = loadBitmapResource( bitmapID );
      }
      else {
        componentBitmap = loadBitmapFile( bitmapID.getName() );
        if (componentBitmap == null) {
          componentBitmap = loadBitmapAsset( bitmapID );
        }
      }
      if (componentBitmap != null) {
        imageCache.put( bitmapID, componentBitmap );
      }
    }
    return componentBitmap;
  }

  /**
   * Loads a thumb nail image, fitting into rectangle width, height
   *
   * @param imageStorage
   * @param width        desired with
   * @param height       desired height
   * @return thumb nail for image from imageSteam
   */
  public abstract ComponentBitmap loadBitmapThumbnail( Storage imageStorage, String imageFileName, int width, int height );

  /**
   * Add new color to platform's color table. If a color with same name already exists it is replaced
   *
   * @param name  color's name
   * @param red   red (0 .. 255)
   * @param green green (0 .. 255)
   * @param blue  blue (0 .. 255)
   * @param alpha alpha value / opacity (0.0 .. 1.0)
   * @return
   */
  public abstract ComponentColor setColor( QName name, int red, int green, int blue, double alpha );

  public abstract ComponentColor getColor(QName colorID);

  public abstract void setColor(QName colorID, String value);

  public abstract void setFont(QName fontID, String name, QName style, int size);

  public abstract ComponentFont createFont(String name, QName style, int size);

  public abstract ComponentFont getFont(QName fontID);

  public Border getBorder( QName borderID ) {
    return (Border) borders.get( borderID );
  }

  public void setBorder( QName borderID, Border border ) {
    borders.put( borderID, border );
  }

  /**
   * Returns this platform's screen object
   * @return this platform's screen object
   */
  public abstract Screen getScreen();

  public abstract ComponentFactory getComponentFactory();

  public abstract InputManager getInputManager();

  /**
   * Set some platform characteristics, e.g. make skinDialog focusable in JDK 1.4,
   * toggle keyboard on screen.
   * This method is needed since j2sdk1.4.* requires
   * calling setFocusable on Windows to allow keyboard input.
   * @param dialog the dialog to make focusable
   */
  public abstract void initWindow(Object dialog);

  /**
 * Shows or hides the on screen keyboard
 * @param hasFocus if true the on screen keyboard is shown, otherwise hidden
 */
  public abstract void toggleKeyboard(boolean hasFocus);

  public abstract boolean isSingleWindow ();

  public abstract boolean useDoubleBuffering ();

  /**
   * Returns true if cursor keys should be used as alternative to TAB key for moving
   * focus within dialogs, e.g. on windows mobile there are cursor buttons or track balls
   * but no TAB keys.
   * @return true if cursor keys should be used for moving focus
   */
  public boolean useCursor4Focus() {
    return true;
  }

  /**
   * Returns the platform specific color specified by the given colorString.
   * The colorString should be in a platform neutral format which can be interpreted by all platforms. Be careful
   * in using platform specific formats here!
   *
   * @param colorString
   * @return
   */
  public abstract ComponentColor getColor(String colorString);

  /**
   * Returns the platform specific color specified by the given values.
   *
   * @param red   red (0 .. 255)
   * @param green green (0 .. 255)
   * @param blue  blue (0 .. 255)
   * @return
   */
  public abstract ComponentColor getColor( int red, int green, int blue );

  /**
   * Returns the platform specific color specified by the given values.
   *
   * @param red   red (0 .. 255)
   * @param green green (0 .. 255)
   * @param blue  blue (0 .. 255)
   * @param alpha alpha value / opacity (0.0 .. 1.0)
   * @return
   */
  public abstract ComponentColor getColor( int red, int green, int blue, double alpha );

  /**
   *
   */
  public abstract Dialog createDialog(Layouter layouterX, Layouter layouterY, short x, short y, short width, short height);

  /**
   * Returns this platform's navigation capability: full keyboard, 4-way or 2-way
   * @return one of the constants NAVKEYS_*
   */
  public abstract QName getNavigationKeys();

  /**
   * Called to exit application, e.g. via System.exit(0) on J2SE desktop
   * @param context
   */
  public abstract void exit( Context context );

  /**
   * Returns true if FontMetrics.charWidth() is working on this Platform. On some platforms
   * charWidth returns 0 on others (e.g. J0 on WinMobile 5) it takes more than 500ms per character. 
   * @return true if FontMetrics.charWidth() is working on this Platform
   */
  public abstract boolean isCharWidthUsable();

  public abstract ControllerBuilder getControllerBuilder();

  public void loadGui( Context context, String filename ) throws Exception {
    Data data;

    Logger.debug("loading gui...");
    ModelFactoryTable.getInstance().getOrSetFactory( ControllerFactory.NAMESPACE, ControllerFactory.class );
    Storage guiStrorage = Platform.getInstance().getGuiStorage();
    InputStream inStream = null;
    try {
      if (filename.endsWith(".xml")) {
        inStream = guiStrorage.read( filename );
        this.app = (Application) new XmlReader().loadData( inStream, null );
      }
      else {
        throw new IllegalArgumentException("Unknown gui file type (expected .xml), filename="+ filename );
      }
    }
    finally {
      StreamHelper.close( inStream );
    }
    Logger.debug("...gui loaded");

    //----- Load external data factories
    Root dataRoot = context.getDataRoot();
    for (ModelIterator dataIter = this.app.dataIter(); dataIter.hasNext(); ) {
      data = (Data) dataIter.next();
      String storage;
      if (data.getStorage() == null) storage="XML";
      else storage = data.getStorage().getName().toUpperCase();
      ModelReader modelReader = null;
      try {
        if (storage.equals( "XML" )) {
          throw new RuntimeException( "TODO" );
          //Logger.info( "loading XML data path="+data.getPath() );
          //XmlData xmlData = new XmlData(data.getPortTypeName(), data.getPath(), dataRoot);
        }
        else if (storage.equals( "BO-XML" )) {
          throw new RuntimeException( "TODO" );
          //Logger.info( "loading BO-XML data for ns="+data.getNamespace()+", path "+data.getPath() );
          //Namespace ns = Namespace.getInstance( data.getNamespace() );
          //BoXmlData xmlData = new BoXmlData( data.getPortTypeName(), data.getPath(), dataRoot, ns );
        }
        else if (storage.equals( "CSV" )) {
          Logger.info( "loading CSV data for ns=" + data.getNamespace() + ", path " + data.getPath() );
          modelReader = (ModelReader) Class.forName( "de.pidata.models.csv.CSVReader" ).newInstance();
        }
      }
      catch (Exception ex) {
        String msg = "Error creating ModelReader for storage '"+storage+"'";
        Logger.error( msg, ex );
        throw new IllegalArgumentException( msg );
      }
      if (modelReader == null) {
        throw new IllegalArgumentException( "Unknown data source, storage="+storage );
      }
      else {
        Namespace ns = Namespace.getInstance( data.getNamespace() );
        Model model = modelReader.loadData( data.getPath(), data.namespaceTable() );
        ModelFactoryTable.getInstance().getFactory( ns ).addRootRelation( model.type().name(), model.type(), 0, 1, null );
        dataRoot.add( model.type().name(), model );
      }
    }

    //----- Move StringTables from app definition to dataRoot
    for (ModelIterator strTabIter = this.app.stringTableIter(); strTabIter.hasNext(); ) {
      StringTable strTab = (StringTable) strTabIter.next();
      app.removeStringTable(strTab);
      dataRoot.add(strTab.type().name(), strTab);
    }

    getControllerBuilder().init( this.app, createGuiBuilder() );
  }

  protected abstract GuiBuilder createGuiBuilder() throws Exception;

  public void startGui( Context context ) throws Exception {
    QName serviceID, opID;

    //----- Start initial Dialog or service
    Logger.debug("launching initial dialog...");
    serviceID = this.app.getService();
    opID = this.app.getOperation();
    if (opID != null) {
      if (serviceID == null) {
        openInitialDialog( context, opID );
      }
      else {
        //----- Load Services in background
        this.run();

        ServiceManager.getInstance().invokeService(context, serviceID.getName(), opID.getName(), null);
      }
    }
  }

  protected abstract void openInitialDialog( Context context, QName opID ) throws Exception;

  /**
   * Update static text in uiContainer by properties from language file for dialogID.
   * Befor writing to UI entries enclosed in "{GLOSSARYNAME}" are replaced by matching
   * entry from language file "glossary".
   *
   * @param uiContainer the uiContainer to be updated
   * @param dialogID    the dialogID uses as property filename
   * @param language    the language, e.g. "de". If null system default is used.
   */
  public void updateLanguage( UIContainer uiContainer, QName dialogID, String language ) {
    if (dialogID != null) {
      SystemManager sysMan = SystemManager.getInstance();
      Properties langProps = sysMan.getLanguageProps( dialogID.getName(), language );
      if (langProps != null) {
        Properties glossaryProps = sysMan.getGlossaryProps( language );
        getUiFactory().updateLanguage( uiContainer, langProps, glossaryProps );
      }
    }
  }

  public void run() {
    Namespace ns;

    try {
      if (initialDlgCtrl != null)  {
        synchronized(initialDlgCtrl) {
          initialDlgCtrl.wait();
        }
      }
      setStartupProgress( 70 );

      if (loadServices) {
        //----- Load service factories. We use Class.forName to avoid earlier class loading.
        //      This was removed from system.properties factory list to reduce system statup time.
        Logger.debug( "loading service factories..." );
        ns = Namespace.getInstance( "http://schemas.xmlsoap.org/soap/envelope/" );
        if (ModelFactoryTable.getInstance().getFactory( ns ) == null) {
          Class.forName( "de.pidata.comm.soap.SoapFactory" ).newInstance();
        }
        ns = Namespace.getInstance( "http://www.pidata.de/res/service/log" );
        if (ModelFactoryTable.getInstance().getFactory( ns ) == null) {
          Class.forName( "de.pidata.service.log.LogFactory" ).newInstance();
        }
        ns = Namespace.getInstance( "http://schemas.xmlsoap.org/wsdl/" );
        if (ModelFactoryTable.getInstance().getFactory( ns ) == null) {
          Class.forName( "de.pidata.wsdl.WSDLFactory" ).newInstance();
        }

        //----- Load WSDL
        Logger.debug( "loading services..." );
        ServiceManager serviceMgr = ServiceManager.getInstance();
        serviceMgr.setActive( true );
        Logger.debug( "... services loaded" );
      }

      //----- Load to do list to dataRoot
      // TodoListManager.getInstance().loadTodoList( context.getDataRoot() );

      //----- Start system synchronizer
      BackgroundSynchronizer sync = BackgroundSynchronizer.getInstance();
      if (sync == null) {
        Logger.debug("synchronizer is disabled");
      }
      else {
        Logger.debug("starting synchronizer...");
        BackgroundSynchronizer.getInstance().start();  //TODO create SyncListener instance
        Logger.debug("synchronizer started...");
      }
      setStartupProgress( 100 );
    }
    catch (Exception ex) {
      Logger.error("Error loading services", ex);
    }
  }

  public abstract UIFactory getUiFactory();

  public abstract PlatformScheduler createScheduler();

  public abstract boolean hasTableFirstRowForEmptySelection();

  /**
   * Adds paired Bluetooth devices to deviceTable
   *
   * @param deviceTable table for the result
   */
  public abstract void getPairedBluetoothDevices( DeviceTable deviceTable );

  public abstract SPPConnection createBluetoothSPPConnection( String serverAddress, String uuidString, MessageSplitter messageSplitter );

  public abstract MediaInterface getMediaInterface();

  public boolean isOnUiThread() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO: please implement Platform specific" );
  }

  public abstract void runOnUiThread( Runnable runnable);

  /**
   * Open url using System default web browser
   * @param url the url to be opened
   * @return false if an error/exception occurred calling browser
   */
  public abstract boolean openBrowser( String url );
  
  public abstract NfcTool getNfcTool();
}
