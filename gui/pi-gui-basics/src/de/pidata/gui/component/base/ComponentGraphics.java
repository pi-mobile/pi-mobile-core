/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

public interface ComponentGraphics {
  /**
   * render mode : resizes skinned to given width and height
   */
  public static final short RESIZE = 1;
  /* render mode : uses the inner graphic of the skinned and resizes it to
   * given width and height
   * may result in curious images
   * for plain background color use render mode EXPLODE
   */
  public static final short EXPLODE = 2;
  /* render mode : uses the color in the middle of the skinned as background color
   * for the rest of the skinned
   * to display icons etc. use render mode EXPLODE
   */
  //TODO funktioniert nicht mit Personal Java
  public static final short FILLED = 3;
  /**
   * render mode : "deletes" the middle skinned of a component. the result is a "bordered" skinned
   * with black background color in the middle
   */
  public static final short BORDERED = 4;

  /**
   * render mode :
   * not working properly yet  todo: raus, weil anders gelöst?
   */
  //public static final short TILED   = 5;
  /**
   * render mode : centers skinned in a bufferedImage with given width and heigth
   * not working properly yet
   */
  //public static final short CENTER  = 6;


  /**
   * Translate and clip graph
   * @param rect
   */
  void setClip(Rect rect);

  Object getGraph ();

  void getClip (Rect rect);

  void paintString(String string, int posX, int posY, ComponentColor compColor, ComponentFont compFont);

  void paintString (char[] buffer, int offset, int length, int posX, int posY, ComponentColor compColor, ComponentFont compFont);

  void paintLine (short x1, short y1, short x2, short y2);

  void paintRect(short x, short y, short width, short height);

  void paintEllipse (short x1, short y1, short width, short height);

  void clipRect (short x, short y, short width, short height);

  void translate (short offsetX, short offsetY);

  void setGraph (Object graph);

  void fillRect(int x, int y, int width, int height);

  void paintBackground(ComponentColor compColor, int x, int y, int width, int height);

  void setColor(ComponentColor compColor);

  void setFont(ComponentFont font);

  void paintBitmap(ComponentBitmap componentBitmap, int x, int y, int width, int height);

  /**
   * renders an skinned to a specific size in a specific mode
   *
   * @param width  width after render process
   * @param height height after render process
   * @param mode   render mode
   *               RESIZE
   *               EXPLODE
   *               BORDERED
   *               FILLED
   *               STRETCHED
   */
  void paintImage(ComponentBitmap componentBitmap, int x, int y, int width, int height, int mode)  // end of paintImage(...)
      ;
}
