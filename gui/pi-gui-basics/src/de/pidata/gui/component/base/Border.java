/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

/**
 * Created by IntelliJ IDEA.
 * User: Doris
 * Date: 02.06.2004
 * Time: 17:33:09
 * To change this template use File | Settings | File Templates.
 */
public interface Border {

  public short getTop();

  public short getLeft();

  public short getRight();

  public short getBottom();

  /**
   * Adds the two border sizes in one direction and returns the result.
   * For direction X this is the size of the left and right border, for direction Y it is
   * the top and bottom border.
   * @param direction
   * @return the total border size in one direction
   */
  public short get2BorderSize(Direction direction);

  public short getOffset (Direction direction);

  /**
   * Does the paint of this border
   * @param graph the graphicsImage of this border
   * @param width the outer width of this border
   * @param height the outer height of this border
   */
  void doPaint(ComponentGraphics graph, int width, int height);
}
