/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.gui.event.TextWrapListener;
import de.pidata.qnames.QName;

public class TextRow {
  private static final boolean DEBUG = false;

  protected int borderWidth = 0;

  private QName lineID;

  private CharBuffer buffer;
  private int startOffset;
  private int length;
  private int charsVisible;

  private TextWrapListener wrapListener;

  /** Creates a new TextField.
   */
  public TextRow( QName lineID, CharBuffer buffer, int charsVisible) {
    this.lineID = lineID;
    this.buffer = buffer;
    this.charsVisible = charsVisible;
    this.startOffset = 0;
    this.length = -1;
  }

  public TextWrapListener getWrapListener() {
    return wrapListener;
  }

  public void setWrapListener(TextWrapListener wrapListener) {
    this.wrapListener = wrapListener;
  }

  public void setBuffer(CharBuffer buffer) {
    setBuffer(buffer, 0, -1);
  }

  public void setBuffer(CharBuffer buffer, int startOffset, int length) {
    this.buffer = buffer;
    if (length < -1) throw new IllegalArgumentException("Length must be >= -1, but is: "+length);
    setRange(startOffset, length);
  }

  public CharBuffer getBuffer() {
    return this.buffer;
  }

  public QName getFormat() {
    return null;
  }

  public void setRange(int startOffset, int length) {
    if ((startOffset + length) > buffer.length()) {
      throw new IllegalArgumentException( "length out of range: startOffset="+startOffset+", length="+length+", buffer.length="+buffer.length() );
    }
    this.startOffset = startOffset;
    this.length = length;
  }

  public int getStartOffset() {
    return startOffset;
  }

  public int getLength() {
    if (length == -1) return buffer.length();
    else return length;
  }

  /**
   * Returns first offset of char buffer after this text line
   * @return first offset of char buffer after this text line
   */
  public int getEndOffset() {
    return startOffset + getLength() - 1;
  }

  public String getVisibleString() {
    int startOffset = getStartOffset();
    int length = getLength();
    if (buffer == null || length <= 0 || buffer.length() <= startOffset) {
      return "";
    }
    else {
      return buffer.toString().substring( startOffset, getEndOffset()+1 );
    }  
  }
}
