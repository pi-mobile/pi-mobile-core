/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

/**
 * Stores minimum, preferred, maximum and current value for teh size of
 * one direction (height or width). It is used for representing the 2-dimensional
 * view of a component.
 */
public class CompSizeLimit extends SizeLimit{

  private Component owner; //Rückverweis auf die Component
  private Direction direction;

  public CompSizeLimit(Component owner, Direction direction) {
    this.owner = owner;
    this.direction = direction;
  }

  /**
   * Initializes this SizeLimit
   * @param owner
   * @param min
   * @param pref
   * @param max
   * @throws IllegalArgumentException if not min <= pref <= max
   */
  public CompSizeLimit(Component owner, Direction direction, short min, short pref, short max) {
    super(min, pref, max);
    this.owner = owner;
    this.direction = direction;
  }

  /**
   * Sets a new current value. If current has changed owner.resized() is called.
   *
   * @param current the new current value
   * @return true if current value has changed
   * @throws IllegalArgumentException if not min <= current <= max
   */
  public boolean setCurrent(short current) {
    boolean changed = super.setCurrent(current);
    if ( changed || !(this.owner.getPaintState(this.direction).hasPaintStateReached(PaintState.STATE_LAYOUTED))) {
      this.owner.getPaintState(this.direction).doPaintState(PaintState.STATE_BOUNDS_SET);
      this.owner.doLayout(this.direction);
    }
    return changed;
  }

}
