/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;


import de.pidata.qnames.QName;

public interface ComponentColor {

  public static final QName WHITE = ComponentFactory.NAMESPACE.getQName("WHITE");
  public static final QName BLACK = ComponentFactory.NAMESPACE.getQName("BLACK");
  public static final QName GRAY  = ComponentFactory.NAMESPACE.getQName("GRAY");
  public static final QName RED   = ComponentFactory.NAMESPACE.getQName("RED");
  public static final QName YELLOW = ComponentFactory.NAMESPACE.getQName("YELLOW");
  public static final QName GREEN = ComponentFactory.NAMESPACE.getQName("GREEN");
  public static final QName DARKGRAY   = ComponentFactory.NAMESPACE.getQName("DARKGRAY");
  public static final QName LIGHTGRAY  = ComponentFactory.NAMESPACE.getQName("LIGHTGRAY");
  public static final QName BLUE       = ComponentFactory.NAMESPACE.getQName("BLUE");
  public static final QName ORANGE     = ComponentFactory.NAMESPACE.getQName("ORANGE");
  public static final QName CYAN       = ComponentFactory.NAMESPACE.getQName("CYAN");
  public static final QName MAGENTA    = ComponentFactory.NAMESPACE.getQName("MAGENTA");
  public static final QName PINK       = ComponentFactory.NAMESPACE.getQName("PINK");
  public static final QName DARKGREEN  = ComponentFactory.NAMESPACE.getQName("DARKGREEN");
  public static final QName LIGHTGREEN  = ComponentFactory.NAMESPACE.getQName("LIGHTGREEN");
  public static final QName TRANSPARENT  = ComponentFactory.NAMESPACE.getQName("TRANSPARENT");

  public static final String CONSTANT_HEX     = "0x";

  Object getColor ();

  void setColor (Object color);
}
