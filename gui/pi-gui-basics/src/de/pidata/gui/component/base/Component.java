/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.gui.event.ComponentListener;
import de.pidata.gui.guidef.InputMode;
import de.pidata.gui.layout.Layoutable;
import de.pidata.models.binding.BindingListener;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

/**
 * describes a paintable element  todo formulieren...
 * A component stores all information about size and position as well
 * as its state during the layout process.
 * Ebeso wird die Information ÃŒber das Aussehen der Komponente gespeichrt
 * Weiterhin wird der Status der Komponente verwendet, um die anzuzeigende Variante zu bestimmen.
 */
public interface Component extends Layoutable, BindingListener {

  public void setID(QName id);

  public QName getID();

  /**
   * Returns this component's Dialog. If this component's dialog is not assigned to
   * a Dialog (e.g. while building) null is returned.
   * @return this component's Dialog or null
   */
  public Dialog getDialog();

  /**
   * Returns the Container this component is part of. In some cases (Popup) that is different
   * from getLayoutParent()
   * @return the Container this component is part of
   */
  public Container getParentContainer();

  /**
  * Returns true if this component is a descendant of othercomponent.
  * @param otherComponent the component to check
  * @return true if this component is a descendant of othercomponent
  */
  public boolean isDescendant(Component otherComponent);

  /**
   * This method is called whenever a state change occured. Each sub class
   * has to decide what to do, e.g. repaint its content
   */
  public void stateChanged(int oldState, int newState);

  public PaintState getPaintState (Direction direction);

  public ComponentState getComponentState();

  /**
   * Calculates this component's min, max and pref sizes in one direction and
   * stores them in sizeLimit
   * @param direction
   * @param sizeLimit
   */
  public void calcSizes( Direction direction, SizeLimit sizeLimit);

  public void repaint();

  public void doLayout( Direction direction );

  /**
   * Returns this component's foreground color
   * @return this component's foreground color
   */
  ComponentColor getForeground ();

  /**
   * Sets the foreground color of this component.
   * @param color the new foreground color
   */
  void setForeground (ComponentColor color);

  /**
   * Returns this component's background color
   * @return this component's background color
   */
  ComponentColor getBackground ();

  /**
   * Sets the background of this component
   * @param color the new background color
   */
  void setBackground (ComponentColor color);

  /**
   * Gets the font of this component.
   * @return This component's font.
   */
  ComponentFont getFont ();

  /**
   * Sets the font of this component.
   * @param font the new font for this component
   */
  void setFont (ComponentFont font);

  /**
   * Call the action valid for the mouse event.
   * The action is defined in the controller.
   * @param state
   * @param lastPointedComponent
   * @param posInComp
   */
  void processMouseEvent(int state, Component lastPointedComponent, Position posInComp);

  public void setEnabled( boolean enabled );

  public boolean isEnabled();

  public void setVisible( boolean visible );

  public boolean isVisible();

  /**
   * Toggles focus, selected, enabled and visible state of this component at position (posX, posY) or for
   * entry identified by key. It depends on the type of component what is meant by posX, posY and key.
   * Many components will just ignore posX and posY values or key.
   * The default implementaion calls the componentListener.
   * @param posX      the x position of the source event
   * @param posY      the y position of the source event
   * @param focus     set focus on/off
   * @param selected  set selected if true, deselected if false, do not change if null
   * @param enabled   set enabled if true, disabled if false, do not change if null
   * @param visible   set visible if true, invisible if false, do not change if null
   */
  public void setState( short posX, short posY, Boolean focus, Boolean selected, Boolean enabled, Boolean visible );

  /**
   * Called whenever this component's content has changed and so it's size limits are invalid.
   */
  void contentChanged();

  /**
   * Check if painting is allowed. The component must be visible, and must be layouted.
   * @return true if painting is allowed
   */
  boolean allowPaint ();

  boolean setFocus( boolean focus );

  boolean command( QName cmd, char inputChar, int index );

  /**
   * Sets a component listener for this component. SelectionKey may be used as identifier
   * if listener wants to listen on multiple components. Listener is called whenever
   * a input event occurs on this component.
   * @param listener     the component listener
   */
  public void setComponentListener( ComponentListener listener );

  /**
   * Returns the ComponentListener
   * @return the ComponentListener
   */
  public ComponentListener getComponentListener();

  /**
   * Updates the value at posX, posY. The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX  horizontal position, starting from 0
   * @param posY  vertical position, starting from 0
   * @param valueType
   * @param value the new value
   */
  public void updateValue( short posX, short posY, Type valueType, Object value );

  /**
   * Inserts the value before posX, posY. The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX  horizontal position, starting from 0, to insert after last give posX = last+1
   * @param posY  vertical position, starting from 0, to insert after last give posY = last+1
   * @param valueType
   * @param value the value to be inserted
   */
  public void insertValue( short posX, short posY, SimpleType valueType, Object value );

  /**
   * Tells the component the count of available values, e.g. for displaying a scroll bar
   *
   * @param sizeX horizontal count
   * @param sizeY vertical count
   */
  public void setDataCount( short sizeX, short sizeY );

  /**
   * Sets the cursor position to (posX,posY). The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   * @param posX   horizontal position, starting from 0
   * @param posY   vertical position, starting from 0
   */
  public void setCursorPos( short posX, short posY );

  /**
   * Returns this View's current input mode
   * @return this View's current input mode
   */
  public InputMode getInputMode();

  /**
   * Sets this View's current input mode
   * @param inputMode this View's new input mode - one of the constants InputManager.MODE_*
   */
  public void setInputMode( InputMode inputMode );

  /**
   * This method is called when this view's controller is closing,
   * i.e. controller's dialog is closing.
   * The view has to release all its resources, e.g. remove
   * itself as listener.
   */
  public void closing();
}