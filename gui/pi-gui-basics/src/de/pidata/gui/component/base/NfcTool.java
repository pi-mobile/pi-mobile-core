package de.pidata.gui.component.base;

import de.pidata.connect.nfc.NfcScanListener;

public interface NfcTool {

  void scanNFCTag( NfcScanListener nfcScanListener );

}
