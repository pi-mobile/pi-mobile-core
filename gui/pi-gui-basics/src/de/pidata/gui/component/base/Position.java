/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

public class Position {
  private short x;
  private short y;
  private short dx = 0;
  private short dy = 0;

  public Position(short x, short y) {
    this.x = x;
    this.y = y;
  }

  /**
   * Translates this position by x-=dx and y-=dy and adds dx and dy to
   * internal dx and dy.
   * @param dx the x translation
   * @param dy the y translation
   */
  public void translate(short dx, short dy) {
    this.x -= dx;
    this.y -= dy;
    this.dx += dx;
    this.dy += dy;
  }

  public short getX() {
    return x;
  }

  public short getY() {
    return y;
  }

  /**
   * Returns the sum of all dx values passed to translate
   * @return the sum of all dx values passed to translate
   */
  public short getDx() {
    return dx;
  }

  /**
   * Returns the sum of all dy values passed to translate
   * @return the sum of all dy values passed to translate
   */
  public short getDy() {
    return dy;
  }

  /**
   * Resets x and y to the values bewfore first translaten
   * and set dx,dy == 0
   */
  public void reset() {
    this.x += dx;
    this.y += dy;
    this.dx = 0;
    this.dx = 0;
  }
}
