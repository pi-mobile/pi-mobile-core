/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.gui.component.base.Border;
import de.pidata.gui.component.base.Direction;

public abstract class AbstractBorder implements Border{
  protected short top;
  protected short left;
  protected short right;
  protected short bottom;

  public AbstractBorder() {
  }

  public AbstractBorder(short top, short left, short right, short bottom) {
    this.top = top;
    this.left = left;
    this.right = right;
    this.bottom = bottom;
  }

  public short getTop() {
     return top;
   }

  public short getLeft() {
    return left;
  }

  public short getRight() {
    return right;
  }

  public short getBottom() {
    return bottom;
  }

  /**
   * Adds the two border sizes in one direction and returns the result.
   * For direction X this is the size of the left and right border, for direction Y it is
   * the top and bottom border.
   *
   * @param direction
   * @return the total border size in one direction
   */
  public short get2BorderSize(Direction direction) {
    if (direction == Direction.X) {
      return (short) (left + right);
    }
    else {
      return (short) (top + bottom);
    }
  }

  public short getOffset (Direction direction) {
    if (direction == Direction.X) {
      return (short) left;
    }
    else {
      return (short) top;
    }
  }
}
