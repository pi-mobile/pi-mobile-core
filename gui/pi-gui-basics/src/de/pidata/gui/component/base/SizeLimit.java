/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

/**
 * Stores minimum, preferred, maximum and current value for a size (height or width)
 */
public class SizeLimit {

  private static final boolean DEBUG = false;
  private static final String ME = "SizeLimit";

  private short min;
  private short pref;
  private short max;
  private short current;

  public SizeLimit() {
  }

  /**
   * Initializes this SizeLimit
   * @param min
   * @param pref
   * @param max
   * @throws IllegalArgumentException if not min <= pref <= max
   */
  public SizeLimit(short min, short pref, short max) {
    checkMinMax(min, pref, max);
    this.min = min;
    this.pref = pref;
    this.max = max;
  }

  /**
   * Checks if min <= pref <= max
   * @param min
   * @param pref
   * @param max
   * @throws IllegalArgumentException if not min <= pref <= max
   */
  private void checkMinMax(short min, short pref, short max) {
    if (pref < min) {
      throw new IllegalArgumentException("Pref="+pref+" must not be less than min="+min);
    }
    if (pref > max) {
      throw new IllegalArgumentException("Pref="+pref+" must not be greater than max="+max);
    }
  }

  /**
   * Checks if min <= current <= max
   * @param current
   * @param min
   * @param max
   * @throws IllegalArgumentException if not min <= current <= max
   */
  private void checkCurrent(short current, short min, short max) {
    if (current < min) {
      throw new IllegalArgumentException("Current="+current+" must be greater or equal to min="+this.min);
    }
    if (current > max) {
      throw new IllegalArgumentException("Current="+current+" must be less or equal to max="+this.max);
    }
  }
  /**
   * Copies min, pref and max values from source
   * @param source the SizeLimit to copy from
   */
  public void copy(SizeLimit source) {
    this.min  = source.getMin();
    this.pref = source.getPref();
    this.max  = source.getMax();
  }

  /**
   * Returns maximum of value1, value2
   * @param value1
   * @param value2
   * @return  maximum of value1, value2
   */
  private short maxValue(short value1, short value2) {
    if (value1 > value2) return value1;
    else return value2;
  }

  /**
   * Returns minimum of value1, value2
   * @param value1
   * @param value2
   * @return  maximum of value1, value2
   */
  private short minValue(short value1, short value2) {
    if (value1 < value2) return value1;
    else return value2;
  }

  /**
   * Copies maximums of min, pref and max values from source1, source2
   * @param source1 the first SizeLimit to copy from
   * @param source2 the second SizeLimit to copy from
   */
  public void copyMax(SizeLimit source1, SizeLimit source2) {
    this.min  = maxValue(source1.getMin(), source2.getMin());
    this.pref = maxValue(source1.getPref(), source2.getPref());
    this.max  = maxValue(source1.getMax(), source2.getMax());
  }

  /**
   * Sets min pref and max
   * @param min
   * @param pref
   * @param max
   * @throws IllegalArgumentException if not min <= pref <= max
   */
  public void set(short min, short pref, short max) {
    checkMinMax(min, pref, max);
    this.min = min;
    this.pref = pref;
    this.max = max;
  }

  /**
   * Sets min pref and max. All values larger than Short.MAX_VALUE are
   * reduced to Short.MAX_VALUE.
   * @param min
   * @param pref
   * @param max
   * @throws IllegalArgumentException if not min <= pref <= max
   */
  public void set(int min, int pref, int max) {
    if (min > Short.MAX_VALUE) min = Short.MAX_VALUE;
    if (pref > Short.MAX_VALUE) pref = Short.MAX_VALUE;
    if (max > Short.MAX_VALUE) max = Short.MAX_VALUE;
    set((short) min, (short) pref, (short) max);
  }

  /**
   * Returns the minimum for this SizeLimit's current value
   * @return the minimum for this SizeLimit's current value
   */
  public short getMin() {
    return min;
  }

  /**
   * Sets a new min value.
   * @param min the new max value
   * @throws IllegalArgumentException if not ((min <= current <= max) and (min <= pref <= max))
   */
  public void setMin(short min) {
    checkMinMax(min, this.pref, this.max);
    this.min = min;
  }

  /**
   * Returns the preferred value for this SizeLimit's current value
   * @return the preferred value for this SizeLimit's current value
   */
  public short getPref() {
    return pref;
  }

  /**
   * Sets a new preferred value for this this SizeLimit's current value
   * @param pref the new preferred value
   * @throws IllegalArgumentException if not (min <= pref <= max)
   */
  public void setPref(short pref) {
    checkMinMax(this.min, pref, this.max);
    this.pref = pref;
  }

  /**
   * Returns the minimum for this SizeLimit's current value
   * @return the minimum for this SizeLimit's current value
   */
  public short getMax() {
    return max;
  }

  /**
   * Sets a new max value.
   * @param max the new max value
   * @throws IllegalArgumentException if not ((min <= current <= max) and (min <= pref <= max))
   */
  public void setMax(short max) {
    checkMinMax(this.min, this.pref, max);
    this.max = max;
  }

  /**
   * Returns the current value of this SizeLimit. Min <= current <= max is guaranteed.
   * @return the current value of this SizeLimit
   */
  public short getCurrent() {
    return current;
  }

  /**
   * Sets a new current value. If current has changed owner.resized() is called.
   * @param current the new current value
   * @return true if current value has changed
   * @throws IllegalArgumentException if not min <= current <= max
   */
  public boolean setCurrent(short current) {
    boolean changed = (current != this.current);
    if (changed) {
      checkCurrent(current, this.min, this.max);
      this.current = current;
    }
    return changed;
  }

  /**
   * Sets current value so that it fits min and max. The changed current value is returned.
   * @param current the newe current value
   * @return the changed current value
   */
  public short setCurrentFit(short current) {
    if (current < min) {
      current = min;
    } else {
      if (current > max) {
        current = max;
      }
    }
    setCurrent(current);
    return this.current;
  }

  /**
   * Returns the scale factor * 1000 of current value relative to preferred value.
   * This will prevent calculatiing with decimal numbers.
   * @return
   */
  public int currentFactor() {
    if (current == 0) {
      return 0;
    }
    else {
      return this.pref * 1000 / current;
    }
  }

  /**
   * Adjusts this limit's min, pref and max values, so that they min and pref are
   * the maximum and max is the minimum of this SizeLimit and otherLimit
   * @param otherLimit the limit this limit is adjusted to
   */
  public void adjustLimit(SizeLimit otherLimit) {
    this.min  = maxValue(this.min,  otherLimit.getMin());
    this.pref = maxValue(this.pref, otherLimit.getPref());
    if (this.pref < min) this.pref = this.min;
    this.max  = minValue(this.max,  otherLimit.getMax());
    if (this.max < pref) this.max = this.pref;
  }

  /**
   * Adjusts this limit's min, pref and max values, so that they min and pref are
   * the maximum and max is the minimum of this SizeLimit and otherLimit
   * @param otherLimit the limit this limit is adjusted to
   * @param cellSpan the number of cells otherLimit spans, used to divide
   *                 otherlLimit's min, pref and max values
   */
  public void adjustLimit(SizeLimit otherLimit, byte cellSpan) {
    this.min  = maxValue(this.min,  (short) (otherLimit.getMin() / cellSpan));
    this.pref = maxValue(this.pref, (short) (otherLimit.getPref() / cellSpan));
    if (this.pref < min) this.pref = this.min;
    this.max  = minValue(this.max,  (short) (otherLimit.getMax() / cellSpan));
    if (this.max < pref) this.max = this.pref;
  }
}
