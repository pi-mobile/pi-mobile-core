/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.gui.event.InputManager;
import de.pidata.gui.layout.Layoutable;
import de.pidata.log.Logger;

import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: pru Date: 05.04.2004 Time: 13:04:29 To change this template use File | Settings |
 * File Templates.
 *
 * TODO: asynchron - später
 */
public abstract class PaintManager extends Thread{

  private static final boolean DEBUG = false;

  // Time in hundred millis to wait until repaint is forced if no usable event occurs
  private static final int FORCE_TIMEOUT = 3;
  protected static PaintManager instance;

  private boolean shallIStopThread = false;
  private int eventTimer = -1;

  /** The paintQueue */
  protected Vector paintQueue = new Vector();

  /** Stores all windows of one common parent frame
   */
  public Vector dialogs = new Vector();
  private Dialog lastVisibleDialog = null;

  private Popup activePopup = null;

  private ComponentGraphics compGraphics;
  private ComponentBitmap doubleBufferingImg;
  private Vector systemThreadActions = new Vector();


  protected PaintManager() {
    if (instance != null) {
      throw new IllegalArgumentException("Must not create a second instance of PaintManager!");
    }
    instance = this;
    start();
  }

  public static PaintManager getInstance() {
    return instance;
  }


  public ComponentGraphics getCompGraphics () {
    return compGraphics;
  }

  public void setCompGraphics (ComponentGraphics compGraphics) {
    this.compGraphics = compGraphics;
  }

  /** Add window to this PaintManager
   *  @param dialog the rootContainer to be added
   */
  public void addDialog(Dialog dialog) {

    // test if window already exits
    if (this.dialogs.size() == 0 || this.dialogs.indexOf(dialog) < 0) {
      this.dialogs.addElement(dialog);
    }

    setLastVisibleDialog(dialog);
  }
  
  public Dialog getLastVisibleDialog () {
    return lastVisibleDialog;
  }

  public void setLastVisibleDialog (Dialog lastVisibleDialog) {
    this.lastVisibleDialog = lastVisibleDialog;
  }

  public Popup getActivePopup () {
    return activePopup;
  }

  public void setActivePopup (Popup activePopup) {
    this.activePopup = activePopup;
  }

  public void resetPopupToPaint() {
    this.activePopup = null;
  }

  /** Returns a new PaintEvent Object. Do not use the constructor directly.
   * @param component the Component to doPaint
   */
  public PaintEvent getNewEvent(Component component) {
    if (shallPaint(component)) {
      return new PaintEvent(component);
    }
    else {
      return null;
    }
  } // end of getNewEvent(component)

  /** Returns a new PaintEvent Object. Do not use the constructor directly.
   * @param component the SkinComponent to doPaint
   * @param x         the x-coordinate of the paintArea
   * @param y         the y-coordinate of the paintArea
   * @param width     the width of the paintArea
   * @param height    the height of the paintArea
   */
  public PaintEvent getNewEvent(Component component, int x, int y, int width, int height) {
    if (shallPaint(component)) {
      return new PaintEvent(component, x, y, width, height);
    }
    else {
      return null;
    }
  } // end of getNewEvent(component, x, y, widht, height)

  public boolean shallPaint(Component component) {
    return (component.getDialog() == lastVisibleDialog);
  }

  /**
   * Add a paintevent to the queue
   * @param paintEvent
   */
  public synchronized boolean deliverPaintEvent(PaintEvent paintEvent) {
    boolean shallAdd = false;
    if (paintEvent != null) {
      int size = paintQueue.size();
      if (size > 0) {
        shallAdd = (paintQueue.indexOf(paintEvent) == -1);
      }
      else {
        shallAdd = true;
      }
      if (shallAdd) {
        this.paintQueue.addElement(paintEvent);
        if (this.eventTimer == 0) {
          this.eventTimer = FORCE_TIMEOUT;
        }
      }
    }
    return shallAdd;
  }

  public void deliverPaintEvent(Component component) {
    deliverPaintEvent(new PaintEvent(component));
  }

  public void deliverPaintEvent(Component component, int x, int y, int width, int height) {
    deliverPaintEvent(new PaintEvent(component, x, y, width,  height));
  }

  public synchronized void addSystemThreadAction(SystemThreadAction actionObject) {
    this.systemThreadActions.addElement(actionObject);
    if (this.eventTimer == 0) {
      this.eventTimer = FORCE_TIMEOUT;
    }
  }

  /** Processes an paintEvent for an SkinComponent to paint.
   */
  public final void processPaintEvents() {
    PaintEvent event;
    Component component;
    SystemThreadAction action;

    synchronized(this) {
      if (DEBUG) Logger.debug( "---> processPaintEvents, queue size="+paintQueue.size());
      if (this.eventTimer >= 0) {
        this.eventTimer = -1;
      }
    }

    getInputManager().setMouseBlocked();

    //----- loop over all entries of paint queue
    Vector notProcessed = new Vector();
    while (this.canProcessPaintEvents()) {
      event = this.getNextEventToProcess();
      if (event != null) {
        component = event.getComponent();
        if (!processComponent(component)) {
          notProcessed.addElement( event );
        }
      }
    }  // end of while(...)

    synchronized(this) {
      this.eventTimer = 0;
    }

    //----- paint active popup and its child popups now, so it will be painted over all painted stuff
    Popup popupToPaint = getActivePopup();
    if( compGraphics != null) {
      while (popupToPaint != null) {
        //todo: optimieren; Dialog in Rechtecke schneiden und einzeln malen...
        popupToPaint.getPaintState( Direction.X ).undoPaintState( PaintState.STATE_PAINTED );
        popupToPaint.getPaintState( Direction.Y ).undoPaintState( PaintState.STATE_PAINTED );
        processComponent(popupToPaint);
        popupToPaint = popupToPaint.getChildPopup(); 
      }
    }

    //----- process other system thread actions
    do {
      synchronized(this) {
        if (systemThreadActions.size() > 0) {
          action = (SystemThreadAction) systemThreadActions.elementAt(0);
          systemThreadActions.removeElementAt(0);
        }
        else {
          action = null;
        }
      }
      if (action != null) {
        action.doSystemThreadAction();
      }
    } while (action != null);

    getInputManager().setMouseReleased();

    //----- add not processed events to paint queue
    if (DEBUG) Logger.debug( "<--- processPaintEvents, not processed count="+notProcessed.size());
    synchronized( this ) {
      for (int i = 0; i < notProcessed.size(); i++) {
        paintQueue.addElement( notProcessed.elementAt( i ) );
      }
      if (notProcessed.size() > 0) {
        forceRepaint();
      }
    }

  } // end of processPaintEvents()

  /**
   * Processes paint requestt for the given component.
   * @param component the component to be painted
   * @return false if native graphics is not available
   */
  private boolean processComponent(Component component) {
    ComponentGraphics compGraphics = getCompGraphics();
    Dialog dialog;
    if ((component != null) && (component.isVisible())) {
      dialog = component.getDialog();
      if (dialog != null) {
        getGraphics(dialog, compGraphics);
        if (compGraphics.getGraph() == null) {
          return false;
        }
        else {
          //----- Check if graphics' clip allows painting of comp, if not return false
          Rect clip = new Rect();
          compGraphics.getClip( clip );
          int x1 = AbstractContainer.calcPosInWindow( component, Direction.X );
          int y1 = AbstractContainer.calcPosInWindow( component, Direction.Y );
          int x2 = x1 + component.getSizeLimit( Direction.X ).getCurrent();
          int y2 = y1 + component.getSizeLimit( Direction.Y ).getCurrent();
          Rect screenBounds = dialog.getScreen().getScreenBounds();
          if (x1 < screenBounds.getX()) x1 = screenBounds.getX();
          if (x1 < screenBounds.getY()) y1 = screenBounds.getY();
          if (x2 > screenBounds.getMaxX()) x2 = screenBounds.getMaxX();
          if (y2 > screenBounds.getMaxY()) y2 = screenBounds.getMaxY();
/*          if ((x1 < clip.getX()) || (y1 < clip.getY()) || (x2 > clip.getMaxX()) || (y2 > clip.getMaxY())) {
            if (DEBUG) {
              Logger.debug( "can't process component, x1="+x1+", y1="+y1+", x2="+x2+", y2="+y2);
              Logger.debug( "     clip, x="+clip.getX()+", y="+clip.getY()+", maxX="+clip.getMaxX()+", maxY="+clip.getMaxY());
            }
            return false;
          }
*/
          try {
            doubleBufferingImg = getBufferImage(dialog);
            if (doubleBufferingImg != null) {
              if (paintComponent(component, doubleBufferingImg.getGraphics())) {
                compGraphics.paintBitmap(doubleBufferingImg, 0, 0, doubleBufferingImg.getWidth(), doubleBufferingImg.getHeight());
              }
            }
            else {
              paintComponent(component, compGraphics);
            }
          }
          catch (Throwable ex) {
            Logger.error("Could not process paint event", ex);
            ex.printStackTrace();
          }
          if (dialog.getScreen().isSingleWindow()) {
            // In single window mode we have to repaint all child dialogs
            // becaus they are painted on top of the last full screen
            // dialog. All parent full screen dialogs are invisible.
            Dialog childDlg = dialog.getChildDialog();
            while (childDlg != null) {
              childDlg.repaint();
              childDlg = childDlg.getChildDialog();
            }
          }
        }
      }
    }
    return true;
  }

  public abstract InputManager getInputManager ();

  protected abstract void getGraphics(Dialog dialog, ComponentGraphics compGraphics);

  protected abstract ComponentBitmap getBufferImage(Dialog dialog);

  public synchronized void processingEvent() {
    this.eventTimer = -1;
  }

  protected abstract void forceRepaint();

  /**
   * Main loop creating Platform repaint if some paint events where left
   * after mouse/keyboard event processing. 
   */
  public void run() {
    boolean repaint = false;
    while (!shallIStopThread) {
      try {
        synchronized (this) {
          repaint = false;
          if (eventTimer > 1) {
            eventTimer--;
          }
          else if (eventTimer == 1) {
            repaint = true;
            eventTimer = 0;
          }
        }
        if (repaint) {
          repaint = false;
          forceRepaint();
        }
        Thread.sleep(100);
      }
      catch (Exception e) {
        Logger.error("Error in PaintManager's exceptions loop", e);
        e.printStackTrace();
      }
    }
  } // end of run()

  /**
   * Returns true if there is another PaintEvent to process
   * Declaration as final may be removed if necessary!
   * @return true if there is another PaintEvent to process
   */
  public synchronized final boolean canProcessPaintEvents() {
    return (this.paintQueue.size() > 0);
  }

  /**
   * Returns the next paint event for processing or null if paintQueue is empty
   * Declaration as final may be removed if necessary!
   * @return the next paint event of null
   */
  public synchronized final PaintEvent getNextEventToProcess() {
    PaintEvent event = null;

    do {
      if (this.paintQueue.size() > 0) {
        event = (PaintEvent)this.paintQueue.elementAt(0);
        this.paintQueue.removeElementAt(0);
      }
      else {
        return event;
      }
    }
    while (this.hasOtherEventForSameComponent(event));
    return event;
  } // end of getNextEventToProcess()

  /** Returns true if event match otherEvent in one case:
   *  cases:
   *    - otherEvent's component is the same as event's component
   *    - otherEvent's component is a parent of event's component
   *    - otherEvent's component is a child of event's component
   *  If one of these cases match than otherEvent may be modified.
   *  If queue is empty or no otherEvent machted false is returned
   */
  private boolean hasOtherEventForSameComponent(PaintEvent event) {
    Component component;
    Component otherComponent;
    PaintEvent otherEvent;
    int count;
    int delta;
    int eventX2;
    int eventY2;
    int otherEventX2;
    int otherEventY2;
    int queueSize;

    component = event.getComponent();
    count = 0;

    while (true) {
      // get an otherEvent if queueSize is greater than count
      // pay attention: queueSize may the next time greater !!!
      queueSize = this.paintQueue.size();
      if (queueSize > count) {
        otherEvent = (PaintEvent)this.paintQueue.elementAt(count);
      }
      // no paintEvent matched
      else {
        return false;
      }
      otherComponent = otherEvent.getComponent();

      // test if otherEvent is the entry for the active popup - do not replace by whatever!
      if (otherComponent == activePopup) {
        return false;
      }

      // test if an otherEvent's component is the same as event's component
      if (component == otherComponent) {
        // check paintBounds
        if (!otherEvent.hasPaintBounds()) {
          // throw event away
          return true;
        }
        else if (!event.hasPaintBounds()) {
          // replace otherEvent by event
          otherEvent.replaceBy(event);
          return true;
        }
        else {
          // each event has paintBounds ...
          eventX2 = event.x + event.width;
          eventY2 = event.y + event.height;
          otherEventX2 = otherEvent.x + otherEvent.width;
          otherEventY2 = otherEvent.y + otherEvent.height;

          // check if the paintBounds have (einen Schnittpunkt) if not do nothing
          if (!((event.x > otherEventX2) || (event.y > otherEventY2) ||
          (otherEvent.x > eventX2) || (otherEvent.y > eventY2))) {
            // union the two paintArea's and store them in otherEvent
            delta = otherEvent.x - event.x;
            if (delta > 0) {
              otherEvent.x -= delta;
              otherEvent.width += delta;
            }
            delta = otherEvent.y - event.y;
            if (delta > 0) {
              otherEvent.y -= delta;
              otherEvent.height += delta;
            }
            delta = eventX2 - otherEventX2;
            if (delta > 0) {
              otherEvent.width += delta;
            }
            delta = eventY2 - otherEventY2;
            if (delta > 0) {
              otherEvent.height += delta;
            }
//            otherEvent.setPaintBoundsOn();
            return true;
          }
        }
      } // end of if (dialogComponent == otherComponent)

      // test if an otherEvent's component is a parent of event's component
      if (component.isDescendant(otherComponent)) {
        // check paintBounds
        if (otherEvent.hasPaintBounds()) {
          // otherEvent has paintBounds ...
          // check wheater dialogComponent's paintArea lies in the paintArea of otherComponent
          // if not may union the paintAreas, but may look worser than two paints!

          // TODO:
        }
        else {
          // not necessary to paint because otherEvent's component who is parent
          // does the paint of event's component that is child
          return true;
        }
      }

      // test if an otherEvent's component is a child of event's component
      if (otherComponent.isDescendant(component)) {
        // check paintBounds
        if (event.hasPaintBounds()) {
          // event has paintBounds ...
          // check wheater otherComponent's paintArea lies in the paintArea of dialogComponent
          // if not may union the paintAreas, but may look worser than two paints!

          // TODO:
        }
        else {
          // replace otherEvent by event
          otherEvent.replaceBy(event);
          return true;
        }
      } // end of dialogComponent isParentOf otherComponent
      this.paintQueue.setElementAt(otherEvent, count);
      count++;
    } // end of while (true)
  } // end of hasOtherEventForSameComponent(event)

  /**
   * Returns the number of queued events
   * @return number of queued events
   */
  public synchronized final int getQueueSize() {
    return this.paintQueue.size();
  } 

  public void stopThread() {
    shallIStopThread = true;
  }

  /**
   * Tells this paint manager that the dialog is beeing closed.
   * Removes all paint events for children belonging to dialog.
   * @param dialog the dialog which is beeing closed
   */
  public synchronized void dialogClosed(Dialog dialog) {
    PaintEvent event;
    for (int i = this.paintQueue.size()-1; i >= 0; i--) {
      event = (PaintEvent)this.paintQueue.elementAt(i);
      if (event.getComponent().getDialog() == dialog) {
        this.paintQueue.removeElementAt(i);
      }
    }
  }

  /**
   * Veranlasst eine Component, sich zu malen.
   * @param component
   * @param compGraph
   */
  protected boolean paintComponent(Component component, ComponentGraphics compGraph) {
    Layoutable parent;
    short width = component.getSizeLimit( Direction.X ).getCurrent();
    short height = component.getSizeLimit( Direction.Y ).getCurrent();
    short posX;
    short posY;
    Position translation = new Position((short) 0, (short) 0);

    if (!component.allowPaint()) {
      return false;
    }
    if (    ( component.getPaintState(Direction.X).hasPaintStateReached( PaintState.STATE_PAINTED ))
         && ( component.getPaintState(Direction.Y).hasPaintStateReached( PaintState.STATE_PAINTED )) ) {
      return true;
    }

    // move graph to position of the component
    Rect oldClip = new Rect();
    compGraph.getClip(oldClip);

    posX = component.getPos(Direction.X);
    posY = component.getPos(Direction.Y);

     // move graph to position of the component
    parent = component.getLayoutParent();
    if ((parent != null) && (parent instanceof Container)) {
      ((Container) parent).adaptGraphics(compGraph, translation);
    }

    translation.translate(posX, posY);
    compGraph.translate(posX, posY);
    compGraph.clipRect((short) 0, (short) 0, width, height);

    component.doPaint(compGraph);

    compGraph.translate((short) translation.getX(), (short) translation.getY());
    compGraph.setClip(oldClip);
    return true;
  }
}
