/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.gui.event.InputManager;
import de.pidata.models.types.Type;
import de.pidata.models.types.simple.BooleanType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.QName;

import java.util.Vector;

public class CharEditorHandler implements CharBufferListener {

  private static final char NEW_LINE = '\n';
  private int maxLength;

  private TextArea textArea;

  private short rowsVisible;
  private short charsVisible;

  private short firstVisibleRow = 0;

  private CharBuffer buffer;

  private Vector textRows = new Vector();

  public CharEditorHandler( TextArea textArea, int maxLength ) {
    this.textArea = textArea;
    this.maxLength = maxLength;
    this.buffer = new CharBuffer( maxLength );
    buffer.setListener( this );

    textRows.addElement(  new TextRow(null, buffer, 0) );
    for (int i = 0; i < textRowCount(); i++) {
      updateTextArea(i);
    }
  }

  public String getValue(  ) {
    return buffer.toString();
  }

  public void setValue( String valueObject ) {
    synchronized (buffer) {
      clearAllTextRows();
      if (valueObject == null) {
        buffer.setLength(0);
      }
      else {
        int pos = 0;
        int from = 0;
        buffer.setValue(null, valueObject.toString());
        // TODO: valueType? QName?
//        if ((valueType != null) && (valueType instanceof StringType)) {
//          buffer.setMaxLength( ((StringType) valueType).getMaxLength() );
//        }
        int rowNum = 0;
        for (int i = 0; i < textRowCount(); i++) {
          getTextRow( i ).setRange( buffer.length(), 0 );
        }
        if (buffer.length() > 0) {
          do {
            getTextRow( rowNum ).setRange( from, buffer.length() - from );
            pos = buffer.indexOf( NEW_LINE, from );
            if (pos >= 0) {
              wrapText( rowNum, pos );
              from = pos + 1;
            }
            rowNum++;
          } while (pos >= 0);
        }
      }
      buffer.setCursorPosition(0);
      updateTextArea();
    }
    textArea.setState( (short) -1, (short) -1, null, null, BooleanType.valueOf( textArea.isEnabled() ), null );
  }

  /**
   * if minX is zero set total visible range,
   * if minX is -1 minY contains rowIndex and maxY contains wrap position
   *
   */
  public void setVisibleRange( QName componentID, short minX, short minY, short maxX, short maxY ) {
    if (minX == 0) {
      boolean update = false;
      if (firstVisibleRow != minY) {
        update = true;
        firstVisibleRow = minY;
      }
      rowsVisible = (short) (maxY - minY);
      if (charsVisible != maxX) {
        update = true;
        charsVisible = maxX;
      }
      if (update) {
        updateTextArea();
      }
    }
    //---- wrap notification
    else if (minX == -1) {
      TextRow textRow = getTextRow( firstVisibleRow + minY );
      doLineBreak( textRow, maxX );
    }
  }

  /**
   * Invoked when component is selected/deselected
   *
   * @param posX
   * @param posY
   * @param selected     true if source was selected, false if source was deselected
   */
  public void selected( short posX, short posY, boolean selected ) {
    if (posY < 0) {
      posY = (short) getCursorRow();
    }
    TextRow row = getTextRow( firstVisibleRow + posY );
    if (row != null) {
      int cursorPos = row.getStartOffset() + posX;
      if ((cursorPos > 0) && (buffer.charAt( cursorPos-1 ) == NEW_LINE)) {
        cursorPos--;
      }
      if (cursorPos <= buffer.length()) {
        buffer.setCursorPosition( cursorPos );
      }
      else {
        buffer.setCursorPosition( buffer.length() );
      }
      updateCursorPos();
    }
  }

  /**
   * Tells this dialogView to process the given commandID and inputChar. If this dialogView
   * cannot process that command, false is returned which allows InputManager to
   * send that command to this dialogView's parent.
   * @param commandID the command to process, see constants defined by InputManager
   * @param inputChar the character to process, if command delivers a character
   * @return true if the command was processed
   */
  public boolean processCommand(QName commandID, char inputChar, int index) {
    if (textArea.isEnabled()) {
      if (commandID == InputManager.CMD_SELECT) {
        if (buffer.length() > 0) {
          // fireTextSelectionChangedEvent(new ChangeTextSelectionEvent(this,newpos,newpos));
        }
        return true;
      }
      else if (commandID == InputManager.CMD_ENTER) {
        insertChar( NEW_LINE );
        return true;
      }
      else if (commandID == InputManager.CMD_BACKSPACE) {
        if (moveCursorLeft()) {
          deleteChar();
        }
        return true;
      }
      else if (commandID == InputManager.CMD_DELETE) {
        deleteChar();
        return true;
      }
      else if (commandID == InputManager.CMD_INPUT) {
        insertChar(inputChar);
        return true;
      }
      else if (commandID == InputManager.CMD_PASTE ) {
        String input = Platform.getInstance().getInputManager().paste();
        if (input != null) {
          for (int i = 0; i < input.length(); i++) {
            insertChar( input.charAt(i) );
          }
        }
        return true;
      }
    }
    if (commandID == InputManager.CMD_LEFT ) {
      moveCursorLeft();
      updateCursorPos();
      return true;
    }
    else if (commandID == InputManager.CMD_RIGHT ) {
      moveCursorRight();
      updateCursorPos();
      return true;
    }
    else if (commandID == InputManager.CMD_UP ) {
      moveCursorUp();
      return true;
    }
    else if (commandID == InputManager.CMD_DOWN ) {
      moveCursorDown();
      return true;
    }
    else if (commandID == InputManager.CMD_HOME ) {
      gotoHome();
      return true;
    }
    else if (commandID == InputManager.CMD_END ) {
      gotoEnd();
      return true;
    }
    else if (commandID == InputManager.CMD_COPY ) {
      Platform.getInstance().getInputManager().copy( buffer.toString() );
      return true;
    }
    else if (commandID == InputManager.CMD_GRAB_FOCUS ) {
      textArea.setState( (short) -1, (short) -1, BooleanType.TRUE, null, null, null );
      return true;
    }
    else {
      return false;
    }
  }

  private void updateTextArea() {
    int lastVisibleRow = firstVisibleRow + rowsVisible - 1;
    if (lastVisibleRow > textRowCount()) lastVisibleRow = textRowCount() -1;
    textArea.setDataCount( (short) 0,  (short) textRowCount() );
    for (int rowNum = firstVisibleRow; rowNum <= lastVisibleRow; rowNum++) {
      updateTextArea(rowNum);
    }
  }

  private void updateTextArea(int rowNum) {
    if(rowNum < firstVisibleRow || rowNum > (firstVisibleRow + rowsVisible))
      return;

    short lineIndex = (short) (rowNum - firstVisibleRow);
    TextRow textRow = getTextRow(rowNum);
    String value = getRenderValue( textRow );
    textArea.updateValue( (short) 0, (short) lineIndex, StringType.getDefString(), value );

    if (rowNum == getCursorRow()) {
      updateCursorPos();
    }
  }

  private void updateCursorPos() {
    short cursorRow = (short) getCursorRow();
    TextRow textRow = getTextRow( cursorRow );
    short cursorX = (short) (buffer.getCursorPosition() - textRow.getStartOffset());
    textArea.setCursorPos( cursorX, cursorRow );
  }

  /**
   * Converts the given value into a displayable string. Calls to this method dop not affect
   * the state of this object i.e. all member variables are left unchanged
   *
   * @param valueType value's type
   * @param value     the value to be rendered
   * @return the rendered string
   */
  public String renderValue( Type valueType, Object value ) {
    if (value == null) {
      return "";
    }
    else {
      return value.toString();
    }
  }

  private String getRenderValue(TextRow textRow) {
    return textRow.getVisibleString();
  }

  private int textRowCount() {
    return textRows.size();
  }

  private TextRow getTextRow(int index) {
    if (index > textRowCount()) {
      return null;
    }
    else if (index == textRowCount()) {
      TextRow textRow = new TextRow( null, buffer, charsVisible );
      textRow.setRange( buffer.length(), 0 );
      textRows.addElement( textRow );
      textArea.setDataCount( (short) 0,  (short) textRowCount() );
      return textRow;
    }
    else {
      return (TextRow) textRows.elementAt( index );
    }
  }

  /**
   * Returns the row index of the cursor
   * @return the row index of the cursor
   */
  public int getCursorRow() {
    int cursorPos = buffer.getCursorPosition();
    for (int i = 0; i < textRows.size(); i++ ) {
      TextRow row = (TextRow) textRows.elementAt( i );
      int endOffset = row.getEndOffset();
      if (endOffset > cursorPos) {
        return i;
      }
      else if (endOffset == cursorPos) {
        if ((cursorPos == row.getStartOffset()) || ((cursorPos > 0) && (buffer.charAt(cursorPos-1) != NEW_LINE))) {
          return i;
        }
      }
      else if (cursorPos == buffer.length()) {
        // Special case: Cursor is after last buffer character
        if ((cursorPos > 0) && ((endOffset + 1) == cursorPos)) {
          if ((row.getStartOffset() == cursorPos) || (buffer.charAt(cursorPos-1) != NEW_LINE)) {
            // if current does not end with NEW_LINE cursor is in this line else in next line
            return i;
          }
        }
      }
    }
    return 0;
  }

  private void clearAllTextRows() {
    buffer.setCursorPosition( 0 );
    for (int i = 0; i < textRowCount(); i++) {
      TextRow textRow = getTextRow(i);
      textRow.setRange(0,0);
      updateTextArea( i );
    }
  }

  /**
   * Removes line rowNum by copying ranges from following lines.
   * While copying all startOffsets are shifted by delta (i.e. startOffset += delta)
   * @param rowNum  the row to be removed
   * @param delta   the delta to be added to the startOffsets
   */
  private void removeTextRow( int rowNum, int delta ) {
    synchronized (buffer) {
      int count = textRowCount();
      for (int i = rowNum; i <= count-2; i++) {
        TextRow textRow = getTextRow( i );
        TextRow nextRow = getTextRow( i+1 );
        int start = nextRow.getStartOffset() + delta;
        int len = nextRow.getLength();
        if (start > buffer.length()) {
          start = buffer.length();
          len = 0;
        }
        else if (start+len > buffer.length()) {
          len = buffer.length() - start;
        }
        textRow.setRange( start, len );
      }
      TextRow textRow = getTextRow( count-1 );
      textRow.setRange( buffer.length(), 0 );
    }
  }

  /**
   * Inserts line before rowNum by copying all ranges to following lines.
   * While copying all startOffsets are shifted by delta (i.e. startOffset += delta)
   * The new line fills the range between startOffset and the beginning of previous row at rowNum
   * @param rowNum  the row to be inserted before
   * @param delta   the delta to be added to the startOffsets
   */
  private void insertTextRow( int rowNum, int startOffset, int delta ) {
    synchronized (buffer) {
      TextRow textRow, prevRow;
      //--- for all rows after insert position copy range of row before
      for (int i = textRowCount()-1; i >= rowNum+1; i--) {
        textRow = getTextRow( i );
        prevRow = getTextRow( i-1 );
        int start = prevRow.getStartOffset() + delta;
        int len = prevRow.getLength();
        textRow.setRange( start, len );
      }
      //--- define range for inserted row
      textRow = getTextRow( rowNum );
      TextRow nextRow = getTextRow( rowNum+1 );
      textRow.setRange( startOffset, nextRow.getStartOffset() - startOffset );

      //--- add a new TextRow if last TextRow ends before end of buffer
      prevRow = getTextRow( textRowCount()-1 );
      if (prevRow.getEndOffset() < buffer.length()-1) {
        textRow = getTextRow( textRowCount() );
        int start = prevRow.getEndOffset() + 1;
        textRow.setRange( start, buffer.length() - start );
      }
    }
  }

  protected void moveCursorRight() {
    int cursorPosition = buffer.getCursorPosition();
    if (cursorPosition < buffer.length()) {
      cursorPosition++;
      buffer.setCursorPosition( cursorPosition );
      updateCursorPos();
    }
  }

  protected boolean moveCursorLeft() {
    boolean moved = false;
    int cursorPosition = buffer.getCursorPosition();
    if (cursorPosition > 0) {
      buffer.setCursorPosition( cursorPosition - 1 );
      moved = true;
      updateCursorPos();
    }
    return moved;
  }

  private void moveCursorUp() {
    if (getCursorRow() > 0) {
      TextRow currentRow = getTextRow(getCursorRow());
      int cursorPosition = buffer.getCursorPosition();
      int linePos = cursorPosition - currentRow.getStartOffset();
      TextRow prevRow = getTextRow( getCursorRow() - 1 );
      cursorPosition = prevRow.getStartOffset() + linePos;
      if (cursorPosition > prevRow.getLength()) {
        cursorPosition = prevRow.getEndOffset();
      }
      buffer.setCursorPosition( cursorPosition );
      updateCursorPos();
    }
  }

  private void moveCursorDown() {
    if (getCursorRow() < (textRowCount()-1)) {
      TextRow currentRow = getTextRow(getCursorRow());
      int cursorPosition = buffer.getCursorPosition();
      int linePos = cursorPosition - currentRow.getStartOffset();

      TextRow nextRow = getTextRow( getCursorRow() + 1 );
      if (nextRow.getStartOffset() > 0) {
        cursorPosition = nextRow.getStartOffset() + linePos;
        if (cursorPosition > nextRow.getEndOffset()) {
          cursorPosition = nextRow.getEndOffset();
        }
        if (cursorPosition > buffer.length()) {
          cursorPosition = buffer.length();
        }
        buffer.setCursorPosition( cursorPosition );
        updateCursorPos();
      }
    }
  }

  private void updateStartOffsets( int fromRow, int delta ) {
    for (int i = fromRow; i < textRowCount(); i++) {
      TextRow textRow = getTextRow(i);
      int start = textRow.getStartOffset();
      if (start > 0) {
        start = start + delta;
        int len = textRow.getLength();
        if (start > buffer.length()) {
          start = buffer.length();
          len = 0;
        }
        else if (start+len > buffer.length()) {
          len = buffer.length() - start;
        }
        textRow.setRange( start, len );
      }
    }
  }

  protected void insertChar(char keyChar) {
    synchronized (buffer) {
      if (buffer.length() < maxLength) {
        int cursorPosition = buffer.getCursorPosition();
        int cursorRow = getCursorRow();
        if (cursorPosition >= buffer.length()) {
          buffer.append(keyChar);
        }
        else {
          buffer.insert(cursorPosition, keyChar);
        }

        TextRow cursorLine = getTextRow( cursorRow );
        cursorLine.setRange( cursorLine.getStartOffset(), cursorLine.getLength()+1 );
        updateStartOffsets( cursorRow+1, 1 );
        moveCursorRight();

        if (keyChar == NEW_LINE) {
          if (wrapText( cursorRow, cursorPosition )) {
            updateTextArea();
          }
          else {
            updateTextArea( cursorRow );
            updateTextArea( cursorRow+1 );
          }
        }
        else {
          updateTextArea( cursorRow );
          updateTextArea( cursorRow+1 );
        }
      }
    }
  }

  protected void deleteChar() {
    synchronized (buffer) {
      int positionToDelete = buffer.getCursorPosition();
      if (positionToDelete < buffer.length()) {
        int cursorRow = getCursorRow();
        TextRow currentLine = getTextRow( cursorRow );
        TextRow nextLine = getTextRow( cursorRow + 1 );
        boolean hardBreak = false;
        if (buffer.length() > 0) {
          int endOffset = currentLine.getEndOffset();
          if (positionToDelete != endOffset) {
            hardBreak = (buffer.charAt( currentLine.getEndOffset() ) == NEW_LINE);
          }
        }
        buffer.deleteChar( positionToDelete );

        int start = currentLine.getStartOffset();
        if (hardBreak) {
          // Cursor row ends with NEW_LINE, so we must not change wrap position
          currentLine.setRange( start, currentLine.getLength()-1 );
          updateStartOffsets( cursorRow+1, -1 );
          updateTextArea( cursorRow );
          updateTextArea( cursorRow+1 );
        }
        else {
          //--- concat cursorLine and nextLine to allow new wrap calculation by TextArea
          int length = currentLine.getLength() + nextLine.getLength() - 1;
          currentLine.setRange( start, length );
          removeTextRow( cursorRow+1, -1 );
          updateTextArea();
        }
      }
    }
  }

  protected void gotoHome() {
    TextRow currentLine = getTextRow(getCursorRow());
    if (currentLine != null) {
      buffer.setCursorPosition( currentLine.getStartOffset() );
      updateCursorPos();
    }
  }

  protected void gotoEnd() {
    TextRow currentLine = getTextRow(getCursorRow());
    if (currentLine != null) {
      buffer.setCursorPosition( currentLine.getEndOffset() );
      updateCursorPos();
    }
  }

  /**
   * Truncates line lineNum at breakPos and prepends the following line
   * with the characters after breakPos
   * @param lineNum  the line to wrap
   * @param breakPos absolute index of the character in buffer to wrap after
   * @return true    if a line had to be inserted, i.e. the rangees of all lines with index lineNum and higher
   *                 have been modified
   */
  private boolean wrapText(int lineNum, int breakPos) {
    synchronized (buffer) {
      TextRow aktLine = getTextRow(lineNum);
      TextRow nextLine = getTextRow(lineNum+1);
      int offset = aktLine.getEndOffset();
      if (offset >= 0) {
        char lastChar = buffer.charAt(offset);

        aktLine.setRange( aktLine.getStartOffset(), breakPos-aktLine.getStartOffset()+1 );

        if (lastChar == NEW_LINE) {
          // if aktLine ends with NEW_LINE we have to keep that hard line break
          insertTextRow( lineNum+1, breakPos+1, 0 );
          return true;
        }
        else {
          // add chars of aktLine after breakPos to beginning of nextLine. TextArea wilk calulate possibly further breaks
          int end = nextLine.getEndOffset();
          nextLine.setRange( breakPos+1, end-breakPos );
          return false;
        }
      }
      else {
        return false;
      }
    }
  }

  private int findLine(TextRow source) {
    int lineNum = 0;
    while (lineNum < textRowCount()) {
      if (getTextRow(lineNum) == source) {
        return lineNum;
      }
      lineNum++;
    }
    return -1;
  }

  /**
   * This method is called by TextRow while painting whenever text is too large
   * to fit in TextRow
   * @param textRow
   * @param lastVisible the index of the last visisble character
   */
  public void doLineBreak(TextRow textRow, int lastVisible) {
    int lineNum = findLine(textRow);

    //----- Try to find a space for line break
    int startPos = textRow.getStartOffset();
    int breakPos = startPos + lastVisible - 1;
    while ((breakPos > startPos) && (buffer.charAt(breakPos) != ' ')) { // todo:  && (buffer.charAt(breakPos-1) != NEW_LINE) ) {
      breakPos--;  //todo: manuelle linebreaks gehen verloren!
    }
    if (breakPos <= startPos) breakPos = startPos + lastVisible - 1;

    //----- Break the line
    if (wrapText(lineNum, breakPos)) {
      updateTextArea();
    }
    else {
      updateTextArea( lineNum );
      updateTextArea( lineNum+1 );
    }
  }

  /**
   * This method is called when this controller's dialog is closing.
   * The controller has to realease all its resources, e.g. remove
   * itself as istener.
   */
  public void closing() {
    textArea.setComponentListener(null );
    for (int i = 0; i < textRowCount(); i++) {
      //TODO getTextRow(i).setComponentListener(null, null);
      getTextRow(i).setWrapListener(null);
    }
  }

  public void changed( CharBuffer sender ) {
    updateTextArea();
  }

  public void cursorMoved( CharBuffer sender ) {
    updateCursorPos();
  }
}
