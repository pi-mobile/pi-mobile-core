/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;

/**
 */
public abstract class Scroller extends AbstractContainer{

  private static final boolean DEBUG = false;
  private static final String ME = "Scroller";

  private Direction direction;

  /** position of the scrollbubble (lower end)
   */
//  private short scrollPos;
//  private short maxScrollPos;
  private short minScrollerLength;
  private short minScrollerWidth;

  /** used to calculate position of visible part in container
   */
//  private int scrollFactor;

  /** scrollPos moved when scrollbutton less or more is hit
   */
  private int moveLineValue = 2;

  /** scrollPos moved if scrollbar is hit
   */
  private int movePageValue = 10;

  private int value = 0;
  private int minValue = 0;
  private int maxValue = 100;

  private int valueLength = 20;

  public Scroller (Direction direction) {
    super();
    this.direction = direction;
    this.getComponentState().setVisible(false);
  }

  public Direction getDirection () {
    return direction;
  }

  public int getValue() {
    return value;
  }

  public int getValueLength() {
    return valueLength;
  }

  public int getMinValue() {
    return minValue;
  }

  public int getMaxValue() {
    return maxValue;
  }

  protected abstract void calcMinSizes();

  protected void setMinScrollerLength (short minScrollerLength) {
    this.minScrollerLength = minScrollerLength;
  }

  protected void setMinScrollerWidth (short minScrollerWidth) {
    this.minScrollerWidth = minScrollerWidth;
  }

  public short getMinScrollerLength () {
    if (minScrollerLength == 0) calcMinSizes();
    return minScrollerLength;
  }

  public short getMinScrollerWidth () {
    if (minScrollerWidth == 0) calcMinSizes();
    return minScrollerWidth;
  }

  public void setValueConstraints(int min, int max, int length, int moveLine, int movePage) {
    check(min,max,length, min);
    this.minValue = min;
    this.maxValue = max;
    this.valueLength = length;
    this.moveLineValue = moveLine;
    this.movePageValue = movePage;
    if (this.value < min) setValue(min);
    else if (this.value > max-length) setValue(max-length);
    getPaintState(direction).undoPaintState(PaintState.STATE_LAYOUTED);
  }

  private void check(int min, int max, int length, int value) {
    if (length < 0) {
      throw new IllegalArgumentException("Value length < 0, length="+value);
    }
    if ((max-length) < min) {
      throw new IllegalArgumentException("Value max-length<min, min="+min+", max="+max+", length="+length);
    }
    if ((value < min) || (value > (max-length))) {
      throw new IllegalArgumentException("Value not between max-length and min, min="+min+", max="+max+", length="+length+", value="+value);
    }
  }

  public int getMoveLineValue () {
    return moveLineValue;
  }

  public int getMovePageValue () {
    return movePageValue;
  }

  /**
   * Calculates position and length of scroll bubble.
   * Called after each change of value or value constraints and before parent's
   * reapaint() is called
   * @return the new bubble position
   */
  protected abstract short adjustBubble();

  /**
   * Updates the scroller value. If value is outside the range, it is adjusted.
   * posX and posY are ignored
   *
   * @param posX  ignored
   * @param posY  ignored
   * @param valueType
   * @param value the new scroller value
   */
  public void updateValue( short posX, short posY, Type valueType, Object value ) {
    setValueFit( ((Integer) value).intValue() );
  }

  /**
   * Always throws IllegalArgumentException.
   *
   * @param posX  ignored
   * @param posY  ignored
   * @param valueType
   * @param value ignored
   */
  public void insertValue( short posX, short posY, SimpleType valueType, Object value ) {
    throw new IllegalArgumentException( "insertValue not supported" );
  }

  /**
   * Sets the cursor position to (posX,posY). The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX horizontal position, starting from 0
   * @param posY vertical position, starting from 0
   */
  public void setCursorPos( short posX, short posY ) {
    //TODO
    throw new RuntimeException("TODO");
  }

  public void setMinValue( int minValue ) {
    this.minValue = minValue;
    getPaintState(direction).undoPaintState(PaintState.STATE_LAYOUTED);
  }

  public void setMaxValue( int maxValue ) {
    this.maxValue = maxValue;
    getPaintState(direction).undoPaintState(PaintState.STATE_LAYOUTED);
  }

  /**
   * Checks if newValue is in range and then sets the new value of the scroller.
   * @param newValue the new value for this scroller
   * @throws IllegalArgumentException if value is less than minValue or (value+valueLength) is greater than maxValue
   */
  public void setValue(int newValue) {
    check(minValue, maxValue, valueLength, newValue);
    this.value = newValue;
    adjustBubble();
    getLayoutParent().repaint();
  }

  /**
   * Sets the new value of the scroller. If value is outside the range,
   * adjust it.
   * @param newValue
   */
  public void setValueFit(int newValue) {
    if (newValue < minValue) {
      this.value = minValue;
    }
    else if (newValue > maxValue) {
      this.value = maxValue;
    }
    else {
      this.value = newValue;
    }
    adjustBubble();
    getLayoutParent().repaint();
  }

  /**
   * Calculates and sets the new value of the scroller.
   * Automatically adjusts value to fit within range.
   * @param deltaValue  thew delat for the scroller's value
   * @return the new position.
   */
  public int changeValue( int deltaValue ) {
    int newValue = this.value + deltaValue;
    if (newValue > (maxValue - valueLength)) newValue = this.maxValue - valueLength;
    if (newValue < this.minValue) newValue = this.minValue;
    setValue(newValue);
    return this.value;
  }

  /**
   * Calculates and sets the new value of the scroller.
   * Bubblepos is expected to be within range!
   * @param baseValue    the value to use as base for calculation
   * @param deltaBubble the pixel distance bubble was moved
   * @param barSize     the length of the bar (range for bubble)
   * @return the new position.
   */
  protected int changeValue( int baseValue, short deltaBubble, short barSize) {
    int valRange =  maxValue - minValue;
    int newValue = baseValue + (deltaBubble * valRange / barSize);
    if (newValue > (maxValue - valueLength)) newValue = this.maxValue - valueLength;
    if (newValue < this.minValue) newValue = this.minValue;
    setValue(newValue);
    return this.value;
  }

  /**
   * Calculates the bubble length, may be less than required minuimum
   * @param barSize the length of the bar (range for bubble)
   * @return teh bubble length
   */
  protected short calcBubbleLength(short barSize) {
    int valRange =  maxValue - minValue;
    if (valRange == 0) return barSize;
    else return (short) (valueLength * barSize / valRange);
  }

  /**
   * Calculates the bubble position
   * @param barSize the length of the bar (range for bubble)
   * @return the bubble pos
   */
  protected short calcBubblePos(short barSize) {
    int valueOffset = value - minValue;
    int valRange =  maxValue - minValue;
    if (valRange == 0) return 0;
    else return (short) (valueOffset * barSize / valRange);
  }
}
