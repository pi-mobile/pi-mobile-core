/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

// todo : generell anschauen

/** <P>This event is used to do the doPaint of an Component.</P>
 *
 */
public class PaintEvent {
  /** The Component of this paintEvent
   */
  private Component component;

  /* The bounds of the paintArea of this paintEvent
   */
  public int x;
  public int y;
  public int width;
  public int height;

  private boolean paintBounds;

  /** Creates a new doPaint event for the given imageComponent and bounds
   * @param component the Component to doPaint
   */
  protected PaintEvent(Component component) {
    this.component = component;
    this.paintBounds = false;
  } // end of constuctor

  /** Creates a new doPaint event for the given imageComponent and bounds
   * @param component the Component to doPaint
   * @param x         the x-coordinate of the paintArea
   * @param y         the y-coordinate of the paintArea
   * @param width     the width of the paintArea
   * @param height    the height of the paintArea
   */
  protected PaintEvent(Component component, int x, int y, int width, int height) {
    this.component = component;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.paintBounds = true;
  } // end of constuctor

  // is used by paintManager
  public void replaceBy(PaintEvent event) {
    this.component = event.component;
    this.x = event.x;
    this.y = event.y;
    this.width = event.width;
    this.height = event.height;
    this.paintBounds = event.paintBounds;
  } // end of replace

  /** Returns the Component for which to do the doPaint
   * @return the Component to doPaint
   */
  public Component getComponent() {
    return this.component;
  }

  public boolean hasPaintBounds() {
    return this.paintBounds;
  } // end of hasPaintBounds()

  /**
   * Returns a string representation of this dialogComponent and its values.
   */
  public String toString() {
    String str = "(PaintEvent@"
    + Integer.toHexString(hashCode()) + " for " + this.component.toString();
    if (this.paintBounds) {
      str += ", x=" + this.x + " y=" + this.y + " width=" + this.width + " height=" + this.height;
    }
    str += ")";
    return str;
  } // end of toString()

  public boolean equals(Object obj) {
    boolean result = false;
    if (obj != null && obj instanceof PaintEvent) {
      result = getComponent().equals(((PaintEvent)obj).getComponent());
    }
    return result;
  }

} // end of PaintEvent