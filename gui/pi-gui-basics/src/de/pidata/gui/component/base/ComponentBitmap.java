/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.models.types.simple.Binary;
import de.pidata.qnames.QName;

import java.io.IOException;
import java.io.OutputStream;

public interface ComponentBitmap extends Binary {

  public static final QName TIFF = ComponentFactory.NAMESPACE.getQName("TIFF");
  public static final QName JPEG = ComponentFactory.NAMESPACE.getQName("JPEG");
  public static final QName PNG  = ComponentFactory.NAMESPACE.getQName("PNG");
  public static final QName GIF  = ComponentFactory.NAMESPACE.getQName("GIF");

  public ComponentGraphics getGraphics();

  public int getWidth();

  public int getHeight();

  public Object getImage();

  /**
   * Saves this bitmap to the given OutputStream
   * @param outStream the strem to which image will be written
   * @param imageType the image type, see constants (TIFF, JPEG, ...)
   */
  public void save(OutputStream outStream, QName imageType) throws IOException;
}
