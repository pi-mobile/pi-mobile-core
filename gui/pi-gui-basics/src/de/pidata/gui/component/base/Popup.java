/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.gui.layout.FlowLayouter;
import de.pidata.gui.layout.Layoutable;
import de.pidata.gui.layout.StackLayouter;

/**
 * Basis f�r toolhelp/combobox/context menu/menu/inline edit   //todo �bersetzen
 *
 * Es gibt nur 1 offene Popup/Popup-Kette im System!
 */
public class Popup extends Panel{

  public static final int MODE_FIT = 0;
  public static final int MODE_STICK = 1;
  public static final int MODE_CENTER = 2;

  private Component owner;
  private Dialog dialog;
  private Popup parentPopup;
  private Popup childPopup;

  public Popup() {
    this(false, false);
  }

  public Popup(boolean scrollHor, boolean scrollVert) {
    super( new StackLayouter(Direction.X), new FlowLayouter(Direction.Y), scrollHor, scrollVert, (short) 0 );
    this.getComponentState().setVisible(false);
  }

  public void setVisible( boolean visible ) {
    getComponentState().setVisible(visible);
    Dialog dlg = getDialog();
    if (dlg != null) dlg.forceRepaint();
  }

  /**
   * Returns true if all parents of this Component are visible. If only
   * one of the parents is invisible false is returned.
   *
   * @return true if all parents are visible
   */
  public boolean isVisible(){
    Dialog dlg = getDialog();
    if (dlg != null) {
      return getComponentState().isVisible();
    }
    else {
      return false;
    }
  }

  /**
   * shows this popup at the specified coordinates
   * @param position
   */
  public void showPopup(Component owner, Position position, short width, short height) {
    short windowWidth;
    short windowHeight;
    short popupWidth;
    short popupHeight;
    short x;
    short y;
    short compHeigth = owner.getSizeLimit(Direction.Y).getCurrent();
    short compWidth = owner.getSizeLimit(Direction.X).getCurrent();
    Position newPos;
    Component oldOwner = this.owner;
    Popup oldPopup =  PaintManager.getInstance().getActivePopup();

    this.owner = owner;
    this.dialog = owner.getDialog();
    this.parentPopup = findParentPopup( owner );

    //TODO so wärs richtig, wird aber geclippt:
    // windowWidth = (short) dialog.getScreen().getScreenBounds().width;
    // windowHeight = (short) dialog.getScreen().getScreenBounds().height;
    windowWidth = dialog.getSizeLimit(Direction.X).getCurrent();
    windowHeight = dialog.getSizeLimit(Direction.Y).getCurrent();

    if (!getPaintState(Direction.X).hasPaintStateReached(PaintState.STATE_SIZES_CALC)) {
      calcSizes(Direction.X, getSizeLimit(Direction.X));
    }
    if (!getPaintState(Direction.Y).hasPaintStateReached(PaintState.STATE_SIZES_CALC)) {
      calcSizes(Direction.Y, getSizeLimit(Direction.Y));
    }

    popupWidth = getSizeLimit(Direction.X).getPref();
    popupHeight = getSizeLimit(Direction.Y).getPref();
    if (position == null && width == 0 && height == 0) {
      if (popupWidth < compWidth) {
        popupWidth = compWidth;
      }
      if (popupHeight > windowHeight) {
        popupWidth += getScroller(Direction.Y).getMinScrollerWidth();
      }
      x = calcPosX( windowWidth, calcPosInWindow( owner, Direction.X ), compWidth, popupWidth );
      y = calcPosY( windowHeight, calcPosInWindow( owner, Direction.Y ), compHeigth, popupHeight );
    }
    else {
      x = position.getX();
      y = position.getY();
      if (width > 0) {
        popupWidth = width;
      }
      else {
        popupWidth = getSizeLimit(Direction.X).getPref();
      }
      if (height > 0) {
        popupHeight = height;
      }
      else {
        popupHeight = getSizeLimit(Direction.Y).getPref();
      }
    }

    if (popupHeight > windowHeight) {
      popupHeight = windowHeight;
    }
    if (popupWidth > windowWidth) {
      popupWidth = windowWidth;
    }

    newPos = new Position(x,y);
    doShow(newPos, popupWidth, popupHeight, oldOwner, owner, oldPopup);
  }

  /**
   * Looks the componenent hierarchy upward for teh next container beeing an
   * instance of Popup and returns that Popup or null if no parent Popup is found.
   * @param comp the component to start from
   * @return the first ancestor of comp beeing an instance of Popup
   */
  public static Popup findParentPopup( Component comp ) {
    Container container = comp.getParentContainer();
    while (container != null) {
      if (container instanceof Popup) {
        return (Popup) container;
      }
      container = container.getParentContainer();
    }
    return null;
  }

  private short calcPosX(short windowWidth, short x, short compWidth, short popupWidth) {
    if (compWidth < popupWidth) {
      if (x + compWidth > popupWidth) {
        x = (short) (x + compWidth - popupWidth);
      }
      else {
        x = 0;
      }
    }
    return x;
  }

  private short calcPosY(short windowHeight, short y, short compHeigth, short popupHeight) {
    if (windowHeight - y - compHeigth >= popupHeight) {
      // show below component
      y = (short) (y + compHeigth);
    }
    else {
      if (y > popupHeight) {
        // show above component
        y = (short) (y - popupHeight);
      }
      else {
        // show at window top
        y = 0;
      }
    }
    return y;
  }

  private void doShow(Position position, short popupWidth, short popupHeight, Component oldOwner, Component owner, Popup oldPopup) {
    setPos(Direction.X, position.getX());
    setPos(Direction.Y, position.getY());

    getSizeLimit(Direction.X).setCurrentFit(popupWidth);
    getSizeLimit(Direction.Y).setCurrentFit(popupHeight);

    doLayout(Direction.X);
    doLayout(Direction.Y);

    if (parentPopup == null) {
      PaintManager.getInstance().setActivePopup(this);
    }
    else {
      parentPopup.setChildPopup( this );
    }
    this.getComponentState().setVisible(true);

    // repaint dialog only if popup's owner has changed // todo: hier aendern, falls popup seinen Platz aendern kann!
    // or another popup has been replaced
    if (oldOwner != null && oldOwner != owner) {
      oldOwner.getDialog().repaint();
    }
    else if (oldPopup != null && oldPopup != this) {
      oldPopup.getDialog().repaint();
    }
    owner.repaint();
    repaint();
  }

  public Popup getParentPopup() {
    return parentPopup;
  }

  private void setChildPopup( Popup childPopup ) {
    this.childPopup = childPopup;
  }

  public Popup getChildPopup() {
    return childPopup;
  }

  public void closePopup() {
    Dialog dialog = getDialog();
    if (childPopup != null) {
      childPopup.closePopup();
    }

    if (parentPopup == null) {
      PaintManager paintMgr = PaintManager.getInstance();
      if (paintMgr.getActivePopup() == this) {
        PaintManager.getInstance().setActivePopup(null);
      }
    }
    else {
      this.parentPopup.setChildPopup( null );
    }
    this.getComponentState().setVisible(false);

    setLayoutParent(null);
    if ((owner != null) && getComponentState().hasFocus()) {
      getDialog().switchFocus( owner );
    }
    this.owner = null;
    this.parentPopup = null;
    dialog.repaint();
    dialog = null;
  }

  public Component getOwner () {
    return owner;
  }

  /**
   * Returns the components parent container.
   * The parent container is responsible for layout and position of the component.
   *
   * @return the parent container
   */
  public Container getParentContainer() {
    if (owner == null) {
      return getDialog();
    }
    else {
      if (owner instanceof Container) {
        return (Container) owner;
      }
      else {
        return owner.getParentContainer();
      }
    }
  }

  /**
   * Returns the components parent container.
   * The parent container is responsible for layout and position of the component.
   *
   * @return the parent container
   */
  @Override
  public Container getLayoutParent() {
    return getDialog();
  }

  /**
   * Returns this component's Dialog. If this component's dialog is not assigned to
   * a Dialog (e.g. while building) null is returned.
   *
   * @return this component's Dialog or null
   */
  public Dialog getDialog(){
    return this.dialog;
  }
}