/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.gui.event.InputManager;
import de.pidata.models.types.simple.BooleanType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.QName;

public class CharBufferHandler implements CharBufferListener {

  protected TextLine textLine;

  protected CharBuffer buffer;

  protected QName format;
  private boolean hideInput = false;

  public CharBufferHandler( TextLine textLine, int maxLength ) {
    this.textLine = textLine;
    this.buffer = new CharBuffer( maxLength );
    buffer.setListener( this );
  }

  public void setFormat( QName format ) {
    this.format = format;
  }

  public void setHideInput( boolean hideInput ) {
    this.hideInput = hideInput;
  }

  public String getValue () {
    return buffer.toString();
  }

  public void setValue( String value ) {
    if (value == null) buffer.setValue( null, "" );
    else buffer.setValue( null, value ); // TODO...
    repaintTextLine();
  }

  public void selected( QName compID, short posX, short posY, boolean selected ) {
    buffer.setCursorPosition( posX );
    textLine.repaint();
  }

  public boolean processCommand( QName cmd, char inputChar, int index ) {
    if (textLine.isEnabled()) {
      if (cmd == InputManager.CMD_INPUT) {
        insertChar(inputChar);
        moveCursorRight();
        repaintTextLine();
        return true;
      }
      else if (cmd == InputManager.CMD_BACKSPACE) {
        if (moveCursorLeft()) {
          deleteCharFromCursor();
        }
        return true;
      }
      else if (cmd == InputManager.CMD_DELETE) {
        deleteCharFromCursor();
        return true;
      }
      else if (cmd == InputManager.CMD_PASTE ) {
        String input = Platform.getInstance().getInputManager().paste();
        if (input != null) {
          for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt( i );
            if (ch >= ' ') {
              insertChar( input.charAt(i) );
              moveCursorRight();
            }
          }
          repaintTextLine();
        }
        return true;
      }
    }
    if (cmd == InputManager.CMD_LEFT ) {
      if (moveCursorLeft()) {
        repaintTextLine();
      }
      return true;
    }
    else if (cmd == InputManager.CMD_RIGHT ) {
      if (moveCursorRight()) {
        repaintTextLine();
      }
      return true;
    }
    else if (cmd == InputManager.CMD_HOME ) {
      gotoHome();
      return true;
    }
    else if (cmd == InputManager.CMD_END ) {
      gotoEnd();
      return true;
    }
    else if (cmd == InputManager.CMD_COPY ) {
      Platform.getInstance().getInputManager().copy( buffer.toString() );
      return true;
    }
    else if (cmd == InputManager.CMD_GRAB_FOCUS ) {
      textLine.setState( (short) -1, (short) -1, BooleanType.TRUE, null, null, null );
      return true;
    }
    return false;
  }

  protected void repaintTextLine() {
    textLine.repaint();
  }

  protected CharBuffer getCurrentRow() {
    return buffer;
  }

  protected boolean moveCursorRight() {
    CharBuffer text = getCurrentRow();
    int rowLen = text.length();
    int cursorPosition = buffer.getCursorPosition();
    if (cursorPosition < rowLen) {
      buffer.setCursorPosition( cursorPosition + 1);
      return true;
    }
    else return false;
  }

  protected boolean moveCursorLeft() {
    boolean moved = false;
    int cursorPosition = buffer.getCursorPosition();
    if (cursorPosition > 0) {
      buffer.setCursorPosition( cursorPosition - 1);
      moved = true;
    }
    return moved;
  }

  protected void gotoHome() {
    buffer.setCursorPosition( 0);
    repaintTextLine();
  }

  protected void gotoEnd() {
    CharBuffer text = getCurrentRow();
    int textLen = text.length();
    buffer.setCursorPosition( textLen );
    repaintTextLine();
  }

  protected void insertChar(char keyChar) {
    CharBuffer text = getCurrentRow();
    text.insert( buffer.getCursorPosition(), keyChar);
    //TODO fireEvent(null, text);
  }

  protected void typeChar(char keyChar) {
    insertChar(keyChar);
    moveCursorRight();
    repaintTextLine();
  }

  protected void deleteCharFromCursor() {
    CharBuffer text = getCurrentRow();
    int cursorPosition = buffer.getCursorPosition();
    if (cursorPosition < text.length()) {
      text.deleteChar( cursorPosition );
      repaintTextLine();
      //TODO fireEvent(null, text);
    }
  }

  protected void insertChars(char[] chars) {
    for (int i = 0; i < chars.length; i++) {
      char keyChar = chars[i];
      insertChar(keyChar);
      moveCursorRight();
    }
    repaintTextLine();
  }

  // ----- CharBufferListener

  public void changed( CharBuffer sender ) {
    String str;
    if (hideInput) {
      StringBuffer buf = new StringBuffer();
      for (int i = this.buffer.length(); i > 0; i-- ) {
        buf.append( '*' );
      }
      str = buf.toString();
    }
    else {
      str = this.buffer.toString();
    }
    textLine.updateValue( (short) 0, (short) 0, StringType.getDefString(), str );
  }

  public void cursorMoved( CharBuffer sender ) {
    textLine.setCursorPos( (short) sender.getCursorPosition(), (short) 0 );
  }
}
