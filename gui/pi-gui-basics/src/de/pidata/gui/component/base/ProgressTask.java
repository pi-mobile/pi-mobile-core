/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

/**
 * <add description here>
 *
 * @author Doris Schwarzmaier
 */
public abstract class ProgressTask {

  private String taskName;
  private TaskHandler taskHandler;

  public ProgressTask( String taskName ) {
    this.taskName = taskName;
    taskHandler = Platform.getInstance().getControllerBuilder().createTaskHandler( this );
  }

  public void doPrepare() {
    // default: do nothing
  }

  /**
   * Implement this with the task itself.
   */
  public abstract Object performTask();

  /**
   * Implement this as callback after executing the task.
   */
  public abstract void doFinished( Object result );

  public void doCancelled( Object result ) {
    // default: do nothing
  }

  public final void abortTask() {
    taskHandler.abortTask();
  }

  public final boolean isAbortRequested() {
    return taskHandler.isAbortRequested();
  }

  public final void updateProgress( double progress,String message ) {
    taskHandler.updateProgress( progress, message );
  }

  public String getTaskName() {
    return taskName;
  }

  public TaskHandler getTaskHandler() {
    return taskHandler;
  }
}
