/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.models.types.simple.DecimalObject;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

public class CharBuffer {

  public static final int DEFAULT_LENGTH = 100;

  private char[] chars;
  private int length;
  private int maxLength;
  private String string = null;
  private Namespace namespace;
  private CharBufferListener listener;

  private int cursorPosition = 0;

  public CharBuffer(int length) {
    this.chars = new char[length];
    this.length = 0;
    this.maxLength = length;
  }

  public CharBuffer(String initialValue) {
    this.chars = new char[DEFAULT_LENGTH];
    setValue(null, initialValue);
  }

  public CharBuffer(int length, CharBuffer sourceBuf, int fromPos) {
    this.chars = new char[length];
    this.length = sourceBuf.length() - fromPos;
    System.arraycopy(sourceBuf.getChars(), fromPos, chars, 0, this.length);
  }

  public void setListener( CharBufferListener listener ) {
    this.listener = listener;
  }

  public synchronized void setLength(int length) {
    this.length = length;
    if (cursorPosition > length) cursorPosition = length;
    changed();
  }

  public synchronized int getCursorPosition() {
    return cursorPosition;
  }

  public synchronized void setCursorPosition(int cursorPosition) {
    if ((cursorPosition < 0) || (cursorPosition > length)) {
      throw new IllegalArgumentException("Cursor position out of range (0.."+length+"): "+cursorPosition);
    }
    this.cursorPosition = cursorPosition;
    if (this.listener != null) {
      this.listener.cursorMoved( this );
    }
  }

  public synchronized char[] getChars() {
    return chars;
  }

  public synchronized String toString() {
    if (length > 0) {
      if (string == null) {
        string = new String(chars, 0, length);
      }
      return string;
    }
    else {
      return null;
    }
  }

  public synchronized QName toQName() {
    if (namespace == null) {
      throw new IllegalArgumentException("Cannot convert buffer to QName: namespace is null");
    }
    return this.namespace.getQName(chars, 0, length);
  }

  private void fillBuffer(StringBuffer result, int delta, char fillChar) {
    for (int i = 0; i < delta; i++) {
      result.append(fillChar);
    }
  }

  /**
   * Removes from buffer's beginning all chars equal to trimChar
   * e.g. if buffer is "0002050" and trimChar is "0" result buffer is "2050"
   * @param trimChar the char to be removed
   */
  public synchronized void trimLeft(char trimChar) {
    int index = 0;
    int oldLen = length;
    while ((index < length) && (chars[index] == trimChar)) index++;
    if (index >= length) length = 0;
    else if (index > 0) {
      this.length -= index;
      System.arraycopy(chars, index, chars, 0, this.length);
    }
    if (this.length != oldLen) {
      changed();
    }
  }

  private StringBuffer toBufferFill(int resultLength, char fillChar, boolean prefix) {
    int delta = resultLength - length;
    if (delta <= 0) return null;

    StringBuffer result = new StringBuffer(length);
    if (prefix) {
      fillBuffer(result, delta, fillChar);
      result.append(chars, 0, length);
    }
    else {
      result.append(chars, 0, length);
      fillBuffer(result, delta, fillChar);
    }
    return result;
  }

  public synchronized String toStringFill(int resultLength, char fillChar, boolean prefix) {
    StringBuffer buf = toBufferFill(resultLength, fillChar, prefix);
    if (buf == null) return null;
    else return buf.toString();
  }

  public synchronized QName toQNameFill(int resultLength, char fillChar, boolean prefix) {
    if (namespace == null) {
      throw new IllegalArgumentException("Cannot convert buffer to QName: namespace is null");
    }
    StringBuffer buf = toBufferFill(resultLength, fillChar, prefix);
    if (buf == null) return null;
    else return namespace.getQName(buf, 0, buf.length());
  }

  /**
   * Sets this buffer's namespace and content
   * @param namespace used for creating QName from buffer content, may be null
   * @param value     the new buffer content
   */
  public synchronized void setValue(Namespace namespace, String value) {
    this.namespace = namespace;
    if (value == null) {
      this.length = 0;
    }
    else {
      this.length = value.length();
      if (this.length > 0) {
        if (this.chars.length < this.length) {
          this.chars = new char[this.length+20];
        }
        value.getChars(0, this.length, this.chars, 0);
      }
    }
    this.string = value;
    if (this.cursorPosition > this.length) this.cursorPosition = this.length;
    changed();
  }

  /**
   * Sets this buffer's namespace and content
   * @param value
   */
  public void setValue(QName value) {
    if (value == null) {
      throw new IllegalArgumentException("Must not call setValue(QName) with null value");
    }
    setValue(value.getNamespace(), value.getName());
  }

  public synchronized int length() {
    return length;
  }

  public int getMaxLength() {
    return maxLength;
  }

  public void setMaxLength(int maxLength) {
    this.maxLength = maxLength;
    if (this.length > maxLength) {
      setLength( maxLength );
    }
  }

  private void changed() {
    string = null;
    if (this.listener != null) {
      this.listener.changed( this );
      this.listener.cursorMoved( this );
    }
  }

 /**
   * Deletes the character behind the document's cursor.
   */
  public synchronized void deleteChar(int cursorPos) {
    if (cursorPos >= 0) {
      System.arraycopy(this.chars, cursorPos+1, this.chars, cursorPos, this.length-cursorPos-1);
      this.length--;
      changed();
    }
  }

  /**
   * Inserts a character into the document.
   *
   * @param value The character to insert
   */
  public synchronized boolean insert(int cursorPos, char value) {
    if (this.length < this.maxLength) {
      System.arraycopy(this.chars, cursorPos, this.chars, cursorPos+1, this.length-cursorPos);
      this.chars[cursorPos] = value;
      this.length++;
      changed();
      return true;
    }
    else {
      return false;
    }
  }

  /**
   * Inserts a part from another CharBuffer into the document.
   */
  public synchronized boolean insert(int cursorPos, CharBuffer srcBuf, int fromPos) {
    int insertLen = srcBuf.length() - fromPos;
    if (this.length+insertLen <= this.chars.length) {
      System.arraycopy(this.chars, cursorPos, this.chars, cursorPos+insertLen, this.length-cursorPos);
      System.arraycopy(srcBuf.getChars(), fromPos, this.chars, cursorPos, insertLen);
      this.length += insertLen;
      changed();
      return true;
    }
    else {
      return false;
    }
  }

  /**
   * Inserts a Strign into the document.
   *
   * @param value The character to insert
   */
  public synchronized boolean insert(int cursorPos, String value) {
    int insertLen = value.length();
    if (this.length+insertLen <= this.chars.length) {
      System.arraycopy(this.chars, cursorPos, this.chars, cursorPos+insertLen, this.length-cursorPos);
      value.getChars(0, insertLen, this.chars, cursorPos);
      this.length += insertLen;
      changed();
      return true;
    }
    else {
      return false;
    }
  }

  public synchronized boolean append(char value) {
    if (this.length < this.chars.length) {
      this.chars[this.length] = value;
      length++;
      changed();
      return true;
    }
    else {
      return false;
    }
  }

  public synchronized boolean append(CharBuffer appendBuf) {
    int appendLen = appendBuf.length();
    if ((this.length+appendLen) < this.chars.length) {
      System.arraycopy(appendBuf.getChars(), 0, this.chars, this.length, appendLen);
      this.length += appendLen;
      changed();
      return true;
    }
    else {
      return false;
    }
  }

  public synchronized char charAt(int i) {
    if (i >= 0 && i < this.chars.length) {
      return this.chars[i];
    }
    else {
      throw new ArrayIndexOutOfBoundsException("Char index out of range: "+i);
    }
  }

  public synchronized DecimalObject toDecimal( char decimalSeparator, char thousandSeparator ) {
    //TODO ohne Umweg über String
    if (this.length > 0) {
      String str = toString();
      return new DecimalObject( str, decimalSeparator, thousandSeparator );
    }
    else {
      return null;
    }
  }

  public Integer toInteger() {
    //TODO ohne Umweg über String
    if (this.length > 0) {
      String str = toString();
      return Integer.valueOf( str );
    }
    else {
      return null;
    }
  }

  public synchronized int indexOf(char c, int from) {
    for (int i = from; i < length; i++) {
      if (chars[i] == c) return i;
    }
    return -1;
  }
}
