/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.qnames.QName;

public interface ComponentFont {

  // PLAIN, or a bitwise union of BOLD and/or ITALIC
  public static final QName STYLE_PLAIN  = ComponentFactory.NAMESPACE.getQName("PLAIN");
  public static final QName STYLE_BOLD   = ComponentFactory.NAMESPACE.getQName("BOLD");
  public static final QName STYLE_ITALIC = ComponentFactory.NAMESPACE.getQName("ITALIC");

  public static final QName DEFAULT  = ComponentFactory.NAMESPACE.getQName("DEFAULT");
  public static final QName BOLDFONT = ComponentFactory.NAMESPACE.getQName("BOLDFONT");

  public Object getFont ();

  public void setFont (Component component, Object font);

  public short getStringSize (Component component, String string, Direction direction, boolean withLeading);

  public int getMaxCharWidth (Component component);

  public int getCharWidth (Component component, char ch);

  public int getCharsWidth (Component component, char[] buffer, int offset, int length);

  public int getStringWidth (Component component, String string);

  public int getHeight(Component component);
}
