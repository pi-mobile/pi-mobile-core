/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.log.Logger;

/**
 * repräsentiert und verwaltet den Fortschritt des Layout-Prozess
 * sorgt dafür, dass die richtige Reihenfolge eingehalten wird
 */
public class PaintState {

  private Component owner; /** refernece to component */

  // Don't change the numbers of this LAYOUT_STATE_* constants
  // because the numbers are needed in some methods
  public static final int STATE_INVALID = 0;
  public static final int STATE_SIZES_CALC = 1;
  public static final int STATE_BOUNDS_SET = 2;
  public static final int STATE_LAYOUTED = 3;
  public static final int STATE_RENDERED = 4;
  public static final int STATE_PAINTED = 5;

  private static final boolean debugLayoutSteps = false;

  /** it is not allowed to get a repaint while in painting */
  private boolean isPainting = false;

  /** actual state of the component during layout and painting process */
  private int paintState = STATE_INVALID;

  public PaintState(Component owner) {
    this.owner = owner;
  }

  public int getPrevPaintState(int paintState) {
    return paintState - 1;
  }

  public int getNextPaintState(int paintState) {
    return paintState + 1;
  }

  public int getPaintState() {
    return this.paintState;
  }

  public void doPaintState(int paintState) {
    if (isPainting && paintState != this.paintState && paintState < STATE_PAINTED) {
      //throw new RuntimeException("Change of PaintState is not allowed while painting!");
      if (debugLayoutSteps) {
        Logger.debug("Change of PaintState is not allowed while painting!");
      }
    }
    int stateDiff = paintState - this.paintState;

    if (stateDiff > 1) {
      if (debugLayoutSteps) {
        //" isn't the next step of current PaintState " + this.paintState);
//        Thread.dumpStack(); // Not MIDP compatible !!!
      }
      Logger.warn(this + ": Setting PaintState " + paintState +
                         " isn't the next step of current PaintState " + this.paintState + ", owner="+owner );
    }
    this.paintState = paintState;
  }

  public void undoPaintState(int paintState) {
    if (isPainting && paintState != this.paintState && paintState < STATE_PAINTED) {
      //throw new RuntimeException("Change of PaintState is not allowed while painting!");
      if (debugLayoutSteps) {
        Logger.debug("Change of PaintState is not allowed while painting!");
      }
    }
    if (this.hasPaintStateReached(paintState)) {
      this.paintState = paintState - 1;
    }
  }

  public boolean hasPaintStateReached(int paintState) {
    if (paintState == STATE_INVALID) {
      throw new RuntimeException("Given PaintState is Component.LAYOUT_STATE_INVALID");
    }
    if (this.getPaintState() >= paintState) {
      return true;
    }
    else {
      return false;
    }
  }

  /**
   * Each component has to set this when painting starts.
   * @param painting
   */
  public void setPainting(boolean painting) {
    isPainting = painting;
  }
}
