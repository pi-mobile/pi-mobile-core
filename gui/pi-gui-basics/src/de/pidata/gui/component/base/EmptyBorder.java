/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

public class EmptyBorder extends AbstractBorder {

  public EmptyBorder() {
    super();
  }

  public EmptyBorder(short top, short left, short right, short bottom) {
    super(top, left, right, bottom);
  }

  /**
   * Does the paint of this border
   *
   * @param graph  the graphicsImage of this border
   * @param width  the outer width of this border
   * @param height the outer height of this border
   */
  public void doPaint (ComponentGraphics graph, int width, int height) {
    //do nothing
  }
}
