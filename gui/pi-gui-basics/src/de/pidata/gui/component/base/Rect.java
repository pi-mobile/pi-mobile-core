/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

public class  Rect {

  int x = 0;
  int y = 0;
  int width = 0;
  int height = 0;

  public Rect () {
  }

  public Rect (int x, int y, int width, int height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }

  public int getX () {
    return x;
  }

  public void setX (int x) {
    this.x = x;
  }

  public int getY () {
    return y;
  }

  public void setY (int y) {
    this.y = y;
  }

  public int getWidth () {
    return width;
  }

  public void setWidth (int width) {
    this.width = width;
  }

  public int getHeight () {
    return height;
  }

  public void setHeight (int height) {
    this.height = height;
  }

  public void set (int x, int y, int width, int height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }

  public int getSize (Direction direction) {
    if (direction == Direction.X) {
      return width;
    }
    else {
      return height;
    }
  }

  public int getMaxX() {
    return (x + width);
  }

  public int getMaxY() {
    return (y + height);
  }
}
