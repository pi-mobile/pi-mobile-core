/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.gui.layout.Layouter;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.io.IOException;

/**
 * plattformneutral!
 */
public abstract class ComponentFactory {

  public static final Namespace NAMESPACE = Namespace.getInstance("de.pidata.gui");

  public static final QName ID_LABEL     = NAMESPACE.getQName( "label" );
  public static final QName ID_TEXTFIELD = NAMESPACE.getQName("textfield");
  public static final QName ID_DATEFIELD = NAMESPACE.getQName("datefield");
  public static final QName ID_BUTTON    = NAMESPACE.getQName("button");
  public static final QName ID_RADIOGROUP    = NAMESPACE.getQName("radioGroup");
  public static final QName ID_CHECKBOXGROUP = NAMESPACE.getQName("checkboxGroup");
  public static final QName ID_CHECKBOX      = NAMESPACE.getQName("checkbox");
  public static final QName ID_COMBOBOX      = NAMESPACE.getQName("combobox");
  public static final QName ID_LISTBOX       = NAMESPACE.getQName("listbox");
  public static final QName ID_BUTTON_LEFT   = NAMESPACE.getQName("buttonRight");
  public static final QName ID_BUTTON_RIGHT  = NAMESPACE.getQName("buttonLeft");
  public static final QName ID_BUTTON_UP     = NAMESPACE.getQName("buttonUp");
  public static final QName ID_BUTTON_DOWN   = NAMESPACE.getQName("buttonDown");
  public static final QName ID_SCROLL_BUBBLE = NAMESPACE.getQName("scrollBubble");
  public static final QName ID_SCROLL_BAR    = NAMESPACE.getQName("scrollBar");
  public static final QName ID_IMAGEMAP      = NAMESPACE.getQName("imageMap");
  public static final QName ID_TEXTAREA    = NAMESPACE.getQName("textarea");
  public static final QName ID_TEXTLINE    = NAMESPACE.getQName("textline");
  public static final QName ID_ICON          = NAMESPACE.getQName("icon");
  public static final QName ID_PROGRESSBAR          = NAMESPACE.getQName("progressBar");

  public static final QName BORDER_LISTBOX = NAMESPACE.getQName("listbox");
  public static final QName BORDER_WINDOW  = NAMESPACE.getQName("window");
  public static final QName BORDER_BITMAP  = NAMESPACE.getQName("bitmap");
  public static final QName BORDER_EMPTY   = NAMESPACE.getQName("empty");

  // Color constants todo: sollten in die SkinFactory rein?
  public static final QName COLOR_TABLE_ACTIVE_ROW = NAMESPACE.getQName("COLOR_TABLE_ACTIVE_ROW");
  public static final QName COLOR_TABLE_SELECTED_ROW = NAMESPACE.getQName("COLOR_TABLE_SELECTED_ROW");
  public static final QName COLOR_TABLE_NORMAL_ROW = NAMESPACE.getQName("COLOR_TABLE_NORMAL_ROW");
  public static final QName COLOR_TABLE_BACKGROUND = NAMESPACE.getQName("COLOR_TABLE_BACKGROUND");
  public static final QName COLOR_TABLE_HEADER_BACKGROUND = NAMESPACE.getQName("COLOR_TABLE_HEADER_BACKGROUND");
  public static final QName COLOR_TABLE_HEADER_FONT = NAMESPACE.getQName("COLOR_TABLE_HEADER_FONT");

  public static final QName COLOR_LABEL_TEXT = ComponentFactory.NAMESPACE.getQName("COLOR_LABEL_TEXT");
  public static final QName COLOR_BACKGROUND_PANEL = ComponentFactory.NAMESPACE.getQName("COLOR_BACKGROUND_PANEL");
/*  public static final QName COLOR_FOREGROUND_CELL = ComponentFactory.NAMESPACE.getQName("COLOR_FOREGROUND_CELL");
  public static final QName COLOR_BACKGROUND_CELL = ComponentFactory.NAMESPACE.getQName("COLOR_BACKGROUND_CELL");
  public static final QName COLOR_FOREGROUND_HEADER = ComponentFactory.NAMESPACE.getQName("COLOR_FOREGROUND_HEADER");
  public static final QName COLOR_BACKGROUND_HEADER = ComponentFactory.NAMESPACE.getQName("COLOR_BACKGROUND_HEADER");
  public static final QName COLOR_BORDER_SIMPLE = ComponentFactory.NAMESPACE.getQName("COLOR_BORDER_SIMPLE");
  public static final QName COLOR_BORDER_3D = ComponentFactory.NAMESPACE.getQName("COLOR_BORDER_3D");
  public static final QName COLOR_MARKED_CELL = ComponentFactory.NAMESPACE.getQName("COLOR_MARKED_CELL");
  public static final QName COLOR_BACKGROUND_TITLEBAR = ComponentFactory.NAMESPACE.getQName("COLOR_BACKGROUND_TITLEBAR");
  public static final QName COLOR_CLICKLABEL_NORMAL = ComponentFactory.NAMESPACE.getQName("COLOR_CLICKLABEL_NORMAL");
  public static final QName COLOR_CLICKLABEL_OVER = ComponentFactory.NAMESPACE.getQName("COLOR_CLICKLABEL_OVER");
  public static final QName COLOR_CLICKLABEL_SELECTED = ComponentFactory.NAMESPACE.getQName("COLOR_CLICKLABEL_SELECTED");
  public static final QName COLOR_CLICKLABEL_SELECTED_OVER = ComponentFactory.NAMESPACE.getQName("COLOR_CLICKLABEL_SELECTED_OVER");
  public static final QName COLOR_CLICKLABEL_DISABLED = ComponentFactory.NAMESPACE.getQName("COLOR_CLICKLABEL_DISABLED");
  public static final QName COLOR_CLICKLABEL_SELECTED_DISABLED = ComponentFactory.NAMESPACE.getQName("COLOR_CLICKLABEL_SELECTED_DISABLED");
  public static final QName COLOR_CLICKLABEL_FOCUS = ComponentFactory.NAMESPACE.getQName("COLOR_CLICKLABEL_FOCUS");
  public static final QName COLOR_CLICKLABEL_SELECTED_FOCUS = ComponentFactory.NAMESPACE.getQName("COLOR_CLICKLABEL_SELECTED_FOCUS");
  public static final QName COLOR_CLICKLABEL_SELECTED_FOCUS_OVER = ComponentFactory.NAMESPACE.getQName("COLOR_CLICKLABEL_SELECTED_FOCUS_OVER");
  public static final QName COLOR_TRANSPARENCY = ComponentFactory.NAMESPACE.getQName("COLOR_TRANSPARENCY");
  public static final QName COLOR_LABEL_VALID = ComponentFactory.NAMESPACE.getQName("COLOR_LABEL_VALID");
  public static final QName COLOR_LABEL_INVALID = ComponentFactory.NAMESPACE.getQName("COLOR_LABEL_INVALID");
  public static final QName COLOR_BACKGROUND_LISTBOX = ComponentFactory.NAMESPACE.getQName("COLOR_BACKGROUND_LISTBOX");
  public static final QName COLOR_BACKGROUND_LISTBOX_DISABLED = ComponentFactory.NAMESPACE.getQName("COLOR_BACKGROUND_LISTBOX_DISABLED");
  public static final QName COLOR_BACKGROUND_COMBOBOX = ComponentFactory.NAMESPACE.getQName("COLOR_BACKGROUND_COMBOBOX");
  public static final QName COLOR_BACKGROUND_COMBOBOX_DISABLED = ComponentFactory.NAMESPACE.getQName("COLOR_BACKGROUND_COMBOBOX_DISABLED");
  public static final QName COLOR_BACKGROUND_IMAGEMAP = ComponentFactory.NAMESPACE.getQName("COLOR_BACKGROUND_IMAGEMAP");
  public static final QName COLOR_BACKGROUND_DEFAULT = ComponentFactory.NAMESPACE.getQName("COLOR_BACKGROUND_DEFAULT");
  public static final QName COLOR_SELECTION_TEXTFIELD = ComponentFactory.NAMESPACE.getQName("COLOR_SELECTION_TEXTFIELD");
*/
  // Font constants todo: sollten in die SkinFactory rein?
  public static final QName FONT_HEADER = ComponentFactory.NAMESPACE.getQName("FONT_HEADER");
  public static final QName FONT_CELL = ComponentFactory.NAMESPACE.getQName("FONT_CELL");
  public static final QName FONT_SMALL = ComponentFactory.NAMESPACE.getQName("FONT_SMALL");

  /** Minimum width for a container when having a horizontal scroll bar */
  public static final int CONST_MIN_CONTAINER_WIDTH  = 0;

  /** Maximum width for a container when having a horizontal scroll bar */
  public static final int CONST_MAX_CONTAINER_WIDTH  = 1;

  /** Minimum height for a container when having a vertical scroll bar */
  public static final int CONST_MIN_CONTAINER_HEIGHT = 2;

  /** Maximum height for a container when having a vertical scroll bar */
  public static final int CONST_MAX_CONTAINER_HEIGHT = 3;

  //Frametitlebartickness
  public static final int TITLE_THICKNESS = 24;

  protected short[] constants = new short[4];

  private Platform platform;

  /**
   * Creates a new ComponentFactory and replaces instance.
   */
  protected ComponentFactory() {
  }

  public void init(Platform platform) throws IOException {
    this.platform = platform;
  }

  public final Platform getPlatform() {
    return this.platform;
  }

  /**
   * Returns default X gap for Panel Layouter
   * @return default X gap for Panel Layouter
   */
  public short getGapX() {
     return 3;
  }

  /**
   * Returns default Y gap for Panel Layouter
   * @return default Y gap for Panel Layouter
   */
  public short getGapY() {
    return 1;
  }

  public abstract Dialog createDialog(Layouter layouterX, Layouter layouterY, short x, short y, short width, short height);

  public abstract Container createPanel( Layouter layouterX, Layouter layouterY, boolean scrollHor, boolean scrollVert, short borderSize );

  public abstract Container createTabbedPane();

  public abstract Table createTable(Layouter layouterX, Container header, Container body, Container edit, short visibleRows);

  public Label createLabel( String labelText, String format, int charsVisible ) {
    return new Label(labelText, format, charsVisible);
  }

  public abstract Component createTextField(int charsVisible, String format);

  public abstract Component createTextArea(int charsVisible, int rowsVisible);

  public abstract Component createButton(String caption);

  public abstract Component createButton(QName lookID);

  public abstract Component createTabbedButton(String caption);

  public abstract Component createCheckbox(String caption, String format);

  public abstract Component createRadiobutton(String caption);

  public abstract Component createCombobox(int charsVisible);

  public abstract Component createProgressBar(String caption);

  public abstract Popup createPopup();

  public abstract Scroller createScrollbar(Direction direction);

  public TextLine createTextLine(QName lineID, String buffer, int charsVisible) {
    return new TextLine(lineID, buffer, charsVisible);
  }

  /**
   * Returns the constant identified by constID, one of the constants CONST_*
   * @param constID the constants ID
   * @return the value for that constant
   */
  public short getConstant(int constID) {
    return this.constants[constID];
  }

  /**
   * Creates a border of the given borderType
   * @param borderType one of the constants BORDER_*
   * @return a border of the given borderType
   */
  public abstract Border createBorder(QName borderType, String value, short top, short left, short right, short bottom);

  /**
   * Create component defined by typeID
   * @param typeID one of the constants ID_*
   * @return the component
   * @throws IllegalArgumentException if typeID is unknown
   */
  public Component createComponent(QName typeID, String caption, String format, int charsVisible, int rowsVisible) {
    if (typeID == ID_LABEL) {
      return createLabel(caption, format, charsVisible);
    }
    else if (typeID == ID_BUTTON) {
      return createButton(caption);
    }
    else if (typeID == ID_TEXTFIELD) {
      return createTextField(charsVisible, format);
    }
    else if (typeID == ID_DATEFIELD) {
      //TODO eigenes Datumsfeld implementieren
      return createTextField(charsVisible, format);
    }
    else if (typeID == ID_TEXTAREA) {
      return createTextArea(charsVisible, rowsVisible);
    }
    else if (typeID == ID_COMBOBOX) {
      return createCombobox(charsVisible);
    }
    else if (typeID == ID_CHECKBOX) {
      return createCheckbox(caption, format);
    }
    else if (typeID == ID_TEXTLINE) {
      return createTextLine(null, caption, charsVisible);
    }
    else if (typeID == ID_PROGRESSBAR) {
      return createProgressBar( caption );
    }
    else if (typeID == ID_ICON) {
      if ((caption != null) && (caption.length() > 0)) {
        ComponentBitmap componentBitmap = this.platform.getBitmap( GuiBuilder.NAMESPACE.getQName( caption + ".gif" ) );
        return new Icon(componentBitmap);
      }
      else {
        return new Icon(null);
      }
    }
    else {
      throw new IllegalArgumentException("Unsupported component type: "+typeID);
    }
  }
}
