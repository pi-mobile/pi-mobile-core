/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.gui.event.ComponentListener;
import de.pidata.gui.event.InputManager;
import de.pidata.gui.guidef.CompDef;
import de.pidata.gui.guidef.InputMode;
import de.pidata.gui.layout.LayoutInfo;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.binding.AttributeBinding;
import de.pidata.models.binding.Binding;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelSource;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.qnames.QName;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * AbstractComponent implements the platform independant methods
 * for managing paintable objects.
 *
 *
 */
public abstract class AbstractComponent implements Component {

  private static final boolean DEBUG = false;
  private static final String ME = "AbstractComponent";

  /** reference to the container which directly includes this component */
  private Container parent = null;

  /** State flag, see constants STATE_* . Use setState() to change and
   *  getSyncState() to get the current state.
   * used to decide which variant of look should be used */
  protected ComponentState componentState = new ComponentState(this);

  protected ComponentListener listener;
  protected AttributeBinding binding;

  /** state information during layout process*/
  private PaintState[] paintState = new PaintState[2];

  /** sizeLimit is used to store the components size (min/max/preferred height/width  */
  private CompSizeLimit[] sizeLimit = new CompSizeLimit[2];

  /** position after layout */
  private short[] pos = new short[2];

  /**
   * Here we store the position (cell,span,alignment) in the parent grid
   */
  private LayoutInfo[] layoutInfo = new LayoutInfo[2];

  private ComponentColor foreground = null;
  private ComponentColor background = null;
  private ComponentFont compFont;
  private QName compID;

  protected InputMode inputMode = InputMode.none;
  protected String errorText = null;

  /**
   * initialize the components state
   */
  public AbstractComponent() {
    sizeLimit[Direction.X.getIndex()] = new CompSizeLimit( this, Direction.X );
    sizeLimit[Direction.Y.getIndex()] = new CompSizeLimit( this, Direction.Y );
    layoutInfo[Direction.X.getIndex()] = new LayoutInfo();
    layoutInfo[Direction.Y.getIndex()] = new LayoutInfo();
    paintState[Direction.X.getIndex()] = new PaintState(this);
    paintState[Direction.Y.getIndex()] = new PaintState(this);
    Platform platform = Platform.getInstance();
    setFont(platform.getFont(ComponentFont.DEFAULT));
    setForeground(platform.getColor(ComponentFactory.COLOR_LABEL_TEXT));
    setBackground(platform.getColor(ComponentFactory.COLOR_BACKGROUND_PANEL));
    layoutInfo[Direction.X.getIndex()].setAlignment( LayoutInfo.ALIGN_GROW );
    layoutInfo[Direction.Y.getIndex()].setAlignment( LayoutInfo.ALIGN_CENTER );
  }

  @Override
  public void setID(QName compID) {
    this.compID = compID;
  }

  @Override
  public QName getID() {
    return compID;
  }

  /**
   * Tells the component the count of available values, e.g. for displaying a scroll bar
   *
   * @param sizeX horizontal count
   * @param sizeY vertical count
   */
  @Override
  public void setDataCount( short sizeX, short sizeY ) {
    // do nothing
  }

  /**
   * Set this components foreground color. Use this method to set the color
   * during painting without sending a new repaint request!
   * @param foreground
   */
  public void setCompForeground (ComponentColor foreground) {
    this.foreground = foreground;
  }

  /**
   * Set this components background color. Use this method to set the color
   * during painting without sending a new repaint request!
   * @param background
   */
  public void setCompBackground (ComponentColor background) {
    this.background = background;
  }

  public void setCompFont (ComponentFont font) {
    this.compFont = font;
  }

  public ComponentFont getCompFont () {
    return this.compFont;
  }

  /**
   * Returns this component's Dialog. If this component's dialog is not assigned to
   * a Dialog (e.g. while building) null is returned.
   * @return this component's Dialog or null
   */
  @Override
  public Dialog getDialog() {
    if (this.parent == null) return null;
    else return this.parent.getDialog();
  }

  /**
   * Sends a paint event
   */
  @Override
  public void repaint() {
    if (DEBUG) Logger.debug(ME + ".repaint(...) begin ");
    if (!(this.isVisible())) {
      return;
    }
    getPaintState(Direction.X).undoPaintState(PaintState.STATE_PAINTED);
    getPaintState(Direction.Y).undoPaintState(PaintState.STATE_PAINTED);
    PaintManager.getInstance().deliverPaintEvent(this);
  }

  /**
   * Check if painting is allowed. The component must be visible, and must be layouted.
   * @return true if painting is allowed
   */
  @Override
  public boolean allowPaint () {
    boolean allowPaint = true;
    // ignore invisible components
    if( !isVisible()) {
      allowPaint = false;
    }
    PaintState state =getPaintState(Direction.X);
    if (!state.hasPaintStateReached(PaintState.STATE_BOUNDS_SET)) {
      allowPaint = false;
      Logger.info("allowPaint() denies painting - PaintState X < STATE_BOUNDS_SET");
    }
    else if (!state.hasPaintStateReached(PaintState.STATE_LAYOUTED)) {
      doLayout( Direction.X );
    }

    state = getPaintState(Direction.Y);
    if (!state.hasPaintStateReached(PaintState.STATE_BOUNDS_SET)) {
      allowPaint = false;
      Logger.info("allowPaint() denies painting - PaintState Y < STATE_BOUNDS_SET");
    }
    else if (!state.hasPaintStateReached(PaintState.STATE_LAYOUTED)) {
      doLayout( Direction.Y );
    }
    return allowPaint;
  }

  @Override
  public final void doPaint(ComponentGraphics graph) {
    if (DEBUG) Logger.debug( "-> doPaint, class="+getClass().getName());
    PaintState paintStateX = getPaintState(Direction.X);
    PaintState paintStateY = getPaintState(Direction.Y);

    //--- first set paintState PAINTED because in some cases we have to call repaint
    //    while painting (e.g. word wrap in TextArea)
    paintStateX.doPaintState( PaintState.STATE_RENDERED );
    paintStateY.doPaintState( PaintState.STATE_RENDERED );
    paintStateX.doPaintState( PaintState.STATE_PAINTED );
    paintStateY.doPaintState( PaintState.STATE_PAINTED );

    paintStateX.setPainting(true);
    paintStateY.setPainting(true);
    try {
      doPaintImpl(graph);
    }
    catch (Exception ex) {
      Logger.error( "Error while painting comp="+getClass()+", id="+getID(), ex );
    }
    paintStateX.setPainting(false);
    paintStateY.setPainting(false);

    if (DEBUG) Logger.debug( "<- doPaint, class="+getClass().getName());
  }

  abstract public void doPaintImpl(ComponentGraphics graph);
  
  /**
   * Calculate the position of the children.
   * This method is empty for normal components, but needs implementaion
   * for containers and compositions.
   */
  @Override
  public void doLayout( Direction direction ) {
    // default: do nothing except set state STATE_LAYOUTED
    getPaintState(direction).doPaintState(PaintState.STATE_LAYOUTED);
  }

  /**
   * Returns the components position in one direction.
   * @param direction
   * @return the components position
   */
  @Override
  public short getPos( Direction direction ) {
    return pos[direction.getIndex()];
  }

  /**
   * Sets the components position in one direction.
   * @param direction
   * @param position
   */
  @Override
  public void setPos (Direction direction, short position ) {
    this.pos[direction.getIndex()] = position;
  }

  /**
   * Calculates the components size in one direction
   * @param direction
   * @return the components size
   */
  @Override
  public SizeLimit getSizeLimit( Direction direction ) {
    if (!getPaintState(direction).hasPaintStateReached(PaintState.STATE_SIZES_CALC)) {
      calcSizes( direction, sizeLimit[direction.getIndex()] );
    }
    return sizeLimit[direction.getIndex()];
  }

  /**
   * Returns the components position in the parent containers grid
   * @param direction
   * @return the components position in the grid
   */
  @Override
  public LayoutInfo getLayoutInfo( Direction direction ) {
    return layoutInfo[direction.getIndex()];
  }

  /**
   * Returns the components activation state
   * @return the components state
   */
  @Override
  public ComponentState getComponentState() {
    return this.componentState;
  }

  /**
   * Returns the components paint state
   * @return the components paint state
   */
  @Override
  public PaintState getPaintState (Direction direction) {
    return this.paintState[direction.getIndex()];
  }

  /**
    * Sets a selection listener for this component. SelectionID may be used as identifier
    * if listener wants to listen on multiple components. Listener is called whenever
    * setSelected is called on this component.
    * @param listener     the component listener
   */
  @Override
  public void setComponentListener( ComponentListener listener ) {
    this.listener    = listener;
  }

  @Override
  public ComponentListener getComponentListener() {
    return listener;
  }

  /**
   * Toggles focus, selected, enabled and visible state of this component at position (posX, posY) or for
   * entry identified by key. It depends on the type of component what is meant by posX, posY and key.
   * Many components will just ignore posX and posY values or key.
   * The default implementation calls the componentListener.
   * @param posX      the x position of the source event
   * @param posY      the y position of the source event
   * @param focus     set focus on/off
   * @param selected  set selected if true, deselected if false, do not change if null
   * @param enabled   set enabled if true, disabled if false, do not change if null
   * @param visible   set visible if true, invisible if false, do not change if null
   */
  @Override
  public void setState( short posX, short posY, Boolean focus, Boolean selected, Boolean enabled, Boolean visible ) {
    ComponentState componentState = this.getComponentState();
    if (focus != null) {
      if (focus.booleanValue() && !componentState.hasFocus()) {
        Dialog dlg = getDialog();
        if (dlg != null) {
          dlg.switchFocus( this ); //TODO nicht immer!
        }
      }
      componentState.setFocus( focus.booleanValue() );
    }
    if (selected != null) {
      componentState.setSelected( selected.booleanValue() );
    }
    if (enabled != null) {
      componentState.setEnabled( enabled.booleanValue() );
    }
    if (visible != null) {
      componentState.setVisible( visible.booleanValue() );
    }
  }

  @Override
  public boolean setFocus( boolean focus ) {
    boolean hasFocus = getComponentState().hasFocus();
    if (hasFocus != focus) {
      getComponentState().setFocus( focus );
      if (listener != null) {
        listener.onFocusChanged( compID, focus );
      }
      if (focus) {
        // scroll to visible if we just got focus
        if (parent != null) {
          parent.scrollToVisible(Direction.X, getPos(Direction.X), getSizeLimit(Direction.X).getCurrent());
          parent.scrollToVisible( Direction.Y, getPos( Direction.Y ), getSizeLimit( Direction.Y ).getCurrent() );
        }
      }
    }
    return true;
  }

  @Override
  public boolean isEnabled() {
    ComponentState componentState = this.getComponentState();
    return componentState.isEnabled();
  }

  /**
   * Enables/disables this component
   *
   * @param enabled if true component is enabled, otherwise disabled
   */
  @Override
  public void setEnabled( boolean enabled ) {
    getComponentState().setEnabled( enabled );
  }

  /**
   * Returns true if all parents of this Component are visible. If only
   * one of the parents is invisible false is returned.
   * @return true if all parents are visible
   */
  @Override
  public boolean isVisible() {
    Component parent = this.getParentContainer();
    if (this.getComponentState().isVisible()) {
      if (parent != null) {
        return parent.isVisible();
      }
    }
    return false;
  }

  @Override
  public void setVisible(boolean visible) {
    boolean changed = (isVisible() != visible);
    getComponentState().setVisible(visible);
    if (parent != null && changed) {
      parent.contentChanged();
    }
  }

  /**
   * Sets the info text for given info type. It depends on component's implementation
   * how that info text is presented
   *
   * @param infoType the type of info, one of the constants INFO_*
   * @param infoText the info text
   */
  public void setInfoText( int infoType, String infoText ) {
    if (infoType == ViewPI.INFO_ERROR) {
      this.errorText = infoText;
    }
  }

  @Override
  public boolean command( QName cmd, char inputChar, int index ) {
    if (listener == null) {
      return false;
    }
    else {
      return listener.command( this.compID, cmd, inputChar, index );
    }
  }

  /**
   * Returns the Container this component is part of. In some cases (Popup) that is different
   * from getLayoutParent()
   *
   * @return the Container this component is part of
   */
  @Override
  public Container getParentContainer() {
    return getLayoutParent();
  }

  /**
   * Returns the components parent container.
   * The parent container is responsible for layout and position of the component.
   * @return the parent container
   */
  @Override
  public Container getLayoutParent() {
    return parent;
  }

  /**
   * Sets the components parent container.
   * This method is called while adding a component to a container.
   * @param parent
   */
  @Override
  public void setLayoutParent(Container parent) {
    this.parent = parent;
    setVisible((parent != null));
  }

  /**
   * Returns true if this component is a descendant of othercomponent.
   * @param otherComponent the component to check
   * @return true if this component is a descendant of othercomponent
   */
  @Override
  public boolean isDescendant(Component otherComponent) {
    if (otherComponent instanceof Container) {
      Component parent = getParentContainer();
      if (parent == null) return false;
      if (parent == otherComponent) return true;
      if (parent instanceof Container) {
        return parent.isDescendant(otherComponent);
      }
    }
    return false;
  }

 /**
  * Returns this component's foreground color
  * @return this component's foreground color
  */
 @Override
  public ComponentColor getForeground () {
    if (DEBUG) Logger.debug(ME + ".getForeground(...) begin ");
    return foreground;
  }

  /**
   * Sets the foreground color of this component.
   * ATTENTION do not use this method while painting is in process (doPaint()); use setCompForeground() instead!
   * @param color the new foreground color
   */
  @Override
  public void setForeground (ComponentColor color) {
    if (DEBUG) Logger.debug(ME + ".setForeground(...) begin ");
    if ((color != null) && (!color.equals(this.foreground))) {
      this.foreground = color;
      this.repaint();
    }
  }

 /**
  * Returns this component's background color
  * @return this component's background color
  */
 @Override
 public ComponentColor getBackground () {
   if (DEBUG) Logger.debug(ME + ".getBackground(...) begin ");
   return background;
 }

  /**
   * Sets the background of this component
   * ATTENTION do not use this method while painting is in process (doPaint()); use setCompBackground() instead!
   * @param color the new background color
   */
  @Override
  public void setBackground (ComponentColor color) {
    if (DEBUG) Logger.debug(ME + ".setBackground(...) begin ");
    if ((color == null) || (!color.equals(this.background))) {
      this.background = color;
      this.repaint();
    }
  }

  /**
   * Gets the compFont of this component.
   * @return This component's compFont.
   */
  @Override
  public ComponentFont getFont () {
    if (DEBUG) Logger.debug(ME + ".getFont(...) begin ");
    return this.compFont;
  }

  /**
   * Sets the compFont of this component.
   * @param font the new compFont for this component
   */
  @Override
  public void setFont (ComponentFont font) {
    if (DEBUG) Logger.debug( ME + ".setFont(...) begin ");
    if ((font != null) && (!font.equals(this.compFont))) {
      this.compFont = font;
      this.repaint();
    }
  }
  /**
   * Returns this Controller's current input mode
   *
   * @return this Controller's current input mode
   */
  @Override
  public InputMode getInputMode() {
    return this.inputMode;
  }

  /**
   * Sets this Controller's current input mode
   *
   * @param inputMode this Controller's new input mode
   */
  @Override
  public void setInputMode( InputMode inputMode ) {
    this.inputMode = inputMode;
  }

  /**
   * Typically no action is neede for normal compoennts.
   * todo May be used to do a new layout when text field's content has changed?
   */
  @Override
  public void contentChanged() {
    return;
  }

  protected void processScroller( Position posInComp ) {
    if (listener != null) {
      int count = posInComp.getY();
      if (count > 0) {
        for (int i = 0; i < count; i++) {
          listener.command( getID(), InputManager.CMD_DOWN, (char) 0, 0 );
        }
      }
      else {
        for (int i = 0; i > count; i--) {
          listener.command( getID(), InputManager.CMD_UP, (char) 0, 0 );
        }
      }
    }
  }

  /**
   * Call the action valid for the mouse event.
   * The action is defined in the controller.
   * @param state
   * @param lastPointedComponent
   * @param posInComp
   */
  @Override
  public void processMouseEvent(int state, Component lastPointedComponent, Position posInComp) {
    if (DEBUG) Logger.debug( ME + ".processMouseEvent(...) begin ");

    if (state == InputManager.STATE_SELECTED || state == InputManager.STATE_LOCAL_MENU ){
      if (lastPointedComponent == this) {
        setState( posInComp.getX(), posInComp.getY(), Boolean.TRUE, Boolean.TRUE, null, null );
        if (listener != null) {
          listener.selected( this.getID(), posInComp.getX(), posInComp.getY(), componentState.isSelected() );
        }
      }
    }
    else if (state == InputManager.STATE_SCROLL) {
      processScroller( posInComp );
    }
  }

  /**
   * Updates this component's fields and children from the give componen defintion
   * @param compDef the component definition
   */
  public void fromCompDef( CompDef compDef ) {
    if (compDef != null) {
      QName compID = compDef.getID();
      if ((compID != null) && (compID != this.compID)) {
        throw new IllegalArgumentException( "Changing component ID is not allowed, old="+this.compID+", new="+compID );
      }
    }
  }

  @Override
  public final void valueChanged( Binding source, Object newValue ) {
    try {
      Model model = this.binding.getModel(); 
      if (model != null) {
        String xml = (String) model.get( ((AttributeBinding) source).getAttributeName() );
        if ((xml != null) && (xml.length() > 0)) {
          InputStream in = new ByteArrayInputStream( xml.getBytes() );
          XmlReader reader = new XmlReader();
          CompDef compDef = (CompDef) reader.loadData( in, null );
          fromCompDef( compDef );
        }
        else {
          fromCompDef( null );
        }
      }  
      else {
        fromCompDef( null );
      }
    }
    catch (IOException e) {
      Logger.error( "Error reading/prodcessing comp definition", e );
      fromCompDef( null );
    }
  }

  @Override
  public void modelChanged( Binding source, Model newModel ) {
    // do nothing
  }

  /**
   * Called by Binding after it's modelSource has been changed
   *
   * @param source
   * @param modelSource the new modelSource, never null
   */
  @Override
  public void modelSourceChanged( Binding source, ModelSource modelSource ) {
    // do nothing - only valid for ValueController
  }

  @Override
  public void closing() {
    // default: do nothing
  }
}
