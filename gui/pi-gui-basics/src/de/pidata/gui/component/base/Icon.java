/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

public class Icon extends AbstractComponent {

  private ComponentBitmap componentBitmap;
  private String     buffer;
  private String     oldValue;

  public Icon(ComponentBitmap componentBitmap) {
    this.componentBitmap = componentBitmap;
  }

  public ComponentBitmap getBitmap() {
    return componentBitmap;
  }

  public void setBitmap(ComponentBitmap componentBitmap) {
    this.componentBitmap = componentBitmap;
  }

  /**
   * Sets this renderer's value
   *          @param value
   @param row
   @param col

   */
  public Component initRenderer(Object value, int row, int col) {
    if (value instanceof ComponentBitmap) {
      this.componentBitmap = (ComponentBitmap) value;
    }
    else if (value instanceof QName) {
      this.componentBitmap = Platform.getInstance().getBitmap( (QName) value);
    }
    else {
      this.componentBitmap = Platform.getInstance().getBitmap( GuiBuilder.NAMESPACE.getQName( value.toString() ) );
    }
    return this;
  }

  /**
   * Updates the value at posX, posY. The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX  horizontal position, starting from 0
   * @param posY  vertical position, starting from 0
   * @param valueType
   * @param value the new value
   */
  public void updateValue( short posX, short posY, Type valueType, Object value ) {
    this.buffer = (String) value;
  }

  /**
   * Inserts the value before posX, posY. The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX  horizontal position, starting from 0, to insert after last give posX = last+1
   * @param posY  vertical position, starting from 0, to insert after last give posY = last+1
   * @param valueType
   * @param value the value to be inserted
   */
  public void insertValue( short posX, short posY, SimpleType valueType, Object value ) {
    //TODO
    throw new RuntimeException("TODO");
  }

  public void setCursorPos( short cursorPosX, short cursorPosY ) {
    // do nothing
  }

  public QName getFormat() {
    return null;
  }

  /**
   * Calculates this component's min, max and pref sizes in one direction and
   * stores them in sizeLimit
   *
   * @param direction
   * @param sizeLimit
   */
  public void calcSizes(Direction direction, SizeLimit sizeLimit) {
    short size;
    if (direction == Direction.X) {
      size = (short) componentBitmap.getWidth();
    }
    else {
      size = (short) componentBitmap.getHeight();
    }
    sizeLimit.set(size, size, size);
    this.getPaintState(direction).doPaintState(PaintState.STATE_SIZES_CALC);
  }

  /**
   * This method is called whenever a state change occured. Each sub class
   * has to decide what to do, e.g. repaint its content
   */
  public void stateChanged( int oldState, int newState ) {
    // do nothing
  }

  /**
   * Called for painting this component
   * This method has to be implemented for each individual component.
   *
   * @param graph the graphics context to use for painting
   */
  public void doPaintImpl(ComponentGraphics graph) {
    if (!allowPaint()) {
      return;
    }
    if ((buffer != null) && (buffer.length() > 0)) {
      String value = buffer.toString();
      if (!value.equals(oldValue)) {
        this.componentBitmap = Platform.getInstance().getBitmap( GuiBuilder.NAMESPACE.getQName( value ) );
        oldValue = value;
      }
    }
    short width = getSizeLimit(Direction.X).getCurrent();
    short height = getSizeLimit(Direction.Y).getCurrent();
    ComponentColor background = getBackground();
    graph.paintBackground( background, 0, 0, width, height);
    graph.paintBitmap(componentBitmap, 0, 0, width, height);
  }
}
