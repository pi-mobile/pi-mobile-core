/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.gui.layout.LayoutInfo;
import de.pidata.gui.layout.Layoutable;

import java.util.Vector;

/**
 * Allgemeine Verwaltung der Components im Container
 */
public abstract class AbstractContainer extends AbstractComponent implements Container {

  private static final boolean DEBUG = false;
  private static final String ME = "AbstractContainer";

  protected Vector   compList = new Vector();

  /** Virtual size of all children; may be greater than the container's size if scrollers are present. */
  private short childrenSize[] = new short[2];

  protected AbstractContainer () {
    getLayoutInfo( Direction.X ).setAlignment( LayoutInfo.ALIGN_GROW );
    getLayoutInfo( Direction.Y ).setAlignment( LayoutInfo.ALIGN_GROW );
  }

  /**
   * Called whenever this containter's content has changed and so it's size limits are invalid.
   * If container is visible the current implementation resets PaintState.STATE_SIZES_CALC and
   * calls calcSizes() for both directions.
   */
  public void contentChanged() {
    getPaintState(Direction.X).undoPaintState(PaintState.STATE_SIZES_CALC);
    getPaintState(Direction.Y).undoPaintState(PaintState.STATE_SIZES_CALC);
    if (isVisible()) {
      calcSizes(Direction.X, getSizeLimit(Direction.X));
      calcSizes(Direction.Y, getSizeLimit(Direction.Y));
      getPaintState(Direction.X).doPaintState(PaintState.STATE_BOUNDS_SET);
      getPaintState(Direction.Y).doPaintState(PaintState.STATE_BOUNDS_SET);
      doLayout(Direction.X);
      doLayout(Direction.Y);
      repaint();
    }
  }

  /**
   * add any Component to the list of components
   *
   * @param layoutable
   * @return index of component in list of components
   */
  public int add( Layoutable layoutable ){
    int index = compList.size();
    this.compList.addElement( layoutable );
    layoutable.setLayoutParent(this);
    contentChanged();
    return index;
  }

  public void removeAll () {
    int childCount = childCount();
    Layoutable comp;
    for (int i=childCount-1; i>=0; i--) {
      comp = getChild(i);
      comp.setLayoutParent(null);
      compList.removeElementAt(i);
    }
    contentChanged();
  }

  public void removeAt( int index ) {
    Layoutable comp = getChild(index);
    comp.setLayoutParent(null);
    compList.removeElementAt(index);
    contentChanged();
  }

  public void remove( Layoutable layoutable ) {
    layoutable.setLayoutParent(null);
    compList.removeElement( layoutable );
    contentChanged();
  }


  public int childCount(){
    return this.compList.size();
  }

  public Layoutable getChild(int index){
    return (Layoutable) this.compList.elementAt(index);
  }

  public Component findComponentAt( Position pos ) {
    Layoutable layoutable = findLayoutableAt( pos );
    if (layoutable instanceof Component) {
      return (Component) layoutable;
    }
    else {
      return this;
    }
  }

  /**
   * Returns true if posX is between x and (x + width)
   * @param pos the x position
   * @return true if posX is between x and (x + width)
   */
  public static boolean containsPos( Layoutable layoutable, Direction direction, short pos) {
    short posRel = (short) (pos - layoutable.getPos(direction));
    if (posRel < 0) return false;
    if (posRel < layoutable.getSizeLimit( direction ).getCurrent()) return true;
    return false;
  }

  /**
   * Calculates the position relative to the windows upper left corner
   * @param direction
   * @return the calculated position
   */
  public static short calcPosInWindow( Layoutable layoutable, Direction direction) {
    Layoutable parent = layoutable.getLayoutParent();
    short parentPos = 0;
    if ((parent != null) && ( !(parent instanceof Dialog) )) {
      parentPos = calcPosInWindow( parent, direction );
    }
    return (short) (layoutable.getPos(direction) + parentPos);
  }

  public Layoutable findLayoutableAt(Position pos){
    Layoutable comp;
    int compIndex = this.childCount() - 1;

    short x = pos.getX();
    short y = pos.getY();

    //--- find component in inner area. To support overlaping components we must begin
    //    with last component in list
    while (compIndex >= 0) {
      comp = this.getChild(compIndex);
      if ( containsPos( comp, Direction.X, x) && containsPos( comp, Direction.Y, y) ) {
        if (comp.isVisible()) {
          pos.translate(comp.getPos(Direction.X), comp.getPos(Direction.Y));
          if ( comp instanceof Container) {
            return ((Container)comp).findComponentAt (pos);
          }
          return comp;
        }
      }
      compIndex--;
    }

    return this;
  }

  /**
   * Only special implementation of AbstractContainer may have a border, so this returns null.
   * @return null
   */
  public Border getBorder() {
    return null;
  }

  public void setBorder(Border border) {
    throw new IllegalArgumentException("Border not supported by this container!");
  }

  /**
   * Only special implementation of AbstractContainer may have a border, so this returns null.
   * @return null
   */
  public Scroller getScroller(Direction direction) {
    return null;
  }


  /**
   * Called for painting this component
   * @param graph the graphics context to use for painting
   */
  public void doPaintImpl(ComponentGraphics graph) {
    if (!allowPaint()) {
      return;
    }
    int width = getSizeLimit( Direction.X ).getCurrent();
    int height = getSizeLimit( Direction.Y ).getCurrent();

    //----- Draw background
    graph.paintBackground(getBackground(), 0,  0, width, height);

    doPaintChildren( this, graph );
  }

  /**
   * Paint all children in the container.
   * @param container
   * @param compGraph
   */
  public void doPaintChildren( Container container, ComponentGraphics compGraph ) {
    int compCount = this.childCount();
    Layoutable child;
    short compX;
    short compY;
    short width;
    short height;
    Rect oldClip = new Rect();

    for( int i = 0; i<compCount; i++ ){
      child = this.getChild(i);

      compX = child.getPos(Direction.X);
      compY = child.getPos(Direction.Y);
      width = child.getSizeLimit(Direction.X).getCurrent();
      height = child.getSizeLimit(Direction.Y).getCurrent();

      // nur malen, wenn innerhalb des angezeigten Bereichs
      // der angezeigte Bereich startet immer mit 0!
      //TODO Komponenten die ganz drau0en sind überspringen
      // if (compX >= 0 && width > 0 &&
      //    compY >= 0 && height > 0) {
      compGraph.translate(compX, compY);
      compGraph.getClip(oldClip);

      compGraph.clipRect( (short) 0, (short) 0, (short) (width + 1), (short) (height + 1) );
      child.doPaint(compGraph);

      compGraph.setClip(oldClip);
      compGraph.translate((short) -compX, (short) -compY);
      //}
    }
  }

  /**
   * Translate and clip graph for containers content
   */
  public void adaptContentGraphics(ComponentGraphics compGraph, Position translation) {
    short width = this.getSizeLimit( Direction.X ).getCurrent();
    short height = this.getSizeLimit( Direction.Y ).getCurrent();

    compGraph.clipRect((short) 0, (short) 0, width, height);
  }

  /**
   * Translates and clips graph for a component to the origin of dialog
   * @param compGraph
   * @param translation
   */
  public void adaptGraphics (ComponentGraphics compGraph, Position translation) {
    Container parent;
    short posX = getPos(Direction.X);
    short posY = getPos(Direction.Y);
    parent = getLayoutParent();
    if (parent != null) {
      parent.adaptGraphics(compGraph, translation);
    }
    compGraph.translate(posX, posY);
    translation.translate(posX, posY);
    adaptContentGraphics(compGraph, translation);
  }

  /**
   * Scroll this container if possible so that the range form pos to pos+size gets visible.
   * @param dir Direction
   * @param pos  beginning of the range to make visible
   * @param size the size of the range to make visible 
   */
  public void scrollToVisible(Direction dir, int pos, int size) {
    Scroller scroller = getScroller(dir);

    int visSize = clientSize(dir);

    int newPos = pos;
    if (pos < 0) {
      if (scroller != null ) scroller.setValueFit(scroller.getValue() + pos);
      newPos = 0;
    }
    else if ((pos + size) > visSize) {
      if (scroller != null ) scroller.setValueFit(scroller.getValue() + pos - visSize + size);
      newPos = visSize - size;
      if (newPos < 0) newPos = 0;
    }

    Container parent = getLayoutParent();
    if (parent != null) {
      parent.scrollToVisible(dir, newPos + getPos(dir), size);
    }
  }

  /**
   * Get the size of the area which can be used while painting children.
   * @param dir direction
   * @return useable size within given direction
   */
  public short clientSize(Direction dir) {
    short visSize = getSizeLimit(dir).getCurrent();
    Scroller oppScroll = getScroller(dir.getOpposite());
    if (oppScroll != null) {
      if (oppScroll.isVisible()) {
        visSize =(short) (visSize - oppScroll.getSizeLimit(dir).getCurrent());
      }
    }
    return visSize;
  }

  /**
   * Remember the total size of all children as calculated by the layouter.
   * @param childrenSize
   */
  public void setChildrenSize(Direction direction, short childrenSize) {
    this.childrenSize[direction.getIndex()] = childrenSize;
  }

  public short getChildrenSize(Direction direction) {
    return childrenSize[direction.getIndex()];
  }

  /**
   * Determine if container does layout for itself, i.e. it has a layouter
   * for direction.
   * @param direction the direction
   * @return true; only panel without own layouter may return false.
   */
  public boolean doesLayout (Direction direction) {
    return true;
  }

  /**
   * Determines the size which can be used by the layouter to place the container's
   * children. This size may be lesser than the container's size if scrollbars have to be displayed.
   * @param direction
   * @param targetSize the size of the container's painting area
   * @param minSizeChilds
   * @param prefSizeChilds
   * @return the size available for layouting the containers's children
   */
  public short getLayoutSize( Direction direction, short targetSize, short minSizeChilds, short prefSizeChilds ) {
    return targetSize;
  }

  /**
   * Calculate and set container's sizes. If border or scroller is used,
   * the sizes will be added.
   * @param sizeLimit
   * @param direction
   */
  public void calcSizeLimit (SizeLimit sizeLimit, Direction direction) {
    int minSize = sizeLimit.getMin();
    int prefSize = sizeLimit.getPref();
    int maxSize = sizeLimit.getMax();
    Border border = getBorder();
    Scroller scroller = getScroller(direction.getOpposite());

    // if scroller is available, minSize = minimum scrollbar length + space for opposite scrollbar!
    if (scroller != null) {
      minSize = (short) (scroller.getMinScrollerLength() + scroller.getMinScrollerWidth());
      maxSize = Short.MAX_VALUE;
      if (prefSize < minSize) {
        prefSize = minSize;
      }
      if (maxSize < prefSize) {
        maxSize = prefSize;
      }
    }

    //----- add border's insets if exists
    if (border != null) {
      int insetsSize = border.get2BorderSize(direction);

      minSize   += insetsSize;
      prefSize  += insetsSize;
      maxSize   += insetsSize;
    }
    if (minSize > Short.MAX_VALUE) {
      minSize = Short.MAX_VALUE;
    }
    if (prefSize > Short.MAX_VALUE) {
      prefSize = Short.MAX_VALUE;
    }
    if (maxSize > Short.MAX_VALUE) {
      maxSize = Short.MAX_VALUE;
    }

    sizeLimit.set(minSize, prefSize, maxSize);
  }
}
