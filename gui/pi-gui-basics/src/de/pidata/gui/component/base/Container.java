/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.gui.layout.Layoutable;

/**
 * stellt Methoden für die allgemeine Verarbeitung des container bereit.
 * Die Methoden müssen plattformabhängig implenentiert werden.
 */
public interface Container extends Component {

  public Border getBorder();

  public void setBorder(Border border);

  public Scroller getScroller (Direction direction);

  /**
   * add any Component to the list of components
   * 
   * @param layoutable
   * @return index of component in list of components
   */
  public int add( Layoutable layoutable );

  void removeAll();

  void remove( Layoutable layoutable );

  void removeAt(int index);

  int childCount();

  public Layoutable getChild(int index);

  Component findComponentAt(Position pos);

  /**
   * Translates and clips graph for a component to the origin of dialog
   * @param translation
   */
  void adaptGraphics (ComponentGraphics compGraph, Position translation);

  /**
   * Calculate and set container's sizes. If border or scroller is used,
   * the sizes will be added.
   * @param sizeLimit
   * @param direction
   */
  void calcSizeLimit (SizeLimit sizeLimit, Direction direction);

  void scrollToVisible(Direction dir, int pos, int size);

  short clientSize(Direction dir);

  void setChildrenSize(Direction direction, short childSize);

  short getChildrenSize(Direction direction);

  /**
   * Determine if container does layout for itself, i.e. it has a layouter
   * for direction.
   * @param direction the direction
   * @return true; only panel without own layouter may return false.
   */
  public boolean doesLayout( Direction direction );

 /**
   * Determines the size which can be used by the layouter to place the container's
   * children. This size may be lesser than the container's size if scrollbars have to be displayed.
   * @param direction
   * @param targetSize the size of the container's painting area
   * @param minSizeChilds
   * @param prefSizeChilds
  * @return the size available for layouting the containers's children
   */
  short getLayoutSize( Direction direction, short targetSize, short minSizeChilds, short prefSizeChilds );
}
