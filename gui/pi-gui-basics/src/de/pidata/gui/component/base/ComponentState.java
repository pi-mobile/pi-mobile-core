/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

/**
 * beschreibt den Status der Component, anhand der unterschiedliches Aussehen gezeichnet werden muss
 * in Abhängigkeit von den Anwenderaktionen
 *
 * In Abhängigkeit von Programmaktionen nimmt eine Komponente verschieden Zustände an,
 * die zu unterschiedlichen Darstellungen führen.
 *
 * visible / invisible
 * mouse is over component
 * component has focus
 * user pressed mouse key
 * compnent is activ/inactive
 *
 * PresentationState /
 */
public class ComponentState {
  public static final int STATE_NORMAL = 0;
  public static final int STATE_OVER = 1;
  public static final int STATE_DISABLED = 2;
  public static final int STATE_DISABLED_OVER = 3;  // normally not used
  public static final int STATE_NORMAL_FOCUS = 4;
  public static final int STATE_OVER_FOCUS = 5;
  public static final int STATE_DISABLED_FOCUS = 6; // normally not used
  public static final int STATE_DISABLED_OVER_FOCUS = 7; // normally not used
  public static final int STATE_PRESSED = 8;
  public static final int STATE_PRESSED_OVER = 9;
  public static final int STATE_DISABLED_PRESSED = 10;
  public static final int STATE_DISABLED_PRESSED_OVER = 11; // normally not used
  public static final int STATE_PRESSED_FOCUS = 12;
  public static final int STATE_PRESSED_OVER_FOCUS = 13;
  public static final int STATE_DISABLED_PRESSED_FOCUS = 14; // normally not used
  public static final int STATE_DISABLED_PRESSED_OVER_FOCUS = 15; // normally not used

  public static final int STATE_MASK_OVER = 1;
  public static final int STATE_MASK_DISABLED = 2;
  public static final int STATE_MASK_FOCUS = 4;
  public static final int STATE_MASK_PRESSED = 8;

  private Component owner; // used to hold the referencing component

  public int state = STATE_NORMAL; // this holds the actual state; please use methods to set/get!

  /**
   * If visible is false painting is suppressed. Do not access this member
   * directly, use isVisible() and setVisible() instead
   */
  private boolean visible = false;

  public ComponentState(Component owner) {
    this.owner = owner;
  }

  public int getState() {
    return state;
  }

  public void setState(int state) {
    int oldState = this.getState();
    if (oldState != state) {
      this.state = state;
      this.owner.stateChanged(oldState, state);
    }
  }

  /**
   * Set state without repaint event
   * @param state
   */
  public void setInitialState(int state) {
    this.state = state;
  }

  /**
    * Returns true if all parents of this Component are visible. If only
    * one of the parents is invisible false is returned.
    * @return true if all parents are visible
    */
  public boolean isVisible() {
    return this.visible;
  }

   /**
    * Sets the visible state of this container. Setting true is only successful
    * if all parents of this container are already visible
    */
  public void setVisible(boolean visible) {
    this.visible = visible;
  }

  /**
   * Sets the pointer state for this component. Most sub classes will change
   * their skinned depending on the state. The coordinate (x,y) mav be (0,0) for
   * special pointing devices e.g. selection wheels like used in car navigation
   * systems.<BR>
   * This default implementation does nothing.
   * @param over    true if the pointer is over this component
   * @param pressed true if the pointer button used for selection is pressed.
   *                For a mouse this is the button1 (left).
   */
  public void setCompState(boolean over, boolean pressed) {
     int newState = this.getState();

     if (over) {
       newState |= STATE_MASK_OVER;
     }
     else {
       newState &= (~STATE_MASK_OVER);
     }

     if (pressed) {
       newState |= STATE_MASK_PRESSED;
     }
     else {
       newState &= (~STATE_MASK_PRESSED);
     }

     this.setState(newState);
   }

  public void setSelected(boolean selected) {
    int newState = this.getState();

    if (selected) {
      newState |= STATE_MASK_PRESSED;
    }
    else {
      newState &= (~STATE_MASK_PRESSED);
    }

    this.setState(newState);
  }

  /**
   * @return the selected state of this component
   */
  public boolean isSelected() {
    return ((this.getState() & STATE_MASK_PRESSED) != 0);
  }

  public void setOver(boolean isOver) {
    int newState = this.getState();

    if (isOver) {
      newState |= STATE_MASK_OVER;
    }
    else {
      newState &= (~STATE_MASK_OVER);
    }

    this.setState(newState);
  }

  public boolean isOver() {
    return ((this.getState() & STATE_MASK_OVER) != 0);
  }

  /**
   * Returns the pressed state of the last pointer state sent to this
   * component.
   * @return the pressed state of this component
   */
  public boolean isPressed() {
    return this.isSelected();
  }

  public boolean isFocusable() {
    return true;
  }

  /**
   * Sets the enabled state of this component
   * @param enabled true for enabled, false for disabled
   */
  public void setEnabled(boolean enabled) {
    int newState = this.getState();

    //----- Set/clear disabled flag in state
    if (enabled) {
      newState &= (~STATE_MASK_DISABLED);
    }
    else {
      newState |= STATE_MASK_DISABLED;
    }

    this.setState(newState);
  }

  /** Returns the enabled state of this component.
   * @return true if this component is enabled
   */
  public boolean isEnabled() {
    return ((this.getState() & STATE_MASK_DISABLED) == 0);
  }


  /** Called by this Component's dialog element when the corresponding
   * parameter gained or lost focus.
   */
  public void setFocus(boolean hasFocus) {
    if (isFocusable()) {
      int newState = this.getState();

      //----- Set/clear focus flag in state
      if (hasFocus) {
        newState |= STATE_MASK_FOCUS;
      }
      else {
        newState &= (~STATE_MASK_FOCUS);
      }

      this.setState(newState);
    }
  }

  /** Returns true if this skinned component has input focus
   * @return true if this skinned component has input focus
   */
  public boolean hasFocus() {
    return isFocusState(this.getState());
  }

  /**
   * Return true if the given state indicates a focussed component.
   * @param state
   * @return true if the given state indicates a focussed component
   */
  public boolean isFocusState(int state) {
    return (( state & STATE_MASK_FOCUS) != 0);
  }
}
