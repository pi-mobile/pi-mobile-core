/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.base;

import de.pidata.gui.event.InputManager;
import de.pidata.log.Logger;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.Calendar;

public class CharDateBufferHandler extends CharBufferHandler {

  public static final Namespace NAMESPACE = Namespace.getInstance("de.pidata.gui");

  // Contains the current date
  private Calendar calendar = DateObject.getCalendar();

  // if the user moves the cursor it will be set 'false'
  private boolean state_new = true;

  // States which represent the position in the field
  // used for date
  private static final int STATE_DAY_FIRST = 1;
  private static final int STATE_DAY_SECOND = 2;
  private static final int STATE_MONTH_FIRST = 3;
  private static final int STATE_MONTH_SECOND = 4;
  private static final int STATE_YEAR_FIRST = 5;
  private static final int STATE_YEAR_SECOND = 6;
  private static final int STATE_YEAR_THIRD = 7;
  private static final int STATE_YEAR_FOURTH = 8;
  private static final int STATE_DATE_END = 9;
  // used for time
  private static final int STATE_HOUR_FIRST = 10;
  private static final int STATE_HOUR_SECOND = 11;
  private static final int STATE_MINUTE_FIRST = 12;
  private static final int STATE_MINUTE_SECOND = 13;
  private static final int STATE_TIME_END = 14;
  // represents input state: STATE_DAY_FIRST, STATE_HOUR_FIRST ...
  private int current_input_state;
  // if state_new the backspace command will use this history
  // if you enter a new character, the state is inserted
  // if you remove a character, the history_index is used to resolv the last state
  private int[] state_history = new int[15];
  // represents the index for the state_history
  private int history_index = 0;

  private static final char CHAR_CONSTANT_POINT = '.';
  private static final char CHAR_CONSTANT_COLON = ':';
  private static final char CHAR_CONSTANT_CERO = '0';

  private QName dateFormat = DateTimeType.TYPE_DATE;

  // supported formats: update the list, if necessary
  // 31.12.03 instead of 31.12.2003
  public static final QName FORMAT_SHORTYEAR = NAMESPACE.getQName("SHORTYEAR");
  public static final QName FORMAT_TIME = NAMESPACE.getQName("TIME");


  public CharDateBufferHandler( TextLine textLine, int maxLength ) {
    super( textLine, maxLength );
  }

  public void setDateFormat( QName dateFormat ) {
    this.dateFormat = dateFormat;
  }

  private String getDisplayValue() {
    return super.getValue();
  }

  private void setDisplayValue( String value ) {
    super.setValue( value );
  }

  public void setValue( String value ) {
    String strValue = null;
    // Following is not MIDP compatible: buffer.replace(0, buffer.length(), (String) valueObject);
    //    buffer.delete(0, buffer.length());
    if (buffer.length() > 0) clear();

    if (value == null || value.length() == 0) {
      clear();
    }
    else {
      DateObject dateObject = new DateObject( dateFormat, value );
      strValue = dateObject.toDisplayString( calendar, dateFormat, (format == FORMAT_SHORTYEAR) );

      if (strValue != null) {
        buffer.insert( 0, strValue );
        buffer.setCursorPosition( buffer.length() );
      }
      state_new = false;
    }
    resetState();
    setDisplayValue( strValue );
  }

  public String getValue( ) {
    // retrieve the current field value
    String dateString = getDisplayValue();
    if ((dateString == null) || (dateString.length() == 0)) {
      return null;
    }
    dateString = dateString.trim();
    boolean isInputWrong = false;
    try {
      if (dateFormat == DateTimeType.TYPE_DATE) {

        if (dateString != null && dateString.length() == 10) {
          String tempString = dateString.substring(0, 2);
          short day = Short.parseShort(tempString);
          tempString = dateString.substring(3, 5);
          short month = Short.parseShort(tempString);
          tempString = dateString.substring(6, dateString.length());
          short year = Short.parseShort(tempString);
          updateCalendar(day, month, year);
        }
        else if (dateString != null && dateString.length() == 8 && format != null && format == FORMAT_SHORTYEAR) {
          String tempString = dateString.substring(0, 2);
          short day = Short.parseShort(tempString);
          tempString = dateString.substring(3, 5);
          short month = Short.parseShort(tempString);
          tempString = dateString.substring(6, dateString.length());
          short year = Short.parseShort(tempString);
          year += (short) 2000;
          updateCalendar(day, month, year);
        }
        else {
          isInputWrong = true;
        }
      }
      else if (dateFormat == DateTimeType.TYPE_TIME) {
        if (dateString != null && dateString.length() == 5) {
          String tempString = dateString.substring(0, 2);
          short hour = Short.parseShort(tempString);
          tempString = dateString.substring(3, 5);
          short minute = Short.parseShort(tempString);
          updateCalendar(hour, minute);
        }
        else {
          isInputWrong = true;
        }
      }
      else if (dateFormat == DateTimeType.TYPE_DATETIME) {

        if (dateString != null && dateString.length() > 10) {
          String tempString = dateString.substring(0, 2);
          short day = Short.parseShort(tempString);
          tempString = dateString.substring(3, 5);
          short month = Short.parseShort(tempString);
          if ((format == null && dateString.length() == 16)
                  || (format != null && dateString.length() == 16)) {
            tempString = dateString.substring(6, 10);
            short year = Short.parseShort(tempString);
            tempString = dateString.substring(11, 13);
            short hour = Short.parseShort(tempString);
            tempString = dateString.substring(14, 16);
            short minute = Short.parseShort(tempString);
            updateCalendar(day, month, year, hour, minute);
          }
          else if (format != null && format == FORMAT_SHORTYEAR && dateString.length() == 14) {
            tempString = dateString.substring(6, 8);
            short year = Short.parseShort(tempString);
            year += (short) 2000;
            tempString = dateString.substring(9, 11);
            short hour = Short.parseShort(tempString);
            tempString = dateString.substring(12, 14);
            short minute = Short.parseShort(tempString);
            updateCalendar(day, month, year, hour, minute);
          }
          else {
            isInputWrong = true;
          }
        }
        else if (dateString != null && dateString.length() == 10) {
          String tempString = dateString.substring(0, 2);
          short day = Short.parseShort(tempString);
          tempString = dateString.substring(3, 5);
          short month = Short.parseShort(tempString);
          tempString = dateString.substring(6, dateString.length());
          short year = Short.parseShort(tempString);
          short hour = 0;
          short minute = 0;
          updateCalendar(day, month, year, hour, minute);
        }
        else if (dateString != null && format != null && format == FORMAT_SHORTYEAR && dateString.length() == 8) {
          String tempString = dateString.substring(0, 2);
          short day = Short.parseShort(tempString);
          tempString = dateString.substring(3, 5);
          short month = Short.parseShort(tempString);
          tempString = dateString.substring(6, dateString.length());
          short year = Short.parseShort(tempString);
          year += 2000;
          short hour = 0;
          short minute = 0;
          updateCalendar(day, month, year, hour, minute);
        }
        else {
          isInputWrong = true;
        }
      }
      if (isInputWrong) {
        throw new IllegalArgumentException("Wrong input.");
      }
    }
    catch (Exception e) {
      Logger.warn( "Could not convert Date value=" + dateString + ", msg=" + e.getMessage() );
      setDisplayValue( "" );
    }
    if (!isInputWrong) {
      DateObject dateObject = new DateObject( dateFormat, calendar.getTime().getTime() );
      return dateObject.toString();
    }
    return null;
  }

  /**
   * Tells this dialogController to process the given commandID and inputChar. If this dialogController
   * cannot process that command, false is returned which allows InputManager to
   * send that command to this dialogController's parent.
   *
   * @param commandID the command to process, see constants defined by InputManager
   * @param inputChar the character to process, if command delivers a character
   * @return true if the command was processed
   */
  public boolean processCommand(QName commandID, char inputChar, int index) {
    if (textLine.isEnabled()) {
      if (commandID == InputManager.CMD_SELECT) {
        if (buffer.length() > 1) {
          buffer.setCursorPosition( index );
        }
        return true;
      }
      else if (commandID == InputManager.CMD_INPUT) {

        evaluateInput(inputChar);
        return true;
      }
      else if (commandID == InputManager.CMD_BACKSPACE) {
        /* Deaktiviert, da die State-Machine nicht richtig funktioniert
        if (state_new) {
          history_index = history_index - 1;

          if (history_index > -1) {
            int history_state = state_history[history_index];
            int bufferLength = buffer.length();
            int calls = 0;

            if (dateFormat == DateTimeType.DATE
                || dateFormat == DateTimeType.DATETIME) {

              switch (history_state) {

                case STATE_DAY_FIRST:
                  // do some stuff
                  calls = bufferLength;

                  break;

                case STATE_DAY_SECOND:
                  // do some stuff
                  calls = bufferLength - 1;

                  break;

                case STATE_MONTH_FIRST:
                  // do some stuff
                  calls = bufferLength - 3;

                  break;

                case STATE_MONTH_SECOND:
                  //do some stuff
                  calls = bufferLength - 4;

                  break;

                case STATE_YEAR_FIRST:
                  //do some stuff
                  calls = bufferLength - 6;

                  break;

                case STATE_YEAR_SECOND:
                  //do some stuff
                  calls = bufferLength - 7;

                  break;

                case STATE_YEAR_THIRD:
                  //do some stuff
                  if (format == null) {
                    calls = bufferLength - 8;
                  }
                  else if (format == FORMAT_SHORTYEAR) {
                    calls = bufferLength - 6;
                  }


                  break;

                case STATE_YEAR_FOURTH:
                  //do some stuff
                  if (format == null) {
                    calls = bufferLength - 9;
                  }
                  else if (format == FORMAT_SHORTYEAR) {
                    calls = bufferLength - 7;
                  }


                  break;

                case STATE_HOUR_FIRST:
                  // do some stuff
                  if (format == null) {
                    calls = bufferLength - 11;
                    //                } else if (format.equals("shortYear")) {
                  }
                  else if (format == FORMAT_SHORTYEAR) {
                    calls = bufferLength - 9;
                  }

                  break;

                case STATE_HOUR_SECOND:
                  // do some stuff
                  if (format == null) {
                    calls = bufferLength - 12;
                  }
                  else if (format == FORMAT_SHORTYEAR) {
                    calls = bufferLength - 10;
                  }

                  break;

                case STATE_MINUTE_FIRST:
                  // do some stuff
                  if (format == null) {
                    calls = bufferLength - 14;
                  }
                  else if (format == FORMAT_SHORTYEAR) {
                    calls = bufferLength - 12;
                  }

                  break;

                case STATE_MINUTE_SECOND:
                  //do some stuff
                  if (format == null) {
                    calls = bufferLength - 15;
                  }
                  else if (format == FORMAT_SHORTYEAR) {
                    calls = bufferLength - 13;
                  }

                  break;

                default :
                  // DO NOTHING
                  break;

                  //signal error
              }
            }
            else if (dateFormat == DateTimeType.TIME) {

              switch (history_state) {

                case STATE_HOUR_FIRST:
                  // do some stuff
                  calls = bufferLength;

                  break;

                case STATE_HOUR_SECOND:
                  // do some stuff
                  calls = bufferLength - 1;

                  break;

                case STATE_MINUTE_FIRST:
                  // do some stuff
                  calls = bufferLength - 3;

                  break;

                case STATE_MINUTE_SECOND:
                  //do some stuff
                  calls = bufferLength - 4;

                  break;

                default :
                  // DO NOTHING
                  break;
                  //signal error
              }
            }

            for (int i = 0; i < calls; i++) {
              moveCursorLeft();
              deleteCharFromCursor();
            }
            current_input_state = history_state;

          }
          else if (buffer != null && buffer.length() > 0) {
            history_index = 0;
            doSetValue(null);
          }
        }
        else { */
        setValue( "" );
        // }

        if (buffer.length() == 0) {
          resetState();
        }
        return true;
      }
      else if (commandID == InputManager.CMD_DELETE ) {
        deleteCharFromCursor();
        if (buffer.length() == 0) {
          resetState();
        }
        return true;
      }
    }
    if (commandID == InputManager.CMD_LEFT) {
      moveCursorLeft();
      repaintTextLine();
      if (buffer.length() > 0) {
        if (state_new)
          state_new = false;
      }
      return true;
    }
    else {
      return super.processCommand(commandID, inputChar, index);
    }
  }

  /*
   *
   */
  public void clear() {
    buffer.setValue( null, "" );
    state_new = true;
    buffer.setCursorPosition( 0 );
  }

  /**
   *
   *
   */
  private void resetState() {
    state_new = true;
    if (dateFormat == DateTimeType.TYPE_DATE) {
      current_input_state = STATE_DAY_FIRST;
    }
    else if (dateFormat == DateTimeType.TYPE_TIME) {
      current_input_state = STATE_HOUR_FIRST;
    }
    else if (dateFormat == DateTimeType.TYPE_DATETIME) {
      current_input_state = STATE_DAY_FIRST;
    }
    history_index = 0;
  }

  /**
   *
   */
  private void evaluateInput(char inputChar) {

    if (state_new) {
      if (Character.isDigit(inputChar)) {
        if (dateFormat == DateTimeType.TYPE_DATE) {
          evaluateDateInput(inputChar);
        }
        else if (dateFormat == DateTimeType.TYPE_TIME) {
          evaluateTimeInput(inputChar);
        }
        else if (dateFormat == DateTimeType.TYPE_DATETIME) {
          evaluateDateTimeInput(inputChar);
        }
      } // no digit
    }
    else {
      typeChar(inputChar);
    }
  }


  /**
   * @param inputChar
   */
  private void evaluateTimeInput(char inputChar) {
    switch (current_input_state) {

      case STATE_HOUR_FIRST:

        state_history[history_index] = current_input_state;
        history_index++;

        // do some stuff
        if (inputChar > 50 && inputChar <= 57) {
          char[] chars = {CHAR_CONSTANT_CERO, inputChar, CHAR_CONSTANT_COLON};
          insertChars(chars);
          current_input_state = STATE_MINUTE_FIRST;
          state_history[history_index] = current_input_state;
          history_index++;
        }
        else if (inputChar >= 48 && inputChar <= 50) {
          typeChar(inputChar);
          current_input_state = STATE_HOUR_SECOND;
          state_history[history_index] = current_input_state;
          history_index++;
        }
        break;

      case STATE_HOUR_SECOND:
        char firstHour = buffer.charAt(0);
        if (firstHour == 48 || firstHour == 49) {
          char[] chars = {inputChar, CHAR_CONSTANT_COLON};
          insertChars(chars);
          current_input_state = STATE_MINUTE_FIRST;
          state_history[history_index] = current_input_state;
          history_index++;
        }
        else if (firstHour == 50 && (inputChar >= 48 && inputChar <= 52)) {
          char[] chars = {inputChar, CHAR_CONSTANT_COLON};
          insertChars(chars);
          current_input_state = STATE_MINUTE_FIRST;
          state_history[history_index] = current_input_state;
          history_index++;
        }
        break;

      case STATE_MINUTE_FIRST:
        firstHour = buffer.charAt(0);
        char secondHour = buffer.charAt(1);

        if ((firstHour == 48 || firstHour == 49) && inputChar <= 53) {
          typeChar(inputChar);
          current_input_state = STATE_MINUTE_SECOND;
          state_history[history_index] = current_input_state;
          history_index++;
        }
        else if (firstHour == 50 && secondHour < 52 && inputChar <= 53) {
          typeChar(inputChar);
          current_input_state = STATE_MINUTE_SECOND;
          state_history[history_index] = current_input_state;
          history_index++;
        }
        else if (firstHour == 50 && secondHour == 52 && inputChar == 48) {
          char[] chars = {inputChar, inputChar};
          insertChars(chars);
          current_input_state = STATE_TIME_END;
          state_history[history_index] = current_input_state;
        }
        else if (inputChar > 53) {
          char[] chars = {CHAR_CONSTANT_CERO, inputChar};
          insertChars(chars);
          current_input_state = STATE_TIME_END;
          state_history[history_index] = current_input_state;
        }
        break;

      case STATE_MINUTE_SECOND:

        char firstMinute = buffer.charAt(3);

        if (firstMinute < 54) {
          typeChar(inputChar);
          current_input_state = STATE_TIME_END;
          state_history[history_index] = current_input_state;
        }
        break;
    }
  }

  /**
   * @param inputChar
   */
  private void evaluateDateInput(char inputChar) {
    switch (current_input_state) {

      case STATE_DAY_FIRST:

        state_history[history_index] = current_input_state;
        history_index++;
        // do some stuff
        if (inputChar > 51 && inputChar <= 57) {
          char[] chars = {CHAR_CONSTANT_CERO, inputChar, CHAR_CONSTANT_POINT};
          insertChars(chars);
          current_input_state = STATE_MONTH_FIRST;
          state_history[history_index] = current_input_state;
          history_index++;

        }
        else if (inputChar >= 48 && inputChar <= 51) {
          typeChar(inputChar);
          current_input_state = STATE_DAY_SECOND;
          state_history[history_index] = current_input_state;
          history_index++;
        }

        break;

      case STATE_DAY_SECOND:
        // do some stuff
        char firstDay = buffer.charAt(0);
        if (firstDay >= 48 && firstDay < 51) {
          char[] chars = {inputChar, CHAR_CONSTANT_POINT};
          insertChars(chars);
          current_input_state = STATE_MONTH_FIRST;
          state_history[history_index] = current_input_state;
          history_index++;
        }
        else if (firstDay == 51 && (inputChar == 48 || inputChar == 49)) {
          char[] chars = {inputChar, CHAR_CONSTANT_POINT};
          insertChars(chars);
          current_input_state = STATE_MONTH_FIRST;
          state_history[history_index] = current_input_state;
          history_index++;
        }

        break;

      case STATE_MONTH_FIRST:
        // do some stuff
        if (inputChar == 48 || inputChar == 49) {
          typeChar(inputChar);
          current_input_state = STATE_MONTH_SECOND;
          state_history[history_index] = current_input_state;
          history_index++;
        }
        else if (inputChar > 49 && inputChar <= 57) {
          char[] chars = {CHAR_CONSTANT_CERO, inputChar, CHAR_CONSTANT_POINT};
          insertChars(chars);
          if (format == null) {
            current_input_state = STATE_YEAR_FIRST;
            state_history[history_index] = current_input_state;
            history_index++;
          }
          else if (format == FORMAT_SHORTYEAR) {
            current_input_state = STATE_YEAR_THIRD;
            state_history[history_index] = current_input_state;
            history_index++;
          }
        }

        break;

      case STATE_MONTH_SECOND:
        //do some stuff
        char firstMonth = buffer.charAt(3);
        if (firstMonth == 48) {
          char[] chars = {inputChar, CHAR_CONSTANT_POINT};
          insertChars(chars);
          current_input_state = STATE_YEAR_FIRST;
          state_history[history_index] = current_input_state;
          history_index++;
        }
        else if (firstMonth == 49 && (inputChar >= 48 && inputChar < 51)) {
          char[] chars = {inputChar, CHAR_CONSTANT_POINT};
          insertChars(chars);
          if (format == FORMAT_SHORTYEAR) {
            current_input_state = STATE_YEAR_THIRD;
            state_history[history_index] = current_input_state;
            history_index++;
          }
          else {
            current_input_state = STATE_YEAR_FIRST;
            state_history[history_index] = current_input_state;
            history_index++;
          }
        }

        break;

      case STATE_YEAR_FIRST:
        //do some stuff
        typeChar(inputChar);
        current_input_state = STATE_YEAR_SECOND;
        state_history[history_index] = current_input_state;
        history_index++;

        break;

      case STATE_YEAR_SECOND:
        //do some stuff
        typeChar(inputChar);
        current_input_state = STATE_YEAR_THIRD;
        state_history[history_index] = current_input_state;
        history_index++;

        break;

      case STATE_YEAR_THIRD:
        //do some stuff
        typeChar(inputChar);

        current_input_state = STATE_YEAR_FOURTH;
        state_history[history_index] = current_input_state;
        history_index++;

        break;

      case STATE_YEAR_FOURTH:
        //do some stuff
        typeChar(inputChar);

        current_input_state = STATE_DATE_END;
        state_history[history_index] = current_input_state;

//      Date date = (Date)internalGetValue();

        break;

      default :
        // DO NOTHING
        break;

      //signal error
    }
  }

  /**
   * @param inputChar
   */
  private void evaluateDateTimeInput(char inputChar) {
    switch (current_input_state) {

      case STATE_DAY_FIRST:
        // do some stuff
        evaluateDateInput(inputChar);
        break;

      case STATE_DAY_SECOND:
        // do some stuff
        evaluateDateInput(inputChar);
        break;

      case STATE_MONTH_FIRST:
        // do some stuff
        evaluateDateInput(inputChar);
        break;

      case STATE_MONTH_SECOND:
        //do some stuff
        evaluateDateInput(inputChar);
        break;

      case STATE_YEAR_FIRST:
        //do some stuff
        evaluateDateInput(inputChar);
        break;

      case STATE_YEAR_SECOND:
        //do some stuff
        evaluateDateInput(inputChar);
        break;

      case STATE_YEAR_THIRD:
        //do some stuff
        evaluateDateInput(inputChar);
        break;

      case STATE_YEAR_FOURTH:
        //do some stuff
        typeChar(inputChar);
        typeChar(' ');

        current_input_state = STATE_HOUR_FIRST;
        state_history[history_index] = current_input_state;
        history_index++;

//        internalGetValue();

        break;

      case STATE_HOUR_FIRST:
        // do some stuff
        if (inputChar > 50 && inputChar <= 57) {
          char[] chars = {CHAR_CONSTANT_CERO, inputChar, CHAR_CONSTANT_COLON};
          insertChars(chars);
          current_input_state = STATE_MINUTE_FIRST;
          state_history[history_index] = current_input_state;
          history_index++;
        }
        else if (inputChar >= 48 && inputChar <= 50) {
          typeChar(inputChar);
          current_input_state = STATE_HOUR_SECOND;
          state_history[history_index] = current_input_state;
          history_index++;
        }

        break;

      case STATE_HOUR_SECOND:
        char firstHour;
        if (format != null && format == FORMAT_SHORTYEAR) {
          firstHour = buffer.charAt(9);
        }
        else {
          firstHour = buffer.charAt(11);
        }

        if (firstHour == 48 || firstHour == 49) {
          char[] chars = {inputChar, CHAR_CONSTANT_COLON};
          insertChars(chars);
          current_input_state = STATE_MINUTE_FIRST;
          state_history[history_index] = current_input_state;
          history_index++;
        }
        else if (firstHour == 50 && (inputChar >= 48 && inputChar <= 52)) {
          char[] chars = {inputChar, CHAR_CONSTANT_COLON};
          insertChars(chars);
          current_input_state = STATE_MINUTE_FIRST;
          state_history[history_index] = current_input_state;
          history_index++;
        }

        break;

      case STATE_MINUTE_FIRST:
        if (format != null && format == FORMAT_SHORTYEAR) {
          firstHour = buffer.charAt(9);
        }
        else {
          firstHour = buffer.charAt(11);
        }
        char secondHour = buffer.charAt(1);

        if ((firstHour == 48 || firstHour == 49) && inputChar <= 53) {
          typeChar(inputChar);
          current_input_state = STATE_MINUTE_SECOND;
          state_history[history_index] = current_input_state;
          history_index++;
        }
        else if (firstHour == 50 && secondHour < 52 && inputChar <= 53) {
          typeChar(inputChar);
          current_input_state = STATE_MINUTE_SECOND;
          state_history[history_index] = current_input_state;
          history_index++;
        }
        else if (firstHour == 50 && secondHour == 52 && inputChar == 48) {
          char[] chars = {inputChar, inputChar};
          insertChars(chars);
          current_input_state = STATE_TIME_END;
          state_history[history_index] = current_input_state;
        }
        else if (inputChar > 53) {
          char[] chars = {CHAR_CONSTANT_CERO, inputChar};
          insertChars(chars);
          current_input_state = STATE_TIME_END;
          state_history[history_index] = current_input_state;
        }

        break;

      case STATE_MINUTE_SECOND:

        char firstMinute;
        if (format != null && format == FORMAT_SHORTYEAR) {
          firstMinute = buffer.charAt(12);
        }
        else {
          firstMinute = buffer.charAt(14);
        }

        if (firstMinute < 54) {
          typeChar(inputChar);
          current_input_state = STATE_TIME_END;
          state_history[history_index] = current_input_state;
        }

//      Date date = (Date)internalGetValue();

        break;

      default :
        // DO NOTHING
        break;

      //signal error
    }
  }

  /**
   * @param day
   * @param month
   * @param year
   */
  private void updateCalendar(short day, short month, short year) {

    calendar.set(Calendar.YEAR, year);
    calendar.set(Calendar.MONTH, month - 1);
    calendar.set(Calendar.DAY_OF_MONTH, day);

    short monthSet = (short) (calendar.get(Calendar.MONTH) + 1);
    short daySet = (short) calendar.get(Calendar.DAY_OF_MONTH);
    short yearSet = (short) calendar.get(Calendar.YEAR);

    // TODO Exception Handling
    if (day != daySet) { // the specified date is not valid
      short correctDay = (short) (day - 1); // correct the day
      updateCalendar(correctDay, month, year); // recursive: one more try
    }

    if (day == daySet) { // painting for the valid date

      StringBuffer sb = new StringBuffer();
      if (daySet > 9) {
        sb.append(daySet);
      }
      else {
        sb.append('0');
        sb.append(daySet);
      }
      sb.append('.');
      if (monthSet > 9) {
        sb.append(monthSet);
      }
      else {
        sb.append('0');
        sb.append(monthSet);
      }
      sb.append('.');
      if (format != null && format == FORMAT_SHORTYEAR) {
        if (yearSet >= 2000) yearSet -= 2000;
        if (yearSet <= 9) sb.append('0');
      }
      sb.append(yearSet);

      if (!buffer.toString().equals(sb.toString())) {
        clear();
        insertChars( sb.toString().toCharArray() );
      }
    }
  }

  /**
   * @param hour
   * @param minute
   */
  private void updateCalendar(short hour, short minute) {

    calendar.set(Calendar.HOUR_OF_DAY, hour);
    calendar.set(Calendar.MINUTE, minute);

    short hourSet = (short) calendar.get(Calendar.HOUR_OF_DAY);
    short minuteSet = (short) calendar.get(Calendar.MINUTE);

    // TODO Exception Handling
    if (hour != hourSet) { // the specified hour is not valid
      short correctHour = (short) (hour - 1); // correct the day
      updateCalendar(correctHour, minute); // recursive: one more try
    }

    if (hour == hourSet) { // painting for the valid date

      StringBuffer sb = new StringBuffer();
      if (hourSet > 9) {
        sb.append(hourSet);
      }
      else {
        sb.append('0');
        sb.append(hourSet);
      }
      sb.append(':');
      if (minuteSet > 9) {
        sb.append(minuteSet);
      }
      else {
        sb.append('0');
        sb.append(minuteSet);
      }

      if (!buffer.toString().equals(sb.toString())) {
        clear();
        insertChars( sb.toString().toCharArray() );
      }
    }
  }

  /**
   * @param day
   * @param month
   * @param year
   * @param hour
   * @param minute
   */
  private void updateCalendar(short day, short month, short year, short hour, short minute) {

    calendar.set(Calendar.YEAR, year);
    calendar.set(Calendar.MONTH, month - 1);
    calendar.set(Calendar.DAY_OF_MONTH, day);
    calendar.set(Calendar.HOUR_OF_DAY, hour);
    calendar.set(Calendar.MINUTE, minute);

    short monthSet = (short) (calendar.get(Calendar.MONTH) + 1);
    short daySet = (short) calendar.get(Calendar.DAY_OF_MONTH);
    short yearSet = (short) calendar.get(Calendar.YEAR);
    short hourSet = (short) calendar.get(Calendar.HOUR_OF_DAY);
    short minuteSet = (short) calendar.get(Calendar.MINUTE);

    // TODO Exception Handling
    if (day != daySet) { // the specified date is not valid
      short correctDay = (short) (day - 1); // correct the day
      updateCalendar(correctDay, month, year); // recursive: one more try
    }

    if (day == daySet) { // painting for the valid date
      StringBuffer sb = new StringBuffer();
      if (daySet > 9) {
        sb.append(daySet);
      }
      else {
        sb.append('0');
        sb.append(daySet);
      }
      sb.append('.');
      if (monthSet > 9) {
        sb.append(monthSet);
      }
      else {
        sb.append('0');
        sb.append(monthSet);
      }
      sb.append('.');
      if (format != null && format == FORMAT_SHORTYEAR) {
        if (yearSet >= 2000) yearSet -= 2000;
        if (yearSet <= 9) sb.append('0');
      }
      sb.append(yearSet);
      sb.append(' ');

      if (hourSet > 9) {
        sb.append(hourSet);
      }
      else {
        sb.append('0');
        sb.append(hourSet);
      }
      sb.append(':');
      if (minuteSet > 9) {
        sb.append(minuteSet);
      }
      else {
        sb.append('0');
        sb.append(minuteSet);
      }

      if (!buffer.toString().equals(sb.toString())) {
        clear();
        insertChars( sb.toString().toCharArray() );
      }
    }
  }

  // ----- CharBufferListener

  public void changed( CharBuffer sender ) {
    textLine.updateValue( (short) 0, (short) 0, StringType.getDefString(), getDisplayValue() );
  }
}
