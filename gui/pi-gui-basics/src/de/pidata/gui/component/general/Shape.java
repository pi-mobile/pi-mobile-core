/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.general;

import de.pidata.gui.component.base.*;
import de.pidata.gui.layout.LayoutInfo;
import de.pidata.gui.layout.Layoutable;
import de.pidata.log.Logger;
import de.pidata.qnames.Key;
import de.pidata.qnames.QName;

import java.util.Hashtable;

public class Shape implements Layoutable {

  public static final QName SHAPE_RECT = ComponentFactory.NAMESPACE.getQName("rect");
  public static final QName SHAPE_LINE = ComponentFactory.NAMESPACE.getQName("line");
  public static final QName SHAPE_ELLIPSE = ComponentFactory.NAMESPACE.getQName("ellipse");
  public static final QName SHAPE_VERT_SIZE = ComponentFactory.NAMESPACE.getQName("vertSize");
  public static final QName SHAPE_HOR_SIZE = ComponentFactory.NAMESPACE.getQName("horSize");

  public static final QName ID_X = ComponentFactory.NAMESPACE.getQName( "x" );
  public static final QName ID_Y = ComponentFactory.NAMESPACE.getQName( "y" );
  public static final QName ID_WIDTH = ComponentFactory.NAMESPACE.getQName( "width" );
  public static final QName ID_HEIGHT = ComponentFactory.NAMESPACE.getQName( "height" );
  public static final QName ID_VISIBLE = ComponentFactory.NAMESPACE.getQName( "visible" );

  protected Key shapeKey;
  protected QName typeID;
  protected ComponentColor color;
  protected ComponentColor selectedColor;
  protected short x, y, width, height;
  protected float scale = 1.0f;
  protected boolean selected = false;
  protected boolean visible = true;

  protected Container layoutParent;
  protected Hashtable parameters;
  protected Hashtable factors;

  /** sizeLimit is used to store the components size (min/max/preferred height/width  */
  private SizeLimit[] sizeLimit = new SizeLimit[2];

  /** position after layout */
  private short[] pos = new short[2];

  /** Here we store the position (cell, span, alignment) in the parent grid  */
  private LayoutInfo[] layoutInfo = new LayoutInfo[2];
  private static final int SIZESHAPE_THICKNESS = 10;

  /**
   * Creates a new Shape. Must call init() afterward to get a working Shape.
   */
  public Shape() {
  }

  /**
   * Used to init Shape after call to empty constructor
   * @param shapeID
   * @param typeID
   * @param color
   * @param selectedColor
   * @param x
   * @param y
   * @param width
   * @param height
   */
  public void init( QName shapeID, QName typeID, ComponentColor color, ComponentColor selectedColor, short x, short y, short width, short height ) {
    this.shapeKey = shapeID;
    this.typeID = typeID;
    this.color = color;
    if (selectedColor != null) this.selectedColor = selectedColor;
    else this.selectedColor = color;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    init();
  }

  public Shape(Key shapeKey, ComponentColor color, ComponentColor selectedColor, String coords) {
    int pos, start;

    this.shapeKey = shapeKey;
    this.color = color;
    if (selectedColor != null) this.selectedColor = selectedColor;
    else this.selectedColor = color;

    try {
      pos = coords.indexOf(' ');
      typeID = ComponentFactory.NAMESPACE.getQName(coords.substring(0,pos));
      start = pos+1;
      pos = coords.indexOf(',');
      this.x = Short.parseShort(coords.substring(start,pos));
      start = pos+1;
      pos = coords.indexOf(',', start);
      this.y = Short.parseShort(coords.substring(start,pos));
      start = pos+1;
      pos = coords.indexOf(',', start);
      this.width  = (short) (Short.parseShort(coords.substring(start,pos)) - x);
      this.height = (short) (Short.parseShort(coords.substring(pos+1)) - y);
    }
    catch (Exception ex) {
      Logger.error("Invalid coordinates: '"+coords+"', shapeKey="+shapeKey, ex);
    }
    init();
  }

  private void init() {
    calcSizes();
    layoutInfo[0] = new LayoutInfo();
    layoutInfo[1] = new LayoutInfo();
  }

  public void setScale( float scale ) {
    this.scale = scale;
    layoutChanged();
  }

  public short getX() {
    return x;
  }

  public short getY() {
    return y;
  }

  public short getWidth() {
    return width;
  }

  public short getHeight() {
    return height;
  }

  public void calcSizes() {
    short x, y, w, h, wMax, hMax;

    if (scale == 1.0f) {
      x = this.x;
      y = this.y;
      w = this.width;
      h = this.height;
    }
    else {
      x = (short) ((float) this.x * scale);
      y = (short) ((float) this.y * scale);
      w = (short) ((float) this.width * scale);
      h = (short) ((float) this.height * scale);
    }

    if (w > 0) {
      w = (short) (x + w);
      wMax = w;
    }
    else {
      if (typeID == SHAPE_VERT_SIZE) {
        short labelWidth = 0;
        if (layoutParent != null) labelWidth = (short) getLayoutParent().getFont().getStringWidth( getLayoutParent(), ""+this.height );
        if (labelWidth > SIZESHAPE_THICKNESS) w = labelWidth;
        else w = SIZESHAPE_THICKNESS;
      }
      else w = (short) (x - w);
      wMax = Short.MAX_VALUE;
    }
    if (h > 0) {
      h = (short) (y + h);
      hMax = h;
    }
    else {
      if (typeID == SHAPE_HOR_SIZE) {
        short labelheight = 0;
        if (layoutParent != null) labelheight = (short) (getLayoutParent().getFont().getHeight( getLayoutParent() ) + 2 );
        if (labelheight > SIZESHAPE_THICKNESS) h = labelheight;
        else h = SIZESHAPE_THICKNESS;
      }
      else h = (short) (y - h);
      hMax = Short.MAX_VALUE;
    }

    sizeLimit[Direction.X.getIndex()] = new SizeLimit( w, w, wMax );
    sizeLimit[Direction.Y.getIndex()] = new SizeLimit( h, h, hMax );
    if (layoutParent != null) {
      layoutParent.getPaintState( Direction.X ).undoPaintState( PaintState.STATE_SIZES_CALC );
      layoutParent.getPaintState( Direction.Y ).undoPaintState( PaintState.STATE_SIZES_CALC );
    }
  }

  public Key getKey() {
    return this.shapeKey;
  }

  public QName getTypeID() {
    return typeID;
  }

  public ComponentColor getColor() {
    return color;
  }

  public boolean isSelected() {
    return selected;
  }

  public void setSelected(boolean selected) {
    this.selected = selected;
  }

  /**
   * Called for painting this shape
   * @param graph the graphics context to use for painting
   */
  public final void doPaint(ComponentGraphics graph) {
    short w, h, x, y;
    String label;
    if (selected) {
      graph.setColor(selectedColor);
    }
    else {
      graph.setColor(color);
    }

    if (scale == 1.0f) {
      x = this.x;
      y = this.y;
      w = this.width;
      h = this.height;
    }
    else {
      x = (short) ((float) this.x * scale);
      y = (short) ((float) this.y * scale);
      w = (short) ((float) this.width * scale);
      h = (short) ((float) this.height * scale);
    }

    if (w <= 0) {
      w = (short) (getSizeLimit( Direction.X ).getCurrent() - x + w);
    }
    if (h <= 0) {
      h = (short) (getSizeLimit( Direction.Y ).getCurrent() - y + h);
    }

    paintShape( graph, this.typeID, x, y, w, h );
  }

  protected void paintShape( ComponentGraphics graph, QName typeID, short x, short y, short w, short h ){
    String label;
    if (typeID == SHAPE_RECT) {
      graph.paintRect(x, y, w, h);
    }
    else if (typeID == SHAPE_LINE) {
      graph.paintLine(x, y, w, h);
    }
    else if (typeID == SHAPE_ELLIPSE) {
      graph.paintEllipse(x, y, w, h);
    }
    else if (typeID == SHAPE_VERT_SIZE) {
      ComponentFont font = getLayoutParent().getFont();
      if (this.height > 0) label = "" + this.height;
      else label = "" + (short) ((float) h / this.scale);
      short xm = (short) (x - 1 + w / 2);
      short x1 = (short) (xm - SIZESHAPE_THICKNESS / 2);
      short x2 = (short) (xm + SIZESHAPE_THICKNESS / 2);
      short y2 = (short) (y - 1 + h);
      short ht = (short) font.getHeight( getLayoutParent() );
      short yl = (short) (y - 1 + (h - ht) / 2);
      graph.paintLine( x1, y, x2, y );
      graph.paintLine( x1, y2, x2, y2 );
      graph.paintLine( xm, y, xm, yl );
      graph.paintString( label, x, yl, color, font );
      graph.paintLine( xm, (short) (yl + ht), xm, y2 );
    }
    else if (typeID == SHAPE_HOR_SIZE) {
      ComponentFont font = getLayoutParent().getFont();
      if (this.width > 0) label = "" + this.width;
      else label = "" + (short) ((float) w / this.scale);
      short ym = (short) (y - 1 + font.getHeight( getLayoutParent() ) / 2);
      short y1 = (short) (y + 1);
      short y2 = (short) (y - 1 + SIZESHAPE_THICKNESS);
      short x2 = (short) (x - 1 + w);
      short wt = (short) font.getStringWidth( getLayoutParent(), label );
      short xl = (short) (x - 1 + (w - wt) / 2);
      short yl = (short) (y - 1 + (h - font.getHeight( getLayoutParent() )) / 2);
      graph.paintLine( x, y1, x, y2 );
      graph.paintLine( x2, y1, x2, y2 );
      graph.paintLine( x, ym, xl, ym );
      graph.paintString( label, xl, yl, color, font );
      graph.paintLine( (short) (xl + wt), ym, x2, ym );
    }
    else {
      throw new IllegalArgumentException("Unsupported type ID="+this.typeID);
    }
  }

  public boolean containsPos(short posX, short posY) {
    short w, h, x, y;
    if (scale == 1.0f) {
      x = this.x;
      y = this.y;
      w = this.width;
      h = this.height;
    }
    else {
      x = (short) ((float) this.x * scale);
      y = (short) ((float) this.y * scale);
      w = (short) ((float) this.width * scale);
      h = (short) ((float) this.height * scale);
    }
    return ((posX >= x) && (posX < (x+w)) && (posY >= y) && (posY < y+h));
  }

  /**
   * Returns size limit for direction
   *
   * @param direction the direction
   * @return this component's size limit in one direction
   */
  public SizeLimit getSizeLimit( Direction direction ) {
    return sizeLimit[ direction.getIndex() ];
  }

  /**
   * Returns grid position and cell span for direction
   *
   * @param direction the direction
   * @return the LayoutInfo containing grid position and cell span for direction
   */
  public LayoutInfo getLayoutInfo( Direction direction ) {
    return layoutInfo[ direction.getIndex() ];
  }

  /**
   * Returns the pixel position for direction relative to layout grid's origin.
   *
   * @param direction the direction
   * @return the layout position and size of this component
   */
  public short getPos( Direction direction ) {
    return pos[ direction.getIndex() ];
  }

  /**
   * Sets pixel positon relative to layout grid's origin for direction.
   * This method is called by layouter.
   * @param direction the direction
   * @param position  the new pixel position
   */
  public void setPos( Direction direction, short position ) {
    pos[ direction.getIndex() ] = position;
  }

  /**
   * Returns the components parent container.
   * The parent container is responsible for layout and position of the component.
   *
   * @return the parent container
   */
  public Container getLayoutParent() {
    return this.layoutParent;
  }

  /**
   * Sets parent container
   *
   * @param parent the parent container
   */
  public void setLayoutParent( Container parent ) {
    this.layoutParent = parent;
  }

  /**
    * Returns true if all parents of this Component are visible. If only
    * one of the parents is invisible false is returned.
    * @return true if all parents are visible
    */
  public boolean isVisible() {
    Container parent = this.getLayoutParent();
    if (parent != null) {
      return (visible && parent.isVisible());
    }
    return false;
  }

  public void layoutChanged() {
    calcSizes();
    Container parent = this.getLayoutParent();
    if (parent != null) {
      parent.contentChanged();
    }
  }

  public void setParamValue( QName id, String source, Double factor, Object compareTo ) {
    if (parameters == null) {
      parameters = new Hashtable();
      factors = new Hashtable();
    }
    parameters.put( source, id );
    if (factor != null) factors.put( source, factor );
    else factors.put( source, compareTo );
  }

  public void parameterUpdated( String source, Object value ) {
    if (parameters != null) {
      QName id = (QName) parameters.get( source );
      if ((id != null) && (value != null) && (((String) value).length() > 0)) {
        if (id == ID_X) {
          Double factor = (Double) factors.get( source );
          x = (short) ((double) (Short.parseShort( (String) value ) * factor.doubleValue()));
          layoutChanged();
        }
        else if (id == ID_Y) {
          Double factor = (Double) factors.get( source );
          y = (short) ((double) (Short.parseShort( (String) value ) * factor.doubleValue()));
          layoutChanged();
        }
        else if (id == ID_WIDTH) {
          Double factor = (Double) factors.get( source );
          width = (short) ((double) (Short.parseShort( (String) value ) * factor.doubleValue()));
          layoutChanged();
        }
        else if (id == ID_HEIGHT) {
          Double factor = (Double) factors.get( source );
          height = (short) ((double) (Short.parseShort( (String) value ) * factor.doubleValue()));
          layoutChanged();
        }
        else if (id == ID_VISIBLE) {
          Object compareTo = factors.get( source );
          if (compareTo == null) {
            visible = true;
          }
          else{
            visible = compareTo.equals( value );
          }
          layoutChanged();
        }
      }
    }
  }
}
