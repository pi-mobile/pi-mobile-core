/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.general;

import de.pidata.gui.component.base.*;
import de.pidata.gui.guidef.*;
import de.pidata.gui.layout.*;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.simple.DecimalObject;
import de.pidata.qnames.QName;

import java.util.Hashtable;

public class SimpleGuiBuilder implements GuiBuilder {

  private static final String ME = "SimpleGuiBuilder";
  private static final Double EINS = new Double( 1.0 );

  private ComponentFactory compFactory = Platform.getInstance().getComponentFactory();
  private Model moduleContainer;
  private Hashtable shapeClasses = new Hashtable();

  public Model getModuleContainer() {
    return moduleContainer;
  }

  public void setModuleContainer( Model moduleContainer ) {
    this.moduleContainer = moduleContainer;
  }

  public void addShapeClass( QName typeID, Class shapeClass ) {
    shapeClasses.put( typeID, shapeClass );
  }

  private short getShort(Short value, int defValue) {
    if (value == null) return (short) defValue;
    else return value.shortValue();
  }

  private short getShort(Integer value, int defValue) {
    if (value == null) return (short) defValue;
    else return value.shortValue();
  }

  private ComponentColor createColor(String colorDef) {
    return compFactory.getPlatform().getColor(colorDef);
  }

  /**
   * Decides whether background names a path to a bitmap or a color definition
   * @param background
   * @return true if background names a color definiton
   */
  private boolean isColorDef(String background) {
    if ((background == null) || (background.length() == 0)) { // no color, transparent
      return true;
    }
    else if (background.equals("none")) { // no color, transparent
      return true;
    }
    else if (background.startsWith("0x")) { // rgb
      return true;
    }
    else if (background.startsWith("file:")) { // absolute bitmap path
      return false;
    }
    else if (background.indexOf("/") >= 0) { // relative bitmap path
      return false;
    }
    else { // color definition string
      return true;
    }
  }

  public Dialog createDialog( DialogDef dialogDef ) {
    PanelType panelDef = dialogDef.getPanelDef();
    Layouter layouterX = null;
    Layouter layouterY = null;
    Layout layout;
    ModelIterator iter;
    Dialog dialog;

    for (iter = panelDef.layoutIter(); iter.hasNext(); ) {
      layout = (Layout) iter.next();
      if (layout.key() == ID_COLS) {
        layouterX = createLayouter(layout, Direction.X);
      }
      else if (layout.key() == ID_ROWS) {
        layouterY = createLayouter(layout, Direction.Y);
      }
    }

    short x = 0;
    short y = 0;
    short width = 0;
    short height = 0;
    /* TODO welchen Sinn hatten x, y, width und height an der Applikation?
    if ((coords != null) && (coords.length() > 0)) {
      Rect rect = calcCoords(coords);
      x = (short) rect.getX();
      y = (short) rect.getY();
      width = (short) rect.getWidth();
      height = (short) rect.getHeight();
    }
    else {
      x = getShort(app.getX(), 0);
      y = getShort(app.getY(), 0);
      width = getShort(app.getWidth(), compFactory.getPlatform().getScreen().getScreenBounds().getWidth());
      height = getShort(app.getHeight(), compFactory.getPlatform().getScreen().getScreenBounds().getHeight());
    }
    */

    String diaCoords = dialogDef.getCoords();
    short diaX,diaY,diaWidth,diaHeight;
    if ((diaCoords != null) && (diaCoords.length() > 0)) {
      Rect rect = calcCoords(diaCoords);
      x = (short) rect.getX();
      y = (short) rect.getY();
      width = (short) rect.getWidth();
      height = (short) rect.getHeight();
    }
    else {
      diaX = getShort(dialogDef.getX(), 0);
      if (diaX > 0) x = diaX;
      diaY = getShort(dialogDef.getY(), 0);
      if (diaY > 0) y = diaY;
      diaWidth = getShort(dialogDef.getWidth(), 0);
      if (diaWidth > 0) width = diaWidth;
      diaHeight = getShort(dialogDef.getHeight(), 0);
      if (diaHeight > 0) height = diaHeight;
    }

    dialog = compFactory.createDialog(layouterX, layouterY, x, y, width, height);

    Border border = createBorder(panelDef.getBorderDef());
    if (dialog != null) {
      dialog.setBorder(border);
    }
    initContainerBackground(dialog, panelDef.getBackground());

    addComps(dialog, panelDef, dialog);
    return dialog;
  }

  private void addComps( Container container, Model containerDef, Dialog dialog) {
    ModelIterator iter;
    Model child;
    Rect gridPos = new Rect(0,0,1,1);
    for (iter = containerDef.iterator(null, null); iter.hasNext(); ) {
      child = iter.next();
      if (child instanceof DrawDef) {
        Component childComp = createDrawComp( gridPos, (DrawDef) child );
        dialog.addComponent( ((DrawDef) child).getID(), childComp );
        container.add( childComp );
      }
      else if (child instanceof PanelType) {
        container.add(createPanel(gridPos, (PanelType) child, dialog, null, null));
      }
      else if (child instanceof PanelModule) {
        container.add(createPanelModule(gridPos, (PanelModule) child, dialog));
      }
      else if (child instanceof TableDef) {
        container.add(createTable(gridPos, (TableDef) child, dialog));
      }
      else if (child instanceof CardsDef) {
        container.add(createCards(gridPos, (CardsDef) child, dialog));
      }
      else if (child instanceof CompDef) {
        container.add(createComp(gridPos, (CompDef) child, dialog));
      }
      else if (child instanceof BorderDef) {
        //do nothing - already processed todo oder nicht?
      }
      else if (child instanceof Layout) {
        //do nothing - already processed
      }
      else {
        Logger.warn(ME + ".addComps(...): unknown component type " + child.type().name());
      }
    }
  }

  public PaintComp createDrawComp( Rect gridPos, DrawDef drawDef ) {
    ModelIterator iter;
    ShapeDef shapeDef;
    String background = drawDef.getBackground();
    short width = getShort( drawDef.getWidth(), 0 );
    short height = getShort( drawDef.getHeight(), 0);
    PaintComp comp;
    Layouter layouterX = null;
    Layouter layouterY = null;

    for (iter = drawDef.layoutIter(); iter.hasNext(); ) {
      Layout layout = (Layout) iter.next();
      if (layout.key() == ID_COLS) {
        layouterX = createLayouter(layout, Direction.X);
        layouterX.setGap( (short) 0 );
      }
      else if (layout.key() == ID_ROWS) {
        layouterY = createLayouter(layout, Direction.Y);
        layouterY.setGap( (short) 0 );
      }
    }
    float scale;
    DecimalObject scaleObj = drawDef.getScale();
    if (scaleObj == null) scale = 1.0f;
    else scale = scaleObj.floatValue();

    if (isColorDef(background)) {
      comp = new PaintComp( this, layouterX, layouterY, width, height, createColor(background), scale );
      comp.setID(drawDef.getID());
    }
    else {
      ComponentBitmap bitmap = compFactory.getPlatform().getBitmap( GuiBuilder.NAMESPACE.getQName( background ) );
      comp = new PaintComp( this, layouterX, layouterY, width, height, bitmap, scale );
      comp.setID(drawDef.getID());
    }
    setLayoutPos(comp, drawDef.getCol(), drawDef.getRow(), drawDef.getColSpan(), drawDef.getRowSpan(), gridPos);

    setAlignment(comp, Direction.X, drawDef.getAlignX());
    setAlignment(comp, Direction.Y, drawDef.getAlignY());

    Rect shapeGridPos = new Rect();
    for (iter = drawDef.shapeDefIter(); iter.hasNext(); ) {
      shapeDef = (ShapeDef) iter.next();
      comp.addShape( createShape(shapeGridPos, shapeDef) );
    }
    return comp;
  }

  public Shape createShape(Rect gridPos, ShapeDef shapeDef) {
    Shape shape;
    short x,y,width,height;
    String coords;
    ComponentColor color = null;
    ComponentColor selColor = null;

    QName temp = shapeDef.getColor();
    if (temp != null) color = compFactory.getPlatform().getColor( temp );
    else color = compFactory.getPlatform().getColor( ComponentColor.BLACK );
    temp = shapeDef.getSelectedColor();
    if (temp != null) selColor = compFactory.getPlatform().getColor( temp );
    else selColor = compFactory.getPlatform().getColor( ComponentColor.RED );
    coords = shapeDef.getCoords();

    if ((coords != null) && (coords.length() > 0)) {
      shape = new Shape(shapeDef.getID(), color, selColor, coords);
    }
    else {
      x = getShort( shapeDef.getX(), 0 );
      y = getShort( shapeDef.getY(), 0 );
      width =  getShort( shapeDef.getWidth(), 0 );
      height = getShort( shapeDef.getHeight(), 0 );
      QName typeID = shapeDef.getTypeID();
      if (typeID.getNamespace() == NAMESPACE) {
        shape = new Shape();
      }
      else {
        try {
          shape = (Shape) ((Class) shapeClasses.get( typeID )).newInstance();
        }
        catch (Exception ex) {
          Logger.error( "Could not inistantiate shape class, typeID="+typeID, ex );
          shape = new Shape();
          typeID = Shape.SHAPE_RECT;
          color = compFactory.getPlatform().getColor( ComponentColor.RED );
        }
      }
      shape.init( shapeDef.getID(), shapeDef.getTypeID(), color, selColor, x, y, width, height );
    }

    setLayoutPos(shape, shapeDef.getCol(), shapeDef.getRow(), shapeDef.getColSpan(), shapeDef.getRowSpan(), gridPos);
    setShapeAlignment( shape, Direction.X, shapeDef.getAlignX(), shape.getWidth() );
    setShapeAlignment( shape, Direction.Y, shapeDef.getAlignY(), shape.getHeight() );

    for (ModelIterator paramIter = shapeDef.paramIter(); paramIter.hasNext(); ) {
      KeyAndValue param = (KeyAndValue) paramIter.next();
      String value = param.getValue();
      String compareTo = null;
      Double factor = EINS;
      int pos = value.indexOf( '/' );
      if (pos > 0) {
        factor = new Double( 1.0 / Double.valueOf( value.substring( pos+1 ) ).doubleValue() );
        value = value.substring( 0, pos );
      }
      else {
        pos = value.indexOf( '*' );
        if (pos > 0) {
          factor = new Double( Double.valueOf( value.substring( pos+1 )).doubleValue() );
          value = value.substring( 0, pos );
        }
        else {
          pos = value.indexOf( '=' );
          if (pos > 0) {
            compareTo = value.substring( pos+1 );
            value = value.substring( 0, pos );
            factor = null;
          }
        }
      }
      shape.setParamValue( param.getID(), value, factor, compareTo );
    }

    return shape;
  }

  /**
   * Calculate x, y, width, height from coordinate string
   * @param coords commaseparated coordinate string
   * @return rectangle that represents the coordinates
   */
  private Rect calcCoords(String coords) {
    Rect rect = new Rect(0, 0, 0, 0);
    int pos, start;

    pos = coords.indexOf(',');
    rect.setX(Short.parseShort(coords.substring(0,pos)));
    start = pos+1;
    pos = coords.indexOf(',', start);
    rect.setY(Short.parseShort(coords.substring(start,pos)));
    start = pos+1;
    pos = coords.indexOf(',', start);
    rect.setWidth((short) (Short.parseShort(coords.substring(start,pos)) - rect.getX())); //todo: warum höhe/breite um offset kürzen?
    rect.setHeight((short) (Short.parseShort(coords.substring(pos+1)) - rect.getY()));

    return rect;
  }

  private Container createTable (Rect gridPos, TableDef tableDef, Dialog dialog) {
    Layout layout;
    Layouter layouterX = null;
    Layouter layouterX2 = null;
    ModelIterator iter;
    Container header;
    Container body;
    Container edit = null;
    Container table;
    PanelType editDef;

    for (iter = tableDef.layoutIter(); iter.hasNext(); ) {
      layout = (Layout) iter.next();
      if (layout.key() == ID_COLS) {
        layouterX = createLayouter(layout, Direction.X);
        //TODO ohne eigenen Layouter für den Body klappts nicht mit den PaintStates
        layouterX2 = createLayouter(layout, Direction.X);
      }
    }

    header = createPanel(new Rect(0,0,1,1), tableDef.getHeader(), dialog, layouterX, null);
    body = createPanel(new Rect(0,0,1,1), tableDef.getBody(), dialog, layouterX2, null);
    editDef = tableDef.getEdit();
    if (editDef != null) {
      edit = createPanel(new Rect(0,0,1,1), editDef, dialog, null, null);
      // auto layouter has set growing layout positions for edit comps -> place all edit comps at 0,0
      for (int i = 0; i < edit.childCount(); i++) {
        Layoutable layoutable = edit.getChild( i );
        layoutable.getLayoutInfo(Direction.X).setGrid( (byte) 0, (byte) 1 );
        layoutable.getLayoutInfo(Direction.Y).setGrid( (byte) 0, (byte) 1 );
      }
    }

//    hasScroller = tableDef.getScroller(); todo scroller an binding definierbar?
//    scrollHor = (hasScroller == ID_SCROLLBOTH || hasScroller == ID_SCROLLHOR);
//    scrollVert = (hasScroller == ID_SCROLLBOTH || hasScroller == ID_SCROLLVERT);

    short visRows = getShort( tableDef.getRowsVisible(), Short.MAX_VALUE );

    table = compFactory.createTable(layouterX, header, body, edit, visRows);
    table.setID(tableDef.getID());
    setLayoutPos(table, tableDef.getCol(), tableDef.getRow(), tableDef.getColSpan(), tableDef.getRowSpan(), gridPos);

    initContainerBackground(table, tableDef.getBackground());

    dialog.addComponent( tableDef.getID(), table );
    return table;
  }

  private void initContainerBackground (Container container, String colorStr) {
    ComponentColor background = createColor(colorStr);
    if (background != null) {
      container.setBackground(background);
    }
  }

  private Border createBorder(BorderDef borderDef) {
    if (borderDef == null) return null;

    QName typeID = borderDef.getType();
    short top, left, right, bottom;
    Integer temp;
    temp = borderDef.getTop();
    if (temp == null) top = -1; else top = temp.shortValue();
    temp = borderDef.getLeft();
    if (temp == null) left = -1; else left = temp.shortValue();
    temp = borderDef.getRight();
    if (temp == null) right = -1; else right = temp.shortValue();
    temp = borderDef.getBottom();
    if (temp == null) bottom = -1; else bottom = temp.shortValue();
    return compFactory.createBorder(typeID, borderDef.getValue(), top, left, right, bottom);
  }

  private void setAlignment(Layoutable layoutable, Direction direction, QName align) {
    if (align != null) {
      LayoutInfo layoutInfo = layoutable.getLayoutInfo( direction );
      if (align == ID_ALIGNLEFT || align == ID_ALIGNTOP) layoutInfo.setAlignment( LayoutInfo.ALIGN_MIN );
      else if (align == ID_ALIGNRIGHT || align == ID_ALIGNBOTTOM) layoutInfo.setAlignment( LayoutInfo.ALIGN_MAX );
      else if (align == ID_ALIGNCENTER) layoutInfo.setAlignment( LayoutInfo.ALIGN_CENTER );
      else if (align == ID_ALIGNGROW) layoutInfo.setAlignment( LayoutInfo.ALIGN_GROW );
      else throw new RuntimeException("unknown alignment '" + align.getName());
    }
  }

  private void setShapeAlignment( Layoutable layoutable, Direction direction, QName align, short size ) {
    if (align == null) {
      if (size <= 0) {
        layoutable.getLayoutInfo( direction ).setAlignment( LayoutInfo.ALIGN_GROW );
      }
    }
    else {
      setAlignment( layoutable, Direction.X, align );
    }
  }

  private void setLayoutPos(Layoutable layoutable, Integer col, Integer row, Integer colSpan, Integer rowSpan, Rect gridPos) {
    byte x, y, dx, dy;
    // row not defined: use same row than last component
    if (row == null) {
      y = (byte) gridPos.getY();
    }
    else {
      y = row.byteValue();
      // row < 0: use current row + abs(row)
      if (y < 0) {
        y = (byte) (gridPos.getY() - y);
        gridPos.setX(0);
        gridPos.setY(y);
      }
    }
    // col not defined: use column right of last component
    if (col == null) {
      x = (byte) gridPos.getX();
    }
    else {
      x = col.byteValue();
    }
    if (colSpan == null) dx = 1;
    else dx = colSpan.byteValue();
    if (rowSpan == null) dy = 1;
    else dy = rowSpan.byteValue();

    layoutable.getLayoutInfo(Direction.X).setGrid(x, dx);
    layoutable.getLayoutInfo(Direction.Y).setGrid(y, dy);

    gridPos.setX(x + dx);
  }

  public Component createComp(Rect gridPos, CompDef compDef, Dialog dialog) {
    QName typeID = compDef.getTypeID();
    int charsVisible = 0;
    Integer chVis = compDef.getCharsVisible();
    if (chVis != null) charsVisible = chVis.intValue();
    int rowsVisible = 0;
    Integer rVis = compDef.getRowsVisible();
    if (rVis != null) rowsVisible = rVis.intValue();
    Component comp = compFactory.createComponent(typeID, compDef.getCaption(), compDef.getFormat(), charsVisible, rowsVisible);
    comp.setID(compDef.getID());
    setLayoutPos(comp, compDef.getCol(), compDef.getRow(), compDef.getColSpan(), compDef.getRowSpan(), gridPos);

    setAlignment(comp, Direction.X, compDef.getAlignX());
    setAlignment(comp, Direction.Y, compDef.getAlignY());

    comp.setBackground(createColor(compDef.getBackground()));   //todo: default color bzw. von parent holen

    dialog.addComponent( compDef.getID(), comp );
    return comp;
  }

  public Container createPanelModule(Rect gridPos, PanelModule panelModule, Dialog dialog) {
    QName moduleID = panelModule.getModule();
    de.pidata.gui.guidef.Module moduleDef = null;
    if (this.moduleContainer != null) {
      moduleDef = (de.pidata.gui.guidef.Module) this.moduleContainer.get(Application.ID_MODULE, moduleID);
    }  
    if (moduleDef == null) {
      throw new IllegalArgumentException("Unknown module ID="+moduleID);
    }

    PanelType panelDef = moduleDef.getPanelDef(); //todo: was ist mit panel id im module gemeint, wenn nur 1 panel zulässig ist?
    if (panelDef == null) { //todo: module nur mit controller zulassen?
      throw new IllegalArgumentException("No panel defined in module ID="+moduleID);
    }

    //panelDef.setID(panelModule.getID()); //todo: com.daimler.sysland.clone, mehr ersetzen
    return createPanel(gridPos, panelDef, dialog, null, null);
  }

  public Container createPanel(Rect gridPos, PanelType panelDef, Dialog dialog, Layouter layouterX, Layouter layouterY) {
    Layout layout;
    ModelIterator iter;
    Container panel;
    QName hasScroller;
    boolean scrollHor;
    boolean scrollVert;
    Border border;

    for (iter = panelDef.layoutIter(); iter.hasNext(); ) {
      layout = (Layout) iter.next();
      if (layout.key() == ID_COLS) {
        layouterX = createLayouter(layout, Direction.X);
      }
      else if (layout.key() == ID_ROWS) {
        layouterY = createLayouter(layout, Direction.Y);
      }
    }

    hasScroller = panelDef.getScroller();
    scrollHor = (hasScroller == ID_SCROLLBOTH || hasScroller == ID_SCROLLHOR);
    scrollVert = (hasScroller == ID_SCROLLBOTH || hasScroller == ID_SCROLLVERT);

    panel = compFactory.createPanel( layouterX, layouterY, scrollHor, scrollVert, (short) 1 );
    panel.setID(panelDef.getID());
    setLayoutPos(panel, panelDef.getCol(), panelDef.getRow(), panelDef.getColSpan(), panelDef.getRowSpan(), gridPos);

    border = createBorder(panelDef.getBorderDef());
    if (border != null) {
      ((Panel) panel).setBorder(border);
    }

    initContainerBackground(panel, panelDef.getBackground());

    if (panelDef.getID() != null) {
      dialog.addComponent( panelDef.getID(), panel );
    }
    addComps(panel, panelDef, dialog);
    return panel;
  }

  private Component createCards(Rect gridPos, CardsDef cardsDef, Dialog dialog) {
    Container tabbedPane;

    tabbedPane = compFactory.createTabbedPane();
    tabbedPane.setID(cardsDef.getID());
    setLayoutPos(tabbedPane, cardsDef.getCol(), cardsDef.getRow(), cardsDef.getColSpan(), cardsDef.getRowSpan(), gridPos);

    initContainerBackground(tabbedPane, cardsDef.getBackground());

    dialog.addComponent( cardsDef.getID(), tabbedPane );
    addComps(tabbedPane, cardsDef, dialog);

    return tabbedPane;
  }

  private Layouter createLayouter(Layout layout, Direction direction) {
    QName type = layout.getTypeID();
    if (type == ID_GRIDLAYOUTER) {
      //TODO
      throw new RuntimeException("TODO gridlayouter");
    }
    else if (type == ID_STACKLAYOUTER) {
      return new StackLayouter(direction);
    }
    else if (type == ID_FLOWLAYOUTER) {
      return new FlowLayouter(direction);
    }
    else {
      throw new IllegalArgumentException("Unknown layout type: "+type);
    }
  }


}
