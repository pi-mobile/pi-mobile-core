/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.general;

import de.pidata.gui.component.base.Screen;
import de.pidata.gui.event.InputManager;
import de.pidata.qnames.QName;

/**
 * erfasst alle Mausevents
 * event ist einem Status zugeordnet
 * Status OVER wird direkt im InputManager behandelt; die Komponente initiiert lediglich ein repaint.
 * alle anderen stati werden an die Komponente weitergegeben <code>processMouseEvent</code>.
 * Komponente entscheidet über Aktion: painting wird von der Komponente erledigt;
 * gibt bei SELECTED bei Bedarf Info an Controller. Dort wird Aktion ausgeführt.
 *
 * ??? processMouseEvent ruft select; select ruft selected?
 *
 * erfasst alle Tastaturevents
 *
 * Falls Popup aktiv ist: wird geschlossen, falls eine Komponente ausserhalb des
 * Popup den Status SELECTED erhält.
 *
 * Falls Scroller mit Status DRAGGING aktiv ist: solange event = DRAGGING,
 * erhält generell der Scroller den event. Sobald anderer Status kommt, wird
 * der Srcoller inaktiv.
 */
public abstract class AbstractInputManager implements InputManager {

  private Screen screen;

  protected AbstractInputManager( Screen screen ) {
    this.screen = screen;
  }

  public Screen getScreen() {
    return screen;
  }

  /**
   * Get the character from input code
   * @param keyCode
   * @return the character
   */
  protected char getKeyChar(int keyCode) {
    if ((keyCode >= 0x20) && (keyCode <= 0xFF)) {
      return (char) keyCode;
    }
    else {
      return 0;
    }
  }

  //if (keyChar >= ' ') { //TODO ONLY CHARACTERS - so OK für MIDP
  public abstract boolean isLetter( char keyChar);

  /**
   * Deliver the command to the processing controller and switch focus to the next component if necessary.
   * @param cmd the command, one of the constants InputManager.CMD_*
   * @param keyChar character from input
   * @param index position in the text buffer
   */
  protected abstract void sendEvent( QName cmd, char keyChar, int index);

}
