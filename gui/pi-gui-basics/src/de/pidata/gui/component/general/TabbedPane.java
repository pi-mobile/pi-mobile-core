/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.general;

import de.pidata.gui.component.base.*;
import de.pidata.gui.event.ComponentListener;
import de.pidata.gui.event.InputManager;
import de.pidata.gui.layout.FlowLayouter;
import de.pidata.gui.layout.Layoutable;
import de.pidata.gui.layout.StackLayouter;
import de.pidata.qnames.QName;

import java.util.Vector;

public class TabbedPane extends Panel implements ComponentListener {

  private Vector containerList = new Vector();
  private int currentTab = -1;
  private short tabsHeight = 0;
  private Border contentBorder;

  public TabbedPane(Border contentBorder) {
    super( new FlowLayouter(Direction.X), new StackLayouter(Direction.Y), false, false, (short) 1 );
    this.contentBorder = contentBorder;
    this.getLayouter(Direction.X).setGap((short) 0); //todo: konfigurierbar
    this.getLayouter(Direction.Y).setGap((short) 0);
    super.setBorder(null);
  }

  /**
   * add any Component to the list of components
   *
   * @param layoutable
   * @return index of component in list of components
   */
  public int add( Layoutable layoutable ) {
    if (!(layoutable instanceof Container)) {
      throw new IllegalArgumentException( "Layoutable must be a container, but is: "+layoutable );
    }
    Container panel = (Container) layoutable;
    QName compID = panel.getID();
    int compIndex = this.containerList.size();
    this.containerList.addElement( panel );
    panel.setLayoutParent(this);

    Component tabButton = Platform.getInstance().getComponentFactory().createTabbedButton(compID.getName());
    byte index = (byte) childCount();
    tabButton.setID( ComponentFactory.NAMESPACE.getQName( ""+index ) );
    tabButton.getLayoutInfo(Direction.X).setGrid(index, (byte) 1);
    tabButton.getLayoutInfo(Direction.Y).setGrid((byte) 0, (byte) 1);
    tabButton.setBackground( panel.getBackground() );
    super.add(tabButton);
    tabButton.setComponentListener(this );

    if (this.currentTab < 0) {
      selectTab( 0 );
      tabButton.getComponentState().setInitialState(ComponentState.STATE_PRESSED);
      panel.setVisible( true );
    }
    else {
      panel.setVisible( false );
    }
    if (listener != null) {
      listener.setVisibleRange( getID(), (short) 0, (short) 0, (short) childCount(), (short) 0 );
    }
    return compIndex;
  }

  public void removeAll () { // todo...
    super.removeAll();
  }

  public void remove( Layoutable layoutable ) {
    int tabIndex = containerList.indexOf( layoutable );
    layoutable.setLayoutParent(null);
    this.containerList.removeElement( layoutable );
    super.removeAt(tabIndex);
  }

  public void removeAt(int index) {
    Component comp = (Component) containerList.elementAt(index);
    comp.setLayoutParent(null);
    containerList.removeElementAt(index);
    super.removeAt(index);
  }

  /**
   * Sets a selection listener for this component. SelectionID may be used as identifier
   * if listener wants to listen on multiple components. Listener is called whenever
   * setSelected is called on this component.
   *
   * @param listener     the selection listener
   */
  public void setComponentListener( ComponentListener listener ) {
    super.setComponentListener( listener );
    if (listener != null) {
      listener.setVisibleRange( getID(), (short) 0, (short) 0, (short) childCount(), (short) 0 );
    }
  }

  /**
   * Invoked when component is selected/deselected
   *
   * @param componentID
   * @param posX
   * @param posY
   * @param selected    true if source was selected, false if source was deselected
   */
  public void selected( QName componentID, short posX, short posY, boolean selected ) {
    if (selected) {
      int index = Integer.parseInt( componentID.getName() );
      if (index >= 0) selectTab(index);
    }
  }

  private int indexOf( QName selectionKey ) {
    for (int i = 0; i < this.containerList.size(); i++) {
      Component pane = (Component) containerList.elementAt( i );
      if (pane.getID() == selectionKey) {
        return i;
      }
    }
    return -1;
  }

  /**
   * Called by Component whenever a input command arrives.
   *
   * @param compID
   * @param cmd       the command, one of the constants InputManager.CMD_*
   * @param inputChar optional input character for the command
   * @param index     optional index parameter for the command @return true if the command has been processed, otherwise false
   */
  public boolean command( QName compID, QName cmd, char inputChar, int index ) {
    if (cmd == InputManager.CMD_LEFT ) {
      if (currentTab > 0) {
        selectTab( currentTab - 1 );
      }
      return true;
    }
    else if (cmd == InputManager.CMD_RIGHT ) {
      if (currentTab < (containerList.size() - 1)) {
        selectTab( currentTab + 1 );
      }
      return true;
    }
    else if (cmd == InputManager.CMD_HOME ) {
      selectTab( 0 );
      return true;
    }
    else if (cmd == InputManager.CMD_END ) {
      selectTab( containerList.size() - 1 );
      return true;
    }
    return false;
  }

  /**
   * Called by Component whenever it gains or looses input focus.
   *
   * @param compID
   *@param hasFocus true if component gained input focus @return if false focus switch is not allowed by ComponentListener
   */
  public void onFocusChanged( QName compID, boolean hasFocus ) {
    // do nothing
  }

  /**
   * This method is called when this controller's dialog is closing.
   * The controller has to realease all its resources, e.g. remove
   * itself as istener.
   */
  public void closing() {
    // do nothing
  }

  public void setVisibleRange( QName componentID, short minX, short minY, short maxX, short maxY ) {
    // do nothing
  }

  public void selectTab(int index) {
    this.currentTab = index;
    for (int i = 0; i<childCount(); i++) {
      if (i == index) {
        ((Component) this.containerList.elementAt(i)).setVisible( true );
        ((Component) getChild(i)).getComponentState().setSelected( true );
      }
      if (i!=index) {
        ((Component) getChild(i)).getComponentState().setSelected( false );
        ((Component) this.containerList.elementAt(i)).setVisible( false );
      }
    }
    repaint();
    ((Component) this.containerList.elementAt(this.currentTab)).repaint();
    if (listener != null) {
      listener.selected( getID(), (short) this.currentTab, (short) 0 , true );
    }
  }

  public Component getCurrentPage() {
    if (this.currentTab >= 0) {
      return (Component) this.containerList.elementAt(this.currentTab);
    }
    else {
      return null;
    }
  }

  /**
   * Calculates this component's min, max and pref sizes in one direction and
   * stores them in sizeLimit
   *
   * @param direction
   * @param sizeLimit
   */
  public void calcSizes(Direction direction, SizeLimit sizeLimit) {
    int minSize = 0;
    int prefSize = 0;
    int maxSize = 0;
    int inset = 0;
    int size;
    Component page;
    SizeLimit childSize;

    // calculate size of tabs
    super.calcSizes(direction, sizeLimit);
    minSize = sizeLimit.getMin();
    prefSize = sizeLimit.getPref();
    maxSize = sizeLimit.getMax();

    if (contentBorder != null) {
      if (direction == Direction.X) {
        inset = contentBorder.get2BorderSize(direction);
      }
      else {
        inset = contentBorder.getBottom();
      }
    }

    // calculate pages
    for (int i = containerList.size()-1; i >= 0; i--) {
      page = (Component) containerList.elementAt(i);
      childSize = page.getSizeLimit(direction);
      size = childSize.getMin() + inset;
      if (size > minSize) minSize = size;
      size = childSize.getPref() + inset;
      if (size > prefSize) prefSize = size;
      size = childSize.getMax() + inset;
      if (size > maxSize) maxSize = size;
    }

    if (direction == Direction.Y) {
      this.tabsHeight = getChild( 0 ).getSizeLimit( direction ).getPref();
      minSize  += this.tabsHeight;
      prefSize += this.tabsHeight;
      maxSize  += this.tabsHeight;
    }

    sizeLimit.set(minSize, prefSize, maxSize);
    getPaintState(direction).doPaintState(PaintState.STATE_SIZES_CALC);
  }

  /**
   * Calculate the position of the children.
   * This method is empty for normal components, but needs implementaion
   * for containers.
   */
  public void doLayout(Direction direction) {
    short size = getSizeLimit(direction).getCurrent();
    int inset = 0;
    short pos;
    Component page;
    PaintState paintState;
    SizeLimit panelSizeLimit;

    paintState = getPaintState(direction);
    if (!paintState.hasPaintStateReached(PaintState.STATE_BOUNDS_SET)) {
      return;
    }

    if (direction == Direction.Y) {
      getLayouter(direction).doLayout((short) 0, this.tabsHeight);
      for (int i = containerList.size()-1; i >= 0; i--) {
        page = (Component) containerList.elementAt(i);
        page.setPos(direction, this.tabsHeight);
        if (contentBorder != null) {
          inset = tabsHeight + contentBorder.getBottom();
        }
        else {
          inset = tabsHeight;
        }
        page.getSizeLimit(direction).setCurrentFit((short) (size - inset ));
        page.doLayout(direction);
      }
    }
    else {
      getLayouter(direction).doLayout((short) 0, size);
      for (int i = containerList.size()-1; i >= 0; i--) {
        page = (Component) containerList.elementAt(i);
        pos = 0;
        if (contentBorder != null) {
          pos = contentBorder.getLeft();
          inset = contentBorder.get2BorderSize(direction);
        }
        page.setPos(direction, pos);
        page.getSizeLimit(direction).setCurrentFit((short) (size - inset));
        page.doLayout(direction);
      }
    }
    paintState.doPaintState(PaintState.STATE_LAYOUTED);
  }

  /**
   * Called for painting this component
   *
   * @param compGraph the graphics context to use for painting
   */
  public void doPaintImpl(ComponentGraphics compGraph) {
    short compX;
    short compY;
    short width, borderWidth;
    short height, borderHeight;
    Border border;

    if (!allowPaint()) {
      return;
    }
    
    super.doPaintImpl(compGraph);

    Component child = getCurrentPage();
    if (child != null) {
      Rect oldClip = new Rect();

      compX = child.getPos(Direction.X);
      compY = child.getPos(Direction.Y);
      width = child.getSizeLimit(Direction.X).getCurrent();
      height = child.getSizeLimit(Direction.Y).getCurrent();

      compGraph.translate((short) 0, compY);
      if (contentBorder != null) {
        borderWidth = getSizeLimit(Direction.X).getCurrent();
        borderHeight = (short) (getSizeLimit(Direction.Y).getCurrent() - this.tabsHeight);
        border = getBorder();
        if (border != null) {
          borderWidth -= border.get2BorderSize(Direction.X);
          borderHeight -= border.get2BorderSize(Direction.Y);
        }
        contentBorder.doPaint(compGraph, borderWidth, borderHeight);
      }
      compGraph.translate(compX, (short) 0);
      compGraph.getClip(oldClip);

      compGraph.clipRect((short) 0, (short) 0, width, height);
      child.doPaint(compGraph);

      compGraph.setClip(oldClip);
      compGraph.translate((short) -compX, (short) -compY);
    }
  }

  public Component findComponentAt(Position pos) {
    Component comp = super.findComponentAt(pos);
    Component page;
    if (comp == this) {
      page = getCurrentPage();
      if (containsPos( page, Direction.X, pos.getX() ) && containsPos( page, Direction.Y, pos.getY() ) ) {
        pos.translate(page.getPos(Direction.X), page.getPos(Direction.Y));
        if (page instanceof Container) {
          comp = ((Container) page).findComponentAt (pos);
        }
      }
    }
    return comp;
  }
}
