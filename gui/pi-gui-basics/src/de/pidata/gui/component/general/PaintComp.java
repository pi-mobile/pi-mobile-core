/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.component.general;

import de.pidata.gui.component.base.*;
import de.pidata.gui.event.InputManager;
import de.pidata.gui.guidef.CompDef;
import de.pidata.gui.guidef.DrawDef;
import de.pidata.gui.guidef.ShapeDef;
import de.pidata.gui.layout.Layoutable;
import de.pidata.gui.layout.Layouter;
import de.pidata.log.Logger;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.simple.DecimalObject;

import java.util.Vector;

public class PaintComp extends Panel {

  private SimpleGuiBuilder guiBuilder;
  private short width, height;
  private float scale;
  private ComponentBitmap backgroundImage;
  private short lastX = -1;
  private short lastY = -1;
  private short selectedIndex = -1;
  private boolean drawEnabled;

  private Vector paramKeys = new Vector();
  private Vector paramValues = new Vector();

  public PaintComp( SimpleGuiBuilder guiBuilder, Layouter layouterX, Layouter layouterY, short width, short height, ComponentBitmap background, float scale ) {
    super( layouterX, layouterY, false, false, (short) 0 );
    this.guiBuilder = guiBuilder;
    this.width = width;
    this.height = height;
    this.backgroundImage = background;
    this.scale = scale;
  }

  public PaintComp( SimpleGuiBuilder guiBuilder, Layouter layouterX, Layouter layouterY, short width, short height, ComponentColor backgroundColor, float scale ) {
    super( layouterX, layouterY, false, false, (short) 0 );
    this.guiBuilder = guiBuilder;
    this.width = width;
    this.height = height;
    setBackground(backgroundColor);
    this.scale = scale;
  }

  public void setDrawEnabled( boolean drawEnabled ) {
    this.drawEnabled = drawEnabled;
  }

  /**
   * Replaces the Shape value. posX and posY are ignored
   *
   * @param posX  ignored
   * @param posY  ignored
   * @param valueType
   * @param value the Shape to be replaced
   */
  public void updateValue( short posX, short posY, Type valueType, Object value ) {
    if (posY == -1) {
      paramKeys.removeAllElements();
      paramValues.removeAllElements();
    }
    else if ((value == null) && (posX == 0)) {
      paramKeys.removeElementAt( posY );
      paramValues.removeElementAt( posY );
      repaint();
    }
    else {
      if (posY >= 0) {
        if (posX == 0) {
          if (posY >= paramKeys.size()) paramKeys.setSize( posY+1 );
          paramKeys.setElementAt( (String) value, posY );
        }
        else if (posX == 1) {
          if (posY >= paramValues.size()) paramValues.setSize( posY+1 );
          paramValues.setElementAt( value, posY );
          parameterUpdated( (String) paramKeys.elementAt(posY), value );
        }
      }
    }
  }

  /**
   * Adds the Shape value. posX and posY are ignored
   *
   * @param posX  ignored
   * @param posY  ignored
   * @param valueType
   * @param value the Shape to be added
   */
  public void insertValue( short posX, short posY, SimpleType valueType, Object value ) {
    if (posX == 0) {
      if (posY != paramKeys.size()) {
        throw new IllegalArgumentException( "Insert only allowed at end of list, paramKeys size="+paramKeys.size()+", posY="+posY );
      }
      paramKeys.addElement( (String) value );
    }
    else if (posX == 1) {
      if (posY != paramValues.size()) {
        throw new IllegalArgumentException( "Insert only allowed at end of list, paramValues size="+paramValues.size()+", posY="+posY );
      }
      paramValues.addElement( value );
      parameterUpdated( (String) paramKeys.elementAt(posY), value );
    }
  }

  private Shape getShape( short index ) {
    return (Shape) getChild( index );
  }

  private Shape shapeFromString( String value ) {
    if ((value == null) || (value.length() <= 1)) {
      return null;
    }
    else {
      Platform platform =  Platform.getInstance();
      ComponentColor color = Platform.getInstance().getColor(ComponentColor.GRAY);
      ComponentColor selectedColor = Platform.getInstance().getColor(ComponentColor.RED);
      Shape shape = new Shape( null, color, selectedColor, (String) value );
      return shape;
    }
  }

  /**
   * Sets the cursor position to (posX,posY). The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param posX horizontal position, starting from 0
   * @param posY vertical position, starting from 0
   */
  public void setCursorPos( short posX, short posY ) {
    //TODO
    throw new RuntimeException("TODO");
  }

  /**
   * add any Component to the list of components
   *
   * @param layoutable
   * @return index of component in list of components
   */
  public int add( Layoutable layoutable ) {
    if (layoutable instanceof Shape) {
      ((Shape) layoutable).setScale( scale );
    }
    return super.add( layoutable );
  }

  public void addShape(Shape shape) {
    add( shape );
  }

  /**
   * Removes all entries from this StringListComp
   */
  public void clear() {
    removeAll();
  }

  public ComponentBitmap getBackgroundImage() {
    return backgroundImage;
  }

  public void setBackgroundImage(ComponentBitmap backgroundImage) {
    this.backgroundImage = backgroundImage;
  }

  /**
   * Calculates this component's min, max and pref sizes in one direction and
   * stores them in sizeLimit
   *
   * @param direction
   * @param sizeLimit
   */
  public void calcSizes(Direction direction, SizeLimit sizeLimit) {
    super.calcSizes( direction, sizeLimit );
    if (direction == Direction.X) {
      if (width > 0) {
        sizeLimit.set( width, width, width );
      }
    }
    else {
      if (height > 0) {
        sizeLimit.set( height, height, height );
      }
    }
  }

  /**
   * Determine layout size and toggles visibility of scroller if necessary
   *
   * @param direction
   * @param targetSize    the size of target's painting area
   * @param minSizeChilds
   * @param prefSizeChilds
   * @return the size available for layouting target's children
   */
  public short getLayoutSize( Direction direction, short targetSize, short minSizeChilds, short prefSizeChilds ) {
    if (targetSize < prefSizeChilds) {
      if (targetSize >= minSizeChilds) {
        return minSizeChilds;
      }
      else {
        return super.getLayoutSize( direction, targetSize, minSizeChilds, prefSizeChilds );
      }
    }
    else {
      return prefSizeChilds;
    }
  }

  /**
   * delegate layout calculation to layouter
   *
   * @param direction
   */
  public void doLayout( Direction direction ) {
    super.doLayout( direction );   
  }

  /**
   * Called for painting this component
   *
   * @param compGraph the graphics context to use for painting
   */
  public void doPaintImpl( ComponentGraphics compGraph ) {
    super.doPaintImpl( compGraph );
  }

  /**
   * Called for painting this component
   * This method has to be implemented for each individual component.
   *
   * @param graph the graphics context to use for painting
   */
/*  public void doPaintImpl(ComponentGraphics graph) {
    Shape shape;

    if (!allowPaint()) {
      return;
    }

    if (this.backgroundImage != null) {
      graph.paintBitmap(this.backgroundImage, 0, 0, this.width, this.height);
    }
    else {
      graph.paintBackground(getBackground(), 0, 0, width, height);
    }
    for (int i = 0; i < shapes.size(); i++) {
      shape = (Shape) shapes.elementAt(i);
      if (shape != null) shape.doPaint(graph);
    }
  }
*/
  private void parameterUpdated( String source, Object value ) {
    for (short i = 0; i < childCount(); i++) {
      Shape shape = getShape( i );
      shape.parameterUpdated( source, value );
    }
  }

  /**
   * Updates this component's fields and children from the give componen defintion
   *
   * @param compDef the component definition
   */
  public void fromCompDef( CompDef compDef ) {
    try {
      if (compDef == null) {
        removeAll();
      }
      else {
        DrawDef drawDef = (DrawDef) compDef;

        DecimalObject scaleObj = drawDef.getScale();
        if (scaleObj == null) scale = 1.0f;
        else scale = scaleObj.floatValue();

        Rect gridPos = new Rect();
        for (ModelIterator iter = drawDef.shapeDefIter(); iter.hasNext(); ) {
          ShapeDef shapeDef = (ShapeDef) iter.next();
          addShape( guiBuilder.createShape(gridPos, shapeDef));
        }
      }
    }
    catch (Exception ex) {
      Logger.error( "Could not parse drawDef", ex );
    }
  }

  private short findShapeAt(short posX, short posY) {
    short i = (short) (childCount() - 1);
    Shape shape;
    while (i >= 0) {
      shape = getShape( i );
      if ((shape != null) && shape.containsPos(posX, posY)) {
        return i;
      }
      i--;
    }
    return -1;
  }

  /**
   * Toggles focus, selected, enabled and visible state of this component at position (posX, posY) or for
   * entry identified by key. It depends on the type of component what is meant by posX, posY and key.
   * Many components will just ignore posX and posY values or key.
   * The default implementaion calls the componentListener.
   * @param posX      the x position of the source event
   * @param posY      the y position of the source event
   * @param focus     set focus on/off
   * @param selected  set selected if true, deselected if false, do not change if null
   * @param enabled   set enabled if true, disabled if false, do not change if null
   * @param visible   set visible if true, invisible if false, do not change if null
   */
  public void setState( short posX, short posY, Boolean focus, Boolean selected, Boolean enabled, Boolean visible ) {
    if (posY == -1) {
      super.setState( posX, posY, focus, selected, enabled, visible );
    }
    else {
      if ((posY >= 0) && (selected != null)) {
        Shape shape;
        if (this.selectedIndex >= 0) {
          shape = getShape( this.selectedIndex );
          if (shape != null) {
            shape.setSelected( false );
          }
        }
        if (posY < childCount()) {
          shape = getShape( posY );
          if (shape != null) {
            shape.setSelected( selected.booleanValue() );
            this.selectedIndex = posY;
          }
        }
      }
    }
  }

  /**
   * This method is called whenever a state change occured. Each sub class
   * has to decide what to do, e.g. repaint its content
   */
  public void stateChanged( int oldState, int newState ) {
    // do nothing
  }

  /**
   * Call the action valid for the mouse event.
   * The action is defined in the controller.
   *
   * @param state
   * @param lastPointedComponent
   * @param posInComp
   */
  public void processMouseEvent(int state, Component lastPointedComponent, Position posInComp) {
    if (state == InputManager.STATE_SELECTED || state == InputManager.STATE_LOCAL_MENU ){
      if (lastPointedComponent == this) {
        this.setState( posInComp.getX(), posInComp.getY(), Boolean.TRUE, Boolean.TRUE, null, null );
        lastX = posInComp.getX();
        lastY = posInComp.getY();
        if (listener != null) {
          short shapeIndex = findShapeAt(lastX, lastY);
          if (shapeIndex >= 0) {
            listener.selected( getID(), (short) 0, shapeIndex, true );
          }
        }
      }
    }
    else if (state == InputManager.STATE_DRAGGING) {
      if (drawEnabled) {
        //TODO sollte das nicht besser der PaintController machen
        if (lastX >= 0) {
          ComponentColor color = getForeground();
          Shape line = new Shape();
          line.init(null, Shape.SHAPE_LINE, color, color, lastX, lastY, posInComp.getX(), posInComp.getY());
          addShape( line );
          repaint();
        }
        lastX = posInComp.getX();
        lastY = posInComp.getY();
      }
    }
    else if (state == InputManager.STATE_MOVING) {
      if (drawEnabled) {
        lastX = -1;
        lastY = -1;
      }  
    }
    else if (state == InputManager.STATE_SCROLL) {
      processScroller( posInComp );
    }
  }
}
