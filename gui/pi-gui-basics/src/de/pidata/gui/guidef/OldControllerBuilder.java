/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.guidef;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.event.Dialog;
import de.pidata.gui.view.base.*;
import de.pidata.log.Logger;
import de.pidata.models.binding.*;
import de.pidata.models.simpleModels.SimpleStringTable;
import de.pidata.models.simpleModels.StringMap;
import de.pidata.models.tree.*;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

/**
 *
 * @deprecated use new Controller definition!
 */
@Deprecated
public class OldControllerBuilder {

  public static final Namespace NAMESPACE = Namespace.getInstance("de.pidata.gui");
  public static final QName ID_SCROLLER = NAMESPACE.getQName( "scroller" );
  public static final QName ID_HEADER = NAMESPACE.getQName("header");
  public static final QName ID_BODY = NAMESPACE.getQName("body");
  public static final QName ID_NAME = NAMESPACE.getQName("name");
  public static final QName ID_TABLE = NAMESPACE.getQName("binding");

  public static final QName ID_TEXTCONTROLLER = NAMESPACE.getQName("TextController");
  public static final QName ID_DATECONTROLLER = NAMESPACE.getQName("DateController");
  public static final QName ID_SELECTIONCONTROLLER = NAMESPACE.getQName("SelectionController");
  public static final QName ID_TABLECONTROLLER = NAMESPACE.getQName("TableController");
  public static final QName ID_TREETABLECONTROLLER = NAMESPACE.getQName("TreeTableController");
  public static final QName ID_TREECONTROLLER = NAMESPACE.getQName("TreeController");
  public static final QName ID_FLAGCONTROLLER = NAMESPACE.getQName("FlagController");
  public static final QName ID_PASSWORDCONTROLLER = NAMESPACE.getQName("PasswordController");
  public static final QName ID_PROGRESSCONTROLLER = NAMESPACE.getQName("ProgressController");
  public static final QName ID_NUMBERCONTROLLER = NAMESPACE.getQName("NumberController");
  public static final QName ID_INTEGERCONTROLLER = NAMESPACE.getQName("IntegerController");
  public static final QName ID_SAPIDCONTROLLER = NAMESPACE.getQName("SapIdController");
  public static final QName ID_TEXTEDITOR = NAMESPACE.getQName("TextEditor");
  public static final QName ID_CODEEDITOR = NAMESPACE.getQName("CodeEditor");
  public static final QName ID_HTMLEDITOR = NAMESPACE.getQName("HTMLEditor");
  public static final QName ID_WEBVIEW = NAMESPACE.getQName("WebView");
  public static final QName ID_PAINTCONTROLLER = NAMESPACE.getQName("PaintController");
  public static final QName ID_IMAGECONTROLLER = NAMESPACE.getQName("ImageController");
  public static final QName ID_NODECONTROLLER = NAMESPACE.getQName("NodeController");
  public static final QName ID_TABPANECONTROLLER = NAMESPACE.getQName("TabPaneController");
  public static final QName ID_ENUMCONTROLLER = NAMESPACE.getQName( "EnumController" );
  public static final QName ID_CUSTOMCONTROLLER = NAMESPACE.getQName( "CustomController" );

  public static final QName ID_CONTROLLERGROUP = NAMESPACE.getQName("ControllerGroup");
  public static final QName ID_TABGROUP = NAMESPACE.getQName("TabGroup");
  public static final QName ID_MODULE = NAMESPACE.getQName("Module");

  public static final QName ID_TEXT  = NAMESPACE.getQName("text");
  public static final QName ID_LABEL = NAMESPACE.getQName("label");
  public static final QName ID_SELECTOR = NAMESPACE.getQName("selector");
  public static final QName ID_ROW = NAMESPACE.getQName("row");
  public static final QName ID_BUTTON = NAMESPACE.getQName("button");
  public static final QName ID_PAINT = NAMESPACE.getQName("paint");
  public static final QName ID_PROGRESS = NAMESPACE.getQName("progress");
  public static final QName ID_NODE = NAMESPACE.getQName("node");

  /** Allow selecting none or one value */
  public static final QName SELECTION_MAX_ONE = NAMESPACE.getQName("MAX_ONE");

  /** Allow selecting at least one value */
  public static final QName SELECTION_MIN_ONE = NAMESPACE.getQName("MIN_ONE");

  /** Allow selecting exactly one value */
  public static final QName SELECTION_SINGLE = NAMESPACE.getQName("SINGLE");

  /** Allow selecting zero or more values */
  public static final QName SELECTION_MULTIPLE = NAMESPACE.getQName("MULTIPLE");
  private static final boolean DEBUG = false;
  private Hashtable controllerBuilders = new Hashtable();
  private GuiBuilder guiBuilder;
  protected Application app;

  private ViewFactory viewFactory;

  /** IMPORTANT: for interim compatibility mode only! **/
  private static OldControllerBuilder instance;
  public static OldControllerBuilder getInstance() {
    if (instance == null) {
      instance = new OldControllerBuilder();
    }
    return instance;
  }

  public void init( Application app, GuiBuilder guiBuilder ) {
    this.app = app;
    setGuiBuilder( guiBuilder );
  }

  public Application getApplication() {
    return app;
  }

  public ViewFactory getViewFactory() {
    if (viewFactory == null) {
      viewFactory = new ViewFactory();
    }
    return viewFactory;
  }

  public void setViewFactory( ViewFactory viewFactory ) {
    this.viewFactory = viewFactory;
  }

  public DialogDef getDialogDef( QName dialogID ) {
    return this.app.getDialogDef( dialogID );
  }

  private TextController newTextController( QName typeID ) {
    TextController textController;
    if (typeID == ID_TEXTCONTROLLER) textController =  new TextController();
    else if (typeID == ID_NUMBERCONTROLLER) textController = new NumberController();
    else if (typeID == ID_INTEGERCONTROLLER) textController = new IntegerController();
    else if (typeID == ID_SAPIDCONTROLLER) textController = new SapIdController();
    else if (typeID == ID_PASSWORDCONTROLLER) textController = new PasswordController();
    else throw new IllegalArgumentException("Unknown text controller type: "+ typeID);
    return textController;
  }

  public Controller createController(ControllerBuilder newBuilder, ControllerType ctrlDef, MutableControllerGroup ctrlGroup) throws Exception {
    QName typeID  = ctrlDef.getTypeID();
    if (typeID == ID_TEXTCONTROLLER) {
      return createInputController( ctrlDef, ctrlGroup, typeID );
    }
    else if (typeID == ID_NODECONTROLLER) {
      return createNodeController( ctrlDef, ctrlGroup );
    }
    else if (typeID == ID_TABPANECONTROLLER) {
      return createTabPaneController( ctrlDef, ctrlGroup );
    }
    else if (typeID == ID_DATECONTROLLER) {
      return createDateController( ctrlDef, ctrlGroup, typeID );
    }
    else if (typeID == ID_PROGRESSCONTROLLER) {
      return createProgressController( ctrlDef, ctrlGroup, typeID );
    }
    else if (typeID == ID_NUMBERCONTROLLER) {
      return createInputController( ctrlDef, ctrlGroup, typeID );
    }
    else if (typeID == ID_INTEGERCONTROLLER) {
      return createInputController( ctrlDef, ctrlGroup, typeID );
    }
    else if (typeID == ID_SELECTIONCONTROLLER) {
      return createListController( newBuilder, ctrlDef, ctrlGroup );
    }
    else if (typeID == ID_FLAGCONTROLLER) {
      return createFlagController( ctrlDef, ctrlGroup );
    }
    else if (typeID == ID_SAPIDCONTROLLER) {
      return createInputController( ctrlDef, ctrlGroup, typeID );
    }
    else if (typeID == ID_PASSWORDCONTROLLER) {
      return createInputController( ctrlDef, ctrlGroup, typeID );
    }
    else if (typeID == ID_TEXTEDITOR) {
      return createTextEditor( ctrlDef, ctrlGroup, typeID );
    }
    else if (typeID == ID_CODEEDITOR) {
      return createCodeEditor( ctrlDef, ctrlGroup, typeID );
    }
    else if (typeID == ID_WEBVIEW) {
      return createWebView( ctrlDef, ctrlGroup, typeID );
    }
    else if (typeID == ID_HTMLEDITOR) {
      return createHTMLEditor( ctrlDef, ctrlGroup, typeID );
    }
    else if (typeID == ID_PAINTCONTROLLER) {
      return createPaintController( ctrlDef, ctrlGroup );
    }
    else if (typeID == ID_IMAGECONTROLLER) {
      return createImageController( ctrlDef, ctrlGroup );
    }
    else if (typeID == ID_CONTROLLERGROUP) {
      return createControllerGroup( newBuilder, ctrlDef, ctrlGroup );
    }
    else if (typeID == ID_TABGROUP) {
      return createTabGroup( newBuilder, ctrlDef, ctrlGroup );
    }
    else if (typeID == ID_MODULE) {
      QName moduleID = ctrlDef.getModuleID();
      if (moduleID == null) {
        return createModuleController( newBuilder, ctrlDef, null, ctrlGroup );
      }
      else {
        Module moduleDef = this.app.getModule( moduleID );
        if (moduleDef != null) {
          return createModuleController( newBuilder, ctrlDef, moduleDef, ctrlGroup );
        }
        else {
          DialogModuleType dialogModule = this.app.getDialogModule( moduleID );
          return createModuleController( newBuilder, ctrlDef, dialogModule, ctrlGroup );
        }
      }
    }
    else if (typeID == ID_ENUMCONTROLLER) {
      return createEnumController (newBuilder, ctrlDef, ctrlGroup);
    }
    else if (typeID == ID_CUSTOMCONTROLLER) {
      return createCustomController (newBuilder, ctrlDef, ctrlGroup);
    }
    else {
      return createOtherController( ctrlDef, ctrlGroup );
    }
  }

  public void setGuiBuilder( GuiBuilder guiBuilder ) {
    this.guiBuilder = guiBuilder;
    if (guiBuilder != null) {
      guiBuilder.setModuleContainer( this.app );
    }
  }

  public GuiBuilder getGuiBuilder() {
    return guiBuilder;
  }

  public void addControllerType(QName controllerID, ControllerBuilder builder) {
    this.controllerBuilders.put(controllerID, builder);
  }

  public boolean getBoolean(Boolean boolObject) {
    if (boolObject == null) return false;
    else return boolObject.booleanValue();
  }

  /**
   * Creates the dialogController's child Controllers defined by the given GUI definition.
   * @param iter Iterator over the child Controller definitions
   * @param dlgController the DialogController which will contain the created Controllers
   */
  protected void createDlgControllers( ControllerBuilder newBuilder, ModelIterator iter, DialogController dlgController ) {
    while (iter.hasNext()) {
      Model model = iter.next();
      createOneController( newBuilder, model, dlgController );
    }
  }

  public void addChildControllers( ControllerBuilder newBuilder, MutableControllerGroup ctrl, ControllerType ctrlDef, boolean readOnly ) {
    ModelIterator iter;Model model;
    for (iter = ctrlDef.iterator(null, null); iter.hasNext(); ) {
      model = iter.next();
      Controller childCtrl = createOneController( newBuilder, model, ctrl );
      if (readOnly && (childCtrl instanceof ValueController)) {
        ((ValueController) childCtrl).setReadOnly( true );
      }
    }
  }

  public void addChildControllers( ControllerBuilder newBuilder, MutableControllerGroup ctrl, Module moduleDef, boolean readOnly ) {
    ModelIterator iter;Model model;
    for (iter = moduleDef.iterator(null, null); iter.hasNext(); ) {
      model = iter.next();
      Controller childCtrl = createOneController( newBuilder, model, ctrl );
      if (readOnly && (childCtrl instanceof ValueController)) {
        ((ValueController) childCtrl).setReadOnly( true );
      }
    }
  }

  /**
   * Builds a specific {@link Controller} as defined by the given model. The model should represent a {@link ControllerType} (old).
   * @param model the Controller definition from the GUI definition file.
   * @param ctrlGroup the Controller group which will contain the created Controller
   * @return the specific created Controller
   */
  protected Controller createOneController( ControllerBuilder newBuilder, Model model, MutableControllerGroup ctrlGroup ) {
    QName typeID;
    OldControllerBuilder builder = null;
    Controller ctrl;
    try {
      if (model instanceof ControllerType) {
        ControllerType ctrlDef = (ControllerType) model;
        typeID  = ctrlDef.getTypeID();
        if (typeID == null) {
          throw new IllegalArgumentException("typeID not defined for controller ID="+ctrlDef.key());
        }
        //builder = (ControllerBuilder) controllerBuilders.get(typeID); // TODO: unused by now; don't reactivate in old GUI
        if (builder == null) builder = this;
        ctrl = builder.createController(newBuilder, ctrlDef, ctrlGroup);
        ctrlGroup.addController( ctrlDef.getID(), ctrl );
        if (ctrl instanceof ListController) {
          Controller detailCtrl = ((ListController) ctrl).getDetailController();
          if (detailCtrl != null) {
            ctrlGroup.addController( detailCtrl.getName(), detailCtrl );
          }
        }
      }
      else if (model instanceof TableControllerDef) {
        TableControllerDef tableCtrlDef = (TableControllerDef) model;
        ctrl = createTableController(newBuilder, tableCtrlDef, ctrlGroup);
        ctrlGroup.addController( tableCtrlDef.getID(), ctrl );
      }
      else if (model instanceof TreeControllerDef) {
        TreeControllerDef treeCtrlDef = (TreeControllerDef) model;
        ctrl = createTreeController(treeCtrlDef, ctrlGroup);
        ctrlGroup.addController( treeCtrlDef.getID(), ctrl );
      }
      else if (model instanceof TreeTableControllerDef) {
        TreeTableControllerDef treeCtrlDef = (TreeTableControllerDef) model;
        ctrl = createTreeTableController(newBuilder, treeCtrlDef, ctrlGroup);
        ctrlGroup.addController( treeCtrlDef.getID(), ctrl );
      }
      else if (model instanceof ActionDef) {
        ActionDef actionDef = (ActionDef) model;
        ctrl = createAction(actionDef, ctrlGroup);
        ctrlGroup.addController(actionDef.getID(), ctrl );
      }
      else {
        ctrl = null;
      }
    }
    catch (Exception ex) {
      String msg = "Could not create controller, id="+model.key();
      Logger.error( msg, ex);
      throw new IllegalArgumentException( msg );
    }
    return ctrl;
  }

  protected Controller createAction( ActionDef actionDef, ControllerGroup ctrlGroup ) {
    ModelIterator iter;
    Comp compRef;
    ButtonViewPI button = null;
    GuiOperation guiOp;
    String classname;

    for (iter = actionDef.compIter(); iter.hasNext(); ) {
      compRef = (Comp) iter.next();
      button = getViewFactory().createButtonView( compRef.getCompID(), compRef.getModuleID() );
    }
    classname = actionDef.getClassname();
    guiOp = createGuiOp( actionDef.getID(), classname, actionDef.createIter(), actionDef.invokeIter(), actionDef.getCommand(), ctrlGroup );

    if (button != null) {
      return createButtonController( actionDef, ctrlGroup, button, guiOp );
    }
    else {
      return createActionController( actionDef, ctrlGroup, guiOp );
    }
  }

  private Controller createActionController( ActionDef actionDef, ControllerGroup ctrlGroup, GuiOperation guiOp ) {
    DefaultActionController defaultActionController = new DefaultActionController();

    QName shortCut = actionDef.getShortCut();
    if (shortCut != null) {
      ctrlGroup.getDialogController().addShortCut( shortCut, guiOp, defaultActionController );
    }

    String command = actionDef.getCommand();
    if (command != null && command.length() > 0) {
      ctrlGroup.getDialogController().addShortCut(NAMESPACE.getQName(command), guiOp, defaultActionController );
    }

    defaultActionController.init( actionDef.getID(), ctrlGroup, guiOp );

    return defaultActionController;
  }

  public Controller createButtonController( ActionDef actionDef, ControllerGroup ctrlGroup, ButtonViewPI button, GuiOperation guiOp ) {
    ButtonController buttonController = new ButtonController();

    QName shortCut = actionDef.getShortCut();
    if (shortCut != null) {
      ctrlGroup.getDialogController().addShortCut( shortCut, guiOp, buttonController );
    }

    String command = actionDef.getCommand();
    if (command != null && command.length() > 0) {
      ctrlGroup.getDialogController().addShortCut( NAMESPACE.getQName( command ), guiOp, buttonController );
    }

    Binding valueBinding = null;
    Binding textBinding = null;
    Binding iconBinding = null;
    QName valueID = actionDef.getValueID();
    if (valueID != null) {
      valueBinding = createAttributeBinding( ctrlGroup, actionDef.getDataSource(), actionDef.getModel(), valueID, actionDef.namespaceTable() );
    }
    QName labelValueID = actionDef.getLabelValueID();
    if (labelValueID != null) {
      textBinding = createAttributeBinding( ctrlGroup, actionDef.getDataSource(), actionDef.getModel(), labelValueID, actionDef.namespaceTable() );
    }
    QName iconValueID = actionDef.getIconValueID();
    if (iconValueID != null) {
      iconBinding = createAttributeBinding( ctrlGroup, actionDef.getDataSource(), actionDef.getModel(), iconValueID, actionDef.namespaceTable() );
    }
    buttonController.init( actionDef.getID(), ctrlGroup, button, valueBinding, textBinding, iconBinding, guiOp );

    return buttonController;
  }

  public GuiOperation createGuiOp( QName eventID, String classname, ModelIterator createIter, ModelIterator invokeIter, String command, ControllerGroup controllerGroup ) {
    GuiOperation guiOp;
    if ((classname != null) && (classname.length() > 0)) {
      try {
        guiOp = (GuiOperation) Class.forName(classname).newInstance();
        guiOp.init( eventID, command, controllerGroup );
      }
      catch (Exception e) {
        e.printStackTrace();
        throw new IllegalArgumentException("Could not create action class: "+classname);
      }
    }
    else {
      GuiInvokeOperation guiInvokeOp = new GuiInvokeOperation();
      Vector createList = new Vector();
      Object action;
      while (createIter.hasNext()) {
        CreateType createDef = (CreateType) createIter.next();
        createList.addElement(createDef);
      }
      Vector invokeList = new Vector();
      while (invokeIter.hasNext()) {
        InvokeType invokeDef = (InvokeType) invokeIter.next();
        invokeList.addElement(invokeDef);
      }
      guiInvokeOp.init( eventID, createList, invokeList, command, controllerGroup );
      guiOp = guiInvokeOp;
    }
    return guiOp;
  }

  public ModuleController createModuleController( ControllerBuilder newBuilder, ControllerType ctrlDef, Model moduleDef, ControllerGroup parentGroup ) throws Exception {
    ModelBinding binding;
    String modelPath = ctrlDef.getModel();
    if (modelPath == null) {
      binding = null;
    }
    else {
      binding = createModelBinding( parentGroup, ctrlDef.getDataSource(), ctrlDef.getModel(), null, ctrlDef.namespaceTable() );
    }
    return createModuleController( newBuilder, ctrlDef, moduleDef, parentGroup, binding );
  }

  protected ModuleController createModuleController( ControllerBuilder newBuilder, ControllerType ctrlDef, Model moduleDef, ControllerGroup parentGroup, ModelBinding binding ) throws Exception {
    ModuleController ctrl;
    String ctrlClassName = ctrlDef.getClassname();
    if (ctrlClassName == null) {
      ctrl = new ModuleController();
    }
    else {
      ctrl = (ModuleController) Class.forName( ctrlClassName ).newInstance();
    }
    Comp compRef = ctrlDef.compIter().next();
    ModuleViewPI moduleViewPI = getViewFactory().createModuleView( compRef.getCompID(), compRef.getModuleID() );

    ctrl.init( ctrlDef.getID(), parentGroup, moduleViewPI, binding, ctrlDef.getModuleID(), getBoolean( ctrlDef.getReadOnly() ) );
    if (moduleDef != null) {
      ModuleGroup moduleGroup = null;
      if (moduleDef instanceof Module) {
        moduleGroup = createModuleGroup( newBuilder, (Module) moduleDef, ctrl, ctrl, "." );
      }
      else {
        moduleGroup = ((DialogModuleType) moduleDef).createController( newBuilder, ctrl, ctrl, ".", moduleDef.namespaceTable() );
      }
      ctrl.setModuleGroup( moduleGroup, ViewAnimation.NONE );
    }
    return ctrl;
  }

  /**
   * Loads and creates the module'S controller and adds it to the parent {@link ModuleController}.
   *
   * @return the loaded module's controller
   * @throws Exception
   */
  public ModuleGroup loadModule( ControllerBuilder newBuilder, ModuleController moduleCtrl, QName moduleID, ModelSource modelSource, String modelPath ) throws Exception {
    Module moduleDef = (Module) this.app.get( Application.ID_MODULE, moduleID );
    ModuleGroup moduleGroup = createModuleGroup( newBuilder, moduleDef, moduleCtrl, modelSource, modelPath );
    moduleCtrl.setModuleGroup( moduleGroup, ViewAnimation.NONE );
    return moduleGroup;
  }

  protected DefaultControllerGroup createControllerGroup( ControllerBuilder newBuilder, ControllerType ctrlDef, ControllerGroup parentGroup ) throws Exception {
    ModelBinding binding = createModelBinding( parentGroup, ctrlDef.getDataSource(), ctrlDef.getModel(), null, ctrlDef.namespaceTable() );
    DefaultControllerGroup ctrl;
    String classname = ctrlDef.getClassname();
    if ((classname == null) || (classname.length() == 0)) {
      ctrl = new DefaultControllerGroup();
    }
    else {
      ctrl = (DefaultControllerGroup) Class.forName(classname).newInstance();
    }

    ctrl.init( ctrlDef.getID(), parentGroup, null, binding, getBoolean( ctrlDef.getReadOnly() ) );
    addChildControllers( newBuilder, ctrl, ctrlDef, getBoolean( ctrlDef.getReadOnly() ) );
    return ctrl;
  }

  /**
   * Create new ModuleGroup having ID moduleID
   * @param moduleDef    module definition to create ModuleGroup from
   * @param moduleCtrl   controller to load module into
   * @param modelSource  optional model data source for new ModuleGroup's model - must be null if ModuleGroup creates its own model
   * @param modelPath    optional path to ModuleGroup's model
   * @return             the new ModuleGroup
   * @throws Exception
   */
  public ModuleGroup createModuleGroup( ControllerBuilder newBuilder, Module moduleDef, ModuleController moduleCtrl, ModelSource modelSource, String modelPath ) throws Exception {
    ModelBinding binding = createModelBinding( moduleCtrl, modelSource, modelPath, null, moduleDef.namespaceTable() );
    ModuleGroup ctrl;
    String classname = moduleDef.getController();
    if ((classname == null) || (classname.length() == 0)) {
      ctrl = new ModuleGroup();
    }
    else {
      ctrl = (ModuleGroup) Class.forName(classname).newInstance();
    }

    ctrl.init( moduleDef.getName(), moduleCtrl, binding, moduleCtrl.isReadOnly(), null );
    addChildControllers( newBuilder, ctrl, moduleDef, moduleCtrl.isReadOnly() );
    return ctrl;
  }

  protected ControllerGroup createTabGroup( ControllerBuilder newBuilder, ControllerType ctrlDef, ControllerGroup parentGroup ) throws Exception {
    ModelBinding binding = createModelBinding( parentGroup, ctrlDef.getDataSource(), ctrlDef.getModel(), null, ctrlDef.namespaceTable() );
    TabControllerGroup ctrl;
    TabGroupViewPI tabGroup;

    String classname = ctrlDef.getClassname();
    if ((classname == null) || (classname.length() == 0)) {
      ctrl = new TabControllerGroup();
    }
    else {
      ctrl = (TabControllerGroup) Class.forName(classname).newInstance();
    }

    Comp comp = ctrlDef.compIter().next();
    tabGroup = getViewFactory().createTabGroupViewPI( comp.getCompID(), comp.getModuleID() );

    ctrl.init( ctrlDef.getID(), parentGroup, tabGroup, binding, ctrlDef );
    addChildControllers( newBuilder, ctrl, ctrlDef, getBoolean( ctrlDef.getReadOnly() ) );

    //TODO: not tested
    throw new RuntimeException( "TODO: not tested ;-)" );
    //return ctrl;
  }

  protected Controller createOtherController( ControllerType ctrlDef, ControllerGroup parentGroup ) throws Exception {
    String classname = ctrlDef.getClassname();
    if ((classname == null) || (classname.length() == 0)) {
      throw new IllegalArgumentException( "Missing classname for non standard controller typeID="+ctrlDef.getTypeID() );
    }
    Controller ctrl = (Controller) Class.forName(classname).newInstance();
    ctrl.setParentController( parentGroup );
    return ctrl;
  }

  /**
   * Tries to find the data source controller within ctrlGroup or one of its parents.
   * Recursive searching ist stopped at ctrlGroup's DialogController.
   * @param ctrlGroup    the controller group to start searching for dataSourceID
   * @param dataSourceID ID of the dat source controller
   * @return the data source controller
   * @throws IllegalArgumentException if dataSourceID could not be found
   * @throws ClassCastException if controller with dataSourceID is not a SelectionController
   */
  private ModelSource getDataSource( ControllerGroup ctrlGroup, QName dataSourceID ) {
    ModelSource modelSource;
    if (dataSourceID == null) {
      modelSource = ctrlGroup;
    }
    else {
      modelSource = (ModelSource) ctrlGroup.getController(dataSourceID);
      while ((modelSource == null) && (!(ctrlGroup instanceof DialogController))) {
        ctrlGroup = ctrlGroup.getParentGroup();
        modelSource = (ModelSource) ctrlGroup.getController(dataSourceID);
      }
      if (modelSource == null) {
        throw new IllegalArgumentException( "Unknown data source ID="+dataSourceID );
      }
    }
    return modelSource;
  }

  protected Binding createAttributeBinding( ControllerGroup ctrlGroup, QName dataSourceID, String modelPath, QName valueID, NamespaceTable namespaceTable ) {
    if ((valueID == null) && (modelPath == null) && (dataSourceID == null)) {
      return null;
    }
    ModelSource modelSource = getDataSource( ctrlGroup, dataSourceID );
    XPath xPath = null;
    if ((modelPath != null) && (modelPath.length() > 0)) {
      xPath = new XPath( namespaceTable, modelPath );    //TODO NS sollte eigentlich der default-NS des GUI-Definition sein
    }
    AttributeBinding binding = new AttributeBinding( ctrlGroup.getContext(), modelSource, xPath, valueID );
    return binding;
  }

  public Binding createAttributeBinding( ControllerGroup ctrlGroup, ModelSource modelSource, String modelPath, QName valueID, NamespaceTable namespaceTable ) {
    if (((valueID == null) && (modelPath == null)) || (modelSource == null)) {
      return null;
    }
    XPath xPath = null;
    if ((modelPath != null) && (modelPath.length() > 0)) {
      xPath = new XPath( namespaceTable, modelPath );    //TODO NS sollte eigentlich der default-NS des GUI-Definition sein
    }
    AttributeBinding binding = new AttributeBinding( ctrlGroup.getContext(), modelSource, xPath, valueID );
    return binding;
  }

  protected ModelBinding createModelBinding( ControllerGroup ctrlGroup, QName dataSourceID, String modelPath, QName relationID, NamespaceTable namespaceTable ) {
    ModelSource modelSource = getDataSource( ctrlGroup, dataSourceID );
    return createModelBinding( ctrlGroup, modelSource, modelPath, relationID, namespaceTable );
  }

  protected ModelBinding createModelBinding( ControllerGroup ctrlGroup, ModelSource modelSource, String modelPath, QName relationID, NamespaceTable namespaceTable ) {
    XPath xPath = null;
    if ((modelPath != null) && (modelPath.length() > 0)) {
      xPath = new XPath( namespaceTable, modelPath );    //TODO NS sollte eigentlich der default-NS des GUI-Definition sein
    }
    if (modelSource == null) {
      return null;
    }
    else {
      ModelBinding binding = new ModelBinding( ctrlGroup.getContext(), modelSource, xPath, relationID );
      return binding;
    }
  }

  protected ModelListBinding createModelListBinding( ControllerGroup ctrlGroup, QName dataSourceID, String modelPath, QName relationID, Filter filter, NamespaceTable namespaceTable ) {
    if ((modelPath == null) && (dataSourceID == null)) {
      return null;
    }
    ModelSource modelSource = getDataSource( ctrlGroup, dataSourceID );
    XPath xPath = null;
    if ((modelPath != null) && (modelPath.length() > 0)) {
      xPath = new XPath( namespaceTable, modelPath );    //TODO NS sollte eigentlich der default-NS des GUI-Definition sein
    }
    ModelListBinding binding = new ModelListBinding( ctrlGroup.getContext(), modelSource, xPath, relationID, filter );
    return binding;
  }

  protected Controller createTableController( ControllerBuilder newBuilder, TableControllerDef tableCtrlDef, ControllerGroup ctrlGroup ) throws Exception {
    TableController tableController;
    ModelIterator iter;
    Column colRef;
    TableViewPI tableViewPI;
    ControllerType renderCtrlDef;
    ActionDef cellActionDef;
    ControllerType editCtrlDef;
    Lookup lookup;
    String classname;
    String displayValuePathStr = ".";
    Selection selection = null;
    boolean readOnly;
    boolean selectable;

    QName rowCompID = null;
    for (iter = tableCtrlDef.compIter(); iter.hasNext(); ) {
      Comp compRef = (Comp) iter.next();
      if (compRef.key() == ID_ROW) {
        rowCompID = compRef.getCompID();
      }
    }
    tableViewPI = getViewFactory().createTableView( tableCtrlDef.getTableID(), tableCtrlDef.getModuleID(), rowCompID, tableCtrlDef.getRowColorID(), tableCtrlDef.getColorColumnID() );
    if (tableViewPI == null) {
      throw new IllegalArgumentException("TableController ID="+tableCtrlDef.key()+": table not found, ID="+tableCtrlDef.getTableID() );
    }


    classname = tableCtrlDef.getClassname();
    if ((classname == null) || (classname.length() == 0)) {
      tableController = new DefaultTableController();
    }
    else {
      tableController = (TableController) Class.forName(classname).newInstance();
    }

    Boolean ro = tableCtrlDef.getReadOnly();
    if (ro != null) readOnly = ro.booleanValue();
    else readOnly = false;

    Boolean sel = tableCtrlDef.getSelectable();
    if(sel!=null){
      selectable = sel.booleanValue();
    }
    else{
      selectable = true;
    }

    NamespaceTable namespaceTable = tableCtrlDef.namespaceTable();

    //TODO problem im Kommentar lösen, v.a. für Skin
    //--- We must initialize Table's Binding and Selection after adding column, because if all bindings for column lookup
    //    must be added to controllerGroup's binding list before table's selection is added. Otherwise the column selection
    //    is uninitialized when initializing the table rows
    lookup = tableCtrlDef.getLookup();
    if (lookup != null) {
      selection = createSelection( lookup, ctrlGroup, namespaceTable );
      displayValuePathStr = lookup.getValuePath();
    }
    // If a table has a binding it is used to store the key of the selected table row, not the table row itself
    Binding binding = createAttributeBinding( ctrlGroup, tableCtrlDef.getDataSource(), tableCtrlDef.getModel(), tableCtrlDef.getValueID(), namespaceTable );

    int colIndex = 0;
    for (iter=tableCtrlDef.columnIter(); iter.hasNext(); ) {
      colRef = (Column) iter.next();
      QName colName = colRef.getID();
      QName bodyCompID = colRef.getCompID();
      if (colName == null) colName = bodyCompID;

      Controller renderCtrl;
      renderCtrlDef = colRef.getRenderer();
      cellActionDef = colRef.getCellAction();
      if (renderCtrlDef == null && cellActionDef == null) {
        TextController textController = new TextController();
        Binding attributeBinding = createAttributeBinding( ctrlGroup, tableController, colRef.getModel(), colRef.getValueID(), namespaceTable );
        TextViewPI textComp = getViewFactory().createTextView( bodyCompID, tableCtrlDef.getModuleID() );
        String inpMode = colRef.getInputMode();
        InputMode inputMode = inputModeFromString( inpMode );
        Object format = null;
        if (inputMode == null) {
          inputMode = InputMode.string;
          format = inpMode;
        }
        textController.init( bodyCompID, null, textComp, attributeBinding, inputMode, format, readOnly );
        renderCtrl = textController;
      }
      else {
        if (renderCtrlDef != null) {
          ValueController valueCtrl = (ValueController) createController( newBuilder, renderCtrlDef, null ); // FIXME: don'T add to any container; will be handled dynamically
          if (valueCtrl.getValueBinding() == null) {
            Binding attributeBinding = createAttributeBinding( ctrlGroup, tableController, colRef.getModel(), colRef.getValueID(), namespaceTable );
            valueCtrl.initValueBinding( attributeBinding );
          }
          renderCtrl = valueCtrl;
        }
        else {
          renderCtrl = createAction( cellActionDef, ctrlGroup  );
        }
        QName labelValueID = colRef.getLabelValueID();
        if (labelValueID != null && renderCtrl instanceof FlagController) {
          Binding labelBinding = ((FlagController) renderCtrl).getLabelBinding();
          if (labelBinding == null) {
            labelBinding = createAttributeBinding( ctrlGroup, tableController, colRef.getModel(), labelValueID, namespaceTable );
            ((FlagController) renderCtrl).initLabelBinding( labelBinding );
          }
        }
      }

      ValueController editCtrl;
      editCtrlDef = colRef.getEditor();
      if (editCtrlDef == null) {
        editCtrl = null;
      }
      else {
        editCtrl = (ValueController) createController( newBuilder, editCtrlDef, null );
        if (editCtrl.getValueBinding() == null) {
          Binding attributeBinding = createAttributeBinding( ctrlGroup, tableController, colRef.getModel(), colRef.getValueID(), namespaceTable );
          editCtrl.initValueBinding( attributeBinding );
        }
        QName labelValueID = colRef.getLabelValueID();
        if (labelValueID != null && editCtrl instanceof FlagController) {
          Binding labelBinding = ((FlagController) editCtrl).getLabelBinding();
          if (labelBinding == null) {
            labelBinding = createAttributeBinding( ctrlGroup, tableController, colRef.getModel(), labelValueID, namespaceTable );
            ((FlagController) editCtrl).initLabelBinding( labelBinding );
          }
        }
      }

      String inputModeStr = colRef.getInputMode();
      Object format = null;
      InputMode inputMode = InputMode.fromString( inputModeStr );
      if (inputMode == null) {
        inputMode = InputMode.string;
        format = inputModeStr;
      }
      if (renderCtrl != null) {
        renderCtrl.setInputMode( inputMode, format );
      }

      tableController.addColumn( new ColumnInfo( ctrlGroup.getContext(), colIndex, colName, colRef.getModel(), displayValuePathStr,
          colRef.getValueID(), colRef.getIconValueID(), colRef.getHelpValueID(), colRef.getBackgroundValueID(),
          colRef.getHeadID(), bodyCompID, colRef.getLabelValueID(), namespaceTable, renderCtrl, editCtrl,
          getBoolean( colRef.getReadOnly() ), getBoolean( colRef.getSortable() ) ) );
      //TODO: Auch bei anderen Controllern parent setzen?
      if(renderCtrl instanceof ButtonController){
        renderCtrl.setParentController( tableController );
      }
      colIndex++;
    }

    GuiOperation rowAction = null;
    ActionDef rowActionDef = tableCtrlDef.getRowAction();
    if (rowActionDef != null) {
      String rowActionClassname = rowActionDef.getClassname();
      rowAction = createGuiOp( rowActionDef.getID(), rowActionClassname, rowActionDef.createIter(), rowActionDef.invokeIter(), rowActionDef.getCommand(), ctrlGroup );
    }

    QName inplaceEd = tableCtrlDef.getInplaceEdit();
    InplaceEdit inplaceEdit = null;
    if (inplaceEd != null) {
      inplaceEdit = InplaceEdit.fromString( inplaceEd.getName() );
    }
    if (inplaceEdit == null) {
      inplaceEdit = InplaceEdit.none;
    }
    tableController.init( tableCtrlDef.getID(), ctrlGroup, tableViewPI, binding, selection, readOnly, selectable, inplaceEdit, rowAction );

    GuiOperation rowDoubleClickAction = null;
    ActionDef rowDoubleClickActionDef = tableCtrlDef.getRowDoubleClickAction();
    if (rowDoubleClickActionDef != null) {
      String rowDoubleClickActionClassname = rowDoubleClickActionDef.getClassname();
      rowDoubleClickAction = createGuiOp( rowDoubleClickActionDef.getID(), rowDoubleClickActionClassname, rowDoubleClickActionDef.createIter(), rowDoubleClickActionDef.invokeIter(), rowDoubleClickActionDef.getCommand(), ctrlGroup );
      tableController.setRowDoubleClickAction(rowDoubleClickAction);
    }

    GuiOperation rowMenuAction = null;
    ActionDef rowMenuActionDef = tableCtrlDef.getRowMenuAction();
    if (rowMenuActionDef != null) {
      String rowMenuActionClassname = rowMenuActionDef.getClassname();
      rowMenuAction = createGuiOp( rowMenuActionDef.getID(), rowMenuActionClassname, rowMenuActionDef.createIter(), rowMenuActionDef.invokeIter(), rowMenuActionDef.getCommand(), ctrlGroup );
      tableController.setRowMenuAction(rowMenuAction);
    }


    return tableController;
  }

  public TableController createTableController ( QName tableCompID, String modelPath, QName relationID, ControllerGroup ctrlGroup, List<QName> columnNames, GuiOperation rowAction, NamespaceTable namespaceTable ) {

    TableController tableController = new DefaultTableController();
    QName tableControllerID = tableCompID;
    TableViewPI tableViewPI;
    String displayValuePathStr = null;
    Selection selection;
    boolean readOnly = false;
    boolean selectable = true;

    tableViewPI = getViewFactory().createTableView( tableControllerID, null, null, null, null );
    if (tableViewPI == null) {
      throw new IllegalArgumentException( "TableController ID=" + tableControllerID + ": table not found, ID=" + tableCompID );
    }

    selection = createSelection( relationID, modelPath, SelectionType.MAX_ONE, ctrlGroup, namespaceTable );

    int colIndex = 0;

    for (QName columnName : columnNames) {
      QName colName = columnName;
      QName bodyCompID = NAMESPACE.getQName( columnName.getName() );
      QName colValueID = columnName;
      QName headID = null;
      QName moduleID = null;
      ValueController editCtrl = null;
      TextController textController = new TextController();
      Binding attributeBinding = createAttributeBinding( ctrlGroup, tableController, ".", colValueID, namespaceTable );
      TextViewPI textComp = getViewFactory().createTextView( bodyCompID, moduleID );

      textController.init( bodyCompID, null, textComp, attributeBinding, null, null, readOnly );
      tableController.addColumn( new ColumnInfo( ctrlGroup.getContext(), colIndex, colName, ".", displayValuePathStr,
          colValueID, null, null, null,
          headID, bodyCompID, null,  namespaceTable, textController, editCtrl, true, false ) );
      colIndex++;
    }

    tableController.init( tableControllerID, ctrlGroup, tableViewPI, null, selection, readOnly, selectable, InplaceEdit.none, rowAction );
    return tableController;
  }

  protected Controller createTreeController( TreeControllerDef treeCtrlDef, ControllerGroup ctrlGroup ) throws Exception {
    TreeController treeController;
    ModelIterator iter;
    TreeViewPI treeViewPI;
    Lookup lookup;
    String classname;
    String displayValuePathStr = ".";
    Selection selection = null;
    boolean readOnly;
    boolean lazyLoading;

    treeViewPI = getViewFactory().createTreeView( treeCtrlDef.getTreeID(), treeCtrlDef.getModuleID(), null, null );
    if (treeViewPI == null) {
      throw new IllegalArgumentException("TreeController ID="+treeCtrlDef.key()+": tree not found, ID="+treeCtrlDef.getTreeID() );
    }


    classname = treeCtrlDef.getClassname();
    if ((classname == null) || (classname.length() == 0)) {
      treeController = new DefaultTreeController();
    }
    else {
      treeController = (TreeController) Class.forName(classname).newInstance();
    }
    treeViewPI.setController( treeController );

    Boolean ro = treeCtrlDef.getReadOnly();
    if (ro != null) readOnly = ro.booleanValue();
    else readOnly = false;

    Boolean lazy = treeCtrlDef.getLazyLoading();
    if(lazy != null) lazyLoading = lazy.booleanValue();
    else lazyLoading = true; //default is true;

    ModelBinding treeBinding = null;
    lookup = treeCtrlDef.getLookup();
    if (lookup != null) {
      //selection = createSelection( lookup, ctrlGroup );
      //displayValuePathStr = lookup.getValuePath();
      treeBinding = createModelBinding( ctrlGroup, lookup.getDataSource(), lookup.getModel(), lookup.getValueID(), treeCtrlDef.namespaceTable() );
    }

    // If a table has a binding it is used to store the key of the selected table row, not the table row itself
    Binding binding = null;
    if (treeCtrlDef.getModel() != null) {
      throw new RuntimeException( "TODO" );
      //binding = createAttributeBinding( ctrlGroup, treeCtrlDef.getDataSource(), treeCtrlDef.getModel(), treeCtrlDef.getValueID() );
    }

    ControllerType nodeCtrlDef = treeCtrlDef.getNodeController();
    if (nodeCtrlDef != null) {
      // TODO
      throw new RuntimeException( "TODO" );
    }

    int colIndex = 0;
    for (Node nodeDef : treeCtrlDef.nodeIter()) {
      QName colName = nodeDef.getID();
      if (colName == null) colName = nodeDef.getCompID();

      QName inputMode = null;
      String inputModeStr = nodeDef.getInputMode();
      if ((inputModeStr != null) && (inputModeStr.length() > 0)) {
        inputMode = NAMESPACE.getQName( inputModeStr );
      }
      NodeType nodeType = new NodeType();
      nodeType.setID( nodeDef.getID() );
      nodeType.setCompID( nodeDef.getCompID() );
      nodeType.setModelTypeID( nodeDef.getModelTypeID() );
      nodeType.setValueID( nodeDef.getValueID() );
      nodeType.setIconValueID( nodeDef.getIconValueID() );
      nodeType.setEditFlagID( nodeDef.getEditFlagID() );
      nodeType.setEnabledFlagID( nodeDef.getEnabledFlagID() );
      nodeType.setRelationID( nodeDef.getRelationID() );
      nodeType.setInputMode( nodeDef.getInputMode() );
      treeController.addNodeType( nodeType );
      colIndex++;
    }

    GuiOperation nodeAction = null;
    ActionDef nodeActionDef = treeCtrlDef.getNodeAction();
    if (nodeActionDef != null) {
      String rowActionClassname = nodeActionDef.getClassname();
      nodeAction = createGuiOp( nodeActionDef.getID(), rowActionClassname, nodeActionDef.createIter(), nodeActionDef.invokeIter(), nodeActionDef.getCommand(), ctrlGroup );
    }

    treeController.init( treeCtrlDef.getID(), ctrlGroup, treeViewPI, binding, treeBinding, nodeAction, readOnly, lazyLoading, false );

    return treeController;
  }

  protected Controller createTreeTableController (ControllerBuilder newBuilder, TreeTableControllerDef treeTableCtrlDef, ControllerGroup ctrlGroup) throws Exception {
    TreeTableController treeTableController;
    ModelIterator iter;
    Column colRef;
    TreeTableViewPI treeTableViewPI;
    ControllerType renderCtrlDef;
    ControllerType editCtrlDef;
    Lookup lookup;
    String classname;
    String displayValuePathStr = ".";
    Selection selection = null;
    boolean readOnly;
    boolean selectable;
    boolean lazyLoading;

    QName rowCompID = null;
    for (iter = treeTableCtrlDef.compIter(); iter.hasNext(); ) {
      Comp compRef = (Comp) iter.next();
      if (compRef.key() == ID_ROW) {
        rowCompID = compRef.getCompID();
      }
    }
    treeTableViewPI = getViewFactory().createTreeTableView( treeTableCtrlDef.getTableID(), treeTableCtrlDef.getModuleID(), treeTableCtrlDef.getRowColorID(), treeTableCtrlDef.getColorColumnID());
    if (treeTableViewPI == null) {
      throw new IllegalArgumentException("TreeTableController ID="+treeTableCtrlDef.key()+": table not found, ID="+treeTableCtrlDef.getTableID() );
    }


    classname = treeTableCtrlDef.getClassname();
    if ((classname == null) || (classname.length() == 0)) {
      treeTableController = new DefaultTreeTableController();
    }
    else {
      treeTableController = (TreeTableController) Class.forName(classname).newInstance();
    }



    Boolean ro = treeTableCtrlDef.getReadOnly();
    if (ro != null) readOnly = ro.booleanValue();
    else readOnly = false;

    Boolean lazy = treeTableCtrlDef.getLazyLoading();
    if(lazy != null) lazyLoading = lazy.booleanValue();
    else lazyLoading = true; // default is true

    Boolean sel = treeTableCtrlDef.getSelectable();
    if(sel!=null){
      selectable = sel.booleanValue();
    }
    else{
      selectable = true;
    }

    NamespaceTable namespaceTable = treeTableCtrlDef.namespaceTable();

    //TODO problem im Kommentar lösen, v.a. für Skin
    //--- We must initialize Table's Binding and Selection after adding column, because if all bindings for column lookup
    //    must be added to controllerGroup's binding list before table's selection is added. Otherwise the column selection
    //    is uninitialized when initializing the table rows
    lookup = treeTableCtrlDef.getLookup();
    if (lookup != null) {
      selection = createSelection( lookup, ctrlGroup, namespaceTable );
      displayValuePathStr = lookup.getValuePath();
    }
    // If a table has a binding it is used to store the key of the selected table row, not the table row itself
    Binding rowBinding = createAttributeBinding( ctrlGroup, treeTableCtrlDef.getDataSource(), treeTableCtrlDef.getModel(), treeTableCtrlDef.getValueID(), namespaceTable );

    ModelBinding treeBinding = null;
    lookup = treeTableCtrlDef.getLookup();

    if (lookup != null) {
      //selection = createSelection( lookup, ctrlGroup );
      //displayValuePathStr = lookup.getValuePath();
      treeBinding = createModelBinding( ctrlGroup, lookup.getDataSource(), lookup.getModel(), lookup.getValueID(), namespaceTable );
    }

    int colIndex = 0;
    for (iter=treeTableCtrlDef.columnIter(); iter.hasNext(); ) {
      colRef = (Column) iter.next();
      QName colName = colRef.getID();
      QName bodyCompID = colRef.getCompID();
      if (colName == null) colName = bodyCompID;

      Controller renderCtrl;
      renderCtrlDef = colRef.getRenderer();
      ActionDef cellActionDef = colRef.getCellAction();
      if (renderCtrlDef == null && cellActionDef == null) {
        TextController textController = new TextController();
        Binding attributeBinding = createAttributeBinding( ctrlGroup, treeTableController, colRef.getModel(), colRef.getValueID(), namespaceTable );
        TextViewPI textComp = getViewFactory().createTextView( bodyCompID, treeTableCtrlDef.getModuleID() );
        String inpMode = colRef.getInputMode();
        InputMode inputMode = inputModeFromString( inpMode );
        Object format = null;
        if (inputMode == null) {
          inputMode = InputMode.string;
          format = inpMode;
        }
        textController.init( bodyCompID, null, textComp, attributeBinding, inputMode, format, readOnly );
        renderCtrl = textController;
      }
      else {
        if (renderCtrlDef != null) {
          ValueController valueCtrl = (ValueController) createController( newBuilder, renderCtrlDef, null ); // FIXME: don'T add to any container; will be handled dynamically
          if (valueCtrl.getValueBinding() == null) {
            Binding attributeBinding = createAttributeBinding( ctrlGroup, treeTableController, colRef.getModel(), colRef.getValueID(), namespaceTable );
            valueCtrl.initValueBinding( attributeBinding );
          }
          renderCtrl = valueCtrl;
        }
        else {
          renderCtrl = createAction( cellActionDef, ctrlGroup );
        }
        QName labelValueID = colRef.getLabelValueID();
        if (labelValueID != null && renderCtrl instanceof FlagController) {
          Binding labelBinding = ((FlagController) renderCtrl).getLabelBinding();
          if (labelBinding == null) {
            labelBinding = createAttributeBinding( ctrlGroup, treeTableController, colRef.getModel(), labelValueID, namespaceTable );
            ((FlagController) renderCtrl).initLabelBinding( labelBinding );
          }
        }
      }

      ValueController editCtrl;
      editCtrlDef = colRef.getEditor();
      if (editCtrlDef == null) {
        editCtrl = null;
      }
      else {
        editCtrl = (ValueController) createController( newBuilder, editCtrlDef, null );
        if (editCtrl.getValueBinding() == null) {
          Binding attributeBinding = createAttributeBinding( ctrlGroup, treeTableController, colRef.getModel(), colRef.getValueID(), namespaceTable );
          editCtrl.initValueBinding( attributeBinding );
        }
        QName labelValueID = colRef.getLabelValueID();
        if (labelValueID != null && editCtrl instanceof FlagController) {
          Binding labelBinding = ((FlagController) editCtrl).getLabelBinding();
          if (labelBinding == null) {
            labelBinding = createAttributeBinding( ctrlGroup, treeTableController, colRef.getModel(), labelValueID, namespaceTable );
            ((FlagController) editCtrl).initLabelBinding( labelBinding );
          }
        }
      }

      String inputModeStr = colRef.getInputMode();
      InputMode inputMode = InputMode.fromString( inputModeStr );
      Object format = null;
      if (inputMode == null) {
        inputMode = InputMode.string;
        format = inputModeStr;
      }
      if (renderCtrl != null) {
        renderCtrl.setInputMode( inputMode, format );
      }

      treeTableController.addColumn( new ColumnInfo( ctrlGroup.getContext(), colIndex, colName, colRef.getModel(), displayValuePathStr,
          colRef.getValueID(), colRef.getIconValueID(), colRef.getHelpValueID(), colRef.getBackgroundValueID(),
          colRef.getHeadID(), bodyCompID, colRef.getLabelValueID(), namespaceTable, renderCtrl, editCtrl,
          getBoolean( colRef.getReadOnly() ), getBoolean( colRef.getSortable() ) ) );
      colIndex++;
    }

    for (Node nodeDef : treeTableCtrlDef.nodeIter()) {
      QName colName = nodeDef.getID();
      if (colName == null) colName = nodeDef.getCompID();

      QName inputMode = null;
      String inputModeStr = nodeDef.getInputMode();
      if ((inputModeStr != null) && (inputModeStr.length() > 0)) {
        inputMode = NAMESPACE.getQName( inputModeStr );
      }
      NodeType nodeType = new NodeType();
      nodeType.setID( nodeDef.getID() );
      nodeType.setCompID( nodeDef.getCompID() );
      nodeType.setModelTypeID( nodeDef.getModelTypeID() );
      nodeType.setValueID( nodeDef.getValueID() );
      nodeType.setIconValueID( nodeDef.getIconValueID() );
      nodeType.setEditFlagID( nodeDef.getEditFlagID() );
      nodeType.setEnabledFlagID( nodeDef.getEnabledFlagID() );
      nodeType.setRelationID( nodeDef.getRelationID() );
      nodeType.setInputMode( nodeDef.getInputMode() );
      treeTableController.addNodeType( nodeType );
    }

    GuiOperation nodeAction = null;
    ActionDef nodeActionDef = treeTableCtrlDef.getNodeAction();
    if (nodeActionDef != null) {
      String rowActionClassname = nodeActionDef.getClassname();
      nodeAction = createGuiOp( nodeActionDef.getID(), rowActionClassname, nodeActionDef.createIter(), nodeActionDef.invokeIter(), nodeActionDef.getCommand(), ctrlGroup );
    }

    /*
    GuiOperation rowAction = null;
    ActionDef rowActionDef = treeTableCtrlDef.getRowAction();
    if (rowActionDef != null) {
      String rowActionClassname = rowActionDef.getClassname();
      rowAction = createGuiOp( rowActionDef.getID(), rowActionClassname, rowActionDef.createIter(), rowActionDef.invokeIter(), rowActionDef.getCommand());
    }
    */

    treeTableController.init( treeTableCtrlDef.getID(), ctrlGroup, treeTableViewPI, rowBinding, treeBinding, nodeAction, readOnly, lazyLoading, false );

/*
    GuiOperation rowDoubleClickAction = null;
    ActionDef rowDoubleClickActionDef = treeTableCtrlDef.getRowDoubleClickAction();
    if (rowDoubleClickActionDef != null) {
      String rowDoubleClickActionClassname = rowDoubleClickActionDef.getClassname();
      rowDoubleClickAction = createGuiOp( rowDoubleClickActionDef.getID(), rowDoubleClickActionClassname, rowDoubleClickActionDef.createIter(), rowDoubleClickActionDef.invokeIter(), rowDoubleClickActionDef.getCommand());
      treeTableController.setRowDoubleClickAction(rowDoubleClickAction);
    }

    GuiOperation rowMenuAction = null;
    ActionDef rowMenuActionDef = treeTableCtrlDef.getRowMenuAction();
    if (rowMenuActionDef != null) {
      String rowMenuActionClassname = rowMenuActionDef.getClassname();
      rowMenuAction = createGuiOp( rowMenuActionDef.getID(), rowMenuActionClassname, rowMenuActionDef.createIter(), rowMenuActionDef.invokeIter(), rowMenuActionDef.getCommand());
      treeTableController.setRowMenuAction(rowMenuAction);
    }
*/


    return treeTableController;
  }

  protected Controller createProgressController( ControllerType ctrlDef, ControllerGroup ctrlGroup, QName typeID ) throws Exception {
    ProgressBarController progressBarController;
    ModelIterator iter;
    Comp compRef;
    ProgressBarViewPI progressComp = null;
    String classname;
    Namespace namespace;
    String nsStr;
    int minValue = 0; // FIXME: get from def
    int maxValue = 100;
    boolean readOnly;

    for (iter = ctrlDef.compIter(); iter.hasNext(); ) {
      compRef = (Comp) iter.next();
      if (compRef.key() == ID_PROGRESS) {
        progressComp = getViewFactory().createProgressView( compRef.getCompID(), compRef.getModuleID() );
      }
    }
    if (progressComp == null) {
      throw new IllegalArgumentException("Progress controller ID="+ctrlDef.key()+": progressComp not found");
    }

    nsStr = ctrlDef.getNamespace();
    if ((nsStr != null) && (nsStr.length() > 0)) {
      namespace = ctrlDef.namespaceTable().getByPrefix( nsStr );
    }
    else {
      namespace = null;
    }

    Boolean ro = ctrlDef.getReadOnly();
    if (ro != null) readOnly = ro.booleanValue();
    else readOnly = false;

    classname = ctrlDef.getClassname();
    if ((classname == null) || (classname.length() == 0)) {
      progressBarController = new ProgressBarController();
    }
    else {
      progressBarController = (ProgressBarController) Class.forName(classname).newInstance();
    }
    Binding binding = createAttributeBinding( ctrlGroup, ctrlDef.getDataSource(), ctrlDef.getModel(), ctrlDef.getValueID(), ctrlDef.namespaceTable() );
    if (binding == null) {
      progressBarController.init( ctrlDef.getID(), ctrlGroup, progressComp, readOnly, minValue, maxValue );
    }
    else {
      progressBarController.init( ctrlDef.getID(), ctrlGroup, progressComp, binding, readOnly, minValue, maxValue );
    }
    if (namespace != null) {
      progressBarController.setNamespace( namespace );
    }
    return progressBarController;
  }

  protected Controller createInputController( ControllerType ctrlDef, ControllerGroup ctrlGroup, QName typeID ) throws Exception {
    TextController textController;
    ModelIterator iter;
    Comp compRef;
    TextViewPI textComp = null;
    String classname;
    Namespace namespace;
    String nsStr;
    boolean readOnly;

    for (iter = ctrlDef.compIter(); iter.hasNext(); ) {
      compRef = (Comp) iter.next();
      if (compRef.key() == ID_TEXT) {
        textComp = getViewFactory().createTextView( compRef.getCompID(), compRef.getModuleID() );
      }
    }
    if (textComp == null) {
      throw new IllegalArgumentException("Input controller ID="+ctrlDef.key()+": comp with ID '"+ID_TEXT+"' is missing");
    }

    String inpMode = ctrlDef.getInputMode();
    InputMode inputMode = inputModeFromString( inpMode );
    Object format = null;
    if (inputMode == null) {
      inputMode = InputMode.string;
      format = inpMode;
    }
    Boolean ro = ctrlDef.getReadOnly();
    if (ro != null) readOnly = ro.booleanValue();
    else readOnly = false;

    nsStr = ctrlDef.getNamespace();
    if ((nsStr != null) && (nsStr.length() > 0)) {
      namespace = ctrlDef.namespaceTable().getByPrefix( nsStr );
    }
    else {
      namespace = null;
    }

    classname = ctrlDef.getClassname();
    if ((classname == null) || (classname.length() == 0)) {
      textController = newTextController( typeID );
    }
    else {
      textController = (TextController) Class.forName(classname).newInstance();
    }
    Binding binding = createAttributeBinding( ctrlGroup, ctrlDef.getDataSource(), ctrlDef.getModel(), ctrlDef.getValueID(), ctrlDef.namespaceTable() );
    textController.init( ctrlDef.getID(), ctrlGroup, textComp, binding, inputMode, format, readOnly );
    if (namespace != null) {
      textController.setNamespace( namespace );
    }
    return textController;
  }

  private InputMode inputModeFromString( String inpMode ) {
    if ((inpMode != null) && (inpMode.length() > 0)) {
      InputMode inputMode = InputMode.fromString( inpMode.toLowerCase() );
      return inputMode;
    }
    else {
      return InputMode.string;
    }
  }

  protected Controller createDateController( ControllerType ctrlDef, ControllerGroup ctrlGroup, QName typeID ) throws Exception {
    DateController dateController;
    ModelIterator iter;
    Comp compRef;
    DateViewPI dateComp = null;
    InputMode inputMode = InputMode.date;
    Object format = null;
    String inpMode;
    String classname;
    Namespace namespace;
    String nsStr;
    boolean readOnly;

    for (iter = ctrlDef.compIter(); iter.hasNext(); ) {
      compRef = (Comp) iter.next();
      if (compRef.key() == ID_TEXT) {
        dateComp = getViewFactory().createDateView( compRef.getCompID(), compRef.getModuleID() );
      }
    }
    if (dateComp == null) {
      throw new IllegalArgumentException("Input controller ID="+ctrlDef.key()+": dateComp not found");
    }

    inpMode = ctrlDef.getInputMode();
    if ((inpMode != null) && (inpMode.length() > 0)) {
      inpMode = inpMode.toUpperCase();
      if (inpMode.equals( "NONE" )) inputMode = InputMode.none;
      else if (inpMode.equals( "STRING" )) inputMode = InputMode.string;
      else if (inpMode.equals( "DATE" )) inputMode = InputMode.date;
      else {
        inputMode = InputMode.date;
        format = inpMode;
      }
    }
    Boolean ro = ctrlDef.getReadOnly();
    if (ro != null) readOnly = ro.booleanValue();
    else readOnly = false;

    nsStr = ctrlDef.getNamespace();
    if ((nsStr != null) && (nsStr.length() > 0)) {
      namespace = ctrlDef.namespaceTable().getByPrefix( nsStr );
    }
    else {
      namespace = null;
    }

    classname = ctrlDef.getClassname();
    if ((classname == null) || (classname.length() == 0)) {
      dateController = new DateController();
    }
    else {
      dateController = (DateController) Class.forName(classname).newInstance();
    }
    Binding binding = createAttributeBinding( ctrlGroup, ctrlDef.getDataSource(), ctrlDef.getModel(), ctrlDef.getValueID(), ctrlDef.namespaceTable() );
    if (binding == null) {
      dateController.init( ctrlDef.getID(), ctrlGroup, dateComp, inputMode, format, readOnly );
    }
    else {
      dateController.init( ctrlDef.getID(), ctrlGroup, dateComp, binding, inputMode, format, readOnly );
    }
    if (namespace != null) {
      dateController.setNamespace( namespace );
    }
    return dateController;
  }

  protected Controller createTextEditor( ControllerType ctrlDef, ControllerGroup ctrlGroup, QName typeID ) throws Exception {
    TextEditorController textEditor;
    ModelIterator iter;
    Comp compRef;
    TextEditorViewPI textArea = null;
    String classname;
    Namespace namespace;
    String nsStr;
    boolean readOnly;

    for (iter = ctrlDef.compIter(); iter.hasNext(); ) {
      compRef = (Comp) iter.next();
      if (compRef.key() == ID_TEXT) {
        textArea = getViewFactory().createTextEditorView( compRef.getCompID(), compRef.getModuleID() );
      }
    }
    if (textArea == null) {
      throw new IllegalArgumentException("TextEditor ID="+ctrlDef.key()+": textArea not found");
    }

    nsStr = ctrlDef.getNamespace();
    if ((nsStr != null) && (nsStr.length() > 0)) {
      namespace = ctrlDef.namespaceTable().getByPrefix( nsStr );
    }
    else {
      namespace = null;
    }

    Boolean ro = ctrlDef.getReadOnly();
    if (ro != null) readOnly = ro.booleanValue();
    else readOnly = false;

    classname = ctrlDef.getClassname();
    if ((classname == null) || (classname.length() == 0)) {
      textEditor = new TextEditorController();
    }
    else {
      textEditor = (TextEditorController) Class.forName(classname).newInstance();
    }
    Binding binding = createAttributeBinding( ctrlGroup, ctrlDef.getDataSource(), ctrlDef.getModel(), ctrlDef.getValueID(), ctrlDef.namespaceTable() ) ;
    if (binding == null) {
      textEditor.init( ctrlDef.getID(), ctrlGroup, textArea, readOnly );
    }
    else {
      textEditor.init( ctrlDef.getID(), ctrlGroup, textArea, binding, readOnly );
    }
    if (namespace != null) {
      textEditor.setNamespace( namespace );
    }
    return textEditor;
  }

  protected Controller createCodeEditor( ControllerType ctrlDef, ControllerGroup ctrlGroup, QName typeID ) throws Exception {
    CodeEditorController codeEditor;
    ModelIterator iter;
    Comp compRef;
    CodeEditorViewPI codeArea = null;
    String classname;
    Namespace namespace;
    String nsStr;
    boolean readOnly;

    for (iter = ctrlDef.compIter(); iter.hasNext(); ) {
      compRef = (Comp) iter.next();
      if (compRef.key() == ID_TEXT) {
        codeArea = getViewFactory().createCodeEditorView( compRef.getCompID(), compRef.getModuleID() );
      }
    }
    if (codeArea == null) {
      throw new IllegalArgumentException("CodeEditor ID="+ctrlDef.key()+": codeArea not found");
    }

    nsStr = ctrlDef.getNamespace();
    if ((nsStr != null) && (nsStr.length() > 0)) {
      namespace = ctrlDef.namespaceTable().getByPrefix( nsStr );
    }
    else {
      namespace = null;
    }

    Boolean ro = ctrlDef.getReadOnly();
    if (ro != null) readOnly = ro.booleanValue();
    else readOnly = false;

    classname = ctrlDef.getClassname();
    if ((classname == null) || (classname.length() == 0)) {
      codeEditor = new CodeEditorController();
    }
    else {
      codeEditor = (CodeEditorController) Class.forName(classname).newInstance();
    }
    Binding binding = createAttributeBinding( ctrlGroup, ctrlDef.getDataSource(), ctrlDef.getModel(), ctrlDef.getValueID(), ctrlDef.namespaceTable() ) ;
    if (binding == null) {
      codeEditor.init( ctrlDef.getID(), ctrlGroup, codeArea, readOnly );
    }
    else {
      codeEditor.init( ctrlDef.getID(), ctrlGroup, codeArea, binding, readOnly );
    }
    if (namespace != null) {
      codeEditor.setNamespace( namespace );
    }
    return codeEditor;
  }

  protected Controller createWebView( ControllerType ctrlDef, ControllerGroup ctrlGroup, QName typeID ) throws Exception {
    WebViewController webViewController;
    ModelIterator iter;
    Comp compRef;
    WebViewPI webView = null;
    String classname;
    Namespace namespace;
    String nsStr;
    boolean readOnly;

    for (iter = ctrlDef.compIter(); iter.hasNext(); ) {
      compRef = (Comp) iter.next();
      if (compRef.key() == ID_TEXT) {
        webView = getViewFactory().createWebView( compRef.getCompID(), compRef.getModuleID() );
      }
    }
    if (webView == null) {
      throw new IllegalArgumentException("WebView ID="+ctrlDef.key()+": webView not found");
    }

    nsStr = ctrlDef.getNamespace();
    if ((nsStr != null) && (nsStr.length() > 0)) {
      namespace = ctrlDef.namespaceTable().getByPrefix( nsStr );
    }
    else {
      namespace = null;
    }

    Boolean ro = ctrlDef.getReadOnly();
    if (ro != null) readOnly = ro.booleanValue();
    else readOnly = false;

    classname = ctrlDef.getClassname();
    if ((classname == null) || (classname.length() == 0)) {
      webViewController = new WebViewController();
    }
    else {
      webViewController = (WebViewController) Class.forName(classname).newInstance();
    }
    Binding binding = createAttributeBinding( ctrlGroup, ctrlDef.getDataSource(), ctrlDef.getModel(), ctrlDef.getValueID(), ctrlDef.namespaceTable() ) ;
    if (binding == null) {
      webViewController.init( ctrlDef.getID(), ctrlGroup, webView, readOnly );
    }
    else {
      webViewController.init( ctrlDef.getID(), ctrlGroup, webView, binding, readOnly );
    }
    if (namespace != null) {
      webViewController.setNamespace( namespace );
    }
    return webViewController;
  }

  protected Controller createHTMLEditor( ControllerType ctrlDef, ControllerGroup ctrlGroup, QName typeID ) throws Exception {
    HTMLEditorController htmlEditor;
    ModelIterator iter;
    Comp compRef;
    HTMLEditorViewPI htmlView = null;
    String inpMode;
    String classname;
    Namespace namespace;
    String nsStr;
    boolean readOnly;

    for (iter = ctrlDef.compIter(); iter.hasNext(); ) {
      compRef = (Comp) iter.next();
      if (compRef.key() == ID_TEXT) {
        htmlView = getViewFactory().createHTMLEditorView( compRef.getCompID(), compRef.getModuleID() );
      }
    }
    if (htmlView == null) {
      throw new IllegalArgumentException("HTMLEditor ID="+ctrlDef.key()+": htmlEditor not found");
    }

    nsStr = ctrlDef.getNamespace();
    if ((nsStr != null) && (nsStr.length() > 0)) {
      namespace = ctrlDef.namespaceTable().getByPrefix( nsStr );
    }
    else {
      namespace = null;
    }

    Boolean ro = ctrlDef.getReadOnly();
    if (ro != null) readOnly = ro.booleanValue();
    else readOnly = false;

    classname = ctrlDef.getClassname();
    if ((classname == null) || (classname.length() == 0)) {
      htmlEditor = new HTMLEditorController();
    }
    else {
      htmlEditor = (HTMLEditorController) Class.forName(classname).newInstance();
    }
    Binding binding = createAttributeBinding( ctrlGroup, ctrlDef.getDataSource(), ctrlDef.getModel(), ctrlDef.getValueID(), ctrlDef.namespaceTable() ) ;
    if (binding == null) {
      htmlEditor.init( ctrlDef.getID(), ctrlGroup, htmlView, readOnly );
    }
    else {
      htmlEditor.init( ctrlDef.getID(), ctrlGroup, htmlView, binding, readOnly );
    }
    if (namespace != null) {
      htmlEditor.setNamespace( namespace );
    }
    return htmlEditor;
  }

  protected Controller createNodeController( ControllerType ctrlDef, ControllerGroup ctrlGroup ) throws Exception {
    NodeController nodeController;
    ModelIterator iter;
    Comp compRef;
    NodeViewPI nodeViewPI = null;
    String classname;

    for (iter = ctrlDef.compIter(); iter.hasNext(); ) {
      compRef = (Comp) iter.next();
      if (compRef.key() == ID_NODE) {
        nodeViewPI = getViewFactory().createNodeView( compRef.getCompID(), compRef.getModuleID() );
      }
    }
    if (nodeViewPI == null) {
      throw new IllegalArgumentException("Node ID="+ctrlDef.key()+": node not found");
    }

    classname = ctrlDef.getClassname();
    if ((classname == null) || (classname.length() == 0)) {
      nodeController = new NodeController();
    }
    else {
      nodeController = (NodeController) Class.forName(classname).newInstance();
    }

    nodeController.init( ctrlDef.getID(), ctrlGroup, nodeViewPI );

    return nodeController;
  }

  protected Controller createTabPaneController( ControllerType ctrlDef, ControllerGroup ctrlGroup ) throws Exception {
    TabPaneController tabPaneController;
    ModelIterator iter;
    Comp compRef;
    TabPaneViewPI tabPaneViewPI = null;
    String classname;

    for (iter = ctrlDef.compIter(); iter.hasNext(); ) {
      compRef = (Comp) iter.next();
      if (compRef.key() == ID_NODE) {
        tabPaneViewPI = getViewFactory().createTabPaneView( compRef.getCompID(), compRef.getModuleID() );
      }
    }
    if (tabPaneViewPI == null) {
      throw new IllegalArgumentException("Node ID="+ctrlDef.key()+": node not found");
    }

    classname = ctrlDef.getClassname();
    if ((classname == null) || (classname.length() == 0)) {
      tabPaneController = new TabPaneController();
    }
    else {
      tabPaneController = (TabPaneController) Class.forName(classname).newInstance();
    }

    tabPaneController.init( ctrlDef.getID(), ctrlGroup, tabPaneViewPI );

    return tabPaneController;
  }


  protected Controller createListController( ControllerBuilder newBuilder, ControllerType ctrlDef, MutableControllerGroup ctrlGroup ) throws Exception {
    ListController controller;
    ModelIterator iter;
    Comp compRef;
    Lookup lookup;
    Selection selection;
    Model model;
    String classname;
    String displayValuePathStr = null;
    QName keyAttr, valueID;
    QName rowCompID = null;
    boolean readOnly;

    Comp listCompRef = null;
    for (iter = ctrlDef.compIter(); iter.hasNext(); ) {
      compRef = (Comp) iter.next();
      if (compRef.key() == ID_SELECTOR) {
        listCompRef = compRef;
      }
      else if (compRef.key() == ID_ROW) {
        rowCompID = compRef.getCompID();
      }
    }
    if (listCompRef == null) {
      throw new IllegalArgumentException("AbstractSelectionController ID="+ctrlDef.key()+": selector not found");
    }

    lookup = ctrlDef.getLookup();
    if (lookup != null) {
      selection = createSelection( lookup, ctrlGroup, ctrlDef.namespaceTable() );
      keyAttr = lookup.getKeyAttr();
      valueID = lookup.getValueID();
      displayValuePathStr = lookup.getValuePath();
    }
    else {
      Logger.warn("Lookup missing for AbstractSelectionController ID="+ctrlDef.key());
      model = new DefaultModel( ControllerFactory.STRINGTABLE_TYPE.name(), ControllerFactory.STRINGTABLE_TYPE );
      keyAttr = KeyAndValue.ID_ID;
      valueID = KeyAndValue.ID_VALUE;
      SimpleModelSource modelSource = new SimpleModelSource( ctrlGroup.getContext(), model );
      selection = new SingleSelection( ctrlGroup.getContext(), ctrlDef.namespaceTable(), modelSource, StringTable.ID_STRINGENTRY, null, true );
    }

    XPath displayValuePath = null;
    if (displayValuePathStr != null) {
      displayValuePath = new XPath( ctrlDef.namespaceTable(), displayValuePathStr );
    }

    Boolean ro = ctrlDef.getReadOnly();
    if (ro != null) readOnly = ro.booleanValue();
    else readOnly = false;

    classname = ctrlDef.getClassname();
    if ((classname == null) || (classname.length() == 0)) {
      controller = new DefaultListController();
    }
    else {
      controller = (ListController) Class.forName(classname).newInstance();
    }
    ListViewPI selComp = getViewFactory().createListView( listCompRef.getCompID(), listCompRef.getModuleID(), rowCompID, controller );
    Binding binding = createAttributeBinding( ctrlGroup, ctrlDef.getDataSource(), ctrlDef.getModel(), ctrlDef.getValueID(), ctrlDef.namespaceTable() );
    controller.init( ctrlDef.getID(), ctrlGroup, selComp, binding, selection, keyAttr, displayValuePath, valueID, readOnly );

    //---- Add definition for detail module if existing
    if (ctrlDef.controllerDefCount() > 0) {
      /*ControllerType detailDef = ctrlDef.getControllerDef( null );
      Binding detailBinding = createModelBinding( ctrlGroup, controller, detailDef.getModel(), null, ctrlDef.namespaceTable() );
      Controller detailCtrl = createController( newBuilder, detailDef, ctrlGroup );
      detailBinding.setBindingListener( (BindingListener) detailCtrl );
      ((BindingListener) detailCtrl).setBinding( detailBinding );
      controller.initDetailController( detailCtrl );*/
      throw new IllegalArgumentException( "Deprecated" );
    }
    return controller;
  }

  protected Controller createEnumController( ControllerBuilder newBuilder, ControllerType ctrlDef, MutableControllerGroup ctrlGroup ) throws Exception {
    ListController controller;
    ModelIterator iter;
    Comp compRef;
    Lookup lookup;
    Selection selection;
    Model model;
    String displayValuePathStr = null;
    QName keyAttr, valueID;
    QName rowCompID = null;
    boolean readOnly;

    Comp listCompRef = null;
    for (iter = ctrlDef.compIter(); iter.hasNext(); ) {
      compRef = (Comp) iter.next();
      if (compRef.key() == ID_SELECTOR) {
        listCompRef = compRef;
      }
      else if (compRef.key() == ID_ROW) {
        rowCompID = compRef.getCompID();
      }
    }
    if (listCompRef == null) {
      throw new IllegalArgumentException("AbstractSelectionController ID="+ctrlDef.key()+": selector not found");
    }

    lookup = ctrlDef.getLookup();
    if (lookup != null) {
      throw new IllegalArgumentException("AbstractSelectionController ID="+ctrlDef.key()+": lookup must not be set");
    }

    Binding binding = createAttributeBinding( ctrlGroup, ctrlDef.getDataSource(), ctrlDef.getModel(), ctrlDef.getValueID(), ctrlDef.namespaceTable() );
    model = new SimpleStringTable();
    keyAttr = StringMap.ID_ID;
    valueID = StringMap.ID_VALUE;
    SimpleModelSource modelSource = new SimpleModelSource( ctrlGroup.getContext(), model );
    QName tableRelationID = SimpleStringTable.ID_STRINGMAPENTRY;
    selection = new EnumSelection( ctrlGroup.getContext(), ctrlDef.namespaceTable(), modelSource, tableRelationID, binding, null, true );

    XPath displayValuePath = null;
    if (displayValuePathStr != null) {
      displayValuePath = new XPath( ctrlDef.namespaceTable(), displayValuePathStr );
    }

    Boolean ro = ctrlDef.getReadOnly();
    if (ro != null) readOnly = ro.booleanValue();
    else readOnly = false;

    controller = new DefaultListController();

    ListViewPI selComp = getViewFactory().createListView( listCompRef.getCompID(), listCompRef.getModuleID(), rowCompID, controller );
    controller.init( ctrlDef.getID(), ctrlGroup, selComp, binding, selection, keyAttr, displayValuePath, valueID, readOnly );

    //---- Add definition for detail module if existing
    if (ctrlDef.controllerDefCount() > 0) {
      /*ControllerType detailDef = ctrlDef.getControllerDef( null );
      Binding detailBinding = createModelBinding( ctrlGroup, controller, detailDef.getModel(), null, ctrlDef.namespaceTable() );
      Controller detailCtrl = createController( newBuilder, detailDef, ctrlGroup );
      detailBinding.setBindingListener( (BindingListener) detailCtrl );
      ((BindingListener) detailCtrl).setBinding( detailBinding );
      controller.initDetailController( detailCtrl );*/
      throw new IllegalArgumentException( "Deprecated" );
    }
    return controller;
  }

  protected Selection createSelection( Lookup lookup, ControllerGroup ctrlGroup, NamespaceTable namespaceTable ) throws Exception {
    Filter filter = null;
    Selection selection;
    QName selType = lookup.getSelType();
    QName relationName = lookup.getRelationID();
    FilterDef filterdef = lookup.getFilterDef();
    if (filterdef != null) {
      String className = filterdef.getClassName();
      if ((className != null) && (className.length() > 0)) {
        filter = (Filter) Class.forName( className ).newInstance();
      }
      else {
        filter = new ModelBasedFilter();
      }
      QName dataSourceID = filterdef.getDataSource();
      ModelSource dataSource;
      if (dataSourceID == null) {
        dataSource = null;
      }
      else {
        dataSource = (ModelSource) ctrlGroup.getController( dataSourceID );
        if (dataSource == null) {
          throw new IllegalArgumentException( "Unknown data source ID=" + dataSourceID + " in dialog name=" + ctrlGroup.getName() );
        }
      }
      filter.init( dataSource );
    }


    if ((selType == SELECTION_SINGLE) || (selType == SELECTION_MAX_ONE)) {
      boolean emptyAllowed = (selType == SELECTION_MAX_ONE);
      selection = new SingleSelection( relationName, emptyAllowed );
    }
    else {
      boolean emptyAllowed = (selType == SELECTION_MULTIPLE);
      selection = new MultiSelection( relationName, emptyAllowed );
    }
    selection.setListBinding( createModelListBinding( ctrlGroup, lookup.getDataSource(), lookup.getModel(), lookup.getRelationID(), filter, namespaceTable ) );
    return selection;
  }

  public Selection createSelection( QName relationID, String modelPath, SelectionType selType, ControllerGroup ctrlGroup, NamespaceTable namespaceTable ) {
    Selection selection;

    if ((selType == SelectionType.SINGLE) || (selType == SelectionType.MAX_ONE)) {
      boolean emptyAllowed = (selType == SelectionType.MAX_ONE);
      selection = new SingleSelection( relationID, emptyAllowed );
    }
    else {
      boolean emptyAllowed = (selType == SelectionType.MULTIPLE);
      selection = new MultiSelection( relationID, emptyAllowed );
    }
    selection.setListBinding( createModelListBinding( ctrlGroup, null, modelPath, relationID, null, namespaceTable ) );
    return selection;
  }

  protected Controller createFlagController( ControllerType ctrlDef, ControllerGroup ctrlGroup ) {
    FlagController controller = new FlagController();
    FlagViewPI checkbox;
    boolean readOnly;

    Comp comp = ctrlDef.compIter().next();
    checkbox = getViewFactory().createFlagView( comp.getCompID(), comp.getModuleID(), false );
    if (checkbox == null) {
      throw new IllegalArgumentException("FlagController ID="+ctrlDef.key()+": comp "+comp.getCompID()+" not found");
    }

    String format = ctrlDef.getInputMode();

    Boolean ro = ctrlDef.getReadOnly();
    if (ro != null) readOnly = ro.booleanValue();
    else readOnly = false;

    Binding valueBinding = createAttributeBinding( ctrlGroup, ctrlDef.getDataSource(), ctrlDef.getModel(), ctrlDef.getValueID(), ctrlDef.namespaceTable() );
    Binding labelBinding = createAttributeBinding( ctrlGroup, ctrlDef.getDataSource(), ctrlDef.getModel(), ctrlDef.getLabelValueID(), ctrlDef.namespaceTable() );
    controller.init( ctrlDef.getID(), ctrlGroup, valueBinding, labelBinding, checkbox, format, readOnly );
    return controller;
  }

  protected PaintController createPaintController( ControllerType ctrlDef, ControllerGroup ctrlGroup ) {
    PaintController controller = new PaintController();
    PaintViewPI paintViewPI = null;
    ViewPI labelComp = null;
    boolean readOnly;
    Comp comp = ctrlDef.compIter().next();
    for (ModelIterator iter = ctrlDef.compIter(); iter.hasNext(); ) {
      Comp compRef = (Comp) iter.next();
      if (compRef.key() == ID_PAINT) {
        paintViewPI = getViewFactory().createPaintView( compRef.getCompID(), compRef.getModuleID() );
      }
    }
    if (paintViewPI == null) {
      throw new IllegalArgumentException("PaintController ID="+ctrlDef.key()+": comp "+comp.getCompID()+" not found");
    }
    Boolean ro = ctrlDef.getReadOnly();
    if (ro != null) readOnly = ro.booleanValue();
    else readOnly = false;

    Binding binding = createAttributeBinding( ctrlGroup, ctrlDef.getDataSource(), ctrlDef.getModel(), ctrlDef.getValueID(), ctrlDef.namespaceTable() );
    controller.init( ctrlDef.getID(), ctrlGroup, binding, paintViewPI );

    return controller;
  }

  protected Controller createImageController( ControllerType ctrlDef, ControllerGroup ctrlGroup ) {
    ImageController controller = new ImageController();
    ImageViewPI imageViewPI;

    Comp comp = ctrlDef.compIter().next();
    imageViewPI = getViewFactory().createImageView( comp.getCompID(), comp.getModuleID() );
    if (imageViewPI == null) {
      throw new IllegalArgumentException("ImageController ID="+ctrlDef.key()+": comp "+comp.getCompID()+" not found");
    }

    Binding binding = createAttributeBinding( ctrlGroup, ctrlDef.getDataSource(), ctrlDef.getModel(), ctrlDef.getValueID(), ctrlDef.namespaceTable() );
    controller.init( ctrlDef.getID(), ctrlGroup, binding, imageViewPI );
    return controller;
  }

  protected Controller createCustomController(ControllerBuilder newBuilder,  ControllerType ctrlDef, MutableControllerGroup ctrlGroup ) {
    CustomController ctrl;
    try {
      ctrl = (CustomController) Class.forName( ctrlDef.getClassname() ).newInstance();
    }
    catch (Exception e) {
      throw new IllegalArgumentException( "Error creating custom controller, id="+ctrlDef.getID(), e );
    }
    QName compID = ctrlDef.getComp( null ).getCompID();
    ctrl.init( ctrlDef.getID(), ctrlGroup, compID, newBuilder );
    return ctrl;
  }

  // TODO??? public abstract TaskHandler createTaskHandler( ProgressTask progressTask );

  /**
   * Start...
   *
   * @param newBuilder
   * @param dialogID
   * @param context
   * @param dialog
   * @param parentDlgCtrl
   * @return
   */
  public DialogController createDialogController( ControllerBuilder newBuilder, QName dialogID, Context context, Dialog dialog, DialogController parentDlgCtrl ) {
    if (this.app == null) {
      throw new IllegalArgumentException("Application not initialized - use loadGui()");
    }
    if (dialogID == null) {
      throw new IllegalArgumentException("dialogID must not be null");
    }
    Model dialogDef = this.app.get( null, dialogID );
    if (dialogDef instanceof DialogDef) {
      return createDialogController( newBuilder, (DialogDef) dialogDef, context, dialog, parentDlgCtrl );
    }
    else if (dialogDef instanceof Module) {
      return createDialogController( newBuilder, (Module) dialogDef, context, dialog, parentDlgCtrl );
    }
    else {
      throw new IllegalArgumentException( "Unsupported class for dialog definition, dialogID="+dialogID+", type="+dialogDef.getClass() );
    }
  }

  /**
   * Create/load new ModuleGroup having ID moduleID
   * @param moduleID         ID of the module to be loaded
   * @param moduleController controller to load module into
   * @param modelSource      optional model data source for new ModuleGroup's model - must be null if ModuleGroup creates its own model
   * @param modelPath        optional path to ModuleGroup's model
   * @return                 the new ModuleGroup
   * @throws Exception
   */
  public ModuleGroup createModuleGroup( ControllerBuilder newBuilder, QName moduleID, ModuleController moduleController, ModelSource modelSource, String modelPath ) throws Exception {
    if (this.app == null) {
      throw new IllegalArgumentException("Application not initialized - use loadGui()");
    }
    if (moduleID == null) {
      throw new IllegalArgumentException("moduleID must not be null");
    }
    Module moduleDef = this.app.getModule( moduleID );
    return createModuleGroup( newBuilder, moduleDef, moduleController, modelSource, modelPath );
  }

  private DialogController createDialogController( ControllerBuilder newBuilder, Module module, Context context, de.pidata.gui.event.Dialog dialog, DialogController parentDlgCtrl ) {
    try {
      // DialogClassName is used for Activity class, so always create an Android Controller
      DialogController dlgController = new AbstractDialogController( context, dialog, parentDlgCtrl );

      DialogControllerDelegate delegate = null;
      String delegateClassName = module.getDelegate();
      if ((delegateClassName != null) && (delegateClassName.length() > 0)) {
        try {
          delegate = (DialogControllerDelegate) Class.forName( delegateClassName ).newInstance();
        }
        catch (Exception ex) {
          throw new IllegalArgumentException("Could not create DialogControllerDelegate class: "+delegateClassName
              +", reason: "+ex.getMessage());
        }
      }

      //----- fetch model
      String modelPath = ".";
      boolean fullScreen = true;
      dlgController.init( module.getName(), null, fullScreen, newBuilder, context, delegate, modelPath, module.namespaceTable() );
      createDlgControllers( newBuilder, module.iterator( null, null ), dlgController );
      dlgController.created();
      return dlgController;
    }
    catch (Exception ex) {
      Logger.error( "Error creating dialog ID=" + module.key(), ex );
      return null;
    }
  }

  public DialogController createDialogController( ControllerBuilder newBuilder, DialogDef dialogDef, Context context, de.pidata.gui.event.Dialog dialog, DialogController parentDlgCtrl ) {
    try {
      // DialogClassName is used for Activity class, so always create an Android Controller
      DialogController dlgController = new AbstractDialogController( context, dialog, parentDlgCtrl );

      DialogControllerDelegate delegate = null;
      String delegateClassName = dialogDef.getDelegate();
      if ((delegateClassName != null) && (delegateClassName.length() > 0)) {
        try {
          delegate = (DialogControllerDelegate) Class.forName( delegateClassName ).newInstance();
        }
        catch (Exception ex) {
          throw new IllegalArgumentException("Could not create DialogControllerDelegate class: "+delegateClassName
              +", reason: "+ex.getMessage());
        }
      }

      //----- fetch model
      String modelPath = dialogDef.getModel();
      boolean fullScreen = getBoolean( dialogDef.getFullScreen() );
      boolean cloneModel = getBoolean( dialogDef.getCloneModel() );
      dlgController.init( dialogDef.getID(), dialogDef.getTitle(), fullScreen, newBuilder, context, delegate, modelPath, dialogDef.namespaceTable() );
      createDlgControllers( newBuilder, dialogDef.iterator( null, null ), dlgController );
      dlgController.created();
      return dlgController;
    }
    catch (Exception ex) {
      Logger.error( "Error creating dialog ID=" + dialogDef.key(), ex );
      return null;
    }
  }

  /**
   * Shows a dialog containing a message and a progress bar. Content of message and progress bar
   * can be changed via ProgressController. The handle to the open dialog can also be obtained
   * from ProgressController
   *
   * @param title     the dialog title
   * @param message   the messsage, use CR (\n) for line breaks
   * @param minValue the value representing 0%
   * @param maxValue   the value representing 100%
   * @param cancelable true if progress dialog can be canceled by user
   * @param parentDlgCtrl the parent DialogController
   * @return the ProgressController
   */
//  TODO: public abstract ProgressController createProgressDialog( String title, String message, int minValue, int maxValue,
//                                                           boolean cancelable, DialogController parentDlgCtrl );
//
//
}
