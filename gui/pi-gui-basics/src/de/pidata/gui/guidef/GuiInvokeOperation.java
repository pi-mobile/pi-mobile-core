/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.guidef;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.ControllerGroup;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.controller.base.MutableControllerGroup;
import de.pidata.log.Logger;
import de.pidata.service.base.ServiceException;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.models.tree.ModelIterator;
import de.pidata.qnames.QName;

import java.util.Hashtable;
import java.util.Vector;

public class GuiInvokeOperation extends GuiOperation {

  private Vector invokeList;
  private Vector createList;

  public void init( QName eventID, Vector createList, Vector invokeList, String command, ControllerGroup controllerGroup ) {
    super.init( eventID, command, controllerGroup );
    this.invokeList = invokeList;
    this.createList = createList;
  }

  /**
   * Called to execute this operation
   *
   * @param eventID
   * @param sourceCtrl     original source of the event may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    PartType partDef;
    Model message;
    CreateType createType;
    InvokeType invoke;
    MutableControllerGroup eventGroup = sourceCtrl.getControllerGroup();
    Context context = eventGroup.getDialogController().getContext();
    Model dlgModel = eventGroup.getModel();
    QName msgTypeName;
    Hashtable variables = new Hashtable();
    QName serviceID = null;
    QName operationID = null;

    try {
      if (createList != null) {
        for (int i = 0; i < createList.size(); i++) {
          createType = (CreateType) createList.elementAt(i);
          dlgModel = processCreate( dlgModel, context, createType );
        }
      }
      if ((invokeList == null) || (invokeList.size() == 0)) {
        if (eventID != null) {
          BuiltinOperation builtinOperation = BuiltinOperation.fromString( eventID.getName() );
          if (builtinOperation != null) {
            if (builtinOperation == BuiltinOperation.openDialog) {
              executeBuiltin( eventGroup.getDialogController(), null, builtinOperation );
            }
            else {
              openChildDialog( eventGroup.getDialogController(), null, eventID ); // TODO???
            }
          }
        }
      }
      else {
        for (int i = 0; i < invokeList.size(); i++) {
          invoke = (InvokeType) invokeList.elementAt(i);
          serviceID = invoke.getService();
          operationID = invoke.getOperation();
          if (serviceID == null) {
            // call gui builtin like ok, cancel, open child dialog...
            msgTypeName = invoke.getMessageType();
            if (msgTypeName != null) {
              message = ModelFactoryTable.getInstance().getFactory( msgTypeName.getNamespace() ).createInstance(msgTypeName);
              for (ModelIterator inputIter = invoke.inputIter(); inputIter.hasNext(); ) {
                partDef = (PartType) inputIter.next();
                Model model = GuiOperation.fetchModel(partDef, eventGroup, variables);
                message.add(partDef.getID(), model);
              }
            }
            else {
              PartType input = invoke.getInput(null);
              if (input == null) {
                message = null;
              }
              else {
                message = GuiOperation.fetchModel(input, eventGroup, variables);
              }
            }
            BuiltinOperation builtinOperation = BuiltinOperation.fromString( operationID.getName() );
            if (builtinOperation == null) {
              openChildDialog( eventGroup.getDialogController(), message, operationID );
            }
            else {
              if (builtinOperation == BuiltinOperation.openDialog){
                // so kanns net gehen: openChildDialog( eventGroup.getDialogController(), message, operationID );
                // wo soll denn die Dialog-ID herkommen?
                throw new RuntimeException( "TODO" );
              }
              else {
                executeBuiltin( eventGroup.getDialogController(), message, builtinOperation );
              }
            }
          }
          else {
            dlgModel = GuiOperation.processInvoke( eventGroup, invoke, dlgModel, variables);
          }
        }
      }
    }
    catch (Exception ex) {
      String msg = "Could not execute, last serviceID="+serviceID+" opID="+operationID;
      Logger.error(msg, ex);
      throw new ServiceException(ServiceException.SERVICE_FAILED, msg, ex);
    }
  }
}
