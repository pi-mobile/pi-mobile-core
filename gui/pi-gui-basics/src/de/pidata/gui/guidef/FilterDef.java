// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.gui.guidef;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.SequenceModel;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.lang.String;
import java.util.Hashtable;

public class FilterDef extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "de.pidata.gui" );

  public static final QName ID_CLASSNAME = NAMESPACE.getQName("className");
  public static final QName ID_DATASOURCE = NAMESPACE.getQName("dataSource");

  public FilterDef() {
    super( null, ControllerFactory.FILTERDEF_TYPE, null, null, null );
  }

  public FilterDef(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, ControllerFactory.FILTERDEF_TYPE, attributeNames, anyAttribs, childNames);
  }

  protected FilterDef(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the attribute className.
   *
   * @return The attribute className
   */
  public String getClassName() {
    return (String) get( ID_CLASSNAME );
  }

  /**
   * Set the attribute className.
   *
   * @param className new value for attribute className
   */
  public void setClassName( String className ) {
    set( ID_CLASSNAME, className );
  }

  /**
   * Returns the attribute dataSource.
   *
   * @return The attribute dataSource
   */
  public QName getDataSource() {
    return (QName) get( ID_DATASOURCE );
  }

  /**
   * Set the attribute dataSource.
   *
   * @param dataSource new value for attribute dataSource
   */
  public void setDataSource( QName dataSource ) {
    set( ID_DATASOURCE, dataSource );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
