// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.gui.guidef;

import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.lang.String;

public enum SelectionType {

  MAX_ONE( "MAX_ONE" ),
  SINGLE( "SINGLE" ),
  MULTIPLE( "MULTIPLE" ),
  MIN_ONE( "MIN_ONE" );

  private String value;

  SelectionType( String value ) {
    this.value = value;
  }

  /**
   * Returns the String value of the SelectionType instance.
   *
   * @return The String value of the SelectionType instance.
   */
  public String getValue() {
    return this.value;
  }

  /**
   * Returns the SelectionType instance associated with the given String value.
   *
   * @param stringValue The value to associate with an SelectionType instance
   * @return The SelectionType instance associated with the given String value.
   */
  public static SelectionType fromString( String stringValue ) {
    if (stringValue == null) {
      return null;
    }
    for (SelectionType entry : values()) {
      if (entry.value.equals( stringValue )) {
        return entry;
      }
    }
    return null;
  }

  /**
   * Returns the name of this enum constant as declared in the schema.
   *
   * @return The name of this enum constant as declared in the schema.
   */
  @Override
  public String toString() {
    return value;
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
