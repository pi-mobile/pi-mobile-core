// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.gui.guidef;

import de.pidata.gui.controller.base.*;
import de.pidata.gui.view.base.ModuleViewPI;
import de.pidata.gui.view.base.TextViewPI;
import de.pidata.gui.view.base.ViewAnimation;
import de.pidata.models.binding.Binding;
import de.pidata.models.binding.ModelBinding;
import de.pidata.models.tree.*;
import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.DefaultModel;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.string.Helper;
import java.lang.Boolean;
import java.lang.String;
import java.util.Collection;
import java.util.Hashtable;

public class ModuleCtrlType extends CtrlContainerType {

  public static final Namespace NAMESPACE = Namespace.getInstance( "de.pidata.gui" );

  public static final QName ID_CLASSNAME = NAMESPACE.getQName("classname");
  public static final QName ID_CONTAINERCOMPID = NAMESPACE.getQName("containerCompID");
  public static final QName ID_ID = NAMESPACE.getQName("ID");
  public static final QName ID_MODELBINDING = NAMESPACE.getQName("modelBinding");
  public static final QName ID_MODULEID = NAMESPACE.getQName("moduleID");
  public static final QName ID_NAMESPACE = NAMESPACE.getQName("namespace");
  public static final QName ID_READONLY = NAMESPACE.getQName("readOnly");

  public ModuleCtrlType() {
    super( null, ControllerFactory.MODULECTRLTYPE_TYPE, null, null, null );
  }

  public ModuleCtrlType(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, ControllerFactory.MODULECTRLTYPE_TYPE, attributeNames, anyAttribs, childNames);
  }

  protected ModuleCtrlType(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the attribute ID.
   *
   * @return The attribute ID
   */
  public QName getID() {
    return (QName) get( ID_ID );
  }

  /**
   * Set the attribute ID.
   *
   * @param iD new value for attribute ID
   */
  public void setID( QName iD ) {
    set( ID_ID, iD );
  }

  /**
   * Returns the attribute classname.
   *
   * @return The attribute classname
   */
  public String getClassname() {
    return (String) get( ID_CLASSNAME );
  }

  /**
   * Set the attribute classname.
   *
   * @param classname new value for attribute classname
   */
  public void setClassname( String classname ) {
    set( ID_CLASSNAME, classname );
  }

  /**
   * Returns the attribute containerCompID.
   *
   * @return The attribute containerCompID
   */
  public QName getContainerCompID() {
    return (QName) get( ID_CONTAINERCOMPID );
  }

  /**
   * Set the attribute containerCompID.
   *
   * @param containerCompID new value for attribute containerCompID
   */
  public void setContainerCompID( QName containerCompID ) {
    set( ID_CONTAINERCOMPID, containerCompID );
  }

  /**
   * Returns the attribute namespace.
   * <p>default namespace for QName values</p>
   *
   * @return The attribute namespace
   */
  public String getNamespace() {
    return (String) get( ID_NAMESPACE );
  }

  /**
   * Set the attribute namespace.
   * <p>default namespace for QName values</p>
   *
   * @param namespace new value for attribute namespace
   */
  public void setNamespace( String namespace ) {
    set( ID_NAMESPACE, namespace );
  }

  /**
   * Returns the attribute moduleID.
   * <p>If present: the ID of the module which will be loaded into this container.</p>
   *
   * @return The attribute moduleID
   */
  public QName getModuleID() {
    return (QName) get( ID_MODULEID );
  }

  /**
   * Set the attribute moduleID.
   * <p>If present: the ID of the module which will be loaded into this container.</p>
   *
   * @param moduleID new value for attribute moduleID
   */
  public void setModuleID( QName moduleID ) {
    set( ID_MODULEID, moduleID );
  }

  /**
   * Returns the attribute readOnly.
   *
   * @return The attribute readOnly
   */
  public Boolean getReadOnly() {
    return (Boolean) get( ID_READONLY );
  }

  /**
   * Set the attribute readOnly.
   *
   * @param readOnly new value for attribute readOnly
   */
  public void setReadOnly( Boolean readOnly ) {
    set( ID_READONLY, readOnly );
  }

  /**
   * Returns the element modelBinding.
   *
   * @return The element modelBinding
   */
  public ModelBindingType getModelBinding() {
    return (ModelBindingType) get( ID_MODELBINDING, null );
  }

  /**
   * Set the element modelBinding.
   *
   * @param modelBinding new value for element modelBinding
   */
  public void setModelBinding( ModelBindingType modelBinding ) {
    setChild( ID_MODELBINDING, modelBinding );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  public ModuleController createController( ControllerBuilder builder, MutableControllerGroup ctrlGroup ) throws Exception {
    ModuleController moduleController;
    String classname = getClassname();
    if ((classname == null) || (classname.length() == 0)) {
      moduleController = new ModuleController();
    }
    else {
      moduleController = (ModuleController) Class.forName(classname).newInstance();
    }

    QName containerCompID = getContainerCompID();
    if (containerCompID == null) {
      throw new IllegalArgumentException( "Module controller ID=" + getID() + ": comp is missing" );
    }
    ModuleViewPI moduleViewPI = builder.getViewFactory().createModuleView( containerCompID );

    ModelBindingType bindingDef = getModelBinding();
    ModelBinding binding = null;
    if (bindingDef != null) {
      binding = bindingDef.createModelBinding( ctrlGroup, namespaceTable() );
    }

    QName moduleID = getModuleID();
    moduleController.init( getID(), ctrlGroup, moduleViewPI, binding, moduleID, readOnly() );

    if (ctrlCount() > 0) {
      if (moduleID == null) {
        moduleID = getID();
      }
      ModuleGroup moduleGroup = new ModuleGroup();
      ModelBinding moduleBinding = new ModelBinding( ctrlGroup.getContext(), moduleController, null, null );
      moduleGroup.init( moduleID, moduleController, moduleBinding, moduleController.isReadOnly(), null );
      builder.addChildControllers( moduleGroup, this, moduleController.isReadOnly() );
      moduleController.setModuleGroup( moduleGroup, ViewAnimation.NONE );
    }
    else if (moduleID != null) {
      DialogModuleType dialogModuleDef = builder.app.getDialogModule( moduleID );
      ModuleGroup moduleGroup = dialogModuleDef.createController( builder, moduleController, moduleController, ".", namespaceTable() );
      moduleController.setModuleGroup( moduleGroup, ViewAnimation.NONE );
    }

    return moduleController;
  }

  /**
   * Returns the boolean value of the attribute {@link #ID_READONLY} which is represented by an
   * object {@link Boolean}. If the object is null, returns false.
   * @return true if the attribute object is not null and represents the value true; false otherwise
   */
  public boolean readOnly() {
    boolean readOnly = Helper.getBoolean( getReadOnly() );
    return readOnly;
  }
}
