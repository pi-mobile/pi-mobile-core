/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.guidef;

import de.pidata.models.config.Binding;
import de.pidata.models.config.Configurator;
import de.pidata.models.service.PortType;
import de.pidata.models.service.Service;
import de.pidata.models.service.ServiceManager;
import de.pidata.qnames.Namespace;

public abstract class GuiService implements Service {

  public static final Namespace NAMESPACE = Namespace.getInstance("de.pidata.gui");

  protected ControllerBuilder builder;
  protected PortType          portType;

  public void init( ServiceManager serviceMgr, Configurator configurator, Binding binding ) throws Exception {
    this.builder = (ControllerBuilder) configurator.getInstance( ControllerBuilder.NAMESPACE.getQName( "SkinDialogControllerBuilder") ); // TODO: ???
  }

  public ControllerBuilder getBuilder() {
    return builder;
  }

  public PortType portType() {
    return portType;
  }

  /**
   * Tells this service to shut down, e.g. close database connections
   */
  public void shutdown() {
    //To change body of implemented methods use File | Settings | File Templates.
  }
}
