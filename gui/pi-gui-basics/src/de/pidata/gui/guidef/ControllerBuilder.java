/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.guidef;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.component.base.ProgressTask;
import de.pidata.gui.component.base.TaskHandler;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.view.base.ViewAnimation;
import de.pidata.gui.view.base.ViewFactory;
import de.pidata.log.Logger;
import de.pidata.models.binding.AttributeBinding;
import de.pidata.models.binding.Binding;
import de.pidata.models.binding.ModelListBinding;
import de.pidata.models.binding.Selection;
import de.pidata.models.tree.*;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;

import java.util.Hashtable;

public abstract class ControllerBuilder {

  public static final Namespace NAMESPACE = Namespace.getInstance("de.pidata.gui");

  public static final QName ID_TEXTCONTROLLER = NAMESPACE.getQName("TextController");
  public static final QName ID_DATECONTROLLER = NAMESPACE.getQName("DateController");
  public static final QName ID_SELECTIONCONTROLLER = NAMESPACE.getQName("SelectionController");

  public static final QName ID_TEXT  = NAMESPACE.getQName("text");
  public static final QName ID_LABEL = NAMESPACE.getQName("label");
  public static final QName ID_SELECTOR = NAMESPACE.getQName("selector");
  public static final QName ID_BUTTON = NAMESPACE.getQName("button");

  /** Allow selecting none or one value */
  public static final QName SELECTION_MAX_ONE = NAMESPACE.getQName("MAX_ONE");

  private static final boolean DEBUG = false;
  private Hashtable controllerBuilders = new Hashtable();
  private GuiBuilder guiBuilder;
  protected Application app;

  private ViewFactory viewFactory;

  public void init( Application app, GuiBuilder guiBuilder ) {
    this.app = app;
    setGuiBuilder( guiBuilder );

    // TODO: for compatibility only...
    OldControllerBuilder.getInstance().init( app, guiBuilder );
  }

  public Application getApplication() {
    return app;
  }

  public ViewFactory getViewFactory() {
    if (viewFactory == null) {
      viewFactory = new ViewFactory();
    }
    return viewFactory;
  }

  public void setViewFactory( ViewFactory viewFactory ) {
    this.viewFactory = viewFactory;
  }

  /**
   * Returns the GUI definition for the dialog identified by the given dialogID.
   *
   * @param dialogID the ID of the dialog to find
   * @return the GUI definition for the dialog
   * @deprecated for compatibility with old GUI only...
   */
  @Deprecated
  public DialogDef getDialogDef( QName dialogID ) {
    return OldControllerBuilder.getInstance().getDialogDef( dialogID );
  }

  /**
   * Returns the GUI definition for the dialog identified by the given dialogID.
   * @param dialogID the ID of the dialog to find
   * @return the GUI definition for the dialog
   */
  public DialogType getDialogType( QName dialogID ) {
    return this.app.getDialog( dialogID );
  }

  /**
   * Builds a specific {@link Controller} as defined by the given {@link CtrlType}.
   *
   * @param ctrlDef the definition for the Controller given by the GUI definition file
   * @param ctrlGroup the Controller group which will contain the created Controller
   * @return the specific created Controller
   * @throws Exception
   */
  public Controller createController( CtrlType ctrlDef, MutableControllerGroup ctrlGroup ) throws Exception {
    if (ctrlDef instanceof ButtonCtrlType) {
      return ((ButtonCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof TextCtrlType) {
      return ((TextCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof PasswordCtrlType) {
      return ((PasswordCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof NumberCtrlType) {
      return ((NumberCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof IntegerCtrlType) {
      return ((IntegerCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof DateCtrlType) {
      return ((DateCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof FlagCtrlType) {
      return ((FlagCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof ImageCtrlType) {
      return ((ImageCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof TextEditorCtrlType) {
      return ((TextEditorCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof CodeEditorCtrlType) {
      return ((CodeEditorCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof WebViewCtrlType) {
      return ((WebViewCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof HtmlEditorCtrlType) {
      return ((HtmlEditorCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof PaintCtrlType) {
      return ((PaintCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof ChoiceCtrlType) {
      return ((ChoiceCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof EnumCtrlType) {
      return ((EnumCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof TableCtrlType) {
      return ((TableCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof TreeTableCtrlType) {
      return ((TreeTableCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof TreeCtrlType) {
      return ((TreeCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof ProgressCtrlType) {
      return ((ProgressCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof CustomCtrlType) {
      return ((CustomCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else if (ctrlDef instanceof TabPaneCtrlType) {
      return ((TabPaneCtrlType) ctrlDef).createController( this, ctrlGroup );
    }
    else {
      // TODO: add more...
      throw new IllegalArgumentException( "unsupported Controller type: " + ctrlDef.type().name() + " for: " + ctrlDef.getID() );
    }

    // TODO: work list only...
//    else if (typeID == ID_NODECONTROLLER) {
//      return createNodeController( ctrlDef, ctrlGroup );
//    }
//    else if (typeID == ID_TABGROUP) {
//      return createTabGroup( newBuilder, ctrlDef, ctrlGroup );
//    }
//    else {
//      return createOtherController( ctrlDef, ctrlGroup );
//    }
  }

  public void setGuiBuilder( GuiBuilder guiBuilder ) {
    this.guiBuilder = guiBuilder;
    if (guiBuilder != null) {
      guiBuilder.setModuleContainer( this.app );
    }
  }

  public GuiBuilder getGuiBuilder() {
    return guiBuilder;
  }

  public void addControllerType(QName controllerID, ControllerBuilder builder) {
    this.controllerBuilders.put(controllerID, builder);
  }

  /**
   * Creates the dialogController's child Controllers defined by the given GUI definition.
   * @param iter Iterator over the child Controller definitions
   * @param dlgController the DialogController which will contain the created Controllers
   */
  protected void createDlgControllers( ModelIterator iter, DialogController dlgController ) {
    while (iter.hasNext()) {
      Model model = iter.next();
      createOneController( model, dlgController );
    }
  }

  /**
   *
   * @param ctrl
   * @param moduleDef
   * @param readOnly
   * @deprecated use {@link #addChildControllers(MutableControllerGroup, CtrlContainerType, boolean)} instead
   */
  @Deprecated
  public void addChildControllers( MutableControllerGroup ctrl, Module moduleDef, boolean readOnly ) {
    ModelIterator iter;
    Model model;
    for (iter = moduleDef.iterator(null, null); iter.hasNext(); ) {
      model = iter.next();
      Controller childCtrl = createOneController( model, ctrl );
      if (readOnly && (childCtrl instanceof ValueController)) {
        ((ValueController) childCtrl).setReadOnly( true );
      }
    }
  }

  /**
   * Creates the controllers defined in the given module definition and adds them to the controller group.
   *
   * @param ctrl the controller group holding the module's child controllers; typically a {@link ModuleGroup}
   * @param moduleDef definition of the child controllers
   * @param readOnly
   */
  public void addChildControllers( MutableControllerGroup ctrl, CtrlContainerType moduleDef, boolean readOnly ) {
    for (Object model : moduleDef.iterator( null, null )) {
      if (model instanceof Model) {
        Controller childCtrl = createOneController( (Model) model, ctrl );
        if (readOnly && (childCtrl instanceof ValueController)) {
          ((ValueController) childCtrl).setReadOnly( true );
        }
      }
    }
  }

  /**
   * Builds a specific {@link Controller} as defined by the given model. The model should represent a {@link CtrlType}.
   * @param model the Controller definition from the GUI definition file.
   * @param ctrlGroup the Controller group which will contain the created Controller
   * @return the specific created Controller
   */
  protected Controller createOneController( Model model, MutableControllerGroup ctrlGroup ) {
    QName typeID;
    ControllerBuilder builder = null;
    Controller ctrl;
    QName ctrlID = null;
    try {
      if (model instanceof CtrlType) {
        // new GUI
        CtrlType ctrlDef = (CtrlType) model;
        ctrlID = ctrlDef.getID();
        typeID = ctrlDef.type().name();
        builder = (ControllerBuilder) controllerBuilders.get( typeID );
        if (builder == null) builder = this;
        ctrl = builder.createController( ctrlDef, ctrlGroup );
      }
      else if (model instanceof GroupCtrlType) {
        GroupCtrlType ctrlDef = (GroupCtrlType) model;
        ctrlID = ctrlDef.getID();
        ctrl = ctrlDef.createController( this, ctrlGroup );
      }
      else if (model instanceof ModuleCtrlType) {
        ModuleCtrlType ctrlDef = (ModuleCtrlType) model;
        ctrlID = ctrlDef.getID();
        ctrl = ctrlDef.createController( this, ctrlGroup );
      }
      else {
        Logger.warn( "unknown Controller definition: " + model );
        ctrl = null;
      }
      if (ctrl != null) {
        ctrlGroup.addController( ctrlID, ctrl );
        if (ctrl instanceof ListController) { // TODO...
          Controller detailCtrl = ((ListController) ctrl).getDetailController();
          if (detailCtrl != null) {
            ctrlGroup.addController( detailCtrl.getName(), detailCtrl );
          }
        }
      }
    }
    catch (Exception ex) {
      String msg = "Could not create controller, id="+model.key();
      Logger.error( msg, ex);
      throw new IllegalArgumentException( msg );
    }
    return ctrl;
  }

  public abstract TaskHandler createTaskHandler( ProgressTask progressTask );

  /**
   * Start...
   * @param dialogID
   * @param context
   * @param dialog
   * @param parentDlgCtrl
   * @return
   */
  public DialogController createDialogController( QName dialogID, Context context, de.pidata.gui.event.Dialog dialog, DialogController parentDlgCtrl ) {
    if (this.app == null) {
      throw new IllegalArgumentException("Application not initialized - use loadGui()");
    }
    if (dialogID == null) {
      throw new IllegalArgumentException("dialogID must not be null");
    }
    Model dialogDef = this.app.get( null, dialogID );
    if (dialogDef instanceof DialogType) {
      return ((DialogType) dialogDef).createController( this, context, dialog, parentDlgCtrl );
    }
    else {
      // TODO: eliminiate...
      return OldControllerBuilder.getInstance().createDialogController( this, dialogID, context, dialog, parentDlgCtrl );
    }
  }

  /**
   * Shows a dialog containing a message and a progress bar. Content of message and progress bar
   * can be changed via ProgressController. The handle to the open dialog can also be obtained
   * from ProgressController
   *
   * @param title     the dialog title
   * @param message   the messsage, use CR (\n) for line breaks
   * @param minValue the value representing 0%
   * @param maxValue   the value representing 100%
   * @param cancelable true if progress dialog can be canceled by user
   * @param parentDlgCtrl the parent DialogController
   * @return the ProgressController
   */
  public abstract ProgressController createProgressDialog( String title, String message, int minValue, int maxValue,
                                                           boolean cancelable, DialogController parentDlgCtrl );

 //------------------------------------------------------------------------------------------------------------------------------------
  // TODO: the following are copies from OldControllerBuilder; what to do with them?
  //------------------------------------------------------------------------------------------------------------------------------------

  /**
   *
   * @param eventID
   * @param classname
   * @param command
   * @param controllerGroup
   * @return
   * @deprecated use new GUI's buttonCtrl / actionCtr / operation instead
   */
  @Deprecated
  public GuiOperation createGuiOp( QName eventID, String classname, String command, ControllerGroup controllerGroup ) {
    GuiOperation guiOp;
    if ((classname != null) && (classname.length() > 0)) {
      try {
        guiOp = (GuiOperation) Class.forName(classname).newInstance();
        guiOp.init( eventID, command, controllerGroup );
      }
      catch (Exception e) {
        e.printStackTrace();
        throw new IllegalArgumentException("Could not create action class: "+classname);
      }
    }
    else {
      GuiInvokeOperation guiInvokeOp = new GuiInvokeOperation();
      guiInvokeOp.init( eventID, null, null, command, controllerGroup );
      guiOp = guiInvokeOp;
    }
    return guiOp;
  }

  /**
   *
   * @param ctrlGroup
   * @param modelSource
   * @param modelPath
   * @param valueID
   * @param namespaceTable
   * @return
   */
  public Binding createAttributeBinding( ControllerGroup ctrlGroup, ModelSource modelSource, String modelPath, QName valueID, NamespaceTable namespaceTable ) {
    if (((valueID == null) && (modelPath == null)) || (modelSource == null)) {
      return null;
    }
    XPath xPath = null;
    if ((modelPath != null) && (modelPath.length() > 0)) {
      xPath = new XPath( namespaceTable, modelPath );    //TODO NS sollte eigentlich der default-NS des GUI-Definition sein
    }
    AttributeBinding binding = new AttributeBinding( ctrlGroup.getContext(), modelSource, xPath, valueID );
    return binding;
  }

  public Selection createSelection( QName relationID, String modelPath, SelectionType selType, ControllerGroup ctrlGroup, NamespaceTable namespaceTable ) {
    return OldControllerBuilder.getInstance().createSelection( relationID, modelPath, selType, ctrlGroup, namespaceTable );
  }

  /**
   * @param moduleID
   * @param moduleController
   * @param modelSource
   * @param modelPath
   * @return
   * @throws Exception
   * @deprecated valid only for old GUI; use {@link #loadModule(ModuleController, QName, ModelSource, String)} instead
   */
  @Deprecated
  public ModuleGroup createModuleGroup( QName moduleID, ModuleController moduleController, ModelSource modelSource, String modelPath ) throws Exception {
    return OldControllerBuilder.getInstance().createModuleGroup( this, moduleID, moduleController, modelSource, modelPath );
  }

  /**
   * Creates the module's controller and adds it to the parent {@link ModuleController}.
   *
   * @param moduleCtrl
   * @param moduleID
   * @param modelSource
   * @param modelPath
   * @return the loaded module's controller
   * @throws Exception
   */
  public ModuleGroup loadModule( ModuleController moduleCtrl, QName moduleID, ModelSource modelSource, String modelPath ) throws Exception {
    DialogModuleType moduleDef = this.app.getDialogModule( moduleID );
    if (moduleDef != null) {
      ModuleGroup moduleGroup = moduleDef.createController( this, moduleCtrl, modelSource, modelPath, moduleDef.namespaceTable() );
      moduleCtrl.setModuleGroup( moduleGroup, ViewAnimation.NONE );
      return moduleGroup;
    }
    else {
      return OldControllerBuilder.getInstance().loadModule( this, moduleCtrl, moduleID, modelSource, modelPath );
    }
  }

  /**
   * Creates the module's controller and adds it to the parent {@link ModuleController}.
   *
   * @param moduleCtrl
   * @param moduleID
   * @param model      the model to be used for the new ModuleGroup
   * @return the loaded module's controller
   * @throws Exception
   */
  public ModuleGroup loadModule( ModuleController moduleCtrl, QName moduleID, Model model ) throws Exception {
    DialogModuleType moduleDef = this.app.getDialogModule( moduleID );
    if (moduleDef != null) {
      ModuleGroup moduleGroup = moduleDef.createController( this, moduleCtrl, model, moduleDef.namespaceTable() );
      moduleCtrl.setModuleGroup( moduleGroup, ViewAnimation.NONE );
      return moduleGroup;
    }
    else {
      return OldControllerBuilder.getInstance().loadModule( this, moduleCtrl, moduleID, null, null );
    }
  }

  protected Binding createModelListBinding( ControllerGroup ctrlGroup, QName dataSourceID, String modelPath, QName relationID, Filter filter, NamespaceTable namespaceTable ) {
    if ((modelPath == null) && (dataSourceID == null)) {
      return null;
    }
    ModelSource modelSource = getDataSource( ctrlGroup, dataSourceID );
    XPath xPath = null;
    if ((modelPath != null) && (modelPath.length() > 0)) {
      xPath = new XPath( namespaceTable, modelPath );    //TODO NS sollte eigentlich der default-NS des GUI-Definition sein
    }
    Binding binding = new ModelListBinding( ctrlGroup.getContext(), modelSource, xPath, relationID, filter );
    return binding;
  }

  /**
   * Tries to find the data source controller within ctrlGroup or one of its parents.
   * Recursive searching ist stopped at ctrlGroup's DialogController.
   * @param ctrlGroup    the controller group to start searching for dataSourceID
   * @param dataSourceID ID of the dat source controller
   * @return the data source controller
   * @throws IllegalArgumentException if dataSourceID could not be found
   * @throws ClassCastException if controller with dataSourceID is not a SelectionController
   */
  private ModelSource getDataSource( ControllerGroup ctrlGroup, QName dataSourceID ) {
    ModelSource modelSource;
    if (dataSourceID == null) {
      modelSource = ctrlGroup;
    }
    else {
      modelSource = (ModelSource) ctrlGroup.getController(dataSourceID);
      while ((modelSource == null) && (!(ctrlGroup instanceof DialogController))) {
        ctrlGroup = ctrlGroup.getParentGroup();
        modelSource = (ModelSource) ctrlGroup.getController(dataSourceID);
      }
      if (modelSource == null) {
        throw new IllegalArgumentException( "Unknown data source ID="+dataSourceID );
      }
    }
    return modelSource;
  }
}
