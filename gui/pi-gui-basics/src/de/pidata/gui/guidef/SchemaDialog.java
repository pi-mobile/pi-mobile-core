/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.guidef;

import de.pidata.gui.component.base.ComponentFactory;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.ModelParameterList;
import de.pidata.models.tree.Model;
import de.pidata.models.types.*;
import de.pidata.models.types.simple.*;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.service.base.ParameterList;

public class SchemaDialog {

  public static final Namespace NAMESPACE = Namespace.getInstance("de.pidata.gui");

  private static final String LABEL_PREFIX = "L_";
  private static final String COMP_PREFIX  = "C_";
  private static final String TABLE_PREFIX  = "T_";
  private static final String TABLE_HEADER_PREFIX  = "TH_";
  private static final String TABLE_BODY_PREFIX  = "TB_";
  private static final Integer NEXT_ROW = new Integer(-1);
  private static final Integer ZERO = new Integer(0);
  private static final Integer ONE = new Integer(1);
  private static final Integer TWO = new Integer(2);

  private DialogDef dialogDef;
  private PanelType actionPanel;
  private ControllerBuilder builder;

  public SchemaDialog(String dialogName, ControllerBuilder builder, Type type, QName tableRelation) throws Exception {
    this.builder = builder;
    create(dialogName, type, tableRelation);
  }

  public DialogDef getDialogDef() {
    return dialogDef;
  }

  public void addAction(QName serviceName, BuiltinOperation opID, QName partName) {
    ActionDef actionDef;
    InvokeType invoke;
    PartType input;
    CompDef buttonDef;
    Comp comp;

    QName opName = NAMESPACE.getQName( opID.toString() );
    actionDef = new ActionDef(opName);
    invoke = new InvokeType();
    invoke.setService(serviceName);
    invoke.setOperation(opName);
    if (partName != null) {
      input = new PartType(partName);
      input.setPath(".");
      invoke.addInput(input);
    }
    actionDef.addInvoke(invoke);
    buttonDef = createButtonDef(opName);
    comp = new Comp(ControllerBuilder.ID_BUTTON);
    comp.setCompID(buttonDef.getID());
    actionDef.addComp(comp);

    this.actionPanel.addCompDef(buttonDef);
    this.dialogDef.addActionDef(actionDef);
  }

  private void createControllerDef(DialogDef dlgDef, PanelType panelDef, QName attrID, SimpleType attrType) throws Exception {
    CompDef labelDef = createLabelDef(attrID, LABEL_PREFIX, true);
    CompDef compDef;
    String  attrName = attrID.getName();
    ControllerType ctrlType = new ControllerType(attrID);
    SimpleType rootType = attrType.getRootType();
    Comp comp;

    ctrlType.setModel(".");
    ctrlType.setValueID(attrID);
    if (QNameType.getQName().isAssignableFrom(attrType)) {
      ctrlType.setTypeID(ControllerBuilder.ID_SELECTIONCONTROLLER);
      ValueEnum valueEnum = attrType.getValueEnum();
      if (valueEnum != null) {
        Lookup lookup = new Lookup();
        lookup.setSelType(ControllerBuilder.SELECTION_MAX_ONE);
        lookup.setModel(valueEnum.getPath());
        lookup.setRelationID(valueEnum.getRelation());
        lookup.setValueID(valueEnum.getDisplayAttribute());
        ctrlType.setLookup(lookup);
      }
      compDef  = createCompDef(attrID, ComponentFactory.ID_COMBOBOX, COMP_PREFIX);
      comp = new Comp(ControllerBuilder.ID_SELECTOR);
    }
    else if (rootType instanceof StringType) {
      ctrlType.setTypeID(ControllerBuilder.ID_TEXTCONTROLLER);
      compDef  = createCompDef(attrID, ComponentFactory.ID_TEXTFIELD, COMP_PREFIX);
      comp = new Comp(ControllerBuilder.ID_TEXT);
    }
    else if (rootType instanceof DateTimeType) {
      ctrlType.setTypeID(ControllerBuilder.ID_DATECONTROLLER);
      compDef  = createCompDef(attrID, ComponentFactory.ID_TEXTFIELD, COMP_PREFIX);
      comp = new Comp(ControllerBuilder.ID_TEXT);
    }
    else if (rootType instanceof BooleanType) {
      //TODO BooleanController
      ctrlType.setTypeID(ControllerBuilder.ID_TEXTCONTROLLER);
      compDef  = createCompDef(attrID, ComponentFactory.ID_TEXTFIELD, COMP_PREFIX);
      comp = new Comp(ControllerBuilder.ID_TEXT);
    }
    else {
      //TODO
      ctrlType.setTypeID(ControllerBuilder.ID_TEXTCONTROLLER);
      compDef  = createCompDef(attrID, ComponentFactory.ID_TEXTFIELD, COMP_PREFIX);
      comp = new Comp(ControllerBuilder.ID_TEXT);
    }

    panelDef.addCompDef(labelDef);
    panelDef.addCompDef(compDef);

    Comp label = new Comp(ControllerBuilder.ID_LABEL);
    label.setCompID( NAMESPACE.getQName(LABEL_PREFIX+attrName) );
    ctrlType.addComp(label);
    comp.setCompID( NAMESPACE.getQName(COMP_PREFIX+attrName) );
    ctrlType.addComp(comp);

    dlgDef.addControllerDef(ctrlType);
  }

  private CompDef createLabelDef(QName attributeID, String prefix, boolean newLine) {
    QName   compID;
    CompDef labelDef;
    String  attrName = attributeID.getName();

    compID = NAMESPACE.getQName(prefix+attrName);
    labelDef = new CompDef(compID);
    labelDef.setTypeID(ComponentFactory.ID_LABEL);
    labelDef.setCaption(attrName);
    if (newLine) labelDef.setRow(NEXT_ROW);
    return labelDef;
  }

  private CompDef createButtonDef(QName actionName) {
    QName   compID;
    CompDef compDef;

    compID = NAMESPACE.getQName(COMP_PREFIX+actionName.getName());
    compDef = new CompDef(compID);
    compDef.setTypeID(ComponentFactory.ID_BUTTON);
    compDef.setCaption(actionName.getName());
    return compDef;
  }

  private CompDef createCompDef(QName attributeID, QName compType, String prefix) {
    QName   compID;
    CompDef compDef;
    String  attrName = attributeID.getName();

    compID = NAMESPACE.getQName(prefix+attrName);
    compDef = new CompDef(compID);
    compDef.setTypeID(compType);
    return compDef;
  }

  private QName getTableCompType(SimpleType attrType) {
    Type rootType = attrType.getRootType();
    if (QNameType.getQName().isAssignableFrom(attrType)) {
      return ComponentFactory.ID_LABEL;
    }
    else if (rootType instanceof StringType) {
      return ComponentFactory.ID_LABEL;
    }
    else if (rootType instanceof DateTimeType) {
      return ComponentFactory.ID_LABEL;
    }
    else if (rootType instanceof BooleanType) {
      //TODO BooleanController
      return ComponentFactory.ID_LABEL;
    }
    else {
      //TODO
      return ComponentFactory.ID_LABEL;
    }
  }

  private void createTableDef(DialogDef dialogDef, PanelType panelDef, ComplexType dialogType, QName tableRelation) {
    Layout layout;
    QName colID;
    SimpleType colType;
    ComplexType cType = (ComplexType) dialogType.getChildType(tableRelation);
    String tableName = tableRelation.getName();
    QName tableID = NAMESPACE.getQName(TABLE_PREFIX+tableName);
    TableDef tableDef = new TableDef(tableID);
    tableDef.setRow(NEXT_ROW);
    tableDef.setColSpan(TWO);
    layout = new Layout(NAMESPACE.getQName("cols"));
    layout.setTypeID(NAMESPACE.getQName("FlowLayouter"));
    tableDef.addLayout(layout);

    QName headerName = NAMESPACE.getQName(TABLE_HEADER_PREFIX+tableName);
    PanelType header = new PanelType(headerName);
    layout = new Layout(NAMESPACE.getQName("rows"));
    layout.setTypeID(NAMESPACE.getQName("FlowLayouter"));
    header.addLayout(layout);
    tableDef.setHeader(header);

    QName bodyName = NAMESPACE.getQName(TABLE_BODY_PREFIX+tableName);
    PanelType body = new PanelType(bodyName);
    layout = new Layout(NAMESPACE.getQName("rows"));
    layout.setTypeID(NAMESPACE.getQName("FlowLayouter"));
    body.addLayout(layout);
    tableDef.setBody(body);

    TableControllerDef tableCtrl = new TableControllerDef( NAMESPACE.getQName( tableRelation.getName() ) );
    tableCtrl.setTableID(tableID);

    for (int i = 0; i < cType.attributeCount(); i++) {
      colID = cType.getAttributeName(i);
      int keyIndex = cType.getKeyIndex(colID);
      if (keyIndex >= 0) {
        colType = cType.getKeyAttributeType( keyIndex );
      }
      else {
        colType = cType.getAttributeType(i);
      }
      CompDef headerComp = createLabelDef(colID, TABLE_HEADER_PREFIX, false);
      header.addCompDef(headerComp);

      CompDef bodyComp = createCompDef(colID, getTableCompType(colType), TABLE_BODY_PREFIX);
      body.addCompDef(bodyComp);

      Column tableCol = new Column(colID);
      tableCol.setHeadID(headerComp.getID());
      tableCol.setCompID(bodyComp.getID());
      tableCol.setModel(".");
      tableCol.setValueID(colID);
      tableCtrl.addColumn(tableCol);
    }

    Lookup lookup = new Lookup();
    lookup.setSelType(ControllerBuilder.SELECTION_MAX_ONE);
    lookup.setModel(".");
    lookup.setRelationID(tableRelation);
    tableCtrl.setLookup(lookup);

    panelDef.addTableDef(tableDef);

    dialogDef.addTableControllerDef(tableCtrl);

    //-- Edit action
    ActionDef actionDef;
    InvokeType invoke;
    PartType input;
    CompDef buttonDef;
    Comp comp;

    QName actionName = NAMESPACE.getQName("Edit");
    QName buttonName = NAMESPACE.getQName("B_Edit");
    actionDef = new ActionDef(actionName);
    invoke = new InvokeType();
    invoke.setOperation( NAMESPACE.getQName( tableRelation.getName() ) );
    input = new PartType();
    input.setDataSource( NAMESPACE.getQName( tableRelation.getName() ) );
    invoke.addInput(input);
    actionDef.addInvoke(invoke);
    buttonDef = createButtonDef(buttonName);
    comp = new Comp( ControllerBuilder.ID_BUTTON);
    comp.setCompID(buttonDef.getID());
    actionDef.addComp(comp);

    this.actionPanel.addCompDef(buttonDef);
    dialogDef.addActionDef(actionDef);
  }

  private DialogDef create(String dialogName, Type type, QName tableRelation) throws Exception {
    SimpleType attrType;
    QName attrID;
    PanelType panelDef;
    Layout layout;

    panelDef = new PanelType();
    panelDef.setRow(ZERO);
    panelDef.setRowSpan(ONE);
    panelDef.setCol(ZERO);
    panelDef.setColSpan(ONE);
    layout = new Layout(NAMESPACE.getQName("rows"));
    layout.setTypeID(NAMESPACE.getQName("FlowLayouter"));
    panelDef.addLayout(layout);
    layout = new Layout(NAMESPACE.getQName("cols"));
    layout.setTypeID(NAMESPACE.getQName("FlowLayouter"));
    panelDef.addLayout(layout);

    dialogDef = new DialogDef( NAMESPACE.getQName( dialogName ) );
    dialogDef.setTitle( dialogName );
    dialogDef.setPanelDef(panelDef);

    actionPanel = new PanelType();
    actionPanel.setRow(NEXT_ROW);
    actionPanel.setColSpan(TWO);
    layout = new Layout(NAMESPACE.getQName("rows"));
    layout.setTypeID(NAMESPACE.getQName("FlowLayouter"));
    actionPanel.addLayout(layout);
    layout = new Layout(NAMESPACE.getQName("cols"));
    layout.setTypeID(NAMESPACE.getQName("FlowLayouter"));
    actionPanel.addLayout(layout);
    addAction(null, BuiltinOperation.Ok, null);
    addAction(null, BuiltinOperation.Cancel, null);

    if (type instanceof ComplexType) {
      ComplexType cType = (ComplexType) type;
      for (int i = 0; i < cType.attributeCount(); i++) {
        attrID = cType.getAttributeName(i);
        int keyIndex = cType.getKeyIndex(attrID);
        if (keyIndex < 0) {
          attrType = cType.getAttributeType(i);
          createControllerDef(dialogDef, panelDef, attrID, attrType);
        }
      }
      if (tableRelation != null) {
        createTableDef(dialogDef, panelDef, cType, tableRelation);
      }
    }

    panelDef.addPanelDef(actionPanel);

    return dialogDef;
  }

  public void show( DialogController parentDlgCtrl, Model parameter ) {
    ParameterList parameterList = new ModelParameterList( parameter );
    parentDlgCtrl.openChildDialog( dialogDef, null, parameterList );
  }
}
