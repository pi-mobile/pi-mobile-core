// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.gui.guidef;

import de.pidata.gui.controller.base.MutableControllerGroup;
import de.pidata.gui.controller.base.WebViewController;
import de.pidata.gui.view.base.WebViewPI;
import de.pidata.models.binding.Binding;
import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.util.Hashtable;

public class WebViewCtrlType extends ValueCtrlType {

  public static final Namespace NAMESPACE = Namespace.getInstance( "de.pidata.gui" );

  public static final QName ID_WEBVIEWCOMPID = NAMESPACE.getQName("webViewCompID");

  public WebViewCtrlType( Key id ) {
    super( id, ControllerFactory.WEBVIEWCTRLTYPE_TYPE, null, null, null );
  }

  public WebViewCtrlType(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, ControllerFactory.WEBVIEWCTRLTYPE_TYPE, attributeNames, anyAttribs, childNames);
  }

  protected WebViewCtrlType(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the attribute webViewCompID.
   *
   * @return The attribute webViewCompID
   */
  public QName getWebViewCompID() {
    return (QName) get( ID_WEBVIEWCOMPID );
  }

  /**
   * Set the attribute webViewCompID.
   *
   * @param webViewCompID new value for attribute webViewCompID
   */
  public void setWebViewCompID( QName webViewCompID ) {
    set( ID_WEBVIEWCOMPID, webViewCompID );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  @Override
  public WebViewController createController( ControllerBuilder builder, MutableControllerGroup ctrlGroup ) throws Exception {
    WebViewController webViewController;
    String classname = getClassname();
    if ((classname == null) || (classname.length() == 0)) {
      webViewController = new WebViewController();
    }
    else {
      webViewController = (WebViewController) Class.forName(classname).newInstance();
    }

    QName webViewCompID = getWebViewCompID();
    if (webViewCompID == null) {
      throw new IllegalArgumentException( "WebView controller ID=" + getID() + ": comp is missing" );
    }
    WebViewPI webView = builder.getViewFactory().createWebView( webViewCompID );

    Namespace namespace = namespace();
    if (namespace != null) {
      webViewController.setNamespace( namespace );
    }

    Binding binding = createBinding( builder, ctrlGroup );

    webViewController.init( getID(), ctrlGroup, webView, binding, readOnly() );
    initBindings( ctrlGroup, webViewController );

    return webViewController;
  }
}
