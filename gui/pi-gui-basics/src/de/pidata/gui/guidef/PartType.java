// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.gui.guidef;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.lang.String;
import java.util.Hashtable;

public class PartType extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "de.pidata.gui" );

  public static final QName ID_CONTROLLER = NAMESPACE.getQName("controller");
  public static final QName ID_DATASOURCE = NAMESPACE.getQName("dataSource");
  public static final QName ID_ID = NAMESPACE.getQName("ID");
  public static final QName ID_PATH = NAMESPACE.getQName("path");
  public static final QName ID_VALUE = NAMESPACE.getQName("value");
  public static final QName ID_VARIABLE = NAMESPACE.getQName("variable");

  public PartType() {
    super( null, ControllerFactory.PARTTYPE_TYPE, null, null, null );
  }

  public PartType(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, ControllerFactory.PARTTYPE_TYPE, attributeNames, anyAttribs, childNames);
  }

  protected PartType(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the attribute ID.
   *
   * @return The attribute ID
   */
  public QName getID() {
    return (QName) get( ID_ID );
  }

  /**
   * Set the attribute ID.
   *
   * @param iD new value for attribute ID
   */
  public void setID( QName iD ) {
    set( ID_ID, iD );
  }

  /**
   * Returns the attribute dataSource.
   *
   * @return The attribute dataSource
   */
  public QName getDataSource() {
    return (QName) get( ID_DATASOURCE );
  }

  /**
   * Set the attribute dataSource.
   *
   * @param dataSource new value for attribute dataSource
   */
  public void setDataSource( QName dataSource ) {
    set( ID_DATASOURCE, dataSource );
  }

  /**
   * Returns the attribute path.
   *
   * @return The attribute path
   */
  public String getPath() {
    return (String) get( ID_PATH );
  }

  /**
   * Set the attribute path.
   *
   * @param path new value for attribute path
   */
  public void setPath( String path ) {
    set( ID_PATH, path );
  }

  /**
   * Returns the attribute value.
   *
   * @return The attribute value
   */
  public String getValue() {
    return (String) get( ID_VALUE );
  }

  /**
   * Set the attribute value.
   *
   * @param value new value for attribute value
   */
  public void setValue( String value ) {
    set( ID_VALUE, value );
  }

  /**
   * Returns the attribute variable.
   *
   * @return The attribute variable
   */
  public QName getVariable() {
    return (QName) get( ID_VARIABLE );
  }

  /**
   * Set the attribute variable.
   *
   * @param variable new value for attribute variable
   */
  public void setVariable( QName variable ) {
    set( ID_VARIABLE, variable );
  }

  /**
   * Returns the attribute controller.
   *
   * @return The attribute controller
   */
  public QName getController() {
    return (QName) get( ID_CONTROLLER );
  }

  /**
   * Set the attribute controller.
   *
   * @param controller new value for attribute controller
   */
  public void setController( QName controller ) {
    set( ID_CONTROLLER, controller );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  public PartType( Key id ) {
    super( id, ControllerFactory.PARTTYPE_TYPE, null, null, null );
  }
}
