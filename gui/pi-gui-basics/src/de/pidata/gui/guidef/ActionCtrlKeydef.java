// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.gui.guidef;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.DefaultModel;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.util.Hashtable;

/**
 * <p>Helper element used to define the key for 'actionCtrlType'; DO NOT use in GUi definition files!</p>
 */
public class ActionCtrlKeydef extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "de.pidata.gui" );

  public static final QName ID_ACTIONCTRL = NAMESPACE.getQName("actionCtrl");

  public ActionCtrlKeydef() {
    super( null, ControllerFactory.ACTIONCTRLKEYDEF_TYPE, null, null, null );
  }

  public ActionCtrlKeydef(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, ControllerFactory.ACTIONCTRLKEYDEF_TYPE, attributeNames, anyAttribs, childNames);
  }

  protected ActionCtrlKeydef(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the element actionCtrl.
   *
   * @return The element actionCtrl
   */
  public ActionCtrlType getActionCtrl() {
    return (ActionCtrlType) get( ID_ACTIONCTRL, null );
  }

  /**
   * Set the element actionCtrl.
   *
   * @param actionCtrl new value for element actionCtrl
   */
  public void setActionCtrl( ActionCtrlType actionCtrl ) {
    setChild( ID_ACTIONCTRL, actionCtrl );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
