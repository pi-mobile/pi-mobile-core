// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.gui.guidef;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.DefaultModel;
import de.pidata.models.tree.ModelCollection;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.IntegerType;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.lang.Integer;
import java.lang.String;
import java.util.Collection;
import java.util.Hashtable;

public class ShapeDef extends CompDef {

  public static final Namespace NAMESPACE = Namespace.getInstance( "de.pidata.gui" );

  public static final QName ID_COLOR = NAMESPACE.getQName("color");
  public static final QName ID_COORDS = NAMESPACE.getQName("coords");
  public static final QName ID_HEIGHT = NAMESPACE.getQName("height");
  public static final QName ID_PARAM = NAMESPACE.getQName("param");
  public static final QName ID_SELECTEDCOLOR = NAMESPACE.getQName("selectedColor");
  public static final QName ID_WIDTH = NAMESPACE.getQName("width");
  public static final QName ID_X = NAMESPACE.getQName("x");
  public static final QName ID_Y = NAMESPACE.getQName("y");

  private Collection<KeyAndValue> params = new ModelCollection<KeyAndValue>( ID_PARAM, children );

  public ShapeDef( Key id ) {
    super( id, ControllerFactory.SHAPEDEF_TYPE, null, null, null );
  }

  public ShapeDef(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, ControllerFactory.SHAPEDEF_TYPE, attributeNames, anyAttribs, childNames);
  }

  protected ShapeDef(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the attribute coords.
   *
   * @return The attribute coords
   */
  public String getCoords() {
    return (String) get( ID_COORDS );
  }

  /**
   * Set the attribute coords.
   *
   * @param coords new value for attribute coords
   */
  public void setCoords( String coords ) {
    set( ID_COORDS, coords );
  }

  /**
   * Returns the attribute color.
   *
   * @return The attribute color
   */
  public QName getColor() {
    return (QName) get( ID_COLOR );
  }

  /**
   * Set the attribute color.
   *
   * @param color new value for attribute color
   */
  public void setColor( QName color ) {
    set( ID_COLOR, color );
  }

  /**
   * Returns the attribute selectedColor.
   *
   * @return The attribute selectedColor
   */
  public QName getSelectedColor() {
    return (QName) get( ID_SELECTEDCOLOR );
  }

  /**
   * Set the attribute selectedColor.
   *
   * @param selectedColor new value for attribute selectedColor
   */
  public void setSelectedColor( QName selectedColor ) {
    set( ID_SELECTEDCOLOR, selectedColor );
  }

  /**
   * Returns the attribute x.
   *
   * @return The attribute x
   */
  public Integer getX() {
    return (Integer) get( ID_X );
  }

  /**
   * Set the attribute x.
   *
   * @param x new value for attribute x
   */
  public void setX( Integer x ) {
    set( ID_X, x );
  }

  /**
   * Returns the attribute y.
   *
   * @return The attribute y
   */
  public Integer getY() {
    return (Integer) get( ID_Y );
  }

  /**
   * Set the attribute y.
   *
   * @param y new value for attribute y
   */
  public void setY( Integer y ) {
    set( ID_Y, y );
  }

  /**
   * Returns the attribute width.
   *
   * @return The attribute width
   */
  public Integer getWidth() {
    return (Integer) get( ID_WIDTH );
  }

  /**
   * Set the attribute width.
   *
   * @param width new value for attribute width
   */
  public void setWidth( Integer width ) {
    set( ID_WIDTH, width );
  }

  /**
   * Returns the attribute height.
   *
   * @return The attribute height
   */
  public Integer getHeight() {
    return (Integer) get( ID_HEIGHT );
  }

  /**
   * Set the attribute height.
   *
   * @param height new value for attribute height
   */
  public void setHeight( Integer height ) {
    set( ID_HEIGHT, height );
  }

  /**
   * Returns the param element identified by the given key.
   *
   * @return the param element identified by the given key
   */
  public KeyAndValue getParam( Key paramID ) {
    return (KeyAndValue) get( ID_PARAM, paramID );
  }

  /**
   * Adds the param element.
   *
   * @param param the param element to add
   */
  public void addParam( KeyAndValue param ) {
    add( ID_PARAM, param );
  }

  /**
   * Removes the param element.
   *
   * @param param the param element to remove
   */
  public void removeParam( KeyAndValue param ) {
    remove( ID_PARAM, param );
  }

  /**
   * Returns the param iterator.
   *
   * @return the param iterator
   */
  public ModelIterator<KeyAndValue> paramIter() {
    return iterator( ID_PARAM, null );
  }

  /**
   * Returns the number of params.
   *
   * @return the number of params
   */
  public int paramCount() {
    return childCount( ID_PARAM );
  }

  /**
   * Returns the param collection.
   *
   * @return the param collection
   */
  public Collection<KeyAndValue> getParams() {
    return params;
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
