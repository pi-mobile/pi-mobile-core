// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.gui.guidef;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.ControllerGroup;
import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.DefaultModel;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.string.Helper;
import java.lang.Boolean;
import java.lang.String;
import java.util.Hashtable;

public abstract class CtrlType extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "de.pidata.gui" );

  public static final QName ID_CLASSNAME = NAMESPACE.getQName("classname");
  public static final QName ID_FORMAT = NAMESPACE.getQName("format");
  public static final QName ID_ID = NAMESPACE.getQName("ID");
  public static final QName ID_INPUTMODE = NAMESPACE.getQName("inputMode");
  public static final QName ID_LABELCOMPID = NAMESPACE.getQName("labelCompID");
  public static final QName ID_NAMESPACE = NAMESPACE.getQName("namespace");
  public static final QName ID_READONLY = NAMESPACE.getQName("readOnly");
  public static final QName ID_VISIBILITYATTRBINDING = NAMESPACE.getQName("visibilityAttrBinding");

  public CtrlType( Key id ) {
    super( id, ControllerFactory.CTRLTYPE_TYPE, null, null, null );
  }

  public CtrlType(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, ControllerFactory.CTRLTYPE_TYPE, attributeNames, anyAttribs, childNames);
  }

  protected CtrlType(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the attribute ID.
   *
   * @return The attribute ID
   */
  public QName getID() {
    return (QName) get( ID_ID );
  }

  /**
   * Returns the attribute labelCompID.
   *
   * @return The attribute labelCompID
   */
  public QName getLabelCompID() {
    return (QName) get( ID_LABELCOMPID );
  }

  /**
   * Set the attribute labelCompID.
   *
   * @param labelCompID new value for attribute labelCompID
   */
  public void setLabelCompID( QName labelCompID ) {
    set( ID_LABELCOMPID, labelCompID );
  }

  /**
   * Returns the attribute classname.
   *
   * @return The attribute classname
   */
  public String getClassname() {
    return (String) get( ID_CLASSNAME );
  }

  /**
   * Set the attribute classname.
   *
   * @param classname new value for attribute classname
   */
  public void setClassname( String classname ) {
    set( ID_CLASSNAME, classname );
  }

  /**
   * Returns the attribute readOnly.
   *
   * @return The attribute readOnly
   */
  public Boolean getReadOnly() {
    return (Boolean) get( ID_READONLY );
  }

  /**
   * Set the attribute readOnly.
   *
   * @param readOnly new value for attribute readOnly
   */
  public void setReadOnly( Boolean readOnly ) {
    set( ID_READONLY, readOnly );
  }

  /**
   * Returns the attribute inputMode.
   *
   * @return The attribute inputMode
   */
  public InputMode getInputMode() {
    return (InputMode) get( ID_INPUTMODE );
  }

  /**
   * Set the attribute inputMode.
   *
   * @param inputMode new value for attribute inputMode
   */
  public void setInputMode( InputMode inputMode ) {
    set( ID_INPUTMODE, inputMode );
  }

  /**
   * Returns the attribute format.
   *
   * @return The attribute format
   */
  public String getFormat() {
    return (String) get( ID_FORMAT );
  }

  /**
   * Set the attribute format.
   *
   * @param format new value for attribute format
   */
  public void setFormat( String format ) {
    set( ID_FORMAT, format );
  }

  /**
   * Returns the attribute namespace.
   * <p>default namespace for QName values</p>
   *
   * @return The attribute namespace
   */
  public String getNamespace() {
    return (String) get( ID_NAMESPACE );
  }

  /**
   * Set the attribute namespace.
   * <p>default namespace for QName values</p>
   *
   * @param namespace new value for attribute namespace
   */
  public void setNamespace( String namespace ) {
    set( ID_NAMESPACE, namespace );
  }

  /**
   * Returns the element visibilityAttrBinding.
   *
   * @return The element visibilityAttrBinding
   */
  public AttrBindingType getVisibilityAttrBinding() {
    return (AttrBindingType) get( ID_VISIBILITYATTRBINDING, null );
  }

  /**
   * Set the element visibilityAttrBinding.
   *
   * @param visibilityAttrBinding new value for element visibilityAttrBinding
   */
  public void setVisibilityAttrBinding( AttrBindingType visibilityAttrBinding ) {
    setChild( ID_VISIBILITYATTRBINDING, visibilityAttrBinding );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  public void initBindings( ControllerGroup ctrlGroup, Controller controller ) {
    throw new IllegalArgumentException( "Controller does not handle bindings!" );
  }

  /**
   * Returns the boolean value of the attribute {@link #ID_READONLY} which is represented by an
   * object {@link Boolean}. If the object is null, returns false.
   * @return true if the attribute object is not null and represents the value true; false otherwise
   */
  public boolean readOnly() {
    boolean readOnly = Helper.getBoolean( getReadOnly() );
    return readOnly;
  }

  /**
   * Returns the {@link Namespace} referenced by the attribute {@link #ID_NAMESPACE}. Resolves the string
   * given as the attribute's value as prefix from the namespace table.
   * @return the referenced {@link Namespace}
   */
  public Namespace namespace() {
    Namespace namespace;
    String nsStr = getNamespace();
    if ((nsStr != null) && (nsStr.length() > 0)) {
      namespace = namespaceTable().getByPrefix( nsStr );
    }
    else {
      namespace = null;
    }
    return namespace;
  }

  /**
   * Provided for DEBUG purpose.
   * @return
   */
  @Override
  public String toString() {
    Object id = get( ID_ID );
    if (id == null) {
      return "-- no ID --";
    }
    else {
      return id.toString();
    }
  }
}
