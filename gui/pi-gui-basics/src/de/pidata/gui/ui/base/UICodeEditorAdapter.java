/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ui.base;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.controller.base.SearchListener;
import de.pidata.parser.Parser;

import java.util.List;
import java.util.Map;

/**
 * @author dsc
 */
public interface UICodeEditorAdapter extends UITextEditorAdapter {

  void showLinenumbers( boolean show );

  void setAutoindent( boolean autoindent );

  /**
   * Sets the parser to use for syntax highlighting.
   *
   * Note: If a Parser exists pattern map is ignored.
   *
   * @param parser the parser to use or null toi remove the parser
   */
  void setCodeParser( Parser parser );

  /**
   * Sets the pattern map to use for syntax highlighting
   *
   * Note: If a Parser exists pattern map is ignored.
   *
   * @param patternMap the pattern map to use for highlighting
   */
  void setCodePatternMap( Map<String, String> patternMap );

  void searchAll( String searchStr );

  void searchNext( String searchStr );

  void searchPrev( String searchStr );

  void addLineMarker( String marker, List<Integer> markLinesList, ComponentColor markerColor );

  void refreshLineMarkers();

  void setSearchRegex( boolean searchRegex );

  void initListeners();

  void addSearchListener( SearchListener searchListener );

  void replaceNext( String replaceStr );

  void replaceAll( String replaceStr );
}
