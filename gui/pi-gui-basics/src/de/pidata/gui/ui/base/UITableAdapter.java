/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ui.base;

import de.pidata.gui.controller.base.ColumnInfo;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

import java.util.HashMap;
import java.util.List;

public interface UITableAdapter extends UIListAdapter {

  /**
   * Called by TableViewPI to start cell editing form application code.
   * This call should do the same like a mouse click on an editable cell.
   *
   * @param rowModel    row model to edit
   * @param columnInfo  column to edit
   */
  public void editCell( Model rowModel, ColumnInfo columnInfo );

  public HashMap<QName, Object> getFilteredValues();

  public void unselectValues ( HashMap<QName, Object> unselectedValuesMap);

  public void finishedUpdateAllRows();

  public List<Model> getVisibleRowModels();

  public void columnChanged(ColumnInfo columnInfo);

  void resetFilter();

  void resetFilter(ColumnInfo columnInfo);
}
