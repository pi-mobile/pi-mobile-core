/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ui.base;

import de.pidata.gui.view.base.*;
import de.pidata.gui.view.figure.ShapePI;

import java.util.Properties;

public interface UIFactory {

  /**
   * Searches uiContainer for UI text matching textProps (i.e. having textProp key as ID)
   * and replaces that text by text from textProps. If text contains "{GLOSSARYNAME}" that
   * text is replaced by entry form entry of glossaryProps
   *
   * @param uiContainer   the UI container to process
   * @param textProps     properties with text to use
   * @param glossaryProps properties with glossary entries
   */
  void updateLanguage( UIContainer uiContainer, Properties textProps, Properties glossaryProps );

  UIListAdapter createListAdapter( UIContainer uiContainer, ListViewPI listViewPI );

  UIButtonAdapter createButtonAdapter( UIContainer uiContainer, ButtonViewPI buttonViewPI );

  UITextAdapter createTextAdapter( UIContainer uiContainer, TextViewPI textViewPI );

  UIDateAdapter createDateAdapter( UIContainer uiContainer, DateViewPI dateViewPI );

  UITextEditorAdapter createTextEditorAdapter( UIContainer uiContainer, TextEditorViewPI textEditorViewPI );

  UICodeEditorAdapter createCodeEditorAdapter( UIContainer uiContainer, CodeEditorViewPI codeEditorViewPI );

  UIWebViewAdapter createWebViewAdapter( UIContainer uiContainer, WebViewPI webViewPI );

  UIHTMLEditorAdapter createHTMLEditorAdapter( UIContainer uiContainer, HTMLEditorViewPI htmlEditorViewPI );

  UINodeAdapter createNodeAdapter( UIContainer uiContainer, NodeViewPI nodeViewPI );

  UITableAdapter createTableAdapter( UIContainer uiContainer, TableViewPI tableViewPI );

  UIImageAdapter createImageAdapter( UIContainer uiContainer, ImageViewPI imageViewPI );

  UIFlagAdapter createFlagAdapter( UIContainer uiContainer, FlagViewPI flagViewPI );

  UIPaintAdapter createPaintAdapter( UIContainer uiContainer, PaintViewPI paintViewPI );

  UIProgressBarAdapter createProgressAdapter( UIContainer uiContainer, ProgressBarViewPI progressBarViewPI );

  UITabGroupAdapter createTabGroupAdapter( UIContainer uiContainer, TabGroupViewPI tabGroupViewPI );

  UITreeAdapter createTreeAdapter( UIContainer uiContainer, TreeViewPI treeViewPI );

  UIShapeAdapter createShapeAdapter( UIPaintAdapter uiPaintAdapter, ShapePI shapePI );

  UIFragmentAdapter createFragmentAdapter( UIContainer uiContainer, ModuleViewPI moduleViewPI );

  UITabPaneAdapter createTabPaneAdapter( UIContainer uiContainer, TabPaneViewPI tabPaneViewPI );

  UITreeTableAdapter createTreeTableAdapter( UIContainer uiContainer, TreeTableViewPI tableViewPI );

  UIAdapter createCustomAdapter( UIContainer uiContainer, ViewPI viewPI );
}
