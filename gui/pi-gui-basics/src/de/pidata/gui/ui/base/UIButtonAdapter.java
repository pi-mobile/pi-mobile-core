/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ui.base;

public interface UIButtonAdapter extends UIAdapter {

  /**
   * ---DEPRECATED---
   * setLabel will be called if binding exists using valueID,
   * use iconValueID or labelValueID instead!
   *
   * Set this button's label
   * @param label the label text
   */
  @Deprecated
  public void setLabel( String label );

  /**
   * Set this button's text Property
   * @param text the text
   */
  public void setText( Object text );

  /**
   * Set graphic on Button
   * @param value
   */
  public void setGraphic( Object value );

  /**
   * Returns this button's label
   * @return this button's label
   */
  public String getText();

  public void setSelected( boolean selected );
}
