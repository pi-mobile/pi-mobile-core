/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ui.base;

import de.pidata.models.tree.Model;

/**
 * A UIContainer is something which contains UI elements and may have a separate data context.
 * UIContainers are e.g. Dialogs, Modules/Fragments and also for FXTableCells.
 */
public interface UIContainer {

  /**
   * Returns the UIFactory to use when creating an UIAdapter for a
   * UI component within this container.
   */
  UIFactory getUIFactory();

  /**
   * Returns the data context for this UIContainer
   * @return
   */
  Model getDataContext();
}
