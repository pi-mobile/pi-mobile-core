/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ui.base;

import de.pidata.gui.view.figure.Figure;
import de.pidata.gui.view.figure.FigureHandle;
import de.pidata.gui.view.figure.ShapePI;

public interface UIPaintAdapter extends UIValueAdapter {

  void setSelectionHandles( FigureHandle[] selectionHandles );

  /**
   * Notifies that shape for given figure has been modified.
   * If shapePI is null all shapes of figure are modified.
   *
   * @param figure  the figure having modified shape(s)
   * @param shapePI the shape or null for all shapes
   */
  void figureModified( Figure figure, ShapePI shapePI );

  void figureAdded( Figure figure );

  void figureRemoved( Figure figure );
  
  void shapeRemoved( Figure figure, ShapePI shapePI );
}
