/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ui.base;

import de.pidata.models.tree.Model;

public interface UIListAdapter extends UIAdapter {

  /**
   * Called by ListViewPI in order to select row at index
   * @param displayRow the row to be selected
   * @param selected
   */
  void setSelected( Model displayRow, boolean selected );

  /**
   * Returns the count of display rows
   * @return the count of display rows
   */
  int getDisplayRowCount();

  /**
   * Returns the data displayed in list at position. In case of a sorted list
   * the position may be different from data model's index
   * @param position the position in displayed list
   * @return the model displayed at position
   */
  Model getDisplayRow( int position );

  /**
   * Remove all rows from displayed list
   */
  void removeAllDisplayRows();

  /**
   * Insert newRow into displayed list before beforeRow.
   * @param newRow    the row to be inserted
   * @param beforeRow the row before which newRow is inserted
   */
  void insertRow( Model newRow, Model beforeRow );

  /**
   * Remove row from display list
   * @param removedRow the row to remove
   */
  void removeRow( Model removedRow );

  /**
   * Update changedRow in display list
   * @param changedRow the row to be updated in display list
   */
  void updateRow( Model changedRow );
}
