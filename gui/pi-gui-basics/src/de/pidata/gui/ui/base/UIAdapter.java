/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ui.base;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.qnames.QName;

public interface UIAdapter {

  public ViewPI getViewPI();

  public void setVisible( boolean visible );

  boolean isVisible();

  boolean isFocused();

  public void setEnabled( boolean enabled );

  boolean isEnabled();

  /**
   * Set this UIAdapter's background color
   *
   * @param color new background color
   */
  void setBackground( ComponentColor color );

  /**
   * Resets this UIAdapters background color by its initial background color
   */
  void resetBackground();

  UIContainer getUIContainer();

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  public void detach();

  public void repaint();

  public boolean processCommand( QName cmd, char inputChar, int index );

  /**
   * send onClick request from Controller -> ViewPI -> UIAdapter -> View
   */
  public void performOnClick();

  /**
   * Toggles focus, selected, enabled and visible state of this component at position (posX, posY) or for
   * entry identified by key. It depends on the type of component what is meant by posX, posY and key.
   * Many components will just ignore posX and posY values or key.
   * The default implementaion calls the componentListener.
   *
   * @param posX     the x position of the source event
   * @param posY     the y position of the source event
   * @param focus    set focus on/off
   * @param selected set selected if true, deselected if false, do not change if null
   * @param enabled  set enabled if true, disabled if false, do not change if null
   * @param visible  set visible if true, invisible if false, do not change if null
   */
  void setState( short posX, short posY, Boolean focus, Boolean selected, Boolean enabled, Boolean visible );
}
