/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.event;

import de.pidata.qnames.QName;

/**
 *  Interface between UIAdapter and platform-specific View
 *  (e.g. android.View for Android, gui.component.base.Component for Skin)
 */
public interface ComponentListener {

  /**
   * Invoked when component is selected/deselected
   * @param compID   id of the component sending this message
   * @param posX
   * @param posY
   * @param selected true if source was selected, false if source was deselected
   */
  public void selected( QName compID, short posX, short posY, boolean selected );

  /**
   * Called by Component whenever a input command arrives.
   *
   * @param compID   id of the component sending this message
   * @param cmd       the command, one of the constants InputManager.CMD_*
   * @param inputChar optional input character for the command
   * @param index     optional index parameter for the command
   * @return true if the command has been processed, otherwise false
   */
  public boolean command( QName compID, QName cmd, char inputChar, int index );

  /**
   * Called by Component whenever it gains or looses input focus.
   * @param viewID   id of the view sending this message
   * @param hasFocus true if view gained input focus @return if false focus switch is not allowed by ComponentListener
   */
  public void onFocusChanged( QName viewID, boolean hasFocus );

  /**
   * @param componentID id of the component sending this message
   * @param minX
   * @param minY
   * @param maxX
   * @param maxY
   */
  void setVisibleRange( QName componentID, short minX, short minY, short maxX, short maxY );
}
