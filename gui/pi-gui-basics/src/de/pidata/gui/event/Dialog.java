/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.event;

import de.pidata.gui.controller.base.*;
import de.pidata.gui.controller.file.FileChooserParameter;
import de.pidata.gui.guidef.DialogDef;
import de.pidata.gui.guidef.DialogType;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.qnames.QName;
import de.pidata.rights.RightsRequireListener;
import de.pidata.service.base.ParameterList;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;

public interface Dialog extends UIContainer {

  /**
   * Returns current dialog title. The title is set by DialogController
   * by calling onTitleChanged().
   *
   * @return current dialog title
   */
  String getTitle();

  /**
   * Sets the controller for this dialog.
   * @param dialogController this dialog's controller
   */
  void setController( DialogController dialogController );

  /**
   * Returns this dialog's controller
   * @return this dialog's controller
   */
  DialogController getController();

  void show( Dialog parent, boolean fullScreen );

  void toFront();

  void toBack();

  /**
   * Show/hide busy cursor or busy animation.
   *
   * @param busy if true show busy, otherwise hide
   */
  void showBusy( boolean busy);

  /**
   * Called by DialogController.close() before anything other is
   * done. By returning false this Dialog can interrupt closing if
   * the Platform supports to abort close requests (e.g. Android does
   * not support that). Java FX will try to close children and
   * abort closing if any child dialog need user feedback.
   *
   * @param ok true if closing is OK operation
   * @return false to abort closing
   */
  boolean closing( boolean ok );

  void close( boolean ok, ParameterList resultList );

  void speak( String text );

  void setAlwaysOnTop( boolean onTop );
  boolean getAlwaysOnTop();

  void setMinWidth( int width );

  void setMaxWidth( int width );

  void setMinHeight( int height );

  void setMaxHeight( int height );

  /**
   * Called by DialogController after title has been changed.
   * Application code should always use {@link de.pidata.gui.controller.base.DialogController#setTitle(String)}
   * to change dialog title.
   *
   * @param title the new dialog title
   */
  void onTitleChanged( String title );

  /**
   * Called whenever language has changed and so UI should be updated with localized strings
    */
  public void updateLanguage();

  /**
   * Opens child dialog with given definition {@link DialogType} and title
   *
   * @param dialogType    user interface definition for the dialog to be opened
   * @param title         title for the dialog, if null title from dialogDef is used
   * @param parameterList dialog parameter list
   * @return handle to the opened dialog for use in {@link GuiOperation#dialogClosed(DialogController, boolean, ParameterList)}
   */
  void openChildDialog( DialogType dialogType, String title, ParameterList parameterList );

  /**
   * Opens child dialog with given definition {@link DialogDef} and title
   *
   * @param dialogDef     user interface definition for the dialog to be opened
   * @param title         title for the dialog, if null title from dialogDef is used
   * @param parameterList dialog parameter list
   * @return handle to the opened dialog for use in {@link GuiOperation#dialogClosed(DialogController, boolean, ParameterList)}
   * @deprecated old GUI; use new GUI definition instead!
   */
  @Deprecated
  void openChildDialog( DialogDef dialogDef, String title, ParameterList parameterList );

  /**
   * Opens popup with given moduleDef and title
   *
   * @param popupCtrl
   * @param moduleGroup module group to be shown in a Popup
   * @param title       title for the dialog, if null title from dialogDef is used
   */
  void showPopup( PopupController popupCtrl, ModuleGroup moduleGroup, String title );

  /**
   * Hides current popup. If popup is not visible nothing happens.
   * @param popupController
   */
  void closePopup( PopupController popupController );

  /**
   * Show a message dialog
   *
   * @param title   the dialog title
   * @param message the messsage, use CR (\n) for line breaks
   */
  void showMessage( String title, String message );

  /**
   * Show a question dialog dialog
   *
   * @param title          the dialog title
   * @param message        the messsage, use CR (\n) for line breaks
   * @param btnYesLabel    the label for yes/ok button or null to hide button
   * @param btnNoLabel     the label for no button or null to hide button
   * @param btnCancelLabel the label for cancel button or null to hide button
   */
  void showQuestion( String title, String message, String btnYesLabel, String btnNoLabel, String btnCancelLabel );

  /**
   * Show a dialog with a text input field
   *
   * @param title          the dialog title
   * @param message        the messsage, use CR (\n) for line breaks
   * @param defaultValue   a default value for the input field
   * @param btnOKLabel     the label for ok button or null to hide button
   * @param btnCancelLabel the label for cancel button or null to hide button
   */
  void showInput( String title, String message, String defaultValue, String btnOKLabel, String btnCancelLabel );

  /**
   * Shows the platform specific file chooser dialog.
   *
   * @param title             dialog title
   * @param fileChooserParams parameters
   */
  void showFileChooser( String title, FileChooserParameter fileChooserParams );

  /**
   * Shows the platform specific image chooser dialog.
   * <ul>
   *   <li>assure client has rights to write on destination</li>
   *   <li>use {@link SystemManager#getStorage(String)} and {@link Storage#getPath(String)} to get absolute Path on different Platforms</li>
   * </ul>
   *
   * @param imageChooserType type of the image chooser
   * @param filePath         <b>absolute</b> path of file that has to be used, if image will be created (by using camera)
   */
  void showImageChooser( ImageChooserType imageChooserType, String filePath );

  /**
   * Show a toast message, i.e. a message which appears only for a short moment
   * and ten disappears.
   *
   * @param message the message to show
   */
  void showToast( String message );

  /**
   * Register a key combination command as shortcut and setup the handling for it.
   *
   * @param command the key combination to register
   * @param controller the controller to fire on
   */
  void registerShortcut( QName command, ActionController controller );

  /**
   * Require rights at a specific platform.
   */
  void requireRights();

  /**
   * Add a RightsRequireListener for callback.
   */
  void addRightsRequireListener( RightsRequireListener rightsRequireListener );

  /**
   * Set UI Style
   * @param style
   */
  void setStyle( String style );
}
