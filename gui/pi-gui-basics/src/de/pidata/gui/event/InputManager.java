/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.event;

import de.pidata.gui.component.base.Component;
import de.pidata.gui.component.base.Dialog;
import de.pidata.gui.component.base.Screen;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

public interface InputManager {

  Namespace NAMESPACE = Namespace.getInstance("de.pidata.gui");

  QName CMD_MAINMENU   = NAMESPACE.getQName("MainMenu");
  QName CMD_LOCAL_MENU = NAMESPACE.getQName("LocalMenu");
  QName CMD_SELECT     = NAMESPACE.getQName("Select");
  QName CMD_OPEN       = NAMESPACE.getQName("Open");
  QName CMD_ESC        = NAMESPACE.getQName("Esc");
  QName CMD_NEXT_COMP  = NAMESPACE.getQName("NextComp");
  QName CMD_PREV_COMP  = NAMESPACE.getQName("PrevComp");
  QName CMD_BACKSPACE  = NAMESPACE.getQName("Backspace");
  QName CMD_ENTER      = NAMESPACE.getQName("Enter");
  QName CMD_DELETE     = NAMESPACE.getQName("Delete");
  QName CMD_LEFT       = NAMESPACE.getQName("Left");
  QName CMD_RIGHT      = NAMESPACE.getQName("Right");
  QName CMD_UP         = NAMESPACE.getQName("Up");
  QName CMD_DOWN       = NAMESPACE.getQName("Down");
  QName CMD_PAGE_UP    = NAMESPACE.getQName("PageUp");
  QName CMD_PAGE_DOWN  = NAMESPACE.getQName("PageDown");
  QName CMD_HOME       = NAMESPACE.getQName("Home");
  QName CMD_END        = NAMESPACE.getQName("End");
  QName CMD_INPUT      = NAMESPACE.getQName("Input");
  QName CMD_UNIT_INC   = NAMESPACE.getQName("UnitInc");
  QName CMD_UNIT_DEC   = NAMESPACE.getQName("UnitDec");
  QName CMD_BLOCK_INC  = NAMESPACE.getQName("BlockInc");
  QName CMD_BLOCK_DEC  = NAMESPACE.getQName("BlockDec");
  QName CMD_NEXT_FOCUS = NAMESPACE.getQName("NextFocus");
  QName CMD_PREV_FOCUS = NAMESPACE.getQName("PrevFocus");
  QName CMD_COPY       = NAMESPACE.getQName("Copy");
  QName CMD_PASTE      = NAMESPACE.getQName("Paste");
  QName CMD_GRAB_FOCUS = NAMESPACE.getQName("GrabFocus");

  QName CMD_BACK_SPACE = NAMESPACE.getQName("Backspace");
  QName CMD_TAB = NAMESPACE.getQName("Tab");
  QName CMD_CANCEL = NAMESPACE.getQName("Cancel");
  QName CMD_CLEAR = NAMESPACE.getQName("Clear");
  QName CMD_SHIFT = NAMESPACE.getQName("Shift");
  QName CMD_CONTROL = NAMESPACE.getQName("Ctrl");
  QName CMD_ALT = NAMESPACE.getQName("Alt");
  QName CMD_PAUSE = NAMESPACE.getQName("Pause");
  QName CMD_CAPS = NAMESPACE.getQName("Caps Lock");
  QName CMD_ESCAPE = NAMESPACE.getQName("Esc");
  QName CMD_SPACE = NAMESPACE.getQName("Space");
  QName CMD_COMMA = NAMESPACE.getQName("Comma");
  QName CMD_MINUS = NAMESPACE.getQName("Minus");
  QName CMD_PERIOD = NAMESPACE.getQName("Period");
  QName CMD_SLASH = NAMESPACE.getQName("Slash");
  QName CMD_DIGIT0 = NAMESPACE.getQName("0");
  QName CMD_DIGIT1 = NAMESPACE.getQName("1");
  QName CMD_DIGIT2 = NAMESPACE.getQName("2");
  QName CMD_DIGIT3 = NAMESPACE.getQName("3");
  QName CMD_DIGIT4 = NAMESPACE.getQName("4");
  QName CMD_DIGIT5 = NAMESPACE.getQName("5");
  QName CMD_DIGIT6 = NAMESPACE.getQName("6");
  QName CMD_DIGIT7 = NAMESPACE.getQName("7");
  QName CMD_DIGIT8 = NAMESPACE.getQName("8");
  QName CMD_DIGIT9 = NAMESPACE.getQName("9");
  QName CMD_SEMICOLON = NAMESPACE.getQName("Semicolon");
  QName CMD_EQUALS = NAMESPACE.getQName("Equals");
  QName CMD_A = NAMESPACE.getQName("A");
  QName CMD_B = NAMESPACE.getQName("B");
  QName CMD_C = NAMESPACE.getQName("C");
  QName CMD_D = NAMESPACE.getQName("D");
  QName CMD_E = NAMESPACE.getQName("E");
  QName CMD_F = NAMESPACE.getQName("F");
  QName CMD_G = NAMESPACE.getQName("G");
  QName CMD_H = NAMESPACE.getQName("H");
  QName CMD_I = NAMESPACE.getQName("I");
  QName CMD_J = NAMESPACE.getQName("J");
  QName CMD_K = NAMESPACE.getQName("K");
  QName CMD_L = NAMESPACE.getQName("L");
  QName CMD_M = NAMESPACE.getQName("M");
  QName CMD_N = NAMESPACE.getQName("N");
  QName CMD_O = NAMESPACE.getQName("O");
  QName CMD_P = NAMESPACE.getQName("P");
  QName CMD_Q = NAMESPACE.getQName("Q");
  QName CMD_R = NAMESPACE.getQName("R");
  QName CMD_S = NAMESPACE.getQName("S");
  QName CMD_T = NAMESPACE.getQName("T");
  QName CMD_U = NAMESPACE.getQName("U");
  QName CMD_V = NAMESPACE.getQName("V");
  QName CMD_W = NAMESPACE.getQName("W");
  QName CMD_X = NAMESPACE.getQName("X");
  QName CMD_Y = NAMESPACE.getQName("Y");
  QName CMD_Z = NAMESPACE.getQName("Z");
  QName CMD_OPEN_BRACKET = NAMESPACE.getQName("Open Bracket");
  QName CMD_BACK_SLASH = NAMESPACE.getQName("Back Slash");
  QName CMD_CLOSE_BRACKET = NAMESPACE.getQName("Close Bracket");
  QName CMD_NUMPAD0 = NAMESPACE.getQName("Numpad 0");
  QName CMD_NUMPAD1 = NAMESPACE.getQName("Numpad 1");
  QName CMD_NUMPAD2 = NAMESPACE.getQName("Numpad 2");
  QName CMD_NUMPAD3 = NAMESPACE.getQName("Numpad 3");
  QName CMD_NUMPAD4 = NAMESPACE.getQName("Numpad 4");
  QName CMD_NUMPAD5 = NAMESPACE.getQName("Numpad 5");
  QName CMD_NUMPAD6 = NAMESPACE.getQName("Numpad 6");
  QName CMD_NUMPAD7 = NAMESPACE.getQName("Numpad 7");
  QName CMD_NUMPAD8 = NAMESPACE.getQName("Numpad 8");
  QName CMD_NUMPAD9 = NAMESPACE.getQName("Numpad 9");
  QName CMD_MULTIPLY = NAMESPACE.getQName("Multiply");
  QName CMD_ADD = NAMESPACE.getQName("Add");
  QName CMD_SEPARATOR = NAMESPACE.getQName("Separator");
  QName CMD_SUBTRACT = NAMESPACE.getQName("Subtract");
  QName CMD_DECIMAL = NAMESPACE.getQName("Decimal");
  QName CMD_DIVIDE = NAMESPACE.getQName("Divide");
  QName CMD_NUM_LOCK = NAMESPACE.getQName("Num Lock");
  QName CMD_SCROLL_LOCK = NAMESPACE.getQName("Scroll Lock");
  QName CMD_F1 = NAMESPACE.getQName("F1");
  QName CMD_F2 = NAMESPACE.getQName("F2");
  QName CMD_F3 = NAMESPACE.getQName("F3");
  QName CMD_F4 = NAMESPACE.getQName("F4");
  QName CMD_F5 = NAMESPACE.getQName("F5");
  QName CMD_F6 = NAMESPACE.getQName("F6");
  QName CMD_F7 = NAMESPACE.getQName("F7");
  QName CMD_F8 = NAMESPACE.getQName("F8");
  QName CMD_F9 = NAMESPACE.getQName("F9");
  QName CMD_F10 = NAMESPACE.getQName("F10");
  QName CMD_F11 = NAMESPACE.getQName("F11");
  QName CMD_F12 = NAMESPACE.getQName("F12");
  QName CMD_F13 = NAMESPACE.getQName("F13");
  QName CMD_F14 = NAMESPACE.getQName("F14");
  QName CMD_F15 = NAMESPACE.getQName("F15");
  QName CMD_F16 = NAMESPACE.getQName("F16");
  QName CMD_F17 = NAMESPACE.getQName("F17");
  QName CMD_F18 = NAMESPACE.getQName("F18");
  QName CMD_F19 = NAMESPACE.getQName("F19");
  QName CMD_F20 = NAMESPACE.getQName("F20");
  QName CMD_F21 = NAMESPACE.getQName("F21");
  QName CMD_F22 = NAMESPACE.getQName("F22");
  QName CMD_F23 = NAMESPACE.getQName("F23");
  QName CMD_F24 = NAMESPACE.getQName("F24");
  QName CMD_PRINTSCREEN = NAMESPACE.getQName("Print Screen");
  QName CMD_INSERT = NAMESPACE.getQName("Insert");
  QName CMD_HELP = NAMESPACE.getQName("Help");
  QName CMD_META = NAMESPACE.getQName("Meta");
  QName CMD_BACK_QUOTE = NAMESPACE.getQName("Back Quote");
  QName CMD_QUOTE = NAMESPACE.getQName("Quote");
  QName CMD_KP_UP = NAMESPACE.getQName("Numpad Up");
  QName CMD_KP_DOWN = NAMESPACE.getQName("Numpad Down");
  QName CMD_KP_LEFT = NAMESPACE.getQName("Numpad Left");
  QName CMD_KP_RIGHT = NAMESPACE.getQName("Numpad Right");
  QName CMD_DEAD_GRAVE = NAMESPACE.getQName("Dead Grave");
  QName CMD_DEAD_ACUTE = NAMESPACE.getQName("Dead Acute");
  QName CMD_DEAD_CIRCUMFLEX = NAMESPACE.getQName("Dead Circumflex");
  QName CMD_DEAD_TILDE = NAMESPACE.getQName("Dead Tilde");
  QName CMD_DEAD_MACRON = NAMESPACE.getQName("Dead Macron");
  QName CMD_DEAD_BREVE = NAMESPACE.getQName("Dead Breve");
  QName CMD_DEAD_ABOVEDOT = NAMESPACE.getQName("Dead Abovedot");
  QName CMD_DEAD_DIAERESIS = NAMESPACE.getQName("Dead Diaeresis");
  QName CMD_DEAD_ABOVERING = NAMESPACE.getQName("Dead Abovering");
  QName CMD_DEAD_DOUBLEACUTE = NAMESPACE.getQName("Dead Doubleacute");
  QName CMD_DEAD_CARON = NAMESPACE.getQName("Dead Caron");
  QName CMD_DEAD_CEDILLA = NAMESPACE.getQName("Dead Cedilla");
  QName CMD_DEAD_OGONEK = NAMESPACE.getQName("Dead Ogonek");
  QName CMD_DEAD_IOTA = NAMESPACE.getQName("Dead Iota");
  QName CMD_DEAD_VOICED_SOUND = NAMESPACE.getQName("Dead Voiced Sound");
  QName CMD_DEAD_SEMIVOICED_SOUND = NAMESPACE.getQName("Dead Semivoiced Sound");
  QName CMD_AMPERSAND = NAMESPACE.getQName("Ampersand");
  QName CMD_ASTERISK = NAMESPACE.getQName("Asterisk");
  QName CMD_QUOTEDBL = NAMESPACE.getQName("Double Quote");
  QName CMD_LESS = NAMESPACE.getQName("Less");
  QName CMD_GREATER = NAMESPACE.getQName("Greater");
  QName CMD_BRACELEFT = NAMESPACE.getQName("Left Brace");
  QName CMD_BRACERIGHT = NAMESPACE.getQName("Right Brace");
  QName CMD_AT = NAMESPACE.getQName("At");
  QName CMD_COLON = NAMESPACE.getQName("Colon");
  QName CMD_CIRCUMFLEX = NAMESPACE.getQName("Circumflex");
  QName CMD_DOLLAR = NAMESPACE.getQName("Dollar");
  QName CMD_EURO_SIGN = NAMESPACE.getQName("Euro Sign");
  QName CMD_EXCLAMATION_MARK = NAMESPACE.getQName("Exclamation Mark");
  QName CMD_INVERTED_EXCLAMATION_MARK = NAMESPACE.getQName("Inverted Exclamation Mark");
  QName CMD_LEFT_PARENTHESIS = NAMESPACE.getQName("Left Parenthesis");
  QName CMD_NUMBER_SIGN = NAMESPACE.getQName("Number Sign");
  QName CMD_PLUS = NAMESPACE.getQName("Plus");
  QName CMD_RIGHT_PARENTHESIS = NAMESPACE.getQName("Right Parenthesis");
  QName CMD_UNDERSCORE = NAMESPACE.getQName("Underscore");
  QName CMD_WINDOWS = NAMESPACE.getQName("Windows");
  QName CMD_CONTEXT_MENU = NAMESPACE.getQName("Context Menu");
  QName CMD_FINAL = NAMESPACE.getQName("Final");
  QName CMD_CONVERT = NAMESPACE.getQName("Convert");
  QName CMD_NONCONVERT = NAMESPACE.getQName("Nonconvert");
  QName CMD_ACCEPT = NAMESPACE.getQName("Accept");
  QName CMD_MODECHANGE = NAMESPACE.getQName("Mode Change");
  QName CMD_KANA = NAMESPACE.getQName("Kana");
  QName CMD_KANJI = NAMESPACE.getQName("Kanji");
  QName CMD_ALPHANUMERIC = NAMESPACE.getQName("Alphanumeric");
  QName CMD_KATAKANA = NAMESPACE.getQName("Katakana");
  QName CMD_HIRAGANA = NAMESPACE.getQName("Hiragana");
  QName CMD_FULL_WIDTH = NAMESPACE.getQName("Full Width");
  QName CMD_HALF_WIDTH = NAMESPACE.getQName("Half Width");
  QName CMD_ROMAN_CHARACTERS = NAMESPACE.getQName("Roman Characters");
  QName CMD_ALL_CANDIDATES = NAMESPACE.getQName("All Candidates");
  QName CMD_PREVIOUS_CANDIDATE = NAMESPACE.getQName("Previous Candidate");
  QName CMD_CODE_INPUT = NAMESPACE.getQName("Code Input");
  QName CMD_JAPANESE_KATAKANA = NAMESPACE.getQName("Japanese Katakana");
  QName CMD_JAPANESE_HIRAGANA = NAMESPACE.getQName("Japanese Hiragana");
  QName CMD_JAPANESE_ROMAN = NAMESPACE.getQName("Japanese Roman");
  QName CMD_KANA_LOCK = NAMESPACE.getQName("Kana Lock");
  QName CMD_INPUT_METHOD_ON_OFF = NAMESPACE.getQName("Input Method On/Off");
  QName CMD_CUT = NAMESPACE.getQName("Cut");
  QName CMD_UNDO = NAMESPACE.getQName("Undo");
  QName CMD_AGAIN = NAMESPACE.getQName("Again");
  QName CMD_FIND = NAMESPACE.getQName("Find");
  QName CMD_PROPS = NAMESPACE.getQName("Properties");
  QName CMD_STOP = NAMESPACE.getQName("Stop");
  QName CMD_COMPOSE = NAMESPACE.getQName("Compose");
  QName CMD_ALT_GRAPH = NAMESPACE.getQName("Alt Graph");
  QName CMD_BEGIN = NAMESPACE.getQName("Begin");
  QName CMD_UNDEFINED = NAMESPACE.getQName("Undefined");
  QName CMD_SOFTKEY_0 = NAMESPACE.getQName("Softkey 0");
  QName CMD_SOFTKEY_1 = NAMESPACE.getQName("Softkey 1");
  QName CMD_SOFTKEY_2 = NAMESPACE.getQName("Softkey 2");
  QName CMD_SOFTKEY_3 = NAMESPACE.getQName("Softkey 3");
  QName CMD_SOFTKEY_4 = NAMESPACE.getQName("Softkey 4");
  QName CMD_SOFTKEY_5 = NAMESPACE.getQName("Softkey 5");
  QName CMD_SOFTKEY_6 = NAMESPACE.getQName("Softkey 6");
  QName CMD_SOFTKEY_7 = NAMESPACE.getQName("Softkey 7");
  QName CMD_SOFTKEY_8 = NAMESPACE.getQName("Softkey 8");
  QName CMD_SOFTKEY_9 = NAMESPACE.getQName("Softkey 9");
  QName CMD_GAME_A = NAMESPACE.getQName("Game A");
  QName CMD_GAME_B = NAMESPACE.getQName("Game B");
  QName CMD_GAME_C = NAMESPACE.getQName("Game C");
  QName CMD_GAME_D = NAMESPACE.getQName("Game D");
  QName CMD_STAR = NAMESPACE.getQName("Star");
  QName CMD_POUND = NAMESPACE.getQName("Pound");
  QName CMD_POWER = NAMESPACE.getQName("Power");
  QName CMD_INFO = NAMESPACE.getQName("Info");
  QName CMD_COLORED_KEY_0 = NAMESPACE.getQName("Colored Key 0");
  QName CMD_COLORED_KEY_1 = NAMESPACE.getQName("Colored Key 1");
  QName CMD_COLORED_KEY_2 = NAMESPACE.getQName("Colored Key 2");
  QName CMD_COLORED_KEY_3 = NAMESPACE.getQName("Colored Key 3");
  QName CMD_EJECT_TOGGLE = NAMESPACE.getQName("Eject");
  QName CMD_PLAY = NAMESPACE.getQName("Play");
  QName CMD_RECORD = NAMESPACE.getQName("Record");
  QName CMD_FAST_FWD = NAMESPACE.getQName("Fast Forward");
  QName CMD_REWIND = NAMESPACE.getQName("Rewind");
  QName CMD_TRACK_PREV = NAMESPACE.getQName("Previous Track");
  QName CMD_TRACK_NEXT = NAMESPACE.getQName("Next Track");
  QName CMD_CHANNEL_UP = NAMESPACE.getQName("Channel Up");
  QName CMD_CHANNEL_DOWN = NAMESPACE.getQName("Channel Down");
  QName CMD_VOLUME_UP = NAMESPACE.getQName("Volume Up");
  QName CMD_VOLUME_DOWN = NAMESPACE.getQName("Volume Down");
  QName CMD_MUTE = NAMESPACE.getQName("Mute");
  QName CMD_COMMAND = NAMESPACE.getQName("Command");
  QName CMD_SHORTCUT = NAMESPACE.getQName("Shortcut");

  //TODO Internationalize - current setting is for Germany
  char DECIMAL_SEPARATOR = ',';
  char THOUSAND_SEPARATOR = '.';


  /** Device is moving, all buttons released */
  int STATE_MOVING = 1;

  /** Button went down, next state will be DRAGGING, SELECTED or LOCAL_MENU */
  int STATE_POINTING = 2;

  /** Button went up */
  int STATE_SELECTED = 3;

  /** Buttopn id down and device is moving */
  int STATE_DRAGGING = 4;

  /** Menu button (right) went up */
  int STATE_LOCAL_MENU = 5;

  /** Button went up sencond time on same position */
  int STATE_DOUBLECLICK = 6;

  /** Mouse scroll event */
  int STATE_SCROLL = 7;

  /** Add window to the windows this InputManager listens to.
   *  Before adding window the common parent frame have to be added to this
   *  InputManager.
   *  @param dialog the rootContainer to be added
   */
  void addDialog( Dialog dialog);

  /** Remove window from the windows this InputManager listens to.
   *  @param dialog the rootContainer to be removed
   */
  void removeDialog(Dialog dialog);

  void setMouseBlocked ();

  void setMouseReleased ();

  void setDragComponent ( Component activeScroller);

  boolean isLetter( char keyChar);

  /**
   * Pastes text from clipboard
   * @return the text from clipboard or null
   */
  public String paste();

  /**
   * Copys text to clipboard
   * @param text
   */
  public void copy( String text );

  Screen getScreen();
}
