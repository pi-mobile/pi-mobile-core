/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.file;

import de.pidata.gui.guidef.GuiService;
import de.pidata.qnames.QName;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

public class FileChooserResult extends AbstractParameterList {

  public static final QName PARAM_FILE_PATH = GuiService.NAMESPACE.getQName( "filePath" );

  public FileChooserResult() {
    super( ParameterType.StringType, PARAM_FILE_PATH );
  }

  public FileChooserResult( String filePath ) {
    this();
    setString( PARAM_FILE_PATH, filePath );
  }

  public String getFilePath() {
    return getString( PARAM_FILE_PATH );
  }
}
