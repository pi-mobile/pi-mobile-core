/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.file;

import de.pidata.gui.guidef.GuiService;
import de.pidata.qnames.QName;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

public class FileChooserParameter extends AbstractParameterList {

  public static final String TYPE_OPEN_FILE = "openFile";
  public static final String TYPE_SAVE_FILE = "saveFile";
  public static final String TYPE_OPEN_DIR = "openDir";

  public static final QName PARAM_DIR_PATH = GuiService.NAMESPACE.getQName( "dirPath" );
  public static final QName PARAM_FILE_PATTERN = GuiService.NAMESPACE.getQName( "filePattern" );

  public static final QName PARAM_DIALOG_TYPE = GuiService.NAMESPACE.getQName( "dialogType" );

  public FileChooserParameter() {
    super( ParameterType.StringType, PARAM_DIR_PATH, ParameterType.StringType, PARAM_FILE_PATTERN, ParameterType.StringType, PARAM_DIALOG_TYPE );
  }

  public FileChooserParameter( String dirPath, String filePattern, String dialogType ) {
    this();
    setString( PARAM_DIR_PATH, dirPath );
    setString( PARAM_FILE_PATTERN, filePattern );
    setString( PARAM_DIALOG_TYPE, dialogType );
  }

  public FileChooserParameter( String dirPath, String filePattern ) {
    this( dirPath, filePattern, TYPE_OPEN_FILE );
  }

    public String getDirPath()  {
    return getString( PARAM_DIR_PATH );
  }

  public String getFilePattern()  {
    return getString( PARAM_FILE_PATTERN );
  }

  public String getDialogType()  {
    return getString( PARAM_DIALOG_TYPE );
  }
}
