/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.models.tree.ModelSource;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

public interface ControllerGroup extends Controller, ModelSource {

  public QName getName();

  /**
   * Returns this group's parent group or null if this is a DialogController.
   * @return this group's parent group or null
   */
  public ControllerGroup getParentGroup();  //TODO Controller-Hierarchie überdenken

  /**
   * Returns this ModelSource's context, e.g. to access global data structures
   * @return this ModelSource's context
   */
  public Context getContext(); // TODO müsste identisch sein mit getDialogController().getContext()

  /**
   * Returns this group's child having id controllerID or null. If no direct child is found
   * all ControllerGroups are searched recursively for a matching controller.
   * @param controllerID the id of the controller to be returned
   * @return child controller or null
   */
  public Controller getController( QName controllerID );

  Controller getNextFocusable( Controller fromController, boolean forward );

  Model fetchDataSource( QName dataSourceID );

  Iterable<Controller> controllerIterator();

  DialogController getDialogController();
}
