/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.guidef.GuiService;
import de.pidata.qnames.QName;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

/**
 * Created by pru on 28.04.16.
 */
public class QuestionBoxParams extends AbstractParameterList {

  public static final QName PARAM_TITLE = GuiService.NAMESPACE.getQName( "Title" );
  public static final QName PARAM_MESSAGE = GuiService.NAMESPACE.getQName( "Message" );
  public static final QName PARAM_INPUT = GuiService.NAMESPACE.getQName( "Input" );
  public static final QName PARAM_LABEL_YES_OK = GuiService.NAMESPACE.getQName( "LabelYesOk" );
  public static final QName PARAM_LABEL_NO = GuiService.NAMESPACE.getQName( "LabelNo" );
  public static final QName PARAM_LABEL_CANCEL = GuiService.NAMESPACE.getQName( "LabelCancel" );

  public QuestionBoxParams() {
    super( 6 );
  }

  /**
   * Parameter for Question Dialog with a single OK button
   * @param title          the title to show on question box
   * @param message        the message to show
   */
  public QuestionBoxParams( String title, String message ) {
    this( title, message, "", "Ok", null, null );
  }

  /**
   * Parameter for Question Dialog
   * @param title          the title to show on question box
   * @param message        the message to show
   * @param btnYesLabel    the label for yes/ok button or null to hide button
   * @param btnNoLabel     the label for no button or null to hide button
   * @param btnCancelLabel the label for cancel button or null to hide button
   */
  public QuestionBoxParams( String title, String message, String defaulValue, String btnYesLabel, String btnNoLabel, String btnCancelLabel ) {
    super( 6 );
    defineParam( 0, ParameterType.StringType, PARAM_TITLE, title );
    defineParam( 1, ParameterType.StringType, PARAM_MESSAGE, message );
    defineParam( 2, ParameterType.StringType, PARAM_INPUT, defaulValue );
    defineParam( 3, ParameterType.StringType, PARAM_LABEL_YES_OK, btnYesLabel );
    defineParam( 4, ParameterType.StringType, PARAM_LABEL_NO, btnNoLabel );
    defineParam( 5, ParameterType.StringType, PARAM_LABEL_CANCEL, btnCancelLabel );
  }

  public String getTitle() {
    return getString( PARAM_TITLE );
  }

  public String getMessage() {
    return getString( PARAM_MESSAGE );
  }

  public String getDefaultValue() {
    return getString( PARAM_INPUT );
  }

  public String getLabelYesOk() {
    return getString( PARAM_LABEL_YES_OK );
  }

  public String getLabelNo() {
    return getString( PARAM_LABEL_NO );
  }

  public String getLabelCancel() {
    return getString( PARAM_LABEL_CANCEL );
  }
}
