package de.pidata.gui.controller.base;

public enum ImageChooserType {
  CAMERA_ONLY,
  GALLERY_ONLY,
  CAMERA_AND_GALLERY
}
