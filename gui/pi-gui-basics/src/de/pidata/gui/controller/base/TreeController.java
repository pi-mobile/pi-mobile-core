/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.guidef.NodeType;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.gui.view.base.TreeViewPI;
import de.pidata.models.binding.Binding;
import de.pidata.models.binding.ModelBinding;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelSource;
import de.pidata.qnames.QName;

/**
 * Created by pru on 12.07.2016.
 */
public interface TreeController extends ValueController, ModelSource {

  void init( QName id, ControllerGroup ctrlGroup, TreeViewPI treeViewPI, Binding binding, ModelBinding treeBinding, GuiOperation nodeAction, boolean readOnly, boolean lazyLoading, boolean shoRootNode );

  void addNodeType( NodeType nodeDef );

  void updateChildList( TreeNodePI treeNodePI );

  TreeNodePI getSelectedNode();

  void onSelectionChanged( TreeNodePI treeNode );

  void setSelectedModel( Model newSelectedModel );

  void editNode( TreeNodePI node );

  void onStopEdit( TreeNodePI editedNode, QName valueID, Object newValue );

  NodeType getNodeType( QName typeName );

  TreeNodePI getRootNode();

}
