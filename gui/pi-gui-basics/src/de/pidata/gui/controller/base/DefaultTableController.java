/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.guidef.InplaceEdit;
import de.pidata.gui.ui.base.UIButtonAdapter;
import de.pidata.gui.view.base.ButtonViewPI;
import de.pidata.gui.view.base.ListViewPI;
import de.pidata.gui.view.base.TableViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.binding.*;
import de.pidata.models.tree.*;
import de.pidata.qnames.QName;

import java.util.ArrayList;
import java.util.List;

public class DefaultTableController extends AbstractSelectionController implements TableController {

  private TableViewPI      tableViewPI;
  private TableDelegate    tableDelegate;
  private List<ColumnInfo> columns = new ArrayList<ColumnInfo>();
  private ColumnInfo       editColumn = null;
  private Model            editRow = null;
  private InplaceEdit inplaceEdit = InplaceEdit.none;
  private ValueController editor = null;
  private boolean ignoreUIEvents = false;
  private boolean filterActive = false;
  private boolean selectable;

  public void init( QName id, ControllerGroup parent, TableViewPI tableViewPI, Binding valueBinding,
                    Selection selection, boolean readOnly, boolean selectable, InplaceEdit inplaceEdit, GuiOperation rowAction ) {
    this.tableViewPI = tableViewPI;
    tableViewPI.setController( this );
    if (inplaceEdit == null) {
      this.inplaceEdit = InplaceEdit.none;
    }
    else {
      this.inplaceEdit = inplaceEdit;
    }
    this.selectable = selectable;

    super.init( id, parent, valueBinding, selection, rowAction, readOnly );
  }

  /**
   * Returns the view used for selection
   *
   * @return the selection component
   */
  public ListViewPI getListView() {
    return getTableView();
  }

  /**
   * Returns this Table's view. Result is same like getListView() but
   * avoid's casting.
   *
   * @return this Table's view
   */
  @Override
  public TableViewPI getTableView() {
    return tableViewPI;
  }

  public ViewPI getView() {
    return tableViewPI;
  }

  @Override
  public boolean hasFirstRowForEmptySelection() {
    return Platform.getInstance().hasTableFirstRowForEmptySelection();
  }

  public void addColumn( ColumnInfo column ) {
    this.columns.add( column );
  }

  public int columnCount() {
    return columns.size();
  }

  public ColumnInfo getColumn( int index ) {
    if (index >= 0 && index < columns.size()) {
      return columns.get( index );
    }
    return null;
  }

  public ColumnInfo getColumnByName( QName colName ) {
    ColumnInfo colInfo = null;
    short col;
    for (col = (short) (columns.size()-1); col >= 0; col--) {
      colInfo = columns.get( col );
      if (colInfo.getColName() == colName) {
        return colInfo;
      }
    }
    return null;
  }

  public ColumnInfo getColumnByHeaderID( QName headerID ) {
    for (short i = 0; i < columnCount(); i++ ) {
      ColumnInfo colInfo = getColumn( i );
      QName colHeadName = colInfo.getHeaderCompID();
      if (colHeadName == headerID) {
        return colInfo;
      }
    }
    return null;
  }

  public ColumnInfo getColumnByBodyCompID( QName bodyCompID ) {
    for (short i = 0; i < columnCount(); i++ ) {
      ColumnInfo colInfo = getColumn( i );
      QName compID = colInfo.getBodyCompID();
      if (compID == bodyCompID) {
        return colInfo;
      }
    }
    return null;
  }

  private short getColIndex( QName colName ) {
    ColumnInfo colInfo = null;
    short col;
    for (col = (short) (columns.size()-1); col >= 0; col--) {
      colInfo = columns.get( col );
      if (colInfo.getColName() == colName) {
        return col;
      }
    }
    return -1;
  }

  @Override
  protected void viewUpdateRow( Model changedRow ) {
    tableViewPI.updateRow( changedRow );
  }

  @Override
  protected void viewRemoveRow( Model removedRow ) {
    tableViewPI.removeRow( removedRow );
  }

  @Override
  protected void viewInsertRow( Model newRow, Model beforeRow ) {
    tableViewPI.insertRow( newRow, beforeRow );
  }

  public void viewUpdateAllRows() {
    ignoreUIEvents = true;
    ignoreModelEvents = true;

    tableViewPI.removeAllDisplayRows();

    //--- set empty value if allowed
    if (hasFirstRowForEmptySelection()) {
      tableViewPI.insertRow( null, null );
    }

    //--- update given range of rows
    for (ModelIterator rowIter = selection.valueIter(); rowIter.hasNext(); ) {
      Model row = rowIter.next();
      tableViewPI.insertRow( row, null );
    }
    tableViewPI.finishedUpdateAllRows();
    ignoreModelEvents = false;
    ignoreUIEvents = false;
  }

  protected void viewSetSelection() {
    for (int i = 0; i < selection.selectedValueCount(); i++) {
      tableViewPI.setSelected( selection.getSelectedValue( i ), true );
    }
  }

  /**
   * Callend whenever a validation error occurs
   *
   * @param vex
   */
  protected void setValidationError( ValidationException vex ) {
    if(vex != null) {
      //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
      throw new RuntimeException( "TODO" );
    }
  }

  public boolean isEditable() {
    return inplaceEdit != InplaceEdit.none;
  }

  @Override
  public boolean isSelectable() {
    return this.selectable;
  }

  /**
   * Notifies this table that editing has been started on given cell
   *
   * @param editRow    data row on which editing has been started
   * @param columnInfo column in which editing has been started
   */
  @Override
  public void onStartEdit( Model editRow, ColumnInfo columnInfo ) {
    this.editRow = editRow;
    this.editColumn = columnInfo;
  }

  /**
   * Notifies this table that editing has stopped on given cell. Table now
   * has to store the newValue in the editedRow.
   *
   * @param editedRow  data row on which editing has been stopped
   * @param columnInfo column in which editing has been stopped
   * @param newValue   new value for the given cell
   */
  @Override
  public void onStopEdit( Model editedRow, ColumnInfo columnInfo, Object newValue ) {
    Controller editCtrl = columnInfo.getEditCtrl();
    ModelSource modelSource = new SimpleModelSource( null, editedRow );
    Binding binding = ((ValueController) editCtrl).getValueBinding(); //TODO
    binding.setModelSource( modelSource, binding.getModelPath() );
    binding.storeModelValue( newValue );
    this.editColumn = null;
    this.editRow = null;
  }

  public boolean isFocusable() {
    return rowCount() > 0;
  }

  /**
   * Called by TableViewPI.onSelectedCell()
   *
   * @param selectedRow the row to which the selected cell belongs
   * @param columnInfo  the column tn which the selected cell belongs
   */
  public void onSelectedCell( Model selectedRow, ColumnInfo columnInfo ) {
    if (!ignoreUIEvents) {
      onSelectionChanged( selectedRow, true );
      Controller renderCtrl = null;
      if (columnInfo != null) {
        renderCtrl = columnInfo.getRenderCtrl();
      }
      if (renderCtrl instanceof ButtonController) {
        ((ButtonController) renderCtrl).press( selectedRow );
      }
      else if (renderCtrl instanceof DefaultActionController) {
        QName colName = null;
        if (columnInfo != null) {
          colName = columnInfo.getColName();
        }
        GuiOperation cellOperation = renderCtrl.getGuiOperation(null);
        getDialogController().subAction( cellOperation, colName, this, selectedRow );
      }
      else if (rowAction != null) {
        // Fokuswechsel muss passieren, da sonst zuletzt aktives Feld nicht ins Model schreibt
        QName colName = null;
        if (columnInfo != null) {
          colName = columnInfo.getColName();
        }
        getDialogController().subAction( this.rowAction, colName, this, selectedRow );
      }
    }
  }


  /**
   * Called by TableViewPI.onDoubleClickCell()
   *
   * @param selectedRow the row to which the selected cell belongs
   * @param columnInfo  the column tn which the selected cell belongs
   */
  public void onDoubleClickCell( Model selectedRow, ColumnInfo columnInfo ) {
    if (!ignoreUIEvents) {
      onSelectionChanged( selectedRow, true );
      if (rowDoubleClickAction != null) {
        // Fokuswechsel muss passieren, da sonst zuletzt aktives Feld nicht ins Model schreibt
        QName colName = null;
        if (columnInfo != null) {
          colName = columnInfo.getColName();
        }
        getDialogController().subAction( rowDoubleClickAction, colName, this, selectedRow );
      }
    }
  }

  /**
   * Notifies this table controller of a right cllick on a cell by the user. The implementation should
   * not try to modify table's selection. This is done by UI itself.
   *
   * @param selectedRow the row to which the selected cell belongs
   * @param columnInfo the column tn which the selected cell belongs
   */
  @Override
  public void onRightClickCell( Model selectedRow, ColumnInfo columnInfo ) {
    if (!ignoreUIEvents) {
      onSelectionChanged( selectedRow, true );
      if (rowMenuAction != null) {
        // Fokuswechsel muss passieren, da sonst zuletzt aktives Feld nicht ins Model schreibt
        QName colName = null;
        if (columnInfo != null) {
          colName = columnInfo.getColName();
        }
        getDialogController().subAction( rowMenuAction, colName, this, selectedRow );
      }
    }
  }

  /**
   * Called by ListViewPI whenever a row has been selected or deselected
   *
   * @param selectedRow the row which was selected
   * @param selected    true if row has been selected, false if it has been deselected
   */
  public void onSelectionChanged( Model selectedRow, boolean selected ) {
    if (!ignoreUIEvents) {
      super.onSelectionChanged( selectedRow, selected );
    }
  }

  /**
   * Sets delegate for this table
   *
   * @param tableDelegate new delegate or null to clear
   */
  @Override
  public void setTableDelegate( TableDelegate tableDelegate ) {
    this.tableDelegate = tableDelegate;
  }

  /**
   * Called by TableView if row filtering has been activated/deactivated
   *
   * @param filterActive true if row filtering is active
   */
  @Override
  public void onFiltered( boolean filterActive ) {
    if (this.filterActive != filterActive) {
      this.filterActive = filterActive;
      getSelection().clear();  // Important workaround for JavaFX (see FXTableAdapter) // FIXME: does not work for selType = SINGLE by now!
      if (tableDelegate != null) {
        tableDelegate.tableFiltered( filterActive );
      }
    }
  }
}
