/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.file.FileChooserParameter;
import de.pidata.gui.event.ComponentListener;
import de.pidata.gui.event.Dialog;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.guidef.DialogDef;
import de.pidata.gui.guidef.DialogType;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.Model;
import de.pidata.models.types.Type;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;
import de.pidata.service.base.ParameterList;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;

public interface DialogController extends MutableControllerGroup, ComponentListener {

  QName POPUP = GuiBuilder.NAMESPACE.getQName( "Popup" );

  /**
   * Set the dialog's properties. If the dialog definition has defined an onCreate-service call, this will be invoked.
   * @param dialogDef the dialog definition
   * @param dialogBuilder the builder used to build this dialog
   * @param context the context in which the dialog will find its data
   * @param delegate
   * @param modelType the expected model's type
   * @param namespaceTable
   */
  void init( DialogType dialogDef, ControllerBuilder dialogBuilder, Context context, DialogControllerDelegate delegate, Type modelType, NamespaceTable namespaceTable);

  void init( QName id, String title, boolean fullScreen, ControllerBuilder controllerBuilder, Context context,
             DialogControllerDelegate delegate, String modelPath, NamespaceTable namespaceTable );

  /**
   * Sets parameterList, this must have happened before calling activate()
   *
   * @param parameterList dialog parameters
   */
  void setParameterList( ParameterList parameterList ) throws Exception;

  /**
   * Returns this DialogController's NamespaceTable, needed e.g. fopr evaluating qualified names
   * in XPath expressions. If dialog was created by ControllerBuilder that is the namespace table
   * created from xmlns definition in the XML GUI file.
   * @return NamespaceTable
   */
  NamespaceTable getNamespaceTable();

  /**
   * Called by platform native code when saving UI state
   * @return
   */
  QName getLastGuiOpID();

  /**
   * Called by platform native code when restoring UI state
   * @param lastGuiOpID
   */
  void setLastGuiOpID( QName lastGuiOpID );

  /**
   * Called by dialog builder after having finished creating the dialog, i.e. after
   * having added all children and before refreshBindings() is called.
   */
  void created();

  /**
   * Returns this dialog's title
   * @return this dialog's title
   */
  String getTitle();

  /**
   * Returns this dialog's delegate
   * @return this dialog's delegate or null
   */
  DialogControllerDelegate getDelegate();

  /**
   * Set or changes this dialog's title
   * @param title the new title
   */
  void setTitle( String title );

  /**
   * Adds a command shortcut to this dialog
   * @param command the command (e.g. a key combination)
   * @param guiOp       the operation to execute when command is given
   * @param controller the controller to fire on
   */
  void addShortCut( QName command, GuiOperation guiOp, ActionController controller );

  /**
   * Opens child dialog with given dialogID and title
   *
   * @param dialogID      ID of the dialog to be opened
   * @param title         title for the dialog, if null title from dialog resource is used
   * @param parameterList dialog parameter list
   */
  void openChildDialog( QName dialogID, String title, ParameterList parameterList );

  /**
   * Opens child dialog with given definition {@link DialogType} and title
   *
   * @param dialogType    user interface definition for the dialog to be opened
   * @param title         title for the dialog, if null title from dialogDef is used
   * @param parameterList dialog parameter list
   */
  void openChildDialog( DialogType dialogType, String title, ParameterList parameterList );

  /**
   * Opens child dialog with given definition {@link DialogDef} and title
   *
   * @param dialogDef     user interface definition for the dialog to be opened
   * @param title         title for the dialog, if null title from dialogDef is used
   * @param parameterList dialog parameter list
   * @deprecated old GUI; use new GUI definition instead!
   */
  @Deprecated
  void openChildDialog( DialogDef dialogDef, String title, ParameterList parameterList );

  /**
   * Opens popup with given moduleID and title
   * @param moduleID      ID of the module to be opened in a Popup
   * @param title         title for the dialog, if null title from dialog resource is used
   * @param parameterList dialog parameter list
   */
  void showPopup( QName moduleID, String title, ParameterList parameterList );

  /**
   * Returns first popup controller. Check childPopup recursive to get current popup
   *
   * @return first up controllen
   */
  ModuleController getPopupController();

  /**
   * Called by PopupController after it has been closed
   *
   * @param popupController the popupController which has been closed
   * @param oldModuleGroup
   */
  void popupClosed( PopupController popupController, ModuleGroup oldModuleGroup );

  /**
   * Called after child dialog has been closed
   *
   * @param ok         true if dialog was closed with ok, false if it was closed with cancel
   * @param resultList list of dialog's result parameters (never null)
   */
  void childDialogClosed( boolean ok, ParameterList resultList );

  /**
   * Show a message dialog
   *
   * @param title   the dialog title
   * @param message the messsage, use CR (\n) for line breaks
   */
  void showMessage( String title, String message );

  /**
   * Show a question dialog dialog
   *
   * @param title          the dialog title
   * @param message        the messsage, use CR (\n) for line breaks
   * @param btnYesLabel    the label for yes/ok button or null to hide button
   * @param btnNoLabel     the label for no button or null to hide button
   * @param btnCancelLabel the label for cancel button or null to hide button
   */
  void showQuestion( String title, String message, String btnYesLabel, String btnNoLabel, String btnCancelLabel );

  /**
   * Show a dialog with a text input field
   *
   * @param title          the dialog title
   * @param message        the messsage, use CR (\n) for line breaks
   * @param defaultValue   a default value for the input field
   * @param btnOKLabel     the label for ok button or null to hide button
   * @param btnCancelLabel the label for cancel button or null to hide button
   */
  void showInput( String title, String message, String defaultValue, String btnOKLabel, String btnCancelLabel );

  /**
   * Shows a progress dialog over this dialog
   *
   * @param title     the dialog title
   * @param message   the messsage, use CR (\n) for line breaks
   * @param fromValue the value representing 0%
   * @param toValue   the value representing 100%
   * @param cancelable true if progress dialog can be canceled by user
   * @return the controller for the created progress dialog
   */
  ProgressController showProgressDialog( String title, String message, int fromValue, int toValue, boolean cancelable );

  /**
   * Shows the platform specific file chooser dialog.
   *
   * @param title             dialog title
   * @param fileChooserParams parameters
   */
  void showFileChooser( String title, FileChooserParameter fileChooserParams );

  /**
   * Shows the platform specific image chooser dialog.
   * <ul>
   *   <li>assure client has rights to write on destination</li>
   *   <li>use {@link SystemManager#getStorage(String)} and {@link Storage#getPath(String)} to get absolute Path on different Platforms</li>
   * </ul>
   *
   * @param imageChooserType type of the image chooser
   * @param filePath         <b>absolute</b> path of file that has to be used, if image will be created (by using camera)
   */
  void showImageChooser( ImageChooserType imageChooserType, String filePath );

  /**
   * Show a toast message, i.e. a message which appears only for a short moment
   * and ten disappears.
   *
   * @param message the message to show
   */
  void showToast( String message );

  /**
   * Returns this dialog's parent dialog controller. This ist only valid if dialog
   * is showing (seee methode show)
   *
   * @return  this dialog's parent dialog controller
   */
  DialogController getParentDialogController();

  /**
   * Stores guiOperation and delegates action processing to ActionThread.
   * @param guiOperation the GUI operation to be executed
   * @param eventID      the id of the event which is reason fpr this subAction
   * @param sourceCtrl       original source of the event, may differ from ctrlGroup, e.g. when closing application
   * @param dataContext
   */
  void subAction( GuiOperation guiOperation, QName eventID, Controller sourceCtrl, Model dataContext );


  /**
   * Sets operation to be invoked if dialog is going to be closed. If not null
   * the closeOperation is responsible for closing this dialog by calling doClose()
   * and must not call close().
   *
   * @param closeOperation close operation or null
   */
  void setOnCloseOperation( GuiOperation closeOperation );

  /**
   * Returns on close operation, see setOnCloseOperation() for details.
   *
   * @return the current on close operation, may be null
   */
  GuiOperation getOnCloseOperation();

  /**
   * Closes this dialog by calling doClose(), if dialogComp.closing() returns true
   * and close operation is null.
   *
   * If dialogComp.closing() returns true and close operation is not null that
   * close operation is called and its up to that operation to call doClose() to
   * finally close this dialog.
   *
   * @param ok true if dialog is closed with OK
   * @return true if dialog has been closed successfully,
   *         false if a close operation has been called
   */
  boolean close( boolean ok );

  /**
   * Closes this dialog by calling doClose(), if dialogComp.closing() returns true
   * and close operation is null.
   *
   * If dialogComp.closing() returns true and close operation is not null that
   * close operation is called and its up to that operation to call doClose() to
   * finally close this dialog.
   *
   * @param ok true if dialog is closed with OK
   * @param source original source of the event
   * @return true if dialog has been closed successfully,
   *         false if a close operation has been called
   */
  boolean close( boolean ok, DialogController source );

  /**
   * Closes this dialog, does not call close operation
   *
   * @param ok true if dialog is closed with OK
   */
  void doClose( boolean ok );

  /**
   * Called by Platforms having a special "Back-Button" (e.g. Android) if that Button is pressed.
   */
  void onBackPressed();

  /**
   * Returns the builder used for creating controllers
   * @return  the current controller builder
   */
  ControllerBuilder getControllerBuilder();

  /**
   * Called by a child controller to notify it has gained focus
   * @param focusController
   */
  void setFocusController( Controller focusController );

  Dialog getDialogComp();

  void setDialogComp( Dialog dialog );

  /**
   * Method for internal usage only
   * @param dialogController
   */
  void setChildDialogController( DialogController dialogController );

  /**
   * Returns this dialog's return model
   * @return this dialog's return model
   */
  ParameterList getReturnList();

  Controller getFocusController();

  Model getOriginalModel();

  void setModel( Model model );

  void addController( QName controllerID, Controller controller );

  boolean validate();

  void setValidator( DialogValidator dialogValidator );
}
