/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.view.base.*;
import de.pidata.models.binding.Binding;
import de.pidata.models.binding.ModelBinding;
import de.pidata.qnames.QName;

public interface TreeTableController extends TreeController {

  void init( QName id, ControllerGroup ctrlGroup, TreeTableViewPI treeTableViewPI, Binding rowBinding, ModelBinding treeBinding, GuiOperation nodeAction, boolean readOnly, boolean lazyLoading, boolean showRootNode );

  void addColumn( ColumnInfo columnInfo );

  int columnCount();

  ColumnInfo getColumn( short i );

  ColumnInfo getColumnByBodyCompID( QName columnID );

  /**
   * Notifies this table controller of a cell selection by the user. The implementation should
   * not try to modify table's selection. This is done by UI itself.
   * @param selectedNode  the node to which the selected cell belongs
   * @param columnInfo    the column tn which the selected cell belongs
   */
  void onSelectedCell( TreeNodePI selectedNode, ColumnInfo columnInfo );

  /**
   * Notifies this table controller of a double cllick on a cell by the user. The implementation should
   * not try to modify table's selection. This is done by UI itself.
   * @param selectedNode  the node to which the selected cell belongs
   * @param columnInfo    the column tn which the selected cell belongs
   */
  void onDoubleClickCell( TreeNodePI selectedNode, ColumnInfo columnInfo );

  /**
   * Notifies this table controller of a right cllick on a cell by the user. The implementation should
   * not try to modify table's selection. This is done by UI itself.
   * @param selectedNode  the node to which the selected cell belongs
   * @param columnInfo    the column tn which the selected cell belongs
   */
  void onRightClickCell( TreeNodePI selectedNode, ColumnInfo columnInfo );

}