/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

public class PopupController extends ModuleController {

  private PopupController childPopup;
  private PopupController parentPopup;

  public PopupController( PopupController parentPopup ) {
    this.parentPopup = parentPopup;
  }

  public PopupController getChildPopup() {
    return childPopup;
  }

  public PopupController getParentPopup() {
    return parentPopup;
  }

  public void setChildPopup( PopupController childPopup ) {
    this.childPopup = childPopup;
  }

  public void closePopup() {
    getDialogController().getDialogComp().closePopup( this );
  }

  /**
   * Called by DialogController after childPopup has been closed.
   *
   * @param childPopup the child which has been closed
   */
  public void childPopupClosed( PopupController childPopup ) {
    this.childPopup = null;
  }

  @Override
  public void moduleDeactivated( ModuleGroup moduleGroup ) {
    // Module has been deactivated - now we can remove it from Popup-Hierarchy
    getDialogController().popupClosed( this, moduleGroup );
  }
}
