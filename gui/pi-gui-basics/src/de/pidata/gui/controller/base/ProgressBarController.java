/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.component.base.ProgressTask;
import de.pidata.gui.component.base.TaskHandler;
import de.pidata.gui.guidef.InputMode;
import de.pidata.gui.view.base.ProgressBarViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.binding.AttributeBinding;
import de.pidata.models.binding.Binding;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ValidationException;
import de.pidata.models.types.Type;
import de.pidata.models.types.simple.DecimalObject;
import de.pidata.models.types.simple.DecimalType;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;


/**
 * Handles a decimal value as progress. Provides helper for starting the
 * monitored action within a separate thread.
 *
 * @author Doris Schwarzmaier
 */
public class ProgressBarController extends AbstractValueController implements ProgressController {

  public static final Namespace NAMESPACE = Namespace.getInstance("de.pidata.gui");

  protected ProgressBarViewPI progressBarViewPI;
  private TaskHandler progressTaskHandler;

  private Type valueType = DecimalType.getDefault();
  private Namespace valueNS;

  private String errorMessage;
  private boolean hasError;
  public String getErrorMessage() {
    return errorMessage;
  }
  public boolean hasError() {
    return hasError;
  }


  public void init( QName id, Controller parent, ProgressBarViewPI progressBarViewPI, boolean readOnly ) {
    this.init( id, parent, progressBarViewPI, null, readOnly, 0, 100 );
  }

  public void init( QName id, Controller parent, ProgressBarViewPI progressBarViewPI, Binding binding, boolean readOnly ) {
    this.init( id, parent, progressBarViewPI, binding, readOnly, 0, 100 );
  }

  public void init( QName id, Controller parent, ProgressBarViewPI progressBarViewPI, boolean readOnly, int minValue, int maxValue ) {
    this.init( id, parent, progressBarViewPI, null, readOnly, minValue, maxValue );
  }

  public void init( QName id, Controller parent, ProgressBarViewPI progressBarViewPI, Binding binding, boolean readOnly, int minValue, int maxValue ) {
    this.progressBarViewPI = progressBarViewPI;
    progressBarViewPI.setController( this );
    progressBarViewPI.setInputMode( InputMode.none );
    progressBarViewPI.setMinValue( minValue );
    progressBarViewPI.setMaxValue( maxValue );

    if (binding != null) {
      this.valueType = binding.getValueType();
    }

    super.init( id, parent, binding, readOnly );
  }

  public void show() {
    // do nothing
  }

  public void finish() {
    // do nothing
  }

  @Override
  public void setCancelAction( Runnable cancelAction ) {
    // do nothing
  }

  /**
   * Return GuiOperation with given guiOpID or null if not found.
   *
   * @param guiOpID GuiOperation having eventID==guiOpID or null
   * @return
   */
  @Override
  public GuiOperation getGuiOperation( QName guiOpID ) {
    return null; // this value controller has no GuiOperations
  }

  public void setNamespace( Namespace valueNS ) {
    this.valueNS = valueNS;
  }

  protected Namespace getNamespace() {
    if (valueNS == null) {
      if (valueBinding != null && valueBinding instanceof AttributeBinding) {
        valueNS = ((AttributeBinding) valueBinding).getAttributeName().getNamespace();
      }
      else {
        throw new RuntimeException( "no namespace given" );
      }
    }
    return valueNS;
  }

  public Object getValue() {
    if (progressBarViewPI != null) {
      return progressBarViewPI.getValue();
    }
    return DecimalObject.ZERO;
  }

  public void viewSetValue( Object value ) {
    if (progressBarViewPI != null) {
      if (value == null) {
        progressBarViewPI.updateValue( DecimalObject.ZERO );
      }
      else {
        progressBarViewPI.updateValue( value );
      }
    }
  }

  public void storeValue( Number newValue, Model dataContext ) {
    if (valueBinding != null) {
      try {
        valueBinding.storeModelValue( newValue, dataContext );
      }
      catch (ValidationException vex) {
        setValidationError( vex );
      }
      catch (Exception ex) {
        Logger.error( "Could not store model value", ex );
      }
    }
  }

  public void setProgress( double progress, String message ) {
    if (progressBarViewPI != null) {
      if (message == null) {
        message = "";
      }
      progressBarViewPI.setProgress( progress, message );
    }
  }

  public void setProgress( double progress ) {
    if (progressBarViewPI != null) {
      progressBarViewPI.setProgress( progress );
    }
  }

  public void startAndObserveTask( ProgressTask progressTask ) {
    if (this.progressTaskHandler != null) {
      // another task is already running
      // FIXME: message
      return;
    }
    progressTaskHandler = progressTask.getTaskHandler();
    progressTaskHandler.startTask( this );
  }

  public void abortTask() {
    if (progressTaskHandler != null) {
      progressTaskHandler.abortTask();
    }
  }

  public ViewPI getView() {
    return progressBarViewPI;
  }

  protected void setValidationError( ValidationException vex ) {
    if (vex != null) {
      Logger.warn( "Validation exception in " + getClass().getName() + ", vex=" + vex.getMessage() );

    }
  }

  /**
   * Called to set an optional max value. this allows prograss messages
   * like 8 of 25
   *
   * @param maxValue the max value, following calls to setProgress mus not
   *                 exceed this value
   */
  @Override
  public void setMaxValue( double maxValue ) {
    if (progressBarViewPI != null) {
      progressBarViewPI.setMaxValue( (int) maxValue );
    }
  }

  /**
   * Updates progress indicator with message and per cent value.
   *
   * @param newMessage the message to display or null
   * @param progress   progress in per cent or -1 for aborted with error
   */
  @Override
  public void updateProgress( String newMessage, double progress ) {
    setProgress(progress, newMessage);
  }

  /**
   * Updates progress indicator with message and per cent value.
   *
   * @param progress progress in per cent or -1 for aborted with error
   */
  @Override
  public void updateProgress( double progress ) {
    setProgress( progress );
  }

  @Override
  public void showError( String errorMessage ) {
    this.errorMessage = errorMessage;
    hasError = true;
    if (progressBarViewPI != null) {
      progressBarViewPI.showError( errorMessage );
    }
  }

  @Override
  public void hideError() {
    errorMessage = "";
    hasError = false;
    if (progressBarViewPI != null) {
      progressBarViewPI.hideError( );
    }
  }

  @Override
  public void showInfo( String infoMessage ) {
    if (progressBarViewPI != null) {
      progressBarViewPI.showInfo( infoMessage );
    }
  }

  @Override
  public void showWarning( String warningMessage ){
    if (progressBarViewPI != null) {
      progressBarViewPI.showWarning ( warningMessage );
    }
  }

  public void resetColor (){
    if (progressBarViewPI != null) {
      progressBarViewPI.resetColor ( );
    }
  }
}
