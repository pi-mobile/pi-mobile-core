/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.guidef.*;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UITableAdapter;
import de.pidata.gui.ui.base.UITreeTableAdapter;
import de.pidata.gui.view.base.AbstractViewPI;
import de.pidata.gui.view.base.ColumnView;
import de.pidata.log.Logger;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.XPath;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;

public class ColumnInfo {

  private Context  context;
  private QName    colName;
  private QName    headerCompID;
  private QName    bodyCompID;
  private Controller renderCtrl;
  private Controller editCtrl;
  private QName    valueID;

  private String    align;
  private QName    iconValueID;
  private QName    helpValueID;
  private QName    backgroundValueID;
  private QName    labelValueID;
  private String   modelRef;
  private boolean  readOnly;
  private boolean  sortable;
  private XPath    path = null;
  private int      index;
  private ColumnView columnView;

  /**
   *  @param context context to use for evaluating Model XPath expressions, typically the ControllerGroup's context this
   *                ColumnInfo's table/tree controller is part of
   * @param index
   * @param namespaceTable
   * @param renderCtrl
   * @param editCtrl
   * @param columnDef
   */
  public ColumnInfo( Context context, int index, NamespaceTable namespaceTable,
                     Controller renderCtrl, Controller editCtrl, ColumnType columnDef,
                     AttrBindingType columnBindingDef, AttrBindingType displayBindingDef ) {
    if (context == null) throw new IllegalArgumentException( "context must not be null!" );
    if (renderCtrl == null) throw new IllegalArgumentException( "renderCtrl must not be null!" );
    this.context = context;
    this.index = index;

    this.bodyCompID = columnDef.getColumnCompID();
    this.colName = columnDef.getID();
    if (this.colName == null) {
      this.colName = this.bodyCompID;
    }
    if (this.colName == null) throw new IllegalArgumentException( "colName must not be null!" );

    if (columnBindingDef != null) {
      this.valueID = columnBindingDef.getAttrName();
      this.modelRef = columnBindingDef.getPath();
    }

    AttrBindingType iconBindingDef = columnDef.getIconAttrBinding();
    this.iconValueID = (iconBindingDef == null) ? null : iconBindingDef.getAttrName();
    AttrBindingType helpBindingDef = columnDef.getHelpAttrBinding();
    this.helpValueID = (helpBindingDef == null) ? null : helpBindingDef.getAttrName();
    AttrBindingType backgroundBindingDef = columnDef.getBackgroundAttrBinding();
    this.backgroundValueID = (backgroundBindingDef == null) ? null : backgroundBindingDef.getAttrName();
    AttrBindingType headerBindingDef = columnDef.getHeaderAttrBinding();
    this.headerCompID = (headerBindingDef == null) ? null : headerBindingDef.getAttrName();
    this.labelValueID = (displayBindingDef == null) ? null : displayBindingDef.getAttrName();

    this.align = columnDef.getAlign();

    this.readOnly = columnDef.readOnly();
    this.sortable = columnDef.sortable();

    this.renderCtrl = renderCtrl;
    this.editCtrl = editCtrl;
    if (modelRef != null && (modelRef.length() > 0) && !modelRef.equals( "." )) {
      if (path == null) {
        path = new XPath( namespaceTable, modelRef );
      }
      else {
        path.append( new XPath( namespaceTable, modelRef ) );
      }
    }
  }

  /**
   * @param context context to use for evaluating Model XPath expressions, typically the ControllerGroup's context this
   *                ColumnInfo's table/tree controller is part of
   * @param index
   * @param colName
   * @param modelRef
   * @param valuePathRef
   * @param valueID
   * @param iconValueID
   * @param headerCompID
   * @param bodyCompID
   * @param renderCtrl
   * @param editCtrl
   * @param readOnly
   * @param sortable
   */
  public ColumnInfo( Context context, int index, QName colName, String modelRef, String valuePathRef,
                     QName valueID, QName iconValueID, QName helpValueID, QName backgroundValueID,
                     QName headerCompID, QName bodyCompID, QName labelValueID, NamespaceTable namespaceTable,
                     Controller renderCtrl, Controller editCtrl,
                     boolean readOnly, boolean sortable ) {
    if (context == null) throw new IllegalArgumentException("context must not be null!");
    if (colName == null) throw new IllegalArgumentException("colName must not be null!");
    if (renderCtrl == null) throw new IllegalArgumentException("renderCtrl must not be null!");
    this.context = context;
    this.index = index;
    this.colName = colName;
    this.modelRef = modelRef;
    this.valueID = valueID;
    this.iconValueID = iconValueID;
    this.helpValueID = helpValueID;
    this.backgroundValueID = backgroundValueID;
    this.labelValueID = labelValueID;
    this.readOnly = readOnly;
    this.sortable = sortable;
    this.headerCompID = headerCompID;
    this.bodyCompID = bodyCompID;
    this.renderCtrl = renderCtrl;
    this.editCtrl = editCtrl;
    if ((modelRef != null) && (modelRef.length() > 0) && !modelRef.equals( "." )) {
      path = new XPath( namespaceTable, modelRef );
    }
    if (valuePathRef != null && (valuePathRef.length() > 0) && !valuePathRef.equals( "." )) {
      if (path == null) {
        path = new XPath( namespaceTable, valuePathRef );
      }
      else {
        path.append( new XPath( namespaceTable, valuePathRef ) );
      }
    }
  }

  public int getIndex() {
    return index;
  }

  public Context getContext() {
    return context;
  }

  public QName getColName() {
    return colName;
  }

  public String getModelRef() {
    return modelRef;
  }

  public String getValuePathRef() {
    return modelRef;
  }

  public QName getValueID() {
    return valueID;
  }

  public QName getIconValueID() {
    return iconValueID;
  }
  public QName getLabelValueID() {
    return labelValueID;
  }

  public QName getHelpValueID() {
    return helpValueID;
  }

  public QName getBackgroundValueID() {
    return backgroundValueID;
  }

  public boolean isReadOnly() {
    return readOnly;
  }

  public boolean isSortable() {
    return sortable;
  }

  public QName getHeaderCompID() {
    return headerCompID;
  }

  public QName getBodyCompID() {
    return bodyCompID;
  }

  public Controller getRenderCtrl() {
    return renderCtrl;
  }

  public Controller getEditCtrl() {
    return editCtrl;
  }

  public XPath getPath() {
    return path;
  }

  private Object getAttribute( Model row, QName attrName ) {
    if (attrName == null) {
      return null;
    }
    Model model = getValueModel( row );
    if (model == null) {
      return null;
    }
    else {
      return model.get( attrName );
    }
  }

  /**
   * Returns the Model containing valueID by evaluating path
   * @param row the row to start from
   * @return the value model, i.e. the model having attribute valueID
   */
  public Model getValueModel( Model row ) {
    Model model;
    if (path == null) {
      model = row;
    }
    else {
      model = path.getModel( row, context );
    }
    return model;
  }

  public Object getCellValue( Model row ) {
    if (valueID == null) {
      return row;
    }
    else {
      return getAttribute( row, valueID );
    }
  }

  public Object getIconValue( Model row ) {
    return getAttribute( row, iconValueID );
  }

  public String getAlign( ) {
    return align;
  }

  public String getHelpValue( Model row ) {
    Object value = getAttribute( row, helpValueID );
    if (value == null) {
      return null;
    }
    else {
      return value.toString();
    }
  }

  /**
   * Returns label Value, never null
   *
   * @param row
   * @return
   */
  public String getLabelValue( Model row ) {
    Object value = getAttribute( row, labelValueID );
    if (value instanceof String) {
      return (String) value;
    }
    return "";
  }

  public QName getBackgroundValue( Model row ) {
    Object value = getAttribute( row, backgroundValueID );
    if (value == null) {
      return null;
    }
    else {
      return (QName) value;
    }
  }

  @Override
  public String toString() {
    return colName.toString();
  }

  /**
   * Sets column Name, does not fire an event, so will only work before attachUI
   * @param newColName
   */
  public void setColName (QName newColName){
    this.colName = newColName;
    if (this.columnView != null) {
     columnView.onColumnChanged( this );
    }
  }

  /**
   * Sets the view which the column belongs to.
   * @param columnView
   */
  public void setView( ColumnView columnView ) {
    this.columnView = columnView;
  }
}
