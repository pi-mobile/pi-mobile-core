/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.qnames.QName;

/**
 * Created by pru on 06.05.16.
 */
public abstract class GuiDelegateOperation<DCD extends DialogControllerDelegate> extends GuiOperation {

  @Override
  public final void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {

    DCD delegate;

    if (this.controllerGroup == null) {
      delegate = (DCD) sourceCtrl.getDialogController().getDelegate();
      Logger.warn( "GuiDelegateOperation not initialized: " + getClass().getName() );
    }
    else {
      delegate = (DCD) this.controllerGroup.getDialogController().getDelegate();
    }

    Logger.info( "Executing GUI Op="+getClass().getName() );
    execute( eventID, delegate, sourceCtrl, dataContext );
  }

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   *   <LI>the actionDef's ID if source is a ButtonController</LI>
   *   <LI>the command if source is a short cut</LI>
   *   <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>

   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  protected abstract void execute( QName eventID, DCD delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException;
}
