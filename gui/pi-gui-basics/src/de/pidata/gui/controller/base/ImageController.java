/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.view.base.ImageViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.binding.Binding;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ValidationException;
import de.pidata.models.types.simple.Binary;
import de.pidata.qnames.QName;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

public class ImageController extends AbstractValueController {

  private ImageViewPI imageViewPI;
  private QName imageResourceID;
  private ComponentBitmap image;
  private List<ZoomListener> zoomListenerList;

  public void init( QName id, ControllerGroup ctrlGroup, Binding valueBinding, ImageViewPI imageViewPI ) {
    this.imageViewPI = imageViewPI;
    imageViewPI.setController( this );

    super.init( id, ctrlGroup, valueBinding, true );
  }

  /**
   * Return GuiOperation with given guiOpID or null if not found.
   *
   * @param guiOpID GuiOperation having eventID==guiOpID or null
   * @return
   */
  @Override
  public GuiOperation getGuiOperation( QName guiOpID ) {
    return null; // this value controller has no GuiOperations
  }

  public ViewPI getView() {
    return imageViewPI;
  }

  public boolean isReadOnly() {
    return true;
  }

  /**
   * Callend whenever a validation error occurs
   *
   * @param vex
   */
  protected void setValidationError( ValidationException vex ) {
    // we are read only, so we do not have to do something
  }

  public Object getValue() {
    // we are readOnly, so we do not need to ask our view
    if (image == null) {
      return imageResourceID;
    }
    else {
      return image;
    }
  }

  public void viewSetValue( Object value ) {
    if (value == null) {
      viewSetResourceID( null );
    }
    else {
      if (value instanceof Binary) {
        this.imageResourceID = null;
        if (value instanceof ComponentBitmap) {
          this.image = (ComponentBitmap) value;
        }
        else {
          InputStream imageStream = new ByteArrayInputStream( ((Binary) value).getBytes() );
          this.image = Platform.getInstance().loadBitmap( imageStream );
        }
        imageViewPI.updateValue( (short) -1, (short) -1, image );
      }
      else if (value instanceof QName) {
        viewSetResourceID( (QName) value );
      }
      else {
        this.imageResourceID = null;
        this.image = Platform.getInstance().getBitmap( GuiBuilder.NAMESPACE.getQName( value.toString() ) );
        imageViewPI.updateValue( (short) -1, (short) -1, image );
      }
    }
  }

  protected void viewSetResourceID( QName imageResourceID ) {
    this.imageResourceID = imageResourceID;
    this.image = null;
    imageViewPI.updateValue( (short) -1, (short) -1, imageResourceID );
  }

  public boolean isZoomEnabled () {
    return imageViewPI.isZoomEnabled();
  }

  public void setZoomFactor (double zoomFactor){
    imageViewPI.setZoomFactor(zoomFactor);
  }

  public double getZoomFactor() {
    return imageViewPI.getZoomFactor();
  }

  public void onZoomChanged( double newZoomFactor, Model dataContext ){
    fireZoomChanged( newZoomFactor, dataContext );
  }

  protected void fireZoomChanged ( double newValue, Model dataContext ){
    if (zoomListenerList != null) {
      // Copying listeners prevents problems if listeners are added or removed while send event loop
      Object[] listenerArr = new Object[zoomListenerList.size()];
      zoomListenerList.toArray( listenerArr );
      for (int i = 0; i < listenerArr.length; i++) {
        ZoomListener listener = (ZoomListener) listenerArr[i];
        try {
          listener.zoomChanged( this, newValue, dataContext );
        }
        catch (Exception ex) {
          Logger.error( "Error while event processing", ex );
        }
      }
    }
  }

  public void addZoomListener( ZoomListener zoomListener ) {
    if (this.zoomListenerList == null) {
      this.zoomListenerList = new LinkedList<ZoomListener>();
    }
    this.zoomListenerList.add( zoomListener );
  }

  public void removeZoomListener( ZoomListener zoomListener ) {
    if (this.zoomListenerList != null) {
      this.zoomListenerList.remove( zoomListener );
    }
  }
}
