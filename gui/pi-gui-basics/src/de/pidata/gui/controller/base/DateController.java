/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.guidef.InputMode;
import de.pidata.gui.renderer.DateRenderer;
import de.pidata.gui.view.base.DateViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.binding.Binding;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.qnames.QName;

/**
 * Currently supported formatting:
 * <ul>
 *   <li>date</li>
 *   <li>time</li>
 *   <li>dateTime</li>
 * </ul>
 */
public class DateController extends TextController {

  public void init( QName id, Controller parent, DateViewPI dateViewPI, String format, boolean readOnly ) {
    super.init( id, parent, dateViewPI, DateTimeType.getDefDate(), InputMode.date, format, readOnly );
  }

  public void init( QName id, Controller parent, DateViewPI dateViewPI, Binding valueBinding, String format, boolean readOnly ) {
    if (valueBinding == null) {
      this.init( id, parent, dateViewPI, readOnly);
    }
    else {
      this.init( id, parent, dateViewPI, valueBinding, InputMode.date, format, readOnly );
    }
  }

  public void viewSetValue( Object value ) {
    DateTimeType dateTimeType = (DateTimeType) getValueType();
    if (dateTimeType != null) {
      ((DateViewPI) textViewPI).setFormat( dateTimeType.getType() );
    }
    super.viewSetValue( value );
  }

  /**
     * Expects the date value in canonical string format.
     * @return
     */
  public Object getValue( ) {
    String text = viewGetText();
    //    try {
    if ((text == null) || (text.length() == 0)) {
      return null;
    }
    else {
      return new DateObject( getValueType().name(), text );
    }
    //    }
//    catch (Exception ex) {
//      throw new ValidationException() // TODO
//    }
  }

  public DateObject getDateValue() {
    return (DateObject) getValue();
  }

  /**
   * Creates a renderer matching this controller
   *
   *
   * @param value@return a renderer matching this controller
   */
  @Override
  public String render( Object value ) {
    return DateRenderer.get( ((DateTimeType) getValueType()).getType() ).render( value );
  }

  public void setMinDate (long minDate){
    ViewPI view = getView();
    if(view instanceof DateViewPI) {
      ((DateViewPI) view).setMinDate( minDate );
    }
  }
}
