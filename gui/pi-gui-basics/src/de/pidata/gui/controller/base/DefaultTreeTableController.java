/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.view.base.*;
import de.pidata.models.binding.Binding;
import de.pidata.models.binding.ModelBinding;
import de.pidata.models.tree.*;
import de.pidata.qnames.QName;

import java.util.ArrayList;
import java.util.List;

public class DefaultTreeTableController extends DefaultTreeController implements TreeTableController, EventListener {

  private List<ColumnInfo> columns = new ArrayList<ColumnInfo>();
  protected GuiOperation rowDoubleClickAction;
  protected GuiOperation rowMenuAction;
  private boolean ignoreUIEvents = false;

  public void init( QName id, ControllerGroup ctrlGroup, TreeTableViewPI treeTableViewPI, Binding rowBinding, ModelBinding treeBinding, GuiOperation nodeAction, boolean readOnly, boolean lazyLoading, boolean showRootNode ) {
    super.init( id, ctrlGroup, treeTableViewPI, rowBinding, treeBinding, nodeAction, readOnly, lazyLoading, showRootNode );
  }

  public void addColumn( ColumnInfo column ) {
    this.columns.add( column );
  }

  public int columnCount() {
    return columns.size();
  }

  public ColumnInfo getColumn( short index ) {
    if (index >= 0 && index < columns.size()) {
      return columns.get( index );
    }
    return null;
  }

  public ColumnInfo getColumnByName( QName colName ) {
    ColumnInfo colInfo = null;
    short col;
    for (col = (short) (columns.size() - 1); col >= 0; col--) {
      colInfo = columns.get( col );
      if (colInfo.getColName() == colName) {
        return colInfo;
      }
    }
    return null;
  }

  public ColumnInfo getColumnByHeaderID( QName headerID ) {
    for (short i = 0; i < columnCount(); i++) {
      ColumnInfo colInfo = getColumn( i );
      QName colHeadName = colInfo.getHeaderCompID();
      if (colHeadName == headerID) {
        return colInfo;
      }
    }
    return null;
  }

  public ColumnInfo getColumnByBodyCompID( QName bodyCompID ) {
    for (short i = 0; i < columnCount(); i++) {
      ColumnInfo colInfo = getColumn( i );
      QName compID = colInfo.getBodyCompID();
      if (compID == bodyCompID) {
        return colInfo;
      }
    }
    return null;
  }

  public void setRowMenuAction( GuiOperation rowMenuAction) {
    this.rowMenuAction = rowMenuAction;
  }

  public void setRowDoubleClickAction(GuiOperation rowDoubleClickAction) {
    this.rowDoubleClickAction = rowDoubleClickAction;
  }

  /**
   * Called by TreeTableViewPI.onSelectedCell()
   *
   * @param selectedNode the node to which the selected cell belongs
   * @param columnInfo  the column tn which the selected cell belongs
   */
  @Override
  public void onSelectedCell( TreeNodePI selectedNode, ColumnInfo columnInfo ) {
    if (!ignoreUIEvents) {
      onSelectionChanged( selectedNode );
      if (selectedNode != null) {
        Controller renderCtrl = null;
        if (columnInfo != null) {
          renderCtrl = columnInfo.getRenderCtrl();
        }
        if (renderCtrl instanceof ButtonController) {
          if (columnInfo != null) {
            ((ButtonController) renderCtrl).press( selectedNode.getNodeModel(), columnInfo );
          }
          else {
            ((ButtonController) renderCtrl).press( selectedNode.getNodeModel());
          }
        }
        else if (renderCtrl instanceof DefaultActionController) {
          QName colName = null;
          if (columnInfo != null) {
            colName = columnInfo.getColName();
          }
          GuiOperation cellOperation = renderCtrl.getGuiOperation( null );
          getDialogController().subAction( cellOperation, colName, this, selectedNode.getNodeModel() );
        }
        else if (nodeAction != null) {
          // Fokuswechsel muss passieren, da sonst zuletzt aktives Feld nicht ins Model schreibt
          QName colName = null;
          if (columnInfo != null) {
            colName = columnInfo.getColName();
          }
          getDialogController().subAction( this.nodeAction, colName, this, selectedNode.getNodeModel() );
        }
      }
    }
  }


  /**
   * Called by TableViewPI.onDoubleClickCell()
   *
   * @param selectedNode the node to which the selected cell belongs
   * @param columnInfo  the column tn which the selected cell belongs
   */
  @Override
  public void onDoubleClickCell( TreeNodePI selectedNode, ColumnInfo columnInfo ) {
    if (!ignoreUIEvents) {
      onSelectionChanged( selectedNode );
      if (rowDoubleClickAction != null) {
        // Fokuswechsel muss passieren, da sonst zuletzt aktives Feld nicht ins Model schreibt
        QName colName = null;
        if (columnInfo != null) {
          colName = columnInfo.getColName();
        }
        getDialogController().subAction( rowDoubleClickAction, colName, this, selectedNode.getNodeModel()  );
      }
    }
  }

  /**
   * Notifies this table controller of a right cllick on a cell by the user. The implementation should
   * not try to modify table's selection. This is done by UI itself.
   *
   * @param selectedNode the node to which the selected cell belongs
   * @param columnInfo the column tn which the selected cell belongs
   */
  @Override
  public void onRightClickCell( TreeNodePI selectedNode, ColumnInfo columnInfo ) {
    if (!ignoreUIEvents) {
      onSelectionChanged( selectedNode );
      if (rowMenuAction != null) {
        // Fokuswechsel muss passieren, da sonst zuletzt aktives Feld nicht ins Model schreibt
        QName colName = null;
        if (columnInfo != null) {
          colName = columnInfo.getColName();
        }
        getDialogController().subAction( rowMenuAction, colName, this, selectedNode.getNodeModel()  );
      }
    }
  }
}
