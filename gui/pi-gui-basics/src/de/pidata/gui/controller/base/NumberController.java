/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.event.InputManager;
import de.pidata.gui.guidef.InputMode;
import de.pidata.gui.renderer.NumberRenderer;
import de.pidata.gui.view.base.TextViewPI;
import de.pidata.log.Logger;
import de.pidata.models.binding.Binding;
import de.pidata.models.types.Type;
import de.pidata.models.types.simple.DecimalObject;
import de.pidata.models.types.simple.DecimalType;
import de.pidata.qnames.QName;
import de.pidata.string.Helper;

public class NumberController extends TextController {

  protected int totalDigits = -1;
  protected int fractionDigits = -1;
  protected int leadingDigits = -1;

  public void init( QName id, Controller parent, TextViewPI textViewPI, Binding binding, InputMode inputMode, Object format, boolean readOnly ) {
    super.init( id, parent, textViewPI, binding, inputMode, format, readOnly);
    initFormat( format );
  }

  private void initFormat( Object format ) {
    if (!Helper.isNullOrEmpty( format )) {
      String modeStr = format.toString();
      int pos = modeStr.indexOf( "." );
      if (pos > 0) {
        try {
          leadingDigits = Integer.parseInt( modeStr.substring( 0, pos ) );
          fractionDigits = Integer.parseInt( modeStr.substring( pos + 1 ) );
        }
        catch (Exception ex) {
          Logger.error( "Error parsing input mode for NumberController: " + modeStr, ex );
          throw new IllegalArgumentException( "Illegal input mode for NumberController: " + modeStr );
        }
      }
      else {
        throw new IllegalArgumentException( "Illegal input mode for NumberController: " + modeStr );
      }
    }
  }

  @Override
  public void init( QName id, Controller parent, TextViewPI textViewPI, boolean readOnly ) {
    super.init( id, parent, textViewPI, DecimalType.getDefault(), InputMode.numeric, null, readOnly );
  }

  @Override
  public void init( QName id, Controller parent, TextViewPI textViewPI, Binding binding, boolean readOnly ) {
    if (binding == null) {
      this.init( id, parent, textViewPI, readOnly );
    }
    else {
      this.init(id, parent, textViewPI, binding, InputMode.numeric, null, readOnly);
    }
  }

  public void init( QName id, Controller parent, TextViewPI textViewPI, Binding binding, Integer totalDigits, Integer fractionDigits, boolean readOnly ) {
    this.init( id, parent, textViewPI, binding, readOnly );
    if (totalDigits != null) {
      this.totalDigits = totalDigits.intValue();
    }
    if (fractionDigits != null) {
      this.fractionDigits = fractionDigits.intValue();
    }
  }

  protected Type getValueType() {
    DecimalType valueType = (DecimalType) super.getValueType();
    if (valueType == null) {
      valueType = DecimalType.getDefault();
    }
    return valueType;
  }

  public Object getValue() {
    DecimalObject decimal;
    String text = viewGetText();
    if ((text == null) || (text.length() == 0)) {
      decimal = null;
    }
    else {
      decimal = new DecimalObject( text, InputManager.DECIMAL_SEPARATOR, InputManager.THOUSAND_SEPARATOR );
    }
    Type valueType = getValueType();
    if ((valueType != null) && (decimal != null)) {
      DecimalType decimalType = (DecimalType) valueType;
      if (decimalType.hasFractionDigits()) {
        decimal = decimal.round( decimalType.getFractionDigits() );
      }
    }
    return decimal;
  }

  public Integer getIntegerValue() {
    String text = viewGetText();
    if ((text == null) || (text.length() == 0)) {
      return null;
    }
    else {
      try {
        return Integer.valueOf( text );
      }
      catch (NumberFormatException ex) {
        return null;
      }
    }
  }

  public Short getShortValue() {
    String text = viewGetText();
    if ((text == null) || (text.length() == 0)) {
      return null;
    }
    else {
      try {
        return Short.valueOf( text );
      }
      catch (NumberFormatException ex) {
        return null;
      }
    }
  }

  public void viewSetValue( Object value ) {
    if (value == null) {
      viewSetText( null );
    }
    else {
      DecimalObject decimal;
      if (value instanceof DecimalObject) {
        decimal = (DecimalObject) value;
      }
      else {
        decimal = new DecimalObject( value.toString() );
      }
      int fractionDigits = getFractionDigits( (DecimalType) getValueType() );
      viewSetText( decimal.toString( InputManager.DECIMAL_SEPARATOR, fractionDigits ) );
    }
  }

  private int getFractionDigits( DecimalType valueType ) {
    int fractionDigits;
    if (this.fractionDigits >= 0) {
      fractionDigits = this.fractionDigits;
    }
    else {
      fractionDigits = valueType.getFractionDigits();
    }
    return fractionDigits;
  }

  public int getFractionDigits() {
    return fractionDigits;
  }

  public void setFractionDigits( int fractionDigits ) {
    this.fractionDigits = fractionDigits;
  }

  /**
   * Creates a renderer matching this controller
   *
   *
   * @param value@return a renderer matching this controller
   */
  @Override
  public String render( Object value ) {
    return new NumberRenderer( InputManager.DECIMAL_SEPARATOR, getFractionDigits( (DecimalType) getValueType() ) ).render( value );
  }
}
