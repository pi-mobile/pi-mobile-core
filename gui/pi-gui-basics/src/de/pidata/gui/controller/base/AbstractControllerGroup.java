/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.event.InputManager;
import de.pidata.gui.guidef.InputMode;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIFragmentAdapter;
import de.pidata.gui.view.base.ModuleViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.binding.Binding;
import de.pidata.models.binding.ModelBinding;
import de.pidata.models.tree.ModelSource;
import de.pidata.models.tree.*;
import de.pidata.qnames.QName;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractControllerGroup implements MutableControllerGroup {

  private QName name;
  private ControllerGroup parentGroup;
  protected boolean active = false;
  protected UIContainer uiContainer;

  protected Scope scope = new Scope();
  protected List<Controller> controllerOrder = new ArrayList();
  protected ModelBinding groupBinding;
  protected Model model;

  protected EventSender eventSender = new EventSender( this );

  protected void init( QName name, ControllerGroup parentGroup, ModelBinding groupBinding ) {
    this.name = name;
    this.parentGroup = parentGroup;
    this.groupBinding = groupBinding;
  }

  @Override
  public QName getName() {
    return name;
  }

  /**
   * Returns this controller's view  or null if controller has no view
   *
   * @return this controller's view or null
   */
  @Override
  public ViewPI getView() {
    return null;
  }

  @Override
  public boolean isActive() {
    return active;
  }

  @Override
  public void activate( UIContainer uiContainer ) {
    this.uiContainer = uiContainer;
    ViewPI groupView = getView();
    if (groupView != null) {
      groupView.attachUIComponent( uiContainer );
      if (groupView instanceof ModuleViewPI) {
        uiContainer = (UIFragmentAdapter) groupView.getUIAdapter();
      }
    }
    for (int i = 0; i < controllerOrder.size(); i++) {
      Controller ctrl = controllerOrder.get( i );
      ctrl.activate( uiContainer );
    }
    if (groupBinding == null) {
      setModel( parentGroup.getModel() );
    }
    else {
      groupBinding.setBindingListener( this );
    }
    this.active = true;
  }

  /**
   * Called to deactivate this controller. It now should stop
   * listening on its binding and detach from its UI.
   *
   * @param saveValue if true controller should save its value
   *           if false controller should abort any value changes
   */
  @Override
  public void deactivate( boolean saveValue ) {
    if (saveValue) {
      saveValue();
    }
    this.active = false;
    if (this.groupBinding != null) {
      groupBinding.setBindingListener( null );
    }
    for (int i = 0; i < controllerOrder.size(); i++) {
      Controller ctrl = controllerOrder.get( i );
      ctrl.deactivate( saveValue );
    }
    ViewPI groupView = getView();
    if (groupView != null) {
      groupView.detachUIComponent();
    }
  }

  /**
   * Returns this group's parent group or null if this is a DialogController.
   * @return this group's parent group or null
   */
  @Override
  public ControllerGroup getParentGroup() {
    return parentGroup;
  }

  @Override
  public void setModel(Model model) {
    Model oldModel = this.model;
    if (model != oldModel) {
      this.model = model;
      if (this.active) {
        for (int i = 0; i < controllerOrder.size(); i++) {
          Controller ctrl = controllerOrder.get( i );
          ctrl.activate( uiContainer );
        }
      }
      eventSender.fireEvent( EventListener.ID_MODEL_DATA_CHANGED, this, null, oldModel, model );
    }
  }

  @Override
  public Iterable<Controller> controllerIterator() {
    return controllerOrder;
  }

  /**
   * Returns the current data model of this data source
   *
   * @return the current data model
   */
  @Override
  public Model getModel() {
    return this.model;
  }

  public ModelBinding getGroupBinding() {
    return groupBinding;
  }

  public void setGroupBinding( ModelBinding binding ) {
    if (binding != this.groupBinding) {
      if (this.groupBinding != null) {
        this.groupBinding.disconnect();
      }
      this.groupBinding = binding;
      if (binding != null) {
        binding.setBindingListener( this );
      }
    }
  }

  @Override
  public void valueChanged( Binding source, Object newValue ) {
    if (groupBinding == null) {
      setModel( null );
    }
    else {
      setModel( (Model) newValue );
    }
  }

  @Override
  public void modelChanged( Binding source, Model newModel ) {
    // do nothing - see valueChanged()
  }

  /**
   * Called by Binding after it's modelSource has been changed
   *
   * @param source
   * @param modelSource the new modelSource, never null
   */
  @Override
  public void modelSourceChanged( Binding source, ModelSource modelSource ) {
    // do nothing - only valid for ValueController
  }

  @Override
  public void addController( QName controllerID, Controller controller ) {
    if (scope.get( controllerID ) != null) {
      throw new IllegalArgumentException("Controller or Variable already exists, name="+controllerID);
    }
    if (controller != null) { //todo: dummy da group controller nicht erzeugt werden!
      scope.add( controller );
      controllerOrder.add( controller );
    }
    if (active) {
      controller.activate( uiContainer );
    }
  }

  public void removeController( Controller controller ) {
    controller.deactivate( false );
    scope.remove( controller );
    controllerOrder.remove( controller );
  }

  public void removeAllControllers() {
    while (controllerOrder.size() > 0) {
      Controller controller = controllerOrder.get( controllerOrder.size()-1 );
      removeController( controller );
    }
  }

  /**
   * Returns this group's child having id controllerID or null. If no direct child is found
   * all ControllerGroups are searched recursively for a matching controller.
   * @param controllerID the id of the controller to be returned
   * @return child controller or null
   */
  @Override
  public Controller getController( QName controllerID ) {
    //-- first look for direct children
    for (int i = 0; i < controllerOrder.size(); i++) {
      Controller ctrl = controllerOrder.get( i );
      if (ctrl.getName() == controllerID) {
        return ctrl;
      }
    }
    // not found, so recursively look into controller groups
    for (int i = 0; i < controllerOrder.size(); i++) {
      Controller ctrl = controllerOrder.get( i );
      if (ctrl instanceof ControllerGroup) {
        Controller childCtrl = ((ControllerGroup) ctrl).getController( controllerID );
        if (childCtrl != null) {
          return childCtrl;
        }
      }
    }
    return null;
  }

  /**
   * Returns number of direct child controllers
   *
   * @return number of direct child controllers
   */
  public int controllerCount() {
    return controllerOrder.size();
  }

  /**
   * Returns direct child controller at index
   *
   * @param index index of child controller to be returned
   * @return direct child controller at index
   */
  public Controller getController( int index ) {
    return controllerOrder.get( index );
  }

  /**
   * Return GuiOperation with given guiOpID or null if not found.
   *
   * @param guiOpID GuiOperation having eventID==guiOpID or null
   * @return
   */
  @Override
  public GuiOperation getGuiOperation( QName guiOpID ) {
    for (int i = 0; i < controllerOrder.size(); i++) {
      Controller ctrl = controllerOrder.get( i );
      GuiOperation guiOp = ctrl.getGuiOperation( guiOpID );
      if (guiOp != null) {
        return guiOp;
      }
    }
    return null;
  }

  @Override
  public DialogController getDialogController() {
    ControllerGroup parentGroup = getParentGroup();
    if (parentGroup != null) {
      return parentGroup.getDialogController();
    }
    return null;
  }

  @Override
  public Context getContext() {
    return getDialogController().getContext();
  }

  /**
   * Returns true, if this Controller may get the input focus.
   *
   * @return true, if this Controller may get the input focus.
   */
  @Override
  public boolean isFocusable() {
    return false;
  }

  protected boolean nextFocus( char keyChar, int index ) {
    Controller focusController = getDialogController().getFocusController();
    if (focusController != null) {
      if (this.controllerOrder.contains( focusController )) {
        Controller nextCtrl = getNextFocusable( focusController, true );
        if (nextCtrl != null) {
          return nextCtrl.processCommand( InputManager.CMD_GRAB_FOCUS, keyChar, index );
        }
      }
    }
    return false;
  }

  protected boolean prevFocus( char keyChar, int index ) {
    Controller focusController = getDialogController().getFocusController();
    if (focusController != null) {
      if (this.controllerOrder.contains( focusController )) {
        Controller nextCtrl = getNextFocusable( focusController, false );
        if (nextCtrl != null) {
          return nextCtrl.processCommand( InputManager.CMD_GRAB_FOCUS, keyChar, index );
        }
      }
    }
    return false;
  }

  /**
   * Returns the next focusable controller forward/backward
   * @param fromController the controller where to start search
   * @param forward true for forward direction, false for backward
   * @return the next/previous focusable controller or null if fromController is last/first reached
   */
  @Override
  public Controller getNextFocusable(Controller fromController, boolean forward) {
    int index;
    if (fromController == null) {
      if (forward) index = -1;
      else index = this.controllerOrder.size();
    }
    else {
      index = this.controllerOrder.indexOf(fromController);
    }
    if (forward) {
      while (index < controllerOrder.size()-1) {
        index++;
        Controller nextController = (Controller) this.controllerOrder.get(index);
        if (nextController instanceof ControllerGroup) {
          nextController = ((ControllerGroup) nextController).getNextFocusable( null, forward );
        }
        if (nextController != null && nextController.isFocusable()) {
          return nextController;
        }
      }
    }
    else {
      while (index > 0) {
        index--;
        Controller nextController = (Controller) this.controllerOrder.get(index);
        if (nextController instanceof ControllerGroup) {
          nextController = ((ControllerGroup) nextController).getNextFocusable( null, forward );
        }
        if (nextController != null && nextController.isFocusable()) {
          return nextController;
        }
      }
    }
    return null;
  }

  public Scope getScope() {
    return scope;
  }

  /**
   * Adds listener to this ModelSource's listener list
   *
   * @param listener the listener to be added
   */
  @Override
  public void addListener(EventListener listener) {
    this.eventSender.addListener( listener );
  }

  /**
   * Removes listener from this ModelSource's listener list
   *
   * @param listener the listener to be removed
   */
  @Override
  public void removeListener(EventListener listener) {
    this.eventSender.removeListener( listener );
  }

  /**
   * Called to tell this BindingListener to store its value in the binding.
   * Note: This happens e.g. before a dialog executes a operation to
   * make sure all dialog values are available to that operation.
   */
  public void saveValue() {
    for (int i = 0; i < controllerOrder.size(); i++) {
      Object ctrl = controllerOrder.get( i );
      if (ctrl instanceof ValueController) {
        ((ValueController) ctrl).saveValue();
      }
    }
  }

  @Override
  public Model fetchDataSource(QName dataSourceID) {
    Model param = null;
    if (dataSourceID != null) {
      ModelSource source = (ModelSource) getScope().get( dataSourceID );
      if (source != null) {
        param = source.getModel();
      }
    }
    else {
      param = this.model;
    }
    return param;
  }

  /**
   * Called by command() whenever a input command arrives.
   *
   * @param cmd       the command, one of the constants InputManager.CMD_*
   * @param inputChar optional input character for the command
   * @param index     optional index parameter for the command
   * @return true if the command has been processed, otherwise false
   */
  @Override
  public boolean processCommand( QName cmd, char inputChar, int index ) {
    Platform platform = Platform.getInstance();
    if ((cmd == InputManager.CMD_PREV_COMP)
        || (platform.useCursor4Focus() && ((cmd == InputManager.CMD_UP) || (cmd == InputManager.CMD_LEFT)))) {
      return prevFocus( inputChar, index );
    }
    else if ((cmd == InputManager.CMD_NEXT_COMP)
        || (platform.useCursor4Focus() && ((cmd == InputManager.CMD_DOWN) || (cmd == InputManager.CMD_RIGHT)))) {
      return nextFocus( inputChar, index );
    }
    return false;
  }

  /**
   * Returns the ControllerGroup this Controller belongs to. Returns this,
   * if Controller is a ControllerGroup itself
   *
   * @return the ControllerGroup this Controller belongs to
   */
  @Override
  public MutableControllerGroup getControllerGroup() {
    return this;
  }

  /**
   * Returns this controller's parent controller
   *
   * @return this controller's parent controller
   */
  @Override
  public Controller getParentController() {
    return getParentGroup();
  }

  /**
   * Changes this controller's parent controller,
   *
   * @param parentController the new parent
   */
  @Override
  public void setParentController( Controller parentController ) {
    this.parentGroup = (ControllerGroup) parentController;
  }

  @Override
  public void performOnClick() {
    // do nothing
  }

  @Override
  public void onFocusChanged( QName viewID, boolean hasFocus ) {
    //default: NOP
  }

  @Override
  public Object getValue() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setValue( Object value ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * This method is called when this controller's dialog is closing.
   * The controller has to release all its resources, e.g. remove
   * itself as listener.
   */
  @Override
  public void closing() {
    for (int i = 0; i < controllerOrder.size(); i++) {
      Controller ctrl = controllerOrder.get( i );
      ctrl.closing();
    }
  }

  /**
   * Creates a renderer matching this controller
   *
   *
   * @param value@return a renderer matching this controller
   */
  @Override
  public String render( Object value ) {
    return "ControllerGoup";
  }

  /**
   * Temporary method from transporting old GUI definition's inputMde and format
   *
   * @param inputMode
   * @param format
   */
  @Override
  public void setInputMode( InputMode inputMode, Object format ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void showContextMenu( double mouseDownX, double mouseDownY ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }
}
