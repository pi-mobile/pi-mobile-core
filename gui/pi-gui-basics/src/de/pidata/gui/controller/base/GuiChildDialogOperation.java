/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;

/**
 *
 */
public class GuiChildDialogOperation extends GuiOperation {

  private QName dialogID;
  private Model parameter;

  /**
   * initialize a gui operation
   *
   * @param eventID  the eventID this GuiOp is interested in or null if any
   * @param dialogID the dialog to open as child
   */
  public void init( QName eventID, QName dialogID, Model parameter, ControllerGroup controllerGroup ) {
    super.init( eventID, null, controllerGroup );
    this.dialogID = dialogID;
    this.parameter = parameter;
  }

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID   the id of the event, see method description
   * @param sourceCtrl    original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    try {
      ParameterList parameterList;
      if (parameter == null) {
        parameterList = AbstractParameterList.EMPTY;
      }
      else {
        parameterList = new ModelParameterList( parameter );
      }
      DialogController dlgController = sourceCtrl.getDialogController();
      dlgController.openChildDialog( dialogID, null, parameterList );
    }
    catch (Exception ex) {
      String msg = "Could open child dialogID=" + dialogID;
      Logger.error( msg, ex );
      throw new ServiceException( ServiceException.SERVICE_FAILED, msg, ex );
    }
  }
}
