/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.guidef.GuiService;
import de.pidata.qnames.QName;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

/**
 * Result parameter list for question box dialogs, e.g. message box, confirm dialog and input box
 */
public class QuestionBoxResult extends AbstractParameterList {

  public static final QName YES_OK = GuiService.NAMESPACE.getQName( "Yes" );
  public static final QName NO = GuiService.NAMESPACE.getQName( "No" );
  public static final QName CANCEL = GuiService.NAMESPACE.getQName( "Cancel" );

  public static final QName PARAM_SELECTED_BUTTON = GuiService.NAMESPACE.getQName( "SelectedButton" );
  public static final QName PARAM_INPUT_VALUE = GuiService.NAMESPACE.getQName( "InputValue" );
  public static final QName PARAM_TITLE = GuiService.NAMESPACE.getQName( "Title" );

  public QuestionBoxResult() {
    super( ParameterType.QNameType, PARAM_SELECTED_BUTTON, ParameterType.StringType, PARAM_INPUT_VALUE, ParameterType.StringType, PARAM_TITLE );
  }

  public QuestionBoxResult( QName selectedButton, String inputValue, String title ) {
    this();
    setQName( PARAM_SELECTED_BUTTON, selectedButton );
    setString( PARAM_INPUT_VALUE, inputValue );
    setString( PARAM_TITLE, title );
  }

  public QName getSelectedButton() {
    return getQName( PARAM_SELECTED_BUTTON );
  }

  public String getInputValue() {
    return getString( PARAM_INPUT_VALUE );
  }

  public int getInputValueInt( int defaultValue) {
    String strValue = getString( PARAM_INPUT_VALUE );
    if ((strValue == null) || (strValue.length() == 0)) {
      return defaultValue;
    }
    else {
      try {
        return Integer.parseInt( strValue );
      }
      catch (Exception ex) {
        return defaultValue;
      }
    }
  }

  public String getTitle() {
    return getString( PARAM_TITLE );
  }
}
