/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.guidef.InputMode;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.tree.Variable;
import de.pidata.qnames.QName;

public interface Controller extends Variable {

  /**
   * Returns this controller's view  or null if controller has no view
   *
   * @return this controller's view or null
   */
  ViewPI getView();

  /**
   * Returns the ControllerGroup this Controller belongs to. Returns this,
   * if Controller is a ControllerGroup itself
   * @return the ControllerGroup this Controller belongs to
   */
  MutableControllerGroup getControllerGroup(); //TODO Controller-Hierarchie überdenken

  /**
   * Returns this Controller's DialogController
   *
   * @return DialogController this Controller is part of
   */
  DialogController getDialogController();

  /**
   * Returns true, if this Controller may get the input focus.
   * @return true, if this Controller may get the input focus.
   */
  boolean isFocusable();  // TODO ist das Aufgabe vom Controller oder vom View ?

  /**
   * Called by Component whenever it gains or looses input focus.
   * @param viewID   id of the view sending this message
   * @param hasFocus true if view gained input focus @return if false focus switch is not allowed by ComponentListener
   */
  void onFocusChanged( QName viewID, boolean hasFocus );  // TODO ist das Aufgabe vom Controller oder vom View ?

  /**
     * This method is called when this controller's dialog is closing.
     * The controller has to release all its resources, e.g. remove
     * itself as listener.
     */
  void closing();

  /**
   * Returns this controller's parent controller
   * @return this controller's parent controller
   */
  Controller getParentController(); //TODO Controller-Hierarchie überdenken

  /**
   * Changes this controller's parent controller,
   *
   * @param parentController the new parent
   */
  void setParentController( Controller parentController );

  /**
   * Called by command() whenever a input command arrives.
   *
   * @param cmd       the command, one of the constants InputManager.CMD_*
   * @param inputChar optional input character for the command
   * @param index     optional index parameter for the command
   * @return true if the command has been processed, otherwise false
   */
  boolean processCommand( QName cmd, char inputChar, int index );

  /**
   * Called to activate this controller. Now the controller can
   * attach itself to the UI and then start listening on its binding.
   * In case of a child controller within a complex parent controller
   * (e.g. Table or Tree) a dataContext may be necessary to identify
   * the UI component.
   * 
   *  @param uiContainer the container where this controller's UI components are located
   */
  void activate( UIContainer uiContainer );

  /**
   * Called to deactivate this controller. It now should stop
   * listening on its binding and detach from its UI.
   * @param saveValue if true controller should save its value
   *           if false controller should abort any value changes
   */
  void deactivate( boolean saveValue );

  boolean isActive();

  /**
   * Renders display String from given value using this renderer's format rules
   * @param value  the value to be rendered
   * @return a String rendered from value
   */
  String render( Object value );

  /**
   * send onClick request from Controller -> ViewPI -> UIAdapter -> View
   */
  void performOnClick();

  /**
   * Return GuiOperation with given guiOpID or null if not found.
   * @param guiOpID GuiOperation having eventID==guiOpID or null
   * @return
   */
  GuiOperation getGuiOperation( QName guiOpID );

  /**
   * Set inputMode and format
   * 
   * @param inputMode
   * @param format
   */
  void setInputMode( InputMode inputMode, Object format );

  void showContextMenu( double mouseDownX, double mouseDownY );
}
