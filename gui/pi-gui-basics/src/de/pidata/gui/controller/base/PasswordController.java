/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.view.base.TextViewPI;
import de.pidata.log.Logger;
import de.pidata.models.binding.Binding;
import de.pidata.models.crypt.Encrypter;
import de.pidata.models.crypt.PlainEncrypter;
import de.pidata.models.crypt.DefaultSHA1Encrypter;
import de.pidata.qnames.QName;
import de.pidata.string.Helper;

public class PasswordController extends TextController {

  private Encrypter encrypter;
  private boolean hideInput = false;

  public static final String ENCRYPT_SHA1 = "SHA1";

  public void init( QName id, Controller parent, TextViewPI textViewPI, Binding binding, String encryptor, boolean readOnly ) {
    initEncrypter( encryptor );
    super.init( id, parent, textViewPI, binding, readOnly );
    textViewPI.setHideInput( hideInput );
  }

  private void initEncrypter( String encryptFkt ) {
    try {
      if (Helper.isNullOrEmpty( encryptFkt )) {
        encrypter = new PlainEncrypter();
      }
      else {
        String encryptFktName;
        if (encryptFkt.charAt( 0 ) == '*') {
          hideInput = true;
          encryptFktName = encryptFkt.substring( 1 );
        }
        else {
          encryptFktName = encryptFkt;
        }
        if (ENCRYPT_SHA1.equals( encryptFktName )) {
          encrypter = new DefaultSHA1Encrypter();
          // TODO? encrypter = (Encrypter) Class.forName("de.pidata.models.crypt.DefaultSHA1Encrypter").newInstance();
        }
        else {
          encrypter = (Encrypter) Class.forName( encryptFktName ).newInstance();
        }
      }
    }
    catch (Exception ex) {
      String msg = "Could not create encrypter";
      Logger.error( msg, ex );
      throw new IllegalArgumentException( msg );
    }
  }

  public void viewSetValue( Object value ) {
    viewSetText( "" );
  }

  public Object getValue() {
    String value = viewGetText();
    return encrypter.encrypt( value );
  }
}
