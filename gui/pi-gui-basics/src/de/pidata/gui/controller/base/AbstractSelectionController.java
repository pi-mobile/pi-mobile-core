/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.renderer.SelectionRenderer;
import de.pidata.gui.renderer.StringRenderer;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.models.binding.Binding;
import de.pidata.models.binding.Selection;
import de.pidata.models.tree.*;
import de.pidata.qnames.Key;
import de.pidata.qnames.QName;

public abstract class AbstractSelectionController extends AbstractValueController implements SelectionController, EventListener {

  protected Selection selection;
  protected XPath displayValuePath = null;
  protected QName keyAttr;
  protected EventSender eventSender = new EventSender( this );
  protected Controller detailCtrl;
  protected GuiOperation rowAction;
  protected GuiOperation rowDoubleClickAction;
  protected GuiOperation rowMenuAction;

  /** For lazy loading: Ignore events from model while running viewUpdateAllRows() */
  protected boolean ignoreModelEvents = false;

  protected void init( QName id, Controller parent, Binding valueBinding, Selection selection, GuiOperation rowAction, boolean readOnly ) {
    this.selection = selection;   // we will use our initial selection if we call super.init before setting our selection
    this.rowAction = rowAction;
    super.init( id, parent, valueBinding, readOnly );
    selection.addListener( this );
  }

  /**
   * Called to activate this controller. Now the controller can
   * attach itself to the UI and then start listening on its binding.
   * @param uiContainer the dialog context for this controller
   *
   */
  public void activate( UIContainer uiContainer ) {
    if (selection != null) {
      selection.refresh();
      Model selectedModel = null;
      if (valueBinding != null) {
        Object value = valueBinding.fetchModelValue();
        selectedModel = getRowForValue( value );
        selection.select( selectedModel );
      }
    }
    super.activate( uiContainer );
    viewUpdateAllRows(); // initially fetch values from selection
    viewSetSelection();
    eventSender.fireEvent( EventListener.ID_MODEL_DATA_CHANGED, this, null, null, getSelectedRow() );
  }

  /**
   * Return GuiOperation with given guiOpID or null if not found.
   *
   * @param guiOpID GuiOperation having eventID==guiOpID or null
   * @return
   */
  @Override
  public GuiOperation getGuiOperation( QName guiOpID ) {
    if ((rowAction != null) && (rowAction.eventID == guiOpID)) {
      return rowAction;
    }
    else if ((rowDoubleClickAction != null) && (rowDoubleClickAction.eventID == guiOpID)) {
      return rowDoubleClickAction;
    }
    else if ((rowMenuAction != null) && (rowMenuAction.eventID == guiOpID)) {
      return rowMenuAction;
    }
    else {
      return null;
    }
  }

  /**
   * Returns the selection this ModelSource uses
   *
   * @return the selection this ModelSource uses
   */
  public Selection getSelection() {
    return this.selection;
  }

  /**
   * Returns the total row count of this data source
   *
   * @return the total row count of this data source
   */
  public int rowCount() {
    int count;
    if (selection == null) {
      count = 0;
    }
    else {
      count = selection.valueCount();
      if (hasFirstRowForEmptySelection()) count++;
    }
    return count;
  }

  /**
   * Returns the current selected row model
   *
   * @return the current selected row model
   * @param index
   */
  public Model getSelectedRow( int index ) {
    if (selection == null) return null;
    else return selection.getSelectedValue( index );
  }

  public void selectRow( Model row ) {
    Model oldRow;
    oldRow = getSelectedRow();
    selection.select( row );
    saveValue();
    viewSetSelection();
    eventSender.fireEvent( EventListener.ID_MODEL_DATA_CHANGED, this, null, oldRow, row );
  }

  protected Object keyValue( Model row ) {
    if (keyAttr == null) {
      Key key = row.key();
      if (key == null) return null;
      else return key.getKeyValue( 0 );
    }
    else return row.get( keyAttr );
  }

  public Object getValue() {
    if (selection == null) {
      return null;
    }
    else {
      Model selModel = selection.getSelectedValue(0);
      return getValueForRow( selModel );
    }
  }

  private Object getValueForRow( Model selModel ) {
      if ((selModel != null) && (displayValuePath != null)) {
        selModel = displayValuePath.getModel( selModel, null );
      }
      if (selModel == null) {
        return null;
      }
      return keyValue( selModel );
    }

  protected Model getRowForValue( Object value ) {
    Model selModel;
    Key key;
    if (keyAttr == null) {
      if (value instanceof Model) {
        key = ((Model) value).key();
      }
      else if (value instanceof Key) {
        key = (Key) value;
      }
      else {
        key = new SimpleKey( value );
      }
      selModel = selection.getValue( key, this.displayValuePath );
    }
    else {
      selModel = selection.findValue( keyAttr, value );
      if (selModel == null) {
        key = null;
      }
      else {
        key = selModel.key();
      }
    }
    return selModel;
  }

  public void viewSetValue( Object value ) {
    if (selection != null) {
      //ignoreEvents = true;
      try {
        Model oldRow = getSelectedRow();
        if (value == null) {
          selection.select( null );
          //toggleSelComp( (short) 0, true );
        }
        else {
          Model rowModel = getRowForValue( value );
          if (oldRow != rowModel) {
            selection.select( rowModel );
            // TODO: Following line duplicates event. Make sure (discuss with team) if there are any usecase relying on this event.
            // eventSender.fireEvent( EventListener.ID_MODEL_DATA_CHANGED, this, null, oldRow, selModel );
            //short index = indexOf( key );
          }
        }
      }
      finally {
        //ignoreEvents = false;
      }
    }
  }

  /**
   * Called by ListViewPI whenever a row has been selected or deselected
   *
   * @param selectedRow the row which was selected
   * @param selected    true if row has been selected, false if it has been deselected
   */
  public void onSelectionChanged( Model selectedRow, boolean selected ) {
    Controller ctrl = getDialogController().getFocusController();
    if (ctrl instanceof ValueController) {
      // Make shure we do not loose value of current active input field
      ((ValueController) ctrl).saveValue();
    }
    selection.removeListener( this ); // avoid event recursion
    try {
      boolean isSelected = selection.isSelected( selectedRow );
      if (isSelected != selected) {
        Model oldRow = getSelectedRow();
        if (selected) {
          selection.select( selectedRow );
        }
        else {
          selection.deSelect( selectedRow );
        }
        saveValue();
        Model newRow = getSelectedRow();
        if (oldRow != newRow) {
          eventSender.fireEvent( EventListener.ID_MODEL_DATA_CHANGED, this, null, oldRow, newRow );
        }
      }
    }
    catch (Exception ex) {
      // ignore error - row may have disappeared meanwhile
    }
    selection.addListener( this );
  }

  protected Model getSelectedRow() {
    Model oldRow;
    if (selection.isMultiSelect()) {
      oldRow = null;
    }
    else {
      oldRow = selection.getSelectedValue( 0 );
    }
    return oldRow;
  }

  public void onPrevSelected() {
    selectRow( selection.getSelectedPrev() );
  }

  public void onNextSelected() {
    selectRow( selection.getSelectedNext() );
  }

  /**
   * This method is called when this controller's dialog is closing.
   * The controller has to release all its resources, e.g. remove
   * itself as listener.
   */
  public void closing() {
    this.selection.removeListener(this);
    super.closing();
  }

  /**
   * Returns current index of key or -1 if key is not visible
   *
   * @param key
   * @return
   */
  public short indexOf( Key key ) {
    Model model = selection.getValue( key, displayValuePath );
    if (model == null) {
      if (selection.isEmptyAllowed()) {
        if (hasFirstRowForEmptySelection()) {
          return 0;
        }
        else {
          return -1;
        }
      }
      else {
        throw new IllegalArgumentException( "Empty selection is not allowed, key="+key );
      }
    }
    else {
      short index = (short) selection.indexOf( model );
      if (hasFirstRowForEmptySelection()) {
        index++;
      }
      return index;
    }
  }

  /**
   * Returns the current data model of this data source
   *
   * @return the current data model
   */
  public Model getModel() {
    if (selection == null) {
      return null;
    }
    else {
      return selection.getSelectedValue(0);
    }
  }

  /**
   * Adds listener to this ModelSource's listener list
   *
   * @param listener the listener to be added
   */
  public void addListener(EventListener listener) {
    this.eventSender.addListener( listener );
  }

  /**
   * Removes listener from this ModelSource's listener list
   *
   * @param listener the listener to be removed
   */
  public void removeListener(EventListener listener) {
    this.eventSender.removeListener( listener );
  }

  /**
   * Tells this controller to update all data rows in it's view.
   */
  protected abstract void viewUpdateAllRows();

  protected abstract void viewUpdateRow( Model changedRow );

  protected abstract void viewRemoveRow( Model removedRow );

  protected abstract void viewInsertRow( Model newRow, Model beforeRow );

  /**
   * An event occurred.
   * @param eventSender the sender of this event
   * @param eventID the id of the event
   * @param source the instance where the event occurred on, may be a child of eventSender
   * @param modelID the id of the affected model
   * @param oldValue the old value of the model
   * @param newValue the new value of the model
   */
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {
    if (source == selection) {
      // Selection tells us if the selection itself has changed, but not if the rows have changed
      if (isActive()) {
        viewSetSelection();
        this.eventSender.fireEvent( EventListener.ID_MODEL_DATA_CHANGED, this, null, oldValue, newValue );
      }
    }
    else if (source == selection.getListBinding()) {
      // While running viewUpdateAllRows() we must not process model events to avoid line duplication
      if (!ignoreModelEvents) {
        if (eventID == EventListener.ID_MODEL_DATA_ADDED) {
          // newValue is the inserted row and all following rows moved one down
          Model newModel = (Model) newValue;
          if (newModel == null) {
            viewInsertRow( newModel, null );
          }
          else {
            viewInsertRow( newModel, newModel.nextSibling( selection.getTableRelationID() ) );
          }
        }
        else if (eventID == EventListener.ID_MODEL_DATA_REMOVED) {
          // oldValue is the removed row (null means all rows), newValue is the next row after the deleted one
          Model removedRow = (Model) oldValue;
          if (removedRow == null) {
            viewUpdateAllRows();
          }
          else {
            viewRemoveRow( removedRow );
          }
        }
        else {
          // selection's binding has changed, so we have to replace all table rows
          viewUpdateAllRows();
        }
      }
    }
    else if (source instanceof Model) {
      // source is one of binding's child rows
      viewUpdateRow( (Model) source );
    }
  }

  protected abstract void viewSetSelection();

  /**
   * Creates a renderer matching this controller
   *
   *
   * @param value@return a renderer matching this controller
   */
  @Override
  public String render( Object value ) {
    return new SelectionRenderer( getSelection(), displayValuePath, getListView().getDisplayValueID(), StringRenderer.get( renderMode, renderFormat ) ).render( value );
  }

  public void initDetailController( Controller detailCtrl ) {
    this.detailCtrl = detailCtrl;
  }

  public Controller getDetailController() {
    return detailCtrl;
  }

  public void setRowMenuAction(GuiOperation rowMenuAction) {
    this.rowMenuAction = rowMenuAction;
  }

  public void setRowDoubleClickAction(GuiOperation rowDoubleClickAction) {
    this.rowDoubleClickAction = rowDoubleClickAction;
  }
}
