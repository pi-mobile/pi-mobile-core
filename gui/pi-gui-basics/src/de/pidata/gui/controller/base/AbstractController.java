/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.guidef.InputMode;
import de.pidata.gui.renderer.StringRenderer;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.qnames.QName;

public abstract class AbstractController implements Controller {

  private QName id;
  private Controller parent;
  protected boolean active = false;
  protected InputMode renderMode;
  protected Object renderFormat;

  protected AbstractController() {

  }

  public void init( QName id, Controller parent ) {
    this.id = id;
    this.parent = parent;
  }

  public boolean isActive() {
    return active;
  }

  /**
   * Called to activate this controller. Now the controller can
   * attach itself to the UI and then start listening on its binding.
   * @param uiContainer the dialog context for this controller
   *
   */
  public void activate( UIContainer uiContainer ) {
    try {
      ViewPI view = getView();
      if (view != null) {
        view.attachUIComponent( uiContainer );
        this.active = true;
      }
    }
    catch (Exception ex) {
      Logger.error( "Exception while activating controller ID="+getName(), ex );
    }
  }

  /**
   * Called to deactivate this controller. It now should stop
   * listening on its binding and detach from its UI.
   *
   * @param saveValue if true controller should save its value
   *           if false controller should abort any value changes
   */
  public void deactivate( boolean saveValue ) {
    this.active = false;
    ViewPI view = getView();
    if (view != null) {
      view.detachUIComponent();
    }
  }

  public QName getName() { // TODO: return id?
    return id;
  }

  public Controller getParentController() {
    return parent;
  }

  /**
   * Changes this controller's parent controller,
   *
   * @param parentController the new parent
   */
  @Override
  public void setParentController( Controller parentController ) {
    this.parent = parentController;
  }

  public MutableControllerGroup getControllerGroup() {
    return getParentController().getControllerGroup();
  }

  /**
   * Returns this Controller's DialogController
   *
   * @return DialogController this Controller is part of
   */
  @Override
  public DialogController getDialogController() {
    return getControllerGroup().getDialogController();
  }

  public void onFocusChanged( QName viewID, boolean hasFocus ) {  // TODO ist das Aufgabe vom Controller oder vom View ?
    if (hasFocus) {
      getDialogController().setFocusController( this );
    }
  }

  /**
   * If no specific implementation, give cmd to parent!
   * @param cmd       the command, one of the constants InputManager.CMD_*
   * @param inputChar optional input character for the command
   * @param index     optional index parameter for the command
   * @return
   */
  public final boolean processCommand( QName cmd, char inputChar, int index ) {
    ViewPI view = getView();
    if (view != null) {
      return view.processCommand( cmd,  inputChar, index );
    }
    return false;
  }

  public final void performOnClick() {
    ViewPI view = getView();
    if (view != null) {
      view.performOnClick();
    }
  }

  @Override
  public void showContextMenu( double mouseDownX, double mouseDownY ) {
    // Override controller for showing context menus.
  }

  /**
   * This method is called when this controller's dialog is closing.
   * The controller has to release all its resources, e.g. remove
   * itself as listener.
   */
  public void closing() {
    ViewPI view = getView();
    if (view != null) {
      view.closing();
    }
  }

  /**
   * Creates a renderer matching this controller
   *
   *
   * @param value@return a renderer matching this controller
   */
  @Override
  public String render( Object value ) {
    return StringRenderer.get( renderMode, renderFormat ).render( value );
  }

  /**
   * Temporary method from transporting old GUI definition's inputMde and format fro rendering
   *
   * @param renderMode
   * @param renderFormat
   */
  @Override
  public final void setInputMode( InputMode renderMode, Object renderFormat ) {
    this.renderMode = renderMode;
    this.renderFormat = renderFormat;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + " " + getName().getName();
  }
}
