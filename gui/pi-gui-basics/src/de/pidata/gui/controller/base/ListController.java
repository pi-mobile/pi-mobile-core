/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.view.base.ListViewPI;
import de.pidata.models.binding.Binding;
import de.pidata.models.binding.Selection;
import de.pidata.models.binding.SingleSelection;
import de.pidata.models.tree.XPath;
import de.pidata.qnames.QName;

public interface ListController extends SelectionController {

  void init( QName id, Controller parent, ListViewPI listView, Binding valueBinding,
             Selection selection, QName keyAttr, XPath displayValuePath, QName displayValueID, boolean readOnly );

}
