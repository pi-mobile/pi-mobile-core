/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.guidef.InputMode;
import de.pidata.gui.view.base.TextViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.binding.AttributeBinding;
import de.pidata.models.binding.Binding;
import de.pidata.models.tree.ValidationException;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.*;

public class TextController extends AbstractValueController {

  public static final Namespace NAMESPACE = Namespace.getInstance("de.pidata.gui");

  protected TextViewPI textViewPI;

  private Type valueType;
  private Namespace valueNS;

  private List<TextFieldListener> textFieldListenerList;
  private Map<QName, KeyPressedHandler> keyPressedHandlerMap = new HashMap<QName, KeyPressedHandler>();

  public void init( QName id, Controller parent, TextViewPI textViewPI, boolean readOnly ) {
    this.init(id, parent, textViewPI, StringType.getDefString(), InputMode.string, null, readOnly);
  }

  public void init( QName id, Controller parent, TextViewPI textViewPI, Binding binding, boolean readOnly ) {
    if (binding == null) {
      this.init( id, parent, textViewPI, readOnly );
    }
    else {
      this.init(id, parent, textViewPI, binding, InputMode.string, null, readOnly);
    }
  }

  public void init( QName id, Controller parent, TextViewPI textViewPI, Binding binding, InputMode inputMode, Object format, boolean readOnly ) {
    this.textViewPI = textViewPI;
    textViewPI.setController( this );
    textViewPI.setInputMode( inputMode );
    textViewPI.setFormat( format );

    if (binding != null) {
      this.valueType = binding.getValueType();
    }

    super.init( id, parent, binding, readOnly );
  }

  public void init( QName id, Controller parent, TextViewPI textViewPI, SimpleType valueType, InputMode inputMode, Object format, boolean readOnly ) {
    //TODO: Typklasse in constructor, hier prüfen ob kompatibel
    this.valueType = valueType;

    this.init( id, parent, textViewPI, (Binding) null, inputMode, format, readOnly );
  }

  public void init( QName id, Controller parent, TextViewPI textViewPI, InputMode inputMode, Object format, boolean readOnly ) {
    this.init( id, parent, textViewPI, (SimpleType) null, inputMode, format, readOnly );
  }

  /**
   * Return GuiOperation with given guiOpID or null if not found.
   *
   * @param guiOpID GuiOperation having eventID==guiOpID or null
   * @return
   */
  @Override
  public GuiOperation getGuiOperation( QName guiOpID ) {
    return null; // this value controller has no GuiOperations
  }

  protected Type getValueType() {
    if (valueType == null) {
      if (valueBinding == null) {
        return StringType.getDefString();
      }
      else {
        return valueBinding.getValueType();
      }
    }
    else {
      return valueType;
    }
  }

  public void setNamespace( Namespace valueNS ) {
    this.valueNS = valueNS;
  }

  protected Namespace getNamespace() {
    if (valueNS == null) {
      if (valueBinding != null && valueBinding instanceof AttributeBinding) {
        valueNS = ((AttributeBinding) valueBinding).getAttributeName().getNamespace();
      }
      else {
        throw new RuntimeException( "no namespace given" );
      }
    }
    return valueNS;
  }

  public ViewPI getView() {
    return textViewPI;
  }

  public TextViewPI getTextView() {
    return textViewPI;
  }

  public Object getValue() {
    String text = viewGetText();
    Type valueType = getValueType();
    if ((valueType != null) && (valueType instanceof QNameType)) {
      return getNamespace().getQName( text );
    }
    else {
      return text;
    }
  }

  public String getStringValue() {
    return (String) getValue();
  }

  protected String viewGetText() {
    if (textViewPI != null) return textViewPI.getValue();
    else return "";
  }

  public void viewSetValue( Object value ) {
    if (value == null) {
      viewSetText( "" );
    }
    else {
      if (value instanceof QName) {
        viewSetText( ((QName) value).getName() );
      }
      else {
        viewSetText( value.toString() );
      }
    }
  }

  protected void viewSetText( String text ) {
    if (textViewPI != null) {
      textViewPI.updateValue( render( text ) );
    }
  }

  /**
   * Callend whenever a validation error occurs
   *
   * @param vex
   */
  protected void setValidationError( ValidationException vex ) {
    if (vex == null) {
//TODO      this.textComp.setColor( Color.BLACK );
    }
    else {
      Logger.warn( "Validation exception in " + getClass().getName() + ", vex=" + vex.getMessage() );
//TODO      String errorMsg = vex.getMessage();
//      this.textComp.setColor( Color.RED );
//      // TODO errorMsg anzeigen
    }
  }

  public void addListener( TextFieldListener listener)  {
    if (this.textFieldListenerList == null) {
      this.textFieldListenerList = new LinkedList<TextFieldListener>();
    }
    this.textFieldListenerList.add( listener );
    textViewPI.setListenTextChanges( true );
  }

  public void removeListener(TextFieldListener listener) {
    if (this.textFieldListenerList != null) {
      this.textFieldListenerList.remove( listener );
      if (textFieldListenerList.size() == 0) {
        textViewPI.setListenTextChanges( false );
      }
    }
  }

  protected void fireTextChanged( String oldValue, String newValue ) {
    if (textFieldListenerList != null) {
      // Copying listeners prevents problems if listeners are added or removed while send event loop
      Object[] listenerArr = new Object[textFieldListenerList.size()];
      textFieldListenerList.toArray( listenerArr );
      for (int i = 0; i < listenerArr.length; i++) {
        TextFieldListener listener = (TextFieldListener) listenerArr[i];
        try {
          listener.textChanged( this, oldValue, newValue );
        }
        catch (Exception ex) {
          Logger.error( "Error while event processing", ex );
        }
      }
    }
  }

  public void textChanged( String oldValue, String newValue ) {
    fireTextChanged( oldValue, newValue );
  }

  public void addKeyPressedHandler( QName keyCode, KeyPressedHandler keyPressedHandler ){
    keyPressedHandlerMap.put( keyCode, keyPressedHandler );
  }

  public void removeKeyPressedHandler( QName keyCode ) {
    if (keyPressedHandlerMap.containsKey( keyCode )) {
      keyPressedHandlerMap.remove( keyCode );
    }
  }

  public void keyPressed( QName keyCode ) {
    fireKeyPressed( keyCode );
  }

  protected void fireKeyPressed( QName keyCode ) {
    if (keyPressedHandlerMap.containsKey( keyCode )) {
      KeyPressedHandler keyPressedHandler = keyPressedHandlerMap.get( keyCode );
      keyPressedHandler.keyPressed( this );
    }
  }
}
