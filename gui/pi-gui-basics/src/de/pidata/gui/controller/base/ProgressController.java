/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.component.base.ProgressTask;
import de.pidata.progress.ProgressListener;

/**
 * <add description here>
 *
 * @author Doris Schwarzmaier
 */
public interface ProgressController extends ProgressListener {

  void show();

  void startAndObserveTask( ProgressTask progressTask );

  void setProgress( double progress, String message );

  void setProgress( double progress );

  void abortTask();

  void finish();

  void setCancelAction(Runnable cancelAction);
}
