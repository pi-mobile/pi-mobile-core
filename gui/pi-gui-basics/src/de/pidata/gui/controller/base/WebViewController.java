/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.renderer.Renderer;

import java.util.LinkedList;
import java.util.List;

public class WebViewController extends TextController implements Renderer {

  private List<LocationChangedListener> locationChangedListeners;

  /**
   * Tells this renderer to render value by using controller
   *
   * @param value the value to be rendered
   * @return the rendered value
   */
  @Override
  public String render( Object value ) {
    // will be rendered by WebView component
    if (value == null) {
      return null;
    }
    else {
      return value.toString();
    }
  }

  public void addListener( LocationChangedListener listener)  {
    if (this.locationChangedListeners == null) {
      this.locationChangedListeners = new LinkedList<LocationChangedListener>();
    }
    this.locationChangedListeners.add( listener );
  }

  public void removeListener(LocationChangedListener listener) {
    if (this.locationChangedListeners != null) {
      this.locationChangedListeners.remove( listener );
    }
  }

  public void locationchanged(String source, String target) {
    if (locationChangedListeners != null) {
      for (LocationChangedListener listener : locationChangedListeners) {
        listener.locationChanged( this, source, target );
      }
    }
  }
}
