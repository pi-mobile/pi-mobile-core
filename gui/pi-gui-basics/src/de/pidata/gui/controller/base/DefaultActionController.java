/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.view.base.ViewPI;
import de.pidata.qnames.QName;

/**
 * Implements a Controller with an Operation without UI.
 */
public class DefaultActionController extends AbstractController implements ActionController {

  private GuiOperation operation;

  public void init( QName id, ControllerGroup parent, GuiOperation operation ) {
    super.init( id, parent );

    this.operation = operation;
  }

  @Override
  public void doAction() {
    // deeper implementation ensures that all data will be saved
    MutableControllerGroup ctrlGroup = getControllerGroup();
    ctrlGroup.getDialogController().subAction( this.operation, this.getName(), this, ctrlGroup.getModel() );
  }

  /**
   * Return GuiOperation with given guiOpID or null if not found.
   *
   * @param guiOpID GuiOperation having eventID==guiOpID or null
   * @return
   */
  @Override
  public GuiOperation getGuiOperation( QName guiOpID ) {
    if (guiOpID == null) {
      return operation;
    }
    else if ((operation != null ) && (operation.eventID == guiOpID)) {
      return operation;
    }
    else {
      return null;
    }

  }

  /**
   * Returns this controller's view  or null if controller has no view
   *
   * @return this controller's view or null
   */
  @Override
  public ViewPI getView() {
    return null;
  }

  /**
   * Returns true, if this Controller may get the input focus.
   *
   * @return true, if this Controller may get the input focus.
   */
  @Override
  public boolean isFocusable() {
    return false;
  }

  @Override
  public Object getValue() {
    return null;
  }

  @Override
  public void setValue( Object value ) {
    // NOP
  }
}
