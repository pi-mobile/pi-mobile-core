/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.view.base.PaintViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.gui.view.figure.SimpleFigure;
import de.pidata.models.binding.Binding;
import de.pidata.models.tree.ValidationException;
import de.pidata.qnames.QName;

public class PaintController extends AbstractValueController {

  public static final String IMAGE_DIR = "images";

  private PaintViewPI paintViewPI;

  public void init( QName id, ControllerGroup ctrlGroup, Binding binding, PaintViewPI paintViewPI ) {
    this.paintViewPI = paintViewPI;
    paintViewPI.setController( this );

    super.init( id, ctrlGroup, binding, true );
  }

  /**
   * Return GuiOperation with given guiOpID or null if not found.
   *
   * @param guiOpID GuiOperation having eventID==guiOpID or null
   * @return
   */
  @Override
  public GuiOperation getGuiOperation( QName guiOpID ) {
    return null; // this value controller has no GuiOperations
  }

  public ViewPI getView() {
    return paintViewPI;
  }

  public Object getValue() {
    return paintViewPI.getValue();
  }

  public void viewSetValue( Object value ) {
    paintViewPI.updateValue( value );
  }

  /**
   * Callend whenever a validation error occurs
   *
   * @param vex
   */
  protected void setValidationError( ValidationException vex ) {
    // we are read only, so we do not have to do something
  }

  public void addFigure( SimpleFigure simpleFigure ) {
    paintViewPI.addFigure( simpleFigure );
  }
}
