/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.component.base.PaintManager;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.component.base.Popup;
import de.pidata.gui.controller.file.FileChooserParameter;
import de.pidata.gui.event.Dialog;
import de.pidata.gui.event.InputManager;
import de.pidata.gui.guidef.*;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ModuleViewPI;
import de.pidata.gui.view.base.ViewAnimation;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.binding.Binding;
import de.pidata.models.tree.ModelSource;
import de.pidata.models.tree.EventListener;
import de.pidata.models.types.Type;
import de.pidata.qnames.NamespaceTable;
import de.pidata.service.base.ServiceException;
import de.pidata.models.tree.*;
import de.pidata.qnames.QName;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;
import de.pidata.string.Helper;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;

import java.util.*;

public class AbstractDialogController implements DialogController {

  private static final boolean DEBUG = false;

  /** Constants for dialog state */
  public static final int STATE_INITIAL = 0;
  public static final int STATE_ACTIVE = 1;
  public static final int STATE_CANCELED = 2;
  public static final int STATE_OK = 3;
  protected QName name;
  protected Dialog dialogComp;
  protected Scope scope = new Scope();
  private List<Controller> controllerOrder = new ArrayList<Controller>();
  private Map<QName,GuiOperation> shortCuts = new HashMap<QName,GuiOperation>();
  protected String  title;
  protected DialogController parentDialogController;
  protected GuiOperation guiOperation;
  protected QName eventID;
  private   PopupController popupController;
  protected int state = STATE_INITIAL;
  protected ControllerBuilder controllerBuilder;
  protected Controller focusController;
  protected Context context;
  protected Model model;
  protected Type modelType;
  protected ParameterList returnList = null;
  private DialogController childDialogController;
  protected boolean fullScreen;
  protected Model originalModel = null;
  protected ParameterList parameterList;
  protected DialogControllerDelegate delegate;

  private DialogValidator dialogValidator;
  private EventSender eventSender = new EventSender( this );
  private boolean clonedModel = false;
  private String modelPath;
  private NamespaceTable namespaceTable;

  private GuiOperation lastGuiOperation = null;
  private GuiOperation closeOperation = null;

  public AbstractDialogController( Context context, Dialog dialogComp, DialogController parentDialogController ) {
    this.context = context;
    this.parentDialogController = parentDialogController;
    setDialogComp( dialogComp );
  }

  /**
   * Set the dialog's properties. If the dialog definition has defined an onCreate-service call, this will be invoked.
   * @param dialogDef the dialog definition
   * @param dialogBuilder the builder used to build this dialog
   * @param context the context in which the dialog will find its data
   * @param delegate
   * @param modelType the expected model's type
   * @param namespaceTable
   */
  public void init( DialogType dialogDef, ControllerBuilder dialogBuilder, Context context, DialogControllerDelegate delegate, Type modelType, NamespaceTable namespaceTable) {
    this.name = dialogDef.getID();
    this.title = dialogDef.getTitle();
    this.fullScreen = Helper.getBoolean( dialogDef.getFullScreen() );
    this.controllerBuilder = dialogBuilder;
    this.context = context;
    this.delegate = delegate;
    this.modelType = modelType;
    this.modelPath = dialogDef.getModel();
    this.namespaceTable = namespaceTable;
  }

  /**
   * Set the dialog's properties. If the dialog definition has defined an onCreate-service call, this will be invoked.
   * @param name the dialog's name
   * @param title the dialog's text in the title bar
   * @param fullScreen if true the dialog will span the whole space on screen
   * @param dialogBuilder the builder used to build this dialog
   * @param context the context in which the dialog will find its data
   * @param delegate
   * @param modelPath an optional path definition for finding the initial model
   */
  public void init( QName name, String title, boolean fullScreen, ControllerBuilder dialogBuilder, Context context,
                    DialogControllerDelegate delegate, String modelPath, NamespaceTable namespaceTable ) {
    this.name = name;
    this.title = title;
    this.fullScreen = fullScreen;
    this.controllerBuilder = dialogBuilder;
    this.context = context;
    this.delegate = delegate;
    this.modelPath = modelPath;
    this.namespaceTable = namespaceTable;
  }

  /**
   * Sets parameterList, this must have happened before calling activate()
   *
   * @param parameterList dialog parameters
   */
  @Override
  public void setParameterList( ParameterList parameterList ) throws Exception {
    this.parameterList = parameterList;
    if (delegate == null) {
      //TODO use parameter in findModel
      Model model = null;
      if ((modelPath != null) && (modelPath.length() > 0)) {
        model = context.findModel( modelPath, model, this.namespaceTable );
      }
      setModel( model );
    }
    else {
      setModel( delegate.initializeDialog( this, parameterList ) );
    }
  }

  @Override
  public NamespaceTable getNamespaceTable() {
    return namespaceTable;
  }

  /**
   * Return GuiOperation with given guiOpID or null if not found.
   *
   * @param guiOpID GuiOperation having eventID==guiOpID or null
   * @return
   */
  @Override
  public GuiOperation getGuiOperation( QName guiOpID ) {
    if ((lastGuiOperation != null ) && (lastGuiOperation.eventID == guiOpID)) {
      return lastGuiOperation;
    }
    else if ((closeOperation != null ) && (closeOperation.eventID == guiOpID)) {
      return closeOperation;
    }
    else {
      for (int i = 0; i < controllerOrder.size(); i++) {
        Controller ctrl = controllerOrder.get( i );
        GuiOperation guiOp = ctrl.getGuiOperation( guiOpID );
        if (guiOp != null) {
          return guiOp;
        }
      }
      for (GuiOperation guiOp : shortCuts.values()) {
        if (guiOp.eventID == guiOpID) {
          return guiOp;
        }
      }
      return null;
    }
  }

  @Override
  public QName getLastGuiOpID() {
    if (lastGuiOperation == null) {
      return null;
    }
    else {
      return lastGuiOperation.eventID;
    }
  }

  /**
   * Called by platform native code when restoring UI state
   *
   * @param lastGuiOpID
   */
  @Override
  public void setLastGuiOpID( QName lastGuiOpID ) {
    if (lastGuiOpID != null) {
      this.lastGuiOperation = getGuiOperation( lastGuiOpID );
    }
  }

  /**
   * Called by dialog builder after having finished creating the dialog, i.e. after
   * having added all children and before refreshBindings() is called.
   */
  public void created() {
    if (delegate != null) {
      delegate.dialogCreated( this );
    }
  }

  public void activate( UIContainer uiContainer ) {
    Logger.info( "Activating dialogCtrl ID="+getName() );

    dialogComp.onTitleChanged( title );
    Platform.getInstance().updateLanguage( dialogComp, getName(), null );

    for (int i = 0; i < controllerOrder.size(); i++) {
      Controller ctrl = controllerOrder.get( i );
      ctrl.activate( uiContainer );
    }
    if (delegate != null) {
      delegate.dialogBindingsInitialized( this );
      if (delegate instanceof SettingsHandler) {
        ((SettingsHandler)delegate).loadSettings();
      }
    }
    this.state = STATE_ACTIVE;
  }

  /**
   * Called to deactivate this controller. It now should stop
   * listening on its binding and detach from its UI.
   *
   * @param saveValue if true controller should save its value
   *                  if false controller should abort any value changes
   */
  public void deactivate( boolean saveValue ) {
    Logger.info( "Deactivating dialogCtrl ID="+getName() );
    for (int i = 0; i < controllerOrder.size(); i++) {
      Controller ctrl = controllerOrder.get( i );
      ctrl.deactivate( saveValue );
    }
  }

  /**
   * Tells all controllers to save their values.
   * Note: This is e.g. necessary before calling a operation to make sure all dialog
   * values are stored in the bindings.
   */
  protected void saveValues() {
    for (int i = 0; i < controllerOrder.size(); i++) {
      Controller ctrl = controllerOrder.get( i );
      if (ctrl instanceof ValueController) {
        ((ValueController) ctrl).saveValue();
      }
    }
  }

  public QName getName() {
    return name;
  }

  public String getTitle() {
    return title;
  }

  public Model getModel() {
    return model;
  }

  public void setModel(Model model) {
    if (model != this.model) {
      if (modelType != null) {
        if (!modelType.isAssignableFrom( model.type() )) {
          throw new IllegalArgumentException( "cannot assign model of type:" + model.type() + " to required model type:" + modelType );
        }
      }
      Model oldModel = this.model;
      this.model = model;
      eventSender.fireEvent( EventListener.ID_MODEL_DATA_CHANGED, this, null, oldModel, model );
    }
  }

  public DialogControllerDelegate getDelegate() {
    return delegate;
  }

  public void setDelegate( DialogControllerDelegate delegate ) {
    this.delegate = delegate;
  }

  public Context getContext() {
    return context;
  }

  public Scope getScope() {
    return scope;
  }

  public void setTitle(String title) {
    this.title = title;
    if (this.dialogComp != null) {
      dialogComp.onTitleChanged( title );
    }
  }

  public ControllerBuilder getControllerBuilder() {
    if (this.controllerBuilder == null) {
      this.controllerBuilder = Platform.getInstance().getControllerBuilder();
    }
    return this.controllerBuilder;
  }

  public MutableControllerGroup getControllerGroup() {
    return this;
  }

  public DialogController getDialogController() {
    return this;
  }

  public DialogController getParentDialogController() {
    Controller parent = getParentController();
    if (parent == null) {
      return null;
    }
    else {
      return parent.getControllerGroup().getDialogController();
    }
  }

  /**
   * Returns first popup controller. Check childPopup recursive to get current popup
   *
   * @return first up controllen
   */
  @Override
  public PopupController getPopupController() {
    return popupController;
  }

  /**
   * Returns true, if this Controller may get the input focus.
   *
   * @return true, if this Controller may get the input focus.
   */
  public boolean isFocusable() {
    return false;
  }

  public Controller getParentController() {
    return parentDialogController;
  }

  /**
   * Changes this controller's parent controller,
   *
   * @param parentController the new parent
   */
  @Override
  public void setParentController( Controller parentController ) {
    this.parentDialogController = (DialogController) parentController;
  }

  public Object getValue() {
    return this.model;
  }

  public void setValue( Object value ) {
    setModel( (Model) value );
  }

  public Dialog getDialogComp() {
    return dialogComp;
  }

  public void addController( QName controllerID, Controller controller ) {
    if (scope.get( controllerID ) != null) {
      throw new IllegalArgumentException("Controller or Variable already exists, name="+controllerID);
    }
    if (controller != null) { //todo: dummy da group controller nicht erzeugt werden!
      scope.add( controller );
      controllerOrder.add( controller );
    }
  }

  @Override
  public boolean validate()  {
    if( dialogValidator !=null )
    {
      try {
        return dialogValidator.validate(this);
      }
      catch (Exception e) {
        e.printStackTrace();
        return false;
      }
    }
    else {
      throw new IllegalArgumentException( "Validator is missing!." );
    }
  }

  public void setValidator( DialogValidator dialogValidator ) {
    this.dialogValidator = dialogValidator;
  }

  /**
   * Returns this group's child having id controllerID or null. If no direct child is found
   * all ControllerGroups are searched recursively for a matching controller.
   * @param controllerID the id of the controller to be returned
   * @return child controller or null
   */
  public Controller getController(QName controllerID) {
    //-- first look for direct children
    for (int i = 0; i < controllerOrder.size(); i++) {
      Controller ctrl = controllerOrder.get( i );
      if (ctrl.getName() == controllerID) {
        return ctrl;
      }
    }
    // not found, so recursively look into controller groups
    for (int i = 0; i < controllerOrder.size(); i++) {
      Controller ctrl = controllerOrder.get( i );
      if (ctrl instanceof ControllerGroup) {
        Controller childCtrl = ((ControllerGroup) ctrl).getController( controllerID );
        if (childCtrl != null) {
          return childCtrl;
        }
      }
    }
    return null;
  }

  public Iterable<Controller> controllerIterator(){
    return controllerOrder;
  }

  public void addShortCut( QName command, GuiOperation guiOp, ActionController controller ) {
    shortCuts.put( command, guiOp );
    dialogComp.registerShortcut( command, controller );
    if (DEBUG) Logger.debug( "Added command=" + command + ", op=" + guiOp );
  }

  /**
   * Sets this dialog's return model. This method is called by builtin
   * GuiOperation "OK"
   * @param returnList this dialog's return model
   */
  public void setReturnList( ParameterList returnList ) {
    this.returnList = returnList;
  }

  /**
   * Stores guiOperation and delegates action processing to ActionThread.
   * The eventID is <UL>
   *   <LI>the actionDef's ID if source is a ButtonController</LI>
   *   <LI>the command if source is a short cut</LI>
   *   <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   * If the called guiOperation directly calls another guiOperation only the last guiOperation is stored here and
   * so if finally a child dialog is opened only the last guiOperation's dialogClosed() is invoked on closing that
   * child dialog.
   *
   * @param guiOperation the GUI operation to be executed
   * @param eventID      the id of the event which is reason for this subAction, see method description
   * @param sourceCtrl       original source of the event may differ from ctrlGroup, e.g. when closing application
   * @param dataContext
   */
  public void subAction( GuiOperation guiOperation, QName eventID, Controller sourceCtrl, Model dataContext ) {
    try {
      this.lastGuiOperation = guiOperation;  // Do this first to make sure that in a sequence of actions the last one is stored
      call( guiOperation, eventID, sourceCtrl, dataContext );
    }
    catch (Exception ex) {
      Logger.error( "Error executing operation for eventID="+eventID, ex );
    }
  }

  public void openChildDialog( QName dialogID, String title, ParameterList parameterList ) {
    DialogType dialogType = getControllerBuilder().getDialogType( dialogID );
    if (dialogType != null) {
      Logger.debug( " try new GUI for " + dialogID );
      openChildDialog( dialogType, title, parameterList );
    }
    else {
      DialogDef dialogDef = getControllerBuilder().getDialogDef( dialogID );
      if (dialogDef != null) {
        Logger.debug( " try old GUI for " + dialogID );
        openChildDialog( dialogDef, title, parameterList );
      }
      else {
        throw new IllegalArgumentException( "GUI definition not found: dialogID=" + dialogID );
      }
    }
  }

  /**
   * Opens child dialog with given definition {@link DialogType} and title
   *
   * @param dialogType user interface definition for the dialog to be opened
   * @param title      title for the dialog, if null title from dialogDef is used
   * @param parameter  dialog parameter list
   * @return handle to the opened dialog for use in {@link GuiOperation#dialogClosed(DialogController, boolean, ParameterList)}
   */
  public void openChildDialog( DialogType dialogType, String title, ParameterList parameter ) {
    getDialogComp().openChildDialog( dialogType, title, parameter );
    Logger.info( "Opened child Dialog from parentID="+getName()+", dialogID="+dialogType.getID().getName() );
  }

  /**
   * Opens child dialog with given definition {@link DialogDef} and title
   *
   * @param dialogDef user interface definition for the dialog to be opened
   * @param title     title for the dialog, if null title from dialogDef is used
   * @param parameter dialog parameter list
   * @return handle to the opened dialog for use in {@link GuiOperation#dialogClosed(DialogController, boolean, ParameterList)}
   * @deprecated old GUI; use new GUI definition instead!
   */
  @Deprecated
  public void openChildDialog( DialogDef dialogDef, String title, ParameterList parameter ) {
    getDialogComp().openChildDialog( dialogDef, title, parameter );
    Logger.info( "Opened child Dialog from parentID="+getName()+", dialogID="+dialogDef.getID().getName() );
  }

  /**
   * Opens popup with given moduleID and title
   *
   * @param moduleID      ID of the module to be opened in a Popup
   * @param title         title for the dialog, if null title from dialog resource is used
   * @param parameterList dialog parameter list
   */
  @Override
  public void showPopup( QName moduleID, String title, ParameterList parameterList ) {

    de.pidata.gui.guidef.Module moduleDef = Platform.getInstance().getControllerBuilder().getApplication().getModule( moduleID );
    DialogModuleType dialogModuleType = Platform.getInstance().getControllerBuilder().getApplication().getDialogModule( moduleID );

    PopupController parentPopup = getTopPopupController();
    PopupController newPopupCtrl = new PopupController( parentPopup );
    ModuleViewPI moduleViewPI = new ModuleViewPI( null, null );
    newPopupCtrl.init( POPUP, this, moduleViewPI, null, null, false );
    if (popupController == null) {
      popupController = newPopupCtrl;
      addController( POPUP, popupController );
    }
    else {
      parentPopup.setChildPopup( newPopupCtrl );
    }
    ModuleGroup moduleGroup;
    if (moduleDef != null) {
      moduleGroup = initPopup( moduleDef, newPopupCtrl, parameterList );
    }
    else {
      moduleGroup = initPopup( dialogModuleType, newPopupCtrl, parameterList );
    }

    newPopupCtrl.setModuleGroup( moduleGroup, ViewAnimation.NONE );

    getDialogComp().showPopup( newPopupCtrl, moduleGroup, title );
  }

  @Deprecated
  private ModuleGroup initPopup( de.pidata.gui.guidef.Module moduleDef, PopupController popupCtrl, ParameterList parameterList  ) {
    try {
      ControllerBuilder builder = Platform.getInstance().getControllerBuilder();
      String classname = moduleDef.getController();
      ModuleGroup moduleGroup;
      if ((classname == null) || (classname.length() == 0)) {
        moduleGroup = new ModuleGroup();
      }
      else {
        moduleGroup = (ModuleGroup) Class.forName( classname ).newInstance();
      }
      moduleGroup.init( moduleDef.getName(), popupCtrl, null, false, parameterList );
      builder.addChildControllers( moduleGroup, moduleDef, false );
      return moduleGroup;
    }
    catch (Exception ex) {
      Logger.error( "Error creating popup controller", ex );
      return null;
    }
  }

  private ModuleGroup initPopup( DialogModuleType moduleDef, PopupController popupCtrl, ParameterList parameterList ) {

    try {
      ControllerBuilder builder = Platform.getInstance().getControllerBuilder();
      String classname = moduleDef.getModuleClass();
      ModuleGroup moduleGroup;
      if ((classname == null) || (classname.length() == 0)) {
        moduleGroup = new ModuleGroup();
      }
      else {
        moduleGroup = (ModuleGroup) Class.forName( classname ).newInstance();
      }
      moduleGroup.init( moduleDef.getName(), popupCtrl, null, false, parameterList );
      builder.addChildControllers( moduleGroup, moduleDef, false );
      return moduleGroup;
    }
    catch (Exception ex) {
      Logger.error( "Error creating popup controller", ex );
      return null;
    }
  }

  private PopupController getTopPopupController() {
    PopupController parentPopup = popupController;
    if (parentPopup != null) {
      while (parentPopup.getChildPopup() != null) {
        parentPopup = parentPopup.getChildPopup();
      }
    }
    return parentPopup;
  }

  @Override
  public void popupClosed( PopupController popupController, ModuleGroup oldModuleGroup ) {
    PopupController parentPopup = popupController.getParentPopup();
    if (parentPopup == null) {
      scope.remove( popupController );
      controllerOrder.remove( popupController );
      this.popupController = null;
    }
    else {
      parentPopup.childPopupClosed( popupController );
    }
    if (delegate != null) {
      delegate.popupClosed( oldModuleGroup );
    }
  }

  /**
   * Called after child dialog has been closed
   *
   * @param ok         true if dialog was closed with ok, false if it was closed with cancel
   * @param resultList list of dialog's result parameters (never null)
   */
  @Override
  public void childDialogClosed( boolean ok, ParameterList resultList ) {
    Logger.info( "Closed child Dialog of parentID=" + getName() );
    try {
      if (null != lastGuiOperation) {
        lastGuiOperation.dialogClosed( this, ok, resultList );
      }
      if (delegate != null) {
        delegate.dialogClosed( this, ok, resultList );
      }
    }
    catch (Exception ex) {
      Logger.error( "Error calling dialogClosed", ex );
    }
  }

  public Model fetchDataSource( QName dataSourceID) {
    Model param = null;
    if (dataSourceID != null) {
      ModelSource source = (ModelSource) this.scope.get(dataSourceID);
      if (source != null) {
        param = source.getModel();
      }
    }
    else {
      param = getModel();
    }
    return param;
  }

  /**
   * Returns the next focusable controller forward/backward
   * @param fromController the controller where to start search
   * @param forward true for forward direction, false for backward
   * @return the next/previous focusable controller or null if fromController is last/first reached
   */
  public Controller getNextFocusable(Controller fromController, boolean forward) {
    int index = -1;
    if (fromController != null) {
      index = this.controllerOrder.indexOf(fromController);
      if (index >= 0) {
        // fromController is direct child of Dialog
        return getNextFocusableInDialog( fromController, forward );
      }
      else {
        // fromController is inner child
        Controller fromWithinParentController = fromController;
        ControllerGroup fromParent = fromController.getControllerGroup();
        while (fromParent != null) {
          Controller nextController = fromParent.getNextFocusable( fromWithinParentController, forward );
          if (nextController != null) {
            return nextController;
          }
          fromWithinParentController = fromParent;
          fromParent = fromParent.getControllerGroup();
        }
      }
    }
    else {
      // first shot
      return getNextFocusableInDialog( null, forward );
    }
    return null;
  }

  private Controller getNextFocusableInDialog( Controller fromController, boolean forward ) {
    int index;
    if (fromController == null) {
      if (forward) index = -1;
      else index = this.controllerOrder.size();
    }
    else {
      index = this.controllerOrder.indexOf(fromController);
    }
    if (forward) {
      while (index < controllerOrder.size()-1) {
        index++;
        Controller nextController = this.controllerOrder.get( index );
        if (nextController instanceof ControllerGroup) {
          nextController = ((ControllerGroup) nextController).getNextFocusable( null, forward );
        }
        if (nextController != null && nextController.isFocusable()) {
          return nextController;
        }
      }
    }
    else {
      while (index > 0) {
        index--;
        Controller nextController = this.controllerOrder.get( index );
        if (nextController instanceof ControllerGroup) {
          nextController = ((ControllerGroup) nextController).getNextFocusable( null, forward );
        }
        if (nextController != null && nextController.isFocusable()) {
          return nextController;
        }
      }
    }
    return null;
  }

  public boolean processCommand( QName cmd, char keyChar, int index ) {
    Platform platform = Platform.getInstance();
    if ((cmd == InputManager.CMD_PREV_COMP)
        || (platform.useCursor4Focus() && ((cmd == InputManager.CMD_UP) || (cmd == InputManager.CMD_LEFT)))) {
      return prevFocus( keyChar, index );
    }
    else if ((cmd == InputManager.CMD_NEXT_COMP)
             || (platform.useCursor4Focus() && ((cmd == InputManager.CMD_DOWN) || (cmd == InputManager.CMD_RIGHT)))) {
      return nextFocus( keyChar, index );
    }
    else {
      GuiOperation guiOp = shortCuts.get( cmd );
      if (guiOp == null) {
        if (DEBUG) Logger.debug("command not found: " + cmd);
        return false;
      }
      else {
        if (DEBUG) Logger.debug("command found: " + cmd);
        PaintManager paintManager = PaintManager.getInstance();
        if (paintManager != null) {
          Popup activePopup = paintManager.getActivePopup();
          if (activePopup != null) {
            activePopup.closePopup();
          }
        }
        if (this.focusController != null) {
          this.focusController.onFocusChanged( getName(), false );
        }
        this.subAction( guiOp, cmd, focusController, getModel() );
        return true;
      }
    }
  }

  protected boolean nextFocus( char keyChar, int index ) {
    Controller nextCtrl = getNextFocusable( this.focusController, true );
    if (nextCtrl == null) return true;
    else {
      return nextCtrl.processCommand( InputManager.CMD_GRAB_FOCUS, keyChar, index );
    }
  }

  private boolean prevFocus( char keyChar, int index ) {
    Controller nextCtrl = getNextFocusable( this.focusController, false );
    if (nextCtrl == null) return true;
    else {
      return nextCtrl.processCommand( InputManager.CMD_GRAB_FOCUS, keyChar, index );
    }
  }

  /**
   * Called by Component whenever a input command arrives.
   *
   * @param compID
   *@param cmd       the command, one of the constants InputManager.CMD_*
   * @param inputChar optional input character for the command
   * @param index     optional index parameter for the command @return true if the command has been processed, otherwise false
   */
  public boolean command( QName compID, QName cmd, char inputChar, int index ) {
    return processCommand( cmd, inputChar, index );
  }

  public void performOnClick() {
    // do nothing
  }

  /**
   * Invoked when component is selected/deselected
   *
   * @param componentID
   * @param posX
   * @param posY
   * @param selected     true if source was selected, false if source was deselected
   */
  public void selected( QName componentID, short posX, short posY, boolean selected ) {
    /// do nothing
  }

  /**
   * Called by Component whenever it gains or looses input focus.
   *
   * @param compID
   *@param hasFocus true if component gained input focus @return if false focus switch is not allowed by ComponentListener
   */
  public void onFocusChanged( QName compID, boolean hasFocus ) {
    // do nothing
  }

  public void setVisibleRange( QName componentID, short minX, short minY, short maxX, short maxY ) {
    // do nothing
  }

  /**
   * This method is called when this controller's dialog is closing.
   * The controller has to realease all its resources, e.g. remove
   * itself as istener.
   */
  public synchronized void closing() {
    if (this.state != STATE_OK) {
      this.state = STATE_CANCELED;
    }
    notifyAll();
  }

  /**
   * Adds listener to this Model's listener list
   *
   * @param listener the listener to be added
   */
  public void addListener( EventListener listener ) {
    this.eventSender.addListener( listener );
  }

  /**
   * Removes listener from this Model's listener list
   *
   * @param listener the listener to be removed
   */
  public void removeListener( EventListener listener ) {
    if (this.eventSender != null) {
      this.eventSender.removeListener(listener);
    }
  }

  /**
   * Method for internal usage only
   * @param dialogController
   */
  public void setChildDialogController( DialogController dialogController ) {
    this.childDialogController = dialogController;
  }

  /**
   * Sets operation to be invoked if dialog is going to be closed. If not null
   * the closeOperation is responsible for closing this dialog by calling doClose()
   * and must not call close().
   *
   * @param closeOperation close operation or null
   */
  @Override
  public void setOnCloseOperation( GuiOperation closeOperation ) {
    this.closeOperation = closeOperation;
  }

  /**
   * Returns on close operation, see setOnCloseOperation() for details.
   *
   * @return the current on close operation, may be null
   */
  @Override
  public GuiOperation getOnCloseOperation() {
    return closeOperation;
  }

  /**
   * Closes this dialog by calling doClose(), if dialogComp.closing() returns true
   * and close operation is null.
   *
   * If dialogComp.closing() returns true and close operation is not null that
   * close operation is called and its up to that operation to call doClose() to
   * finally close this dialog.
   *
   * @param ok true if dialog is closed with OK
   * @return true if dialog has been closed successfully,
   *         false if a close operation has been called
   */
  @Override
  public boolean close( boolean ok, DialogController source ) {
    if (getDialogComp().closing( ok )) {
      if (closeOperation != null && ok) {
        subAction( closeOperation, null, source, getModel() );
      }
      else {
        doClose( ok );
      }
      return ((state == STATE_CANCELED) || (state == STATE_OK));
    }
    else {
      return false;
    }
  }

  @Override
  public boolean close( boolean ok ) {
    return close( ok, null );
  }

  /**
   * Called by Platforms having a special "Back-Button" (e.g. Android) if that Button is pressed.
   */
  @Override
  public void onBackPressed() {
    if (delegate == null) {
      close( false );
    }
    else {
      delegate.backPressed( this );
    }
  }

  /**
   * Closes this dialog, does not call close operation
   *
   * @param ok true if dialog is closed with OK
   */
  @Override
  public void doClose( boolean ok ) {

    if (ok) {
      saveValues();
      this.state = STATE_OK;
      if (this.originalModel != null) {
        ModelHelper.updateOriginal( this.originalModel, getModel() );
      }
    }
    else {
      this.state = STATE_CANCELED;
    }

    if (delegate == null) {
      returnList = AbstractParameterList.EMPTY;
    }
    else {
      if (delegate instanceof SettingsHandler) {
        ((SettingsHandler)delegate).saveSettings();
      }
      returnList = delegate.dialogClosing( this, ok );
    }
    deactivate( false );
    this.dialogComp.close( ok, returnList );
  }

  public void call( GuiOperation operation, QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    Logger.info( "Calling GuiOp " + operation.getSimpleName() + ", eventID="+eventID+", dialogCtrlID="+getName() );
    int progress = Platform.getInstance().getStartupProgress();
    if (progress < 100) {
      Logger.warn("System startup not finished ("+progress+"%), ignoring call guiOp: "+operation + " " + operation.getCommand() + ", eventID="+eventID );
      return;
    }
    if (DEBUG) Logger.debug("call guiOp: "+operation + " " + operation.getCommand() + ", eventID="+eventID );
    saveValues();
    operation.execute( eventID, sourceCtrl, dataContext );
  }

  public void setDialogComp( Dialog dialog ) {
    this.dialogComp = dialog;
    dialogComp.setController( this );
  }

  public Model getOriginalModel() {
    return originalModel;
  }

  /**
   * Returns this dialog's return model
   * @return this dialog's return model
   */
  public ParameterList getReturnList() {
    return returnList;
  }

  /**
   * Called by a child controller to notify it has gained focus
   * @param focusController
   */
  public void setFocusController( Controller focusController ) {
    this.focusController = focusController;
  }

  public Controller getFocusController() {
    return focusController;
  }

  /**
   * Returns this ControllerGroup's parent group
   *
   * @return this ControllerGroup's parent group
   */
  public ControllerGroup getParentGroup() {
    return null;
  }

  /**
   * Returns this controller's view  or null if controller has no view
   *
   * @return this controller's view or null
   */
  public ViewPI getView() {
    //TODO: eingefügt, um alten code compilierbar zu machen
    throw new RuntimeException( "TODO" );
  }

  public boolean isActive() {
    return (state != STATE_INITIAL);
  }

  /**
   * Creates a renderer matching this controller
   *
   *
   * @param value@return a renderer matching this controller
   */
  @Override
  public String render( Object value ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void valueChanged( Binding source, Object newValue ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void modelChanged( Binding source, Model newModel ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Called by Binding after it's modelSource has been changed
   *
   * @param source
   * @param modelSource the new modelSource, never null
   */
  @Override
  public void modelSourceChanged( Binding source, ModelSource modelSource ) {
    // do nothing - only valid for ValueController
  }

  /**
   * Show a message dialog
   *
   * @param title   the dialog title
   * @param message the messsage, use CR (\n) for line breaks
   */
  @Override
  public void showMessage( String title, String message ) {
    getDialogComp().showMessage( title, message );
  }

  /**
   * Show a question dialog dialog
   *
   * @param title          the dialog title
   * @param message        the messsage, use CR (\n) for line breaks
   * @param btnYesLabel    the label for yes/ok button or null to hide button
   * @param btnNoLabel     the label for no button or null to hide button
   * @param btnCancelLabel the label for cancel button or null to hide button
   */
  public void showQuestion( String title, String message, String btnYesLabel, String btnNoLabel, String btnCancelLabel ) {
    getDialogComp().showQuestion( title, message, btnYesLabel, btnNoLabel, btnCancelLabel );
  }

  /**
   * Show a dialog with a text input field
   *
   * @param title          the dialog title
   * @param message        the messsage, use CR (\n) for line breaks
   * @param defaultValue   a default value for the input field
   * @param btnOKLabel     the label for ok button or null to hide button
   * @param btnCancelLabel the label for cancel button or null to hide button
   */
  public void showInput( String title, String message, String defaultValue, String btnOKLabel, String btnCancelLabel ) {
    getDialogComp().showInput( title, message, defaultValue, btnOKLabel, btnCancelLabel );
  }

  /**
   * Shows a progress dialog over this dialog
   *
   * @param title     the dialog title
   * @param message   the messsage, use CR (\n) for line breaks
   * @param fromValue the value representing 0%
   * @param toValue   the value representing 100%
   * @param cancelable true if progress dialog can be canceled by user
   * @return the controller for the created progress dialog
   */
  @Override
  public ProgressController showProgressDialog( String title, String message, int fromValue, int toValue, boolean cancelable ) {
    return getControllerBuilder().createProgressDialog( title, message, fromValue, toValue, cancelable,this );
  }

  /**
   * Shows the platform specific file chooser dialog.
   *
   * @param title             dialog title
   * @param fileChooserParams parameters
   */
  @Override
  public void showFileChooser( String title, FileChooserParameter fileChooserParams ) {
    getDialogComp().showFileChooser( title, fileChooserParams );
  }

  /**
   * Shows the platform specific image chooser dialog.
   * <ul>
   *   <li>assure client has rights to write on destination</li>
   *   <li>use {@link SystemManager#getStorage(String)} and {@link Storage#getPath(String)} to get absolute Path on different Platforms</li>
   * </ul>
   *
   * @param imageChooserType type of the image chooser
   * @param filePath         <b>absolute</b> path of file that has to be used, if image will be created (by using camera)
   */
  @Override
  public void showImageChooser( ImageChooserType imageChooserType, String filePath ) {
    getDialogComp().showImageChooser( imageChooserType, filePath );
  }

  /**
   * Show a toast message, i.e. a message which appears only for a short moment
   * and ten disappears.
   *
   * @param message the message to show
   */
  @Override
  public void showToast( String message ) {
    getDialogComp().showToast( message );
  }

  /**
   * Temporary method from transporting old GUI definition's inputMde and format
   *
   * @param inputMode
   * @param format
   */
  @Override
  public void setInputMode( InputMode inputMode, Object format ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void showContextMenu( double mouseDownX, double mouseDownY ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public String toString() {
    return name.getName();
  }

  /*

   */
  public void updateChildrenLanguage() {
    updateChildControllers( controllerIterator() );
  }

  private void updateChildControllers( Iterable<Controller> controllers ) {
    for (Controller controller : controllers) {
      if (controller instanceof ModuleController) {
        ModuleGroup moduleGroup = ((ModuleController)controller).getCurrentModule();
        if (moduleGroup != null) {
          UIContainer moduleContainer = moduleGroup.getUIContainer();
          Platform.getInstance().updateLanguage( moduleContainer, ((ModuleController) controller).getCurrentModule().getName(), null );
          updateChildControllers( ((ModuleController) controller).getCurrentModule().controllerIterator() );
        }
      }
    }
  }
}
