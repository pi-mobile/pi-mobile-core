/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.guidef.BuiltinOperation;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.service.base.ServiceException;

public class GuiBuiltinOperation extends GuiOperation {

  private BuiltinOperation operationID;
  private QName childDialogID;
  private Model parameter;

  /**
   * initialize a gui operation
   *
   * @param eventID     the eventID this GuiOp is interested in or null if any
   * @param operationID the builtin operation
   */
  public void init( QName eventID, BuiltinOperation operationID, Model parameter, ControllerGroup controllerGroup ) {
    super.init( eventID, null, controllerGroup );
    this.operationID = operationID;
    this.parameter = parameter;
  }

  /**
   * initialize a gui operation
   *
   * @param eventID     the eventID this GuiOp is interested in or null if any
   * @param childDialogID the dialog to open as child
   */
  public void init( QName eventID, QName childDialogID, Model parameter, ControllerGroup controllerGroup ) {
    super.init( eventID, null, controllerGroup );
    this.childDialogID = childDialogID;
    this.parameter = parameter;
  }

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID   the id of the event, see method description
   * @param sourceCtrl    original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    try {
      DialogController dlgController = sourceCtrl.getDialogController();
      if (operationID != null) {
        executeBuiltin( dlgController, parameter, operationID );
      }
      else if (childDialogID != null) {
        openChildDialog( dlgController, parameter, childDialogID );
      }
    }
    catch (Exception ex) {
      String msg = "Could execute builtin operationID=" + operationID;
      Logger.error( msg, ex );
      throw new ServiceException( ServiceException.SERVICE_FAILED, msg, ex );
    }
  }
}
