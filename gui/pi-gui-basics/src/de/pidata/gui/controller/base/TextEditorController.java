/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.models.tree.ValidationException;

public class TextEditorController extends TextController {

  protected void setValidationError( ValidationException vex ) {
    //TODO refactoring
//    String errorMsg = null;
//    if (vex != null) {
//      errorMsg = vex.getMessage();
//    }
//    this.textArea.setInfoText( ViewPI.INFO_ERROR, errorMsg );
  }

}
