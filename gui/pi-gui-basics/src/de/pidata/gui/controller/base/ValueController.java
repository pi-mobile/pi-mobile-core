/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.models.binding.Binding;
import de.pidata.models.binding.BindingListener;

public interface ValueController extends Controller, BindingListener {

  void setReadOnly( boolean readOnly );

  boolean isReadOnly();

  void initValueBinding( Binding attributeBinding );

  void initLabelBinding( Binding labelBinding );

  void initVisibilityBinding( Binding visibilityBinding );

  /**
   * Returns this controller's value binding
   * 
   * @return value Binding or null
   */
  Binding getValueBinding();

  /**
   * Called to tell this ValueController to store its value in it's value binding.
   * Note: This happens e.g. before a dialog executes a operation to
   * make sure all dialog values are available to that operation.
   */
  void saveValue();
}
