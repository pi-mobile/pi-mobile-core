/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.view.base.ListViewPI;
import de.pidata.models.tree.ModelSource;
import de.pidata.models.binding.Selection;
import de.pidata.models.tree.Model;
import de.pidata.qnames.Key;

public interface SelectionController extends ValueController, ModelSource {

  /**
   * Returns current index of key or -1 if key is not visible
   * @param key
   * @return
   */
  short indexOf( Key key );

  /**
   * Returns the total row count of this data source
   *
   * @return the total row count of this data source
   */
  int rowCount();

  /**
   * Returns the current selected row model
   *
   * @return the current selected row model
   * @param index
   */
  Model getSelectedRow( int index );

  /**
   * Returns the selection this ModelSource uses
   *
   * @return the selection this ModelSource uses
   */
  Selection getSelection();

  /**
   * Returns the view used for selection
   * @return the selection component
   */
  ListViewPI getListView();

  boolean hasFirstRowForEmptySelection();

  /**
   * Called by ListViewPI whenever a row has been selected or deselected
   *
   * @param selectedRow the row which was selected
   * @param selected    true if row has been selected, false if it has been deselected
   */
  void onSelectionChanged( Model selectedRow, boolean selected );

  void onPrevSelected();

  void onNextSelected();

  /**
   * Sets this controller's detail module
   * @param detailCtrl the detail module
   */
  void initDetailController( Controller detailCtrl );

  /**
   * Returns this controller's detail module
   * @return the detail module
   */
  Controller getDetailController();

  void selectRow( Model row );
}
