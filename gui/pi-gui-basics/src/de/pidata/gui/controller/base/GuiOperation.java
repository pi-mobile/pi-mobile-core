/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.event.InputManager;
import de.pidata.gui.guidef.*;
import de.pidata.models.service.Operation;
import de.pidata.models.service.Service;
import de.pidata.qnames.NamespaceTable;
import de.pidata.service.base.ServiceException;
import de.pidata.models.service.ServiceManager;
import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Type;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;
import de.pidata.system.base.BackgroundSynchronizer;
import de.pidata.system.base.SystemManager;

import java.io.IOException;
import java.util.Hashtable;

/* Encapsulate a GUI operation and its data source.
 * DataSourceID is the ID of a controller implementing ModelSource. If dataSourceID is not null
 * that controller's selected row is fetched and used as parameter when processing action. If
 * data source returns null as selected row action processing is canceled.
 */
public abstract class GuiOperation {

  public static final Namespace NAMESPACE = Namespace.getInstance("de.pidata.gui");

  public static final QName SERVICE_GUI = NAMESPACE.getQName("GuiService");
  public static final QName PROP_DIALOG = NAMESPACE.getQName("DIALOG");

  protected String command;
  protected QName  eventID;
  protected ControllerGroup controllerGroup;

  protected boolean requireAdminRights = false;

  /**
   * initialize a gui operation
   * @param eventID the eventID this GuiOp is interested in or null if any
   * @param command the command to invoke this GuiOperation
   * @param controllerGroup the controllerGroup this GuiOperation belongs to
   */
  public void init( QName eventID, String command, ControllerGroup controllerGroup ) {
    this.command = command;
    this.eventID = eventID;
    if (controllerGroup == null) {
      throw new IllegalArgumentException( "GuiOperation's controllerGroup must not be null" );
    }
    this.controllerGroup = controllerGroup;
  }

  public String getCommand() {
    return command;
  }

  public void setRequireAdminRights(boolean requireAdminRights) {
    this.requireAdminRights = requireAdminRights;
  }

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   *   <LI>the actionDef's ID if source is a ButtonController</LI>
   *   <LI>the command if source is a short cut</LI>
   *   <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>

   * @param eventID    the id of the event, see method description
   * @param sourceCtrl     original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  public abstract void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException;

  protected void executeBuiltin( Controller sourceCtrl, QName dataSourceID, QName serviceID, QName operationID) throws ServiceException {
    DialogController dlgController = sourceCtrl.getDialogController();
    Model parameter = dlgController.fetchDataSource( dataSourceID );
    if (serviceID == null) {
      BuiltinOperation builtinOperation = BuiltinOperation.fromString( operationID.getName() );
      if (builtinOperation != null) {
        if (builtinOperation == BuiltinOperation.openDialog) {
          openChildDialog( dlgController, parameter, operationID );
        }
        else {
          executeBuiltin( dlgController, parameter, builtinOperation );
        }
      }
    }
    else {
      ServiceManager sm = ServiceManager.getInstance();
      if (sm != null) {
        sm.invokeService( dlgController.getContext(), serviceID, operationID, parameter );
      }
    }
  }

  public static void executeBuiltin( Controller sourceCtrl, Model parameter, BuiltinOperation operationID) {
    DialogController dlgController = sourceCtrl.getDialogController();
    if (operationID == BuiltinOperation.Ok) {
      if (dlgController.getOnCloseOperation() == null) {
        dlgController.close( true );
      }
      else {
        dlgController.close( true, dlgController );
      }
    }
    else if (operationID == BuiltinOperation.Cancel) {
      dlgController.close( false );
    }
    else if (operationID == BuiltinOperation.Validate) {
      if (dlgController.validate()) {
        if (dlgController.getOnCloseOperation() == null) {
          dlgController.close( true );
        }
        else {
          dlgController.close( true, dlgController );
        }
      }
    }
    else if (operationID == BuiltinOperation.Sync) {
      BackgroundSynchronizer.getInstance().synchronize( false );
    }
    else if (operationID == BuiltinOperation.Exit) {
      ServiceManager.getInstance().shutdown();
      Platform.getInstance().exit( dlgController.getContext() );
    }
    else if (operationID == BuiltinOperation.Prev) {
      Model curModel = dlgController.getModel();
      Model prevModel = curModel.prevSibling( curModel.getParentRelationID() );
      if (prevModel != null) {
        dlgController.setModel( prevModel );
      }
    }
    else if (operationID == BuiltinOperation.Next) {
      Model curModel = dlgController.getModel();
      Model nextModel = curModel.nextSibling( curModel.getParentRelationID() );
      if (nextModel != null) {
        dlgController.setModel( nextModel );
      }
    }
    else if (operationID == BuiltinOperation.PrevSelection) {
      Controller selection = getSelection( dlgController, parameter );
      selection.processCommand( InputManager.CMD_UP, ' ', 0 );
    }
    else if (operationID == BuiltinOperation.NextSelection) {
      Controller selection = getSelection( dlgController, parameter );
      selection.processCommand( InputManager.CMD_DOWN, ' ', 0 );
    }
    else if (operationID == BuiltinOperation.InitBindings) {
      // no longer needed --> do nothing
    }
    else {
      throw new IllegalArgumentException( "unsupported OperationType: " + operationID );
    }
  }

  public static void openChildDialog( Controller sourceCtrl, Model parameter, QName childDialogID) {
    ParameterList parameterList;
    if (parameter == null) {
      parameterList = AbstractParameterList.EMPTY;
    }
    else {
      parameterList = new ModelParameterList( parameter );
    }
    DialogController dlgCtrl = sourceCtrl.getDialogController();
    dlgCtrl.openChildDialog( childDialogID, null, parameterList );
  }

  private static Controller getSelection( ControllerGroup ctrlGroup, Model parameter) {
    GuiServiceInput input = (GuiServiceInput) parameter;
    PartType selDef = input.getSelection();
    QName selController = selDef.getController();
    Controller selection = ctrlGroup.getController(selController);
    return selection;
  }

  public static void openChildDialog( Controller sourceCtrl, QName dialogID, String title, ParameterList parameterList) {
    DialogController dlgCtrl = sourceCtrl.getDialogController();
    dlgCtrl.openChildDialog( dialogID, title, parameterList );
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    // to be overwritten by children
  }

  /**
   * Returns a string representation of the object. In general, the
   * <code>toString</code> method returns a string that
   * "textually represents" this object. The result should
   * be a concise but informative representation that is easy for a
   * person to read.
   * It is recommended that all subclasses override this method.
   * <p/>
   * The <code>toString</code> method for class <code>Object</code>
   * returns a string consisting of the name of the class of which the
   * object is an instance, the at-sign character `<code>@</code>', and
   * the unsigned hexadecimal representation of the hash code of the
   * object. In other words, this method returns a string equal to the
   * value of:
   * <blockquote>
   * <pre>
   * getClass().getName() + '@' + Integer.toHexString(hashCode())
   * </pre></blockquote>
   *
   * @return a string representation of the object.
   */
  public String toString() {
    return "GUIOperation class="+getClass().getName();
  }

  public String getSimpleName() {
    return getClass().getSimpleName();
  }

  public static void showMessage( Controller sourceCtrl, String title, String message ) {
    DialogController dlgCtrl = sourceCtrl.getDialogController();
    dlgCtrl.showMessage( title, message );
  }

  public static int showInternalError( Controller sourceCtrl, String message, Exception ex) throws ServiceException {
    showMessage( sourceCtrl, "internal error", message );
    throw new ServiceException( ServiceException.SERVICE_FAILED, message, ex );
  }

  /**
   * Show a question dialog dialog
   *
   * @param title          the dialog title
   * @param message        the messsage, use CR (\n) for line breaks
   * @param btnYesLabel    the label for yes/ok button or null to hide button
   * @param btnNoLabel     the label for no button or null to hide button
   * @param btnCancelLabel the label for cancel button or null to hide button
   */
  public static void showQuestion( Controller sourceCtrl, String title, String message,
                                   String btnYesLabel, String btnNoLabel, String btnCancelLabel ) {
    DialogController dlgCtrl = sourceCtrl.getDialogController();
    dlgCtrl.showQuestion( title, message, btnYesLabel, btnNoLabel, btnCancelLabel );
  }

  /**
   * Show a dialog with a text input field
   *
   * @param title          the dialog title
   * @param message        the messsage, use CR (\n) for line breaks
   * @param defaultValue   a default value for the input field
   * @param btnOKLabel     the label for ok button or null to hide button
   * @param btnCancelLabel the label for cancel button or null to hide button
   */
  public static void showInput( Controller sourceCtrl, String title, String message, String defaultValue, String btnOKLabel, String btnCancelLabel ) {
    DialogController dlgCtrl = sourceCtrl.getDialogController();
    dlgCtrl.showInput( title, message, defaultValue, btnOKLabel, btnCancelLabel );
  }

  /**
   * Show a dialog with a password input field
   *
   * @param title          the dialog title
   * @param message        the messsage, use CR (\n) for line breaks
   * @param defaultValue   a default value for the input field
   * @param btnOKLabel     the label for ok button or null to hide button
   * @param btnCancelLabel the label for cancel button or null to hide button
   */
  public static void showPasswordPrompt( Controller sourceCtrl, String title, String message, String defaultValue, String btnOKLabel, String btnCancelLabel ) {
    DialogController dlgCtrl = sourceCtrl.getDialogController();
    // TODO: Implement dlgCtrl.showPasswordPrompt( title, message, defaultValue, btnOKLabel, btnCancelLabel );
    // TODO: for JavaFX there is a FXPasswordDialog
    dlgCtrl.showInput( title, message, defaultValue, btnOKLabel, btnCancelLabel );

    // return dlgCtrl.showPasswordPrompt( title, message, defaultValue, btnOKLabel, btnCancelLabel );
  }

  /**
   * Returns the model defined by the message and part definition
   * within the dialog controller's model.
   * @param partDef message part definition
   * @param ctrlGroup
   * @param variables
   * @return the model found by the message and part definition
   * @throws java.io.IOException
   */
  public static Model fetchModel( PartType partDef, ControllerGroup ctrlGroup, Hashtable variables ) throws IOException {
    Model model;
    String value;

    value = partDef.getValue();
    if ((value != null) && (value.length() > 0)) {
      model = ModelFactoryTable.getInstance().getFactory( NAMESPACE ).createInstance(StringType.getDefString(), value);
    }
    else {
      QName varName = partDef.getVariable();
      if (varName != null) {
        model = (Model) variables.get(varName);
      }
      else {
        QName controllerName = partDef.getController();
        if (controllerName != null) {
          // service must be a GuiService - evaluation will be processed there
          model = partDef;
        }
        else {
          model = findModel( ctrlGroup, partDef.getDataSource(), partDef.getPath(), partDef.namespaceTable() );
        }
      }
    }
    return model;
  }

  /**
   * Returns the model defined by dataSource and modelStr
   *
   * @param dataSource the ID of the controller to be used as data source, if null dialog's model is data source
   * @param modelStr   path to model, if null data source is returned, root path is this dialog's context root
   * @param namespaceTable the namespace table to use when evaluating modelStr - needed to map from prefixes to namespaces
   * @return the model defined by dataSource and modelStr
   */
  public static Model findModel( ControllerGroup ctrlGroup, QName dataSource, String modelStr, NamespaceTable namespaceTable ) {
    Model result;
    Model currentModel;
    if (dataSource == null) {
      currentModel = ctrlGroup.getModel();
    }
    else {
      currentModel = ctrlGroup.fetchDataSource( dataSource );
    }
    if ((modelStr != null) && (modelStr.length() > 0)) {
      result = ctrlGroup.getDialogController().getContext().findModel( modelStr, currentModel, namespaceTable );
    }
    else {
      result = currentModel;
    }
    return result;
  }

  public static Model processCreate(Model model, Context context, CreateType create) throws IOException {
    QName typeName = create.getType();
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( typeName.getNamespace() );
    Type type = factory.getType(typeName);
    Key modelID = null;
    if ((type instanceof ComplexType) && ((ComplexType) type).keyAttributeCount() > 0) {
      modelID = SystemManager.getInstance().createKey(null);
    }
    if (type == null) {
      type = factory.getRootRelation(typeName).getChildType();
    }
    Model child = factory.createInstance(modelID, type, null, null, null);
    String pathStr = create.getPath();
    if ((pathStr != null) && (pathStr.length() > 0)) {
      XPath path = new XPath( create.namespaceTable(), pathStr);
      model = path.getModel( model, context );
    }
    QName relation = create.getRelation();
    if (relation == null) {
      return child;
    }
    else {
      model.add(relation, child);
      return model;
    }
  }

  /**
   * Provides a method for calling a service from a dialog control.
   * CAUTION: the controller must know its context before a service can be invoked!
   * @param ctrlGroup the dialog controller which calls the service
   * @param invoke containing the service definition
   * @param parameter the model at which evaluation starts
   * @return the model returned by the service call
   * @throws ServiceException
   * @throws IOException
   */
  public static Model processInvoke( ControllerGroup ctrlGroup, InvokeType invoke, Model parameter, Hashtable variables)
  throws ServiceException, IOException {
    Model outputMsg, result;
    Model message;

    ServiceManager sm = ServiceManager.getInstance();
    String serviceName = invoke.getService().getName();
    QName opName = invoke.getOperation();
    Service service = sm.getService(serviceName);

    message = buildServiceInput(ctrlGroup, service, opName, invoke, variables);

    Context context = ctrlGroup.getDialogController().getContext();
    context.set( PROP_DIALOG, ctrlGroup );
    outputMsg = sm.invokeService(context, serviceName, opName.getName(), message);

    result = buildServiceOutput(ctrlGroup, service, opName, invoke, outputMsg, variables, parameter);
    return result;
  }

  /**
   * Create a model containing the output elements.
   * @param ctrlGroup
   * @param service
   * @param opName
   * @param invoke
   * @param outputMsg the output container from the called service
   * @param variables
   * @param parameter
   * @return the model containing the output elements
   */
  private static Model buildServiceOutput( ControllerGroup ctrlGroup, Service service, QName opName, InvokeType invoke, Model outputMsg, Hashtable variables, Model parameter) {
    PartType partDef;
    String value;
    Model outParam;
    QName varName;
    String pathStr;
    XPath path;
    Model result = parameter;
    for (ModelIterator outputIter = invoke.outputIter(); outputIter.hasNext(); ) {
      partDef = (PartType) outputIter.next();
      value = partDef.getValue();
      if ((value != null) && (value.length() > 0)) {
        throw new IllegalArgumentException("value is not allowed for output definitions");
      }
      else if (partDef.getDataSource() != null) {
        throw new IllegalArgumentException("datasource is not allowed for output definitions");
      }
      else {
        outParam = service.portType().getOperation( opName ).fetchOutputPart( outputMsg, partDef.getID() );
        varName = partDef.getVariable();
        if (varName == null) {
          pathStr = partDef.getPath();
          if ((pathStr == null) || (pathStr.length() == 0)) {
            result = outParam;
          }
          else {
            path = new XPath( invoke.namespaceTable(), partDef.getPath());
            Model parent = path.getModel( result, ctrlGroup.getDialogController().getContext() );
            parent.add(outParam.type().name(), outParam);
          }
        }
        else {
          variables.put(varName, outParam);
        }
      }
    }
    return result;
  }

  /**
   * Create the container and add the input elements.
   * @param ctrlGroup
   * @param service
   * @param opName
   * @param invoke
   * @param variables
   * @return
   * @throws IOException
   */
  private static Model buildServiceInput( ControllerGroup ctrlGroup, Service service, QName opName, InvokeType invoke, Hashtable variables)
  throws IOException, ServiceException {
    Model message;
    PartType partDef;
    Model model;
    Operation op = service.portType().getOperation( invoke.getOperation() );
    message = op.getInputContainer();

    for (ModelIterator inputIter = invoke.inputIter(); inputIter.hasNext(); ) {
      partDef = (PartType) inputIter.next();
      model = fetchModel(partDef, ctrlGroup, variables);
      if (model != null) {
        op.addInputPart( message, partDef.getID(), model.clone(null, true, false ) );
      }
    }
    return message;
  }


}
