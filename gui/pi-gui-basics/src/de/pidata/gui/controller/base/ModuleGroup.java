/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.binding.Binding;
import de.pidata.models.binding.ModelBinding;
import de.pidata.models.tree.EventListener;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.service.base.ParameterList;

/**
 * A group of controllers created form a module definition.
 *
 * A ModuleGroup can be loaded into a ModuleController.
 */
public class ModuleGroup extends AbstractControllerGroup {

  private boolean readOnly;
  private boolean moduleReady = false;
  protected ParameterList parameterList;

  public void init( QName name, ModuleController parent, ModelBinding groupBinding, boolean readOnly, ParameterList parameterList ) {
    this.readOnly = readOnly;
    this.parameterList = parameterList;
    init( name, parent, groupBinding );
  }

  @Override
  protected void init( QName name, ControllerGroup parentGroup, ModelBinding groupBinding ) {
    super.init( name, parentGroup, groupBinding );
  }

  public boolean isReadOnly() {
    return readOnly;
  }

  public synchronized void activate( UIContainer uiContainer ) {
    this.uiContainer = uiContainer;
    ViewPI groupView = getView();
    if ((groupView != null) && (groupView.getComponentID() != null)) {
      groupView.attachUIComponent( uiContainer );
    }
    if (groupBinding != null) {
      groupBinding.setBindingListener( this );
    }
    this.active = true;

    if (moduleReady) {
      activateModule( uiContainer );
    }
  }

  /**
   * Called by ModuleViewPI after ModuleGroup's UI Fragment has been loaded
   * @param moduleContainer
   */
  public synchronized void activateModule( UIContainer moduleContainer ) {
    Logger.info( "Activating moduleID="+getName() + " in " + getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) +", active=" + active );
    moduleReady = true;
    if (this.active) {
      Platform.getInstance().updateLanguage( moduleContainer, getName(), null );
      for (int i = 0; i < controllerOrder.size(); i++) {
        Controller ctrl = controllerOrder.get( i );
        if (!ctrl.isActive()) {
          ctrl.activate( moduleContainer );
        }
      }
    }
  }

  /**
   * Called to deactivate this controller. It now should stop
   * listening on its binding and detach from its UI.
   *
   * @param saveValue if true controller should save its value
   *                  if false controller should abort any value changes
   */
  @Override
  public void deactivate( boolean saveValue ) {
    super.deactivate( saveValue );
    deactivateModule();
  }

  public synchronized void deactivateModule() {
    Logger.info( "Deactivating moduleID="+getName() + " in " + getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) +", active=" + active );
    moduleReady = false;
    for (int i = 0; i < controllerOrder.size(); i++) {
      Controller ctrl = controllerOrder.get( i );
      if (ctrl.isActive()) {
        ctrl.deactivate( true );
      }
    }
  }

  @Override
  public void setModel( Model model) {
    Model oldModel = this.model;
    if (model != oldModel) {
      this.model = model;
      //--- Small difference to parent implementation: Module must also be ready.
      if (this.active && this.moduleReady) {
        for (int i = 0; i < controllerOrder.size(); i++) {
          Controller ctrl = controllerOrder.get( i );
          ctrl.activate( uiContainer );
        }
      }
      eventSender.fireEvent( EventListener.ID_MODEL_DATA_CHANGED, this, null, oldModel, model );
    }
  }

  public UIContainer getUIContainer() {
    return this.uiContainer;
  }
}
