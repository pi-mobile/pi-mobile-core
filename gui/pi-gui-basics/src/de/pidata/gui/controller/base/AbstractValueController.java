/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.binding.Binding;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelSource;
import de.pidata.models.tree.ValidationException;
import de.pidata.qnames.QName;

public abstract class AbstractValueController extends AbstractController implements ValueController {

  private   boolean readOnly = false;
  protected Binding valueBinding = null;
  protected Binding labelBinding  = null;
  protected Binding visibilityBinding  = null;

  public void init( QName id, Controller parent, Binding valueBinding, boolean readOnly ) {
    super.init( id, parent );
    this.readOnly = readOnly;
    this.valueBinding = valueBinding;
  }

  @Override
  public void initValueBinding( Binding valueBinding ) {
    if (isActive()) {
      throw new IllegalStateException( "cannot change valueBinding in active state" );
    }
    if (this.valueBinding != null) {
      throw new IllegalArgumentException( "Must not redifine valeuBinding" );
    }
    this.valueBinding = valueBinding;
  }

  @Override
  public void initLabelBinding( Binding labelBinding ) {
    if (isActive()) {
      throw new IllegalStateException( "cannot change labelBinding in active state" );
    }
    this.labelBinding = labelBinding;
  }

  @Override
  public void initVisibilityBinding( Binding visibilityBinding ) {
    if (isActive()) {
      throw new IllegalStateException( "cannot change visibilityBinding in active state" );
    }
    this.visibilityBinding = visibilityBinding;
  }

  /**
   * Called to activate this controller. Now the controller can
   * attach itself to the UI and then start listening on its binding.
   * @param uiContainer the dialog context for this controller
   *
   */
  @Override
  public void activate( UIContainer uiContainer ) {
    if (valueBinding != null) {
      valueBinding.refresh();
    }
    super.activate( uiContainer );
    active = false;
    if (valueBinding != null) {
      valueBinding.setBindingListener( this );
    }
    if (labelBinding != null) {
      labelBinding.setBindingListener( this );
    }
    if (visibilityBinding != null) {
      visibilityBinding.setBindingListener( this );
    }
    active = true;
  }

  /**
   * Called to deactivate this controller. It now should stop
   * listening on its binding and detach from its UI.
   *
   * @param saveValue if true controller should save its value
   *           if false controller should abort any value changes
   */
  @Override
  public void deactivate( boolean saveValue ) {
    if (saveValue) {
      saveValue();
    }
    if (this.valueBinding != null) {
      valueBinding.setBindingListener( null );
    }
    if (labelBinding != null) {
      labelBinding.setBindingListener( null );
    }
    if (visibilityBinding != null) {
      visibilityBinding.setBindingListener( null );
    }
    super.deactivate( saveValue );
  }

  @Override
  public void setReadOnly(boolean readOnly) {
    this.readOnly = readOnly;
    viewSetEnabled();
  }

  @Override
  public boolean isReadOnly() {
    if (this.readOnly) {
      return this.readOnly;
    }
    else if (valueBinding != null) {
      return valueBinding.isReadOnly();
    }
    else {
      return false;
    }
  }

  @Override
  public boolean isFocusable() {
    return !isReadOnly();
  }

  @Override
  public Binding getValueBinding() {
    return valueBinding;
  }

  @Override
  public void valueChanged( Binding source, Object newValue ) {
    viewSetEnabled();
    if (source == valueBinding) {
      viewSetValue( newValue );
    }
    else if (source == labelBinding) {
      // TODO labelBinding
    }
    else if (source == visibilityBinding) {
      // TODO visibilityBinding
    }
  }

  @Override
  public void modelChanged( Binding source, Model newModel ) {
    viewSetEnabled();
  }

  @Override
  public void modelSourceChanged( Binding source, ModelSource modelSource ) {
    if (source == valueBinding) {
      ViewPI viewPI = getView();
      if (viewPI != null) {
        UIAdapter uiAdapter = viewPI.getUIAdapter();
        if (uiAdapter != null) {
          if (uiAdapter.getUIContainer().getDataContext() != modelSource.getModel() ) {
            throw new RuntimeException( "Internal error: modelSource's Model is not UIContainer's Model" );
          }
        }
      }
    }
  }

  @Override
  public void setValue( Object value ) {
    if (valueBinding == null) {
      viewSetValue( value );
    }
    else {
      valueBinding.storeModelValue( value );
      valueChanged( valueBinding, value );
    }
  }


  protected void viewSetEnabled() {
    ViewPI viewPI = getView();
    if (viewPI != null) {
      viewPI.setEnabled( !isReadOnly() );
    }
  }

  protected abstract void viewSetValue( Object value );

  /**
   * Callend whenever a validation error occurs
   * @param vex
   */
  protected abstract void setValidationError( ValidationException vex );

  /**
   * Called by binding to tell this BindingListener to store its
   * value in the binding.
   * Note: This happens e.g. before a dialog executes a operation to
   * make sure all dialog values are available to that operation.
   */
  @Override
  public void saveValue() {
    if ((valueBinding != null) && isActive() && (!isReadOnly())) {
      // We must not save if we are inactive to avoid overwriting values while switching context
      // when reusing a controller e.g. as part of a reused ModuleGroup.
      try {
        valueBinding.storeModelValue( getValue() );
        setValidationError( null );
      }
      catch (ValidationException vex) {
        setValidationError( vex );
      }
      catch (Exception ex) {
        Logger.error( "Could not store model value", ex );
      }
    }
  }

  /**
   * This method is called when this controller's dialog is closing.
   * The controller has to realease all its resources, e.g. remove
   * itself as istener.
   */
  @Override
  public void closing() {
    if (valueBinding != null) {
      this.valueBinding.disconnect();
      this.valueBinding = null;
    }
    super.closing();
  }

  @Override
  public void onFocusChanged( QName viewID, boolean hasFocus ) {
    super.onFocusChanged( viewID, hasFocus );
    if (!hasFocus) {
      saveValue();
    }
  }
}
