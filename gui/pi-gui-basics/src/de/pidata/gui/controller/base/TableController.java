/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.guidef.InplaceEdit;
import de.pidata.gui.view.base.TableViewPI;
import de.pidata.models.binding.Binding;
import de.pidata.models.binding.Selection;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

public interface TableController extends SelectionController {

  void init( QName id, ControllerGroup parent, TableViewPI tableViewPI, Binding binding,
             Selection selection, boolean readOnly, boolean selectable, InplaceEdit inplaceEdit, GuiOperation rowAction );

  /**
   * Returns this Table's view. Result is same like getListView() but
   * avoid's casting.
   *
   * @return this Table's view
   */
  TableViewPI getTableView();

  /**
   * Returns true if this table allows (inplace) editing
   * @return
   */
  boolean isEditable();

  /**
   * Returns true if this table allows selecting row(s)
   * @return
   */
  boolean isSelectable();

  /**
   * Notifies this table that editing has been started on given cell
   *
   * @param editRow     data row on which editing has been started
   * @param columnInfo  column in which editing has been started
   */
  void onStartEdit( Model editRow, ColumnInfo columnInfo );

  /**
   * Notifies this table that editing has stopped on given cell. Table now
   * has to store the newValue in the editedRow.
   *
   * @param editedRow   data row on which editing has been stopped
   * @param columnInfo  column in which editing has been stopped
   * @param newValue    new value for the given cell
   */
  void onStopEdit( Model editedRow, ColumnInfo columnInfo, Object newValue );

  /**
   * Returns number of columns this table has
   * @return number of columns
   */
  int columnCount();

  /**
   * Returns column at index. This is the order of definition and my differ from the
   * visible order.
   * @param index index of column to be returned
   * @return the column at index
   */
  ColumnInfo getColumn( int index );

  /**
   * Retuns column having header component with ID headerID
   * @param headerID header component ID of column to be returned
   * @return the column or null
   */
  ColumnInfo getColumnByHeaderID( QName headerID );

  /**
   * Returns column with name columnName
   * @param colName name of the column to be returned
   * @return the column or null
   */
  ColumnInfo getColumnByName( QName colName );

  /**
   * Retuns column having body render component with ID renderCompID
   * @param renderCompID body render component ID of column to be returned
   * @return the column
   */
  ColumnInfo getColumnByBodyCompID( QName renderCompID );

  /**
   * Add a column to this table
   * @param column column to be added
   */
  void addColumn( ColumnInfo column );

  /**
   * Notifies this table controller of a cell selection by the user. The implementation should
   * not try to modify table's selection. This is done by UI itself.
   * @param displayRow           the row to which the selected cell belongs
   * @param columnInfo           the column tn which the selected cell belongs
   */
  void onSelectedCell( Model displayRow, ColumnInfo columnInfo );

  /**
   * Notifies this table controller of a double cllick on a cell by the user. The implementation should
   * not try to modify table's selection. This is done by UI itself.
   * @param displayRow           the row to which the selected cell belongs
   * @param columnInfo           the column tn which the selected cell belongs
   */
  void onDoubleClickCell( Model displayRow, ColumnInfo columnInfo );

  /**
   * Notifies this table controller of a right cllick on a cell by the user. The implementation should
   * not try to modify table's selection. This is done by UI itself.
   * @param displayRow           the row to which the selected cell belongs
   * @param columnInfo           the column tn which the selected cell belongs
   */
  void onRightClickCell( Model displayRow, ColumnInfo columnInfo );

  /**
   * Sets delegate for this table
   *
   * @param tableDelegate new delegate or null to clear
   */
  void setTableDelegate( TableDelegate tableDelegate );

  /**
   * Called by TableView if row filtering has been activated/deactivated
   *
   * @param filterActive true if row filtering is active
   */
  void onFiltered( boolean filterActive );


  /**
   * Sets the GuiOperation which will be executed by a double click on a cell
   * @param rowDoubleClickAction
   */
  void setRowDoubleClickAction( GuiOperation rowDoubleClickAction );


  /**
   * Sets the GuiOperation which will be executed by a right click on a cell
   * @param rowDoubleClickAction
   */
  void setRowMenuAction( GuiOperation rowDoubleClickAction );
}
