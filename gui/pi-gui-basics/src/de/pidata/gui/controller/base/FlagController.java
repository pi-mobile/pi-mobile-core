/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.renderer.BooleanRenderer;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.FlagViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.binding.Binding;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ValidationException;
import de.pidata.qnames.QName;

import java.util.LinkedList;
import java.util.List;

public class FlagController extends AbstractValueController {

  private FlagViewPI flagViewPI;
  private List<FlagListener> flagListenerList;

  public void init( QName id, Controller parent, Binding valueBinding, FlagViewPI flagViewPI, String format, boolean readOnly ) {
    super.init( id, parent, valueBinding, readOnly );
    this.flagViewPI = flagViewPI;
    flagViewPI.setController( this );
    flagViewPI.setFormat( format );
  }

  public void init( QName id, Controller parent, Binding valueBinding, Binding labelBinding, FlagViewPI flagViewPI, String format, boolean readOnly ) {
    super.init( id, parent, valueBinding, readOnly );
    this.flagViewPI = flagViewPI;
    flagViewPI.setController( this );
    flagViewPI.setFormat( format );
    initLabelBinding( labelBinding );
  }

  /**
   * Called to activate this controller. Now the controller can
   * attach itself to the UI and then start listening on its binding.
   *  @param uiContainer the dialog context for this controller
   *
   */
  @Override
  public void activate( UIContainer uiContainer ) { // TODO: remove - super does the same
    super.activate( uiContainer );
    if (this.labelBinding != null) {
      this.labelBinding.setBindingListener( this );
    }
  }

  @Override
  public void valueChanged( Binding source, Object newValue ) {
    if (source == labelBinding) {
      flagViewPI.updateLabel( newValue );
    }
    else {
      super.valueChanged( source, newValue );
    }
  }

  public Binding getLabelBinding() {
    return this.labelBinding;
  }

  public Binding getVisibilityBinding() {
    return this.visibilityBinding;
  }

  public void initLabelBinding( Binding labelBinding ) {
    super.initLabelBinding( labelBinding );
    if (this.labelBinding != null) {
      this.labelBinding.setBindingListener( this );
    }
  }

  public ViewPI getView() {
    return flagViewPI;
  }

  public Object getValue() {
    return flagViewPI.getValue();
  }

  public Boolean getBooleanValue() {
    return (Boolean) getValue();
  }

  public boolean getBoolValue() {
    Boolean value = getBooleanValue();
    if (value == null) {
      return false;
    }
    else {
      return value.booleanValue();
    }
  }

  /**
   * Processing of value NULL:
   * <ul>
   *   <li>RadioButton: evaluates to false</li>
   *   <li>ToggleButton: evaluates to false</li>
   *   <li>CheckBox tristate: evaluates to third state; may lead to unexpected results if UI tristate was not enabled</li>
   * </ul>
   * The correspondence of UI tristate and controller tristate is set at creation time if the UI platform does support it.
   * @param value
   */
  @Override
  protected void viewSetValue( Object value ) {
    if (flagViewPI != null) {
      flagViewPI.updateValue( (Boolean) value );
    }
  }

  protected void setValidationError( ValidationException vex ) {
    String errorMsg = null;
    if (vex != null) {
      Logger.warn( "Validation exception in "+getClass().getName()+", vex="+vex.getMessage() );
      errorMsg = vex.getMessage();
    }
    // TODO this.checkbox.setInfoText( ViewPI.INFO_ERROR, errorMsg );
  }

  private void storeValue( Object checkedValue, Model dataContext ) {
    if (valueBinding != null) {
      try {
        valueBinding.storeModelValue( checkedValue, dataContext );
      }
      catch (ValidationException vex) {
        setValidationError( vex );
      }
      catch (Exception ex) {
        Logger.error( "Could not store model value", ex );
      }
    }
  }

  public void onCheckChanged( Boolean checkedValue, Model dataContext ) {
    storeValue( checkedValue, dataContext );
    fireFlagChanged( checkedValue );
  }

  public void addListener( FlagListener listener)  {
    if (this.flagListenerList == null) {
      this.flagListenerList = new LinkedList<FlagListener>();
    }
    this.flagListenerList.add( listener );
  }

  public void removeListener(FlagListener listener) {
    if (this.flagListenerList != null) {
      this.flagListenerList.remove( listener );
    }
  }

  protected void fireFlagChanged( Boolean newValue ) {
    if (flagListenerList != null) {
      // Copying listeners prevents problems if listeners are added or removed while send event loop
      Object[] listenerArr = new Object[flagListenerList.size()];
      flagListenerList.toArray( listenerArr );
      for (int i = 0; i < listenerArr.length; i++) {
        FlagListener listener = (FlagListener) listenerArr[i];
        try {
          listener.flagChanged( this, newValue );
        }
        catch (Exception ex) {
          Logger.error( "Error while event processing", ex );
        }
      }
    }
  }

  /**
   * Creates a renderer matching this controller
   *
   *
   * @param value@return a renderer matching this controller
   */
  @Override
  public String render( Object value ) {
    return BooleanRenderer.get( renderFormat ).render( value );
  }

  /**
   * Return GuiOperation with given guiOpID or null if not found.
   *
   * @param guiOpID GuiOperation having eventID==guiOpID or null
   * @return
   */
  @Override
  public GuiOperation getGuiOperation( QName guiOpID ) {
    return null; // this value controller has no GuiOperations
  }
}
