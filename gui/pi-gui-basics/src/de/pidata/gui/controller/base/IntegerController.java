/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.guidef.InputMode;
import de.pidata.gui.view.base.TextViewPI;
import de.pidata.models.binding.Binding;
import de.pidata.models.types.simple.IntegerType;
import de.pidata.qnames.QName;

public class IntegerController extends TextController {

  @Override
  public void init( QName id, Controller parent, TextViewPI textViewPI, boolean readOnly ) {
    super.init( id, parent, textViewPI, IntegerType.getDefInt(), InputMode.digit, null, readOnly );
  }

  @Override
  public void init( QName id, Controller parent, TextViewPI textViewPI, Binding valueBinding, boolean readOnly ) {
    if (valueBinding == null) {
      this.init( id, parent, textViewPI, readOnly );
    }
    else {
      this.init(id, parent, textViewPI, valueBinding, InputMode.digit, null, readOnly);
    }
  }

  public Object getValue() {
    String text = viewGetText();
    if (text == null || text.length() == 0) {
      return null;
    }
    else {
      return Integer.valueOf( text );
    }
  }

  public Integer getIntegerValue() {
    return (Integer) getValue();
  }

  public Short getShortValue() {
    String text = viewGetText();
    if (text == null || text.length() == 0) {
      return null;
    }
    else {
      return Short.valueOf( text );
    }
  }
}
