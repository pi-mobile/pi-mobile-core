/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.guidef.GuiService;
import de.pidata.models.tree.CombinedKey;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.models.tree.SimpleKey;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

/**
 * Created by pru on 28.04.16.
 */
public class ModelParameterList extends AbstractParameterList {

  public static final QName PARAM_TYPENAME = GuiService.NAMESPACE.getQName( "TypeName" );
  public static final QName PARAM_KEYCLASS = GuiService.NAMESPACE.getQName( "KeyClass" );
  public static final QName PARAM_KEYVALUE = GuiService.NAMESPACE.getQName( "KeyValue" );
  public static final QName PARAM_NSTABLE = GuiService.NAMESPACE.getQName( "NsTable" );

  public ModelParameterList() {
    super( ParameterType.QNameType, PARAM_TYPENAME, ParameterType.StringType, PARAM_KEYCLASS, ParameterType.StringType, PARAM_KEYVALUE );
  }

  public ModelParameterList( Model model ) {
    this();
    Key key = model.key();
    if (key == null) {
      throw new IllegalArgumentException( "Cannot use model as parameter: model's key is null" );
    }
    setQName( PARAM_TYPENAME, model.type().name() );
    setString( PARAM_KEYCLASS, key.getClass().getName() );
    setString( PARAM_KEYVALUE, key.toKeyString( model.namespaceTable() ) );
    setNamespaceTable( PARAM_NSTABLE, model.namespaceTable() );
  }

  public Key getKey() {
    Key key;
    NamespaceTable namespaceTable = getNamespaceTable( PARAM_NSTABLE );
    QName modelTypeName = getQName( PARAM_TYPENAME );
    ComplexType modelType = (ComplexType) ModelFactoryTable.getInstance().getFactory( modelTypeName.getNamespace() ).getType( modelTypeName );
    String keyClassName = getString( PARAM_KEYCLASS );
    String keyString = getString( PARAM_KEYVALUE );
    if (keyClassName.equals( QName.class.getName() )) {
      key = QName.fromKeyString( keyString, namespaceTable );
    }
    else if (keyClassName.equals( SimpleKey.class.getName() )) {
      key = SimpleKey.fromKeyString( modelType.getKeyAttributeType( 0 ), keyString, null );
    }
    else {
      key = CombinedKey.fromKeyString( modelType, keyString, null );
    }
    return key;
  }
}
