/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.view.base.NodeViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.tree.ValidationException;
import de.pidata.qnames.QName;

public class NodeController extends AbstractValueController {

  private NodeViewPI nodeViewPI;

  public void init( QName id, ControllerGroup ctrlGroup, NodeViewPI nodeViewPI ) {
    this.nodeViewPI = nodeViewPI;
    nodeViewPI.setController( this );

    super.init( id, ctrlGroup, valueBinding, true );
  }

  /**
   * Return GuiOperation with given guiOpID or null if not found.
   *
   * @param guiOpID GuiOperation having eventID==guiOpID or null
   * @return
   */
  @Override
  public GuiOperation getGuiOperation( QName guiOpID ) {
    return null; // this value controller has no GuiOperations
  }

  /**
   * Callend whenever a validation error occurs
   *
   * @param vex
   */
  @Override
  protected void setValidationError( ValidationException vex ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns this controller's view  or null if controller has no view
   *
   * @return this controller's view or null
   */
  @Override
  public ViewPI getView() {
    return nodeViewPI;
  }

  @Override
  public Object getValue() {
    return null;
  }

  @Override
  public void viewSetValue( Object value ) {
    // do nothing
  }
}
