/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.component.base.CharBuffer;
import de.pidata.models.types.Type;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.QName;

public class SapIdController extends TextController {

  public void viewSetValue( Object value ) {
    String strValue = null;
    if (value == null) {
      viewSetText( null );
    }
    else {
      // remove leading Zeros
      if (value instanceof QName) {
        strValue = ((QName) value).getName();
      }
      else {
        strValue = value.toString();
        int pos = strValue.lastIndexOf( ':' );
        if (pos > 0) {
          strValue = strValue.substring( pos+1 );
        }
      }
      strValue = StringType.removeLeadingZeros( strValue );
      viewSetText( strValue );
    }
  }

  public Object getValue() {
    String text = viewGetText();
    CharBuffer buffer = new CharBuffer( null );  //TODO hier wird sinnlos Speicher verbraten
    // add leading Zeros
    int length;
    Type valueType = getValueType();
    if (valueType != null) {
      length = ((StringType) valueType).getMaxLength();
    }
    else {
      length = buffer.length();
    }
    if ((valueType != null) && (valueType instanceof QNameType)) {
      buffer.setValue( getNamespace(), text );
      return buffer.toQNameFill(length, '0', true);
    }
    else {
      buffer.setValue( null, text ); // TODO unschön
      return buffer.toStringFill(length, '0', true);
    }
  }
}
