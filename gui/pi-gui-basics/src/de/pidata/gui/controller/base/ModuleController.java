/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ModuleViewPI;
import de.pidata.gui.view.base.ViewAnimation;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.binding.Binding;
import de.pidata.models.binding.ModelBinding;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.system.base.SystemManager;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

/**
 * A ModuleController allows loading and replacing a module (ModuleGroup)
 */
public class ModuleController extends AbstractControllerGroup {

  private boolean readOnly;
  private QName moduleID;
  private ModuleViewPI moduleViewPI;
  private List<ModuleGroup> moduleList = new LinkedList<ModuleGroup>();

  public void init( QName name, ControllerGroup parent, ModuleViewPI moduleViewPI, ModelBinding binding, QName moduleID, boolean readOnly ) {
    this.moduleViewPI = moduleViewPI;
    this.readOnly = readOnly;
    this.moduleID = moduleID;
    moduleViewPI.setController( this );
    super.init( name, parent, binding );
  }

  @Override
  public void activate( UIContainer uiContainer ) {
    super.activate( uiContainer );
    // Take the UIFragmentAdapter created while activate() as our UIContainer
    this.uiContainer = moduleViewPI.getUIContainer();
  }

  public QName getModuleID() {
    ModuleGroup moduleGroup = getModuleGroup();
    if (moduleGroup == null) {
      return moduleID;
    }
    else {
      return moduleGroup.getName();
    }
  }

  /**
   * Returns this controller's view  or null if controller has no view
   *
   * @return this controller's view or null
   */
  @Override
  public ViewPI getView() {
    return moduleViewPI;
  }

  @Override
  public void addController( QName controllerID, Controller controller ) {
    if (controllerCount() == 0) {
      if (controller instanceof ModuleGroup) {
        // We must not call super.addController() to avoid problems with method call order
        if (active) {
          // Following must be done before adding child controllers: Child controllers must
          // not be activated before module has been loaded
          controller.activate( uiContainer );
        }
        if (scope.get( controllerID ) != null) {
          throw new IllegalArgumentException("Controller or Variable already exists, name="+controllerID);
        }
        scope.add( controller );
        controllerOrder.add( controller );
        moduleViewPI.moduleChanged( (ModuleGroup) controller );
      }
      else {
        throw new IllegalArgumentException( "ModuleController's child must be instanceOf ModuleGroup" );
      }
    }
    else {
      throw new IllegalArgumentException( "ModuleController already has a child" );
    }
  }

  public ModuleGroup getModuleGroup() {
    if (controllerCount() == 0) {
      return null;
    }
    else {
      return (ModuleGroup) getController( 0 );
    }
  }

  /**
   * Replaces current ModuleGroup or adds it none existing
   *
   * @param newModuleGroup the new ModuleGroup or null to remove current ModuleGroup
   * @param viewAnimation
   */
  public void setModuleGroup( ModuleGroup newModuleGroup, ViewAnimation viewAnimation ) {
    while (controllerCount() > 0) {
      removeController( getController( 0 ) );
    }
    if (newModuleGroup == null) {
      moduleViewPI.moduleChanged( null );
    }
    else {
      addController( newModuleGroup.getName(), newModuleGroup );
    }
  }

  /**
   * Called by ModuleView after moduleGroup jhas been deactivated
   *
   * @param moduleGroup the moduleGroup which has been deactivated
   */
  public void moduleDeactivated( ModuleGroup moduleGroup) {
    // do nothing
  }

  public boolean isReadOnly() {
    return readOnly;
  }

  public void onFocusChanged( QName viewID, boolean hasFocus ) {
    //default: NOP
  }

  public void clearModuleList() {
    moduleList.clear();
  }

  public int moduleCount( ) {
    return moduleList.size();
  }

  public int getCurrentModuleIndex( ) {
    return moduleList.indexOf( getCurrentModule() );
  }

  public int getModuleCount( ) { return moduleList.size(); }

  public ModuleGroup getCurrentModule( ) {
    return getModuleGroup();
  }

  public void setCurrentModule( int index ) {
    setCurrentModule( moduleList.get( index ) );
  }

  /**
   * Add module to list of available modules, but does not activate it, so do not forget to
   * call setCurrentModule after all modules are added.
   *
   * @param appModule the module to add to list of available modules
   */
  public void addModule( ModuleGroup appModule ) {
    moduleList.add( appModule );
    // We must not set current module here: We might get two active modules because
    // Module gets active later on callback from loading UI.
  }

  public ModuleGroup getModule(int index) {
    return moduleList.get(index);
  }

  public boolean containsModule(ModuleGroup appModule) {
    return moduleList.contains( appModule );
  }

  public ModuleGroup getNextModule() {
    if(moduleList.size() == 0){
      return null;
    }
    ModuleGroup currentModule = getCurrentModule();
    int currentIndex = moduleList.indexOf( currentModule );
    if (currentIndex+1 < moduleList.size()){
      currentModule = moduleList.get( currentIndex+1 );
    }
    else{
      currentModule = moduleList.get( 0 );
    }
    return currentModule;
  }

  public ModuleGroup getPrevModule() {
    ModuleGroup currentModule = getCurrentModule();
    int currentIndex = moduleList.indexOf( currentModule );
    if(currentIndex == 0){
      currentModule =  moduleList.get( moduleList.size() -1 );
    }
    else{
      currentModule =  moduleList.get( currentIndex-1 );
    }
    return currentModule;
  }

  public void setCurrentModule( ModuleGroup selectModule ) {
    ModuleGroup currentModule = getCurrentModule();
    if (currentModule != null) {
      currentModule.deactivateModule();
    }

    if ((selectModule != null) && (!moduleList.contains( selectModule ))) {
      Logger.error("ModuleController.setCurrentModule: module '" + selectModule + "' is not in the list of modules." );
      selectModule = moduleList.get( 0 );
    }

    setModuleGroup( selectModule, ViewAnimation.NONE );
  }

  /**
   * Creates a renderer matching this controller
   *
   * @param value @return a renderer matching this controller
   */
  @Override
  public String render( Object value ) {
    StringBuilder builder = new StringBuilder();
    ModuleGroup currentModule = getCurrentModule();
    if (currentModule != null) {
      currentModule.setModel( (Model) value );
      for (Controller ctrl : currentModule.controllerOrder) {
        Object childValue = ctrl.getValue();
        if (ctrl instanceof ValueController) {
          Binding binding = ((ValueController) ctrl).getValueBinding();
          if (binding != null) {
            childValue = binding.fetchModelValue();
          }
        }
        builder.append( ctrl.render( childValue ) ).append( "\n" );
      }
    }
    else {
      builder.append( "NO MODULE" );
    }
    return builder.toString();
  }

  public void replaceModule( ModuleGroup oldModule, ModuleGroup newModule ) {
    int moduleIndex = moduleList.indexOf( oldModule );
    moduleList.remove( moduleIndex );
    moduleList.add( moduleIndex, newModule );
  }
}
