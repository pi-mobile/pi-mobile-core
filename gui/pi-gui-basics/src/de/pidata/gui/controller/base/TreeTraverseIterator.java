package de.pidata.gui.controller.base;

import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * Traverses the tree given by its root in pre-order.
 */
public class TreeTraverseIterator implements ModelIterator<Model> {


  private TreeNodePI next;

  private Iterator<TreeNodePI>  currentChildNodeIterator;
  private LinkedList<Iterator<TreeNodePI>> visitedIteratorStack = new LinkedList<>();

  public TreeTraverseIterator( TreeNodePI rootNode ) {
    next = rootNode;
    currentChildNodeIterator = next.childNodeIter().iterator();
  }

  /**
   * Returns true if there is a further Model in this iteration, i.e. the next call to next()
   * will return a Model not null.
   *
   * @return true if there is a further Model in this iteration
   */
  @Override
  public boolean hasNext() {
    return (next != null);
  }

  /**
   * Returns the next Model of this iterator and sets the internal pointer to the next Model.
   *
   * @return the next Model of this iteration
   * @throws IllegalArgumentException if there is no next Model in this iterator
   */
  @Override
  public Model next() {
    TreeNodePI current;
    if (next == null) {
      throw new NoSuchElementException( "No more Elements in iterator." );
    }
    current = next;
    findNext();
    return current.getNodeModel();
  }

  private void findNext() {
    if(currentChildNodeIterator.hasNext()) {
      next = currentChildNodeIterator.next();
      // descend if possible
      visitedIteratorStack.push( currentChildNodeIterator );
      currentChildNodeIterator = next.childNodeIter().iterator();
    }
    else {
      currentChildNodeIterator = visitedIteratorStack.pop();
      while (!currentChildNodeIterator.hasNext()) {
        if (visitedIteratorStack.isEmpty()) {
          currentChildNodeIterator = null;
          break;
        }
        currentChildNodeIterator = visitedIteratorStack.pop();
      }
      if (currentChildNodeIterator == null) {
        // got top
        next = null;
      }
      else {
        next = currentChildNodeIterator.next();
        // descend if possible
        visitedIteratorStack.push( currentChildNodeIterator );
        currentChildNodeIterator = next.childNodeIter().iterator();
      }
    }
  }

  public int memberCount() {
    int count = 0;
    while (hasNext()) {
      next();
      count++;
    }
    return count;
  }

  /**
   * Returns an iterator over elements of type {@code T}.
   *
   * @return an Iterator.
   */
  @Override
  public Iterator<Model> iterator() {
    return this;
  }
}
