package de.pidata.gui.controller.base;

public interface SettingsHandler {

  void loadSettings();

  void saveSettings();
}
