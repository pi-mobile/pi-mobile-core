/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ButtonViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.binding.Binding;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ValidationException;
import de.pidata.models.types.simple.BooleanType;
import de.pidata.qnames.QName;

public class ButtonController extends AbstractValueController implements ActionController {

  private ButtonViewPI buttonViewPI;
  private GuiOperation onClickOperation;

  private boolean enabled = true;
  private Binding iconBinding;

  public void init( QName id, ControllerGroup parent, ButtonViewPI buttonViewPI, Binding valueBinding, GuiOperation onClickOperation ) {
    super.init( id, parent, valueBinding, true );

    this.buttonViewPI = buttonViewPI;
    buttonViewPI.setController( this );

    this.onClickOperation = onClickOperation;
  }

  public void init( QName id, ControllerGroup parent, ButtonViewPI buttonViewPI, Binding valueBinding, Binding textBinding, Binding iconBinding, GuiOperation onClickOperation ) {
    super.init( id, parent, valueBinding, true );

    this.buttonViewPI = buttonViewPI;
    buttonViewPI.setController( this );
    initLabelBinding( textBinding );
    initIconBinding( iconBinding );

    this.onClickOperation = onClickOperation;
  }

  public void initIconBinding( Binding iconBinding ) {
    if (isActive()) {
      throw new IllegalStateException( "cannot change iconBinding in active state" );
    }
    this.iconBinding = iconBinding;
  }

  public ViewPI getView() {
    return buttonViewPI;
  }

  public Object getValue() {
    if (buttonViewPI != null) return buttonViewPI.getText();
    else return "";
  }

  @Override
  public void activate( UIContainer uiContainer ) {
    super.activate( uiContainer );
    if (iconBinding != null) {
      iconBinding.setBindingListener( this );
    }
  }

  @Override
  public void deactivate( boolean saveValue ) {
    if (iconBinding != null) {
      iconBinding.setBindingListener( null );
    }
    super.deactivate( saveValue );
  }

  public void viewSetValue( Object value ) {
    if (buttonViewPI != null) {
      buttonViewPI.updateValue( value );
    }
  }

  @Override
  public void valueChanged( Binding source, Object newValue ) {
    if (source == labelBinding) {
      buttonViewPI.updateText( newValue );
    }
    else if (source == iconBinding) {
      buttonViewPI.updateIcon( newValue );
    }
    super.valueChanged( source, newValue );
  }
  
  public GuiOperation getOnClickOperation() {
    return onClickOperation;
  }

  public boolean isFocusable() {
    return (enabled && (buttonViewPI != null));
  }

  public void setEnabled( boolean enabled ) { //TODO: abstract/default?
    this.enabled = enabled;
    if (buttonViewPI != null) {
      buttonViewPI.setState( (short) -1, (short) -1, null, null, BooleanType.valueOf( enabled ), null );
    }
  }

  protected void viewSetEnabled() {
    if (buttonViewPI != null) {
      buttonViewPI.setEnabled( enabled );
    }
  }

  public void setVisible( boolean visible ) { //TODO: abstract/default?
    if (buttonViewPI != null) {
      buttonViewPI.setState( (short) -1, (short) -1, null, null, null, BooleanType.valueOf( visible ) );
    }
  }

  public void press( Model dataContext ) {
    if (enabled) {
      // Fokuswechsel muss passieren, da sonst zuletzt aktives Feld nicht ins Model schreibt
      MutableControllerGroup ctrlGroup = getControllerGroup();
      if (onClickOperation != null) {
        ctrlGroup.getDialogController().subAction( this.onClickOperation, this.getName(), this, dataContext );
      }
    }
  }

  public void press( Model dataContext, ColumnInfo columnInfo ) {
    if (enabled) {
      // Fokuswechsel muss passieren, da sonst zuletzt aktives Feld nicht ins Model schreibt
      MutableControllerGroup ctrlGroup = getControllerGroup();
      if (onClickOperation != null) {
       ctrlGroup.getDialogController().subAction( this.onClickOperation, columnInfo.getColName(), this, dataContext );
      }
    }
  }

  /**
   * Callend whenever a validation error occurs
   *
   * @param vex
   */
  protected void setValidationError( ValidationException vex ) {
    // we are readOnly, so we do not need to do anything
  }

  @Override
  public void doAction() {
    // button has to get focus
    getView().setState( (short) 0, (short) 0, Boolean.TRUE, null, null, null );
    press( getControllerGroup().getModel() );
  }

  /**
   * Return GuiOperation with given guiOpID or null if not found.
   *
   * @param guiOpID GuiOperation having eventID==guiOpID or null
   * @return
   */
  @Override
  public GuiOperation getGuiOperation( QName guiOpID ) {
    if ((this.onClickOperation != null) && (this.onClickOperation.eventID == guiOpID)) {
      return this.onClickOperation;
    }
    else {
      return null;
    }
  }

  public Binding getVisibilityBinding() {
    return this.visibilityBinding;
  }
}
