/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.guidef.InputMode;
import de.pidata.gui.view.base.CodeEditorViewPI;
import de.pidata.gui.view.base.TextViewPI;
import de.pidata.models.binding.Binding;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.QName;

import java.util.List;

/**
 * @author dsc
 */
public class CodeEditorController extends TextController {

  public static final QName SEARCH_NEXT = GuiBuilder.NAMESPACE.getQName( "searchNext" );
  public static final QName SEARCH_NEXT_SHORTCUT = GuiBuilder.NAMESPACE.getQName( "searchNextShortcut" );
  public static final QName SEARCH_PREV = GuiBuilder.NAMESPACE.getQName( "searchPrev" );
  public static final QName SEARCH_PREV_SHORTCUT = GuiBuilder.NAMESPACE.getQName( "searchPrevShortcut" );
  public static final QName REPLACE_NEXT = GuiBuilder.NAMESPACE.getQName( "replaceNext" );
  public static final QName REPLACE_ALL = GuiBuilder.NAMESPACE.getQName( "replaceAll" );
  public static final QName ACTIVATE_SEARCH = GuiBuilder.NAMESPACE.getQName( "activateSearchText" );
  public static final QName CLEAR_SEARCH = GuiBuilder.NAMESPACE.getQName( "clearSearchText" );
  public static final QName ACTIVATE_REPLACE = GuiBuilder.NAMESPACE.getQName( "activateReplaceText" );

  protected CodeEditorViewPI codeEditorViewPI;

  @Override
  public void init( QName id, Controller parent, TextViewPI textViewPI, boolean readOnly ) {
    codeEditorViewPI = (CodeEditorViewPI) textViewPI;
    super.init( id, parent, textViewPI, StringType.getDefString(), InputMode.string, null, readOnly );
  }

  @Override
  public void init( QName id, Controller parent, TextViewPI textViewPI, Binding valueBinding, boolean readOnly ) {
    if (valueBinding == null) {
      this.init( id, parent, textViewPI, readOnly );
    }
    else {
      codeEditorViewPI = (CodeEditorViewPI)textViewPI;
      this.init( id, parent, textViewPI, valueBinding, InputMode.string, null, readOnly );
    }
  }

  public void showLinenumbers( boolean show ) {
    if (codeEditorViewPI != null) {
      codeEditorViewPI.showLinenumbers( show );
    }
  }

  public void addLineMarker( String marker, List<Integer> markLinesList, ComponentColor markerColor ) {
    if (codeEditorViewPI != null) {
      codeEditorViewPI.addLineMarker( marker, markLinesList, markerColor );
    }
  }

  public void setAutoindent( boolean autoindent ) {
    if (codeEditorViewPI != null) {
      codeEditorViewPI.setAutoindent( autoindent );
    }
  }

  public void searchAll( String searchStr ) {
    if(codeEditorViewPI != null) {
      codeEditorViewPI.searchAll( searchStr );
    }
  }

  public void searchNext( String searchStr ) {
    if(codeEditorViewPI != null) {
      codeEditorViewPI.searchNext( searchStr );
    }
  }

  public void searchPrev( String searchStr ) {
    if(codeEditorViewPI != null) {
      codeEditorViewPI.searchPrev( searchStr );
    }
  }

  public void refreshLineMarkers() {
    if(codeEditorViewPI != null) {
      codeEditorViewPI.refreshLineMarkers();
    }
  }

  public void setSearchRegex( boolean searchRegex ) {
    if(codeEditorViewPI != null) {
      codeEditorViewPI.setSearchRegex( searchRegex );
    }
  }

  public void initListeners() {
    if(codeEditorViewPI != null) {
      codeEditorViewPI.initListeners();
    }
  }

  public void addSearchListener( SearchListener searchListener ) {
    if(codeEditorViewPI != null) {
      codeEditorViewPI.addSearchListener( searchListener );
    }
  }

  public void replaceNext( String replaceStr ) {
    if (codeEditorViewPI != null) {
      codeEditorViewPI.replaceNext( replaceStr );
    }
  }

  public void replaceAll( String replaceStr ) {
    if (codeEditorViewPI != null) {
      codeEditorViewPI.replaceAll( replaceStr );
    }
  }
}
