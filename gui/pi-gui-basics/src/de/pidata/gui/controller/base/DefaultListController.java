/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.guidef.ControllerFactory;
import de.pidata.gui.view.base.ListViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.binding.Binding;
import de.pidata.models.binding.Selection;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.tree.ValidationException;
import de.pidata.models.tree.XPath;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.ArrayList;
import java.util.List;

public class DefaultListController extends AbstractSelectionController implements ListController {

  protected ListViewPI listView;

  public void init( QName id, Controller parent, ListViewPI listView, Binding valueBinding,
                    Selection selection, QName keyAttr, XPath displayValuePath, QName displayValueID, boolean readOnly ) {
    this.listView = listView;
    listView.setController( this );
    listView.setDisplayValueID( displayValueID );

    this.keyAttr = keyAttr;
    this.displayValuePath = displayValuePath;

    super.init( id, parent, valueBinding, selection, null, readOnly );
  }

  /**
   * Returns the view used for selection
   *
   * @return the selection component
   */
  public ListViewPI getListView() {
    return listView;
  }

  public ViewPI getView() {
    return listView;
  }

  @Override
  public boolean hasFirstRowForEmptySelection() {
    return (selection.isEmptyAllowed() && !selection.isMultiSelect());
  }

  /**
   * Callend whenever a validation error occurs
   *
   * @param vex
   */
  protected void setValidationError( ValidationException vex ) {
    String errorMsg = null;
    if (vex != null) {
      Logger.warn( "Validation exception in " + getClass().getName() + ", vex=" + vex.getMessage() );
      errorMsg = vex.getMessage();
    }
    this.listView.setInfoText( ViewPI.INFO_ERROR, errorMsg );
  }

  @Override
  protected void viewUpdateRow( Model changedRow ) {
    listView.updateRow( changedRow );
  }

  @Override
  protected void viewRemoveRow( Model removedRow ) {
    listView.removeRow( removedRow );
  }

  @Override
  protected void viewInsertRow( Model newRow, Model beforeRow ) {
    listView.insertRow( newRow, beforeRow );
  }

  public void viewUpdateAllRows() {
    ignoreModelEvents = true;
    listView.removeAllDisplayRows();

    //--- set empty value if allowed
    if (hasFirstRowForEmptySelection()) {
      listView.insertRow( null, null );
    }

    //--- update all rows
    for (ModelIterator rowIter = selection.valueIter(); rowIter.hasNext(); ) {
      Model row = rowIter.next();
      listView.insertRow( row, null );
    }

    //--- Selection has been cleared on update. Now set selection.
    viewSetSelection();

    /* TODO Spezialfall textView als Liste
    else if (textComp != null) {
      Model selectedRow = selection.getSelectedValue();
      String value;
      if (selectedRow == null) {
        value = "";
      }
      else {
        value = displayString( getDisplayRow( selectedRow ) );
      }
      textComp.setValue( value );
    }*/
    ignoreModelEvents = false;
  }

  protected void viewSetSelection() {
    if (selection.isMultiSelect()) {
      for (int i = 0; i < selection.selectedValueCount(); i++) {
        listView.setSelected( selection.getSelectedValue( i ), true );
      }
    }
    else {
      listView.setSelected( selection.getSelectedValue( 0 ), true );
    }
  }
}
