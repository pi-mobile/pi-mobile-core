/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.guidef.NodeType;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.gui.view.base.TreeViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.binding.Binding;
import de.pidata.models.binding.ModelBinding;
import de.pidata.models.tree.*;
import de.pidata.qnames.QName;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pru on 12.07.2016.
 */
public class DefaultTreeController extends AbstractValueController implements TreeController, EventListener {

  protected TreeViewPI treeViewPI;
  protected NodeType defaultNodeType;
  protected Map<QName,NodeType> nodeTypes = new HashMap<QName,NodeType>();
  protected GuiOperation nodeAction;
  protected Model rootModel;
  protected Model selectedModel;
  protected EventSender eventSender = new EventSender( this );
  protected ModelBinding treeBinding;
  protected boolean lazyLoading;
  protected boolean showRootNode;

  public void init( QName id, ControllerGroup ctrlGroup, TreeViewPI treeViewPI, Binding valueBinding, ModelBinding treeBinding, GuiOperation nodeAction, boolean readOnly, boolean lazyLoading, boolean showRootNode ) {
    super.init( id, ctrlGroup, valueBinding, readOnly );

    this.treeViewPI = treeViewPI;
    treeViewPI.setController( this );
    this.nodeAction = nodeAction;

    this.treeBinding = treeBinding;
    treeBinding.setBindingListener( this );

    this.lazyLoading = lazyLoading;
    this.showRootNode = showRootNode;

  }

  @Override
  public void addNodeType( NodeType nodeType ) {
    if (nodeType.getModelTypeID() == null) {
      defaultNodeType = nodeType;
    }
    else {
      nodeTypes.put( nodeType.getModelTypeID(), nodeType );
    }
  }

  @Override
  public void updateChildList( TreeNodePI treeNodePI ) {
    Model model = treeNodePI.getNodeModel();
    // Remove current nodes
    removeChildNodes(treeNodePI);
    int index = 0;
    for (Model childModel : model.iterator( treeNodePI.getChildRelationID(), null )) {
      NodeType nodeTypeDef = getNodeType( childModel.type().name() );
      TreeNodePI childNode = treeNodePI.setChildModelAt( index, childModel, nodeTypeDef.getValueID(), nodeTypeDef.getIconValueID(), nodeTypeDef.getEditFlagID(), nodeTypeDef.getEnabledFlagID(), nodeTypeDef.getRelationID() );
      if (!lazyLoading) {
        updateChildList( childNode );
      }
      index++;
    }
  }

  private void removeChildNodes( TreeNodePI treeNodePI ) {
    for (TreeNodePI childNode : treeNodePI.childNodeIter()) {
      removeChildNodes( childNode );
      treeNodePI.removeChild( childNode );
    }
  }

  @Override
  public NodeType getNodeType( QName typeName ) {
    NodeType nodeTypeDef = nodeTypes.get( typeName );
    if (nodeTypeDef == null) {
      if (defaultNodeType == null) {
        defaultNodeType = new NodeType();
      }
      nodeTypeDef = defaultNodeType;
    }
    return nodeTypeDef;
  }

  @Override
  public TreeNodePI getRootNode() {
    return treeViewPI.getRootNode();
  }

  /**
   * Return GuiOperation with given guiOpID or null if not found.
   *
   * @param guiOpID GuiOperation having eventID==guiOpID or null
   * @return
   */
  @Override
  public GuiOperation getGuiOperation( QName guiOpID ) {
    if ((nodeAction != null ) && (nodeAction.eventID == guiOpID)) {
      return nodeAction;
    }
    else {
      return null;
    }
  }

  /**
   * Called to activate this controller. Now the controller can
   * attach itself to the UI and then start listening on its binding.
   *  @param uiContainer the dialog context for this controller
   *
   */
  @Override
  public void activate( UIContainer uiContainer ) {
    super.activate( uiContainer );
    fetchRootModel();
  }

  protected void viewSetEnabled() {
    // do not set enabled based on readOnly attribute
  }

  @Override
  public void modelChanged( Binding source, Model newModel ) {
    super.modelChanged( source, newModel );
    if (source == treeBinding) {
      fetchRootModel();
    }
  }

  private void fetchRootModel() {
    if (rootModel != null) {
      rootModel.removeListener( this );
    }

    this.rootModel = treeBinding.getModel();
    if (rootModel != null) {
      buildTree( this.rootModel );

      rootModel.addListener( this );
      setSelectedModel( rootModel );
    }
  }

  public void buildTree( Model rootNodeModel ) {
    NodeType nodeTypeDef = getNodeType( rootNodeModel.type().name() );
    treeViewPI.setRootModel( rootNodeModel, nodeTypeDef.getValueID(), nodeTypeDef.getIconValueID(), nodeTypeDef.getEditFlagID(), nodeTypeDef.getEnabledFlagID(), nodeTypeDef.getRelationID() );
    updateChildList( treeViewPI.getRootNode() );
  }

  /**
   * Callend whenever a validation error occurs
   *
   * @param vex
   */
  @Override
  protected void setValidationError( ValidationException vex ) {
    Logger.error( vex.getMessage(), vex );
  }

  /**
   * Returns this controller's view  or null if controller has no view
   *
   * @return this controller's view or null
   */
  @Override
  public ViewPI getView() {
    return treeViewPI;
  }

  @Override
  public Object getValue() {
    return rootModel;
  }

  @Override
  public void viewSetValue( Object value ) {
    if(value instanceof Model) {
      rootModel = (Model) value;
    }
  }

  /**
   * An event occurred.
   *
   * @param eventSender the sender of this event
   * @param eventID     the id of the event
   * @param source      the instance where the event occurred on, may be a child of eventSender
   * @param modelID     the id of the affected model
   * @param oldValue    the old value of the model
   * @param newValue    the new value of the model
   */
  @Override
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {
    if (source instanceof Model) {
      if (eventID == EventListener.ID_MODEL_DATA_ADDED) {
        // newValue is the inserted row and all following rows moved one down
        Model newModel = (Model) newValue;
        TreeNodePI treeNodePI = treeViewPI.getRootNode().findNode( (Model) source );
        if (treeNodePI != null) {
          updateChildList( treeNodePI );
        }
      }
      else if (eventID == EventListener.ID_MODEL_DATA_REMOVED) {
        // oldValue is the removed row (null means all rows), newValue is the next row after the deleted one
        Model removedNode = (Model) oldValue;
        TreeNodePI treeNodePI = treeViewPI.getRootNode().findNode( removedNode );
        if (treeNodePI != null) {
          removeChildNodes( treeNodePI );
          TreeNodePI parentNode = treeNodePI.getParentNode();
          if (parentNode == null) {
            treeNodePI.setNodeModel( null, null, null, null, null, null );
          }
          else {
            parentNode.removeChild( treeNodePI );
          }
        }
      }
      else if (eventID == EventListener.ID_MODEL_DATA_CHANGED) {
        // source is one of child nodes --> Child Node listens directly on node model
      }
      else {
        Logger.debug( "unknown event: " + eventID );
      }
    }
    else {
      Logger.debug( "got event on source: " + source );
    }
  }

  private void updateChildNode( TreeNodePI treeNodePI ) {
    treeViewPI.updateNode( treeNodePI );
    if (treeNodePI.refreshChildList()) {
      treeViewPI.updateChildList( treeNodePI );
    }
  }

  public void setSelectedModel( Model rootModel ) {
    treeViewPI.setSelectedModel( rootModel );
    selectedModel = rootModel;
  }

  @Override
  public TreeNodePI getSelectedNode() {
    if (treeViewPI == null) {
      // may happen during initialization
      return null;
    }
    else {
      return treeViewPI.getSelectedNode();
    }
  }

  @Override
  public void onSelectionChanged( TreeNodePI treeNode ) {
    Model oldRow = selectedModel;
    if (treeNode == null) {
      this.selectedModel = null;
    }
    else {
      this.selectedModel = treeNode.getNodeModel();
    }
    eventSender.fireEvent( EventListener.ID_MODEL_DATA_CHANGED, this, null, oldRow, selectedModel );
    if (nodeAction != null) {
      getDialogController().subAction( nodeAction, nodeAction.eventID, this, selectedModel );
    }
  }

  @Override
  public void editNode( TreeNodePI treeNode ) {
    treeViewPI.editNode( treeNode );
  }

  @Override
  public void onStopEdit( TreeNodePI editedNode, QName valueID, Object newValue ) {
    Model editedRow = editedNode.getNodeModel();
    editedRow.set( valueID, newValue );
  }

  /**
   * Returns the current data model of this data source
   *
   * @return the current data model
   */
  @Override
  public Model getModel() {
    // Do not use getSelectedNode() - UI may be in undefined state while firing selection changed event
    return this.selectedModel;
  }

  /**
   * Adds listener to this ModelSource's listener list
   *
   * @param listener the listener to be added
   */
  public void addListener(EventListener listener) {
    this.eventSender.addListener( listener );
  }

  /**
   * Removes listener from this ModelSource's listener list
   *
   * @param listener the listener to be removed
   */
  public void removeListener(EventListener listener) {
    this.eventSender.removeListener( listener );
  }

  public boolean showRootNode() {
    return showRootNode;
  }
}
