/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.controller.base;

import de.pidata.gui.view.base.TabPaneViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.binding.AttributeBinding;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ValidationException;
import de.pidata.models.types.simple.BooleanType;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.List;

public class TabPaneController extends AbstractValueController {

  private TabPaneViewPI tabPaneViewPI;
  private GuiOperation onClickOperation;

  private boolean enabled = true;
  private Namespace valueNS;

  public void init( QName id, ControllerGroup parent, TabPaneViewPI tabPaneViewPI) {
    super.init( id, parent, valueBinding, true );

    this.tabPaneViewPI = tabPaneViewPI;
    tabPaneViewPI.setController( this );

    this.onClickOperation = onClickOperation;
  }

  public ViewPI getView() {
    return tabPaneViewPI;
  }

  public Object getValue() {
    //if (tabPaneViewPI != null) return tabPaneViewPI.getLabel();
    return "";
  }

  public void viewSetValue( Object value ) {
    if (tabPaneViewPI != null) {
      throw new RuntimeException( "TODO" ); // was soll hier passieren?
    }
  }

  public GuiOperation getOnClickOperation() {
    return onClickOperation;
  }

  /**
   * Return GuiOperation with given guiOpID or null if not found.
   *
   * @param guiOpID GuiOperation having eventID==guiOpID or null
   * @return
   */
  @Override
  public GuiOperation getGuiOperation( QName guiOpID ) {
    if ((onClickOperation != null ) && (onClickOperation.eventID == guiOpID)) {
      return onClickOperation;
    }
    else {
      return null;
    }
  }

  public boolean isFocusable() {
    return (enabled && (tabPaneViewPI != null));
  }

  public void setEnabled(boolean enabled) { //TODO: abstract/default?
    this.enabled = enabled;
    if (tabPaneViewPI != null) {
      tabPaneViewPI.setState( (short) -1, (short) -1, null, null, BooleanType.valueOf( enabled ), null );
    }
  }

  protected void viewSetEnabled() {
    if (tabPaneViewPI != null) {
      tabPaneViewPI.setEnabled( enabled );
    }
  }

  public void setVisible(boolean visible) { //TODO: abstract/default?
    if (tabPaneViewPI != null) {
      tabPaneViewPI.setState( (short) -1, (short) -1, null, null, null, BooleanType.valueOf(visible) );
    }
  }

  public void press() {
    if (enabled) {
      // Fokuswechsel muss passieren, da sonst zuletzt aktives Feld nicht ins Model schreibt
      Model model = getControllerGroup().getModel();
      getDialogController().subAction( this.onClickOperation, this.getName(), this, model );
    }
  }

  /**
   * Callend whenever a validation error occurs
   *
   * @param vex
   */
  protected void setValidationError( ValidationException vex ) {
    // we are readOnly, so we do not need to do anything
  }

  public String getSelectedTab() {
    if (tabPaneViewPI != null) {
      return tabPaneViewPI.getSelectedTab();
    }
    return null;
  }

  public void setSelectedTab( String tabId ) {
    if (tabPaneViewPI != null) {
      tabPaneViewPI.setSelectedTab(tabId);
    }
  }

  public void setSelectedTab( int index ) {
    if (tabPaneViewPI != null) {
      tabPaneViewPI.setSelectedTab(index);
    }
  }

  public List<String> getTabs() {
    if (tabPaneViewPI != null) {
      return tabPaneViewPI.getTabs();
    }
    return null;
  }

  public void setTabDisabled( String tabId, boolean disabled ) {
    if (tabPaneViewPI != null) {
      tabPaneViewPI.setTabDisabled( tabId, disabled );
    }
  }

  public void tabChanged( String id ) {
    MutableControllerGroup ctrlGroup =  getControllerGroup();
    ctrlGroup.toString();
  }

  public void setTabText(String tabId, String text) {
    if (tabPaneViewPI != null) {
      tabPaneViewPI.setTabText(tabId, text);
    }
  }

  public void setValueNS( Namespace valueNS ) {
    this.valueNS = valueNS;
  }

  protected Namespace getValueNS() {
    if (valueNS == null) {
      if (valueBinding != null && valueBinding instanceof AttributeBinding) {
        valueNS = ((AttributeBinding) valueBinding).getAttributeName().getNamespace();
      }
      else {
        throw new RuntimeException( "no namespace given" );
      }
    }
    return valueNS;
  }
}
