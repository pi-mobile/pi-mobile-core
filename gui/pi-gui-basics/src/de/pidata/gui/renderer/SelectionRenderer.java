/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.renderer;

import de.pidata.models.binding.Selection;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.SimpleKey;
import de.pidata.models.tree.XPath;
import de.pidata.qnames.Key;
import de.pidata.qnames.QName;

public class SelectionRenderer implements Renderer {

  private Selection selection;
  private XPath           valuePath;
  private QName valueID;
  private Renderer        valueRenderer;

  public SelectionRenderer( Selection selection, XPath valuePath, QName valueID, Renderer valueRenderer ) {
    this.selection = selection;
    this.valuePath = valuePath;
    this.valueID = valueID;
    this.valueRenderer = valueRenderer;
  }

  /**
   * Tells this renderer to render value by using controller
   *
   * @param value      the value to be rendered
   * @return the rendered value
   * @throws IllegalArgumentException if controller is not a instance of SelectionController
   */
  public String render( Object value ) {
    Key key;
    if (value instanceof Key) {
      key = (Key) value;
    }
    else {
      key = new SimpleKey( value );
    }
    Model row = selection.getValue( key, valuePath );
    Object displayValue;
    if (row == null) {
      displayValue = null;
    }
    else {
      displayValue = row.get( valueID );
    }
    return valueRenderer.render( displayValue );
  }
}
