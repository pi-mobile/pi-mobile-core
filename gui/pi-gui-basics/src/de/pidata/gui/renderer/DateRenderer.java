/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.renderer;

import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.qnames.QName;

public class DateRenderer implements Renderer {

  private QName dateTimeType;

  public DateRenderer( QName dateTimeType ) {
    this.dateTimeType = dateTimeType;
  }

  /**
   * Tells this renderer to render value by using controller
   *
   * @param value the value to be rendered
   * @return the rendered value
   */
  public String render( Object value ) {
    if (value == null) {
      return "";
    }
    else if (value instanceof DateObject) {
      return ((DateObject) value).toDisplayString( DateObject.getCalendar(), dateTimeType, false );
    }
    else {
      return null;
    }
  }

  private static DateRenderer dateRenderer;
  private static DateRenderer timeRenderer;
  private static DateRenderer dateTimeRenderer;

  public static DateRenderer get( QName dateTimeType ) {
    if (dateTimeType == DateTimeType.TYPE_DATETIME) {
      if (dateTimeRenderer == null) {
        dateTimeRenderer = new DateRenderer( dateTimeType );
      }
      return dateTimeRenderer;
    }
    else if (dateTimeType == DateTimeType.TYPE_TIME) {
      if (timeRenderer == null) {
        timeRenderer = new DateRenderer( dateTimeType );
      }
      return timeRenderer;
    }
    else {
      if (dateRenderer == null) {
        dateRenderer = new DateRenderer( dateTimeType );
      }
      return dateRenderer;
    }
  }
}
