/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.renderer;

import de.pidata.gui.event.InputManager;
import de.pidata.gui.guidef.InputMode;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DecimalObject;
import de.pidata.models.types.simple.DurationObject;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.QName;
import de.pidata.string.Helper;

/**
 * Currently supported formatting:
 * <ul>
 *   <li>SAP number: remove leading zeroes</li>
 *   <li>restrict input length</li>
 * </ul>
 */
public class StringRenderer implements Renderer {

  private InputMode inputMode;
  private int inputLen;

  public StringRenderer() {
    this.inputMode = InputMode.string;
  }

  public StringRenderer( InputMode inputMode ) {
    this.inputMode = inputMode;
  }

  public StringRenderer( InputMode inputMode, int inputLen ) {
    this.inputMode = inputMode;
    this.inputLen = inputLen;
  }

  /**
   * Tells this renderer to render value by using controller
   *
   * @param value the value to be rendered
   * @return the rendered value
   */
  public String render( Object value ) {
    String strValue;
    if (value == null) {
      strValue = "";
    }
    else {
      if (value instanceof QName) {
        strValue = ((QName) value).getName();
      }
      else if (value instanceof DateObject) {
        QName dtFormat = ((DateObject) value).getType();
        strValue = ((DateObject) value).toDisplayString( DateObject.getCalendar(), dtFormat, false );
      }
      else if (value instanceof DurationObject) {
        strValue = ((DurationObject) value).toDisplayString();
      }
      else if (value instanceof DecimalObject) {
        DecimalObject decimal = (DecimalObject) value;
        strValue = decimal.toString( InputManager.DECIMAL_SEPARATOR, ((DecimalObject) value).getScale() );
      }
      else if (value instanceof Boolean) {
        boolean bool = ((Boolean) value).booleanValue();
        if (bool) strValue = "X";
        else strValue = "-";
      }
      else {
        strValue = value.toString();
      }
    }
    if (inputMode == InputMode.sapNumber) {
      strValue = StringType.removeLeadingZeros( strValue );
    }
    else if (inputLen > 0) {
      if ((strValue != null) && (strValue.length() > inputLen)) {
        strValue = strValue.substring( 0, inputLen );
      }
    }
    return strValue;
  }

  //-----------------------------------------------------------------

  private static StringRenderer defaultStringRenderer;
  private static StringRenderer sapNumberStringRenderer;

  public static StringRenderer getDefault() {
    if (defaultStringRenderer == null) {
      defaultStringRenderer = new StringRenderer();
    }
    return defaultStringRenderer;
  }

  public static StringRenderer getSAPNumber() {
    if (sapNumberStringRenderer == null) {
      sapNumberStringRenderer = new StringRenderer( InputMode.sapNumber );
    }
    return sapNumberStringRenderer;
  }

  public static Renderer get( InputMode inputMode ) {
    return get( inputMode, Integer.valueOf( -1 ) );
  }

  public static Renderer get( InputMode inputMode, Object format ) {
    if (inputMode == null) {
      return getDefault();
    }
    else if (inputMode == InputMode.sapNumber) {
      return getSAPNumber();
    }
    else {
      int inputLen = -1;
      if (!Helper.isNullOrEmpty( format )) {
        inputLen = Integer.parseInt( format.toString() );
      }
      return new StringRenderer( inputMode, inputLen );
    }
  }
}
