/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.renderer;

import de.pidata.models.types.simple.BooleanType;
import de.pidata.string.Helper;

/**
 * Currently supported formatting:
 * <ul>
 *   <li>alternative String for TRUE</li>
 * </ul>
 */
public class BooleanRenderer implements Renderer {

  private String trueValue;

  public BooleanRenderer( String trueValue ) {
    this.trueValue = trueValue;
  }

  private Boolean evaluate( Object value ) {
    if (value instanceof Boolean) {
      return (Boolean) value;
    }
    else if (value instanceof String) {
      String str = ((String) value).toUpperCase();
      if (str.equals( "TRUE" )) {
        return BooleanType.TRUE;
      }
      else if (str.equals( "X" )) {
        return BooleanType.TRUE;
      }
      else if (str.equals( "YES" )) {
        return BooleanType.TRUE;
      }
      else if (str.equals( "JA" )) {
        return BooleanType.TRUE;
      }
      else {
        return BooleanType.FALSE;
      }
    }
    else if (value instanceof Character) {
      char ch = ((Character) value).charValue();
      if ((ch == 'X') || (ch == 'J') || (ch == 'Y') || (ch == 'x') || (ch == 'j') || (ch == 'y')) {
        return BooleanType.TRUE;
      }
      else {
        return BooleanType.FALSE;
      }
    }
    else {
      return null;
    }
  }

  /**
   * Tells this renderer to render value by using controller
   *
   * @param value the value to be rendered
   * @return the rendered value
   */
  public String render( Object value ) {
    if (value == null) {
      return null;
    }
    else if (Helper.isNullOrEmpty( trueValue )) {
      Boolean val = evaluate( value );
      if (val == null) {
        return null; // TODO: render tristate?
      }
      else {
        return val.toString();
      }
    }
    else {
      Boolean bool = evaluate( value );
      if (bool == null) {
        return null; // TODO: render tristate?
      }
      else if (bool.booleanValue()) {
        return trueValue;
      }
      else {
        return "";
      }
    }
  }

  private static BooleanRenderer defaultRenderer;

  public static Renderer get( Object trueValue ) {
    if (Helper.isNullOrEmpty( trueValue )) {
      if (defaultRenderer == null) {
        defaultRenderer = new BooleanRenderer( null );
      }
      return defaultRenderer;
    }
    return new BooleanRenderer( trueValue.toString() );
  }
}
