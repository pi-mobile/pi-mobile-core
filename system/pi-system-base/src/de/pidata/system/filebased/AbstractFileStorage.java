/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.filebased;

import de.pidata.log.Logger;
import de.pidata.system.base.Storage;

import java.io.*;

/**
 * Created by pru on 30.01.15.
 */
public abstract class AbstractFileStorage extends Storage {

  private static final boolean DEBUG = false;

  protected String  path;
  private   Storage altReadOnlyStorage;

  public AbstractFileStorage( String storageName, String path, Storage altReadOnlyStorage ) {
    super(storageName);
    this.path = path;
    this.altReadOnlyStorage = altReadOnlyStorage;
  }

  public Storage getAltReadOnlyStorage() {
    return altReadOnlyStorage;
  }

  /**
   * Returns the File object representing fileName
   * @param fileName the file name within this storage or null for this storage itself
   * @return the File object representing fileName
   */
  protected abstract File getFile(String fileName);

  /**
   * Return file object for this storage self
   * @return dir file object
   */
  protected abstract File getStorageFile();

  /**
   * Returns the fully path identifying fileName.
   *
   * @param fileName the name source name within this storage
   * @return the path for fileName
   */
  @Override
  public String getPath( String fileName ) {
    return getFile( fileName ).getAbsolutePath();
  }

  public static boolean isPathSeparator( char ch ) {
    return ((ch == '/') || (ch == '\\'));
  }

  public String getChildPath( String childName ) {
    if (path == null) {
      return '/' + childName;
    }
    else if (path.length() == 1 && isPathSeparator( path.charAt( 0 ))) {
      return path + childName;
    }
    else {
      return path + '/' + childName;
    }
  }

  /**
   * Returns an input stream to source within this storage
   * @param  source  the name source name within this storage
   * @return an input stream to source within this storage
   * @throws java.io.IOException if source does not exist or cannot be accessed
   */
  public InputStream read(String source) throws IOException {
    File inFile = getFile(source);
    InputStream in = null;
    if (inFile.exists()) {
      in = new FileInputStream(inFile);
    }
    else if (altReadOnlyStorage != null) {
      in = altReadOnlyStorage.read( source );
    }
    if (in == null) {
      throw new IllegalArgumentException("File not found: path="+path +" name="+source + ", file="+inFile.getAbsolutePath() );
    }
    return new BufferedInputStream(in);
  }

  /**
   * Returns true if source exists.
   * @param source the source name within this storage or null for this storage itself
   * @return true if source exists
   */
  public boolean exists(String source) {
    File testFile = getFile( source );
    if (testFile.exists()) {
      return true;
    }
    else if (altReadOnlyStorage != null) {
      return altReadOnlyStorage.exists( source );
    }
    else {
      return false;
    }
  }

  /**
   * Returns size of source in bytes.
   * @param source the source name within this storage
   * @return size of source in bytes
   */
  public long size( String source ) {
    File file = getFile(source);
    return file.length();
  }

  /**
   * Returns true if source is a directory
   *
   * @param source the source name within this storage
   * @return true if source is a directory
   */
  public boolean isDirectory( String source ) throws IOException {
    File testFile = getFile(source);
    return testFile.isDirectory();
  }

  /**
   * Creates a sub directory with given name
   *
   * @param childName
   * @return
   * @throws IOException if this sotrage does not allow sub directories,
   *                     childName is not valid for a sub directory
   *                     or any other IO error
   */
  @Override
  public Storage createSubDirectory( String childName ) throws IOException {
    File parentDir = getStorageFile();
    parentDir.mkdir();
    return getSubDirectory( childName );
  }

  /**
   * Returns last modification timestamp of source in millis.
   *
   * @param source the source name within this storage
   * @return last modification timestamp of source in millis
   */
  public long lastModified( String source ) throws IOException {
    File file = getFile( source );
    return file.lastModified();
  }

  /**
   * Returns an output stream to destination within this storage
   *
   * @param destination the name source name within this storage
   * @param overwrite   true: destination is created or overwritten if exists
   *                    false: IOException is thrown if destination exists
   * @param append      if true file is append, else overwritten
   * @return an input stream to source within this storage
   * @throws java.io.IOException if destination cannot be written
   */
  public OutputStream write( String destination, boolean overwrite, boolean append ) throws IOException {
    File outFile = getFile(destination);
    if (outFile.exists() && (!(overwrite || append))) {
      throw new IOException("Destination '"+destination+"' already exists in path '"+path+"'");
    }
    if (!outFile.exists()) {
      append = false;
      new File( outFile.getParent() ).mkdirs();
      return new FileOutputStream(outFile.getPath());
    }
    else {
      return new FileOutputStream(outFile.getPath(), append);
    }
  }

  /**
   * Returns an array representing the directory of this storage (e.g. file names).
   *
   * @return a unordered array of Strings, if sourcePath is not a directoy or empty an array of size 0 is returned
   */
  public String[] list() throws IOException {
    String[] result = null;
    String[] altResult = null;
    File dir = getStorageFile();
    if (dir.exists()) {
      if (dir.isDirectory()) {
        result = dir.list();
      }
    }
    if (altReadOnlyStorage == null) {
      altResult = new String[0];
    }
    else {
      altResult = altReadOnlyStorage.list();
    }
    if (result == null) {
      return altResult;
    }
    else if ((altResult == null) || (altResult.length == 0)) {
      return result;
    }
    else {
      String[] concatResult = new String[result.length + altResult.length];
      System.arraycopy( result, 0, concatResult, 0, result.length );
      System.arraycopy( altResult, 0, concatResult, result.length, concatResult.length );
      return concatResult;
    }
  }

  private void deleteRecursive( File file ) throws IOException {
    if (file.isDirectory()) {
      for (File child : file.listFiles()) {
        deleteRecursive( child );
      }
    }
    boolean success = file.delete();
    if (!success) {
      throw new IOException( "Could not delete file name='"+file.getName()+"'" );
    }
  }

  /**
   * Deletes resource within this storage. If resource does not exist nothing happens.
   * @param resource the name of the resxource to be deleted
   * @throws java.io.IOException if resource could not be deleted
   */
  public void delete(String resource) throws IOException {
    if (DEBUG) Logger.debug( "delete file name=" + resource );
    File file = getFile( resource );
    if (file.exists()) {
      deleteRecursive( file );
    }
  }

  /**
   * Renames filename to newName
   *
   * @param filename name of the file to be renamed
   * @param newName  new name for the file
   * @throws java.io.IOException if filename could not be renamed
   */
  public void rename( String filename, String newName ) throws IOException {
    if (DEBUG) Logger.debug("rename file name="+filename+" to "+newName);
    boolean success = getFile( filename ).renameTo( getFile( newName ) );
    if (!success) {
      throw new IOException( "Could not rename file name='"+filename+"', new name='"+newName+"'" );
    }
  }

  /**
   * Moves each file from this storage to another.
   *
   * @param fileName    file to be moved
   * @param destStorage name of the destination storage - may be the same like this
   * @param newName     new name or null to use fileName
   */
  @Override
  public void move( String fileName, Storage destStorage, String newName ) {
    File from = getFile( fileName );
    if (newName == null) {
      newName = fileName;
    }
    File to = new File( destStorage.getPath( newName ) );
    from.renameTo( to );
  }

  public void move( String path, String fromFile, String toFile ) throws IOException {
    File from = getFile( fromFile );
    File to = getFile( toFile );
    new File( to.getParent() ).mkdirs();
    from.renameTo( to );
  }

  public void copy( String path, String fromFile, String toFile) throws IOException {
    File from = getFile( fromFile );
    File to   = getFile( toFile );
    new File( to.getParent() ).mkdirs();

    FileInputStream in = new FileInputStream( from );
    FileOutputStream out = new FileOutputStream( to );

    byte[] buffer = new byte[8192];
    int read;
    while ((read = in.read( buffer )) != -1) {
      out.write( buffer, 0, read );
    }

    in.close();
    out.close();
  }
}
