//==============================================================================
//  This file is part of the PI-Data mobile framework.
//  (C) Copyright 2005 by PI-Data AG, Germany. All rights reserved.
//==============================================================================
package de.pidata.system.filebased;

import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.system.base.Storage;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.SystemManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

/** @noinspection ConstantConditions*/
public class FilebasedStorage extends Storage {

  private static final boolean DEBUG = false;

  private String path;

  public FilebasedStorage( String storageName, String basePath ) {
    super(storageName);
    path = getStoragePath( storageName, basePath );
  }

  public static boolean isPathSeparator( char ch ) {
    return ((ch == '/') || (ch == '\\'));
  }

  public static String getStoragePath( String storageName, String basePath ) {
    String path;
    if ((basePath == null) || (basePath.length() == 0)) {
      if ((storageName == null) || (storageName.length() == 0)) {
        path = null;
      }
      else {
        path = storageName;
      }
    }
    else {
      if ((storageName == null) || (storageName.length() == 0)) {
        path = basePath;
      }
      else {
        // support absolute paths on Linux and Windows (stating with / oder drive letter)
        if (isPathSeparator(storageName.charAt(0)) || (storageName.length() >= 2) && (storageName.charAt(1) == ':')) {
          path = storageName;
        }
        else {
          if (basePath.equals( "" )) {
            path = storageName;
          }
          else if (".".equals( storageName)) {
            path = basePath;
          }
          else {
            if (isPathSeparator( basePath.charAt( basePath.length()-1 ) )) {
              path = basePath + storageName;
            }
            else {
              path = basePath + "/" + storageName;
            }
          }
        }
      }
    }
    return path;
  }

  /**
   * Returns the fully path identifying fileName.
   *
   * @param fileName the name source name within this storage
   * @return the path for fileName
   */
  @Override
  public String getPath( String fileName ) {
    if (fileName == null) {
      if ((path == null) || (path.length() == 0)) {
        return "";
      }
      else {
        return path;
      }
    }
    else if ((path != null) && (path.length() > 0)) {
      if (path.endsWith( "/" ) || path.endsWith("\\")) {
        return path + fileName;
      }
      else {
        return path + "/" + fileName;
      }
    }
    else {
      return fileName;
    }
  }

  /**
   * Returns the File object representing fileName
   * @param fileName the file name within this storage
   * @return the File object representing fileName
   */
  private File getFile(String fileName) {
    return new File( getPath( fileName ) );
  }

  /**
   * Returns an input stream to source within this storage
   * @param  source  the name source name within this storage
   * @return an input stream to source within this storage
   * @throws IOException if source does not exist or cannot be accessed
   */
  public InputStream read(String source) throws IOException {
    File inFile = getFile(source);
    InputStream in;
    if (inFile.exists()) {
      in = new FileInputStream(inFile);
    }
    else {
      String resourceName = "/"+source;
      if (name != null) {
        resourceName = "/" + name + resourceName;
      }
      in = getClass().getResourceAsStream( resourceName );
      if (in == null) {
        throw new IllegalArgumentException("File not found: path="+path +" name="+source + ", file="+inFile.getAbsolutePath()+", resource="+resourceName );
      }
    }
    return in;
  }

  /**
   * Returns true if source exists.
   * @param source the source name within this storage or null for this storage itself
   * @return true if source exists
   */
  public boolean exists(String source) {
    File testFile = getFile(source);
    if (testFile.exists()) {
      return true;
    }
    else {
      String resourceName = "/"+source;
      if (name != null) {
        resourceName = "/" + name + resourceName;
      }
      InputStream in = getClass().getResourceAsStream( resourceName );
      if (in == null) {
        return false;
      }
      else {
        StreamHelper.close(in);
        return true;
      }
    }
  }

  /**
   * Returns size of source in bytes.
   * @param source the source name within this storage
   * @return size of source in bytes
   */
  public long size( String source ) {
    File file = getFile(source);
    return file.length();
  }

  /**
   * Returns true if source is a directory
   *
   * @param source the source name within this storage
   * @return true if source is a directory
   */
  public boolean isDirectory( String source ) throws IOException {
    File testFile = getFile(source);
    return testFile.isDirectory();
  }

  /**
   * Returns the sub directory for childName
   *
   * @param childName the sub directory's name
   * @return the sub directory storage
   * @throws IOException if childName is not a directory or any other io error
   */
  public Storage getSubDirectory( String childName ) throws IOException {
    File testFile = getFile(childName);
    if (testFile.isDirectory()) {
      return new FilebasedStorage( childName, testFile.getPath() );
    }
    else {
      throw new IOException( "File is not a directory, name="+childName );
    }
  }

  /**
   * Creates a sub directory with given name
   *
   * @param childName
   * @return
   * @throws IOException if this sotrage does not allow sub directories,
   *                     childName is not valid for a sub directory
   *                     or any other IO error
   */
  public Storage createSubDirectory( String childName ) throws IOException {
    File parentDir = getFile(null);
    parentDir.mkdirs();
    return getSubDirectory( childName );
  }

  /**
   * Returns last modification timestamp of source in millis.
   *
   * @param source the source name within this storage
   * @return last modification timestamp of source in millis
   */
  public long lastModified( String source ) throws IOException {
    File file = getFile(source);
    return file.lastModified(); 
  }

  /**
   * Returns an output stream to destination within this storage
   *
   * @param destination the name source name within this storage
   * @param overwrite   true: destination is created or overwritten if exists
   *                    false: IOException is thrown if destination exists
   * @param append      if true file is append, else overwritten
   * @return an input stream to source within this storage
   * @throws java.io.IOException if destination cannot be written
   */
  public OutputStream write( String destination, boolean overwrite, boolean append ) throws IOException {
    File outFile = getFile(destination);
    if (outFile.exists() && (!(overwrite || append))) {
      throw new IOException("Destination '"+destination+"' already exists in path '"+path+"'");
    }
    if (!outFile.exists()) {
      append = false;
      new File( outFile.getParent() ).mkdirs();
    }
    return new FileOutputStream(outFile.getPath(), append);
  }

  /**
   * Returns an array representing the directory of this storage (e.g. file names)
   *
   * @return a unordered array of Strings, if sourcePath is not a directoy or empty an array of size 0 is returned
   */
  public String[] list() {
    String[] result = null;  //TODO Ressourcen ergänzen
    File dir = new File(path);
    if (dir.exists()) {
      if (dir.isDirectory()) {
        result = dir.list();
      }
    }
    if (result == null) {
      result = new String[0];
    }
    return result;
  }

  /**
   * Deletes resource within this storage. If resource does not exist nothing happens.
   * If resource is a directory its content is deleted recursively.
   * @param resource the name of the resource to be deleted
   * @throws IOException if resource could not be deleted
   */
  public void delete(String resource) throws IOException {
    if (DEBUG) Logger.debug("delete file name="+resource);
    File file = getFile( resource );
    if (file.exists()) {
      boolean success = deleteFile( file );
      if (!success) {
        throw new IOException( "Could not delete file name='"+resource+"'" );
      }
    }
  }

  private boolean deleteFile( File file ) throws IOException {
    boolean success = true;
    if (file.isDirectory()) {
      String[] files = file.list();
      for (int i = 0; i < files.length; i++) {
        File contentFile = new File( file, files[i] );
        if (!contentFile.delete()) {
          success = false;
        }
      }
    }
    if (success) {
      return file.delete();
    }
    else {
      return false;
    }
  }

  /**
   * Renames filename to newName
   *
   * @param filename name of the file to be renamed
   * @param newName  new name for the file
   * @throws java.io.IOException if filename could not be renamed
   */
  public void rename( String filename, String newName ) throws IOException {
    if (DEBUG) Logger.debug("rename file name="+filename+" to "+newName);
    boolean success = getFile( filename ).renameTo( getFile( newName ) );
    if (!success) {
      throw new IOException( "Could not rename file name='"+filename+"', new name='"+newName+"'" );
    }
  }

  /**
   * Write a stringBufer to the given filePath
   *
   * @param data the data StringBuffer
   * @param filePath destination for writing
   */
  private void writeToFile(StringBuffer data, String filePath) throws IOException {
    if (DEBUG) Logger.debug("writeToFile --> name="+filePath);
    File file = getFile(filePath);
    File parentDir = new File(new File(file.getAbsolutePath()).getParent());
    parentDir.mkdirs();
    FileWriter fw = null;
    try {
      fw = new FileWriter(file);
      fw.write(data.toString());
      fw.flush();
    }
    finally {
      StreamHelper.close( fw );
    }
    if (DEBUG) Logger.debug("writeToFile <-- name="+filePath);
  }

  /**
   * Moves each file from this storage to another.
   *
   * @param fileName    file to be moved
   * @param destStorage name of the destination storage - may be the same like this
   * @param newName     new name or null to use fileName
   */
  @Override
  public void move( String fileName, Storage destStorage, String newName ) {
    File from = getFile( fileName );
    if (newName == null) {
      newName = fileName;
    }
    File to = new File( destStorage.getPath( newName ) );
    from.renameTo( to );
  }

  public void move(String path, String fromFile, String toFile) throws IOException {
    File from = getFile( fromFile );
    File to = getFile( toFile );
    new File( to.getParent() ).mkdirs();
    from.renameTo( to );
  }

  public void copy(String path, String fromFile, String toFile) throws IOException {
    File from = getFile( fromFile );
    File to   = getFile( toFile );
    new File( to.getParent() ).mkdirs();

    FileInputStream in = new FileInputStream( from );
    FileOutputStream out = new FileOutputStream( to );

    byte[] buffer = new byte[8192];
    int read;
    while ((read = in.read( buffer )) != -1) {
      out.write( buffer, 0, read );
    }

    in.close();
    out.close();
  }

  /**
   * Writes data from inStream to fileName
   * @param inputStream the source input stream
   * @param fileName    path and name of the destination file
   * @return the number of bytes written to fileName
   * @throws java.io.IOException
   */
  public int writeData(InputStream inputStream, String fileName) throws IOException {
    if (DEBUG) Logger.debug("writeData --> name="+fileName);
    BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
    String inputLine;
    File file =getFile(fileName);
    file.getParentFile().mkdirs();
    FileWriter dataFile = new FileWriter(file);
    int sum = 0;

    //----- Receive data file
    int countLine = 0;
    while ((inputLine = in.readLine()) != null) {
      if (inputLine.indexOf(4) >= 0) break;
      dataFile.write(inputLine);
      dataFile.write("\n");
      sum += inputLine.length();
      countLine++;
    }
    dataFile.flush();
    dataFile.close();
    if (DEBUG) Logger.debug("writeData <-- name="+fileName);
    return sum;
  }

  /**
   * Create the
   *
   * @param data the node data
   * @param root the root node of the data
   */
  public void writeData(StringBuffer data, Model root) throws IOException {
    String filePath;

    filePath = getDataFileName(root);
    this.writeToFile(data, filePath);
  }


  public String getDataFileName(Model root) {
    return SystemManager.DATA_ROOT + '/' + root.type().name().getName() + ".xml";
  }

}
