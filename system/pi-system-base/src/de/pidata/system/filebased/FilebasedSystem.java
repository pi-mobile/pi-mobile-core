/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.filebased;

import de.pidata.file.FilebasedRollingLogger;
import de.pidata.log.DefaultLogger;
import de.pidata.log.Level;
import de.pidata.log.Logger;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;

import java.io.*;
import java.util.Locale;
import java.util.Properties;

/**
 * SystemManager for file-based systems.
 */
public abstract class FilebasedSystem extends SystemManager {

  private Properties  applicationProps;
  private Properties  systemProps;
  private boolean     applicationPropsDirty = false;

  /**
   * Initialize SystemManager for Desktop (1.1) use.
   * Initializes SystemReader and Storage.
   */
  protected FilebasedSystem() {
    this( "" );
  }

  protected FilebasedSystem( String basePath ) {
    this( basePath, null, null );
  }

  protected FilebasedSystem( String basePath, Properties systemProps, Properties appProps ) {
    super( basePath, new FilebasedStorage(null, basePath), null, null, null);
    this.systemProps = systemProps;
    this.applicationProps = appProps;
    initProperties();
    initLogging();
    loadFactories();
    Logger.info( "FilebasedSystem initialized, basePath="+getBasePath() );
    Logger.debug( "factories and schemas loaded" );
  }

  public Storage getStorage(String storageName) {
    if (storageName == null) {
      return systemStorage;
    }
    Storage result = (Storage) storageTable.get(storageName);
    if (result == null) {
      result = new FilebasedStorage(storageName, this.basePath);
      storageTable.put(storageName, result);
    }
    return result;
  }

  public Storage getStorage( String storageType, String storagePath ) {
    if (storageType.equals( SystemManager.STORAGE_CLASSPATH )) {
      return getStorage( storagePath );
    }
    else if (storageType.equals( SystemManager.STORAGE_PRIVATE_DOWNLOADS )) {
      return getStorage( storagePath );
    }
    else {
      String path = getProperty( "storage."+storageType, null );
      if (path == null) {
        throw new IllegalArgumentException( "Unknown storage type=" + storageType + " - use property 'storage."+storageType+"' to define path" );
      }
      else {
        return getStorage( path + '/' + storagePath );
      }
    }
  }

  protected Properties getSystemProps() {
    if (systemProps == null) {
      systemProps = new Properties();
    }
    return systemProps;
  }

  public void initProperties() {
    Properties versionProps = new Properties();
    try {
      InputStream versionsStream = getClass().getResourceAsStream( "/version.properties" );
      if (versionsStream == null) {
        Logger.warn( "version.properties not found in class path - using defaults");
      }
      else {
        versionProps.load( versionsStream );
      }
      this.programName = versionProps.getProperty( "application.name", "PI-Mobile" );
      this.programVersion = versionProps.getProperty( "application.version", "3.0" );
      this.programDate = versionProps.getProperty( "application.date", "" );
    }
    catch (Exception e) {
      Logger.error( "Fehler beim Lesen der version.properties", e );
      SystemManager.getInstance().exit();
    }
    if (systemProps == null) {
      Logger.debug( "loading system properties: '" + SystemManager.SYSTEM_PROPFILE + "'" );
      systemProps = loadProps( systemStorage, SystemManager.SYSTEM_PROPFILE );
    }

    // set system base path
    // TODO basePath kommt eigentlich über den Konstruktor und wird vorher schon verwendet. Derzeit braucht aber
    // mindestens der Server folgendes Property!
    String oldBasePath = basePath;
    String path = systemProps.getProperty( SystemManager.KEY_BASEPATH );
    if (path != null) {
      this.basePath = path;
    }
    else if (basePath == null) {
      basePath = "";
    }
    if (!basePath.equals( oldBasePath )) {
      systemStorage = new FilebasedStorage( null, basePath );
    }
    Logger.debug( "basePath: '" + basePath + "'" );

    if (applicationProps == null) {
      applicationProps = new Properties();
      String propfileName = getProperty( SystemManager.KEY_APPLICATION_PROPFILE, SystemManager.APPLICATION_PROPFILE );
      applicationProps = loadProps( systemStorage, propfileName );
    }

    Logger.debug( "starting system...");
  }

  protected void initLogging() {
    Level logLevel = Level.DEBUG;
    String logLevelProp = systemProps.getProperty( SystemManager.KEY_LOGLEVEL);
    if (logLevelProp != null) {
      System.out.print( "loglevel=" + logLevelProp );
      logLevel = Level.fromName( logLevelProp );
      System.out.println( " (" + logLevel + ")" );
    }

    String logfile = systemProps.getProperty( SystemManager.KEY_LOGFILE);
    if (logfile != null) {
      try {
        System.out.println("logfile="+logfile);
        int logfileExpireDays = getPropertyInt( FilebasedRollingLogger.KEY_LOGFILE_EXPIREDAYS, 1 );
        int logfilePurgeDays = getPropertyInt( FilebasedRollingLogger.KEY_LOGFILE_PURGEDAYS, 30 );
        FilebasedRollingLogger logger = new FilebasedRollingLogger( logfile, logfileExpireDays, logfilePurgeDays, logLevel );
        Logger.addLogger( logger );
        addReportFileOwner( logger );
      }
      catch (Exception e) {
        e.printStackTrace();
        Logger.setLogger( new DefaultLogger( logLevel ) );
      }
    }
  }

  public static Properties loadProps( Storage storage, String filename ) {
    // TODO: support database properties
    Properties props = new Properties();
    if (storage.exists( filename )) {
      InputStream in = null;
      try {
        in = storage.read( filename );
        props.load( in );
        in.close();
      }
      catch (IOException e) {
        Logger.warn( "Could not load properties filename=" + filename );
      }
      finally {
        StreamHelper.close( in );
      }
    }
    return props;
  }

  /**
   * Retrieve a system wide property. Properties are not bound to the lifetime
   * of one instance. The implementing class has to supply a mechanism to make
   * them persistent, so they can be accessed across several sessions.
   *
   * @param key The key identifying this property.
   * @return A property bound to that key or null if none has been defined.
   */
  protected String getApplicationProperty( String key ) {
    if (applicationProps == null) {
      return null;
    }
    else {
      return applicationProps.getProperty( key );
    }
  }

  /**
   * Retrieve a system property. System Properties are read only properties used to
   * configure non-application specific system behaviour, such as database connection parameters.
   *
   * @param key The key identifying this property.
   * @return A property bound to that key or null if none has been defined.
   */
  protected String getSystemProperty(String key) {
    String value = System.getProperty( key );
    if (value == null) {
      if (systemProps != null) {
        value = systemProps.getProperty( key );
      }
    }
    return value;
  }

  /**
   * Set a system wide property. The system has to ensure properties are made persistent.
   *
   * @param key The key identifying this property.
   * @param value The value to be bound to that key.
   * @param save indicates if this property should be made persistent immediately or not.
   * @throws java.io.IOException If properties could not be saved in case <code>save</code> is
   *           set to <code>true</code>.
   */
  public void setProperty(String key, String value, boolean save) throws IOException {
    applicationProps.put( key, value );
    applicationPropsDirty = true;
    if (save) {
      saveProperties();
    }
  }

  /**
   * Commit all application properties to some persistent storage, e.g. file system.
   *
   * @throws java.io.IOException If properties could not be saved.
   */
  public void saveProperties() throws IOException {
    if (applicationPropsDirty) {
      FileOutputStream fout = new FileOutputStream( SystemManager.APPLICATION_PROPFILE );
      applicationProps.save( fout, "APPLICATION PROPERTY FILE ------------" );
      fout.close();
    }
    applicationPropsDirty = false;
  }

  @Override
  public Properties getLanguageProps( String propFileName, String language ) {
    if (language == null) {
      language = Locale.getDefault().getLanguage();
    }
    String path = "/lang/" + language + "/" + propFileName+ ".properties";
    try {
      InputStream langPropStream = getClass().getResourceAsStream( path );
      if (langPropStream == null) {
        Logger.warn( "Language properties not found, resource path='"+path+"'" );
      }
      else {
        Properties langProps = new Properties();
        langProps.load( langPropStream );
        return langProps;
      }
    }
    catch (Exception ex) {
      Logger.warn( "Error loading properties from resource '"+path+"'" );
    }
    return null;
  }

  /**
   * Perform some cleanup and exit runtime system.
   */
  public void exit() {
    doExit();
    System.exit( 0 );
  }
}
