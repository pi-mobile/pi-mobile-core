/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.base;

import de.pidata.models.tree.BaseFactory;
import de.pidata.qnames.Key;
import de.pidata.models.tree.SimpleKey;
import de.pidata.qnames.QName;

import java.io.IOException;

public class NumberSequence implements Sequence {

  public static final int KEY_TYPE_LONG = 0;
  public static final int KEY_TYPE_INT = 1;
  public static final int KEY_TYPE_QNAME = 2;

  protected String name;
  protected long min;
  protected long last;
  protected long max;
  protected boolean updateRequested;
  protected int keyType;

  /**
   * Creates a new number sequence
   * @param name         name of the sequence to be created
   * @param initialValue initial value for the new sequence
   * @param max          max value for the new sequence
   * @param keyType      key type to use, one of constants NumberSequence.KEY_TYPE_*
   */
  public NumberSequence( String name, long initialValue, long max, int keyType ) {
    this.name = name;
    this.last = initialValue;
    this.min = initialValue;
    this.max = max;
    this.keyType  = keyType;
  }

  /**
   * Updates this sequence with a new min and max value and resets percent left to 100%
   * @param min  new min value
   * @param max  new max value
   */
  public void update( long min, long max ) throws IOException {
    this.last = min;
    this.min = min;
    this.max = max;
    this.updateRequested = false;
  }

  /**
   * Returns a key made from next value in sequence and increments the sequence value
   *
   * @return the new key
   * @throws IllegalArgumentException if there is no next value in this sequence, i.e next is greater than max()
   */
  public final Key nextKey() throws IOException {
    switch (keyType) {
      case 1: return new SimpleKey( new Integer((int)next(1)) );
      case 2: return QName.getInstance( BaseFactory.NAMESPACE, Long.toString(next(1)) );
      default: return new SimpleKey( new Long(next(1)) );
    }
  }

  /**
   * Increments the sequence by count and returns the first new value
   *
   * @param count number of values needed
   * @return next sequence value or first value if count is greater than 1
   * @throws IllegalArgumentException if there is no next value in this sequence, i.e next is greater than max()
   */
  public long next( int count ) throws IOException {
    if ((this.last+count) >= max) {
      throw new IllegalArgumentException( "Sequence '" + name + "' has not enough values for count="+count+", last="+last+", max=" + max );
    }
    long first = last + 1;
    last += count;
    return first;
  }

  /**
   * Returns this sequence's maximum value
   *
   * @return
   */
  public long max() {
    return max;
  }

  /**
   * Returns a percent value (between 100 and 0) indicating how many sequence values are left
   *
   * @return percent values left in this sequence
   */
  public int percentLeft() {
    long size = max - min;
    if (size == 0) {
      return 0;
    }
    else {
      return (int)  ((max - last + 1) * 100 / (size));
    }  
  }

  public boolean isUpdateRequested() {
    return updateRequested;
  }

  public void setUpdateRequested( boolean updateRequested ) throws IOException {
    this.updateRequested = updateRequested;
  }
}
