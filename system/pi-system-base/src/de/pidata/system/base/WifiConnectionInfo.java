/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.base;


public class WifiConnectionInfo {

  public WifiConnectionInfo( WifiConnectionState connectionState, String wifiSsid, int networkId, int linkSpeed, int ipAddress, String macAddress ) {
    this.connectionState = connectionState;
    this.wifiSsid = wifiSsid;
    this.networkId = networkId;
    this.linkSpeed = linkSpeed;
    this.ipAddress = ipAddress;
    this.macAddress = macAddress;
    this.rssi = 0;
  }

  public WifiConnectionInfo( WifiConnectionState connectionState, String wifiSsid, int networkId, int linkSpeed, int ipAddress, String macAddress, int rssi ) {
    this.connectionState = connectionState;
    this.wifiSsid = wifiSsid;
    this.networkId = networkId;
    this.linkSpeed = linkSpeed;
    this.ipAddress = ipAddress;
    this.macAddress = macAddress;
    this.rssi = rssi;
  }

  private String wifiSsid;
  private int networkId;
  private int linkSpeed;
  private int ipAddress;
  private String macAddress;
  private WifiConnectionState connectionState;
  private int rssi; // Received Signal Strength Indicator

  public String getWifiSsid() {
    return wifiSsid;
  }

  public int getNetworkId() {
    return networkId;
  }

  public int getLinkSpeed() {
    return linkSpeed;
  }

  public int getIpAddress() {
    return ipAddress;
  }

  public String getMacAddress() {
    return macAddress;
  }

  public WifiConnectionState getConnectionState() {
    return connectionState;
  }

  public int getRssi() {
    return this.rssi;
  }

  public void setWifiSsid( String wifiSsid ) {
    this.wifiSsid = wifiSsid;
  }

  public void setNetworkId( int networkId ) {
    this.networkId = networkId;
  }

  public void setLinkSpeed( int linkSpeed ) {
    this.linkSpeed = linkSpeed;
  }

  public void setIpAddress( int ipAddress ) {
    this.ipAddress = ipAddress;
  }

  public void setMacAddress( String macAddress ) {
    this.macAddress = macAddress;
  }

  public void setConnectionState( WifiConnectionState connectionState ) {
    this.connectionState = connectionState;
  }

  @Override
  public boolean equals( Object obj ) {
    WifiConnectionInfo infoToCompare = (WifiConnectionInfo) obj;
    if (infoToCompare.getConnectionState().equals( this.connectionState ) &&
        infoToCompare.getWifiSsid().equals( this.wifiSsid ) &&
        infoToCompare.getMacAddress().equals( this.macAddress )) {
      return true;
    }
    else {
      return false;
    }
  }
}
