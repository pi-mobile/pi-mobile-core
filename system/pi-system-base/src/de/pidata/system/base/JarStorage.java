/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.base;

import de.pidata.stream.StreamHelper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class JarStorage extends Storage {

  public JarStorage(String storagePath) {
    super(storagePath);
  }

  /**
   * Returns the fully path identifying fileName.
   *
   * @param fileName the name source name within this storage
   * @return the path for fileName
   */
  @Override
  public String getPath( String fileName ) {
    if (this.name == null) {
      return "/" + fileName;
    }
    else {
      return  "/" + this.name + "/" + fileName;
    }
  }

  /**
   * Returns an input stream to source within this storage
   *
   * @param source the name source name within this storage
   * @return an input stream to source within this storage
   * @throws java.io.IOException if source does not exist or cannot be accessed
   */
  public InputStream read(String source) throws IOException {
    return getClass().getResourceAsStream( getPath( source ) );
  }

  /**
   * Returns true if source exists.
   * @param source the source name within this storage or null for this storage itself
   * @return true if source exists
   */
  public boolean exists(String source) {
    InputStream in = null;
    try {
      in = read(source);
      boolean result = (in != null);
      return result;
    }
    catch(IOException ex) {
      return false;
    }
    finally {
      StreamHelper.close( in );
    }
  }

  /**
   * Returns size of source in bytes.
   *
   * @param source the source name within this storage
   * @return size of source in bytes
   */
  public long size( String source ) throws IOException {
    throw new IOException("JarStorage does not support file size!");
  }

  /**
   * Returns true if source is a directory
   *
   * @param source the source name within this storage
   * @return true if source is a directory
   */
  public boolean isDirectory( String source ) throws IOException {
    throw new IOException("JarStorage does not support isDirectory for files!");
  }

  /**
   * Returns the sub directory for childName
   *
   * @param childName the sub directory's name
   * @return the sub directory storage
   * @throws IOException if childName is not a directory or any other io error
   */
  public Storage getSubDirectory( String childName ) throws IOException {
    throw new IOException("JarStorage does not support getSubDirectory for files!");
  }

  /**
   * Creates a sub directory with given name
   *
   * @param childName
   * @return
   * @throws IOException if this sotrage does not allow sub directories,
   *                     childName is not valid for a sub directory
   *                     or any other IO error
   */
  public Storage createSubDirectory( String childName ) throws IOException {
    throw new IOException("JarStorage does not allow creating sub directories!");
  }

  /**
   * Returns last modification timestamp of source in millis.
   *
   * @param source the source name within this storage
   * @return last modification timestamp of source in millis
   */
  public long lastModified( String source ) throws IOException {
    throw new IOException("JarStorage does not support lastModified for files!");
  }

  /**
   * Returns an output stream to destination within this storage
   *
   * @param destination the name source name within this storage
   * @param overwrite   true: destination is created or overwritten if exists
   *                    false: IOException is thrown if destination exists
   * @param append
   * @return an input stream to source within this storage
   * @throws java.io.IOException if destination cannot be written
   */
  public OutputStream write( String destination, boolean overwrite, boolean append ) throws IOException {
    throw new IOException("JarStorage is read only!");
  }

  /**
   * Returns an array representing the directory of this storage (e.g. file names)
   *
   * @return a unordered array of Strings or null if sourcePath is not a directoy
   */
  public String[] list() throws IOException {
    throw new IOException("list() not supported by jar storage");
  }

  /**
   * Deletes resource within this storage. If resource does not exist nothing happens.
   *
   * @param resource the name of the resxource to be deleted
   * @throws java.io.IOException if resource could not be deleted
   */
  public void delete(String resource) throws IOException {
    throw new IOException("JarStorage is read only!");
  }

  /**
   * Renames filename to newName
   *
   * @param filename name of the file to be renamed
   * @param newName  new name for the file
   * @throws java.io.IOException if filename could not be renamed
   */
  public void rename( String filename, String newName ) throws IOException {
    throw new IOException("JarStorage is read only!");
  }

  /**
   * Moves each file from this storage to another.
   *
   * @param fileName    file to be moved
   * @param destStorage name of the destination storage - may be the same like this
   * @param newName     new name or null to use fileName
   */
  @Override
  public void move( String fileName, Storage destStorage, String newName ) throws IOException {
    throw new IOException("JarStorage is read only!");
  }
}
