//==============================================================================
//  This file is part of the PI-Data mobile framework.
//  (C) Copyright 2005 by PI-Data AG, Germany. All rights reserved.
//==============================================================================

package de.pidata.system.base;

import de.pidata.stream.StreamHelper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;

public abstract class Storage {
  private static Storage instance = null;
  private static Hashtable instanceMap = new Hashtable();

  protected String name;

  protected Storage(String name ) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public String createTempFileName() {
    String fileName = "_temp";
    int index = 0;
    while (exists( fileName+index )) {
      index++;
    }
    return fileName+index;
  }

  /**
   * Returns the fully path identifying fileName.
   *
   * @param fileName the name source name within this storage or null for the storage itself
   * @return the path for fileName
   */
  public abstract String getPath( String fileName );

  /**
   * Returns an input stream to source within this storage
   *
   * @param  source  the name source name within this storage
   * @return an input stream to source within this storage
   * @throws IOException if source does not exist or cannot be accessed
   */
  public abstract InputStream read(String source) throws IOException;

  /**
   * Returns true if source exists.
   * @param source the source name within this storage or null for this storage itself
   * @return true if source exists
   */
  public abstract boolean exists(String source);

  /**
   * Returns size of source in bytes.
   * @param source the source name within this storage
   * @return size of source in bytes
   */
  public abstract long size(String source) throws IOException;

  /**
   * Returns true if source is a directory
   * @param source the source name within this storage
   * @return true if source is a directory
   */
  public abstract boolean isDirectory(String source) throws IOException;

  /**
   * Returns the sub directory for childName
   * @param childName the sub directory's name
   * @return the sub directory storage
   * @throws IOException if childName is not a directory or any other io error
   */
  public abstract Storage getSubDirectory( String childName ) throws IOException;

  /**
   * Creates a sub directory with given name
   * @param childName
   * @return
   * @throws IOException if this sotrage does not allow sub directories,
   *                     childName is not valid for a sub directory
   *                     or any other IO error
   */
  public abstract Storage createSubDirectory( String childName ) throws IOException;

  /**
   * Returns last modification timestamp of source in millis.
   * @param source the source name within this storage
   * @return last modification timestamp of source in millis
   */
  public abstract long lastModified(String source) throws IOException;

  /**
   * Returns an output stream to destination within this storage
   *
   * @param destination the name source name within this storage
   * @param overwrite   true: destination is created or overwritten if exists
   *                    false: IOException is thrown if destination exists
   * @param append
   * @return an input stream to source within this storage
   * @throws java.io.IOException if destination cannot be written
   */
  public abstract OutputStream write( String destination, boolean overwrite, boolean append ) throws IOException;


  /**
   * Copies a file from this storage to another. Uses storage's methods read() and write(),
   * delete() and rename(). First copies therce file to a temp file within destination storage,
   * then deletes old destination file (if exists) and finally renames temp file to destFileName.
   * Destfile ist replaced if it already exists.
   * @param sourceFileName name of the file within this storage to be copied
   * @param destStorage    name of the destination storage - may be the same like this
   * @param destFileName
   * @throws java.io.IOException exceptions thrown by read(), write(), delete() or rename()
   */
  public void copy( String sourceFileName, Storage destStorage, String destFileName ) throws IOException {
    InputStream in = null;
    OutputStream out = null;
    String tempFileName;
    try {
      in = read( sourceFileName );
      synchronized (this) {
        tempFileName = destStorage.createTempFileName();
        out = destStorage.write( tempFileName, true, false );
      }

      // transfer bytes from the inputfile to the outputfile
      byte[] buffer = new byte[1024];
      int length;
      while ((length = in.read(buffer)) > 0) {
        out.write(buffer, 0, length);
      }
      out.flush();
    }
    finally {
      StreamHelper.close( out );
      StreamHelper.close( in );
    }
    destStorage.delete( destFileName );
    destStorage.rename( tempFileName, destFileName );
  }

  /**
   * Moves each file from this storage to another.
   *
   * @param fileName    file to be moved
   * @param destStorage name of the destination storage - may be the same like this
   * @param newName     new name or null to use fileName
   */
  public abstract void move( String fileName, Storage destStorage, String newName ) throws IOException;

  /**
   * Returns an array representing the directory of this storage (e.g. file names)
   *
   * @return a unordered array of Strings, if sourcePath is not a directoy or empty an array of size 0 is returned
   */
  public abstract String[] list() throws IOException;

  /**
   * Deletes resource within this storage. If resource does not exist nothing happens.
   * If resource is a directory its content is deleted recursively.
   * @param resource the name of the resxource to be deleted
   * @throws IOException if resource could not be deleted
   */
  public abstract void delete(String resource) throws IOException;

  /**
   * Renames filename to newName
   * @param filename name of the file to be renamed
   * @param newName  new name for the file
   * @throws IOException if filename could not be renamed
   */
  public abstract void rename( String filename, String newName ) throws IOException;
}
