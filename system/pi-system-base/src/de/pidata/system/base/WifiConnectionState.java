package de.pidata.system.base;

public enum WifiConnectionState {

  CONNECTING,
  CONNECTED,
  CONNECTION_ERROR,
  DISABLED,
  INITIALIZING,
  UNKNOWN,
  UNAUTHORIZED

}
