/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.base;

import de.pidata.connect.base.ConnectionController;
import de.pidata.log.FileOwner;
import de.pidata.log.Logger;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.ModelFactory;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.settings.Settings;
import de.pidata.string.Helper;

import java.io.IOException;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: mbu
 * Date: 01.11.2003
 * Time: 10:52:55
 * To change this template use Options | File Templates.
 */
public abstract class SystemManager {

  protected static final boolean LOG_CALENDARINFO = false;

  public static final Namespace NS_MAC = Namespace.getInstance("SystemID.MAC");
  public static final Namespace NS_SID = Namespace.getInstance("SystemID.SID");

  public static final String APPLICATION_PROPFILE = "application.properties";
  public static final String SYSTEM_PROPFILE = "system.properties";

  public static final String KEY_BASEPATH = "system.basePath";
  public static final String KEY_APPLICATION_PROPFILE = "system.propFile";
  public static final String KEY_LOGLEVEL = "loglevel";
  public static final String KEY_LOGFILE  = "logfile";
  public static final String KEY_LOGFILE_EXPIREDAYS  = "logfile.expiredays";
  public static final String KEY_LOGFILE_PURGEDAYS  = "logfile.purgedays";
  public static final String KEY_CLIENTID = "clientId";
  public static final String KEY_DESKTOP_PATH = "system.path.desktop";

  public static final String TRANSLOGPATH = "log";
  public static final String DATA_ROOT = "data";

  public static final String STORAGE_PREFIX_PI = "PI_";
  public static final String STORAGE_CLASSPATH = STORAGE_PREFIX_PI + "Classpath";
  public static final String STORAGE_PRIVATE_PICTURES = STORAGE_PREFIX_PI + "Private-Pictures";
  public static final String STORAGE_PRIVATE_DOWNLOADS = STORAGE_PREFIX_PI + "Private-Downloads";
  public static final String STORAGE_APPDATA = STORAGE_PREFIX_PI + "AppData";
  public static final String STORAGE_TEMP = STORAGE_PREFIX_PI + "Temp";

  private static SystemManager instance = null;

  protected String clientID;
  protected QName  userID;
  protected String programName = "PI-Mobile";
  protected String programVersion = "3.0";
  protected String programDate;
  private String userLogFileName;
  protected Storage   systemStorage;
  protected Hashtable storageTable = new Hashtable();
  protected Hashtable sequenceTable = new Hashtable();

  private Settings settings;
  private Properties glossaryProps = null;
  private String glossaryLang = null;
  private List<FileOwner> repotFileOwnerList = new ArrayList<FileOwner>(  );
  private Locale locale;

  public String getBasePath() {
    return basePath;
  }

  protected String basePath;

  protected ConnectionController connectionController;

  /**
   * Create SystemManager with base directory.
   *
   * @param basePath
   * @param programDate
   */
  protected SystemManager( String basePath, Storage systemStorage, String programName, String programVersion, String programDate ) {
    if (instance != null) {
      throw new RuntimeException( "Cannot change SystemManager class." );
    }
    this.basePath = basePath;
    this.systemStorage = systemStorage;
    this.programName = programName;
    this.programVersion = programVersion;
    this.programDate = programDate;
    instance = this;
  }


  public static SystemManager getInstance() {
    return instance;
  }

  public abstract void initProperties();

  public Settings getSettings() {
    return settings;
  }

  public void setSettings( Settings settings ) {
    this.settings = settings;
  }

  public abstract Storage getStorage(String storageName);

  public abstract Storage getStorage( String storageType, String storagePath );

  public String getLogPath() {
    return TRANSLOGPATH;
  }

  public String getUserLogFileName() {
    return userLogFileName;
  }

  public void setUserLogFileName( String userLogFileName ) {
    this.userLogFileName = userLogFileName;
  }

  public ConnectionController getConnectionController() {
    return connectionController;
  }

  public void setConnectionController(ConnectionController connectionController) {
    this.connectionController = connectionController;
  }

  public abstract WifiManager getWifiManager();

  /**
   * Clears web cache/history. Needed e.g. on iOS to get new version of a server side modified file.
   */
  public void clearWebCache() {
    // default not necessary -> do nothing
  }

  public void setClientID( String clientID ) {
    if (clientID != this.clientID) {
      if (this.clientID == null) {
        Logger.info( "clientID="+clientID );
        this.clientID = clientID;
      }
      else {
        throw new IllegalArgumentException("clientID ist already set !!!");
      }
    }
  }

  /**
   * Returns an identifier for the client running this system. The client id
   * MUST NOT change during runtime, i.e. this method will always return the same value.
   *
   * @return A QName used to identify this client.
   */
  public String getClientID() {
    if (clientID == null) {
      clientID = getApplicationProperty( KEY_CLIENTID );
    }
    return clientID;
  }

  public QName getUserID() {
    return userID;
  }

  public void setUserID( QName userID ) {
    this.userID = userID;
  }

  /**
   * Returns running program's name
   * @return running program's name
   */
  public String getProgramName() {
    return programName;
  }

  /**
   * Returns running program's version
   * @return running program's version
   */
  public String getProgramVersion() {
    return programVersion;
  }

  /**
   * Returns running program's version
   * @return running program's version
   */
  public String getProgramDate() {
    return programDate;
  }

  public Context createContext() {
    return new Context( getClientID(), getUserID(), getProgramName(), getProgramVersion() );
  }

  /**
   * Returns a new key by calling the Sequence named sequenceName. The type of retuned key depends
   * on the sequence (long, int, String, QName, ...)
   * @param sequenceName the name of the sequence to get the new key from
   * @return a new key
   */
  public Key createKey( String sequenceName ) throws IOException {
    if (sequenceName == null) {
      sequenceName = "default";
    }
    Sequence sequence = getSequence( sequenceName );
    if (sequence == null) {
      throw new IllegalArgumentException( "Sequence not found, name="+sequenceName );
    }
    return sequence.nextKey();
  }

  /**
   * Returns the sequence named sequenceName or null if no sequence is registered
   * for sequenceName.
   * @param sequenceName the name of the sequence
   * @return tjhe sequence or null
   */
  public Sequence getSequence( String sequenceName ) {
    return (Sequence) sequenceTable.get( sequenceName );
  }

  /**
   * Replaces (or adds) the given sequence. A already existiung sequence with the same name is
   * replaced by the given sequence
   * @param name     the sequence's name
   * @param sequence the new sequence
   */
  public void addSequence( String name, Sequence sequence ) {
    if (sequenceTable.get( name ) != null) {
      throw new IllegalArgumentException( "Must not replace existing sequence, name="+name );
    }
    this.sequenceTable.put( name, sequence );
  }

  /**
   * Retrieve a system wide  property. Properties are not bound to the lifetime
   * of one instance. The implementing class has to supply a mechanism to make
   * them persistent, so they can be accessed across several sessions.
   * If key does not exist within application properties getSystemProperty()
   * is called. If key does not exist for both property tables defaultValue is returned.
   *
   * @param key The key identifying this property.
   * @param defaultValue the value to be returned if property for 'key' is not defined
   * @return A property bound to that key or defaultValue if key does not exist.
   */
  public String getProperty(String key, String defaultValue) {
    String result = getApplicationProperty( key );
    if (result == null) {
      result = getSystemProperty(key );
    }
    if (result == null) {
      return defaultValue;
    }
    else return result;
  }

  /**
   * Retrieve a system wide application property. Properties are not bound to the lifetime
   * of one instance. The implementing class has to supply a mechanism to make
   * them persistent, so they can be accessed across several sessions.
   *
   * @param key The key identifying this property.
   * @return A property bound to that key or null if none has been defined.
   */
  protected abstract String getApplicationProperty( String key );

  /**
   * Retrieve a system property. System Properties are read only properties used to
   * configure non-application specific system behaviour. This method first queries
   * System.getProperty() and if none can be found it tries to obtain the property
   * in a system dependend manner, e.g. via an apropriate property file.
   *
   * @param key The key identifying this property.
   * @return A property bound to that key or null if none has been defined.
   */
  protected abstract String getSystemProperty( String key );

  public int getPropertyInt(String key, int defaultValue) {
    int result;
    String value = getProperty( key, null );
    if (value != null) {
      try {
        result = Integer.parseInt( value );
      }
      catch (Exception ex) {
        Logger.warn( "Error parsing value for property=" + key + ", value=" + value );
        result = defaultValue;
      }
    }
    else {
      result = defaultValue;
    }
    return result;
  }

  public boolean getPropertyBool(String key, boolean defaultValue) {
    boolean result;
    String value = getProperty( key, null );
    if (value != null) {
      try {
        result = Boolean.parseBoolean( value );
      }
      catch (Exception ex) {
        Logger.warn( "Error parsing value for property=" + key + ", value=" + value );
        result = defaultValue;
      }
    }
    else {
      result = defaultValue;
    }
    return result;
  }

  /**
   * Set a system wide application property. The system has to ensure properties are made persistent.
   *
   * @param key The key identifying this property.
   * @param value The value to be bound to that key.
   * @param save indicates if this property should be made persistent immediately or not.
   * @throws IOException If properties could not be saved in case <code>save</code> is
   *           set to <code>true</code>.
   */
  public abstract void setProperty( String key, String value, boolean save ) throws IOException;

  /**
   * Commit all application properties to some persistent storage, e.g. file system.
   *
   * @throws IOException If properties could not be saved.
   */
  public abstract void saveProperties() throws IOException;

  /**
   * Returns properties containing language specific properties
   * 
   * @param propFileName  base file name of properties
   * @param language      language short name, e.g. "de" or "en" or null for default language
   * @return properties or null if no properties existing for given propFileName and language
   */
  public abstract Properties getLanguageProps( String propFileName, String language );

  public Properties getGlossaryProps( String language ) {
    if (language == null) {
      language = Locale.getDefault().getLanguage();
    }
    if (!language.equals( glossaryLang )) {
      glossaryProps = getLanguageProps( "glossary", language );
      glossaryLang = language;
    }
    return glossaryProps;
  }

  public String getGlossaryString( String entryName ) {
    Properties glossaryProps = getGlossaryProps( null );
    String value = glossaryProps.getProperty( entryName );
    if (value == null) {
      return entryName;
    }
    else {
      return value;
    }
  }

  /**
   * Fetches localized message from language file "messages", then replaces parameters
   * enclosed like "{$PARAMNAME}". Finally replaces entries enclosed in "{GLOSSARYNAME}" by matching
   * entry from language file "glossary".
   *
   * @param messageID key for message
   * @param language  language or NULL for system default language
   * @param msgParams message parameters
   * @return message string
   */
  public String getLocalizedMessage( String messageID, String language, Properties msgParams ) {
    Properties msgProps = getLanguageProps( "messages", language );
    if (msgProps == null) {
      return messageID;
    }
    else {
      Properties glossaryProps = getGlossaryProps( language );
      String msg = msgProps.getProperty( messageID );
      if (msg == null) {
        msg = messageID;
      }
      else {
        msg = Helper.replaceParams( msg, "{$", "}", msgParams );
        msg = Helper.replaceParams( msg, "{", "}", glossaryProps );
      }
      return msg;
    }
  }

  /**
   * Perform some cleanup and exit runtime system.
   */
  public abstract void exit();

  protected void doExit() {
    try {
      saveProperties();
    }
    catch (IOException e) {
      Logger.error("Could not save properties", e);
    }
    if (connectionController != null && connectionController.isConnected()) {
      connectionController.disconnect();
    }
    instance = null;
  }

  public void loadFactories() {
    String factoryName;
    ModelFactory factory;
    int i = 0;

    factoryName = getProperty("factory_"+i, null );
    while (factoryName != null) {
      try {
        factory = (ModelFactory) Class.forName(factoryName).newInstance();
      }
      catch (Exception ex) {
        Logger.error("Could not create factory "+factoryName, ex);
      }
      i++;
      factoryName = getProperty("factory_"+i, null );
    }
  }

  /**
   * Returns a new Calendar instance for the current TimeZone, specified in application.properties, e.g:
   * timezone=Germany/Berlin
   * @return the Calendar instance
   */
  public abstract Calendar getCalendar();

  /**
   * Opens system mail application and creates a new mail
   *
   * @param mailAddr destination address, may be null
   * @param title    title, may be null
   * @param body     body, may be null
   * @param attachmentProviderList List of file owners providing files to be attached, may be null
   * @return true if platform supports mail and mail application could be invoked, but no guarantee for successful sending
   */
  public boolean sendMail( String mailAddr, String title, String body, List<FileOwner> attachmentProviderList ) {
    return false;
  }

  public void addReportFileOwner( FileOwner fileOwner ) {
    repotFileOwnerList.add( fileOwner );
  }

  public List<FileOwner> getReportFileOwners(){
    return repotFileOwnerList;
  }

  public Locale getLocale() {
    if (this.locale == null) {
      locale = Locale.getDefault();
    }
    return locale;
  }

  public void setLocale( Locale locale ) {
    Locale.setDefault( locale );
    this.locale = locale;
  }
}
