/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.base;

import de.pidata.qnames.Key;

import java.io.IOException;

public interface Sequence {

  /**
   * Returns a key made from next value in sequence and increments the sequence value
   *
   * @return the new key
   * @throws IllegalArgumentException if there is no next value in this sequence, i.e next is greater than max()
   */
  public Key nextKey() throws IOException;

  /**
   * Returns a percent value (between 100 and 0) indicating how many sequence values are left
   * @return percent values left in this sequence
   */
  public int percentLeft();
}
