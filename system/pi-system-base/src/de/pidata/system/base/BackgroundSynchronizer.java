/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.base;

import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

/**
 * Created by pru on 01.06.16.
 */
public abstract class BackgroundSynchronizer extends Thread {

  protected static BackgroundSynchronizer instance = null;

  public static BackgroundSynchronizer getInstance() {
    return instance;
  }

  public BackgroundSynchronizer() {
    if (instance != null) {
      throw new RuntimeException( "Changing SystemSynchronizer is not allowed." );
    }
    instance = this;
  }

  public abstract Model getStatusModel();

  /**
   * Synchronize data with server. This method uses the default
   * values for checking of pending data and request mode.
   */
  public abstract void synchronize();

  /**
   * Synchronize data with server.
   *
   * @param checkPendingData if true synchronize only if pending data is present
   */
  public abstract void synchronize( boolean checkPendingData );

  /**
   * Retunrs ID of attribute used to identify BackgroundSynchronizer's state within
   * model returned by getStatusModel()
   * 
   * @return state attribute ID
   */
  public abstract QName getStateAttrID();

  /**
   * Retruns true if stateValue is identifying an active state of state attribute.
   *
   * @param stateValue the state value to check
   * @return true for an active state
   */
  public abstract boolean isStateActive( QName stateValue );
}
