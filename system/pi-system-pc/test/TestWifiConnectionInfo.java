import de.pidata.log.Logger;
import de.pidata.models.json.JsonReader;
import de.pidata.models.tree.BaseFactory;
import de.pidata.models.tree.DefaultModel;
import de.pidata.models.tree.Model;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.system.desktop.DesktopSystem;
import de.pidata.system.models.PiSystemFactory;
import de.pidata.system.models.SPAirPortDataType;
import de.pidata.system.models.Spairport_airport_interfaces;
import de.pidata.system.models.Spairport_airport_other_local_wireless_networks;
import de.pidata.system.models.Spairport_current_network_information;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Collection;

public class TestWifiConnectionInfo {

  public static void main( String[] args ) {
    try {
      new DesktopSystem();
      new PiSystemFactory();

      JsonReader jsonReader = new JsonReader(true);

      // Execute command
      String command = "system_profiler SPAirPortDataType -json";
      Process p = Runtime.getRuntime().exec( command );
      try {
        p.waitFor();
      }
      catch (InterruptedException ex) {
        ex.printStackTrace();
      }

      InputStream inputStream = p.getInputStream();


      DefaultComplexType resultParentType = new DefaultComplexType( BaseFactory.NAMESPACE.getQName( "" + System.currentTimeMillis() ), DefaultModel.class.getName(), 0 );
      Type resultType =   PiSystemFactory.SPAIRPORTDATATYPE_TYPE;
      resultParentType.addRelation( resultType.name(), resultType, 0, Integer.MAX_VALUE );

      Model resultParent = new DefaultModel( null, resultParentType );
      jsonReader.loadDataList( inputStream, resultParent, resultType.name(), "UTF-8" );

      SPAirPortDataType spAirPortDataType =  (SPAirPortDataType) resultParent.get(  resultType.name(), null );
      Spairport_airport_interfaces airportInterface = spAirPortDataType.getSpairport_airport_interfaces();

      Collection<Spairport_airport_other_local_wireless_networks> wifiNetworks = airportInterface.getSpairport_airport_other_local_wireless_networkss();

      wifiNetworks.forEach( wifi -> {
        Logger.info( "Found WIFI: " + wifi.get_name());
      } );

      String spairportStatusInformation = airportInterface.getSpairport_status_information();
      String spairportWirelessMacAddress = airportInterface.getSpairport_wireless_mac_address();

      if (spairportStatusInformation.equals( "spairport_status_off" )) {
        Logger.warn( "Wifi Adapter is OFF" );
      }
      else if (spairportStatusInformation.equals( "spairport_status_connected" )) {
        Spairport_current_network_information spairportCurrentNetworkInformation = airportInterface.getSpairport_current_network_information();
        String ssid = spairportCurrentNetworkInformation.get_name();

        Logger.info( "Wifi Adapter is ON and connected to: " );
        Logger.info( " SSID = " + ssid );
        Logger.info( " BSSID = " + spairportWirelessMacAddress );
      }
      else {
        Logger.info( "Wifi Adapter is ON but not connected");
      }

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
