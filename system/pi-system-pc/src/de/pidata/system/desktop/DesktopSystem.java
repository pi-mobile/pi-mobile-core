/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.desktop;

import de.pidata.log.FileOwner;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;
import de.pidata.system.base.WifiManager;
import de.pidata.system.filebased.FilebasedStorage;
import de.pidata.system.linux.WifiManagerLinux;
import de.pidata.system.mac.WifiManagerMacOSX;
import de.pidata.system.windows.WifiManagerWindows;
import de.pidata.log.Logger;
import de.pidata.system.filebased.FilebasedSystem;

import java.awt.*;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.util.*;
import java.util.List;

/**
 * SystemManager for Desktop (1.1) use.
 */
public class DesktopSystem extends FilebasedSystem {

  protected static WifiManager wifiManager;

  public DesktopSystem() {
    this( "." );
  }

  public DesktopSystem( String basePath ) {
    super( basePath );
  }

  public DesktopSystem( String basePath, Properties systemProps, Properties appProps ) {
    super( basePath, systemProps, appProps );
  }

  /**
   * Returns a new Calendar instance for the current TimeZone, specified in application.properties, e.g:
   * timezone=Germany/Berlin
   *
   * DON'T USE LOGGING INSIDE THIS! Timestamp will produce endless loop.
   *
   * @return the Calendar instance
   */
  public Calendar getCalendar() {
    String timeZoneString = getProperty( "timezone", "Europe/Berlin" );
    String localeLanguage = getProperty("localeLanguage", "de");
    String localeCountry = getProperty("localeCountry", "DE");

    TimeZone timeZone = TimeZone.getTimeZone( timeZoneString );
    Locale locale = new Locale( localeLanguage, localeCountry );
    Calendar calendar = Calendar.getInstance( timeZone, locale );

    if (calendar == null) {
      Logger.warn( "-- No Calendar instance found for [" + timeZoneString + "/" + localeLanguage + "_" + localeCountry + "]" );
    }
    return calendar;
  }

  public Storage getStorage( String storageType, String storagePath ) {
    if (storageType.equals( SystemManager.STORAGE_APPDATA )) {
      String appdataPath = getPathAppData();
      if (storagePath != null) {
        appdataPath = appdataPath + '/' + storagePath;
      }
      return getStorage( appdataPath );
    }
    else if (storageType.equals( SystemManager.STORAGE_TEMP )) {
      String appdataPath = getPathTemp();
      if (storagePath != null) {
        appdataPath = appdataPath + '/' + storagePath;
      }
      return getStorage( appdataPath );
    }
    else if (storageType.equals( SystemManager.STORAGE_CLASSPATH )) {
      String path = null;
      try {
        // to not use getLocation().getFile() - contains e.g. "%20" for space
        path = getClass().getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
      }
      catch (URISyntaxException e) {
        Logger.error( "Error getting Class file path"+e.getMessage() );
        path = ".";
      }
      if (path.endsWith( ".jar" )) {
        path = new File( path ).getParent();
      }
      if (storagePath != null) {
        if (!FilebasedStorage.isPathSeparator( path.charAt( path.length()-1 ) )) {
          path = path + '/';
        }
        path = path + storagePath;
      }
      return getStorage( path );
    }
    else {
      return super.getStorage( storageType, storagePath );
    }
  }

  public static String getPathTemp() {
    String appdataPath;
    String osName = System.getProperty("os.name").toUpperCase();
    if (osName.contains("WIN")) {
      appdataPath = System.getenv( "TEMP" );
    }
    else if (osName.contains("MAC")) {
      appdataPath = System.getenv( "TMPDIR" );
    }
    else if (osName.contains("NUX")) {
      appdataPath = System.getProperty( "/tmp" );
    }
    else {
      appdataPath= System.getProperty("user.dir") + "/temp";
    }
    return appdataPath;
  }

  public static String getPathAppData() {
    String appdataPath;
    String osName = System.getProperty("os.name").toUpperCase();
    if (osName.contains("WIN")) {
      appdataPath = System.getenv( "APPDATA" );
    }
    else if (osName.contains("MAC")) {
      appdataPath = System.getProperty( "user.home" ) + "/Library/Application Support";
    }
    else if (osName.contains("NUX")) {
      appdataPath = System.getProperty( "user.home" );
    }
    else {
      appdataPath= System.getProperty("user.dir");
    }
    return appdataPath;
  }

  @Override
  public WifiManager getWifiManager() {
    if (wifiManager == null) {
      String platform = System.getProperty("os.name").toLowerCase();
      String vendor = System.getProperty( "java.vendor" ).toLowerCase();
      Logger.info( "Creating WifiManager for os.name='"+platform+"', java.vendor='"+vendor+"'" );
      if (platform.contains( "linux" )) {
        if (vendor.contains( "android" )) {
          wifiManager = null;
        }
        else {
          wifiManager = new WifiManagerLinux();
        }
      }
      else if (platform.contains( "win" )) {
        wifiManager = new WifiManagerWindows();
      }
      else if (platform.contains( "mac" )) {
        wifiManager = new WifiManagerMacOSX();
      }
      else {
        Logger.warn( "Unsupported platform='"+platform+"'" );
      }
    }
    return wifiManager;
  }

  @Override
  public boolean sendMail( String mailAddr, String title, String body, List<FileOwner> attachmentProviderList ) {
    try {
      StringBuilder builder = new StringBuilder( "mailto:" );
      if (mailAddr != null) {
        builder.append( mailAddr );
      }
      if (title != null) {
        builder.append( "?subject=" ).append( urlEncode( title ) );
      }
      builder.append( "&body=" );
      if (body != null) {
        builder.append( urlEncode( body + "\n" ) );
      }
      List<String> providerBasePaths = new ArrayList<>();

      if (attachmentProviderList != null) {
        for (FileOwner fileOwner : attachmentProviderList) {
          for (String path : fileOwner.getOwnedFiles()) {
            String basePath = path.substring( 0, path.lastIndexOf( FileSystems.getDefault().getSeparator() ) + 1 );
            if (!providerBasePaths.contains( basePath )) {
              providerBasePaths.add( basePath );
            }
            builder.append( urlEncode( path + "\n" )  );
          }
        }
      }
      String os = System.getProperty("os.name").toLowerCase();
      if (os.contains( "linux" )) {
        /*xdg-email - command line tool for sending mail using the user's preferred
          e-mail composer

          Synopsis

          xdg-email [--utf8] [--cc address] [--bcc address] [--subject text] [--body text
          ] [--attach file] [ mailto-uri | address(es) ]

          xdg-email { --help | --manual | --version }

          Use 'man xdg-email' or 'xdg-email --manual' for additional info.
          */
        throw new RuntimeException("TODO");
      }
      else {
        URI mailto = new URI( builder.toString()) ;
        // FIXME: This might cause an unhandled Exception due to missing assitive technologies (AWT) in JRE!!!
        Desktop.getDesktop().mail( mailto );

        // This will not add attachments to the mail client, though it is not possible due to security reasons.
        // Open a window which shows the attachment root(s).
        for (String providerBasePath : providerBasePaths) {
          File providerFile = new File(providerBasePath);
          if (providerFile.exists()) {
           Desktop.getDesktop().open( providerFile );
          }
        }
      }
      return true;
    }
    catch (Exception ex) {
      Logger.error( "Error calling mail application", ex );
      return false;
    }
  }

  private String urlEncode(String str) {
    try {
        return URLEncoder.encode(str, "UTF-8").replace("+", "%20");
    } catch (UnsupportedEncodingException e) {
        throw new RuntimeException(e);
    }
}


 //-------------------------------

  public static void main( String[] args ) {
    DesktopSystem desktopSystem = new DesktopSystem();
    desktopSystem.sendMail( "pit@pi-data.de", "Hallo", "Bitte anhängen:", desktopSystem.getReportFileOwners() );
  }
}
