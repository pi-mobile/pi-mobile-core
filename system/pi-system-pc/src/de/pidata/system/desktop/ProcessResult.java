/*
 * Copyright (c) 2014-2015 Daimler AG. All rights reserved.
 */
package de.pidata.system.desktop;

/**
 * Provides access to the result of a command run as system process.
 *
 * @author Doris Schwarzmaier
 */
public class ProcessResult {

  private int resultCode;
  private String outputString;
  private String errorString;

  public ProcessResult( int resultCode, String outputString, String errorString ) {
    this.resultCode = resultCode;
    this.outputString = outputString;
    this.errorString = errorString;
  }

  public int getResultCode() {
    return resultCode;
  }

  public String getOutputString() {
    return outputString;
  }

  public String getErrorString() {
    return errorString;
  }
}
