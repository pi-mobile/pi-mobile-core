/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.desktop;

import de.pidata.log.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper for invoking command line processes.
 *
 * This class must not be part of core code because iOS does not support command line.
 */
public class ProcessHelper {

  private static final boolean DEBUG = false;

  /**
   * TODO: provide {@link ProcessResult}!
   *
   * @param command
   * @param waitMillis
   * @return
   * @throws Exception
   */
  public static String runAndWaitForResult( String command, int waitMillis ) throws Exception {
    Logger.info( "Executing command: "+command );
    Process p = Runtime.getRuntime().exec( command );
    BufferedReader reader = new BufferedReader( new InputStreamReader( p.getInputStream() ) );
    BufferedReader errReader = new BufferedReader( new InputStreamReader( p.getInputStream() ) );
    try {
      p.waitFor();
      if (waitMillis > 0) {
        Thread.sleep( waitMillis );
      }
    }
    catch (InterruptedException ex) {
      // do nothing
    }
    StringBuilder builder = new StringBuilder();
    String line;
    while ((line = reader.readLine()) != null) {
      if (DEBUG) Logger.debug( "  I->"+line );
      builder.append( line );
    }
    while ((line = errReader.readLine()) != null) {
      if (DEBUG) Logger.debug( "  E->"+line );
      builder.append( line );
    }
    reader.close();
    errReader.close();
    if (DEBUG) Logger.debug( "finished exec" );
    return builder.toString();
  }

  public static List<String> runAndWaitForResultLines( String command ) throws Exception {
    Logger.info( "Executing command: "+command );
    Process p = Runtime.getRuntime().exec( command );
    BufferedReader reader = new BufferedReader( new InputStreamReader( p.getInputStream() ) );
    BufferedReader errReader = new BufferedReader( new InputStreamReader( p.getInputStream() ) );
    try {
      p.waitFor();
    }
    catch (InterruptedException ex) {
      // do nothing
    }
    List<String> result = new ArrayList<>();
    String line;
    while ((line = reader.readLine()) != null) {
      if (DEBUG) Logger.debug( "  I->"+line );
      result.add( line );
    }
    while ((line = reader.readLine()) != null) {
      if (DEBUG) Logger.debug( "  E->"+line );
      result.add( line );
    }
    reader.close();
    errReader.close();
    if (DEBUG) Logger.debug( "finished exec" );
    return result;
  }
}
