/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.windows;

import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;
import de.pidata.system.base.WifiConnectionInfo;
import de.pidata.system.base.WifiManager;
import de.pidata.log.Logger;
import de.pidata.stream.StreamHelper;
import de.pidata.system.desktop.ProcessHelper;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class WifiManagerWindows implements WifiManager {

  private static String wifiProfile = "<?xml version=\"1.0\"?>\n" +
      "<WLANProfile xmlns=\"http://www.microsoft.com/networking/WLAN/profile/v1\">\n" +
      "  <name>--SSID--</name>\n" +
      "  <SSIDConfig>\n" +
      "    <SSID>\n" +
      "      <name>--SSID--</name>\n" +
      "    </SSID>\n" +
      "  </SSIDConfig>\n" +
      "  <connectionType>ESS</connectionType>\n" +
      "  <connectionMode>manual</connectionMode>\n" +
      "  <MSM>\n" +
      "    <security>\n" +
      "      <authEncryption>\n" +
      "        <authentication>open</authentication>\n" +
      "        <encryption>none</encryption>\n" +
      "        <useOneX>false</useOneX>\n" +
      "      </authEncryption>\n" +
      "    </security>\n" +
      "  </MSM>\n" +
      "  <MacRandomization xmlns=\"http://www.microsoft.com/networking/WLAN/profile/v3\">\n" +
      "    <enableRandomization>false</enableRandomization>\n" +
      "  </MacRandomization>\n" +
      "</WLANProfile>\n";

  @Override
  public synchronized List<String> scanWiFi() {
    // SSID 3 : PI-Rail
    //     Network type            : Infrastructure
    //     Authentication          : WPA-Personal
    //     Encryption              : CCMP

    // disconnet then
    /* String disconnectCmd = "cmd /C netsh wlan disconnect";
    try {
      String response = runAndWaitForResult( disconnectCmd );
    }
    catch (Exception ex) {
      Logger.error( "Error disconnecting from wifi", ex );
      return null;
    }*/

    List<String> result = null;
    Storage assetStorage = SystemManager.getInstance().getStorage( SystemManager.STORAGE_CLASSPATH, null );
    String scanWlanExe = "WlanScan.exe";
    if (assetStorage.exists( scanWlanExe )) {
      String cmd = "cmd /C \"" + assetStorage.getPath( scanWlanExe ).replace( '/', '\\' ) + "\"";
      Logger.info( "cmd="+cmd );
      try {
        ProcessHelper.runAndWaitForResult( cmd, 1000 );
      }
      catch (Exception ex) {
        Logger.error( "Error running wifi scan", ex );
      }
    }
    else {
      Logger.warn( "WifiScan.exe not found, path=" + assetStorage.getPath( scanWlanExe ) );
    }
    String command = "cmd /C netsh wlan show networks";
    try {
      result = ProcessHelper.runAndWaitForResultLines( command );
    }
    catch (Exception ex) {
      Logger.error( "Error writing wifi profile", ex );
      return null;
    }

    List<String> wifiList = new ArrayList<String>();
    for (String line : result) {
      if (line.startsWith( "SSID" )) {
        int pos = line.indexOf( ":" );
        if (pos > 0) {
          String ssid = line.substring( pos + 1 ).trim();
          wifiList.add( ssid );
        }
      }
    }
    return wifiList;
  }

  @Override
  public void enableWifi() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public boolean checkConnectionState() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public synchronized String setActiveWifi( String ssid ) {
    // Profile anlegen für das Netz mit Hilfe einer XML-Datei, dann mit Profile verbinden
    // Siehe https://stackoverflow.com/questions/41163737/how-to-connect-to-a-wifi-using-cmd-only
    String profile = wifiProfile.replaceAll( "--SSID--", ssid );
    Storage tempStorage = SystemManager.getInstance().getStorage( SystemManager.STORAGE_TEMP, null );
    File profileFile = new File( tempStorage.getPath( "wifi_profile.xml" ) );
    OutputStream fileStream = null;
    OutputStreamWriter writer = null;
    try {
      fileStream = new FileOutputStream( profileFile );
      writer = new OutputStreamWriter( fileStream );
      writer.write( profile );
      writer.flush();
    }
    catch (Exception ex) {
      Logger.error( "Error writing wifi profile", ex );
      return "ERROR writing wifi profile: "+ex.getMessage();
    }
    finally {
      StreamHelper.close( writer );
      StreamHelper.close( fileStream );
    }

    String result;
    String profileCmd = "cmd /C netsh wlan add profile filename=\""+profileFile.getPath()+"\"";
    try {
      result = ProcessHelper.runAndWaitForResult( profileCmd, 1000 );
    }
    catch (Exception ex) {
      Logger.error( "Error registering wifi profile", ex );
      return "ERROR registering wifi profile: "+ex.getMessage();
    }

    String command = "cmd /C netsh wlan connect name=\""+ssid+"\"";
    try {
      result = ProcessHelper.runAndWaitForResult( command, 3000 );
    }
    catch (Exception ex) {
      Logger.error( "Error connecting to wifi SSID="+ssid, ex );
      return "ERROR connecting to wifi SSID="+ssid+", msg="+ex.getMessage();
    }

    return result;
  }

  @Override
  public WifiConnectionInfo getWiFiConnectionInfo() throws IOException {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  public URLConnection openWiFiConnection( URL url ) throws IOException {
    return url.openConnection();
  }
}
