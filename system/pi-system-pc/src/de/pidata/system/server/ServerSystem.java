/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.server;

import de.pidata.file.FilebasedRollingLogger;
import de.pidata.log.DefaultHandler;
import de.pidata.log.Level;
import de.pidata.log.Logger;
import de.pidata.system.desktop.DesktopSystem;

import java.io.File;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.LogManager;

/**
 * SystemManager for Desktop (1.1) use.
 */
public class ServerSystem extends DesktopSystem {

  public ServerSystem( String basePath ) {
    super( basePath );

    initLogging();
  }

  protected void initLogging() {
    String logfilePrefix = getApplicationProperty( KEY_LOGFILE );
    if ((logfilePrefix == null) || (logfilePrefix.length() == 0)) {
      System.out.println( "INFO: logfile not configured" );
      return;
    }
    String logfileName = getBasePath() + '/' + logfilePrefix;

    int logfileExpireDays = getPropertyInt( FilebasedRollingLogger.KEY_LOGFILE_EXPIREDAYS, 1 );
    int logfilePurgeDays = getPropertyInt( FilebasedRollingLogger.KEY_LOGFILE_PURGEDAYS, 0 );

    Level logLevel = Level.INFO;
    String logLevelProp = getApplicationProperty( KEY_LOGLEVEL );
    if (logLevelProp != null) {
      System.out.print( "loglevel=" + logLevelProp );
      logLevel = Level.fromName( logLevelProp );
      System.out.println( " (" + logLevel + ")" );
    }

    Logger.setLogger( new FilebasedRollingLogger( logfileName, logfileExpireDays, logfilePurgeDays, logLevel ) );

    // attach java.util.logging
    boolean attachJavaLogging = getPropertyBool( Logger.KEY_ATTACH_JAVA_LOGGING, false );
    if (attachJavaLogging) {
      java.util.logging.Logger rootLogger = LogManager.getLogManager().getLogger( "" );
      Handler[] handlers = rootLogger.getHandlers();
      // enable console logging via PI-Mobile handler
      for (Handler handler : handlers) {
        if (handler instanceof ConsoleHandler) {
          rootLogger.removeHandler( handler );
        }
      }
      rootLogger.addHandler( new DefaultHandler() );
    }
  }

  /**
   * Perform some cleanup and exit runtime system.
   */
  public void exit() {
    doExit();
  }

}
