/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.pda;

import de.pidata.system.base.WifiManager;
import de.pidata.log.Logger;
import de.pidata.system.base.SystemManager;
import de.pidata.system.filebased.FilebasedSystem;

import java.util.Calendar;
import java.util.Properties;
import java.util.TimeZone;

/**
 * SystemManager for PDA use.
 */
public class PDASystem extends FilebasedSystem {

  private static final boolean DEBUG = false;

  public PDASystem() {
    this( "." );
  }

  public PDASystem( String basePath ) {
    super( basePath );
  }

  public PDASystem( String basePath, Properties systemProps, Properties appProps ) {
    super( basePath, systemProps, appProps );
  }

  /**
     * Returns a new Calendar instance for the current TimeZone, specified in application.properties, e.g:
     * timezone=Germany/Berlin
     * @return the Calender instance
     */
  public Calendar getCalendar() {
    String timeZoneString = getProperty( "timezone", "Europe/Berlin" );

    TimeZone timeZone = TimeZone.getTimeZone( timeZoneString );
    Calendar calendar = Calendar.getInstance( timeZone );

    if (SystemManager.LOG_CALENDARINFO) {
      Logger.info( "----- available TimeZones" );
      String[] availableTimeZones = TimeZone.getAvailableIDs();
      for (int i = 0; i<availableTimeZones.length; i++) {
        Logger.info( availableTimeZones[i] );
      }
    }

    if (calendar == null) {
      Logger.warn( "No Calendar instance found for ["+timeZoneString+"]" );
    }
    else if (DEBUG) {
      Logger.info( "Use Calendar ["+calendar.toString()+"]" );
    }
    return calendar;
  }

  @Override
  public WifiManager getWifiManager() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }
}
