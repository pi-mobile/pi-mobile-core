/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.linux;

import de.pidata.messages.ErrorMessages;
import de.pidata.system.base.WifiConnectionInfo;
import de.pidata.system.base.WifiConnectionState;
import de.pidata.system.base.WifiManager;
import de.pidata.log.Logger;
import de.pidata.system.desktop.ProcessHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class WifiManagerLinux implements WifiManager {

  public synchronized List<String> scanWiFi() {
    ArrayList<String> networkList = new ArrayList<String>();
    try {
      // Execute command
      String command = "nmcli dev wifi";
      Process p = Runtime.getRuntime().exec( command );
      try {
        p.waitFor();
      }
      catch (InterruptedException ex) {
        ex.printStackTrace();
      }
      BufferedReader reader = new BufferedReader( new InputStreamReader( p.getInputStream() ) );
      String line;

      line = reader.readLine(); // skip table headline
      String[] headers = line.split( " +" );
      int[] pos = new int[headers.length];
      int ssidIndex = -1;
      for (int i = 0; i < headers.length; i++) {
        if ("SSID".equals( headers[i] )) {
          ssidIndex = i;
        }
      }
      while ((line = reader.readLine()) != null) {
        String[] data = line.split( " +" );
        String net = data[ssidIndex];
        if (!networkList.contains( net )) {
          networkList.add( net );
        }
        //System.out.println( "\"" + net + "\"" );
      }
    }
    catch (Exception e) {
      Logger.error( "Error scanning wiFi", e );
    }
    return networkList;
  }

  @Override
  public void enableWifi() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public boolean checkConnectionState() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  public synchronized String setActiveWifi( String ssid ) {
    try {
      String command = "nmcli dev wifi connect " + ssid;
      return ProcessHelper.runAndWaitForResult( command, 1000 );
    }
    catch (Exception ex) {
      Logger.error( "Error activating wiFi", ex );
      return ErrorMessages.ERROR + " "+ex.getMessage();
    }
  }

  @Override
  public WifiConnectionInfo getWiFiConnectionInfo() throws IOException {
    String ssid = null;
    String macAddr = null;
    int networkID = -1;
    int signal = 0;
    WifiConnectionState wifiConnectionState = WifiConnectionState.UNKNOWN;
    ArrayList<String> networkList = new ArrayList<String>();
    try {
      // Execute command
      String command = "nmcli dev wifi";
      Process p = Runtime.getRuntime().exec( command );
      try {
        p.waitFor();
      }
      catch (InterruptedException ex) {
        ex.printStackTrace();
      }
      BufferedReader reader = new BufferedReader( new InputStreamReader( p.getInputStream() ) );
      String line;

      line = reader.readLine(); // skip table headline
      String[] headers = line.split( " +" );
      int[] pos = new int[headers.length];
      for (int i = 1; i < headers.length; i++) {
        pos[i] = line.indexOf( headers[i], pos[i-1]+(headers[i-1].length()) );
      }
      wifiConnectionState = WifiConnectionState.DISABLED;
      while ((line = reader.readLine()) != null) {
        if (line.startsWith( "*" )) {
          wifiConnectionState = WifiConnectionState.CONNECTED;
          for (int i = 0; i < headers.length; i++) {
            if ("SSID".equals( headers[i] )) {
              ssid = line.substring( pos[i], pos[i+1] ).trim();
            }
            else if ("BSSID".equals( headers[i] )) {
              macAddr = line.substring( pos[i], pos[i+1] ).trim();
            }
            else if ("SIGNAL".equals( headers[i] )) {
              signal = Integer.parseInt( line.substring( pos[i], pos[i+1] ).trim() );
            }
          }
        }
        //System.out.println( "\"" + net + "\"" );
      }
    }
    catch (Exception e) {
      Logger.error( "Error scanning wiFi", e );
    }
    return new WifiConnectionInfo( wifiConnectionState, ssid, networkID, -1, -1, macAddr, signal );
  }

  public URLConnection openWiFiConnection( URL url ) throws IOException {
    return url.openConnection();
  }
}
