/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.linux;

import de.pidata.connect.base.ConnectionController;
import de.pidata.log.Logger;
import de.pidata.models.tree.Context;
import de.pidata.system.base.SystemManager;

import java.io.IOException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class LinuxConnectionController implements ConnectionController {

  private boolean hasMAC = false;
  private String clientID = null;

  public LinuxConnectionController() {
    // Following command returns serial but needs root rights: cat /sys/devices/virtual/dmi/id/product_uuid
    fetchMacAddress();
  }

  private void fetchMacAddress() {
    try {
      NetworkInterface ni = NetworkInterface.getByName( "eth0" );
      if (ni == null) {
        Enumeration niEnum = NetworkInterface.getNetworkInterfaces();
        if (niEnum.hasMoreElements()) {
          ni = (NetworkInterface) niEnum.nextElement();
        }
        else {
          return;
        }
      }
      byte[] mac =  ni.getHardwareAddress();
      StringBuffer idBuf
          = new StringBuffer( "MAC");
      for (int i = 0; i < mac.length; i++) {
        idBuf.append("-").append( hex( mac[i] ) );
      }
      clientID = idBuf.toString();
      SystemManager.getInstance().setClientID( clientID );
      hasMAC = true;
    }
    catch (SocketException ex) {
      Logger.error( "Error reading MAC address for eth0", ex );
    }
  }

  private String hex( byte b ) {
    String str = Integer.toHexString( b );
    int len = str.length();
    if (len > 2) str = str.substring( len-2 );
    else if (len == 1) str = "0" + str;
    else if (len == 0) str = "00";
    return str.toUpperCase();
  }

  @Override
  public String connect() throws IOException {
    if (hasMAC) {
    }
    else {
      fetchMacAddress();
    }
    if (hasMAC) {
      return this.clientID;
    }
    else {
      throw new IllegalArgumentException( "Cannot connect, no MAC-Adress" );
    }
  }

  public void disconnect() {
  }

  public boolean isConnected() {
    if (!hasMAC) {
      fetchMacAddress();
    }
    return hasMAC;
  }
}
