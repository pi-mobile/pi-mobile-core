/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.mac;

import de.pidata.log.Logger;
import de.pidata.messages.ErrorMessages;
import de.pidata.models.json.JsonReader;
import de.pidata.models.tree.BaseFactory;
import de.pidata.models.tree.DefaultModel;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.models.tree.SimpleKey;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.system.base.*;
import de.pidata.system.desktop.ProcessHelper;
import de.pidata.system.models.PiSystemFactory;
import de.pidata.system.models.SPAirPortDataType;
import de.pidata.system.models.Spairport_airport_interfaces;
import de.pidata.system.models.Spairport_airport_other_local_wireless_networks;
import de.pidata.system.models.Spairport_current_network_information;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class WifiManagerMacOSX implements WifiManager {

  private final JsonReader jsonReader;
  private WifiConnectionInfo wifiConnectionInfo;


  public WifiManagerMacOSX() {
    ModelFactoryTable.getInstance().getOrSetFactory( PiSystemFactory.NAMESPACE, PiSystemFactory.class );
    jsonReader = new JsonReader( true );
  }

  public synchronized List<String> scanWiFi() {

    ArrayList<String> networkList = new ArrayList<>();

    try {
      // Execute command
      String command = "system_profiler SPAirPortDataType -json";
      Process p = Runtime.getRuntime().exec(command);
      try {
        p.waitFor();
      }
      catch (InterruptedException ex) {
        ex.printStackTrace();
      }

      InputStream inputStream = p.getInputStream();

      try {

        DefaultComplexType resultParentType = new DefaultComplexType( BaseFactory.NAMESPACE.getQName( "" + System.currentTimeMillis() ), DefaultModel.class.getName(), 0 );
        Type resultType = PiSystemFactory.SPAIRPORTDATATYPE_TYPE;
        resultParentType.addRelation( resultType.name(), resultType, 0, Integer.MAX_VALUE );

        Model resultParent = new DefaultModel( null, resultParentType );
        jsonReader.loadDataList( inputStream, resultParent, resultType.name(), "UTF-8" );

        SPAirPortDataType spAirPortDataType = (SPAirPortDataType) resultParent.get( resultType.name(), null );
        Spairport_airport_interfaces airportInterface = spAirPortDataType.getSpairport_airport_interfaces();

        Collection<Spairport_airport_other_local_wireless_networks> wifiNetworks = airportInterface.getSpairport_airport_other_local_wireless_networkss();

        wifiNetworks.forEach( wifi -> {
          String SSID = wifi.get_name();
          if (!networkList.contains( SSID )) {
            networkList.add( SSID );
          }
        } );
      }
      catch (IOException e) {
        Logger.error( "Error reading SPAirPortDataType from json", e );
        throw e;
      }

    }
    catch (Exception e) {
      Logger.error( "Error scanning wiFi", e );
    }
    return networkList;
  }

  @Override
  public void enableWifi() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public boolean checkConnectionState() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public synchronized String setActiveWifi( String ssid ) {
    try {
      String command = "networksetup -setairportnetwork en0 " + ssid;
      return ProcessHelper.runAndWaitForResult( command, 8000 );
    }
    catch (Exception ex) {
      Logger.error( "Error activating wiFi", ex );
      return ErrorMessages.ERROR + " "+ex.getMessage();
    }
  }

  @Override
  public WifiConnectionInfo getWiFiConnectionInfo() throws IOException {

    // Execute command
    String command = "system_profiler SPAirPortDataType -json";
    Process p = Runtime.getRuntime().exec( command );
    try {
      p.waitFor();
    }
    catch (InterruptedException ex) {
      ex.printStackTrace();
    }

    InputStream inputStream = p.getInputStream();

    DefaultComplexType resultParentType = new DefaultComplexType( BaseFactory.NAMESPACE.getQName( "" + System.currentTimeMillis() ), DefaultModel.class.getName(), 0 );
    Type resultType = PiSystemFactory.SPAIRPORTDATATYPE_TYPE;
    resultParentType.addRelation( resultType.name(), resultType, 0, Integer.MAX_VALUE );

    Model resultParent = new DefaultModel( null, resultParentType );
    jsonReader.loadDataList( inputStream, resultParent, resultType.name(), "UTF-8" );

    SPAirPortDataType spAirPortDataType = (SPAirPortDataType) resultParent.get( resultType.name(), null );
    Spairport_airport_interfaces airportInterface = spAirPortDataType.getSpairport_airport_interfaces();

    String spairportStatusInformation = airportInterface.getSpairport_status_information();
    String spairportWirelessMacAddress = airportInterface.getSpairport_wireless_mac_address();

    if (spairportStatusInformation.equals( "spairport_status_off" )) {
      createConnectionInfo( WifiConnectionState.DISABLED, "", "" );
    }
    else if (spairportStatusInformation.equals( "spairport_status_connected" )) {
      Spairport_current_network_information spairportCurrentNetworkInformation = airportInterface.getSpairport_current_network_information();
      String ssid = spairportCurrentNetworkInformation.get_name();
      createConnectionInfo( WifiConnectionState.CONNECTED, ssid, spairportWirelessMacAddress );
    }
    else {
      createConnectionInfo( WifiConnectionState.UNKNOWN, "", "" );
    }

    return wifiConnectionInfo;
  }

  private void createConnectionInfo( WifiConnectionState state, String ssid, String bssid ) {
    if (wifiConnectionInfo == null) {
      wifiConnectionInfo = new WifiConnectionInfo( state, ssid, 0, 0, 0, bssid );
      return;
    }

    if (!wifiConnectionInfo.getConnectionState().equals( state ) ||
        !wifiConnectionInfo.getWifiSsid().equals( ssid ) ||
        !wifiConnectionInfo.getMacAddress().equals( bssid )) {
      wifiConnectionInfo = new WifiConnectionInfo( state, ssid, 0, 0, 0, bssid );
    }
  }

  public URLConnection openWiFiConnection( URL url ) throws IOException {
    return url.openConnection();
  }
}
