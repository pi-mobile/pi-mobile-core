/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.tools;

import de.pidata.log.Logger;
import de.pidata.models.tree.DynamicModelFactory;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.qnames.Namespace;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;
import de.pidata.system.desktop.DesktopSystem;

import java.io.IOException;
import java.io.InputStream;

/**
 * Generates XML Schema (.xsd) describing a given XML file.
 *
 * @see #printUsage()
 */
public class Xml2Schema {

  private static void printUsage() {
    Logger.info("usage: Xml2Schema <namespace> <filename> <schemafilename>");
  }

  public static void main(String[] args) {
    SystemManager system = new DesktopSystem();
    if (args.length != 3) {
      printUsage();
      System.exit(1);
    }
    InputStream in = null;
    try {
      Namespace namespace = Namespace.getInstance( args[0] );
      String schemaFileName = args[2];
      Storage storage = system.getStorage(null);
      String filename = args[1];
      in = storage.read(filename);
      if (in == null) {
        throw new IOException( "source not found: "+ filename);
      }
      DynamicModelFactory dynamicModelFactory = new DynamicModelFactory( namespace, schemaFileName, "1.0" );
      XmlReader xmlReader = new XmlReader();

      xmlReader.loadData( in, null, false, namespace );

      new Factory2Schema( dynamicModelFactory, schemaFileName );
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    finally {
      StreamHelper.close( in );
    }
  }

}
