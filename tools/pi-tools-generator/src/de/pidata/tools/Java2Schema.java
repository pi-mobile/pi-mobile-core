/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.tools;

import de.pidata.log.Logger;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.system.base.SystemManager;
import de.pidata.system.desktop.DesktopSystem;

/**
 * Generates XML Schema (.xsd) describing the model represented by a given Java model class file.
 *
 * @see #printUsage()
 */
public class Java2Schema {

  private static void printUsage() {
    Logger.info("usage: Java2Schema <namespace> <typename> <classname> <schemafilename>");
  }

  public static void main(String[] args) {
    SystemManager system = new DesktopSystem();
    if (args.length != 3) {
      printUsage();
      System.exit(1);
    }
    try {
      Namespace namespace = Namespace.getInstance( args[0] );
      QName typeName = namespace.getQName( args[1] );
      Class javaClass = Class.forName( args[2] );
      JavaType type = new JavaType( typeName, javaClass, true );
      new Factory2Schema( ModelFactoryTable.getInstance().getFactory( namespace ), args[3] );
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
