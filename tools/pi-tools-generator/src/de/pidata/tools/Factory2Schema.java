/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.tools;

import de.pidata.models.tree.BaseFactory;
import de.pidata.models.tree.ModelFactory;
import de.pidata.models.tree.QNameIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.StringType;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Generates an XML Schema file (.xsd) based on the given {@link ModelFactory}.
 * This typically is employed by a generator which is designed to provide a specific conversion task.
 */
public class Factory2Schema {

  private StringBuffer schemaBuffer = new StringBuffer();
  private ModelFactory factory;
  private int indent = 0;

  public Factory2Schema( ModelFactory factory, String fileName ) throws IOException {
    this.factory = factory;

    schemaBuffer.append( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" );
    schemaBuffer.append( "<xsd:schema id=\"ModelFactory\" targetNamespace=\"" ).append( factory.getTargetNamespace().getUri() ).append( "\"\n" );
    schemaBuffer.append( "    xmlns=\"" ).append( factory.getTargetNamespace().getUri() ).append( "\"\n" );
    schemaBuffer.append( "    xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n" );
    schemaBuffer.append( "    elementFormDefault=\"unqualified\" attributeFormDefault=\"unqualified\">\n" );
    schemaBuffer.append( "\n" );
    indent++;

    for (QNameIterator typeNameIter = factory.typeNames(); typeNameIter.hasNext(); ) {
      QName typeName = typeNameIter.next();
      Type type = factory.getType( typeName );
      if (type instanceof SimpleType) {
        addSimpleType( (SimpleType) type, typeName );
      }
      else {
        addComplexType( (ComplexType) type );
      }
      schemaBuffer.append( "\n" );
    }

    for (QNameIterator relNameIter = factory.relationNames(); relNameIter.hasNext(); ) {
      Relation rel = factory.getRootRelation( relNameIter.next() );
      addElement( rel, false );
      schemaBuffer.append( "\n" );
    }
    indent--;
    schemaBuffer.append( "</xsd:schema>\n" );

    writeToFile( schemaBuffer, fileName );
  }

  private void addIndent() {
    for (int i = 0; i < indent; i++) {
      schemaBuffer.append( "  " );
    }
  }

  private void addStringType( String typeName, StringType type ) {
    addIndent();
    schemaBuffer.append( "<xsd:simpleType" );
    if (typeName != null) {
      schemaBuffer.append( " name=\"" ).append( typeName ).append( "\"" );
    }
    schemaBuffer.append( ">\n" );
    indent++;
    addIndent();
    schemaBuffer.append( "<xsd:restriction base=\"xsd:string\">\n" );
    indent++;
    addIndent();
    schemaBuffer.append( "<xsd:minLength value=\"0\"/>\n" );
    addIndent();
    schemaBuffer.append( "<xsd:maxLength value=\"" ).append( type.getMaxLength() ).append( "\"/>\n" );
    indent--;
    addIndent();
    schemaBuffer.append( "</xsd:restriction>\n" );
    indent--;
    addIndent();
    schemaBuffer.append( "</xsd:simpleType>\n" );
    if (typeName != null) {
      schemaBuffer.append( "\n" );
    }
  }

  private void addSimpleType( SimpleType attrType, QName typeName ) {
    SimpleType baseType = (SimpleType) attrType.getBaseType();
    if (baseType instanceof StringType) {
      addStringType( typeName.getName(), (StringType) attrType );
    }
    else {
      throw new RuntimeException( "TODO" );
    }
  }

  private String getTypeName( Type type ) {
    QName typeName = type.name();
    if (typeName == null) {
      return null;
    }
    else if (typeName.getNamespace() == BaseFactory.NAMESPACE) {
      return "xsd:" + typeName.getName();
    }
    else if (typeName.getNamespace() == factory.getTargetNamespace()) {
      return typeName.getName();
    }
    else {
      return typeName.toString();
    }
  }

  private void addComplexType( ComplexType complexType ) {
    QName typeName = complexType.name();
    addIndent();
    schemaBuffer.append( "<xsd:complexType" );
    schemaBuffer.append( " name=\"" ).append( typeName.getName() ).append( "\"" );
    schemaBuffer.append( ">\n" );
    indent++;
    addChildren( complexType );
    addAttributes( complexType );
    indent--;
    addIndent();
    schemaBuffer.append( "</xsd:complexType>\n" );
  }

  private void addChildren( ComplexType complexType ) {
    if (complexType.relationCount() > 0) {
      addIndent();
      schemaBuffer.append( "<xsd:sequence>\n" );
      indent++;
      for (QNameIterator relnameIter = complexType.relationNames(); relnameIter.hasNext(); ) {
        addElement( complexType.getRelation( relnameIter.next() ), true );
      }
      indent--;
      addIndent();
      schemaBuffer.append( "</xsd:sequence>\n" );
    }
  }

  private void addAttributes( ComplexType complexType ) {
    for (int i = 0; i < complexType.attributeCount(); i++) {
      QName attrName = complexType.getAttributeName( i );
      if (attrName != null) {
        addIndent();
        SimpleType attrType = complexType.getAttributeType( i );
        schemaBuffer.append( "<xsd:attribute name=\"" ).append( attrName.getName() ).append( "\"" );
        String typeName = getTypeName( attrType );
        if (typeName != null) {
          schemaBuffer.append( " type=\"" ).append( typeName ).append( "\"/>\n" );
        }
        else {
          schemaBuffer.append( ">\n" );
          addSimpleType( attrType, null );
          schemaBuffer.append( "</xsd:attribute>\n" );
        }
      }
    }
  }

  private void addElement( Relation relation, boolean addOccurs ) {
    addIndent();
    schemaBuffer.append( "<xsd:element name=\"" + relation.getRelationID().getName() + "\" " );
    schemaBuffer.append( "type=\"" ).append( getTypeName( relation.getChildType() ) + "\"" );
    if (addOccurs) {
      schemaBuffer.append( " minOccurs=\"" + relation.getMinOccurs() + "\"" );
      int max = relation.getMaxOccurs();
      if (max == Integer.MAX_VALUE) {
        schemaBuffer.append( " maxOccurs=\"unbounded\"" );
      }
      else {
        schemaBuffer.append( " maxOccurs=\"" + relation.getMaxOccurs() + "\"" );
      }
    }
    schemaBuffer.append( "/>\n" );
  }

  /**
   * Write a stringBuffer to the given filePath
   *
   * @param data     the data StringBuffer
   * @param filePath destination for writing
   */
  private void writeToFile( StringBuffer data, String filePath )
      throws IOException {
    System.out.println( "Write to file: " + filePath );
    File file = new File( filePath );
    File parentDir = new File( new File( file.getAbsolutePath() ).getParent() );
    parentDir.mkdirs();
    FileWriter fw = new FileWriter( file );
    fw.write( data.toString() );
    fw.flush();
    fw.close();
  }
}
