/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.tools;

import de.pidata.qnames.Namespace;
import de.pidata.models.types.simple.StringType;
import de.pidata.models.types.Type;
import de.pidata.qnames.Key;
import de.pidata.models.tree.*;
import de.pidata.qnames.QName;
import java.util.Hashtable;

public class CodeGenFactory extends de.pidata.models.tree.AbstractModelFactory {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://schema.pidata.de/codegen" );

  public static final QName ID_COLLECTION = NAMESPACE.getQName( "collection" );
  public static final QName ID_IMPLEMENTATION = NAMESPACE.getQName( "implementation" );
  public static final QName ID_DEPRECATED = NAMESPACE.getQName( "deprecated" );
  public static final QName ID_DOC = NAMESPACE.getQName( "doc" );
  public static final QName ID_TYPE_CLASS = NAMESPACE.getQName( "typeClass" );

  public static final StringType TEXTTYPE = new StringType( NAMESPACE.getQName( "textType" ), 0, Integer.MAX_VALUE );

  public CodeGenFactory() {
    super( NAMESPACE, "http://schema.pidata.de/codegen", "3.0" );
    attributes = new Hashtable();
    attributes.put( ID_IMPLEMENTATION, StringType.getDefString());
    attributes.put( ID_COLLECTION, StringType.getDefString());
    attributes.put( ID_DEPRECATED, StringType.getDefString() );
    attributes.put( ID_DOC, StringType.getDefString() );
    attributes.put( ID_TYPE_CLASS, StringType.getDefString() );

    addType( TEXTTYPE );
  }

  public Model createInstance( Key key, Type typeDef, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    Class modelClass = typeDef.getValueClass();
    return super.createInstance( key, typeDef, attributes, anyAttribs, children );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
