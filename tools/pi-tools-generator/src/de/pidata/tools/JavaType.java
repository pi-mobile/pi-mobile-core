/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.tools;

import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.AnyElement;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.*;
import de.pidata.qnames.QName;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;

public class JavaType extends DefaultComplexType {

  private Class pojoClass;

  public JavaType( QName relationName, Class pojoClass, boolean createRootRelation ) {
    super( relationName.getNamespace().getQName( pojoClass.getName() ), DefaultModel.class.getName(), 0 );
    this.pojoClass = pojoClass;

    Namespace ns = relationName.getNamespace();
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( ns );
    if (factory == null) {
      factory = new DefaultModelFactory( ns, relationName.getName() + ".xsd", "1.0" );
    }
    factory.addType( this );
    if (createRootRelation) {
      factory.addRootRelation( relationName, this, 0, Integer.MAX_VALUE, null );
    }

    Method[] methods = pojoClass.getMethods();
    for (int i = 0; i < methods.length; i++) {
      String methodName = methods[i].getName();
      Class[] params = methods[i].getParameterTypes();
      if ((methods[i].getDeclaringClass() == pojoClass)
          && methodName.startsWith( "get" ) && (params.length == 0)) {
        String name = methodName.substring( 3, 4 ).toLowerCase() + methodName.substring( 4 );
        QName attrName = ns.getQName( name );
        if (factory.getType( attrName ) == null) {
          addAttrOrChild( attrName, methods[i].getReturnType() );
        }
      }
      else if ((methods[i].getDeclaringClass() == pojoClass) && (methodName.length() > 3)
               && methodName.startsWith( "add" ) && (params.length == 1)) {
        String name = methodName.substring( 3, 4 ).toLowerCase() + methodName.substring( 4 );
        QName childName = ns.getQName( name );
        if (factory.getType( childName ) == null) {
           if (methods[i].getParameterTypes()[0].equals(Object.class)) {
             addRelation( AnyElement.ANY_ELEMENT, AnyElement.ANY_TYPE, 0, 1 );
           }
           else {
             JavaType childJavaType = new JavaType( childName, methods[i].getParameterTypes()[0], false );
             addRelation( childName, childJavaType, 0, Integer.MAX_VALUE );
           }
        }
      }
    }
  }

  public Class getPojoClass() {
    return pojoClass;
  }

  private void addAttrOrChild( QName attrName, Class valueClass ) {
    Type childType = getSimpleType( valueClass );
    if (childType != null) {
      addAttributeType( attrName, (SimpleType) childType );
    }
    else if (valueClass.equals(Object.class)) {
      addRelation( AnyElement.ANY_ELEMENT, AnyElement.ANY_TYPE, 0, 1 );
    }
    else {
      JavaType childJavaType = new JavaType( attrName, valueClass, false );
      addRelation( attrName, childJavaType, 0, 1 );
    }
  }

  public static SimpleType getSimpleType( Class valueClass ) {
    String className = valueClass.getName();
    if (className.equals( "int" )) {
      return IntegerType.getDefInt();
    }
    if (className.equals( "short" )) {
      return IntegerType.getDefShort();
    }
    if (className.equals( "byte" )) {
      return IntegerType.getDefByte();
    }
    if (className.equals( "long" )) {
      return IntegerType.getDefLong();
    }
    if (className.equals( "boolean" )) {
      return BooleanType.getDefault();
    }
    if (className.equals( "char" )) {
      return StringType.getDefString();
    }
    if (BigDecimal.class.isAssignableFrom( valueClass )) {
      return DecimalType.getDefault();
    }
    if (Character.class.isAssignableFrom( valueClass )) {
      return StringType.getDefString();
    }
    if (String.class.isAssignableFrom( valueClass )) {
      return StringType.getDefString();
    }
    if (java.util.Date.class.isAssignableFrom( valueClass )) {
      return DateTimeType.getDefDateTime();
    }
    if (Float.class.isAssignableFrom( valueClass )) {
      return DecimalType.getDefault();
    }
    if (Double.class.isAssignableFrom( valueClass )) {
      return DecimalType.getDefDouble();
    }
    if (Byte.class.isAssignableFrom( valueClass )) {
      return IntegerType.getDefByte();
    }
    if (Short.class.isAssignableFrom( valueClass )) {
      return IntegerType.getDefShort();
    }
    if (Integer.class.isAssignableFrom( valueClass )) {
      return IntegerType.getDefInt();
    }
    if (Long.class.isAssignableFrom( valueClass )) {
      return IntegerType.getDefLong();
    }
    if (byte[].class.isAssignableFrom( valueClass )) {
      return BinaryType.base64Binary;
    }
    return null;
  }

  /**
   * Returns pojo's value of attribute name by calling correspondimg getter
   * via Reflection
   *
   * @param pojo the POJO containing the attribute value
   * @param name the attribute's name
   * @return the value attribute name
   */
  public static Object getAttribute( Object pojo, String name ) throws Exception {
    Method method;
    Class pojoClass = pojo.getClass();
    String methodName = "get" + name.substring( 0, 1 ).toUpperCase() + name.substring( 1 );
    Class[] params = new Class[0];
    try {
       method = pojoClass.getMethod( methodName, params );
    }
    catch (NoSuchMethodException ex) {
       methodName = "is" + name.substring( 0, 1 ).toUpperCase() + name.substring( 1 );
       method = pojoClass.getMethod( methodName, params );
    }
    return getAttribute( pojo, method );
  }

  public static Object getAttribute( Object pojo, Method method )
      throws IllegalAccessException, InvocationTargetException {
    Object value = method.invoke( pojo, null );

    value = convert4Model( value );
    return value;
  }

  public static Object convert4Model( Object value ) {
    // Spezialtypen Decimal, DateTime, ...
    if (value instanceof BigDecimal) {
      value = new DecimalObject( value.toString() );
    }
    else if (value instanceof BigInteger) {
      value = new DecimalObject( value.toString() );
    }
    else if (value instanceof Float) {
      value = new DecimalObject( value.toString() );
    }
    else if (value instanceof Double) {
      value = new DecimalObject( value.toString() );
    }
    else if (value instanceof java.util.Date) {
      value = new DateObject( DateTimeType.TYPE_DATETIME, ((java.util.Date) value).getTime() );
    }
    else if (value instanceof byte[]) {
      value = new BinaryBytes( (byte[]) value );
    }
    return value;
  }

  public static Iterator getIterator( Object pojo, String name ) throws Exception {
    Class pojoClass = pojo.getClass();
    String methodName = name.substring( 0, 1 ).toLowerCase() + name.substring( 1 ) + "Iterator";
    Class[] params = new Class[0];
    Method method;
    try {
      method = pojoClass.getMethod( methodName, params );
    }
    catch (NoSuchMethodException ex) {
       methodName = name.substring( 0, 1 ).toLowerCase() + name.substring( 1 ) + "Iter";
       method = pojoClass.getMethod( methodName, params );
    }
    Object value = method.invoke( pojo, null );
    return (Iterator) value;
  }

  public static Object convert4Pojo( Object value ) {
     // Spezialtypen Decimal, DateTime, ...
     if (value instanceof DateObject) {
       value = new java.util.Date( ((DateObject) value).getTime() );
     }
     else if (value instanceof DecimalObject) {
       //TODO prüfen, was der setter braucht: BigDecimal, BigInteger, Float, Double
       value = new BigDecimal( value.toString() );
     }
     else if (value instanceof BinaryBytes) {
       int size = ((Binary) value).size();
       byte[] bytes = ( (Binary) value ).getBytes();
       if (bytes.length != size) {
         value = new byte[size];
         System.arraycopy( bytes, 0, value, 0, size );
       }
       else {
         value = bytes;
       }
     }
     return value;
  }

  /**
   *
   * @param pojo
   * @param attrName Attribut-Name, wenn null wird erste setXXX( Object ) verwendet
   * @param value
   * @throws Exception
   */
  public static void setAttribute( Object pojo, String attrName, Object value ) throws Exception {
    if (value != null) {
      Method method = null;
      value = convert4Pojo( value );

      Class[] paramClasses = new Class[1];
      if (attrName == null) {
        paramClasses[0] = Object.class;
        method = findMethod( pojo, "set", paramClasses );
      }
      else {
        String methodName = "set" + attrName.substring( 0, 1 ).toUpperCase() + attrName.substring( 1 );
        paramClasses[0] = value.getClass();
        try {
          method = pojo.getClass().getMethod( methodName, paramClasses );
        }
        catch (NoSuchMethodException ex) {
          if (paramClasses[0].equals(Integer.class)) {
            paramClasses[0] = int.class;
          }
          else if (paramClasses[0].equals(Short.class)) {
            paramClasses[0] = short.class;
          }
          else if (paramClasses[0].equals(Long.class)) {
            paramClasses[0] = long.class;
          }
          else if (paramClasses[0].equals(Byte.class)) {
            paramClasses[0] = byte.class;
          }
          else if (paramClasses[0].equals(Boolean.class)) {
            paramClasses[0] = boolean.class;
          }
          else if (paramClasses[0].equals(Character.class)) {
            paramClasses[0] = char.class;
          }
          else {
            throw ex;
          }
          method = pojo.getClass().getMethod( methodName, paramClasses );
        }
      }
      Object[] params = new Object[1];
      params[0] = value;
      method.invoke( pojo, params );
    }
  }

  private static Method findMethod( Object pojo, String prefix, Class[] params )
  {
    Class[] objParam = new Class[1];
    Class pojoClass = pojo.getClass();
    Method[] allMethods = pojoClass.getMethods();
    for (int i = 0; i < allMethods.length; i++) {
      if ((allMethods[i].getDeclaringClass() == pojoClass)
          && allMethods[i].getName().startsWith( prefix ) ) {
        Class[] methodParams = allMethods[i].getParameterTypes();
        if (methodParams.length == params.length) {
           boolean ok = true;
           for (int j = 0; j < params.length; j++) {
              if (!methodParams[j].equals(params[j])) {
                 ok = false;
                 break;
              }
           }
           if (ok)return allMethods[i];
        }
      }
    }
    return null;
  }

  public static void addChild( Object pojo, String name, Object childPojo ) throws Exception {
    Class[] paramClasses = new Class[1];
    Method method;
    if (name == null) {
      paramClasses[0] = Object.class;
      method = findMethod( pojo, "add", paramClasses );
    }
    else {
      String methodName = "add" + name.substring( 0, 1 ).toUpperCase() + name.substring( 1 );
      paramClasses[0] = childPojo.getClass();
      try
      {
        method = pojo.getClass().getMethod( methodName, paramClasses );
      }
      catch (NoSuchMethodException ex)
      {
        methodName = "set" + name.substring( 0, 1 ).toUpperCase() + name.substring( 1 );
        method = pojo.getClass().getMethod( methodName, paramClasses );
      }
    }
    Object[] params = new Object[1];
    params[0] = childPojo;
    method.invoke( pojo, params );
  }

  /**
   * Fills model with the coresponding Values of the given pojo (calling get-Methods)
   *
   * @param pojo  the POJO containing the attribute values
   * @param model the model to be initialized with pojo's attributes
   */
  private static void copyValues( Object pojo, Model model ) {
    ComplexType type = (ComplexType) model.type();
    int attrCount = type.attributeCount();
    for (int i = 0; i < attrCount; i++) {
      QName attrName = type.getAttributeName( i );
      Object value;
      try {
        value = getAttribute( pojo, attrName.getName() );
      }
      catch (Exception e) {
        e.printStackTrace();
        throw new IllegalArgumentException( "Could not read attribute " + attrName + " from POJO " + pojo.getClass().getName() + ", msg=" + e.getMessage() );
      }
      model.set( attrName, value );
    }
  }

  /**
   * Fills pjo with the coresponding Values of the given model (calling set-Methods)
   *
   * @param model the model containing the attribute values
   * @param pojo  the POJO to be initialized with model's attributes
   */
  private static void copyValues( Model model, Object pojo ) {
    ComplexType type = (ComplexType) model.type();
    int attrCount = type.attributeCount();
    for (int i = 0; i < attrCount; i++) {
      QName attrName = type.getAttributeName( i );
      Object value = model.get( attrName );
      try {
        setAttribute( pojo, attrName.getName(), value );
      }
      catch (Exception e) {
        throw new IllegalArgumentException( "Could not write attribute " + attrName + " from POJO " + pojo.getClass().getName() + ", msg=" + e.getMessage() );
      }
    }
  }

  /**
   * Creates a DefaultModel having this JavaType and fills the created Model
   * with the coresponding Values of the given pojo (calling get-Methods)
   *
   * @param pojo the POJO containing the attribute values
   * @return a DefaultModel equivalent to pojo
   */
  public Model createModel( Object pojo ) {
    if (pojo == null) {
      return null;
    }
    if (!pojoClass.equals( pojo.getClass() )) {
      throw new IllegalArgumentException( "POJO must have same class than this JavaObject ("
          + pojoClass + ") but is " + pojo.getClass().getName() );
    }
    DefaultModel model = new DefaultModel( null, this );
    copyValues( pojo, model );
    return model;
  }

  /**
   * Creates a new Model instance for the given type and initializes that Model
   * with the coresponding Values of the given pojo (calling get-Methods).
   * The pojo must have a getXXX methode for each attribute defined by type.
   *
   * @param pojo the POJO containing the attribute values
   * @param type the type of the Model to be returned
   * @return a Model equivalent to pojo
   */
  public static Model createModel( Object pojo, Type type ) {
    if (pojo == null) {
      return null;
    }
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( type.name().getNamespace() );
    if (type instanceof SimpleType) {
      SimpleModel model = new SimpleModel( type, convert4Model(pojo) );
      return model;
    }
    else {
      Model model = factory.createInstance( (ComplexType) type );
      copyValues( pojo, model );
      return model;
    }
  }

  public static Object createObject( Model model, Class pojoClass ) {
    Object pojo;
    if (model == null) {
       return null;
    }
    try {
      if ( model instanceof SimpleModel ) {
        pojo = convert4Pojo( model.getContent() );
      }
      else {
        pojo = pojoClass.newInstance();
        copyValues( model, pojo );
      }
    }
    catch (Exception e) {
      throw new IllegalArgumentException( "Could not create pojo class=" + pojoClass.getName() + ", msg=" + e.getMessage() );
    }
    return pojo;
  }
}
