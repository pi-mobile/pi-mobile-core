/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;

import java.util.Hashtable;

public class ExtensionType extends de.pidata.models.xml.schema.Annotated {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_BASE = NAMESPACE.getQName("base");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "extensionType" ), ExtensionType.class.getName(), 0, de.pidata.models.xml.schema.Annotated.TYPE );
    TYPE = type;
  }

  static void buildType() {
    DefaultComplexType type = (DefaultComplexType) TYPE;
    type.addAttributeType( ID_BASE, QNameType.getInstance());
    type.addRelation( TypeDefParticle.ID_GROUP, GroupRef.TYPE, 0, 1);
    type.addRelation( TypeDefParticle.ID_ALL, All.TYPE, 0, 1);
    type.addRelation( TypeDefParticle.ID_CHOICE, ExplicitGroup.TYPE, 0, 1);
    type.addRelation( TypeDefParticle.ID_SEQUENCE, ExplicitGroup.TYPE, 0, 1);
    type.addRelation( AttrDecls.ID_ATTRIBUTE, Attribute.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( AttrDecls.ID_ATTRIBUTEGROUP, AttributeGroupRef.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( AttrDecls.ID_ANYATTRIBUTE, Wildcard.TYPE, 0, 1);
  }

  private AttrDecls attrDecls;
  private TypeDefParticle typeDefParticle;

  public ExtensionType() {
    this( null );
    init();
  }

  public ExtensionType( QName id ) {
    super( id, TYPE, null, null, null );
    init();
  }

  public ExtensionType( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
    init();
  }

  protected ExtensionType( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
    init();
  }

  /**
   * Query the <code>base</code> attribute.
   *
   * @return The value of the base attribute.
   */
  public QName getBase() {
    return (QName) get( ID_BASE );
  }

  /**
   * Set the <code>base</code> attribute.
   *
   * @param _base Value to set for base
   */
  public void setBase( QName _base ) {
    set( ID_BASE, _base );
  }

  //==============================================================
  // End of generated code. checksum=
  //==============================================================

  private void init() {
    this.attrDecls = new AttrDecls(this, this.children);
    this.typeDefParticle = new TypeDefParticle(this, this.children);
  }

  public AttrDecls getAttrDecls() {
    return attrDecls;
  }

  public TypeDefParticle getTypeDefParticle() {
    return typeDefParticle;
  }

}
