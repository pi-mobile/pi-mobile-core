/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;

import java.util.Hashtable;

public class SimpleExtensionType extends de.pidata.models.xml.schema.ExtensionType {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_ATTRIBUTEGROUP = NAMESPACE.getQName("attributeGroup");
  public static final QName ID_ATTRIBUTE = NAMESPACE.getQName("attribute");
  public static final QName ID_ANYATTRIBUTE = NAMESPACE.getQName("anyAttribute");
  public static final QName ID_ANNOTATION = NAMESPACE.getQName("annotation");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "simpleExtensionType" ), SimpleExtensionType.class.getName(), 0, de.pidata.models.xml.schema.ExtensionType.TYPE );
    TYPE = type;
    type.addAttributeType( AnyAttribute.ANY_ATTRIBUTE, AnyAttribute.ANY_ATTR_TYPE );
    type.addRelation( ID_ANNOTATION, Annotation.TYPE, 0, 1);
    type.addRelation( ID_ATTRIBUTE, Attribute.TYPE, 1, 1);
    type.addRelation( ID_ATTRIBUTEGROUP, AttributeGroupRef.TYPE, 1, 1);
    type.addRelation( ID_ANYATTRIBUTE, Wildcard.TYPE, 1, 1);
  }

  public SimpleExtensionType() {
    this( null );
  }

  public SimpleExtensionType( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public SimpleExtensionType( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected SimpleExtensionType( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }


  /**
   * Query the <code>annotation</code> attribute.
   *
   * @return The value of the annotation attribute.
   */
  public Annotation getAnnotation() {
    int count = childCount( ID_ANNOTATION );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_ANNOTATION, null );
    return (Annotation) it.next();
  }

  /**
   * Set the <code>annotation</code> attribute.
   *
   * @param _annotation Value to set for annotation
   */
  public void setAnnotation( Annotation _annotation ) {
    int count = childCount( ID_ANNOTATION );
    if (count > 0) {
      removeAll( ID_ANNOTATION );
    }
    add( ID_ANNOTATION, _annotation );
  }

  /**
   * Query the <code>attribute</code> attribute.
   *
   * @return The value of the attribute attribute.
   */
  public Attribute getAttribute() {
    int count = childCount( ID_ATTRIBUTE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_ATTRIBUTE, null );
    return (Attribute) it.next();
  }

  /**
   * Set the <code>attribute</code> attribute.
   *
   * @param _attribute Value to set for attribute
   */
  public void setAttribute( Attribute _attribute ) {
    int count = childCount( ID_ATTRIBUTE );
    if (count > 0) {
      removeAll( ID_ATTRIBUTE );
    }
    add( ID_ATTRIBUTE, _attribute );
  }

  /**
   * Query the <code>attributeGroup</code> attribute.
   *
   * @return The value of the attributeGroup attribute.
   */
  public AttributeGroupRef getAttributeGroup() {
    int count = childCount( ID_ATTRIBUTEGROUP );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_ATTRIBUTEGROUP, null );
    return (AttributeGroupRef) it.next();
  }

  /**
   * Set the <code>attributeGroup</code> attribute.
   *
   * @param _attributeGroup Value to set for attributeGroup
   */
  public void setAttributeGroup( AttributeGroupRef _attributeGroup ) {
    int count = childCount( ID_ATTRIBUTEGROUP );
    if (count > 0) {
      removeAll( ID_ATTRIBUTEGROUP );
    }
    add( ID_ATTRIBUTEGROUP, _attributeGroup );
  }

  /**
   * Query the <code>anyAttribute</code> attribute.
   *
   * @return The value of the anyAttribute attribute.
   */
  public Wildcard getAnyAttribute() {
    int count = childCount( ID_ANYATTRIBUTE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_ANYATTRIBUTE, null );
    return (Wildcard) it.next();
  }

  /**
   * Set the <code>anyAttribute</code> attribute.
   *
   * @param _anyAttribute Value to set for anyAttribute
   */
  public void setAnyAttribute( Wildcard _anyAttribute ) {
    int count = childCount( ID_ANYATTRIBUTE );
    if (count > 0) {
      removeAll( ID_ANYATTRIBUTE );
    }
    add( ID_ANYATTRIBUTE, _anyAttribute );
  }
}
