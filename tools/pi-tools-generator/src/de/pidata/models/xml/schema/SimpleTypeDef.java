/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.BaseFactory;
import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.tree.ValidationException;
import de.pidata.models.types.*;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.NamespaceTable;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.AbstractSimpleType;
import de.pidata.models.types.simple.DefaultValueEnum;
import de.pidata.qnames.QName;
import de.pidata.tools.CodeGenFactory;

import java.util.Hashtable;

public abstract class SimpleTypeDef extends de.pidata.models.xml.schema.Annotated implements de.pidata.models.types.SimpleType {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_FINAL = NAMESPACE.getQName("final");
  public static final QName ID_LIST = NAMESPACE.getQName("list");
  public static final QName ID_RESTRICTION = NAMESPACE.getQName("restriction");
  public static final QName ID_UNION = NAMESPACE.getQName("union");


  public static final ComplexType TYPE;

  protected ValueEnum valueEnum;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "simpleType" ), SimpleTypeDef.class.getName(), 0, de.pidata.models.xml.schema.Annotated.TYPE );
    TYPE = type;
  }

  static void buildType() {
    DefaultComplexType type = (DefaultComplexType) TYPE;
    type.addAttributeType( ID_FINAL, SchemaFactory.SIMPLEDERIVATIONSET);
    type.addRelation( ID_RESTRICTION, RestrictionDef.TYPE, 1, 1);
    type.addRelation( ID_LIST, List.TYPE, 1, 1);
    type.addRelation( ID_UNION, Union.TYPE, 1, 1);
  }

  public SimpleTypeDef() {
    this( null );
  }

  public SimpleTypeDef( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public SimpleTypeDef( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected SimpleTypeDef( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  /**
   * Query the <code>final</code> attribute.
   *
   * @return The value of the final attribute.
   */
  public String getFinal() {
    return (String) get( ID_FINAL );
  }

  /**
   * Set the <code>final</code> attribute.
   *
   * @param _final Value to set for final
   */
  public void setFinal( String _final ) {
    set( ID_FINAL, _final );
  }

  /**
   * Query the <code>restriction</code> attribute.
   *
   * @return The value of the restriction attribute.
   */
  public RestrictionDef getRestriction() {
    int count = childCount( ID_RESTRICTION );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_RESTRICTION, null );
    return (RestrictionDef) it.next();
  }

  /**
   * Set the <code>restriction</code> attribute.
   *
   * @param _restrictionDef Value to set for restriction
   */
  public void setRestriction( RestrictionDef _restrictionDef ) {
    int count = childCount( ID_RESTRICTION );
    if (count > 0) {
      removeAll( ID_RESTRICTION );
    }
    add( ID_RESTRICTION, _restrictionDef );
  }

  /**
   * Query the <code>list</code> attribute.
   *
   * @return The value of the list attribute.
   */
  public List getList() {
    int count = childCount( ID_LIST );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_LIST, null );
    return (List) it.next();
  }

  /**
   * Set the <code>list</code> attribute.
   *
   * @param _list Value to set for list
   */
  public void setList( List _list ) {
    int count = childCount( ID_LIST );
    if (count > 0) {
      removeAll( ID_LIST );
    }
    add( ID_LIST, _list );
  }

  /**
   * Query the <code>union</code> attribute.
   *
   * @return The value of the union attribute.
   */
  public Union getUnion() {
    int count = childCount( ID_UNION );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_UNION, null );
    return (Union) it.next();
  }

  /**
   * Set the <code>union</code> attribute.
   *
   * @param _union Value to set for union
   */
  public void setUnion( Union _union ) {
    int count = childCount( ID_UNION );
    if (count > 0) {
      removeAll( ID_UNION );
    }
    add( ID_UNION, _union );
  }

  //==============================================================
  // End of generated code. checksum=
  //==============================================================

  /**
   * Returns this simple type's root type by recursively calling getBaseType().
   *
   * @return this simple type's root type
   */
  public SimpleType getRootType() {
    Type baseType = getBaseType();
    while (baseType.getBaseType() != null) {
      baseType = baseType.getBaseType();
    }
    return (SimpleType) baseType;
  }

  /**
   * Creates a value for this type by parsing the given stringValue
   *
   * @param stringValue string representation of the value to be created, may be null
   * @param namespaces
   * @return a value for this type by parsing the given stringValue
   */
  public Object createValue( String stringValue, NamespaceTable namespaces ) {
    return ((SimpleType) getBaseType()).createValue(stringValue, namespaces );
  }

  /**
   * Creates a default value for this type.
   *
   * @return a default value for this type
   */
  public Object createDefaultValue() {
    //TODO
    return null;
  }

  /**
   * Returns the message for the given errorID
   *
   * @param errorID the ID of the error for the requested message
   * @return the message for the given errorID
   */
  public String getErrorMessage(int errorID) {
    return AbstractSimpleType.createErrorMessage(errorID, this);
  }

  /**
   * Returns the Class to be used for values conforming to this Type
   *
   * @return the Class to be used for values conforming to this Type
   */
  public Class getValueClass() {
    String implementation = (String) get( CodeGenFactory.ID_IMPLEMENTATION );
    if ((implementation == null) || (implementation.length() == 0)) {
      return getBaseType().getValueClass();
    }
    else {
      try {
        return Class.forName( implementation );
      }
      catch (ClassNotFoundException e) {
        throw new IllegalArgumentException( "Implementation class not found: "+implementation );
      }
    }
  }

  /**
   * Checks if value is valid for this type.
   *
   * @param value the value to be checked
   * @return NO_ERROR if value is valid, otherwise the errorID
   */
  public int checkValid(Object value) {
    if ((value == null) || (getValueClass().isAssignableFrom(value.getClass()))) {
      return ValidationException.NO_ERROR;
    }
    else {
      return ValidationException.ERROR_WRONG_CLASS;
    }
  }

  /**
   * Converts value to be compatible with this type. Use this method if checkValid returned
   * ValidationException.NEEDS_CONVERSION
   *
   * @param value the value to be converted
   * @return the converted value
   * @throws IllegalArgumentException if conversion is not possible
   */
  public Object convert( Object value ) {
    int valid = checkValid(value);
    if ((valid == ValidationException.NO_ERROR) || (valid == ValidationException.NEEDS_CONVERSION)) {
      return value;
    }
    else {
      throw new IllegalArgumentException( "Value='"+value+"' cannot be converted for type="+this );
    }
  }

  /**
   * Returns true if this type is the same or a base type (recursive)
   * of childType
   *
   * @param childType the type to check assignability
   * @return true if instances of childType can be assigned to fields of this type
   */
  public boolean isAssignableFrom(Type childType) {
    QName typeID = name();
    if (childType.name() == typeID) {
      return true;
    }
    Type baseType = childType.getBaseType();
    while (baseType != null) {
      if (baseType.name() == typeID) {
        return true;
      }
      baseType = baseType.getBaseType();
    }
    return false;
  }

  /**
   * Returns this type's value enumeration or null
   *
   * @return this type's value enumeration or null
   */
  public ValueEnum getValueEnum() {
    if (this.valueEnum == null) {
      Annotation annotation = getAnnotation();
      if (annotation != null) {
        Appinfo appinfo = annotation.getAppinfo();
        if (appinfo != null) {
          String path = (String) appinfo.get(BaseFactory.ID_PATH);
          if (path != null) {
            QName relation = (QName) appinfo.get(BaseFactory.ID_RELATION);
            QName valueName = (QName) appinfo.get(BaseFactory.ID_DISPLAY_ATTRIBUTE);
            this.valueEnum = new DefaultValueEnum(path, relation, valueName);
          }
        }
      }
    }
    return this.valueEnum;
  }

  /**
   * Returns true if this type represents an enum type; false otherwise.
   * @return true if this type represents an enum type; false otherwise
   */
  public boolean isEnumDef() {
    RestrictionDef restriction = getRestriction();
    if (restriction != null) {
      return (restriction.enumerationCount() > 0);
    }
    return false;
  }

  @Override
  public int hashCode() {
    return name().hashCode();
  }

  @Override
  public boolean equals( Object obj ) {
    if (obj == null) {
      return false;
    }
    else if (obj instanceof SimpleTypeDef) {
      if (this.name() == null) {
        return false; // TODO?
      }
      SimpleTypeDef other = (SimpleTypeDef) obj;
      return this.name() == other.name();
    }
    else {
      return false;
    }
  }

}
