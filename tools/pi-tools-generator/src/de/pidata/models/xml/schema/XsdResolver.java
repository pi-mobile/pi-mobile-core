package de.pidata.models.xml.schema;

import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

public interface XsdResolver {
  SimpleType getAttribute( QName attrName );

  Attribute getAttributeDef( QName attrName, boolean climbUp );

  AttributeGroup getAttributeGroupDef( QName attrGroupName, boolean climbUp );

  Group getGroupDef( QName groupName, boolean climbUp );

  Type getType( QName typeName );

  Element getElement( QName elementName );
}
