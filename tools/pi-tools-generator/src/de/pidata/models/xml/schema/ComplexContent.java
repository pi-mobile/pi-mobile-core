/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.BooleanType;
import de.pidata.qnames.QName;

import java.util.Hashtable;

public class ComplexContent extends de.pidata.models.xml.schema.Annotated {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_EXTENSION = NAMESPACE.getQName("extension");
  public static final QName ID_RESTRICTION = NAMESPACE.getQName("restriction");
  public static final QName ID_MIXED = NAMESPACE.getQName("mixed");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "complexContent" ), ComplexContent.class.getName(), 0, de.pidata.models.xml.schema.Annotated.TYPE );
    TYPE = type;
    type.addAttributeType( ID_MIXED, BooleanType.getDefault());
    type.addRelation( ID_RESTRICTION, ComplexRestrictionType.TYPE, 1, 1);
    type.addRelation( ID_EXTENSION, ExtensionType.TYPE, 1, 1);
  }

  public ComplexContent() {
    this( null );
  }

  public ComplexContent( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public ComplexContent( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected ComplexContent( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  /**
   * Query the <code>mixed</code> attribute.
   *
   * @return The value of the mixed attribute.
   */
  public Boolean getMixed() {
    return (Boolean) get( ID_MIXED );
  }

  /**
   * Set the <code>mixed</code> attribute.
   *
   * @param _mixed Value to set for mixed
   */
  public void setMixed( Boolean _mixed ) {
    set( ID_MIXED, _mixed );
  }

  /**
   * Query the <code>restriction</code> attribute.
   *
   * @return The value of the restriction attribute.
   */
  public ComplexRestrictionType getRestriction() {
    int count = childCount( ID_RESTRICTION );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_RESTRICTION, null );
    return (ComplexRestrictionType) it.next();
  }

  /**
   * Set the <code>restriction</code> attribute.
   *
   * @param _restriction Value to set for restriction
   */
  public void setRestriction( ComplexRestrictionType _restriction ) {
    int count = childCount( ID_RESTRICTION );
    if (count > 0) {
      removeAll( ID_RESTRICTION );
    }
    add( ID_RESTRICTION, _restriction );
  }

  /**
   * Query the <code>extension</code> attribute.
   *
   * @return The value of the extension attribute.
   */
  public ExtensionType getExtension() {
    int count = childCount( ID_EXTENSION );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_EXTENSION, null );
    return (ExtensionType) it.next();
  }

  /**
   * Set the <code>extension</code> attribute.
   *
   * @param _extension Value to set for extension
   */
  public void setExtension( ExtensionType _extension ) {
    int count = childCount( ID_EXTENSION );
    if (count > 0) {
      removeAll( ID_EXTENSION );
    }
    add( ID_EXTENSION, _extension );
  }
}
