/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.AnyElement;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.StringType;

import java.util.Hashtable;

/**
 *
 * @implNote The current XSD implementation does not support mixed content.
 * As a replacement we define an element within the codegen namespace which holds simple content. So
 * a documentation element which can be processed by the {@link de.pidata.models.xml.binder.XmlReader} may look like this:
 * <pre>
 *   ...<br>
 *   xmlns:pi="http://schema.pidata.de/codegen"<br>
 *   ...<br>
 *   &lt;documentation&gt;<br>
 *     &lt;pi:p&gt;Here comes the text.&lt;/pi:p&gt;<br>
 *   &lt;/documentation&gt;
 * </pre>
 */
public class Documentation extends SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );
  public static final Namespace NAMESPACE_PI = Namespace.getInstance( "http://schema.pidata.de/codegen" );

  public static final QName ID_SOURCE = NAMESPACE.getQName("source");
  public static final QName ID_LANG = NAMESPACE.getQName("lang");
  public static final QName ID_P = NAMESPACE_PI.getQName("p");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "documentation" ), Documentation.class.getName(), 0);
    TYPE = type;
    type.addAttributeType( ID_SOURCE, StringType.getDefString());
    type.addAttributeType( ID_LANG, StringType.getDefString());
    type.addAttributeType( AnyAttribute.ANY_ATTRIBUTE, AnyAttribute.ANY_ATTR_TYPE );
    type.addRelation( AnyElement.ANY_ELEMENT, AnyElement.ANY_TYPE, 1, 1);

    // helper relation for mixed content
    type.addRelation( ID_P, StringType.getDefString(), 0, Integer.MAX_VALUE );
  }

  public Documentation() {
    this( null );
  }

  public Documentation( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public Documentation( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected Documentation( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  /**
   * Query the <code>source</code> attribute.
   *
   * @return The value of the source attribute.
   */
  public String getSource() {
    return (String) get( ID_SOURCE );
  }

  /**
   * Set the <code>source</code> attribute.
   *
   * @param _source Value to set for source
   */
  public void setSource( String _source ) {
    set( ID_SOURCE, _source );
  }

  /**
   * Query the <code>lang</code> attribute.
   *
   * @return The value of the lang attribute.
   */
  public String getLang() {
    return (String) get( ID_LANG );
  }

  /**
   * Set the <code>lang</code> attribute.
   *
   * @param _lang Value to set for lang
   */
  public void setLang( String _lang ) {
    set( ID_LANG, _lang );
  }


  //--------------------------------------------------------------------------------
  // The following methods simulate the content as mixed is not supported by the XmlReader;
  // see Javadoc.
  //--------------------------------------------------------------------------------

  public String getParagraph( Key commentID ) {
    Model m = get( ID_P, commentID );
    if (m == null) return null;
    else return (String) m.getContent();
  }

  public void addCParagraph( String paragraph ) {
    add( ID_P, new SimpleModel( new StringType( NAMESPACE_PI.getQName("textType"), StringType.getDefString(), 0, Integer.MAX_VALUE ), paragraph ) );
  }

  public void removeParagraph( SimpleModel paragraph ) {
    remove( ID_P, paragraph );
  }

  public ModelIterator<SimpleModel> paragraphIter() {
    return iterator( ID_P, null );
  }

  public int paragraphCount() {
    return childCount( ID_P );
  }

}
