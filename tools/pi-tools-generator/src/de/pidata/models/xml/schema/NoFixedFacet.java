/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.StringType;

import java.util.Hashtable;

public class NoFixedFacet extends de.pidata.models.xml.schema.Facet {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_VALUE = NAMESPACE.getQName("value");
  public static final QName ID_ANNOTATION = NAMESPACE.getQName("annotation");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "noFixedFacet" ), NoFixedFacet.class.getName(), 0, de.pidata.models.xml.schema.Facet.TYPE );
    TYPE = type;
    type.addAttributeType( ID_VALUE, StringType.getDefString());
    type.addAttributeType( AnyAttribute.ANY_ATTRIBUTE, AnyAttribute.ANY_ATTR_TYPE );
    type.addRelation( ID_ANNOTATION, Annotation.TYPE, 0, 1);
  }

  public NoFixedFacet() {
    this( null );
  }

  public NoFixedFacet( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public NoFixedFacet( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected NoFixedFacet( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }


  /**
   * Query the <code>value</code> attribute.
   *
   * @return The value of the value attribute.
   */
  public String getValue() {
    return (String) get( ID_VALUE );
  }

  /**
   * Set the <code>value</code> attribute.
   *
   * @param _value Value to set for value
   */
  public void setValue( String _value ) {
    set( ID_VALUE, _value );
  }

  /**
   * Query the <code>annotation</code> attribute.
   *
   * @return The value of the annotation attribute.
   */
  public Annotation getAnnotation() {
    int count = childCount( ID_ANNOTATION );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_ANNOTATION, null );
    return (Annotation) it.next();
  }

  /**
   * Set the <code>annotation</code> attribute.
   *
   * @param _annotation Value to set for annotation
   */
  public void setAnnotation( Annotation _annotation ) {
    int count = childCount( ID_ANNOTATION );
    if (count > 0) {
      removeAll( ID_ANNOTATION );
    }
    add( ID_ANNOTATION, _annotation );
  }
}
