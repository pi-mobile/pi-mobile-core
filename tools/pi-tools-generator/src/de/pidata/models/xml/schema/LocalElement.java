/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;
import de.pidata.tools.CodeGenFactory;

import java.util.Collection;
import java.util.Hashtable;

public class LocalElement extends de.pidata.models.xml.schema.Element {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "localElement" ), LocalElement.class.getName(), 0, de.pidata.models.xml.schema.Element.TYPE );
    TYPE = type;
    type.addAttributeType( AnyAttribute.ANY_ATTRIBUTE, AnyAttribute.ANY_ATTR_TYPE );
    type.addAttributeType( ID_NAME, QNameType.getNCName());
  }

  public LocalElement() {
    this( null );
  }

  public LocalElement( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public LocalElement( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected LocalElement( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  /**
   * Returns the interface to be used as collection for this relation,
   * e.g. java.util.List or java.util.Set
   *
   * @return the interface to be used as collection for this relation
   */
  @Override
  public Class getCollection() throws ClassNotFoundException {
    String name = (String) get( CodeGenFactory.ID_COLLECTION );
    if (name == null) {
      return Collection.class;
    }
    else {
      return Class.forName( name );
    }
  }
}
