/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;

import java.util.Hashtable;

public class Redefine extends de.pidata.models.xml.schema.OpenAttrs implements XsdResolver {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_ATTRIBUTEGROUP = NAMESPACE.getQName("attributeGroup");
  public static final QName ID_ID = NAMESPACE.getQName("id");
  public static final QName ID_COMPLEXTYPE = NAMESPACE.getQName("complexType");
  public static final QName ID_GROUP = NAMESPACE.getQName("group");
  public static final QName ID_SCHEMALOCATION = NAMESPACE.getQName("schemaLocation");
  public static final QName ID_SIMPLETYPE = NAMESPACE.getQName("simpleType");
  public static final QName ID_ANNOTATION = NAMESPACE.getQName("annotation");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "redefine" ), Redefine.class.getName(), 0, de.pidata.models.xml.schema.OpenAttrs.TYPE );
    TYPE = type;
    type.addAttributeType( ID_SCHEMALOCATION, StringType.getDefString());
    type.addAttributeType( ID_ID, QNameType.getIDType());
    type.addRelation( ID_ANNOTATION, Annotation.TYPE, 1, Integer.MAX_VALUE);
    type.addRelation( ID_SIMPLETYPE, TopLevelSimpleType.TYPE, 1, Integer.MAX_VALUE);
    type.addRelation( ID_COMPLEXTYPE, TopLevelComplexType.TYPE, 1, Integer.MAX_VALUE);
    type.addRelation( ID_GROUP, NamedGroup.TYPE, 1, Integer.MAX_VALUE);
    type.addRelation( ID_ATTRIBUTEGROUP, AttributeGroup.TYPE, 1, Integer.MAX_VALUE);
  }

  public Redefine() {
    this( null );
  }

  public Redefine( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public Redefine( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected Redefine( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  /**
   * Query the <code>schemaLocation</code> attribute.
   *
   * @return The value of the schemaLocation attribute.
   */
  public String getSchemaLocation() {
    return (String) get( ID_SCHEMALOCATION );
  }

  /**
   * Set the <code>schemaLocation</code> attribute.
   *
   * @param _schemaLocation Value to set for schemaLocation
   */
  public void setSchemaLocation( String _schemaLocation ) {
    set( ID_SCHEMALOCATION, _schemaLocation );
  }

  /**
   * Query the <code>id</code> attribute.
   *
   * @return The value of the id attribute.
   */
  public String getId() {
    return (String) get( ID_ID );
  }

  /**
   * Set the <code>id</code> attribute.
   *
   * @param _id Value to set for id
   */
  public void setId( String _id ) {
    set( ID_ID, _id );
  }

  /**
   * Query the <code>annotation</code> attribute.
   *
   * @return The value of the annotation attribute.
   */
  public Annotation getAnnotation() {
    int count = childCount( ID_ANNOTATION );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_ANNOTATION, null );
    return (Annotation) it.next();
  }

  /**
   * Set the <code>annotation</code> attribute.
   *
   * @param _annotation Value to set for annotation
   */
  public void setAnnotation( Annotation _annotation ) {
    int count = childCount( ID_ANNOTATION );
    if (count > 0) {
      removeAll( ID_ANNOTATION );
    }
    add( ID_ANNOTATION, _annotation );
  }

  /**
   * Query the <code>simpleType</code> attribute.
   *
   * @return The value of the simpleType attribute.
   */
  public SimpleTypeDef getSimpleType() {
    int count = childCount( ID_SIMPLETYPE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_SIMPLETYPE, null );
    return (SimpleTypeDef) it.next();
  }

  /**
   * Set the <code>simpleType</code> attribute.
   *
   * @param _simpleTypeDef Value to set for simpleType
   */
  public void setSimpleType( SimpleTypeDef _simpleTypeDef ) {
    int count = childCount( ID_SIMPLETYPE );
    if (count > 0) {
      removeAll( ID_SIMPLETYPE );
    }
    add( ID_SIMPLETYPE, _simpleTypeDef );
  }

  /**
   * Query the <code>complexType</code> attribute.
   *
   * @return The value of the complexType attribute.
   */
  public SchemaFactory getComplexType() {
    int count = childCount( ID_COMPLEXTYPE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_COMPLEXTYPE, null );
    return (SchemaFactory) it.next();
  }

  /**
   * Set the <code>complexType</code> attribute.
   *
   * @param _complexType Value to set for complexType
   */
  public void setComplexType( TopLevelComplexType _complexType ) {
    int count = childCount( ID_COMPLEXTYPE );
    if (count > 0) {
      removeAll( ID_COMPLEXTYPE );
    }
    add( ID_COMPLEXTYPE, _complexType );
  }

  /**
   * Query the <code>group</code> attribute.
   *
   * @return The value of the group attribute.
   */
  public NamedGroup getGroup() {
    int count = childCount( ID_GROUP );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_GROUP, null );
    return (NamedGroup) it.next();
  }

  /**
   * Set the <code>group</code> attribute.
   *
   * @param _group Value to set for group
   */
  public void setGroup( NamedGroup _group ) {
    int count = childCount( ID_GROUP );
    if (count > 0) {
      removeAll( ID_GROUP );
    }
    add( ID_GROUP, _group );
  }

  /**
   * Query the <code>attributeGroup</code> attribute.
   *
   * @return The value of the attributeGroup attribute.
   */
  public AttributeGroup getAttributeGroup() {
    int count = childCount( ID_ATTRIBUTEGROUP );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_ATTRIBUTEGROUP, null );
    return (AttributeGroup) it.next();
  }

  /**
   * Set the <code>attributeGroup</code> attribute.
   *
   * @param _attributeGroup Value to set for attributeGroup
   */
  public void setAttributeGroup( AttributeGroup _attributeGroup ) {
    int count = childCount( ID_ATTRIBUTEGROUP );
    if (count > 0) {
      removeAll( ID_ATTRIBUTEGROUP );
    }
    add( ID_ATTRIBUTEGROUP, _attributeGroup );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  @Override
  public SimpleType getAttribute( QName attrName ) {
    RootSchema rootSchema = getRootSchema();
    ChildSchema redefineSchema = rootSchema.getChildSchema( getSchemaLocation() );
    SimpleType attribute = null;
    if (redefineSchema != null) {
      attribute = redefineSchema.getAttribute( attrName );
    }
    if (attribute == null) {
      attribute = ((AnyType) getParent( true )).getResolver().getAttribute( attrName );
    }
    return attribute;
  }

  @Override
  public Attribute getAttributeDef( QName attrName, boolean climbUp ) {
    RootSchema rootSchema = getRootSchema();
    ChildSchema redefineSchema = rootSchema.getChildSchema( getSchemaLocation() );
    Attribute attribute = null;
    if (redefineSchema != null) {
      attribute = redefineSchema.getAttributeDef( attrName, true );
    }
    if (attribute == null) {
      attribute = ((AnyType) getParent( true )).getResolver().getAttributeDef( attrName, true );
    }
    return attribute;
  }

  @Override
  public AttributeGroup getAttributeGroupDef( QName attrGroupName, boolean climbUp ) {
    RootSchema rootSchema = getRootSchema();
    ChildSchema redefineSchema = rootSchema.getChildSchema( getSchemaLocation() );
    AttributeGroup attributeGroup = null;
    if (redefineSchema != null) {
      attributeGroup = redefineSchema.getAttributeGroupDef( attrGroupName, true );
    }
    if (attributeGroup == null) {
      attributeGroup = ((AnyType) getParent( true )).getResolver().getAttributeGroupDef( attrGroupName, true );
    }
    return attributeGroup;
  }

  @Override
  public Group getGroupDef( QName groupName, boolean climbUp ) {
    RootSchema rootSchema = getRootSchema();
    ChildSchema redefineSchema = rootSchema.getChildSchema( getSchemaLocation() );
    Group group = null;
    if (redefineSchema != null) {
      group = redefineSchema.getGroupDef( groupName, true );
    }
    if (group == null) {
      group = ((AnyType) getParent( true )).getResolver().getGroupDef( groupName, true );
    }
    return group;
  }

  @Override
  public Type getType( QName typeName ) {
    RootSchema rootSchema = getRootSchema();
    ChildSchema redefineSchema = rootSchema.getChildSchema( getSchemaLocation() );
    Type type = null;
    if (redefineSchema != null) {
      type = redefineSchema.getType( typeName );
    }
    if (type == null) {
      type = ((AnyType) getParent( true )).getResolver().getType( typeName );
    }
    return type;
  }

  @Override
  public Element getElement( QName elementName ) {
    RootSchema rootSchema = getRootSchema();
    ChildSchema redefineSchema = rootSchema.getChildSchema( getSchemaLocation() );
    Element element = null;
    if (redefineSchema != null) {
      element = redefineSchema.getElement( elementName );
    }
    if (element == null) {
      element = ((AnyType) getParent( true )).getResolver().getElement( elementName );
    }
    return element;
  }
}
