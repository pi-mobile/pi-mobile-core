package de.pidata.models.xml.schema;

import de.pidata.models.tree.ModelIterator;

public class AllGroupIterator extends AllSchemaIterator<Group> {

  public AllGroupIterator( RootSchema schema ) {
    super( schema );
    currentItemIter = schema.groupIter();
    findNext();
  }

  @Override
  protected ModelIterator<Group> getItemIterator( Schema currentSchema ) {
    return currentSchema.groupIter();
  }
}
