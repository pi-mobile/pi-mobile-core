/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;

import java.util.Hashtable;

public class AttributeGroupRef extends de.pidata.models.xml.schema.Annotated { // extends AttributeGroup {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_REF = NAMESPACE.getQName("ref");
  public static final QName ID_ANNOTATION = NAMESPACE.getQName("annotation");

  public static final ComplexType TYPE;

  static {
//    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "attributeGroupRef" ), "AttributeGroupRef", AttributeGroup.TYPE );
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "attributeGroupRef" ), AttributeGroupRef.class.getName(), 0, de.pidata.models.xml.schema.Annotated.TYPE );
    TYPE = type;
    type.addAttributeType( ID_REF, QNameType.getInstance());
    type.addAttributeType( AnyAttribute.ANY_ATTRIBUTE, AnyAttribute.ANY_ATTR_TYPE );
    type.addRelation( ID_ANNOTATION, Annotation.TYPE, 0, 1);
  }

  public AttributeGroupRef() {
    this( null );
  }

  public AttributeGroupRef( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public AttributeGroupRef( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected AttributeGroupRef( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  /**
   * Query the <code>ref</code> attribute.
   *
   * @return The value of the ref attribute.
   */
  public QName getRef() {
    return (QName) get( ID_REF );
  }

  /**
   * Set the <code>ref</code> attribute.
   *
   * @param _ref Value to set for ref
   */
  public void setRef( QName _ref ) {
    set( ID_REF, _ref );
  }

  /**
   * Query the <code>annotation</code> attribute.
   *
   * @return The value of the annotation attribute.
   */
  public Annotation getAnnotation() {
    int count = childCount( ID_ANNOTATION );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_ANNOTATION, null );
    return (Annotation) it.next();
  }

  /**
   * Set the <code>annotation</code> attribute.
   *
   * @param _annotation Value to set for annotation
   */
  public void setAnnotation( Annotation _annotation ) {
    int count = childCount( ID_ANNOTATION );
    if (count > 0) {
      removeAll( ID_ANNOTATION );
    }
    add( ID_ANNOTATION, _annotation );
  }

  //==============================================================
  // End of generated code. checksum=
  //==============================================================

  public AttrDecls getAttrDecls() {
    throw new IllegalArgumentException("AttrDecls not allowed for AttributeGroupRef");
  }

  public AttributeGroup getAttributeGroup() {
    QName ref = getRef();
    XsdResolver resolver = getResolver();
    return resolver.getAttributeGroupDef( ref, true );
  }

  /**
   * Returns this group's sum of attributes (recursively).
   *
   * @return this group's sum of attributes
   */
  public int attributeSum() {
    return getAttributeGroup().getAttrDecls().attributeSum();
  }

  /**
   * Returns this group's attribute at index or null if index is too large.
   *
   * @param index the attribute's index
   * @return the attribute definition or null. The result may be an attribute
   *         reference
   */
  public Attribute getAttribute(int index) {
    return getAttributeGroup().getAttrDecls().getAttribute(index);
  }

  /**
   * Returns index of the given attribute within this group (recursive)
   * or -1 if not found.
   *
   * @param  attributeName the attribute's name
   * @return index of the given attribute or -1
   */
  public int indexOfAttribute(QName attributeName) {
    return getAttributeGroup().getAttrDecls().indexOfAttribute(attributeName);
  }
}
