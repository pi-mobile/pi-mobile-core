/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.tree.SequenceModel;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.Hashtable;

/**
 * Provides helper methods for attribute declarations according to the
 * group definition "attrDecls" of XMLSchema.xsd.
 */
public class AttrDecls {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );
  public static final QName ID_ATTRIBUTEGROUP = NAMESPACE.getQName("attributeGroup");
  public static final QName ID_ATTRIBUTE = NAMESPACE.getQName("attribute");
  public static final QName ID_ANYATTRIBUTE = NAMESPACE.getQName("anyAttribute");

  private ChildList children;
  private SequenceModel model;
  private Hashtable attrCache = new Hashtable();

  public AttrDecls(SequenceModel model, ChildList children) {
    this.children = children;
    this.model = model;
  }

  /**
   * Adds an attribute definition
   * @param _attribute the attribute definition
   */
  public void addAttribute( Attribute _attribute ) {
    this.model.add( ID_ATTRIBUTE, _attribute );
  }

  /**
   * Removes an attribute definition
   * @param _attribute the attribute definition
   */
  public void removeAttribute( Attribute _attribute ) {
    this.model.remove( ID_ATTRIBUTE, _attribute );
    this.attrCache.clear();
  }

  /**
   * Returns an Iterator over all direct attribute definitions.
   * Attributes in attribute groups are not contained!
   * @return an Iterator over all direct attribute definitions
   */
  public ModelIterator attributeIter() {
    return this.model.iterator( ID_ATTRIBUTE, null );
  }

  /**
   * Returns the count of all direct attribute definitions.
   * Attributes in attribute groups are not conted!
   * @return the count of all direct attribute definitions
   */
  public int attributeCount() {
    return this.model.childCount( ID_ATTRIBUTE );
  }

  /**
   * Adds an attribute group definition
   * @param _attributeGroup the attribute group definition
   */
  public void addAttributeGroup( AttributeGroupRef _attributeGroup ) {
    this.model.add( ID_ATTRIBUTEGROUP, _attributeGroup );
    this.attrCache.clear();
  }

  /**
   * Removes an attribute group definition
   * @param _attributeGroup the attribute group definition
   */
  public void removeAttributeGroup( AttributeGroupRef _attributeGroup ) {
    this.model.remove( ID_ATTRIBUTEGROUP, _attributeGroup );
    this.attrCache.clear();
  }

  /**
   * Returns an iterator over all attribute group definitions
   * @return an iterator over all attribute group definitions
   */
  public ModelIterator attributeGroupIter() {
    return this.model.iterator( ID_ATTRIBUTEGROUP, null );
  }

  /**
   * Returns the count of all attribute group definitions
   * @return the count of all attribute group definitions.
   */
  public int attributeGroupCount() {
    return this.model.childCount( ID_ATTRIBUTEGROUP );
  }

  /**
   * Query the <code>anyAttribute</code> attribute.
   *
   * @return The value of the anyAttribute attribute.
   */
  public Wildcard getAnyAttribute() {
    int count = this.model.childCount( ID_ANYATTRIBUTE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = this.model.iterator( ID_ANYATTRIBUTE, null );
    return (Wildcard) it.next();
  }

  /**
   * Set the <code>anyAttribute</code> attribute.
   *
   * @param _anyAttribute Value to set for anyAttribute
   */
  public void setAnyAttribute( Wildcard _anyAttribute ) {
    int count = this.model.childCount( ID_ANYATTRIBUTE );
    if (count > 0) {
      this.model.removeAll( ID_ANYATTRIBUTE );
    }
    this.model.add( ID_ANYATTRIBUTE, _anyAttribute );
  }

  /**
   * Returns this type's sum of attributes. That is the sum of directly
   * defined attributes plus the sizes of all attributeGroup
   * definitions.
   *
   * @return this type's sum of attributes
   */
  public int attributeSum() {
    QName relationID;
    int   count = 0;
    AttributeGroupRef attrGroup;

    Model model = children.getFirstChild(null);
    while (model != null) {
      relationID = model.getParentRelationID();
      if (relationID == ID_ATTRIBUTE) {
        count++;
      }
      else if (relationID == ID_ATTRIBUTEGROUP) {
        attrGroup = (AttributeGroupRef) model;
        count += attrGroup.attributeSum();
      }
      model = model.nextSibling(null);
    }
    return count;
  }

  /**
   * Returns this type's attribute at index or null if index is too large.
   * Search is done in order of this type's schema definition. AttributeGroups
   * are processed recursively.
   *
   * @param index the attribute's index
   * @return the attribute definition or null. The result may be an attribute
   *         reference
   */
  public Attribute getAttribute(int index) {
    QName relationID;
    int   count;
    AttributeGroupRef attrGroup;

    Model model = children.getFirstChild(null);
    while (model != null) {
      relationID = model.getParentRelationID();
      if (relationID == ID_ATTRIBUTE) {
        if (index == 0) {
          return (Attribute) model;
        }
        index--;
      }
      else if (relationID == ID_ATTRIBUTEGROUP) {
        attrGroup = (AttributeGroupRef) model;
        count = attrGroup.attributeSum();
        if (index >= count) {
          index -= count;
        }
        else {
          return attrGroup.getAttribute(index);
        }
      }
      model = model.nextSibling(null);
    }
    return null;
  }

  /**
   * Returns index of the given attribute within this attrDecl (recursive)
   * or -1 if not found.
   *
   * @param  attributeName the attribute's name
   * @return index of the given attribute or -1
   */
  public int indexOfAttribute(QName attributeName) {
    QName relationID;
    int   index = 0;
    int   localIndex;
    AttributeGroupRef attrGroup;
    Attribute attr;
    QName attrName;

    if (attrCache != null) {
      Integer indexObj = (Integer) attrCache.get(attributeName);
      if (indexObj != null) return indexObj.intValue();
    }
    else {
      attrCache = new Hashtable();
    }

    Model model = children.getFirstChild(null);
    while (model != null) {
      relationID = model.getParentRelationID();
      if (relationID == ID_ATTRIBUTE) {
        attr = (Attribute) model;
        attrName = attr.getAttributeName();
        attrCache.put(attrName, new Integer(index));
        if (attrName == attributeName) {
          return index;
        }
        index++;
      }
      else if (relationID == ID_ATTRIBUTEGROUP) {
        attrGroup = (AttributeGroupRef) model;
        localIndex = attrGroup.indexOfAttribute(attributeName);
        if (localIndex >= 0) {
          attrCache.put(attributeName, new Integer(index + localIndex));
          return index + localIndex;
        }
        else {
          index += attrGroup.attributeSum();
        }
      }
      model = model.nextSibling(null);
    }
    return -1;
  }
}
