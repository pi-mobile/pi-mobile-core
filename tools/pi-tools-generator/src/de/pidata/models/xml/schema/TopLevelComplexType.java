/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;

import java.util.Hashtable;

public class TopLevelComplexType extends de.pidata.models.xml.schema.ComplexTypeDef {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_COMPLEXCONTENT = NAMESPACE.getQName("complexContent");
  public static final QName ID_SIMPLECONTENT = NAMESPACE.getQName("simpleContent");
  public static final QName ID_NAME = NAMESPACE.getQName("name");
  public static final QName ID_ANNOTATION = NAMESPACE.getQName("annotation");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "topLevelComplexType" ), TopLevelComplexType.class.getName(), 1, de.pidata.models.xml.schema.ComplexTypeDef.TYPE );
    TYPE = type;
    type.addKeyAttributeType( 0,ID_NAME, QNameType.getInstance());
    type.addAttributeType( AnyAttribute.ANY_ATTRIBUTE, AnyAttribute.ANY_ATTR_TYPE );
    type.addRelation( ID_ANNOTATION, Annotation.TYPE, 0, 1);
  }

  public TopLevelComplexType() {
    this( null );
  }

  public TopLevelComplexType( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public TopLevelComplexType( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected TopLevelComplexType( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  /**
   * Query the <code>name</code> attribute.
   *
   * @return The value of the name attribute.
   */
  public QName getName() {
    return (QName) get( ID_NAME );
  }

  /**
   * Set the <code>name</code> attribute.
   *
   * @param _name Value to set for name
   */
  public void setName( QName _name ) {
    set( ID_NAME, _name );
  }

  /**
   * Query the <code>annotation</code> attribute.
   *
   * @return The value of the annotation attribute.
   */
  public Annotation getAnnotation() {
    int count = childCount( ID_ANNOTATION );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_ANNOTATION, null );
    return (Annotation) it.next();
  }

  /**
   * Set the <code>annotation</code> attribute.
   *
   * @param _annotation Value to set for annotation
   */
  public void setAnnotation( Annotation _annotation ) {
    int count = childCount( ID_ANNOTATION );
    if (count > 0) {
      removeAll( ID_ANNOTATION );
    }
    add( ID_ANNOTATION, _annotation );
  }

  @Override
  public SimpleType getContentType() {
    if (getMixed()) {
      return null;
    }
    else {
      return StringType.getDefString();
    }
  }

  /**
   * Query the <code>simpleContent</code> attribute.
   *
   * @return The value of the simpleContent attribute.
   */
  public SimpleContent getSimpleContent() {
    int count = childCount( ID_SIMPLECONTENT );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_SIMPLECONTENT, null );
    return (SimpleContent) it.next();
  }

  /**
   * Set the <code>simpleContent</code> attribute.
   *
   * @param _simpleContent Value to set for simpleContent
   */
  public void setSimpleContent( SimpleContent _simpleContent ) {
    int count = childCount( ID_SIMPLECONTENT );
    if (count > 0) {
      removeAll( ID_SIMPLECONTENT );
    }
    add( ID_SIMPLECONTENT, _simpleContent );
  }

  /**
   * Query the <code>complexContent</code> attribute.
   *
   * @return The value of the complexContent attribute.
   */
  public ComplexContent getComplexContent() {
    int count = childCount( ID_COMPLEXCONTENT );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_COMPLEXCONTENT, null );
    return (ComplexContent) it.next();
  }

  /**
   * Set the <code>complexContent</code> attribute.
   *
   * @param _complexContent Value to set for complexContent
   */
  public void setComplexContent( ComplexContent _complexContent ) {
    int count = childCount( ID_COMPLEXCONTENT );
    if (count > 0) {
      removeAll( ID_COMPLEXCONTENT );
    }
    add( ID_COMPLEXCONTENT, _complexContent );
  }

  //==============================================================
  // End of generated code. checksum=
  //==============================================================

  public QName name() {
    return getName();
  }

  @Override
  public String toString() {
    Object name = get( ID_NAME );
    if (name == null) {
      return "-- unnamed --";
    }
    else {
      return name.toString();
    }
  }
}
