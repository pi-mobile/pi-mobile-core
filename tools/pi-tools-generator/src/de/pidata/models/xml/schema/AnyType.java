/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.qnames.Key;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.SequenceModel;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.AnyElement;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;

import java.util.Hashtable;

public class AnyType extends SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );


  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "anyType" ), AnyType.class.getName(), 0);
    TYPE = type;
    type.addAttributeType( AnyAttribute.ANY_ATTRIBUTE, AnyAttribute.ANY_ATTR_TYPE );
    type.addRelation( AnyElement.ANY_ELEMENT, AnyElement.ANY_TYPE, 0, Integer.MAX_VALUE);
  }                                               

  public AnyType() {
    this( null );
  }

  public AnyType( Key id ) {
    super( id, TYPE, null, null, null );
  }

  public AnyType( Key modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected AnyType( Key modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  //==============================================================
  // End of generated code. checksum=
  //==============================================================

  /**
   * Returns the schema root for this schema part
   * @return the schema root for this schema part
   */
  public RootSchema getRootSchema() {
    Model parent = this;
    while (parent.type() != Schema.TYPE) {
      parent = parent.getParent( true );
    }
    return ((Schema) parent).getRootSchema();
  }

  public XsdResolver getResolver() {
    Model parent = this;
    while (!(parent instanceof XsdResolver)) {
      parent = parent.getParent( true );
    }
    return ((XsdResolver) parent);
  }
}
