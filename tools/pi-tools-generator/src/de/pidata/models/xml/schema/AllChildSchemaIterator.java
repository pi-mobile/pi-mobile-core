/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;

import java.util.Iterator;

public class AllChildSchemaIterator<E extends Model> implements ModelIterator<ChildSchema> {

  private boolean hasNext = true;
  private Iterator<ChildSchema> childSchemaIterator;
  
  public AllChildSchemaIterator( RootSchema rootSchema ) {
    childSchemaIterator = rootSchema.childSchemaMap.values().iterator();
  }

  /**
   * Returns an iterator over elements of type {@code T}.
   *
   * @return an Iterator.
   */
  @Override
  public Iterator<ChildSchema> iterator() {
    return this;
  }

  /**
   * Returns true if there is a further Model in this iteration, i.e. the next call to next()
   * will return a Model not null.
   *
   * @return true if there is a further Model in this iteration
   */
  @Override
  public boolean hasNext() {
    return childSchemaIterator.hasNext();
  }

  /**
   * Returns the next Model of this iterator and sets the internal pointer to the next Model.
   *
   * @return the next Model of this iteration
   * @throws IllegalArgumentException if there is no next Model in this iterator
   */
  @Override
  public ChildSchema next() {
    if (childSchemaIterator.hasNext()) {
      return childSchemaIterator.next();
    }
    hasNext = false;
    return null;
  }
}
