/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.SequenceModel;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.AnyElement;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.StringType;

import java.util.Hashtable;

public class Appinfo extends SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_SOURCE = NAMESPACE.getQName("source");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "appinfo" ), Appinfo.class.getName(), 0 );
    TYPE = type;
    type.addAttributeType( ID_SOURCE, StringType.getDefString());
    type.addAttributeType( AnyAttribute.ANY_ATTRIBUTE, AnyAttribute.ANY_ATTR_TYPE );
    type.addRelation( AnyElement.ANY_ELEMENT, AnyElement.ANY_TYPE, 0, 1);
  }

  public Appinfo() {
    this( null );
  }

  public Appinfo( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public Appinfo( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected Appinfo( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  /**
   * Query the <code>source</code> attribute.
   *
   * @return The value of the source attribute.
   */
  public String getSource() {
    return (String) get( ID_SOURCE );
  }

  /**
   * Set the <code>source</code> attribute.
   *
   * @param _source Value to set for source
   */
  public void setSource( String _source ) {
    set( ID_SOURCE, _source );
  }

}
