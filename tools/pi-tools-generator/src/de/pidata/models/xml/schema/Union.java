/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.StringType;

import java.util.Hashtable;

public class Union extends de.pidata.models.xml.schema.Annotated {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_MEMBERTYPES = NAMESPACE.getQName("memberTypes");
  public static final QName ID_SIMPLETYPE = NAMESPACE.getQName("simpleType");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "union" ), Union.class.getName(), 0, de.pidata.models.xml.schema.Annotated.TYPE );
    TYPE = type;
    type.addAttributeType( ID_MEMBERTYPES, new StringType( ID_MEMBERTYPES, 0, Integer.MAX_VALUE ));
    type.addRelation( ID_SIMPLETYPE, LocalSimpleType.TYPE, 0, Integer.MAX_VALUE);
  }

  public Union() {
    this( null );
  }

  public Union( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public Union( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected Union( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  /**
   * Query the <code>memberTypes</code> attribute.
   *
   * @return The value of the memberTypes attribute.
   */
  public String getMemberTypes() {
    return (String) get( ID_MEMBERTYPES );
  }

  /**
   * Set the <code>memberTypes</code> attribute.
   *
   * @param _memberTypes Value to set for memberTypes
   */
  public void setMemberTypes( String _memberTypes ) {
    set( ID_MEMBERTYPES, _memberTypes );
  }

  public void addSimpleType( LocalSimpleType _simpleType ) {
    add( ID_SIMPLETYPE, _simpleType );
  }

  public void removeSimpleType( LocalSimpleType _simpleType ) {
    remove( ID_SIMPLETYPE, _simpleType );
  }

  public ModelIterator simpleTypeIter() {
    return iterator( ID_SIMPLETYPE, null );
  }

  public int simpleTypeCount() {
    return childCount( ID_SIMPLETYPE );
  }
}
