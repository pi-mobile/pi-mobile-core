/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.SimpleKey;
import de.pidata.models.types.SimpleType;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.Hashtable;

public class ChildSchema extends Schema {

  private RootSchema rootSchema;
  
  public ChildSchema( SimpleKey modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children, RootSchema rootSchema ) {
    super( modelID, attributes, anyAttribs, children );
    this.rootSchema = rootSchema;
  }

  @Override
  public void init() {
    // TODO nothing to do
  }

  @Override
  public RootSchema getRootSchema() {
    return rootSchema;
  }

  /**
   * Returns this Factory's target namespace. There is a 1:1 association between Factory and
   * targetNamespace.
   *
   * @return this Factory's target namespace
   */
  public Namespace getTargetNamespace() {
    return rootSchema.getTargetNamespace();
  }

  @Override
  protected void addChildSchema( String schemaLocation, ChildSchema childSchema ) {
    rootSchema.addChildSchema( schemaLocation, childSchema );
  }

  @Override
  protected ChildSchema getChildSchema( String schemaLocation ) {
    return rootSchema.getChildSchema( schemaLocation );
  }

  /**
   * Returns the type for the given attribute name
   *
   * @param attrName the attribute's name
   * @return the attribute's type or null if not found
   */
  @Override
  public SimpleType getAttribute( QName attrName ) {
    SimpleType attribute = super.getAttribute( attrName );
    if (attribute == null) {
      attribute = getRootSchema().getAttribute( attrName );
    }
    return attribute;
  }

  public Attribute getAttributeDef( QName attrName, boolean climbUp ) { // TODO
    Attribute attribute = super.getAttributeDef( attrName, climbUp );
    if ((attribute == null) && (climbUp)) {
      attribute = getRootSchema().getAttributeDef( attrName, true );
    }
    return attribute;
  }

  public AttributeGroup getAttributeGroupDef( QName attrGroupName, boolean climbUp ) {
    AttributeGroup attributeGroup = super.getAttributeGroupDef( attrGroupName, climbUp );
    if ((attributeGroup == null) && (climbUp)) {
      attributeGroup = getRootSchema().getAttributeGroupDef( attrGroupName, true );
    }
    return attributeGroup;
  }

  public Group getGroupDef( QName groupName, boolean climbUp ) {
    Group group = super.getGroupDef( groupName, climbUp );
    if ((group == null) && (climbUp)) {
      group = getRootSchema().getGroupDef( groupName, true );
    }
    return group;
  }
}
