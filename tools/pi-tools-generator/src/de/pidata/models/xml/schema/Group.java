/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.BaseFactory;
import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.DecimalObject;
import de.pidata.models.types.simple.IntegerType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;

import java.util.Hashtable;

public abstract class Group extends de.pidata.models.xml.schema.Annotated {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_REF = NAMESPACE.getQName("ref");
  public static final QName ID_MAXOCCURS = NAMESPACE.getQName("maxOccurs");
  public static final QName ID_NAME = NAMESPACE.getQName("name");
  public static final QName ID_MINOCCURS = NAMESPACE.getQName("minOccurs");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "group" ), Group.class.getName(), 0, de.pidata.models.xml.schema.Annotated.TYPE );
    TYPE = type;
    type.addAttributeType( ID_NAME, QNameType.getInstance());
    type.addAttributeType( ID_REF, QNameType.getInstance());
    type.addAttributeType( ID_MINOCCURS, IntegerType.getDefNonNegativeInteger());
    type.addAttributeType( ID_MAXOCCURS, SchemaFactory.ALLNNI);
// Children defined in inheriting classes to avoid class loader problems
//    type.addRelation( Particle.ID_ELEMENT, LocalElement.TYPE, 0, Integer.MAX_VALUE);
//    type.addRelation( Particle.ID_GROUP, GroupRef.TYPE, 0, Integer.MAX_VALUE);
//    type.addRelation( Particle.ID_CHOICE, ExplicitGroup.TYPE, 0, Integer.MAX_VALUE);
//    type.addRelation( Particle.ID_SEQUENCE, ExplicitGroup.TYPE, 0, Integer.MAX_VALUE);
//    type.addRelation( Particle.ID_ANY, Any.TYPE, 0, Integer.MAX_VALUE);
//    type.addRelation( TypeDefParticle.ID_ALL, All.TYPE, 0, Integer.MAX_VALUE);
  }

  private Particle particle;

  public Group() {
    this( null );
    this.particle = new Particle(this, this.children);
  }

  public Group( QName id ) {
    super( id, TYPE, null, null, null );
    this.particle = new Particle(this, this.children);
  }

  public Group( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
    this.particle = new Particle( this, this.children );
  }

  protected Group( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
    this.particle = new Particle( this, this.children );
  }

  /**
   * Query the <code>name</code> attribute.
   *
   * @return The value of the name attribute.
   */
  public QName getName() {
    return (QName) get( ID_NAME );
  }

  /**
   * Set the <code>name</code> attribute.
   *
   * @param _name Value to set for name
   */
  public void setName( QName _name ) {
    set( ID_NAME, _name );
  }

  /**
   * Query the <code>ref</code> attribute.
   *
   * @return The value of the ref attribute.
   */
  public QName getRef() {
    return (QName) get( ID_REF );
  }

  /**
   * Set the <code>ref</code> attribute.
   *
   * @param _ref Value to set for ref
   */
  public void setRef( QName _ref ) {
    set( ID_REF, _ref );
  }

  /**
   * Query the <code>minOccurs</code> attribute.
   *
   * @return The value of the minOccurs attribute.
   */
  public Integer getMinOccurs() {
    return (Integer) get( ID_MINOCCURS );
  }

  /**
   * Set the <code>minOccurs</code> attribute.
   *
   * @param _minOccurs Value to set for minOccurs
   */
  public void setMinOccurs( Integer _minOccurs ) {
    set( ID_MINOCCURS, _minOccurs );
  }

  /**
   * Query the <code>maxOccurs</code> attribute.
   *
   * @return The value of the maxOccurs attribute.
   */
  public String getMaxOccurs() {
    return (String) get( ID_MAXOCCURS );
  }

  /**
   * Set the <code>maxOccurs</code> attribute.
   *
   * @param _maxOccurs Value to set for maxOccurs
   */
  public void setMaxOccurs( String _maxOccurs ) {
    set( ID_MAXOCCURS, _maxOccurs );
  }

  //==============================================================
  // End of generated code. checksum=
  //==============================================================

  public NestedParticle getParticle() {
    return particle;
  }

  public int minOccurs() {
    Integer min = getMinOccurs();
    if (min == null) return 1;
    else return min.intValue();
  }

  public int maxOccurs() {
    String maxStr = getMaxOccurs();
    if (maxStr == null) return 1;
    else if (maxStr.equals(BaseFactory.UNBOUNDED)) {
      return Integer.MAX_VALUE;
    }
    else {
      return Integer.parseInt(maxStr);
    }
  }
}
