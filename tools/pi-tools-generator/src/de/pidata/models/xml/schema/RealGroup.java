/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;

import java.util.Hashtable;

public class RealGroup extends de.pidata.models.xml.schema.Group {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_SEQUENCE = NAMESPACE.getQName("sequence");
  public static final QName ID_CHOICE = NAMESPACE.getQName("choice");
  public static final QName ID_ALL = NAMESPACE.getQName("all");
  public static final QName ID_ANNOTATION = NAMESPACE.getQName("annotation");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "realGroup" ), RealGroup.class.getName(), 0, de.pidata.models.xml.schema.Group.TYPE );
    TYPE = type;
    type.addAttributeType( AnyAttribute.ANY_ATTRIBUTE, AnyAttribute.ANY_ATTR_TYPE );
    type.addRelation( ID_ANNOTATION, Annotation.TYPE, 0, 1);
    type.addRelation( NestedParticle.ID_ELEMENT, LocalElement.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( NestedParticle.ID_GROUP, GroupRef.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( NestedParticle.ID_CHOICE, ExplicitGroup.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( NestedParticle.ID_SEQUENCE, ExplicitGroup.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( NestedParticle.ID_ANY, Any.TYPE, 0, Integer.MAX_VALUE);
//TODO    type.addRelation( TypeDefParticle.ID_ALL, All.TYPE, 0, Integer.MAX_VALUE);
  }

  public RealGroup() {
    this( null );
  }

  public RealGroup( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public RealGroup( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected RealGroup( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }
}
