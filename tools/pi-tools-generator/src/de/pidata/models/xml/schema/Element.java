/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.Relation;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.BooleanType;
import de.pidata.models.types.simple.IntegerType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;

import java.util.Hashtable;

public abstract class Element extends de.pidata.models.xml.schema.Annotated implements Relation {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_FIXED = NAMESPACE.getQName("fixed");
  public static final QName ID_NILLABLE = NAMESPACE.getQName("nillable");
  public static final QName ID_TYPE = NAMESPACE.getQName("type");
  public static final QName ID_SUBSTITUTIONGROUP = NAMESPACE.getQName("substitutionGroup");
  public static final QName ID_UNIQUE = NAMESPACE.getQName("unique");
  public static final QName ID_MINOCCURS = NAMESPACE.getQName("minOccurs");
  public static final QName ID_REF = NAMESPACE.getQName("ref");
  public static final QName ID_KEY = NAMESPACE.getQName("key");
  public static final QName ID_COMPLEXTYPE = NAMESPACE.getQName("complexType");
  public static final QName ID_ABSTRACT = NAMESPACE.getQName("abstract");
  public static final QName ID_FINAL = NAMESPACE.getQName("final");
  public static final QName ID_KEYREF = NAMESPACE.getQName("keyref");
  public static final QName ID_MAXOCCURS = NAMESPACE.getQName("maxOccurs");
  public static final QName ID_NAME = NAMESPACE.getQName("name");
  public static final QName ID_FORM = NAMESPACE.getQName("form");
  public static final QName ID_SIMPLETYPE = NAMESPACE.getQName("simpleType");
  public static final QName ID_DEFAULT = NAMESPACE.getQName("default");
  public static final QName ID_BLOCK = NAMESPACE.getQName("block");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "element" ), Element.class.getName(), 0, de.pidata.models.xml.schema.Annotated.TYPE );
    TYPE = type;
    type.addAttributeType( ID_REF, QNameType.getInstance());
    type.addAttributeType( ID_TYPE, QNameType.getInstance());
    type.addAttributeType( ID_SUBSTITUTIONGROUP, QNameType.getInstance());
    type.addAttributeType( ID_MINOCCURS, IntegerType.getDefNonNegativeInteger());
    type.addAttributeType( ID_MAXOCCURS, SchemaFactory.ALLNNI);
    type.addAttributeType( ID_DEFAULT, StringType.getDefString());
    type.addAttributeType( ID_FIXED, StringType.getDefString());
    type.addAttributeType( ID_NILLABLE, BooleanType.getDefault());
    type.addAttributeType( ID_ABSTRACT, BooleanType.getDefault());
    type.addAttributeType( ID_FINAL, SchemaFactory.DERIVATIONSET);
    type.addAttributeType( ID_BLOCK, SchemaFactory.BLOCKSET);
    type.addAttributeType( ID_FORM, SchemaFactory.FORMCHOICE);
    type.addRelation( ID_SIMPLETYPE, LocalSimpleType.TYPE, 0, 1);
    type.addRelation( ID_COMPLEXTYPE, LocalComplexType.TYPE, 0, 1);
    type.addRelation( ID_UNIQUE, Keybase.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( ID_KEY, Keybase.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( ID_KEYREF, Keyref.TYPE, 0, Integer.MAX_VALUE);
  }

  public Element() {
    this( null );
  }

  public Element( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public Element( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected Element( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  /**
   * Query the <code>name</code> attribute.
   *
   * @return The value of the name attribute.
   */
  public QName getName() {
    QName result = (QName) get( ID_NAME );
    if (result == null) {
      result = getRefElement().getRelationID();
    }
    return result;
  }

  /**
   * Set the <code>name</code> attribute.
   *
   * @param _name Value to set for name
   */
  public void setName( QName _name ) {
    set( ID_NAME, _name );
  }

  /**
   * Query the <code>ref</code> attribute.
   *
   * @return The value of the ref attribute.
   */
  public QName getRef() {
    return (QName) get( ID_REF );
  }

  /**
   * Set the <code>ref</code> attribute.
   *
   * @param _ref Value to set for ref
   */
  public void setRef( QName _ref ) {
    set( ID_REF, _ref );
  }

  /**
   * Query the <code>type</code> attribute.
   *
   * @return The value of the type attribute.
   */
  public QName getType() {
    return (QName) get( ID_TYPE );
  }

  /**
   * Set the <code>type</code> attribute.
   *
   * @param _type Value to set for type
   */
  public void setType( QName _type ) {
    set( ID_TYPE, _type );
  }

  /**
   * Query the <code>substitutionGroup</code> attribute.
   *
   * @return The value of the substitutionGroup attribute.
   */
  public QName getSubstitutionGroup() {
    return (QName) get( ID_SUBSTITUTIONGROUP );
  }

  /**
   * Set the <code>substitutionGroup</code> attribute.
   *
   * @param _substitutionGroup Value to set for substitutionGroup
   */
  public void setSubstitutionGroup( QName _substitutionGroup ) {
    set( ID_SUBSTITUTIONGROUP, _substitutionGroup );
  }

  /**
   * Query the <code>default</code> attribute.
   *
   * @return The value of the default attribute.
   */
  public String getDefault() {
    return (String) get( ID_DEFAULT );
  }

  /**
   * Set the <code>default</code> attribute.
   *
   * @param _default Value to set for default
   */
  public void setDefault( String _default ) {
    set( ID_DEFAULT, _default );
  }

  /**
   * Query the <code>fixed</code> attribute.
   *
   * @return The value of the fixed attribute.
   */
  public String getFixed() {
    return (String) get( ID_FIXED );
  }

  /**
   * Set the <code>fixed</code> attribute.
   *
   * @param _fixed Value to set for fixed
   */
  public void setFixed( String _fixed ) {
    set( ID_FIXED, _fixed );
  }

  /**
   * Query the <code>nillable</code> attribute.
   *
   * @return The value of the nillable attribute.
   */
  public Boolean getNillable() {
    return (Boolean) get( ID_NILLABLE );
  }

  /**
   * Set the <code>nillable</code> attribute.
   *
   * @param _nillable Value to set for nillable
   */
  public void setNillable( Boolean _nillable ) {
    set( ID_NILLABLE, _nillable );
  }

  /**
   * Query the <code>abstract</code> attribute.
   *
   * @return The value of the abstract attribute.
   */
  public Boolean getAbstract() {
    return (Boolean) get( ID_ABSTRACT );
  }

  /**
   * Set the <code>abstract</code> attribute.
   *
   * @param _abstract Value to set for abstract
   */
  public void setAbstract( Boolean _abstract ) {
    set( ID_ABSTRACT, _abstract );
  }

  /**
   * Query the <code>final</code> attribute.
   *
   * @return The value of the final attribute.
   */
  public String getFinal() {
    return (String) get( ID_FINAL );
  }

  /**
   * Set the <code>final</code> attribute.
   *
   * @param _final Value to set for final
   */
  public void setFinal( String _final ) {
    set( ID_FINAL, _final );
  }

  /**
   * Query the <code>block</code> attribute.
   *
   * @return The value of the block attribute.
   */
  public String getBlock() {
    return (String) get( ID_BLOCK );
  }

  /**
   * Set the <code>block</code> attribute.
   *
   * @param _block Value to set for block
   */
  public void setBlock( String _block ) {
    set( ID_BLOCK, _block );
  }

  /**
   * Query the <code>form</code> attribute.
   *
   * @return The value of the form attribute.
   */
  public String getForm() {
    return (String) get( ID_FORM );
  }

  /**
   * Set the <code>form</code> attribute.
   *
   * @param _form Value to set for form
   */
  public void setForm( String _form ) {
    set( ID_FORM, _form );
  }

  /**
   * Query the <code>simpleType</code> attribute.
   *
   * @return The value of the simpleType attribute.
   */
  public LocalSimpleType getSimpleType() {
    int count = childCount( ID_SIMPLETYPE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_SIMPLETYPE, null );
    return (LocalSimpleType) it.next();
  }

  /**
   * Set the <code>simpleType</code> attribute.
   *
   * @param _simpleType Value to set for simpleType
   */
  public void setSimpleType( LocalSimpleType _simpleType ) {
    int count = childCount( ID_SIMPLETYPE );
    if (count > 0) {
      removeAll( ID_SIMPLETYPE );
    }
    add( ID_SIMPLETYPE, _simpleType );
  }

  /**
   * Query the <code>complexType</code> attribute.
   *
   * @return The value of the complexType attribute.
   */
  public LocalComplexType getComplexType() {
    int count = childCount( ID_COMPLEXTYPE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_COMPLEXTYPE, null );
    return (LocalComplexType) it.next();
  }

  /**
   * Set the <code>complexType</code> attribute.
   *
   * @param _complexType Value to set for complexType
   */
  public void setComplexType( LocalComplexType _complexType ) {
    int count = childCount( ID_COMPLEXTYPE );
    if (count > 0) {
      removeAll( ID_COMPLEXTYPE );
    }
    add( ID_COMPLEXTYPE, _complexType );
  }

  public void addUnique( Keybase _unique ) {
    add( ID_UNIQUE, _unique );
  }

  public void removeUnique( Keybase _unique ) {
    remove( ID_UNIQUE, _unique );
  }

  public ModelIterator uniqueIter() {
    return iterator( ID_UNIQUE, null );
  }

  public int uniqueCount() {
    return childCount( ID_UNIQUE );
  }

  public void addKey( Keybase _key ) {
    add( ID_KEY, _key );
  }

  public void removeKey( Keybase _key ) {
    remove( ID_KEY, _key );
  }

  public ModelIterator keyIter() {
    return iterator( ID_KEY, null );
  }

  public int keyCount() {
    return childCount( ID_KEY );
  }

  public void addKeyref( Keyref _keyref ) {
    add( ID_KEYREF, _keyref );
  }

  public void removeKeyref( Keyref _keyref ) {
    remove( ID_KEYREF, _keyref );
  }

  public ModelIterator keyrefIter() {
    return iterator( ID_KEYREF, null );
  }

  public int keyrefCount() {
    return childCount( ID_KEYREF );
  }

  //==============================================================
  // End of generated code. checksum=
  //==============================================================

  /**
   * Query the <code>minOccurs</code> attribute.
   *
   * @return The value of the minOccurs attribute.
   */
  public int getMinOccurs() {
    Integer min = (Integer) get( ID_MINOCCURS );
    if (min == null) return 1;
    else return (int) min.intValue();
  }

  /**
   * Set the <code>minOccurs</code> attribute.
   *
   * @param minOccurs Value to set for minOccurs
   */
  public void setMinOccurs( int minOccurs ) {
    Integer min = new Integer(minOccurs);
    set( ID_MINOCCURS, min );
  }

  /**
   * Query the <code>maxOccurs</code> attribute.
   *
   * @return The value of the maxOccurs attribute.
   */
  public int getMaxOccurs() {
    String max = (String) get( ID_MAXOCCURS );
    if ((max == null) || (max.length() == 0)) return 1;
    if (max.equals(BaseFactory.UNBOUNDED)) return Integer.MAX_VALUE;
    else return Integer.parseInt(max);
  }

  /**
   * Set the <code>maxOccurs</code> attribute.
   *
   * @param maxOccurs Value to set for maxOccurs
   */
  public void setMaxOccurs( int maxOccurs ) {
    String max;
    if (maxOccurs == Integer.MAX_VALUE) max = BaseFactory.UNBOUNDED;
    else max = Integer.toString(maxOccurs);
    set( ID_MAXOCCURS, max );
  }

  /**
   * Returns the element referenced by this element (attribute 'ref')
   * @return the element referenced by this element
   */
  public Relation getRefElement() {
    QName ref = getRef();
    if (ref == null) {
      return null;
    }
    else {
      Schema schema = getRootSchema();
      ModelFactory factory = (ModelFactory) schema; // TODO???
      if (ref.getNamespace() != schema.getTargetNamespace()) {
        factory = ModelFactoryTable.getInstance().getFactory( ref.getNamespace() );
      }
      return factory.getRootRelation( ref );
    }  
  }

  /**
   * Returns the element this element is substituted by (attribute 'substitutionGroup')
   * @return the element substituting this element
   */
  public Element getSubstitutionElement() {
    QName substName = getSubstitutionGroup();
    if (substName == null) {
      return null;
    }
    else {
      return getResolver().getElement( substName );
    }
  }

  /**
   * Returns the type defined by this element. If element is using
   * ref the type of the referenced element is returned.
   * @return the type defined by this element
   */
  public Type getChildType() {
    Type result;
    QName typeName = getType();
    if (typeName == null) {
      Relation ref = getRefElement();
      if (ref == null) {
        result = getComplexType();
        if (result == null) {
          result = getSimpleType();
        }
        if (result == null) {
          SchemaFactory factory = (SchemaFactory) getFactory();
          String name = getName().getName();
          QName tyName = factory.getTargetNamespace().getQName( name );
          result = factory.getType( tyName );
          if (result ==  null) {
            result = new DefaultComplexType( tyName, DefaultModel.VALUE_CLASS.getName(), 0 );
            factory.addType( result );
          }
        }
        return result;
      }
      else {
        return ref.getChildType();
      }
    }
    else {
      ModelFactory factory = ModelFactoryTable.getInstance().getFactory( typeName.getNamespace() );
      if (factory == null) {
        throw new IllegalArgumentException( "Factory not found for typeName=" + typeName );
      }
      result = factory.getType( typeName ); // TODO:
      if (result == null) {
        throw new IllegalArgumentException( "Type not found in its factory, typeName=" + typeName );
      }
      return result;
    }
  }

  public QName getRelationID() {
    return getName();
  }

  public void addKeyReference( ModelReference keyRef ) {
    Type type = getChildType();
    if (type instanceof ComplexTypeDef) {
      ((ComplexTypeDef) getChildType()).addKeyReference( keyRef );
    }
    else if (type instanceof DefaultComplexType) {
      ((DefaultComplexType) getChildType()).addKeyReference( keyRef );
    }
    else {
      throw new IllegalArgumentException( "Cannot add keyRef to type="+type.name() );
    }
  }

  @Override
  public String toString() {
    Object name = get( ID_NAME );
    if (name == null) {
      return "-- unnamed --";
    }
    else {
      return name.toString();
    }
  }
}
