/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.*;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.*;

public class RootSchema extends Schema implements ModelFactory {

  protected Map<String, ChildSchema> childSchemaMap = new HashMap<>();

  public RootSchema( SimpleKey modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, attributes, anyAttribs, children );
  }
  
  public void init() {
    Namespace targetNS = getTargetNamespace();
    ModelFactoryTable.getInstance().setFactory( targetNS, this );
    ((SchemaFactory) getFactory()).processKeyDefs( this );
  }

  @Override
  public RootSchema getRootSchema() {
    return this;
  }

  @Override
  protected void addChildSchema( String schemaLocation, ChildSchema childSchema ) {
    childSchemaMap.put( schemaLocation, childSchema );
  }

  @Override
  protected ChildSchema getChildSchema( String schemaLocation ) {
    return childSchemaMap.get( schemaLocation );
  }

  public ModelIterator<ChildSchema> allChildSchemaIterator() {
    ensureIncludedSchemasLoaded();
    return new AllChildSchemaIterator<>( getRootSchema() );
  }

  public ModelIterator<Element> allElementIterator() {
    ensureIncludedSchemasLoaded();
    return new AllElementIterator( this );
  }

  public ModelIterator<Attribute> allAttributeIter() {
    ensureIncludedSchemasLoaded();
    return new AllAttributeIterator( this );
  }

  public ModelIterator<AttributeGroup> allAttributeGroupIter() {
    ensureIncludedSchemasLoaded();
    return new AllAttributeGroupIterator( this );
  }

  public ModelIterator<TopLevelComplexType> allComplexTypeIter() {
    ensureIncludedSchemasLoaded();
    return new AllComplexTypeIterator( this );
  }

  public ModelIterator<TopLevelSimpleType> allSimpleTypeIter() {
    ensureIncludedSchemasLoaded();
    return new AllSimpleTypeIterator( this );
  }

  public ModelIterator<Group> allGroupIter() {
    ensureIncludedSchemasLoaded();
    return new AllGroupIterator( this );
  }

  /**
   * Creates a new model instance for the given simple type.
   *
   * @param type   the new model's type
   * @param value  the new model's value
   * @return a new model instance for the given simple type
   */
  public Model createInstance(SimpleType type, Object value) {
    Model simpleType = get( ID_SIMPLETYPE, type.name() );
    if (simpleType == null) {
      for (ChildSchema includedSchema : allChildSchemaIterator()) {
        simpleType = includedSchema.get( ID_SIMPLETYPE, type.name() );
        if (simpleType != null) {
          break;
        }
      }
    }
    if (simpleType == null) {
      throw new IllegalArgumentException( "Type not defined by this schema, name=" + type.name() ); // TODO: includes?
    }
    return new SimpleModel( type, value );
  }

  /**
   * Returns the type for the given attribute name
   *
   * @param attrName the attribute's name
   * @return the attribute's type or null if not found
   */
  public SimpleType getAttribute( QName attrName ) {
    SimpleType attrType = super.getAttribute( attrName );
    if (attrType == null) {
      for (ChildSchema includedSchema : allChildSchemaIterator()) {
        Attribute attr = includedSchema.getAttributeDef( attrName, false );
        if (attr != null) {
          return attr.getAttributeType();
        }
      }
    }
    if (attrType == null) {
      ModelFactory modelFactory = ModelFactoryTable.getInstance().getFactory( attrName.getNamespace() );
      if (modelFactory != null) {
        return modelFactory.getAttribute( attrName );
      }
    }
    return attrType;
  }

  public Attribute getAttributeDef( QName attrName, boolean climbUp ) {
    Attribute attr = super.getAttributeDef( attrName, climbUp );
    if (attr == null) {
      for (ChildSchema includedSchema : allChildSchemaIterator()) {
        attr = includedSchema.getAttributeDef( attrName, false );
        if (attr != null) {
          break;
        }
      }
    }
    return attr;
  }

  public AttributeGroup getAttributeGroupDef( QName attrGroupName, boolean climbUp ) {
    AttributeGroup attrGroup = super.getAttributeGroupDef( attrGroupName, climbUp );
    if (attrGroup == null) {
      for (ChildSchema includedSchema : allChildSchemaIterator()) {
        attrGroup = includedSchema.getAttributeGroupDef( attrGroupName, false );
        if (attrGroup != null) {
          break;
        }
      }
    }
    return attrGroup;
  }

  public Group getGroupDef( QName groupName, boolean climbUp ) {
    Group group = super.getGroupDef( groupName, climbUp );
    if (group == null) {
      for (ChildSchema includedSchema : allChildSchemaIterator()) {
        group = includedSchema.getGroupDef( groupName, false );
        if (group != null) {
          break;
        }
      }
    }
    return group;
  }

  /**
   * Returns the type definition for the given type name.
   * Hint: Use FactoryTable to get a type you do not which factory supports it.
   *
   * @param typeName the name of the type to be returned
   * @return the type definition for the given type name
   */
  public Type getType( QName typeName ) {
    Type type = super.getType( typeName );
    if (type != null) {
      return type;
    }
    ensureIncludedSchemasLoaded();
    for (ChildSchema includedSchema : allChildSchemaIterator()) {
      type = includedSchema.getType( typeName );
      if (type != null) {
        return type;
      }
    }
    return null;
  }

  public Element getElement( QName elementName ) {
    Element element = super.getElement( elementName );
    if (element == null) {
      for (ChildSchema includedSchema : allChildSchemaIterator()) {
        element = includedSchema.getElement( elementName );
        if (element != null) {
          break;
        }
      }
    }
    return element;
  }
}
