/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.simple.IntegerType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.DecimalObject;
import de.pidata.models.types.simple.DecimalType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.StringType;

import java.util.Hashtable;

public class All extends de.pidata.models.xml.schema.ExplicitGroup {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_MAXOCCURS = NAMESPACE.getQName("maxOccurs");
  public static final QName ID_MINOCCURS = NAMESPACE.getQName("minOccurs");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "all" ), All.class.getName(), 0, de.pidata.models.xml.schema.ExplicitGroup.TYPE );
    TYPE = type;
    type.addAttributeType( ID_MINOCCURS, IntegerType.getDefNonNegativeInteger());
    type.addAttributeType( ID_MAXOCCURS, IntegerType.getDefNonNegativeInteger());
    type.addAttributeType( AnyAttribute.ANY_ATTRIBUTE, AnyAttribute.ANY_ATTR_TYPE );
  }

  public All() {
    this( null );
  }

  public All( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public All( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected All( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  /**
   * Query the <code>minOccurs</code> attribute.
   *
   * @return The value of the minOccurs attribute.
   */
  public Integer getMinOccurs() {
    return (Integer) get( ID_MINOCCURS );
  }

  /**
   * Set the <code>minOccurs</code> attribute.
   *
   * @param _minOccurs Value to set for minOccurs
   */
  public void setMinOccurs( DecimalObject _minOccurs ) {
    set( ID_MINOCCURS, _minOccurs );
  }

  /**
   * Query the <code>maxOccurs</code> attribute.
   *
   * @return The value of the maxOccurs attribute.
   */
  public String getMaxOccurs() {
    return (String) get( ID_MAXOCCURS );
  }

  /**
   * Set the <code>maxOccurs</code> attribute.
   *
   * @param _maxOccurs Value to set for maxOccurs
   */
  public void setMaxOccurs( String _maxOccurs ) {
    set( ID_MAXOCCURS, _maxOccurs );
  }
}
