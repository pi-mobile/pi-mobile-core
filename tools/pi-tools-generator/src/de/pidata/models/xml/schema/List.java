/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;

import java.util.Hashtable;

public class List extends de.pidata.models.xml.schema.Annotated {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_ITEMTYPE = NAMESPACE.getQName("itemType");
  public static final QName ID_SIMPLETYPE = NAMESPACE.getQName("simpleType");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "list" ), List.class.getName(), 0, de.pidata.models.xml.schema.Annotated.TYPE );
    TYPE = type;
    type.addAttributeType( ID_ITEMTYPE, QNameType.getInstance());
    type.addRelation( ID_SIMPLETYPE, LocalSimpleType.TYPE, 0, 1);
  }

  public List() {
    this( null );
  }

  public List( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public List( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected List( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  /**
   * Query the <code>itemType</code> attribute.
   *
   * @return The value of the itemType attribute.
   */
  public QName getItemType() {
    return (QName) get( ID_ITEMTYPE );
  }

  /**
   * Set the <code>itemType</code> attribute.
   *
   * @param _itemType Value to set for itemType
   */
  public void setItemType( QName _itemType ) {
    set( ID_ITEMTYPE, _itemType );
  }

  /**
   * Query the <code>simpleType</code> attribute.
   *
   * @return The value of the simpleType attribute.
   */
  public LocalSimpleType getSimpleType() {
    int count = childCount( ID_SIMPLETYPE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_SIMPLETYPE, null );
    return (LocalSimpleType) it.next();
  }

  /**
   * Set the <code>simpleType</code> attribute.
   *
   * @param _simpleType Value to set for simpleType
   */
  public void setSimpleType( LocalSimpleType _simpleType ) {
    int count = childCount( ID_SIMPLETYPE );
    if (count > 0) {
      removeAll( ID_SIMPLETYPE );
    }
    add( ID_SIMPLETYPE, _simpleType );
  }
}
