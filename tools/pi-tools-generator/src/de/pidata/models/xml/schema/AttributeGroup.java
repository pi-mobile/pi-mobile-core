/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;

import java.util.Hashtable;

public class AttributeGroup extends de.pidata.models.xml.schema.Annotated {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_REF = NAMESPACE.getQName("ref");
  public static final QName ID_NAME = NAMESPACE.getQName("name");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "attributeGroup" ), AttributeGroup.class.getName(), 1, de.pidata.models.xml.schema.Annotated.TYPE );
    TYPE = type;
    type.addKeyAttributeType( 0, ID_NAME, QNameType.getInstance());
    type.addAttributeType( ID_REF, QNameType.getInstance());
    type.addRelation( AttrDecls.ID_ATTRIBUTE, Attribute.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( AttrDecls.ID_ATTRIBUTEGROUP, AttributeGroupRef.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( AttrDecls.ID_ANYATTRIBUTE, Wildcard.TYPE, 0, 1);
  }

  private AttrDecls attrDecls;

  public AttributeGroup() {
    this( null );
    this.attrDecls = new AttrDecls(this, this.children);
  }

  public AttributeGroup( QName id ) {
    super( id, TYPE, null, null, null );
    this.attrDecls = new AttrDecls(this, this.children);
  }

  public AttributeGroup( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
    this.attrDecls = new AttrDecls( this, this.children );
  }

  protected AttributeGroup( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
    this.attrDecls = new AttrDecls( this, this.children );
  }

  /**
   * Query the <code>name</code> attribute.
   *
   * @return The value of the name attribute.
   */
  public QName getName() {
    return (QName) get( ID_NAME );
  }

  /**
   * Set the <code>name</code> attribute.
   *
   * @param _name Value to set for name
   */
  public void setName( QName _name ) {
    set( ID_NAME, _name );
  }

  /**
   * Query the <code>ref</code> attribute.
   *
   * @return The value of the ref attribute.
   */
  public QName getRef() {
    return (QName) get( ID_REF );
  }

  /**
   * Set the <code>ref</code> attribute.
   *
   * @param _ref Value to set for ref
   */
  public void setRef( QName _ref ) {
    set( ID_REF, _ref );
  }

  /**
   * Query the <code>attribute</code> attribute.
   *
   * @return The value of the attribute attribute.
   */
  public Attribute getAttribute() {
    int count = childCount( AttrDecls.ID_ATTRIBUTE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( AttrDecls.ID_ATTRIBUTE, null );
    return (Attribute) it.next();
  }

  /**
   * Set the <code>attribute</code> attribute.
   *
   * @param _attribute Value to set for attribute
   */
  public void setAttribute( Attribute _attribute ) {
    int count = childCount( AttrDecls.ID_ATTRIBUTE );
    if (count > 0) {
      removeAll( AttrDecls.ID_ATTRIBUTE );
    }
    add( AttrDecls.ID_ATTRIBUTE, _attribute );
  }

  /**
   * Query the <code>attributeGroup</code> attribute.
   *
   * @return The value of the attributeGroup attribute.
   */
  public AttributeGroupRef getAttributeGroup() {
    int count = childCount( AttrDecls.ID_ATTRIBUTEGROUP );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( AttrDecls.ID_ATTRIBUTEGROUP, null );
    return (AttributeGroupRef) it.next();
  }

  /**
   * Set the <code>attributeGroup</code> attribute.
   *
   * @param _attributeGroup Value to set for attributeGroup
   */
  public void setAttributeGroup( AttributeGroupRef _attributeGroup ) {
    int count = childCount( AttrDecls.ID_ATTRIBUTEGROUP );
    if (count > 0) {
      removeAll( AttrDecls.ID_ATTRIBUTEGROUP );
    }
    add( AttrDecls.ID_ATTRIBUTEGROUP, _attributeGroup );
  }

  /**
   * Query the <code>anyAttribute</code> attribute.
   *
   * @return The value of the anyAttribute attribute.
   */
  public Wildcard getAnyAttribute() {
    int count = childCount( AttrDecls.ID_ANYATTRIBUTE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( AttrDecls.ID_ANYATTRIBUTE, null );
    return (Wildcard) it.next();
  }

  /**
   * Set the <code>anyAttribute</code> attribute.
   *
   * @param _anyAttribute Value to set for anyAttribute
   */
  public void setAnyAttribute( Wildcard _anyAttribute ) {
    int count = childCount( AttrDecls.ID_ANYATTRIBUTE );
    if (count > 0) {
      removeAll( AttrDecls.ID_ANYATTRIBUTE );
    }
    add( AttrDecls.ID_ANYATTRIBUTE, _anyAttribute );
  }

  //==============================================================
  // End of generated code. checksum=
  //==============================================================

  public AttrDecls getAttrDecls() {
    return attrDecls;
  }
}
