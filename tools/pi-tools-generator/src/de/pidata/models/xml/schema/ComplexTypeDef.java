/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.*;
import de.pidata.models.types.*;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.AbstractSimpleType;
import de.pidata.models.types.simple.BooleanType;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;
import de.pidata.tools.CodeGenFactory;

import java.util.Hashtable;
import java.util.Vector;

public abstract class ComplexTypeDef extends Annotated implements de.pidata.models.types.ComplexType {

  public static final QName ID_MIXED = NAMESPACE.getQName("mixed");
  public static final QName ID_ABSTRACT = NAMESPACE.getQName("abstract");
  public static final QName ID_FINAL = NAMESPACE.getQName("final");
  public static final QName ID_BLOCK = NAMESPACE.getQName("block");
  public static final QName ID_COMPLEXCONTENT = NAMESPACE.getQName("complexContent");
  public static final QName ID_SIMPLECONTENT = NAMESPACE.getQName("simpleContent");

  public static final de.pidata.models.types.ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "complexTypeDef" ), ComplexTypeDef.class.getName(), 0, de.pidata.models.xml.schema.OpenAttrs.TYPE );
    TYPE = type;
  }

  static void buildType() {
    DefaultComplexType type = (DefaultComplexType) TYPE;
    type.addAttributeType( ID_MIXED, BooleanType.getDefault());
    type.addAttributeType( ID_ABSTRACT, BooleanType.getDefault(), Boolean.FALSE );
    type.addAttributeType( ID_FINAL, SchemaFactory.DERIVATIONSET);
    type.addAttributeType( ID_BLOCK, SchemaFactory.DERIVATIONSET);
    type.addRelation( ID_SIMPLECONTENT, SimpleContent.TYPE, 0, 1);
    type.addRelation( ID_COMPLEXCONTENT, ComplexContent.TYPE, 0, 1);
    type.addRelation( TypeDefParticle.ID_GROUP, GroupRef.TYPE, 0, 1);
    type.addRelation( TypeDefParticle.ID_ALL, All.TYPE, 0, 1);
    type.addRelation( TypeDefParticle.ID_CHOICE, ExplicitGroup.TYPE, 0, 1);
    type.addRelation( TypeDefParticle.ID_SEQUENCE, ExplicitGroup.TYPE, 0, 1);
    type.addRelation( AttrDecls.ID_ATTRIBUTE, Attribute.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( AttrDecls.ID_ATTRIBUTEGROUP, AttributeGroupRef.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( AttrDecls.ID_ANYATTRIBUTE, Wildcard.TYPE, 0, 1);
  }

  private AttrDecls attrDecls;
  private TypeDefParticle typeDefParticle;
  private Vector keyFields = new Vector();

  public ComplexTypeDef() {
    init();
  }

  public ComplexTypeDef(QName id) {
    super(id);
    init();
  }

  public ComplexTypeDef( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, attributes, anyAttribs, children );
    init();
  }

  public ComplexTypeDef( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
    init();
  }

  /**
   * Query the <code>mixed</code> attribute.
   *
   * @return The value of the mixed attribute.
   */
  public Boolean getMixed() {
    return (Boolean) get( ID_MIXED );
  }

  /**
   * Set the <code>mixed</code> attribute.
   *
   * @param _mixed Value to set for mixed
   */
  public void setMixed( Boolean _mixed ) {
    set( ID_MIXED, _mixed );
  }

  /**
   * Query the <code>abstract</code> attribute.
   *
   * @return The value of the abstract attribute.
   */
  public Boolean getAbstract() {
    return (Boolean) get( ID_ABSTRACT );
  }

  /**
   * Set the <code>abstract</code> attribute.
   *
   * @param _abstract Value to set for abstract
   */
  public void setAbstract( Boolean _abstract ) {
    set( ID_ABSTRACT, _abstract );
  }

  /**
   * Query the <code>final</code> attribute.
   *
   * @return The value of the final attribute.
   */
  public String getFinal() {
    return (String) get( ID_FINAL );
  }

  /**
   * Set the <code>final</code> attribute.
   *
   * @param _final Value to set for final
   */
  public void setFinal( String _final ) {
    set( ID_FINAL, _final );
  }

  /**
   * Query the <code>block</code> attribute.
   *
   * @return The value of the block attribute.
   */
  public String getBlock() {
    return (String) get( ID_BLOCK );
  }

  /**
   * Set the <code>block</code> attribute.
   *
   * @param _block Value to set for block
   */
  public void setBlock( String _block ) {
    set( ID_BLOCK, _block );
  }


  /**
   * Query the <code>simpleContent</code> attribute.
   *
   * @return The value of the simpleContent attribute.
   */
  public SimpleContent getSimpleContent() {
    int count = childCount( ID_SIMPLECONTENT );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_SIMPLECONTENT, null );
    return (SimpleContent) it.next();
  }

  /**
   * Set the <code>simpleContent</code> attribute.
   *
   * @param _simpleContent Value to set for simpleContent
   */
  public void setSimpleContent( SimpleContent _simpleContent ) {
    int count = childCount( ID_SIMPLECONTENT );
    if (count > 0) {
      removeAll( ID_SIMPLECONTENT );
    }
    add( ID_SIMPLECONTENT, _simpleContent );
  }

  /**
   * Query the <code>complexContent</code> attribute.
   *
   * @return The value of the complexContent attribute.
   */
  public ComplexContent getComplexContent() {
    int count = childCount( ID_COMPLEXCONTENT );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_COMPLEXCONTENT, null );
    return (ComplexContent) it.next();
  }

  /**
   * Set the <code>complexContent</code> attribute.
   *
   * @param _complexContent Value to set for complexContent
   */
  public void setComplexContent( ComplexContent _complexContent ) {
    int count = childCount( ID_COMPLEXCONTENT );
    if (count > 0) {
      removeAll( ID_COMPLEXCONTENT );
    }
    add( ID_COMPLEXCONTENT, _complexContent );
  }

  //==============================================================
  // End of generated code. checksum=
  //==============================================================

  private Hashtable keyReferences;

  private void init() {
    ComplexContent content = getComplexContent();
    if (content != null) {
      ExtensionType ext = content.getExtension();
      this.attrDecls = ext.getAttrDecls();
      this.typeDefParticle = ext.getTypeDefParticle();
    }
    else {
      this.attrDecls = new AttrDecls(this, this.children);
      this.typeDefParticle = new TypeDefParticle(this, this.children);
    }
  }

  public abstract QName name();

  /**
   * Returns true if this type can hold mixed content
   *
   * @return true if this type  can hold mixed content
   */
  @Override
  public boolean isMixed() {
    Boolean value = getMixed();
    if (value == null) {
      return false;
    }
    else {
      return value.booleanValue();
    }
  }

  /**
   * Returns true if this type is an abstract type
   *
   * @return true if this type is an abstract type
   */
  @Override
  public boolean isAbstract() {
    Boolean value = getAbstract();
    if (value == null) {
      return false;
    }
    else {
      return value.booleanValue();
    }
  }

  public AttrDecls getAttrDecls() {
    return attrDecls;
  }

  public TypeDefParticle getTypeDefParticle() {
    return typeDefParticle;
  }

  private int firstAttrIndex() {
    Type baseType = getBaseType();
    if (baseType instanceof ComplexType) {
      return ((ComplexType) baseType).attributeCount();
    }
    else {
      return 0;
    }
  }

  /**
   * Returns the number of attributes forming this model's key
   * beginning with attribute at index 0. Result will be 0 or 1
   * in most cases. The attribute list must be ordered, so that
   * all attributes beeing part of the key are coming frist.
   *
   * @return the nmumber of key attributes
   */
  public int keyAttributeCount() {
    int size = keyFields.size();
    if (size == 0) {
      Type baseType = getBaseType();
      if (baseType instanceof ComplexType) {
        size = ((ComplexType) baseType).keyAttributeCount();
      }
    }
    return size;
  }

  /**
   * Defines the given attributeName as part of the key attributes
   *
   * @param index         index of the attribute witing key
   * @param attributeName the name of the key attribute
   */
  public void setKeyAttribute(int index, QName attributeName) {
    if (index >= keyFields.size()) {
      keyFields.setSize(index+1);
    }
    keyFields.setElementAt(attributeName, index);
  }

  /**
   * Returns index within key of attributeName or -1 if attributeName is not part of the key
   *
   * @param attributeName the attribute's name
   * @return index within key of attributeName or -1 if attributeName is not part of the key
   */
  public int getKeyIndex(QName attributeName) {
    return keyFields.indexOf(attributeName);
  }

  /**
   * Returns the name of the ith key attribute
   *
   * @param i the index of attribute within key
   * @return the name of the ith key attribute
   */
  public QName getKeyAttribute(int i) {
    ComplexType baseType = (ComplexType) getBaseType();
    if ((baseType != null) && (i < baseType.keyAttributeCount())) {
      return baseType.getKeyAttribute( i );
    }
    else {
      return (QName) keyFields.elementAt(i);
    }
  }

  /**
   * Returns the type definition for the key attribute at index.
   *
   * @param index index of the key attribute
   * @return the type definition for the key attribute at index.
   */
  public SimpleType getKeyAttributeType(int index) {
    return getAttributeType(getKeyAttribute(index));
  }

  /**
   * Returns the number of attributes for Objects using this type
   *
   * @return the number of attributes for Objects using this type
   */
  public int attributeCount() {
    return firstAttrIndex() + attrDecls.attributeSum();
  }

  /**
   * Create array to store attributes defined by this type.
   * Needed by XML parser.
   * @return object array for attributes with matching size for this type
   */
  @Override
  public Object[] createAttributeArray() {
    return new Object[attributeCount()];
  }

  /**
   * Returns the type definition for the attribute at index.
   *
   * @param index index of the attribute
   * @return the type definition for the attribute at index.
   */
  public SimpleType getAttributeType(int index) {
    int localIndex = firstAttrIndex();
    if (index < localIndex) {
      return ((de.pidata.models.types.ComplexType) getBaseType()).getAttributeType(index);
    }
    else {
      Attribute attr = getAttrDecls().getAttribute(index - localIndex);
      return attr.getAttributeType();
    }
  }

  /**
   * Returns the default value for the attribute at index or null if
   * a default is not defined.
   *
   * @param index index of the attribute
   * @return default value for the attribute at index or null
   */
  @Override
  public Object getAttributeDefault( int index ) {
    int localIndex = firstAttrIndex();
    if (index < localIndex) {
      return ((de.pidata.models.types.ComplexType) getBaseType()).getAttributeDefault( index );
    }
    else {
      Attribute attr = getAttrDecls().getAttribute( index - localIndex );
      return attr.getAttributeType().createValue( attr.getDefault(), namespaceTable() );
    }
  }

  /**
   * Returns the value of this Model's attribute with attributeID.
   *
   * @return the value for attributeID or null if attributeID does not exist
   */
  public SimpleType getAttributeType(QName attributeID) {
    int index = indexOfAttribute(attributeID);
    if (index < 0) return null;
    else return getAttributeType(index);
  }

  /**
   * Returns the ID (name) for the attribute at index.
   *
   * @param index index of the attribute
   * @return the ID (name) for the attribute at index.
   */
  public QName getAttributeName(int index) {
    int localIndex = firstAttrIndex();
    if (index < localIndex) {
      return ((ComplexType) getBaseType()).getAttributeName(index);
    }
    else {
      Attribute attr = getAttrDecls().getAttribute(index - localIndex);
      return attr.getAttributeName();
    }
  }

  /**
   * Returns the index of the attribute identified by attributeID.
   * This may be used to store attributes in an array. The order
   * of attributes depends on the order in the type definition
   *
   * @param attributeID the attribute's ID
   * @return the index of attributeID or -1 if attributeID does
   *         not exist
   */
  public int indexOfAttribute(QName attributeID) {
    int localIndex = firstAttrIndex();
    int index = getAttrDecls().indexOfAttribute(attributeID);
    if (index >= 0) {
      return index + localIndex;
    }
    else {
      return ((ComplexType) getBaseType()).indexOfAttribute(attributeID);
    }
  }

  /**
   * Returns this types anyAttribiute definition or null if not present
   *
   * @return this types anyAttribiute definition or null
   */
  public AnyAttribute getAnyAttribute() {
    Wildcard anyAttrDef = attrDecls.getAnyAttribute();
    if (anyAttrDef == null) {
      Type basetype = getBaseType();
      if (basetype instanceof ComplexType) {
        return ((ComplexType) basetype).getAnyAttribute();
      }
      else {
        return null;
      }  
    }
    else {
      Namespace[] namespaces;
      String ns = anyAttrDef.getNamespace();
      if (ns == null) {
        namespaces = null;
      }
      else {
        namespaces = new Namespace[1];
        namespaces[0] = Namespace.getInstance(ns);
      }
      AnyAttribute result = new AnyAttribute(namespaces);
      return result;
    }
  }

  /**
   * Returns the type definition for the child relation identified by relationID
   *
   * @param relationID ID of the child relation for which type information is requested
   * @return the type definition for the child relation identified by relationID
   */
  public Type getChildType(QName relationID) {
    Relation element = getRelation(relationID);
    if (element == null) {
      throw new IllegalArgumentException( "Unknown relation name="+relationID+", type="+name());
    }
    return element.getChildType();
  }

  /**
   * Returns the child relation identified by relationID or null if relationID is
   * not defined.
   *
   * @param relationID ID of the child relation for which type information is requested
   * @return the child relation identified by relationID
   */
  public Relation getRelation(QName relationID) {
    Relation result = getTypeDefParticle().getElement(relationID);
    if (result == null) {
      ComplexType basetype = (ComplexType) getBaseType();
      if (basetype != null) {
        result = basetype.getRelation(relationID);
      }
    }
    return result;
  }

  /**
   * Checks if this type allows child having childDef to be added to a model
   * having this type.
   *
   * @param relationID ID of the child relation for which checking is done
   * @param childType  the child definition to be checked
   * @return NO_ERROR if child is allowed, otherwise the error number
   */
  public int allowsChild(QName relationID, Type childType) {
    Relation element = getTypeDefParticle().getElement(relationID);
    Type type = element.getChildType();
    if (type.isAssignableFrom(childType)) {
      return ValidationException.NO_ERROR;
    }
    else {
      return ValidationException.ERROR_WRONG_TYPE;
    }
  }

  /**
   * Returns an Enumeration over this type's child types
   *
   * @return an Enumeration over this type's child types
   */
  public QNameIterator relationNames() {
    Type basetype = getBaseType();
    if (basetype instanceof ComplexType) {
      return getTypeDefParticle().elementNameIter( ((ComplexType) basetype).relationNames() );
    }
    else {
      return getTypeDefParticle().elementNameIter( null );
    }
  }

  /**
   * Returns the count of child relationNames defined by this type
   *
   * @return the count of child relationNames defined by this type
   */
  public int relationCount() {
    ComplexType basetype = (ComplexType) getBaseType();
    if (basetype == null) {
      return getTypeDefParticle().elementCount();
    }
    else {
      return basetype.relationCount() + getTypeDefParticle().elementCount();
    }
  }

  /**
   * Returns this type's base type. Is null for base schema simpleTypes and
   * for complexTypes without base type definition.
   *
   * @return this type's parent type or null
   */
  public Type getBaseType() {
    QName baseTypeName = null;
    ComplexContent complex = getComplexContent();
    if (complex != null) {
      ExtensionType ext = complex.getExtension();
      if (ext != null) {
        baseTypeName = ext.getBase();
      }
      else {
        RestrictionType rest  = complex.getRestriction();
        if (rest != null) {
          baseTypeName = rest.getBase();
        }
      }
    }
    else {
      SimpleContent simple = getSimpleContent();
      if (simple != null) {
        ExtensionType ext = simple.getExtension();
        if (ext != null) {
          baseTypeName = ext.getBase();
        }
        else {
          RestrictionType rest  = simple.getRestriction();
          if (rest != null) {
            baseTypeName = rest.getBase();
          }
        }
      }
    }
    if (baseTypeName == null) {
      return null;
    }
    else {
      ModelFactory factory = ModelFactoryTable.getInstance().getFactory( baseTypeName.getNamespace() );
      if (factory == null) {
        throw new IllegalArgumentException( "Factory not found for base type name="+baseTypeName );
      }
      Type baseType = factory.getType(baseTypeName);
      if (baseType == null) {
        throw new IllegalArgumentException( "Base type not defined in factory, base type name="+baseTypeName );
      }
      return baseType;
    }
  }

  /**
   * Returns the message for the given errorID
   *
   * @param errorID the ID of the error for the requested message
   * @return the message for the given errorID
   */
  public String getErrorMessage(int errorID) {
    return AbstractSimpleType.createErrorMessage(errorID, this);
  }

  /**
   * Returns the Class to be used for values conforming to this Type
   *
   * @return the Class to be used for values conforming to this Type
   */
  public Class getValueClass() {
    String implementation = (String) get( CodeGenFactory.ID_IMPLEMENTATION );
    if ((implementation == null) || (implementation.length() == 0)) {
      Type baseType = getBaseType();
      if (baseType == null) return DefaultModel.VALUE_CLASS;
      else return baseType.getValueClass();
    }
    else {
      try {
        return Class.forName( implementation );
      }
      catch (ClassNotFoundException e) {
        throw new IllegalArgumentException( "Implementation class not found: "+implementation );
      }
    }
  }

  /**
   * Checks if value is valid for this type.
   *
   * @param value the value to be checked
   * @return NO_ERROR if value is valid, otherwise the errorID
   */
  public int checkValid(Object value) {
    if ((value == null) || (getValueClass().isAssignableFrom(value.getClass()))) {
      return ValidationException.NO_ERROR;
    }
    else {
      return ValidationException.ERROR_WRONG_CLASS;
    }
  }

  /**
   * Converts value to be compatible with this type. Use this method if checkValid returned
   * ValidationException.NEEDS_CONVERSION
   *
   * @param value the value to be converted
   * @return the converted value
   * @throws IllegalArgumentException if conversion is not possible
   */
  public Object convert( Object value ) {
    int valid = checkValid(value);
    if ((valid == ValidationException.NO_ERROR) || (valid == ValidationException.NEEDS_CONVERSION)) {
      return value;
    }
    else {
      throw new IllegalArgumentException( "Value='"+value+"' cannot be converted for type="+this );
    }
  }

  /**
   * Returns true if this type is the same or a base type (recursive)
   * of childType
   *
   * @param childType the type to check assignability
   * @return true if instances of childType can be assigned to fields of this type
   */
  public boolean isAssignableFrom(Type childType) {
    QName typeID = name();
    if (childType.name() == typeID) {
      return true;
    }
    Type baseType = childType.getBaseType();
    while (baseType != null) {
      if (baseType.name() == typeID) {
        return true;
      }
      baseType = baseType.getBaseType();
    }
    return false;
  }

  /**
   * Adds the given reference to this type definition. A existing reference
   * with the same name will be replaced.
   * @param reference the reference to add to this type
   */
  public void addKeyReference( ModelReference reference ) {
    if (keyReferences == null) {
      keyReferences = new Hashtable();
    }
    this.keyReferences.put( reference.getName(), reference );
  }

  /**
   * Returns the key reference with name refName
   *
   * @param refName name of the key reference
   * @return the key reference with name refName
   */
  public ModelReference getKeyReference( QName refName ) {
    if (keyReferences == null) {
      return null;
    }
    else {
      return (ModelReference) keyReferences.get( refName );
    }
  }

  /**
   * Returns an iterator over all key reference names
   *
   * @return an iterator over all key reference names
   */
  public QNameIterator keyRefNames() {
    if (this.keyReferences == null) {
      return QNameIteratorEnum.EMPTY_ITERATOR;
    }
    else {
      return new QNameIteratorEnum( this.keyReferences.keys() );
    }
  }

  @Override
  public int hashCode() {
    return name().hashCode();
  }

  @Override
  public boolean equals( Object obj ) {
    if (obj == null) {
      return false;
    }
    else if (obj instanceof ComplexTypeDef) {
      if (this.name() == null) {
        return false; // TODO?
      }
      ComplexTypeDef other = (ComplexTypeDef) obj;
      return this.name() == other.name();
    }
    else {
      return false;
    }
  }
}
