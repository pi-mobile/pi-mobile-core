/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.tree.SequenceModel;
import de.pidata.models.types.Relation;
import de.pidata.qnames.QName;

public class Particle extends NestedParticle {

  public static final QName ID_ALL = NAMESPACE.getQName("all");

  public Particle(SequenceModel model, ChildList children) {
    super(model, children);
  }

  public void addAll( All _All ) {
    this.model.add( ID_ALL, _All );
  }

  public void removeAll( All _All ) {
    this.model.remove( ID_ALL, _All );
  }

  public ModelIterator AllIter() {
    return this.model.iterator( ID_ALL, null );
  }

  public int AllCount() {
    return this.model.childCount( ID_ALL );
  }

  protected int childElementSum(Model model) {
    QName relationID = model.getParentRelationID();
    if (relationID == ID_ANY) {
      All all =  (All) model;
      return all.getNestedParticle().elementSum();
    }
    return super.childElementSum(model);
  }

  protected Relation getChildElement(Model model, QName elementName) throws ClassNotFoundException {
    QName relationID = model.getParentRelationID();
    if (relationID == ID_ALL) {
      All all =  (All) model;
      return all.getParticle().getElement(elementName);
    }
    return super.getChildElement(model, elementName);
  }
}
