/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.*;
import de.pidata.models.types.Relation;
import de.pidata.qnames.Key;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.StringType;

import java.util.Hashtable;
import java.util.Vector;

public class SchemaFactory extends BaseFactory {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_FINAL = NAMESPACE.getQName("final");
  public static final QName ID_ABSTRACT = NAMESPACE.getQName("abstract");
  public static final QName ID_MIXED = NAMESPACE.getQName("mixed");
  public static final QName ID_NAME = NAMESPACE.getQName("name");
  public static final QName ID_BLOCK = NAMESPACE.getQName("block");

  public static final de.pidata.models.types.SimpleType FORMCHOICE = new StringType( NAMESPACE.getQName("formChoice"), StringType.getDefNMTOKEN(), 0, Integer.MAX_VALUE );

  public static final de.pidata.models.types.SimpleType DERIVATIONSET = new StringType( NAMESPACE.getQName("derivationSet"), StringType.getDefToken(), 0, Integer.MAX_VALUE );
  public static final de.pidata.models.types.SimpleType SIMPLEDERIVATIONSET = new StringType( NAMESPACE.getQName("simpleDerivationSet"), StringType.getDefToken(), 0, Integer.MAX_VALUE );
  public static final de.pidata.models.types.SimpleType FULLDERIVATIONSET = new StringType( NAMESPACE.getQName("fullDerivationSet"), StringType.getDefToken(), 0, Integer.MAX_VALUE );

  public static final de.pidata.models.types.SimpleType ALLNNI = new StringType( NAMESPACE.getQName("allNNI"), StringType.getDefNMTOKEN(), 0, Integer.MAX_VALUE );
  public static final de.pidata.models.types.SimpleType NAMESPACELIST = new StringType( NAMESPACE.getQName("namespaceList"), StringType.getDefToken(), 0, Integer.MAX_VALUE );
  public static final de.pidata.models.types.SimpleType BLOCKSET = new StringType( NAMESPACE.getQName("blockSet"), StringType.getDefToken(), 0, Integer.MAX_VALUE );
  public static final de.pidata.models.types.SimpleType PUBLIC = new StringType( NAMESPACE.getQName("public"), StringType.getDefToken(), 0, Integer.MAX_VALUE );


  private Vector keyDefinitions = new Vector();
  private RootSchema rootSchema;


  public SchemaFactory() {
    super();
    addType( FORMCHOICE );
    addType( FULLDERIVATIONSET );
    addType( PUBLIC );
    addType( ALLNNI );
    addType( BLOCKSET );
    addType( DERIVATIONSET );
    addType( NAMESPACELIST );
    addType( SIMPLEDERIVATIONSET );
    addType( ExplicitGroup.TYPE );
    addType( LocalSimpleType.TYPE );
    addType( LocalComplexType.TYPE );
    addType( SimpleRestrictionType.TYPE );
    addType( RestrictionType.TYPE );
    addType( RestrictionDef.TYPE );
    addType( RealGroup.TYPE );
    addType( NamedGroup.TYPE );
    addType( NamedAttributeGroup.TYPE );
    addType( TopLevelComplexType.TYPE );
    addType( GroupRef.TYPE );
    addType( Wildcard.TYPE );
    addType( AnyType.TYPE );
    addType( TopLevelAttribute.TYPE );
    addType( AttributeGroup.TYPE );
    addType( AttributeGroupRef.TYPE );
    addType( ExtensionType.TYPE );
    addType( Keybase.TYPE );
    addType( TopLevelSimpleType.TYPE );
    addType( ComplexRestrictionType.TYPE );
    addType( NoFixedFacet.TYPE );
    addType( NumFacet.TYPE );
    addType( TopLevelElement.TYPE );
    addType( SimpleExtensionType.TYPE );
    addType( Attribute.TYPE );
    addType( All.TYPE );
    addType( NarrowMaxMin.TYPE );
    addType( SimpleExplicitGroup.TYPE );
    addType( OpenAttrs.TYPE );
    addType( Annotated.TYPE );
    addType( LocalElement.TYPE );
    addType( Appinfo.TYPE );
    addType( Documentation.TYPE );
    addType( Annotation.TYPE );
    addType( WhiteSpace.TYPE );
    addType( Union.TYPE );
    addType( Keyref.TYPE );
    addType( Include.TYPE );
    addType( Redefine.TYPE );
    addType( ComplexContent.TYPE );
    addType( List.TYPE );
    addType( Import.TYPE );
    addType( Notation.TYPE );
    addType( Schema.TYPE );
    addType( SimpleContent.TYPE );
    addType( Any.TYPE );
    addType( Field.TYPE );
    addType( TotalDigits.TYPE );
    addType( Selector.TYPE );
    addType( Pattern.TYPE );
    addRootRelation(NAMESPACE.getQName("unique"), Keybase.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("whiteSpace"), WhiteSpace.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("appinfo"), Appinfo.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("group"), NamedGroup.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("maxLength"), NumFacet.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("topLevelElement"), TopLevelElement.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("attributeGroup"), NamedAttributeGroup.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("minLength"), NumFacet.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("union"), Union.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("keyref"), Keyref.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("include"), Include.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("redefine"), Redefine.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("length"), NumFacet.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("documentation"), Documentation.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("restriction"), RestrictionDef.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("complexContent"), ComplexContent.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("list"), List.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("schema"), Schema.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("enumeration"), NoFixedFacet.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("simpleContent"), SimpleContent.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("choice"), ExplicitGroup.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("key"), Keybase.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("maxExclusive"), NoFixedFacet.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("any"), Any.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("anyAttribute"), Wildcard.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("minExclusive"), NoFixedFacet.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("fractionDigits"), NumFacet.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("field"), Field.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("annotation"), Annotation.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("complexType"), TopLevelComplexType.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("maxInclusive"), NoFixedFacet.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("minInclusive"), NoFixedFacet.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("attribute"), TopLevelAttribute.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("all"), All.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("simpleType"), TopLevelSimpleType.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("notation"), Notation.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("totalDigits"), TotalDigits.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("selector"), Selector.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("pattern"), Pattern.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("import"), Import.TYPE, 1, Integer.MAX_VALUE);
    addRootRelation(NAMESPACE.getQName("sequence"), ExplicitGroup.TYPE, 1, Integer.MAX_VALUE);
    if (attributes == null) attributes = new Hashtable();
    attributes.put(NAMESPACE.getQName("id"), StringType.getDefString());
    attributes.put(NAMESPACE.getQName("space"), StringType.getDefString());
    attributes.put(NAMESPACE.getQName("base"), StringType.getDefString());
    attributes.put(NAMESPACE.getQName("lang"), StringType.getDefString());

    ExtensionType.buildType();
    RestrictionType.buildType();
    ComplexTypeDef.buildType();
    ExplicitGroup.buildType();
    SimpleTypeDef.buildType();
    LocalComplexType.buildType();
  }

  public Model createInstance( Key key, Type typeDef, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    Class modelClass = typeDef.getValueClass();
    if (modelClass == NamedGroup.class) {
      return new NamedGroup((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == RealGroup.class) {
      return new RealGroup((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == NamedAttributeGroup.class) {
      return new NamedAttributeGroup((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == TopLevelComplexType.class) {
      return new TopLevelComplexType((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == GroupRef.class) {
      return new GroupRef((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == Wildcard.class) {
      return new Wildcard((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == AnyType.class) {
      return new AnyType((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == TopLevelAttribute.class) {
      return new TopLevelAttribute((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == AttributeGroup.class) {
      return new AttributeGroup((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == AttributeGroupRef.class) {
      return new AttributeGroupRef((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == ExtensionType.class) {
      return new ExtensionType((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == Keybase.class) {
      return new Keybase((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == TopLevelSimpleType.class) {
      return new TopLevelSimpleType((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == ComplexRestrictionType.class) {
      return new ComplexRestrictionType((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == ExplicitGroup.class) {
      return new ExplicitGroup((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == NoFixedFacet.class) {
      return new NoFixedFacet((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == NumFacet.class) {
      return new NumFacet((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == SimpleRestrictionType.class) {
      return new SimpleRestrictionType((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == LocalComplexType.class) {
      return new LocalComplexType((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == LocalSimpleType.class) {
      return new LocalSimpleType((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == TopLevelElement.class) {
      return new TopLevelElement((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == SimpleExtensionType.class) {
      return new SimpleExtensionType((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == Attribute.class) {
      return new Attribute((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == All.class) {
      return new All((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == NarrowMaxMin.class) {
      return new NarrowMaxMin((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == SimpleExplicitGroup.class) {
      return new SimpleExplicitGroup((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == OpenAttrs.class) {
      return new OpenAttrs((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == RestrictionType.class) {
      return new RestrictionType((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == Annotated.class) {
      return new Annotated((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == LocalElement.class) {
      return new LocalElement((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == Appinfo.class) {
      return new Appinfo((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == Documentation.class) {
      return new Documentation((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == Annotation.class) {
      return new Annotation((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == WhiteSpace.class) {
      return new WhiteSpace((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == Union.class) {
      return new Union((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == Keyref.class) {
      return new Keyref((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == Include.class) {
      return new Include((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == Redefine.class) {
      return new Redefine((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == RestrictionDef.class) {
      return new RestrictionDef((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == ComplexContent.class) {
      return new ComplexContent((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == List.class) {
      return new List((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == Import.class) {
      return new Import((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == Notation.class) {
      return new Notation((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == Schema.class) {
      if (rootSchema == null) {
        return new RootSchema( (SimpleKey) key, attributes, anyAttribs, children );
      }
      else {
        return new ChildSchema( (SimpleKey) key, attributes, anyAttribs, children, rootSchema );
      }
    }
    if (modelClass == SimpleContent.class) {
      return new SimpleContent((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == Any.class) {
      return new Any((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == Field.class) {
      return new Field((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == TotalDigits.class) {
      return new TotalDigits((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == Selector.class) {
      return new Selector((QName) key, attributes, anyAttribs, children);
    }
    if (modelClass == Pattern.class) {
      return new Pattern((QName) key, attributes, anyAttribs, children);
    }
    return super.createInstance(key, typeDef, attributes, anyAttribs, children);
  }

  public void addKeyDefinition( Keybase keybase ) {
    this.keyDefinitions.addElement( keybase );
  }

  protected void processKeyDefs( Schema schema ) {
    for (int i = 0; i < keyDefinitions.size(); i++) {
      Keybase key = (Keybase) keyDefinitions.elementAt( i );
      Element element = (Element) key.getParent( true );
      Element selectorElement;
      ComplexTypeDef type = (ComplexTypeDef) element.getChildType();

      String path = key.getSelector().getXpath();
      QName childName;

      // TODO komplette XPath-Ausdrücke zulassen
      if (path.equals(".")) {
        selectorElement = element;
      }
      else {
        childName = null;
        String[] pathSteps = path.split( "/" );
        ComplexType pathType = type;
        for (int stepNum = 0 ; stepNum < pathSteps.length; stepNum++) {
          if (pathSteps[stepNum].indexOf( ':' ) > 0) {
            childName = QName.getInstance( pathSteps[stepNum], schema.namespaceTable() );
          }
          else {
            childName = QName.getInstance( schema.getTargetNamespace(), pathSteps[stepNum] );
          }
          if (stepNum < pathSteps.length - 1) {
            Relation relation = pathType.getRelation( childName );
            if (relation == null) {
              throw new IllegalArgumentException( "keyRef selector not found, path="+path+", step="+childName );
            }
            Type tmp = relation.getChildType();
            if (tmp instanceof ComplexType) {
              pathType = (ComplexType) tmp;
            }
            else {
              throw new IllegalArgumentException( "keyRef selector contains simple type, path="+path+", step="+childName );
            }
          }
        }
        selectorElement = (Element) pathType.getRelation(childName);
      }

      if (selectorElement == null) {
        throw new IllegalArgumentException( "keyRef selector not found, path="+path );
      }

      if (key instanceof Keyref) {
        Type refType = selectorElement.getChildType();
        ((Keyref) key).setRefType( refType );
        selectorElement.addKeyReference( (Keyref) key );
      }
      else {
        int index = 0;
        ComplexTypeDef childTypeDef = (ComplexTypeDef) selectorElement.getChildType();
        ComplexType baseType = (ComplexType) childTypeDef.getBaseType();
        if (baseType != null) {
          index = baseType.keyAttributeCount();
        }
        for (ModelIterator fieldIter = key.fieldIter(); fieldIter.hasNext(); ) {
          Field keyField = (Field) fieldIter.next();
          QName keyFieldName = keyField.getAttributeName();
          childTypeDef.setKeyAttribute(index, keyFieldName);
          index++;
        }
      }
    }

    keyDefinitions.removeAllElements();
  }

  public void setRootSchema( RootSchema rootSchema ) {
    this.rootSchema = rootSchema;
  }
}
