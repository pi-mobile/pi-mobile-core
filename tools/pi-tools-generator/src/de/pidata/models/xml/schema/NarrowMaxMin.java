/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.DecimalType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.StringType;

import java.util.Hashtable;

public class NarrowMaxMin extends de.pidata.models.xml.schema.LocalElement {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_KEY = NAMESPACE.getQName("key");
  public static final QName ID_COMPLEXTYPE = NAMESPACE.getQName("complexType");
  public static final QName ID_KEYREF = NAMESPACE.getQName("keyref");
  public static final QName ID_UNIQUE = NAMESPACE.getQName("unique");
  public static final QName ID_MAXOCCURS = NAMESPACE.getQName("maxOccurs");
  public static final QName ID_SIMPLETYPE = NAMESPACE.getQName("simpleType");
  public static final QName ID_MINOCCURS = NAMESPACE.getQName("minOccurs");
  public static final QName ID_ANNOTATION = NAMESPACE.getQName("annotation");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "narrowMaxMin" ), NarrowMaxMin.class.getName(), 0, de.pidata.models.xml.schema.LocalElement.TYPE );
    TYPE = type;
    type.addAttributeType( ID_MINOCCURS, new DecimalType( ID_MINOCCURS, 10, 0 ));
    type.addAttributeType( ID_MAXOCCURS, new StringType( ID_MAXOCCURS, 0, Integer.MAX_VALUE ));
    type.addAttributeType( AnyAttribute.ANY_ATTRIBUTE, AnyAttribute.ANY_ATTR_TYPE );
    type.addRelation( ID_ANNOTATION, Annotation.TYPE, 0, 1);
    type.addRelation( ID_SIMPLETYPE, LocalSimpleType.TYPE, 1, 1);
    type.addRelation( ID_COMPLEXTYPE, LocalComplexType.TYPE, 1, 1);
    type.addRelation( ID_UNIQUE, Keybase.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( ID_KEY, Keybase.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( ID_KEYREF, Keyref.TYPE, 0, Integer.MAX_VALUE);
  }

  public NarrowMaxMin() {
    this( null );
  }

  public NarrowMaxMin( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public NarrowMaxMin( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected NarrowMaxMin( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }
}
