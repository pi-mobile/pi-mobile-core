/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;

import java.util.Hashtable;

public class SimpleRestrictionType extends de.pidata.models.xml.schema.RestrictionType {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_WHITESPACE = NAMESPACE.getQName("whiteSpace");
  public static final QName ID_ATTRIBUTEGROUP = NAMESPACE.getQName("attributeGroup");
  public static final QName ID_ANYATTRIBUTE = NAMESPACE.getQName("anyAttribute");
  public static final QName ID_MINEXCLUSIVE = NAMESPACE.getQName("minExclusive");
  public static final QName ID_MAXINCLUSIVE = NAMESPACE.getQName("maxInclusive");
  public static final QName ID_MAXLENGTH = NAMESPACE.getQName("maxLength");
  public static final QName ID_MAXEXCLUSIVE = NAMESPACE.getQName("maxExclusive");
  public static final QName ID_FRACTIONDIGITS = NAMESPACE.getQName("fractionDigits");
  public static final QName ID_TOTALDIGITS = NAMESPACE.getQName("totalDigits");
  public static final QName ID_LENGTH = NAMESPACE.getQName("length");
  public static final QName ID_ATTRIBUTE = NAMESPACE.getQName("attribute");
  public static final QName ID_MININCLUSIVE = NAMESPACE.getQName("minInclusive");
  public static final QName ID_PATTERN = NAMESPACE.getQName("pattern");
  public static final QName ID_MINLENGTH = NAMESPACE.getQName("minLength");
  public static final QName ID_ENUMERATION = NAMESPACE.getQName("enumeration");
  public static final QName ID_SIMPLETYPE = NAMESPACE.getQName("simpleType");
  public static final QName ID_ANNOTATION = NAMESPACE.getQName("annotation");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "simpleRestrictionType" ), SimpleRestrictionType.class.getName(), 0, de.pidata.models.xml.schema.RestrictionType.TYPE );
    TYPE = type;
    type.addAttributeType( AnyAttribute.ANY_ATTRIBUTE, AnyAttribute.ANY_ATTR_TYPE );
    type.addRelation( ID_ANNOTATION, Annotation.TYPE, 0, 1);
    type.addRelation( ID_SIMPLETYPE, LocalSimpleType.TYPE, 1, 1);
    type.addRelation( ID_MINEXCLUSIVE, NoFixedFacet.TYPE, 1, 1);
    type.addRelation( ID_MININCLUSIVE, NoFixedFacet.TYPE, 1, 1);
    type.addRelation( ID_MAXEXCLUSIVE, NoFixedFacet.TYPE, 1, 1);
    type.addRelation( ID_MAXINCLUSIVE, NoFixedFacet.TYPE, 1, 1);
    type.addRelation( ID_TOTALDIGITS, TotalDigits.TYPE, 1, 1);
    type.addRelation( ID_FRACTIONDIGITS, NumFacet.TYPE, 1, 1);
    type.addRelation( ID_LENGTH, NumFacet.TYPE, 1, 1);
    type.addRelation( ID_MINLENGTH, NumFacet.TYPE, 1, 1);
    type.addRelation( ID_MAXLENGTH, NumFacet.TYPE, 1, 1);
    type.addRelation( ID_ENUMERATION, NoFixedFacet.TYPE, 1, 1);
    type.addRelation( ID_WHITESPACE, WhiteSpace.TYPE, 1, 1);
    type.addRelation( ID_PATTERN, Pattern.TYPE, 1, 1);
    type.addRelation( ID_ATTRIBUTE, Attribute.TYPE, 1, 1);
    type.addRelation( ID_ATTRIBUTEGROUP, AttributeGroupRef.TYPE, 1, 1);
    type.addRelation( ID_ANYATTRIBUTE, Wildcard.TYPE, 1, 1);
  }

  public SimpleRestrictionType() {
    this( null );
  }

  public SimpleRestrictionType( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public SimpleRestrictionType( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected SimpleRestrictionType( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }


  /**
   * Query the <code>annotation</code> attribute.
   *
   * @return The value of the annotation attribute.
   */
  public Annotation getAnnotation() {
    int count = childCount( ID_ANNOTATION );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_ANNOTATION, null );
    return (Annotation) it.next();
  }

  /**
   * Set the <code>annotation</code> attribute.
   *
   * @param _annotation Value to set for annotation
   */
  public void setAnnotation( Annotation _annotation ) {
    int count = childCount( ID_ANNOTATION );
    if (count > 0) {
      removeAll( ID_ANNOTATION );
    }
    add( ID_ANNOTATION, _annotation );
  }

  /**
   * Query the <code>simpleType</code> attribute.
   *
   * @return The value of the simpleType attribute.
   */
  public LocalSimpleType getSimpleType() {
    int count = childCount( ID_SIMPLETYPE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_SIMPLETYPE, null );
    return (LocalSimpleType) it.next();
  }

  /**
   * Set the <code>simpleType</code> attribute.
   *
   * @param _simpleType Value to set for simpleType
   */
  public void setSimpleType( LocalSimpleType _simpleType ) {
    int count = childCount( ID_SIMPLETYPE );
    if (count > 0) {
      removeAll( ID_SIMPLETYPE );
    }
    add( ID_SIMPLETYPE, _simpleType );
  }

  /**
   * Query the <code>minExclusive</code> attribute.
   *
   * @return The value of the minExclusive attribute.
   */
  public NoFixedFacet getMinExclusive() {
    int count = childCount( ID_MINEXCLUSIVE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_MINEXCLUSIVE, null );
    return (NoFixedFacet) it.next();
  }

  /**
   * Set the <code>minExclusive</code> attribute.
   *
   * @param _minExclusive Value to set for minExclusive
   */
  public void setMinExclusive( NoFixedFacet _minExclusive ) {
    int count = childCount( ID_MINEXCLUSIVE );
    if (count > 0) {
      removeAll( ID_MINEXCLUSIVE );
    }
    add( ID_MINEXCLUSIVE, _minExclusive );
  }

  /**
   * Query the <code>minInclusive</code> attribute.
   *
   * @return The value of the minInclusive attribute.
   */
  public NoFixedFacet getMinInclusive() {
    int count = childCount( ID_MININCLUSIVE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_MININCLUSIVE, null );
    return (NoFixedFacet) it.next();
  }

  /**
   * Set the <code>minInclusive</code> attribute.
   *
   * @param _minInclusive Value to set for minInclusive
   */
  public void setMinInclusive( NoFixedFacet _minInclusive ) {
    int count = childCount( ID_MININCLUSIVE );
    if (count > 0) {
      removeAll( ID_MININCLUSIVE );
    }
    add( ID_MININCLUSIVE, _minInclusive );
  }

  /**
   * Query the <code>maxExclusive</code> attribute.
   *
   * @return The value of the maxExclusive attribute.
   */
  public NoFixedFacet getMaxExclusive() {
    int count = childCount( ID_MAXEXCLUSIVE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_MAXEXCLUSIVE, null );
    return (NoFixedFacet) it.next();
  }

  /**
   * Set the <code>maxExclusive</code> attribute.
   *
   * @param _maxExclusive Value to set for maxExclusive
   */
  public void setMaxExclusive( NoFixedFacet _maxExclusive ) {
    int count = childCount( ID_MAXEXCLUSIVE );
    if (count > 0) {
      removeAll( ID_MAXEXCLUSIVE );
    }
    add( ID_MAXEXCLUSIVE, _maxExclusive );
  }

  /**
   * Query the <code>maxInclusive</code> attribute.
   *
   * @return The value of the maxInclusive attribute.
   */
  public NoFixedFacet getMaxInclusive() {
    int count = childCount( ID_MAXINCLUSIVE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_MAXINCLUSIVE, null );
    return (NoFixedFacet) it.next();
  }

  /**
   * Set the <code>maxInclusive</code> attribute.
   *
   * @param _maxInclusive Value to set for maxInclusive
   */
  public void setMaxInclusive( NoFixedFacet _maxInclusive ) {
    int count = childCount( ID_MAXINCLUSIVE );
    if (count > 0) {
      removeAll( ID_MAXINCLUSIVE );
    }
    add( ID_MAXINCLUSIVE, _maxInclusive );
  }

  /**
   * Query the <code>totalDigits</code> attribute.
   *
   * @return The value of the totalDigits attribute.
   */
  public TotalDigits getTotalDigits() {
    int count = childCount( ID_TOTALDIGITS );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_TOTALDIGITS, null );
    return (TotalDigits) it.next();
  }

  /**
   * Set the <code>totalDigits</code> attribute.
   *
   * @param _totalDigits Value to set for totalDigits
   */
  public void setTotalDigits( TotalDigits _totalDigits ) {
    int count = childCount( ID_TOTALDIGITS );
    if (count > 0) {
      removeAll( ID_TOTALDIGITS );
    }
    add( ID_TOTALDIGITS, _totalDigits );
  }

  /**
   * Query the <code>fractionDigits</code> attribute.
   *
   * @return The value of the fractionDigits attribute.
   */
  public NumFacet getFractionDigits() {
    int count = childCount( ID_FRACTIONDIGITS );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_FRACTIONDIGITS, null );
    return (NumFacet) it.next();
  }

  /**
   * Set the <code>fractionDigits</code> attribute.
   *
   * @param _fractionDigits Value to set for fractionDigits
   */
  public void setFractionDigits( NumFacet _fractionDigits ) {
    int count = childCount( ID_FRACTIONDIGITS );
    if (count > 0) {
      removeAll( ID_FRACTIONDIGITS );
    }
    add( ID_FRACTIONDIGITS, _fractionDigits );
  }

  /**
   * Query the <code>length</code> attribute.
   *
   * @return The value of the length attribute.
   */
  public NumFacet getLength() {
    int count = childCount( ID_LENGTH );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_LENGTH, null );
    return (NumFacet) it.next();
  }

  /**
   * Set the <code>length</code> attribute.
   *
   * @param _length Value to set for length
   */
  public void setLength( NumFacet _length ) {
    int count = childCount( ID_LENGTH );
    if (count > 0) {
      removeAll( ID_LENGTH );
    }
    add( ID_LENGTH, _length );
  }

  /**
   * Query the <code>minLength</code> attribute.
   *
   * @return The value of the minLength attribute.
   */
  public NumFacet getMinLength() {
    int count = childCount( ID_MINLENGTH );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_MINLENGTH, null );
    return (NumFacet) it.next();
  }

  /**
   * Set the <code>minLength</code> attribute.
   *
   * @param _minLength Value to set for minLength
   */
  public void setMinLength( NumFacet _minLength ) {
    int count = childCount( ID_MINLENGTH );
    if (count > 0) {
      removeAll( ID_MINLENGTH );
    }
    add( ID_MINLENGTH, _minLength );
  }

  /**
   * Query the <code>maxLength</code> attribute.
   *
   * @return The value of the maxLength attribute.
   */
  public NumFacet getMaxLength() {
    int count = childCount( ID_MAXLENGTH );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_MAXLENGTH, null );
    return (NumFacet) it.next();
  }

  /**
   * Set the <code>maxLength</code> attribute.
   *
   * @param _maxLength Value to set for maxLength
   */
  public void setMaxLength( NumFacet _maxLength ) {
    int count = childCount( ID_MAXLENGTH );
    if (count > 0) {
      removeAll( ID_MAXLENGTH );
    }
    add( ID_MAXLENGTH, _maxLength );
  }

  /**
   * Query the <code>enumeration</code> attribute.
   *
   * @return The value of the enumeration attribute.
   */
  public NoFixedFacet getEnumeration() {
    int count = childCount( ID_ENUMERATION );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_ENUMERATION, null );
    return (NoFixedFacet) it.next();
  }

  /**
   * Set the <code>enumeration</code> attribute.
   *
   * @param _enumeration Value to set for enumeration
   */
  public void setEnumeration( NoFixedFacet _enumeration ) {
    int count = childCount( ID_ENUMERATION );
    if (count > 0) {
      removeAll( ID_ENUMERATION );
    }
    add( ID_ENUMERATION, _enumeration );
  }

  /**
   * Query the <code>whiteSpace</code> attribute.
   *
   * @return The value of the whiteSpace attribute.
   */
  public WhiteSpace getWhiteSpace() {
    int count = childCount( ID_WHITESPACE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_WHITESPACE, null );
    return (WhiteSpace) it.next();
  }

  /**
   * Set the <code>whiteSpace</code> attribute.
   *
   * @param _whiteSpace Value to set for whiteSpace
   */
  public void setWhiteSpace( WhiteSpace _whiteSpace ) {
    int count = childCount( ID_WHITESPACE );
    if (count > 0) {
      removeAll( ID_WHITESPACE );
    }
    add( ID_WHITESPACE, _whiteSpace );
  }

  /**
   * Query the <code>pattern</code> attribute.
   *
   * @return The value of the pattern attribute.
   */
  public Pattern getPattern() {
    int count = childCount( ID_PATTERN );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_PATTERN, null );
    return (Pattern) it.next();
  }

  /**
   * Set the <code>pattern</code> attribute.
   *
   * @param _pattern Value to set for pattern
   */
  public void setPattern( Pattern _pattern ) {
    int count = childCount( ID_PATTERN );
    if (count > 0) {
      removeAll( ID_PATTERN );
    }
    add( ID_PATTERN, _pattern );
  }

  /**
   * Query the <code>attribute</code> attribute.
   *
   * @return The value of the attribute attribute.
   */
  public Attribute getAttribute() {
    int count = childCount( ID_ATTRIBUTE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_ATTRIBUTE, null );
    return (Attribute) it.next();
  }

  /**
   * Set the <code>attribute</code> attribute.
   *
   * @param _attribute Value to set for attribute
   */
  public void setAttribute( Attribute _attribute ) {
    int count = childCount( ID_ATTRIBUTE );
    if (count > 0) {
      removeAll( ID_ATTRIBUTE );
    }
    add( ID_ATTRIBUTE, _attribute );
  }

  /**
   * Query the <code>attributeGroup</code> attribute.
   *
   * @return The value of the attributeGroup attribute.
   */
  public AttributeGroupRef getAttributeGroup() {
    int count = childCount( ID_ATTRIBUTEGROUP );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_ATTRIBUTEGROUP, null );
    return (AttributeGroupRef) it.next();
  }

  /**
   * Set the <code>attributeGroup</code> attribute.
   *
   * @param _attributeGroup Value to set for attributeGroup
   */
  public void setAttributeGroup( AttributeGroupRef _attributeGroup ) {
    int count = childCount( ID_ATTRIBUTEGROUP );
    if (count > 0) {
      removeAll( ID_ATTRIBUTEGROUP );
    }
    add( ID_ATTRIBUTEGROUP, _attributeGroup );
  }

  /**
   * Query the <code>anyAttribute</code> attribute.
   *
   * @return The value of the anyAttribute attribute.
   */
  public Wildcard getAnyAttribute() {
    int count = childCount( ID_ANYATTRIBUTE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_ANYATTRIBUTE, null );
    return (Wildcard) it.next();
  }

  /**
   * Set the <code>anyAttribute</code> attribute.
   *
   * @param _anyAttribute Value to set for anyAttribute
   */
  public void setAnyAttribute( Wildcard _anyAttribute ) {
    int count = childCount( ID_ANYATTRIBUTE );
    if (count > 0) {
      removeAll( ID_ANYATTRIBUTE );
    }
    add( ID_ANYATTRIBUTE, _anyAttribute );
  }
}
