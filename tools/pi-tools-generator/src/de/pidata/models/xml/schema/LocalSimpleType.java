/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;

import java.util.Hashtable;

public class LocalSimpleType extends de.pidata.models.xml.schema.SimpleTypeDef {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "localSimpleType" ), LocalSimpleType.class.getName(), 0, de.pidata.models.xml.schema.SimpleTypeDef.TYPE );
    TYPE = type;
    type.addAttributeType( AnyAttribute.ANY_ATTRIBUTE, AnyAttribute.ANY_ATTR_TYPE );
  }

  public LocalSimpleType() {
    this( null );
  }

  public LocalSimpleType( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public LocalSimpleType( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected LocalSimpleType( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  //==============================================================
  // End of generated code. checksum=
  //==============================================================

  public QName name() {
    Model parent = getParent( true );
    if (parent != null) {
      if (parent instanceof Element) {
        return ((Element) parent).getName();
      }
      else if (parent instanceof Attribute) {
        return ((Attribute) parent).getAttributeName();
      }
    }
    return null;
  }

  /**
   * Returns this type's base type. Is null for base schema simpleTypes and
   * for complexTypes without base type definition.
   *
   * @return this type's parent type or null
   */
  public Type getBaseType() {
    QName baseTypeName = getRestriction().getBase();
    Type baseType = ModelFactoryTable.getInstance().getFactory( baseTypeName.getNamespace() ).getType(baseTypeName);
    return baseType;
  }

  @Override
  public SimpleType getContentType() {
    return this;
  }
}
