/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;

import java.util.Hashtable;

public class Keyref extends de.pidata.models.xml.schema.Keybase implements ModelReference {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_REFER = NAMESPACE.getQName("refer");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "keyref" ), Keyref.class.getName(), 0, de.pidata.models.xml.schema.Keybase.TYPE );
    TYPE = type;
    type.addAttributeType( ID_REFER, QNameType.getInstance());
  }

  public Keyref() {
    this( null );
  }

  public Keyref( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public Keyref( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected Keyref( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  /**
   * Query the <code>refer</code> attribute.
   *
   * @return The value of the refer attribute.
   */
  public QName getRefer() {
    return (QName) get( ID_REFER );
  }

  /**
   * Set the <code>refer</code> attribute.
   *
   * @param _refer Value to set for refer
   */
  public void setRefer( QName _refer ) {
    set( ID_REFER, _refer );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  private Type refType;
  private Model refModel;

  public void setRefType( Type refType ) {
    this.refType = refType;
  }

  /**
   * Returns the name of the type referenced
   *
   * @return the name of the type referenced
   */
  public QName getRefTypeName() {
    return refType.name();
  }

  /**
   * Returns the name of the referencing attribute at index.
   * Order and type of the attributes is the same as defined by the
   * key of the referenced type
   *
   * @param index index of the referencing attribute
   * @return name of the referencing attribute at index
   */
  public QName getRefAttribute( int index ) {
    ModelIterator fieldIter = fieldIter();
    for (int i = 0; i <= index; i++) {
      if (fieldIter().hasNext()) {
        Field field = (Field) fieldIter.next();
        if (i == index) {
          return field.getAttributeName();
        }
      }
    }
    return null;
  }

  /**
   * Returns the model referenced by this model
   *
   * @return the model referenced by this model
   */
  public Model refModel() {
    if (refModel == null) {
      refModel = ModelHelper.findModel( this );
    }
    return refModel;
  }
}
