/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.Restriction;
import de.pidata.models.types.simple.StringType;

import java.util.Hashtable;

public class TopLevelSimpleType extends de.pidata.models.xml.schema.SimpleTypeDef {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_NAME = NAMESPACE.getQName("name");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "topLevelSimpleType" ), TopLevelSimpleType.class.getName(), 1, de.pidata.models.xml.schema.SimpleTypeDef.TYPE );
    TYPE = type;
    type.addKeyAttributeType( 0, ID_NAME, QNameType.getInstance());
  }

  public TopLevelSimpleType() {
    this( null );
  }

  public TopLevelSimpleType( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public TopLevelSimpleType( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected TopLevelSimpleType( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  /**
   * Query the <code>name</code> attribute.
   *
   * @return The value of the name attribute.
   */
  public QName getName() {
    return (QName) get( ID_NAME );
  }

  /**
   * Set the <code>name</code> attribute.
   *
   * @param _name Value to set for name
   */
  public void setName( QName _name ) {
    set( ID_NAME, _name );
  }

  //==============================================================
  // End of generated code. checksum=
  //==============================================================

  public QName name() {
    return getName();
  }

  /**
   * Returns this type's base type. Is null for base schema simpleTypes and
   * for complexTypes without base type definition.
   *
   * @return this type's parent type or null
   */
  public Type getBaseType() {
    QName baseTypeName;
    Restriction rest = getRestriction();
    if (rest == null) baseTypeName = StringType.TYPE_STRING;
    else baseTypeName = getRestriction().getBase();
    Type baseType = ModelFactoryTable.getInstance().getFactory( baseTypeName.getNamespace() ).getType(baseTypeName);
    return baseType;
  }

  @Override
  public SimpleType getContentType() {
    return this;
  }

  @Override
  public String toString() {
    Object name = get( ID_NAME );
    if (name == null) {
      return "-- unnamed --";
    }
    else {
      return name.toString();
    }
  }
}
