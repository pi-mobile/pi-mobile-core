/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.log.Logger;
import de.pidata.models.tree.*;
import de.pidata.qnames.Key;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.complex.DefaultRelation;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.SystemManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public abstract class Schema extends de.pidata.models.xml.schema.OpenAttrs implements XsdResolver {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  private static final QName ID_ATTRIBUTEGROUP = NAMESPACE.getQName("attributeGroup");
  private static final QName ID_INCLUDE = NAMESPACE.getQName("include");
  private static final QName ID_ELEMENTFORMDEFAULT = NAMESPACE.getQName("elementFormDefault");
  private static final QName ID_IMPORT = NAMESPACE.getQName("import");
  private static final QName ID_LANG = NAMESPACE.getQName("lang");
  private static final QName ID_NOTATION = NAMESPACE.getQName("notation");
  private static final QName ID_GROUP = NAMESPACE.getQName("group");
  private static final QName ID_TARGETNAMESPACE = NAMESPACE.getQName("targetNamespace");
  private static final QName ID_ID = NAMESPACE.getQName("id");
  private static final QName ID_FINALDEFAULT = NAMESPACE.getQName("finalDefault");
  private static final QName ID_ATTRIBUTE = NAMESPACE.getQName("attribute");
  private static final QName ID_COMPLEXTYPE = NAMESPACE.getQName("complexType");
  private static final QName ID_ELEMENT = NAMESPACE.getQName("element");
  private static final QName ID_ATTRIBUTEFORMDEFAULT = NAMESPACE.getQName("attributeFormDefault");
  private static final QName ID_VERSION = NAMESPACE.getQName("version");
  private static final QName ID_REDEFINE = NAMESPACE.getQName("redefine");
  private static final QName ID_BLOCKDEFAULT = NAMESPACE.getQName("blockDefault");
  protected static final QName ID_SIMPLETYPE = NAMESPACE.getQName("simpleType");
  private static final QName ID_ANNOTATION = NAMESPACE.getQName("annotation");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "schema" ), Schema.class.getName(), 0, de.pidata.models.xml.schema.OpenAttrs.TYPE );
    TYPE = type;
    type.addAttributeType( ID_TARGETNAMESPACE, StringType.getDefString());
    type.addAttributeType( ID_VERSION, StringType.getDefToken());
    type.addAttributeType( ID_FINALDEFAULT, SchemaFactory.FULLDERIVATIONSET);
    type.addAttributeType( ID_BLOCKDEFAULT, SchemaFactory.BLOCKSET);
    type.addAttributeType( ID_ATTRIBUTEFORMDEFAULT, SchemaFactory.FORMCHOICE);
    type.addAttributeType( ID_ELEMENTFORMDEFAULT, SchemaFactory.FORMCHOICE);
    type.addAttributeType( ID_ID, QNameType.getIDType());
    type.addAttributeType( ID_LANG, StringType.getDefString());
    type.addRelation( ID_INCLUDE, Include.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( ID_IMPORT, Import.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( ID_REDEFINE, Redefine.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( ID_ANNOTATION, Annotation.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( ID_SIMPLETYPE, TopLevelSimpleType.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( ID_COMPLEXTYPE, TopLevelComplexType.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( ID_GROUP, NamedGroup.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( ID_ATTRIBUTEGROUP, AttributeGroup.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( ID_ELEMENT, TopLevelElement.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( ID_ATTRIBUTE, Attribute.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( ID_NOTATION, Notation.TYPE, 0, Integer.MAX_VALUE);
  }

  public Schema() {
    this( null );
    init();
  }

  public Schema( SimpleKey id ) {
    super( id, TYPE, null, null, null );
    init();
  }

  public Schema( SimpleKey modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
    init();
  }

  protected Schema( SimpleKey modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
    init();
  }

  abstract public void init();
  abstract public RootSchema getRootSchema();

  /**
   * Query the <code>version</code> attribute.
   *
   * @return The value of the version attribute.
   */
  public String getVersion() {
    return (String) get( ID_VERSION );
  }

  /**
   * Set the <code>version</code> attribute.
   *
   * @param _version Value to set for version
   */
  public void setVersion( String _version ) {
    set( ID_VERSION, _version );
  }

  /**
   * Query the <code>finalDefault</code> attribute.
   *
   * @return The value of the finalDefault attribute.
   */
  public String getFinalDefault() {
    return (String) get( ID_FINALDEFAULT );
  }

  /**
   * Set the <code>finalDefault</code> attribute.
   *
   * @param _finalDefault Value to set for finalDefault
   */
  public void setFinalDefault( String _finalDefault ) {
    set( ID_FINALDEFAULT, _finalDefault );
  }

  /**
   * Query the <code>blockDefault</code> attribute.
   *
   * @return The value of the blockDefault attribute.
   */
  public String getBlockDefault() {
    return (String) get( ID_BLOCKDEFAULT );
  }

  /**
   * Set the <code>blockDefault</code> attribute.
   *
   * @param _blockDefault Value to set for blockDefault
   */
  public void setBlockDefault( String _blockDefault ) {
    set( ID_BLOCKDEFAULT, _blockDefault );
  }

  /**
   * Query the <code>attributeFormDefault</code> attribute.
   *
   * @return The value of the attributeFormDefault attribute.
   */
  public String getAttributeFormDefault() {
    return (String) get( ID_ATTRIBUTEFORMDEFAULT );
  }

  /**
   * Set the <code>attributeFormDefault</code> attribute.
   *
   * @param _attributeFormDefault Value to set for attributeFormDefault
   */
  public void setAttributeFormDefault( String _attributeFormDefault ) {
    set( ID_ATTRIBUTEFORMDEFAULT, _attributeFormDefault );
  }

  /**
   * Query the <code>elementFormDefault</code> attribute.
   *
   * @return The value of the elementFormDefault attribute.
   */
  public String getElementFormDefault() {
    return (String) get( ID_ELEMENTFORMDEFAULT );
  }

  /**
   * Set the <code>elementFormDefault</code> attribute.
   *
   * @param _elementFormDefault Value to set for elementFormDefault
   */
  public void setElementFormDefault( String _elementFormDefault ) {
    set( ID_ELEMENTFORMDEFAULT, _elementFormDefault );
  }

  /**
   * Query the <code>id</code> attribute.
   *
   * @return The value of the id attribute.
   */
  public QName getId() {
    return (QName) get( ID_ID );
  }

  /**
   * Set the <code>id</code> attribute.
   *
   * @param _id Value to set for id
   */
  public void setId( QName _id ) {
    set( ID_ID, _id );
  }

  /**
   * Query the <code>lang</code> attribute.
   *
   * @return The value of the lang attribute.
   */
  public String getLang() {
    return (String) get( ID_LANG );
  }

  /**
   * Set the <code>lang</code> attribute.
   *
   * @param _lang Value to set for lang
   */
  public void setLang( String _lang ) {
    set( ID_LANG, _lang );
  }

  private void addInclude( Include include ) {
    add( ID_INCLUDE, include );
  }

  private void removeInclude( Include include ) {
    remove( ID_INCLUDE, include );
  }

  private ModelIterator includeIter() {
    return iterator( ID_INCLUDE, null );
  }

  private int includeCount() {
    return childCount( ID_INCLUDE );
  }


  private void addImport( Import _import ) {
    add( ID_IMPORT, _import );
  }

  private void removeImport( Import _import ) {
    remove( ID_IMPORT, _import );
  }

  public ModelIterator importIter() { // TODO
    return iterator( ID_IMPORT, null );
  }

  private int importCount() {
    return childCount( ID_IMPORT );
  }


  private void addRedefine( Redefine redefine ) {
    add( ID_REDEFINE, redefine );
  }

  private void removeRedefine( Redefine redefine ) {
    remove( ID_REDEFINE, redefine );
  }

  private ModelIterator redefineIter() {
    return iterator( ID_REDEFINE, null );
  }

  private int redefineCount() {
    return childCount( ID_REDEFINE );
  }


  private void addSimpleType( TopLevelSimpleType simpleTypeDef ) {
    add( ID_SIMPLETYPE, simpleTypeDef );
  }

  private void removeSimpleType( TopLevelSimpleType simpleTypeDef ) {
    remove( ID_SIMPLETYPE, simpleTypeDef );
  }

  protected ModelIterator simpleTypeIter() {
    return iterator( ID_SIMPLETYPE, null );
  }

  private int simpleTypeCount() {
    return childCount( ID_SIMPLETYPE );
  }


  private void addComplexType( TopLevelComplexType complexType ) {
    add( ID_COMPLEXTYPE, complexType );
  }

  private void removeComplexType( TopLevelComplexType complexType ) {
    remove( ID_COMPLEXTYPE, complexType );
  }

  protected ModelIterator complexTypeIter() {
    return iterator( ID_COMPLEXTYPE, null );
  }

  private int complexTypeCount() {
    return childCount( ID_COMPLEXTYPE );
  }


  private void addElement( TopLevelElement _localElement ) {
    add( ID_ELEMENT, _localElement );
  }

  private void removeElement( TopLevelElement _localElement ) {
    remove( ID_ELEMENT, _localElement );
  }

  protected ModelIterator<Element> elementIter() {
    return iterator( ID_ELEMENT, null );
  }

  private int elementCount() {
    return childCount( ID_ELEMENT );
  }


  private void addGroup( NamedGroup _group ) {
    add( ID_GROUP, _group );
  }

  private void removeGroup( NamedGroup _group ) {
    remove( ID_GROUP, _group );
  }

  protected ModelIterator groupIter() {
    return iterator( ID_GROUP, null );
  }

  private int groupCount() {
    return childCount( ID_GROUP );
  }


  /**
   * Adds an attribute definition
   * @param _attribute the attribute definition
   */
  private void addAttribute( TopLevelAttribute _attribute ) {
    add( ID_ATTRIBUTE, _attribute );
  }

  /**
   * Removes an attribute definition
   * @param _attribute the attribute definition
   */
  private void removeAttribute( TopLevelAttribute _attribute ) {
    remove( ID_ATTRIBUTE, _attribute );
  }

  /**
   * Returns an Iterator over all direct attribute definitions.
   * Attributes in attribute groups are not contained!
   * @return an Iterator over all direct attribute definitions
   */
  protected ModelIterator attributeIter() {
    return iterator( ID_ATTRIBUTE, null );
  }

  /**
   * Returns the count of all direct attribute definitions.
   * Attributes in attribute groups are not conted!
   * @return the count of all direct attribute definitions
   */
  private int attributeCount() {
    return childCount( ID_ATTRIBUTE );
  }


  /**
   * Adds an attribute group definition
   * @param _attributeGroup the attribute group definition
   */
  private void addAttributeGroup( AttributeGroupRef _attributeGroup ) {
    add( ID_ATTRIBUTEGROUP, _attributeGroup );
  }

  /**
   * Removes an attribute group definition
   * @param _attributeGroup the attribute group definition
   */
  private void removeAttributeGroup( AttributeGroupRef _attributeGroup ) {
    remove( ID_ATTRIBUTEGROUP, _attributeGroup );
  }

  /**
   * Returns an iterator over all attribute group definitions
   * @return an iterator over all attribute group definitions
   */
  protected ModelIterator attributeGroupIter() {
    return iterator( ID_ATTRIBUTEGROUP, null );
  }

  /**
   * Returns the count of all attribute group definitions
   * @return the count of all attribute group definitions.
   */
  private int attributeGroupCount() {
    return childCount( ID_ATTRIBUTEGROUP );
  }


  private void addNotation( Notation _notation ) {
    add( ID_NOTATION, _notation );
  }

  private void removeNotation( Notation _notation ) {
    remove( ID_NOTATION, _notation );
  }

  private ModelIterator notationIter() {
    return iterator( ID_NOTATION, null );
  }

  private int notationCount() {
    return childCount( ID_NOTATION );
  }


  private void addAnnotation( Annotation _annotation ) {
    add( ID_ANNOTATION, _annotation );
  }

  private void removeAnnotation( Annotation _annotation ) {
    remove( ID_ANNOTATION, _annotation );
  }

  private ModelIterator annotationIter() {
    return iterator( ID_ANNOTATION, null );
  }

  private int annotationCount() {
    return childCount( ID_ANNOTATION );
  }

  //==============================================================
  // End of generated code. checksum=
  //==============================================================

  private boolean includedSchemasLoaded = false;

  /**
   * Load all directly included schemata.
   *
   */
  protected void ensureIncludedSchemasLoaded() {
    if (!includedSchemasLoaded) {
      includedSchemasLoaded = true;
      for (Object include : includeIter()) {
        String schemaLocation = ((Include) include).getSchemaLocation();
        Logger.info( "include " + schemaLocation );
        ChildSchema childSchema = getChildSchema( schemaLocation );
        if (childSchema != null) {
          Logger.info( "...already loaded" );
          continue;
        }
        try {
          ChildSchema includedSchema = loadIncludeXSD( schemaLocation );
          includedSchema.ensureIncludedSchemasLoaded();
        }
        catch (IOException e) {
          // TODO: what?
          Logger.error( "...error loading " + schemaLocation, e );
        }
      }
      for (Object redefine : redefineIter()) {
        String schemaLocation = ((Redefine) redefine).getSchemaLocation();
        Logger.info( "redefine " + schemaLocation );
        ChildSchema redefineSchema = getChildSchema( schemaLocation );
        if (redefineSchema != null) {
          Logger.info( "...already loaded" );
        }
        else {
          try {
            redefineSchema = loadRedefineXSD( schemaLocation );
            redefineSchema.ensureIncludedSchemasLoaded();
           }
          catch (IOException e) {
            // TODO: what?
            Logger.error( "...error loading " + schemaLocation, e );
          }
        }
        processRedefinitons( redefineSchema );
      }
    }
  }

  public Model createInstance(ComplexType type) {
    return createInstance(type.name(), type, null, null, null);
  }

  /**
   * Creates a new model instance for the given simple type.
   *
   * @param type   the new model's type
   * @param value  the new model's value
   * @return a new model instance for the given simple type
   */
  public Model createInstance(SimpleType type, Object value) {
    Model simpleType = get(ID_SIMPLETYPE, type.name());
    if (simpleType == null) {
      throw new IllegalArgumentException("Type not defined by this schema, name="+type.name());
    }
    return new SimpleModel(type, value);
  }

  /**
   * Creates a new instance for the given typeDef.
   *
   * @param key
   * @param type       the type definition describing the model to be created
   * @param attributes the attributes for the new instace - must be same order
   *                   than defined in type; may be null
   * @param anyAttribs Hastable containing attributes according to anyAttribute
   *                   definition of type, may be null
   * @param children   the initial list of children, may be null
   */
  public Model createInstance( Key key, Type type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    //TODO prüfen. ob wir den type kennen. Problem: es kann auch ein annonymer Type eines Elements sein
/*    Type typeDef = getType(type.name());
if (typeDef == null) {
 throw new IllegalArgumentException("Type not defined by this schema, name="+type.name());
} */
    if (type instanceof ComplexType) {
      return new DefaultModel( key, (ComplexType) type, attributes, anyAttribs, children );
    }
    else {
      return new SimpleModel( type );
    }
  }

  /**
   * Creates a new instance for the root relation definied by rootRelationName
   *
   * @param rootRelationName the name of a root relation defined by this factory
   * @return a new instance for rootRelationName
   */
  public Model createInstance(QName rootRelationName) {
    Relation relation = getRootRelation(rootRelationName);
    Type inputType = (Type) relation.getChildType();
    return createInstance( rootRelationName, inputType, null, null, null );
  }

  /**
   * Returns this Factory's target namespace. There is a 1:1 association between Factory and
   * targetNamespace.
   *
   * @return this Factory's target namespace
   */
  public Namespace getTargetNamespace() {
    String nsUri = (String) get(ID_TARGETNAMESPACE);
    return Namespace.getInstance( nsUri );
  }

  /**
   * Returns the root relation for the given relationName or null if not defined
   *
   * @param relationName name of the relation to be returned
   * @return root relation
   */
  public Relation getRootRelation(QName relationName) {
    Relation result = (Relation) get(ID_ELEMENT, relationName);
    if (result != null) {
      return result;
    }
    return getMessagePart(relationName);
  }

  private Attribute findAttribute( QName attrName ) {
    for (ModelIterator<Attribute> attrIter = attributeIter(); attrIter.hasNext();) {
      Attribute attr = attrIter.next();
      if (attr.getName() == attrName) {
        return attr;
      }
    }
    return null;
  }

  /**
   * Returns the type for the given attribute name
   *
   * @param attrName the attribute's name
   * @return the attribute's type or null if not found
   */
  @Override
  public SimpleType getAttribute( QName attrName ) {
    Attribute attr = findAttribute( attrName );
    if (attr == null) {
      return null;
    }
    QName typeName = attr.getType();
    return (SimpleType) ModelFactoryTable.getInstance().getFactory( typeName.getNamespace() ).getType(typeName);
  }

  public Attribute getAttributeDef( QName attrName, boolean climbUp ) { // TODO
    return findAttribute( attrName );
  }

  public AttributeGroup getAttributeGroupDef( QName attrGroupName, boolean climbUp ) {
    return (AttributeGroup) get( ID_ATTRIBUTEGROUP, attrGroupName );
  }

  public Group getGroupDef( QName groupName, boolean climbUp ) {
    return (Group) get( ID_GROUP, groupName );
  }

  /**
   * Returns the type definition for the given type name.
   * Hint: Use FactoryTable to get a type you do not which factory supports it.
   *
   * @param typeName the name of the type to be returned
   * @return the type definition for the given type name
   */
  @Override
  public Type getType( QName typeName ) {
    Type type = (Type) get(ID_SIMPLETYPE, typeName);
    if (type != null) {
      return type;
    }
    type = (Type) get(ID_COMPLEXTYPE, typeName);
    if (type != null) {
      return type;
    }
    Element elem = (Element) get(ID_ELEMENT, typeName);
    if (elem != null) {
      return elem.getChildType();
    }
    return null;
  }

  @Override
  public Element getElement( QName elementName ) {
    return (Element) get( Schema.ID_ELEMENT, elementName );
  }

  /**
   * Returns this factory's schema loacation
   *
   * @return this factory's schema loacation or null
   */
  public String getSchemaLocation() {
    return (String) get(ID_TARGETNAMESPACE);
  }

  protected Vector additionalRootRelations;

  /**
   * Adds the given Type to this Factory
   * @param type the type to be added
   * @throws IllegalArgumentException if type's namespace is different from factories
   *               targetNamespace or if type already exists for this factory
   */
  public void addType( Type type ) {
    //TODO
    throw new IllegalArgumentException( "TODO" );
  }

  public QNameIterator typeNames() {
    //TODO
    throw new IllegalArgumentException( "TODO" );
  }

  public QNameIterator relationNames() {
    //TODO
    throw new IllegalArgumentException( "TODO" );
  }

    /**
   * Adds relation to this factory's root relations
   *
   * @param rootRel the relation to be added
   */
  public void addRootRelation(Relation rootRel) {
    if (this.additionalRootRelations == null) {
      this.additionalRootRelations = new Vector();
    }
    this.additionalRootRelations.addElement(rootRel);
  }

  /**
   * Creates a relation for the given relationName and adds it to this
   * factory's root relations
   *
   * @param relationName the relation name
   * @param type         the relations's type
   * @param minOccurs    the min occurence
   * @param maxOccurs    the max occurence
   * @param substitutionGroup the relations's substitutionGroup or null
   * @return the created relation
   */
  public Relation addRootRelation( QName relationName, Type type, int minOccurs, int maxOccurs, QName substitutionGroup ) {
    DefaultRelation rel = new DefaultRelation( relationName, type, minOccurs, maxOccurs, Collection.class, substitutionGroup );
    addRootRelation( rel );
    return rel;
  }

  public Relation getMessagePart(QName relationName) {
    Relation rel;
    if (this.additionalRootRelations != null) {
      for (int i = 0; i < additionalRootRelations.size(); i++) {
        rel = (Relation) additionalRootRelations.elementAt(i);
        if (rel.getRelationID() == relationName) {
          return rel;
        }
      }
    }
    return null;
  }

  /**
   * Loads given XSD file and registers the Schema as root schema.
   * @param sysMan    the SystemManager to use
   * @param fileName  path and name of the XSD file
   * @return the Schema instance created from fileName
   * @throws IOException
   */
  public static RootSchema loadXSD(SystemManager sysMan, String fileName) throws IOException {
    XmlReader reader = new XmlReader();
    InputStream in = null;
    RootSchema schema;
    try {
      in = sysMan.getStorage(null).read(fileName);
      schema = (RootSchema) reader.loadData( in, null );
      return schema;
    }
    finally {
      StreamHelper.close( in );
    }
  }

  /**
   * Loads given XSD file and registers the Schema as included schema.
   * @param schemaLocation  path and name of the XSD file
   * @return the Schema instance created from fileName
   * @throws IOException
   */
  private ChildSchema loadIncludeXSD( String schemaLocation ) throws IOException {
    // process local files only
    String schemaFilename = getSchemaFilename( schemaLocation );
    if (schemaFilename == null) { // TODO?
      Logger.info( "...skipped" );
      return null;
    }
    ChildSchema childSchema = loadXSD( schemaFilename );
    if (childSchema != null) {
      addChildSchema( schemaFilename, childSchema );
    }
    return childSchema;
  }

  /**
   * Loads given XSD file. Does not register it, processing must be done by caller.
   * @param schemaLocation  path and name of the XSD file
   * @return the Schema instance created from fileName
   * @throws IOException
   */
  private ChildSchema loadRedefineXSD( String schemaLocation ) throws IOException {
    // process local files only
    String schemaFilename = getSchemaFilename( schemaLocation );
    if (schemaFilename == null) { // TODO?
      Logger.info( "...skipped" );
      return null;
    }
    ChildSchema childSchema = loadXSD( schemaFilename );

    // TODO: dummy only - needs special handling!
    if (childSchema != null) {
      addChildSchema( schemaFilename, childSchema );
    }
    return childSchema;
  }

  /**
   * TODO: special handling!
   * @param redefineSchema
   */
  private void processRedefinitons( ChildSchema redefineSchema ) {
    // TODO Not yet implemented
    Logger.debug( "...should redefine " + redefineSchema.getTargetNamespace() );
  }

  /**
   * Loads given XSD file as child schema. Leaves processing to caller.
   * @param schemaFilename  path and name of the XSD file
   * @return the Schema instance created from fileName
   * @throws IOException
   */
  private ChildSchema loadXSD( String schemaFilename ) throws IOException {
    XmlReader reader = new XmlReader();
    InputStream in = null;
    try {
      in = SystemManager.getInstance().getStorage( null ).read( schemaFilename );
      SchemaFactory schemaFactory = (SchemaFactory) ModelFactoryTable.getInstance().getFactory( Schema.NAMESPACE ); // TODO?
      schemaFactory.setRootSchema ( getRootSchema() );
      ChildSchema childSchema = (ChildSchema) reader.loadData( in, null, false, getTargetNamespace() );
      schemaFactory.setRootSchema ( null );
      return childSchema;
    }
    finally {
      StreamHelper.close( in );
    }
  }

  abstract protected void addChildSchema( String schemaLocation, ChildSchema childSchema );
  abstract protected ChildSchema getChildSchema( String schemaLocation );

  public static void loadSchemas(SystemManager sysMan) {
    String schemaName;
    int i = 0;

    schemaName = sysMan.getProperty("schema_"+i, null );
    while (schemaName != null) {
      try {
        Schema.loadXSD(sysMan, schemaName);
      }
      catch(IOException ex) {
        Logger.error("Could not load schema: "+schemaName, ex);
      }
      i++;
      schemaName = sysMan.getProperty("schema_"+i, null );
    }
  }

  public Namespace getDefaultNamespace() {
    Namespace ns = namespaceTable().getDefaultNamespace();
    if (ns == null) {
      ns = getTargetNamespace();
    }
    return ns;
  }

  /**
   * Handle locally stored schema XSD only. Get local copy for requested schema location. TODO
   *
   * @param filename the original schema location
   * @return the path to the local copy
   */
  private String getSchemaFilename( String filename ) {
    if (filename == null) {
      return null;
    }
    if (filename.startsWith( "http" )) {
      int lastPos = filename.lastIndexOf( "/" );
      if (lastPos > 0) {
        filename = filename.substring( lastPos + 1 );
      }
    }
    // skip known XML schema TODO???
    if ("res/xml.xsd".equals( filename )) {
      return null;
    }
    return filename;
  }
}
