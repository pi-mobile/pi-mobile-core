/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.StringType;

import java.util.Hashtable;

public class Wildcard extends de.pidata.models.xml.schema.Annotated {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_PROCESSCONTENTS = NAMESPACE.getQName("processContents");
  public static final QName ID_NAMESPACE = NAMESPACE.getQName("namespace");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "wildcard" ), Wildcard.class.getName(), 0, de.pidata.models.xml.schema.Annotated.TYPE );
    TYPE = type;
    type.addAttributeType( ID_NAMESPACE, SchemaFactory.NAMESPACELIST);
    type.addAttributeType( ID_PROCESSCONTENTS, new StringType( ID_PROCESSCONTENTS, 0, Integer.MAX_VALUE ));
  }

  public Wildcard() {
    this( null );
  }

  public Wildcard( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public Wildcard( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected Wildcard( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  /**
   * Query the <code>namespace</code> attribute.
   *
   * @return The value of the namespace attribute.
   */
  public String getNamespace() {
    return (String) get( ID_NAMESPACE );
  }

  /**
   * Set the <code>namespace</code> attribute.
   *
   * @param _namespace Value to set for namespace
   */
  public void setNamespace( String _namespace ) {
    set( ID_NAMESPACE, _namespace );
  }

  /**
   * Query the <code>processContents</code> attribute.
   *
   * @return The value of the processContents attribute.
   */
  public String getProcessContents() {
    return (String) get( ID_PROCESSCONTENTS );
  }

  /**
   * Set the <code>processContents</code> attribute.
   *
   * @param _processContents Value to set for processContents
   */
  public void setProcessContents( String _processContents ) {
    set( ID_PROCESSCONTENTS, _processContents );
  }
}
