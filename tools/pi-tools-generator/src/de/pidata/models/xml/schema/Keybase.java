/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;

import java.util.Hashtable;

public class Keybase extends de.pidata.models.xml.schema.Annotated {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_SELECTOR = NAMESPACE.getQName("selector");
  public static final QName ID_FIELD = NAMESPACE.getQName("field");
  public static final QName ID_NAME = NAMESPACE.getQName("name");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "keybase" ), Keybase.class.getName(), 1, de.pidata.models.xml.schema.Annotated.TYPE );
    TYPE = type;
    type.addKeyAttributeType( 0, ID_NAME, QNameType.getInstance());
    type.addRelation( ID_SELECTOR, Selector.TYPE, 1, 1);
    type.addRelation( ID_FIELD, Field.TYPE, 1, Integer.MAX_VALUE);
  }

  public Keybase() {
    this( null );
  }

  public Keybase( QName id ) {
    super( id, TYPE, null, null, null );
    ((SchemaFactory) getFactory()).addKeyDefinition( this );
  }

  public Keybase( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
    ((SchemaFactory) getFactory()).addKeyDefinition( this );
  }

  protected Keybase( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
    ((SchemaFactory) getFactory()).addKeyDefinition( this );
  }

  /**
   * Query the <code>name</code> attribute.
   *
   * @return The value of the name attribute.
   */
  public QName getName() {
    return (QName) get( ID_NAME );
  }

  /**
   * Set the <code>name</code> attribute.
   *
   * @param _name Value to set for name
   */
  public void setName( QName _name ) {
    set( ID_NAME, _name );
  }

  /**
   * Query the <code>selector</code> attribute.
   *
   * @return The value of the selector attribute.
   */
  public Selector getSelector() {
    int count = childCount( ID_SELECTOR );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_SELECTOR, null );
    return (Selector) it.next();
  }

  /**
   * Set the <code>selector</code> attribute.
   *
   * @param _selector Value to set for selector
   */
  public void setSelector( Selector _selector ) {
    int count = childCount( ID_SELECTOR );
    if (count > 0) {
      removeAll( ID_SELECTOR );
    }
    add( ID_SELECTOR, _selector );
  }

  public void addField( Field _field ) {
    add( ID_FIELD, _field );
  }

  public void removeField( Field _field ) {
    remove( ID_FIELD, _field );
  }

  public ModelIterator fieldIter() {
    return iterator( ID_FIELD, null );
  }

  public int fieldCount() {
    return childCount( ID_FIELD );
  }
}
