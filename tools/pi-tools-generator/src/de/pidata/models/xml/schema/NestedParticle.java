/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.tree.SequenceModel;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.Relation;
import de.pidata.models.types.complex.AnyElement;
import de.pidata.models.types.complex.DefaultRelation;
import de.pidata.qnames.QName;
import de.pidata.tools.CodeGenFactory;

import java.util.Collection;

public class NestedParticle {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_GROUP = NAMESPACE.getQName("group");
  public static final QName ID_SEQUENCE = NAMESPACE.getQName("sequence");
  public static final QName ID_CHOICE = NAMESPACE.getQName("choice");
  public static final QName ID_ELEMENT = NAMESPACE.getQName("element");
  public static final QName ID_ANY = NAMESPACE.getQName("any");

  protected ChildList children;
  protected SequenceModel model;

  public NestedParticle(SequenceModel model, ChildList children) {
    this.children = children;
    this.model = model;
  }

  public void addElement( LocalElement _localElement ) {
    this.model.add( ID_ELEMENT, _localElement );
  }

  public void removeElement( LocalElement _localElement ) {
    this.model.remove( ID_ELEMENT, _localElement );
  }

  public ModelIterator elementIter() {
    return this.model.iterator( ID_ELEMENT, null );
  }

  public int elementCount() {
    return this.model.childCount( ID_ELEMENT );
  }

  public void addGroup( GroupRef _group ) {
    this.model.add( ID_GROUP, _group );
  }

  public void removeGroup( GroupRef _group ) {
    this.model.remove( ID_GROUP, _group );
  }

  public ModelIterator groupIter() {
    return this.model.iterator( ID_GROUP, null );
  }

  public int groupCount() {
    return this.model.childCount( ID_GROUP );
  }

  public void addChoice( ExplicitGroup _choice ) {
    this.model.add( ID_CHOICE, _choice );
  }

  public void removeChoice( ExplicitGroup _choice ) {
    this.model.remove( ID_CHOICE, _choice );
  }

  public ModelIterator choiceIter() {
    return this.model.iterator( ID_CHOICE, null );
  }

  public int choiceCount() {
    return this.model.childCount( ID_CHOICE );
  }

  public void addSequence( ExplicitGroup _sequence ) {
    this.model.add( ID_SEQUENCE, _sequence );
  }

  public void removeSequence( ExplicitGroup _sequence ) {
    this.model.remove( ID_SEQUENCE, _sequence );
  }

  public ModelIterator sequenceIter() {
    return this.model.iterator( ID_SEQUENCE, null );
  }

  public int sequenceCount() {
    return this.model.childCount( ID_SEQUENCE );
  }

  public void addAny( Any _any ) {
    this.model.add( ID_ANY, _any );
  }

  public void removeAny( Any _any ) {
    this.model.remove( ID_ANY, _any );
  }

  public ModelIterator anyIter() {
    return this.model.iterator( ID_ANY, null );
  }

  public int anyCount() {
    return this.model.childCount( ID_ANY );
  }

  protected int childElementSum(Model model) {
    QName relationID = model.getParentRelationID();
    if (relationID == ID_ELEMENT) {
      return 1;
    }
    else if (relationID == ID_ANY) {
      return 1;
    }
    else if (relationID == ID_SEQUENCE) {
      ExplicitGroup sequence = (ExplicitGroup) model;
      return sequence.getNestedParticle().elementSum();
    }
    else if (relationID == ID_CHOICE) {
      ExplicitGroup choice = (ExplicitGroup) model;
      return choice.getNestedParticle().elementSum();
    }
    else if (relationID == ID_GROUP) {
      GroupRef groupRef =  (GroupRef) model;
      return groupRef.getGroup().getParticle().elementSum();
    }
    return 0;
  }

  public int elementSum() {
    int count = 0;
    Model model = children.getFirstChild(null);
    while (model != null) {
      count += childElementSum(model);
      model = model.nextSibling(null);
    }
    return count;
  }

  protected Relation getChildElement(Model model, QName elementName) throws ClassNotFoundException {
    Relation elem = null;
    int refMax = 0;
    QName relationID = model.getParentRelationID();
    if (relationID == ID_ELEMENT) {
      Element childElem = (Element) model;
      if (childElem.getName() == elementName) return childElem;
      if (childElem.getRef() == elementName) {
        refMax = elem.getMaxOccurs();
        elem = childElem.getRefElement();
      }
      else return null;
    }
    else if (relationID == ID_ANY) {
      Any any = (Any) model;
      Class collection = ((Any) model).getCollection();
      return new DefaultRelation( relationID, new AnyElement(), any.minOccurs(), any.maxOccurs(), collection, null );
    }
    else if (relationID == ID_SEQUENCE) {
      ExplicitGroup sequence = (ExplicitGroup) model;
      elem = sequence.getNestedParticle().getElement(elementName);
      if (elem == null) return null;
      refMax = sequence.maxOccurs();
    }
    else if (relationID == ID_CHOICE) {
      ExplicitGroup choice = (ExplicitGroup) model;
      elem = choice.getNestedParticle().getElement(elementName);
      if (elem == null) return null;
      refMax = choice.maxOccurs();
    }
    else if (relationID == ID_GROUP) {
      GroupRef groupRef =  (GroupRef) model;
      elem = groupRef.getGroup().getParticle().getElement(elementName);
      if (elem == null) return null;
      refMax = groupRef.maxOccurs();
    }
    if (elem != null) {
      if (elem.getMaxOccurs() < refMax) {
        String name = (String) model.get( CodeGenFactory.ID_COLLECTION );
        Class collection;
        if (name == null) {
          collection = Collection.class;
        }
        else {
          collection = Class.forName( name );
        }
        elem = new DefaultRelation( elementName, elem.getChildType(), elem.getMinOccurs(), refMax, collection, elem.getSubstitutionGroup() ) ;
      }
    }
    return elem;
  }

  protected Relation getChildElement(Model model, int index) throws ClassNotFoundException {
    QName relationID = model.getParentRelationID();
    if (relationID == ID_ELEMENT) {
      if (index == 0) {
        Element elem = (Element) model;
        if (elem.getRef() != null) return elem.getRefElement();
        else return elem;
      }
      return null;
    }
    else if (relationID == ID_ANY) {
      Any any = (Any) model;
      Class collection = ((Any) model).getCollection();
      return new DefaultRelation( relationID, new AnyElement(), any.minOccurs(), any.maxOccurs(), collection, null );
    }
    else if (relationID == ID_SEQUENCE) {
      ExplicitGroup sequence = (ExplicitGroup) model;
      return sequence.getNestedParticle().getElement(index);
    }
    else if (relationID == ID_CHOICE) {
      ExplicitGroup choice = (ExplicitGroup) model;
      return choice.getNestedParticle().getElement(index);
    }
    else if (relationID == ID_GROUP) {
      GroupRef groupRef =  (GroupRef) model;
      return groupRef.getGroup().getParticle().getElement(index);
    }
    return null;
  }

  public Relation getElement(QName elementName) throws ClassNotFoundException {
    Relation element;
    Model model = children.getFirstChild(null);
    while (model != null) {
      element = getChildElement(model, elementName);
      if (element != null) {
        return element;
      }
      model = model.nextSibling(null);
    }
    return null;
  }

  public Relation getElement(int index) throws ClassNotFoundException {
    Relation element;
    int count;
    Model model = children.getFirstChild(null);
    while (model != null) {
      count = childElementSum(model);
      if (index < count) {
        element = getChildElement(model, index);
        return element;
      }
      index -= count;
      model = model.nextSibling(null);
    }
    return null;
  }
}
