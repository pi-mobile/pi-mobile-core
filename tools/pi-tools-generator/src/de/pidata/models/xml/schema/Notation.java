/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;

import java.util.Hashtable;

public class Notation extends de.pidata.models.xml.schema.Annotated {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_PUBLIC = NAMESPACE.getQName("public");
  public static final QName ID_NAME = NAMESPACE.getQName("name");
  public static final QName ID_SYSTEM = NAMESPACE.getQName("system");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "notation" ), Notation.class.getName(), 1, de.pidata.models.xml.schema.Annotated.TYPE );
    TYPE = type;
    type.addKeyAttributeType( 0, ID_NAME, QNameType.getInstance());
    type.addAttributeType( ID_PUBLIC, SchemaFactory.PUBLIC);
    type.addAttributeType( ID_SYSTEM, StringType.getDefString());
  }

  public Notation() {
    this( null );
  }

  public Notation( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public Notation( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected Notation( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  /**
   * Query the <code>name</code> attribute.
   *
   * @return The value of the name attribute.
   */
  public QName getName() {
    return (QName) get( ID_NAME );
  }

  /**
   * Set the <code>name</code> attribute.
   *
   * @param _name Value to set for name
   */
  public void setName( QName _name ) {
    set( ID_NAME, _name );
  }

  /**
   * Query the <code>public</code> attribute.
   *
   * @return The value of the public attribute.
   */
  public String getPublic() {
    return (String) get( ID_PUBLIC );
  }

  /**
   * Set the <code>public</code> attribute.
   *
   * @param _public Value to set for public
   */
  public void setPublic( String _public ) {
    set( ID_PUBLIC, _public );
  }

  /**
   * Query the <code>system</code> attribute.
   *
   * @return The value of the system attribute.
   */
  public String getSystem() {
    return (String) get( ID_SYSTEM );
  }

  /**
   * Set the <code>system</code> attribute.
   *
   * @param _system Value to set for system
   */
  public void setSystem( String _system ) {
    set( ID_SYSTEM, _system );
  }
}
