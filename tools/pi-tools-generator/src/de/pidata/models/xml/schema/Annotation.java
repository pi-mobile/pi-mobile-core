/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;

import java.util.Hashtable;

public class Annotation extends de.pidata.models.xml.schema.OpenAttrs {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_ID = NAMESPACE.getQName("id");
  public static final QName ID_DOCUMENTATION = NAMESPACE.getQName("documentation");
  public static final QName ID_APPINFO = NAMESPACE.getQName("appinfo");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "annotation" ), Annotation.class.getName(), 0, de.pidata.models.xml.schema.OpenAttrs.TYPE );
    TYPE = type;
    type.addAttributeType( ID_ID, QNameType.getIDType());
    type.addRelation( ID_APPINFO, Appinfo.TYPE, 1, 1);
    type.addRelation( ID_DOCUMENTATION, Documentation.TYPE, 1, 1);
  }

  public Annotation() {
    this( null );
  }

  public Annotation( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public Annotation( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected Annotation( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  /**
   * Query the <code>id</code> attribute.
   *
   * @return The value of the id attribute.
   */
  public QName getId() {
    return (QName) get( ID_ID );
  }

  /**
   * Set the <code>id</code> attribute.
   *
   * @param _id Value to set for id
   */
  public void setId( QName _id ) {
    set( ID_ID, _id );
  }

  /**
   * Query the <code>appinfo</code> attribute.
   *
   * @return The value of the appinfo attribute.
   */
  public Appinfo getAppinfo() {
    int count = childCount( ID_APPINFO );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_APPINFO, null );
    return (Appinfo) it.next();
  }

  /**
   * Set the <code>appinfo</code> attribute.
   *
   * @param _appinfo Value to set for appinfo
   */
  public void setAppinfo( Appinfo _appinfo ) {
    int count = childCount( ID_APPINFO );
    if (count > 0) {
      removeAll( ID_APPINFO );
    }
    add( ID_APPINFO, _appinfo );
  }

  /**
   * Query the <code>documentation</code> attribute.
   *
   * @return The value of the documentation attribute.
   */
  public Documentation getDocumentation() {
    int count = childCount( ID_DOCUMENTATION );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_DOCUMENTATION, null );
    return (Documentation) it.next();
  }

  /**
   * Set the <code>documentation</code> attribute.
   *
   * @param _documentation Value to set for documentation
   */
  public void setDocumentation( Documentation _documentation ) {
    int count = childCount( ID_DOCUMENTATION );
    if (count > 0) {
      removeAll( ID_DOCUMENTATION );
    }
    add( ID_DOCUMENTATION, _documentation );
  }
}
