/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.tree.QNameIterator;
import de.pidata.models.tree.QNameIteratorEnum;
import de.pidata.models.tree.SequenceModel;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.Relation;
import de.pidata.qnames.QName;

import java.util.Vector;

/**
 * Provides helper methods for type definition particles according to the
 * group definition "typeDefParticle" of XMLSchema.xsd.
 */
public class TypeDefParticle {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );
  public static final QName ID_ALL = NAMESPACE.getQName("all");
  public static final QName ID_GROUP = NAMESPACE.getQName("group");
  public static final QName ID_SEQUENCE = NAMESPACE.getQName("sequence");
  public static final QName ID_CHOICE = NAMESPACE.getQName("choice");

  protected ChildList children;
  protected SequenceModel model;

  public TypeDefParticle(SequenceModel model, ChildList children) {
    this.children = children;
    this.model = model;
  }

  /**
   * Query the <code>group</code> attribute.
   *
   * @return The value of the group attribute.
   */
  public GroupRef getGroup() {
    int count = this.model.childCount( ID_GROUP );
    if (count == 0) {
      return null;
    }
    ModelIterator it = this.model.iterator( ID_GROUP, null );
    return (GroupRef) it.next();
  }

  /**
   * Set the <code>group</code> attribute.
   *
   * @param _group Value to set for group
   */
  public void setGroup( GroupRef _group ) {
    int count = this.model.childCount( ID_GROUP );
    if (count > 0) {
      this.model.removeAll( ID_GROUP );
    }
    this.model.add( ID_GROUP, _group );
  }

  /**
   * Query the <code>choice</code> attribute.
   *
   * @return The value of the choice attribute.
   */
  public ExplicitGroup getChoice() {
    int count = this.model.childCount( ID_CHOICE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = this.model.iterator( ID_CHOICE, null );
    return (ExplicitGroup) it.next();
  }

  /**
   * Set the <code>choice</code> attribute.
   *
   * @param _choice Value to set for choice
   */
  public void setChoice( ExplicitGroup _choice ) {
    int count = this.model.childCount( ID_CHOICE );
    if (count > 0) {
      this.model.removeAll( ID_CHOICE );
    }
    this.model.add( ID_CHOICE, _choice );
  }

  /**
   * Query the <code>sequence</code> attribute.
   *
   * @return The value of the sequence attribute.
   */
  public ExplicitGroup getSequence() {
    int count = this.model.childCount( ID_SEQUENCE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = this.model.iterator( ID_SEQUENCE, null );
    return (ExplicitGroup) it.next();
  }

  /**
   * Set the <code>sequence</code> attribute.
   *
   * @param _sequence Value to set for sequence
   */
  public void setSequence( ExplicitGroup _sequence ) {
    int count = this.model.childCount( ID_SEQUENCE );
    if (count > 0) {
      this.model.removeAll( ID_SEQUENCE );
    }
    this.model.add( ID_SEQUENCE, _sequence );
  }

  /**
   * Query the <code>all</code> attribute.
   *
   * @return The value of the all attribute.
   */
  public All getAll() {
    int count = this.model.childCount( ID_ALL );
    if (count == 0) {
      return null;
    }
    ModelIterator it = this.model.iterator( ID_ALL, null );
    return (All) it.next();
  }

  /**
   * Set the <code>all</code> attribute.
   *
   * @param _all Value to set for all
   */
  public void setAll( All _all ) {
    int count = this.model.childCount( ID_ALL );
    if (count > 0) {
      this.model.removeAll( ID_ALL );
    }
    this.model.add( ID_ALL, _all );
  }

  public int elementCount() {
    ExplicitGroup sequence = getSequence();
    if (sequence != null) {
      return sequence.getNestedParticle().elementSum();
    }
    ExplicitGroup choice = getChoice();
    if (choice != null) {
      return choice.getNestedParticle().elementSum();
    }
    GroupRef groupRef = getGroup();
    if (groupRef != null) {
      return groupRef.getGroup().getParticle().elementSum();
    }
    All all = getAll();
    if (all != null) {
      return all.getNestedParticle().elementSum();
    }
    return 0;
  }

  public Relation getElement(QName elementName) {
    try {
      ExplicitGroup sequence = getSequence();
      if (sequence != null) {
        return sequence.getNestedParticle().getElement(elementName);
      }
      ExplicitGroup choice = getChoice();
      if (choice != null) {
        return choice.getNestedParticle().getElement(elementName);
      }
      GroupRef groupRef = getGroup();
      if (groupRef != null) {
        return groupRef.getGroup().getParticle().getElement(elementName);
      }
      All all = getAll();
      if (all != null) {
        return all.getNestedParticle().getElement(elementName);
      }
      return null;
    }
    catch (ClassNotFoundException ex) {
      throw new IllegalArgumentException( "Error getting element", ex );
    }
  }

  public Relation getElement(int index) throws ClassNotFoundException {
    ExplicitGroup sequence = getSequence();
    if (sequence != null) {
      return sequence.getNestedParticle().getElement(index);
    }
    ExplicitGroup choice = getChoice();
    if (choice != null) {
      return choice.getNestedParticle().getElement(index);
    }
    GroupRef groupRef = getGroup();
    if (groupRef != null) {
      return groupRef.getGroup().getParticle().getElement(index);
    }
    All all = getAll();
    if (all != null) {
      return all.getNestedParticle().getElement(index);
    }
    return null;
  }

  public QNameIterator elementNameIter( QNameIterator baseIter ) {
    int elemCount = elementCount();
    if (elemCount == 0) {
      if (baseIter == null) return QNameIteratorEnum.EMPTY_ITERATOR;
      else return baseIter;
    }
    else {
      try {
        Vector names = new Vector();
        for (int i = 0; i < elemCount; i++) {
          names.addElement(getElement(i).getRelationID());
        }
        if (baseIter == null) return new QNameIteratorEnum( names.elements() );
        else return new QNameIteratorEnum( names.elements(), baseIter );
      }
      catch (ClassNotFoundException ex) {
        throw new IllegalArgumentException( "Error creating elementNameIter", ex );
      }
    }
  }
}
