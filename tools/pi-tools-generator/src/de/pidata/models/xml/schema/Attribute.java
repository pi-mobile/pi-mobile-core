/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.ModelFactory;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;

import java.util.Hashtable;

public class Attribute extends de.pidata.models.xml.schema.Annotated {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_FIXED = NAMESPACE.getQName("fixed");
  public static final QName ID_REF = NAMESPACE.getQName("ref");
  public static final QName ID_TYPE = NAMESPACE.getQName("type");
  public static final QName ID_FORM = NAMESPACE.getQName("form");
  public static final QName ID_NAME = NAMESPACE.getQName("name");
  public static final QName ID_SIMPLETYPE = NAMESPACE.getQName("simpleType");
  public static final QName ID_DEFAULT = NAMESPACE.getQName("default");
  public static final QName ID_USE = NAMESPACE.getQName("use");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "attribute" ), Attribute.class.getName(), 0, de.pidata.models.xml.schema.Annotated.TYPE );
    TYPE = type;
//    type.addKeyAttributeType( 0, ID_NAME, QNameType.getInstance());
    type.addAttributeType( ID_NAME, QNameType.getInstance());
    type.addAttributeType( ID_REF, QNameType.getInstance());
    type.addAttributeType( ID_TYPE, QNameType.getInstance());
    type.addAttributeType( ID_USE, new StringType( ID_USE, 0, Integer.MAX_VALUE ));
    type.addAttributeType( ID_DEFAULT, StringType.getDefString());
    type.addAttributeType( ID_FIXED, StringType.getDefString());
    type.addAttributeType( ID_FORM, SchemaFactory.FORMCHOICE);
    type.addRelation( ID_SIMPLETYPE, LocalSimpleType.TYPE, 0, 1);
  }

  public Attribute() {
    this( null );
  }

  public Attribute( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public Attribute( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected Attribute( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }

  /**
   * Query the <code>name</code> attribute.
   *
   * @return The value of the name attribute.
   */
  public QName getName() {
    return (QName) get( ID_NAME );
  }

  /**
   * Set the <code>name</code> attribute.
   *
   * @param _name Value to set for name
   */
  public void setName( QName _name ) {
    set( ID_NAME, _name );
  }

  /**
   * Query the <code>ref</code> attribute.
   *
   * @return The value of the ref attribute.
   */
  public QName getRef() {
    return (QName) get( ID_REF );
  }

  /**
   * Set the <code>ref</code> attribute.
   *
   * @param _ref Value to set for ref
   */
  public void setRef( QName _ref ) {
    set( ID_REF, _ref );
  }

  /**
   * Query the <code>type</code> attribute.
   *
   * @return The value of the type attribute.
   */
  public QName getType() {
    return (QName) get( ID_TYPE );
  }

  /**
   * Set the <code>type</code> attribute.
   *
   * @param _type Value to set for type
   */
  public void setType( QName _type ) {
    set( ID_TYPE, _type );
  }

  /**
   * Query the <code>use</code> attribute.
   *
   * @return The value of the use attribute.
   */
  public String getUse() {
    return (String) get( ID_USE );
  }

  /**
   * Set the <code>use</code> attribute.
   *
   * @param _use Value to set for use
   */
  public void setUse( String _use ) {
    set( ID_USE, _use );
  }

  /**
   * Query the <code>default</code> attribute.
   *
   * @return The value of the default attribute.
   */
  public String getDefault() {
    return (String) get( ID_DEFAULT );
  }

  /**
   * Set the <code>default</code> attribute.
   *
   * @param _default Value to set for default
   */
  public void setDefault( String _default ) {
    set( ID_DEFAULT, _default );
  }

  /**
   * Query the <code>fixed</code> attribute.
   *
   * @return The value of the fixed attribute.
   */
  public String getFixed() {
    return (String) get( ID_FIXED );
  }

  /**
   * Set the <code>fixed</code> attribute.
   *
   * @param _fixed Value to set for fixed
   */
  public void setFixed( String _fixed ) {
    set( ID_FIXED, _fixed );
  }

  /**
   * Query the <code>form</code> attribute.
   *
   * @return The value of the form attribute.
   */
  public String getForm() {
    return (String) get( ID_FORM );
  }

  /**
   * Set the <code>form</code> attribute.
   *
   * @param _form Value to set for form
   */
  public void setForm( String _form ) {
    set( ID_FORM, _form );
  }

  /**
   * Query the <code>simpleType</code> attribute.
   *
   * @return The value of the simpleType attribute.
   */
  public LocalSimpleType getSimpleType() {
    int count = childCount( ID_SIMPLETYPE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_SIMPLETYPE, null );
    return (LocalSimpleType) it.next();
  }

  /**
   * Set the <code>simpleType</code> attribute.
   *
   * @param _simpleType Value to set for simpleType
   */
  public void setSimpleType( LocalSimpleType _simpleType ) {
    int count = childCount( ID_SIMPLETYPE );
    if (count > 0) {
      removeAll( ID_SIMPLETYPE );
    }
    add( ID_SIMPLETYPE, _simpleType );
  }

  //==============================================================
  // End of generated code. checksum=
  //==============================================================

  public SimpleType getRefAttribute() {
    QName ref = getRef();
    if (ref == null) {
      return null;
    }
    else {
      return getResolver().getAttribute( ref );
    }
  }

  public SimpleType getAttributeType() {
    QName typeName = getType();
    if (typeName == null) {
      QName ref = getRef();
      if (ref == null) {
        SimpleType type = getSimpleType();
        if (type == null) {
          return StringType.getDefString();
        }
        else {
          return type;
        }
      }
      else {
        SimpleType refType = getRefAttribute();
        if (refType == null) {
          throw new IllegalArgumentException( "Could not find refAttribute="+getRef() );
        }
        else {
          typeName = refType.name();
        }
      }
    }
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( typeName.getNamespace() );
    if (factory == null) {
      throw new IllegalArgumentException("No factory found for type name="+typeName);
    }
    return (SimpleType) factory.getType(typeName);
  }

  public QName getAttributeName() {
    QName name = getName();
    if (name == null) {
      return getRef();
    }
    else {
      return name;
    }
  }
}
