/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;

import java.util.Iterator;
import java.util.NoSuchElementException;

public abstract class AllSchemaIterator<E extends Model> implements ModelIterator<E> {

  private final ModelIterator<ChildSchema> childSchemaIterator;

  private E next = null;
  private boolean hasNext = true;
  private Schema currentSchema;
  protected ModelIterator<E> currentItemIter;

  public AllSchemaIterator( RootSchema schema ) {
    childSchemaIterator = schema.allChildSchemaIterator();
    currentSchema = schema;
  }

  /**
   * Returns an iterator over elements of type {@code T}.
   *
   * @return an Iterator.
   */
  @Override
  public Iterator<E> iterator() {
    return this;
  }

  /**
   * Returns {@code true} if the iteration has more elements.
   * (In other words, returns {@code true} if {@link #next} would
   * return an element rather than throwing an exception.)
   *
   * @return {@code true} if the iteration has more elements
   */
  @Override
  public boolean hasNext() {
    return hasNext;
  }

  /**
   * Returns the next element in the iteration.
   *
   * @return the next element in the iteration
   * @throws NoSuchElementException if the iteration has no more elements
   */
  @Override
  public E next() {
    E current;
    if (!hasNext) {
      throw new NoSuchElementException( "No more Elements in iterator." );
    }
    current = next;
    findNext();
    return current;
  }

  public void findNext() {
    if (currentItemIter.hasNext()) {
      next = currentItemIter.next();
      return;
    }
    if (childSchemaIterator.hasNext()) {
      currentSchema = childSchemaIterator.next();
      currentItemIter = getItemIterator( currentSchema );
      findNext();
      return;
    }
    hasNext = false;
    next = null;
  }

  protected abstract ModelIterator<E> getItemIterator( Schema currentSchema );
}
