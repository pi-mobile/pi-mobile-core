/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.xml.schema;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Namespace;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.QName;

import java.util.Hashtable;

public class LocalComplexType extends de.pidata.models.xml.schema.ComplexTypeDef {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final QName ID_ATTRIBUTEGROUP = NAMESPACE.getQName("attributeGroup");
  public static final QName ID_ATTRIBUTE = NAMESPACE.getQName("attribute");
  public static final QName ID_SEQUENCE = NAMESPACE.getQName("sequence");
  public static final QName ID_COMPLEXCONTENT = NAMESPACE.getQName("complexContent");
  public static final QName ID_ANYATTRIBUTE = NAMESPACE.getQName("anyAttribute");
  public static final QName ID_GROUP = NAMESPACE.getQName("group");
  public static final QName ID_CHOICE = NAMESPACE.getQName("choice");
  public static final QName ID_SIMPLECONTENT = NAMESPACE.getQName("simpleContent");
  public static final QName ID_ALL = NAMESPACE.getQName("all");
  public static final QName ID_ANNOTATION = NAMESPACE.getQName("annotation");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "localComplexType" ), LocalComplexType.class.getName(), 0, de.pidata.models.xml.schema.ComplexTypeDef.TYPE );
    TYPE = type;
  }

  static void buildType() {
    DefaultComplexType type = (DefaultComplexType) TYPE;
    type.addAttributeType( AnyAttribute.ANY_ATTRIBUTE, AnyAttribute.ANY_ATTR_TYPE );
    type.addRelation( ID_ANNOTATION, Annotation.TYPE, 0, 1);
    type.addRelation( ID_SIMPLECONTENT, SimpleContent.TYPE, 1, 1);
    type.addRelation( ID_COMPLEXCONTENT, ComplexContent.TYPE, 1, 1);
    type.addRelation( ID_GROUP, GroupRef.TYPE, 1, 1);
    type.addRelation( ID_ALL, All.TYPE, 1, 1);
    type.addRelation( ID_CHOICE, ExplicitGroup.TYPE, 1, 1);
    type.addRelation( ID_SEQUENCE, ExplicitGroup.TYPE, 1, 1);
    type.addRelation( ID_ATTRIBUTE, Attribute.TYPE, 1, 1);
    type.addRelation( ID_ATTRIBUTEGROUP, AttributeGroupRef.TYPE, 1, 1);
    type.addRelation( ID_ANYATTRIBUTE, Wildcard.TYPE, 1, 1);
  }

  public LocalComplexType() {
    this( null );
  }

  public LocalComplexType( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public LocalComplexType( QName modelID, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, TYPE, attributes, anyAttribs, children );
  }

  protected LocalComplexType( QName modelID, ComplexType type, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {
    super( modelID, type, attributes, anyAttribs, children );
  }


  /**
   * Query the <code>annotation</code> attribute.
   *
   * @return The value of the annotation attribute.
   */
  public Annotation getAnnotation() {
    int count = childCount( ID_ANNOTATION );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_ANNOTATION, null );
    return (Annotation) it.next();
  }

  /**
   * Set the <code>annotation</code> attribute.
   *
   * @param _annotation Value to set for annotation
   */
  public void setAnnotation( Annotation _annotation ) {
    int count = childCount( ID_ANNOTATION );
    if (count > 0) {
      removeAll( ID_ANNOTATION );
    }
    add( ID_ANNOTATION, _annotation );
  }

  @Override
  public SimpleType getContentType() {
    if (getMixed()) {
      return null;
    }
    else {
      return StringType.getDefString();
    }
  }

  /**
   * Query the <code>simpleContent</code> attribute.
   *
   * @return The value of the simpleContent attribute.
   */
  public SimpleContent getSimpleContent() {
    int count = childCount( ID_SIMPLECONTENT );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_SIMPLECONTENT, null );
    return (SimpleContent) it.next();
  }

  /**
   * Set the <code>simpleContent</code> attribute.
   *
   * @param _simpleContent Value to set for simpleContent
   */
  public void setSimpleContent( SimpleContent _simpleContent ) {
    int count = childCount( ID_SIMPLECONTENT );
    if (count > 0) {
      removeAll( ID_SIMPLECONTENT );
    }
    add( ID_SIMPLECONTENT, _simpleContent );
  }

  /**
   * Query the <code>complexContent</code> attribute.
   *
   * @return The value of the complexContent attribute.
   */
  public ComplexContent getComplexContent() {
    int count = childCount( ID_COMPLEXCONTENT );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_COMPLEXCONTENT, null );
    return (ComplexContent) it.next();
  }

  /**
   * Set the <code>complexContent</code> attribute.
   *
   * @param _complexContent Value to set for complexContent
   */
  public void setComplexContent( ComplexContent _complexContent ) {
    int count = childCount( ID_COMPLEXCONTENT );
    if (count > 0) {
      removeAll( ID_COMPLEXCONTENT );
    }
    add( ID_COMPLEXCONTENT, _complexContent );
  }

  /**
   * Query the <code>group</code> attribute.
   *
   * @return The value of the group attribute.
   */
  public GroupRef getGroup() {
    int count = childCount( ID_GROUP );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_GROUP, null );
    return (GroupRef) it.next();
  }

  /**
   * Set the <code>group</code> attribute.
   *
   * @param _group Value to set for group
   */
  public void setGroup( GroupRef _group ) {
    int count = childCount( ID_GROUP );
    if (count > 0) {
      removeAll( ID_GROUP );
    }
    add( ID_GROUP, _group );
  }

  /**
   * Query the <code>all</code> attribute.
   *
   * @return The value of the all attribute.
   */
  public All getAll() {
    int count = childCount( ID_ALL );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_ALL, null );
    return (All) it.next();
  }

  /**
   * Set the <code>all</code> attribute.
   *
   * @param _all Value to set for all
   */
  public void setAll( All _all ) {
    int count = childCount( ID_ALL );
    if (count > 0) {
      removeAll( ID_ALL );
    }
    add( ID_ALL, _all );
  }

  /**
   * Query the <code>choice</code> attribute.
   *
   * @return The value of the choice attribute.
   */
  public ExplicitGroup getChoice() {
    int count = childCount( ID_CHOICE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_CHOICE, null );
    return (ExplicitGroup) it.next();
  }

  /**
   * Set the <code>choice</code> attribute.
   *
   * @param _choice Value to set for choice
   */
  public void setChoice( ExplicitGroup _choice ) {
    int count = childCount( ID_CHOICE );
    if (count > 0) {
      removeAll( ID_CHOICE );
    }
    add( ID_CHOICE, _choice );
  }

  /**
   * Query the <code>sequence</code> attribute.
   *
   * @return The value of the sequence attribute.
   */
  public ExplicitGroup getSequence() {
    int count = childCount( ID_SEQUENCE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_SEQUENCE, null );
    return (ExplicitGroup) it.next();
  }

  /**
   * Set the <code>sequence</code> attribute.
   *
   * @param _sequence Value to set for sequence
   */
  public void setSequence( ExplicitGroup _sequence ) {
    int count = childCount( ID_SEQUENCE );
    if (count > 0) {
      removeAll( ID_SEQUENCE );
    }
    add( ID_SEQUENCE, _sequence );
  }

  /**
   * Query the <code>attribute</code> attribute.
   *
   * @return The value of the attribute attribute.
   */
  public Attribute getAttribute() {
    int count = childCount( ID_ATTRIBUTE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_ATTRIBUTE, null );
    return (Attribute) it.next();
  }

  /**
   * Set the <code>attribute</code> attribute.
   *
   * @param _attribute Value to set for attribute
   */
  public void setAttribute( Attribute _attribute ) {
    int count = childCount( ID_ATTRIBUTE );
    if (count > 0) {
      removeAll( ID_ATTRIBUTE );
    }
    add( ID_ATTRIBUTE, _attribute );
  }

  /**
   * Query the <code>attributeGroup</code> attribute.
   *
   * @return The value of the attributeGroup attribute.
   */
  public AttributeGroupRef getAttributeGroup() {
    int count = childCount( ID_ATTRIBUTEGROUP );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_ATTRIBUTEGROUP, null );
    return (AttributeGroupRef) it.next();
  }

  /**
   * Set the <code>attributeGroup</code> attribute.
   *
   * @param _attributeGroup Value to set for attributeGroup
   */
  public void setAttributeGroup( AttributeGroupRef _attributeGroup ) {
    int count = childCount( ID_ATTRIBUTEGROUP );
    if (count > 0) {
      removeAll( ID_ATTRIBUTEGROUP );
    }
    add( ID_ATTRIBUTEGROUP, _attributeGroup );
  }

  /**
   * Query the <code>anyAttribute</code> attribute.
   *
   * @return The value of the anyAttribute attribute.
   */
  public Wildcard get_AnyAttribute() {
    int count = childCount( ID_ANYATTRIBUTE );
    if (count == 0) {
      return null;
    }
    ModelIterator it = iterator( ID_ANYATTRIBUTE, null );
    return (Wildcard) it.next();
  }

  /**
   * Set the <code>anyAttribute</code> attribute.
   *
   * @param _anyAttribute Value to set for anyAttribute
   */
  public void set_AnyAttribute( Wildcard _anyAttribute ) {
    int count = childCount( ID_ANYATTRIBUTE );
    if (count > 0) {
      removeAll( ID_ANYATTRIBUTE );
    }
    add( ID_ANYATTRIBUTE, _anyAttribute );
  }

  //==============================================================
  // End of generated code. checksum=
  //==============================================================

  /**
   * Builds a unique generic name for the anonymous local type.
   *
   * @return
   */
  public QName name() {
    Model parent = getParent( true );
    if (parent == null) {
      return null;
    }
    else {
      Element parentElement = (Element) parent;
      QName parentElementName = parentElement.getName();
      Model parentType = parentElement.getParent( false );
      while (parentType != null && !(parentType instanceof ComplexType)) {
        parentType = parentType.getParent( false );
      }
      if (parentType != null) {
        Namespace namespace = parentElementName.getNamespace();
        QName parentTypeName = ((ComplexType) parentType).name();
        return namespace.getQName( parentTypeName.getName() + ".local." + parentElementName.getName() );
      }
      else {
        return parentElementName;
      }
    }
  }
}
