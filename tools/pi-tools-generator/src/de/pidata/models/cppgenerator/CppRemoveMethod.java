/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.cppgenerator;

import de.pidata.models.tree.SimpleModel;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

import java.io.PrintWriter;

public class CppRemoveMethod extends CppMethodGenerator {

  private QName memberName;
  private Type type;

  public CppRemoveMethod( CppClassGenerator parentClass, QName memberName, Type type ) {
    super( parentClass, "remove" + upperCaseFirst( memberName.getName() ) );
    this.memberName = memberName;
    this.type = type;
  }

  protected void writeMethod( PrintWriter pw ) {
    String classForType;
    classForType = getClassForType( type, true );
    String variable = lowerCaseFirst( convertToValidlName( memberName.getName() ) );
    String variableName = ensureNonColidingMemberName( variable );
    writeMethodDeclaration( parentClass.getName() + "::", pw, variableName, classForType );
    pw.println( " {" );
    pw.println( "  int pos = -1;" );
    pw.println( "  for (int i = 0; i < this->"+variable+"_count; i++) {" );
    pw.println( "    if (pos >= 0) {" );
    pw.println( "      this->"+variableName+"[i-1] = this->"+variableName+"[i];" );
    pw.println( "    }" );
    pw.println( "    else if (&(this->"+variableName+"[i]) == "+variableName+") {" );
    pw.println( "      pos = i; " );
    pw.println( "    }" );
    pw.println( "  }" );
    pw.println( "  if (pos >= 0) {" );
    pw.println( "    this->"+variable+"_count--;" );
    pw.println( "  }" );
    pw.println( "}" );
  }

  protected void writeMethodInterface( PrintWriter pw ) {
    String classForType;
    classForType = getClassForType( type, true );
    String variable = ensureNonColidingMemberName( lowerCaseFirst( convertToValidlName( memberName.getName() ) ) );
    pw.print( "  " );
    writeMethodDeclaration( "", pw, variable, classForType );
    pw.println( ";" );
  }

  private void writeMethodDeclaration( String prefix, PrintWriter pw, String variable, String classForType ) {
    if (type instanceof SimpleType) {
      pw.print( "void " + prefix + getName() + "( SimpleModel " + variable + " )" );
    }
    else {
      pw.print( "void " + prefix + getName() + "( " + classForType + " " + variable + " )" );
    }
  }
}
