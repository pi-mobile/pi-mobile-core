/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.cppgenerator;

import de.pidata.models.generator.TypeClassGenerator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.simple.Restriction;
import de.pidata.models.xml.schema.SimpleTypeDef;
import de.pidata.qnames.QName;

import java.io.PrintWriter;
import java.util.List;

public class CppWriteAttrMethod extends CppMethodGenerator{

  private ComplexType type;

  public CppWriteAttrMethod( CppClassGenerator parentClass, ComplexType type ) {
    super( parentClass, "writeAttr" );
    this.type = type;
  }

  protected void writeMethod( PrintWriter pw ) {
    writeMethodDeclaration( parentClass.getName() + "::", pw );
    pw.println( " {" );
    ComplexType parentType = (ComplexType) type.getBaseType();
    if (parentType != null) {
      String parentClassName = getClassForType( parentType, false );
      pw.println( "  " + parentClassName + "::writeAttr( xmlWriter );" );
    }
    for (int i = ((CppTypeClassGenerator) parentClass).firstAttrIndex(); i < type.attributeCount(); i++) {
      SimpleType attrType = type.getAttributeType( i );
      String variable = ensureNonColidingMemberName( lowerCaseFirst( convertToValidlName( type.getAttributeName( i ).getName() ) ) );
      String attrIdConstant = createIDConstant( type.getAttributeName(i) );
      boolean done = false;
      pw.print( "  xmlWriter->attribute( " + attrIdConstant + "," );
      if (attrType instanceof SimpleTypeDef) {
        Restriction restriction = ((SimpleTypeDef) attrType).getRestriction();
        if (restriction != null) {
          List<String> enumeration = restriction.enumeration();
          if ((enumeration != null) && (enumeration.size() > 0)) {
            String enumStrArrayName = getEnumStrArrayName( attrType );
            pw.print( "(char *) " + enumStrArrayName + "[this->" + variable +"]" );
            done = true;
          }
        }
      }
      if (!done) {
        pw.print( " this->" + variable );
      }
      pw.println( " );" );
    }
    pw.println( "}" );
  }

  protected void writeMethodInterface( PrintWriter pw ) {
    pw.print( "  virtual " );
    writeMethodDeclaration( "", pw );
    pw.println( ";" );
  }

  private void writeMethodDeclaration( String prefix, PrintWriter pw ) {
    pw.print( "void "+prefix+"writeAttr( XmlWriter* xmlWriter )" );
  }

}
