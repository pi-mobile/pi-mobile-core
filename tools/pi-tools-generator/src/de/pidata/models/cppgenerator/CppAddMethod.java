/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.cppgenerator;

import de.pidata.models.tree.SimpleModel;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

import java.io.PrintWriter;

public class CppAddMethod extends CppMethodGenerator {

  private QName memberName;
  private Type type;

  public CppAddMethod( CppClassGenerator parentClass, QName memberName, Type type ) {
    super( parentClass, "add" + upperCaseFirst( memberName.getName() ) );
    this.memberName = memberName;
    this.type = type;
  }

  protected void writeMethod( PrintWriter pw ) {
    String typeClassName = getClassForType( type, true );
    String variable = lowerCaseFirst( convertToValidlName( memberName.getName() ) );
    writeMethodDeclaration( parentClass.getName() + "::", pw, typeClassName );
    pw.println( " {" );
    pw.println( "  if (this->"+variable+"_count >= "+parentClass.getDimVariable( memberName.getName() )+") {" );
    pw.println( "        fatalError( \""+parentClass.getName()+"::"+getName()+" - Array Overflow for "+variable+"_count\" );" );
    pw.println( "  }" );
    pw.println( "  int index = this->"+variable+"_count++;" );
    pw.println( "  return get"+upperCaseFirst( variable )+"At( index );" );
    pw.println( "}" );
  }

  protected void writeMethodInterface( PrintWriter pw ) {
    String typeClassName = getClassForType( type, true );
    pw.print( "  " );
    writeMethodDeclaration( "", pw, typeClassName );
    pw.println( ";" );
  }

  private void writeMethodDeclaration( String prefix, PrintWriter pw, String typeClassName ) {
    pw.print( typeClassName + " " + prefix + getName() + "()" );
  }
}
