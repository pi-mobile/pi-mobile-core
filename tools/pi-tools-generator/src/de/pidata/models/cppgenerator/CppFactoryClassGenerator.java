/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.cppgenerator;

import de.pidata.log.Logger;
import de.pidata.models.tree.DefaultModel;
import de.pidata.models.tree.QNameIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.simple.*;
import de.pidata.models.xml.schema.SimpleTypeDef;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.tools.CodeGenFactory;

import java.io.CharArrayWriter;
import java.io.PrintWriter;
import java.util.*;

public class CppFactoryClassGenerator extends CppClassGenerator {
  private List<SimpleTypeDef> simpleTypes = new ArrayList<SimpleTypeDef>();
  private List<ComplexType> complexTypes = new ArrayList<ComplexType>();
  private Vector rootElements = new Vector();
  private Hashtable rootAttributes = new Hashtable();

  private int nsCounter = 0;
  private HashMap<Namespace, String> nsTable = new HashMap<Namespace, String>();
  private HashMap<String, QName> idTable = new HashMap<String, QName>();
  private HashMap<String, String> dimVariables = new HashMap<String, String>();
  protected boolean writeNamespaces = true;

  private String schemaLocation;
  private String schemaVersion;

  private CharArrayWriter contantsCW;
  private CharArrayWriter staticCW;

  public CppFactoryClassGenerator( String className, Namespace targetNamespace, String outputDirName, String schemaLocation, String schemaVersion) {
    super(className, targetNamespace, null, outputDirName);
    this.schemaLocation = schemaLocation;
    this.schemaVersion = schemaVersion;

    addImport( CPP_CLASS_NAMESPACE, true );
    addImport( CPP_CLASS_QNAME, true );
    addImport( "Model", true );

    addNamespace( targetNamespace, "NS_"+className.toUpperCase() );
  }

  @Override
  public CppFactoryClassGenerator getFactoryClassGenerator() {
    return this;
  }

  protected void addNamespace( Namespace targetNamespace, String namespace ) {
    nsTable.put( targetNamespace, namespace );
  }

  /**
   * Get an identifier to be used for that Namespace during code generation. For each namespace
   * a public static field will be created with this identifier.
   *
   * @param namespace
   * @return
   */
  protected String getNamespaceConstant( Namespace namespace ) {
    String id = this.nsTable.get( namespace );
    if (id == null) {
      id = "XMLNS_NS" + nsCounter++;
      nsTable.put( namespace, id );
    }
    return id;
  }

  @Override
  public String createIDConstant( QName valueName ) {
    String constant = "ID_" + CppClassGenerator.toConstantName( valueName.getName() );
    this.idTable.put( constant, valueName );
    return lowerCaseFirst( getName() ) + "->" + constant;
  }

  public String getDimStructName() {
    return lowerCaseFirst( getName() ) + "Dim";
  }

  public String getDimVariable( String relName ) {
    String varName = dimVariables.get( relName );
    if (varName == null) {
      varName = "max_"+relName+"_count";
      dimVariables.put( relName, varName );
    }
    return getDimStructName() + "." + varName;
  }

  public void addSimpleType( SimpleTypeDef simpleType, boolean asConstant) {
    this.simpleTypes.add( simpleType );
  }

  public void addComplexType( ComplexType complexType, boolean asConstant) {
    this.complexTypes.add(complexType);
  }

  public void addRootElement( Relation rootRel) {
    this.rootElements.addElement(rootRel);
    createIDConstant( rootRel.getRelationID() );
  }

  public void addRootAttribute(QName name, SimpleType type) {
    this.rootAttributes.put( name, type );
    createIDConstant( name );
  }

  private void createSimpleType( PrintWriter pw, String typeName, SimpleTypeDef type, boolean asConstant, StringBuilder enumStrBuilder ) {
    QName typeID = type.name();
    String baseTypeName = null;
    String params = null;
    SimpleType baseType = (SimpleType) type.getBaseType();
    boolean minIncl = true;
    boolean maxIncl = true;
    Restriction restriction = type.getRestriction();
    String temp;
    String implementation = (String) type.get( CodeGenFactory.ID_IMPLEMENTATION );

    if (baseType instanceof QNameType) {
      baseTypeName = "QNameType";
      int min, max;
      if (restriction == null) {
        min = 0;
        max = Integer.MAX_VALUE;
      }
      else {
        min = restriction.minLength();
        max = restriction.maxLength();
      }
      params = Integer.toString(min) + ", ";
      if (max == Integer.MAX_VALUE) {
        params += "Integer.MAX_VALUE";
      }
      else {
        params += Integer.toString(max);
      }
    }
    else if (baseType instanceof IntegerType) {
      baseTypeName = "IntegerType";
//      if (valueClass.getName().endsWith("Integer")) {
//        result = new Integer((int)resultSet.getInt(columnName));
//      }
//      if (valueClass.getName().endsWith("Long")) {
//        result = new Long(resultSet.getInt(columnName));
//      }
//      if (valueClass.getName().endsWith("Short")) {
//        result = new Short((short) resultSet.getInt(columnName));
//      }
//      if (valueClass.getName().endsWith("Byte")) {
//        result = new Byte((byte) resultSet.getInt(columnName));
//      }

      int min, max;
      if (restriction == null) {
        min = 0;
        max = Integer.MAX_VALUE;
        minIncl = true;
        maxIncl = true;
      }
      else {
        min = restriction.minLength();
        max = restriction.maxLength();
        //TODO minIncl = restriction.minInclusive();
        //TODO maxIncl = restriction.maxInclusive();
      }
      params = "Integer.class, IntegerType.getDefInt(), "; //todo: unterscheiden in long/integer/...
      //if (max == Integer.MAX_VALUE) {
      params += "Integer.MAX_VALUE";
      //}
      //else {
      //  params += Integer.toString(max);
      //}
      params += ", " + Integer.toString(min) + ", ";
      params += Boolean.toString(maxIncl) + ", " + Boolean.toString(minIncl);
    }
    else if (baseType instanceof DecimalType) {
      baseTypeName = "DecimalType";
      DecimalType decType = (DecimalType) baseType;
      int min, max, totalDigits, fractionDigits;
      if (restriction == null) {
        totalDigits = decType.getTotalDigits();
        fractionDigits = decType.getFractionDigits();
      }
      else {
        totalDigits = restriction.totalDigits();
        fractionDigits = restriction.fractionDigits();
      }
      params = Integer.toString(totalDigits) + ", " + Integer.toString(fractionDigits);
    }
    //TODO weitere restrictions implementieren, StringType als letzten lassen
    else if (baseType instanceof StringType) {
      if (restriction == null) {
        params = null;
      }
      else {
        List<String> enumList = restriction.enumeration();
        if (enumList != null) {
          // static const char * functionType_names[] { "pulse", "switch", "toggle", "int" };
          // enum pinType_t { MOTOR_PWM, MOTOR_DIR, LIGHT_FRONT, LIGHT_BACK, LOGIC, PWM, LOW_SIDE, HIGH_SIDE };
          StringBuilder builder = new StringBuilder( "enum ");
          enumStrBuilder.append( "  const char*  ");
          builder.append( getClassForType( type, true ) );
          enumStrBuilder.append( typeName ).append( "_names["+enumList.size()+"] = { " );
          builder.append( " { " );
          boolean first = true;
          for (String str : enumList) {
            if (first) {
              first = false;
            }
            else {
              builder.append( ", " );
              enumStrBuilder.append( ", " );
            }
            builder.append( CppClassGenerator.toConstantName( typeName + "_" + str ) );
            enumStrBuilder.append( "\"" ).append( str ).append( "\"" );
          }
          builder.append( " };" );
          enumStrBuilder.append( " };\n" );
          pw.println( builder.toString() );
          pw.println();
        }
      }
    }
  }

  protected void writeIDConstants( PrintWriter pw ) {
    addImport( CPP_CLASS_QNAME, true );
    ArrayList<String> keyList = new ArrayList<String>( idTable.keySet() );
    Collections.sort( keyList );
    for (String id : keyList) {
      pw.print( "  const QName* " );
      pw.print( id );
      QName name = idTable.get( id );
      Namespace ns = name.getNamespace();
      String nsConstant = nsTable.get( ns );
      if (ns != null) {
        pw.print( " = "+nsConstant );
      }
      else {
        pw.print( " = Namespace::getInstance( \"" + ns.getUri() + "\" )" );
      }
      pw.print( "->getQName( \"" + name.getName() );
      pw.println( "\" );" );
    }
  }

  protected void writeConstants( PrintWriter pw ) {
    pw.println( "public:" );
    if (writeNamespaces) {
      Iterator it = nsTable.keySet().iterator();
      while (it.hasNext()) {
        Namespace ns = (Namespace) it.next();
        String id = nsTable.get( ns );
        pw.println( "  Namespace* " + id + " = Namespace::getInstance( \"" + ns.getUri() + "\" );" );
      }
      pw.println();
    }
    writeIDConstants( pw );
  }

  protected void writeTypeDefs( PrintWriter pw, StringBuilder enumStrBuilder ) {
    pw.println();
    for (int typeNum = 0; typeNum < this.simpleTypes.size(); typeNum++) {
      SimpleTypeDef simpleType = simpleTypes.get( typeNum );
      createSimpleType( pw, simpleType.name().getName(), simpleType, true, enumStrBuilder );
    }
  }

  private void insertTypeDef( PrintWriter pw, ComplexType type, boolean typeDeclared, CppFactoryClassGenerator factoryClassGenerator ) {
    if (!typeDeclared) {
      pw.println( "    DefaultComplexType type;");
    }
    pw.println( "    type = new DefaultComplexType( null, \"" + DefaultModel.class.getName() + "\" );" );

    int count = type.attributeCount();
    for (int i = 0; i < type.attributeCount(); i++) {
      QName attrName = type.getAttributeName(i);
      SimpleType attrType = type.getAttributeType(i);
      Object attrDefault = type.getAttributeDefault( i );
      int keyIndex = type.getKeyIndex(attrName);
      writeAttributeDef( pw, keyIndex, attrName, attrType, attrDefault, getName() );
    }
    AnyAttribute attrAny = type.getAnyAttribute();
    if (attrAny != null) {
      writeAttributeDef( pw, -1, attrAny.name(), attrAny, attrAny.createDefaultValue(), getName() );
    }

    for (QNameIterator relIter = type.relationNames(); relIter.hasNext(); ) {
      QName relName = relIter.next();
      Relation childRel = type.getRelation(relName);
      writeRelation( pw, childRel, false, factoryClassGenerator );
    }
  }

  protected void writeConstructor( PrintWriter headerPW, PrintWriter pw ) {
    boolean typeDeclared = false;
    String name;
    //----- constructor
    pw.print("  public " + getName() + "() {\n");
    pw.print("    super( NAMESPACE, \"");
    pw.print(this.schemaLocation + "\", \""+ this.schemaVersion +"\" );\n");
    int elemCount = complexTypes.size() + simpleTypes.size();
    for (int typeNum = 0; typeNum < this.simpleTypes.size(); typeNum++) {
      SimpleTypeDef simpleType = simpleTypes.get( typeNum );
      pw.print("    addType( " + CppClassGenerator.toConstantName((simpleType.name().getName())) + " );\n");
    }
    for (int typeNum = 0; typeNum < this.complexTypes.size(); typeNum++) {
      ComplexType complexType = complexTypes.get(typeNum);
      pw.print("    addType( " + getTypeConstant(complexType) + " );\n");
    }
    for (int i = 0; i < rootElements.size(); i++) {
      Relation relation = (Relation) rootElements.elementAt(i);
      Type childType = relation.getChildType();
      if (childType.name() == null) {
        insertTypeDef( pw, (ComplexType) relation.getChildType(), typeDeclared, this );
        typeDeclared = true;
        name = "type";
      }
      else{
        name = getTypeConstant(relation.getChildType());
      }
      pw.print("    addRootRelation( "+createIDConstant( relation.getRelationID() ) + ", ");
      pw.print(name + ", " + relation.getMinOccurs());
      pw.print(", " + relation.getMaxOccurs() + ");\n");
    }
    if (this.rootAttributes.size() > 0) {
      pw.print("    attributes = new Hashtable();\n");
      for (Enumeration attrEnum = this.rootAttributes.keys(); attrEnum.hasMoreElements();) {
        QName attrName = (QName) attrEnum.nextElement();
        SimpleType attrType = (SimpleType) this.rootAttributes.get(attrName);
        String typeName =  getTypeConstant(attrType);
        pw.print("    attributes.put( "+createIDConstant( attrName ) );
        pw.print(", " + typeName + ");\n");
      }
    }
    pw.println("  }");
    pw.println();
  }

  @Override
  protected void writeFields( PrintWriter headerPW ) {
    // no fields
  }

  private void writeCreateInstance( PrintWriter headerPW, PrintWriter pw ) {
    pw.print("  public Model createInstance(Key key, Type typeDef, Object[] attributes, Hashtable anyAttribs, ChildList children) {\n");
    pw.print("    Class modelClass = typeDef.getValueClass();\n");
    for (int typeNum = 0; typeNum < this.complexTypes.size(); typeNum++) {
      ComplexType complexType = complexTypes.get( typeNum );
      if (!complexType.isAbstract()) {
        String classname = getClassForType( complexType, false );
        pw.print( "    if (modelClass == " + classname + ".class) {\n" );
        pw.print( "      return new " + classname + "(key, attributes, anyAttribs, children);\n    }\n" );
      }
    }
    pw.print("    return super.createInstance(key, typeDef, attributes, anyAttribs, children);\n");
    pw.print("  }\n\n");
  }

  protected void writeMethods( PrintWriter headerPW, PrintWriter classPW ) {
    writeCreateInstance(headerPW, classPW );
    super.writeMethods( headerPW, classPW );
  }

  public PrintWriter getConstantsPW() {
    if (contantsCW == null) {
      contantsCW = new CharArrayWriter();
    }
    return new PrintWriter( contantsCW );
  }

  public PrintWriter getStaticPW() {
    if (staticCW == null) {
      staticCW = new CharArrayWriter();
    }
    return new PrintWriter( staticCW );
  }

  /**
   * Perform class file generation. Files will be generated in a directory
   * according to the package name.
   */
  public void generate() {
    PrintWriter headerPW = null;
    PrintWriter classPW = null;
    try {
      headerPW = createHeaderPrintWriter();
      classPW = createClassPrintWriter();

      CharArrayWriter cw = new CharArrayWriter();
      PrintWriter mem = new PrintWriter( cw );
      //writeClassBody( headerPW, mem );
      mem.flush();
      mem.close();

      addImport( CPP_CLASS_NAMESPACE, true );

      headerPW.println( getHeader() );
      headerPW.println();
      String headerDefName = getName().toUpperCase()+"_H";
      headerPW.println( "#ifndef "+headerDefName );
      headerPW.println( "#define "+headerDefName );
      headerPW.println();
      writeImports( headerPW, true );

      StringBuilder enumStrBuilder = new StringBuilder();
      writeTypeDefs( headerPW, enumStrBuilder );

      Collections.sort( complexTypes, new Comparator<ComplexType>() {
        @Override
        public int compare( ComplexType o1, ComplexType o2 ) {
          return o1.name().getName().compareTo( o2.name().getName() );
        }
      } );
      for (ComplexType type : complexTypes) {
        String childTypeClassName = getClassForType( type, false );
        headerPW.println( "class "+childTypeClassName+";" );
      }
      headerPW.println();

      writeClassDeclaration(headerPW);
      writeConstants( headerPW );
      headerPW.println();
      headerPW.println( enumStrBuilder.toString() );
      headerPW.println( "  static "+getName()+"* getInstance();" );
      headerPW.println( "  static int getVersion();" );
      headerPW.println();

      for (ComplexType type : complexTypes) {
        if (!type.isAbstract()) {
          String childTypeClassName = getClassForType( type, false );
          headerPW.println( "  " + childTypeClassName + "* getNew" + childTypeClassName + "( Model* forParent, const QName* forRelation );" );
        }
      }

      classPW.println( "#include \""+getName()+".h\"" );
      writeImports( classPW, false );
      classPW.print( contantsCW.toCharArray() );

      classPW.print( cw.toCharArray() );
      classPW.println( getName() + " instance" + getName() + ";" );
      classPW.println();
      classPW.println( getName()+"* "+getName()+"::getInstance() {" );
      classPW.println( " return &(instance"+getName()+");" );
      classPW.println( "}" );
      classPW.println();
      classPW.println( "int "+getName()+"::getVersion() {" );
      if (schemaVersion == null) {
        classPW.println( " return 0;" );
      }
      else {
        classPW.println( " return " + schemaVersion + ";" );
      }
      classPW.println( "}" );

      if (!writeExtensionLines( headerPW, extensionLinesHeader )) {
        headerPW.println( "};" );
        headerPW.println();
        headerPW.println( "static "+getName()+"* "+lowerCaseFirst( getName() )+" = "+getName()+"::getInstance();\n" );
        headerPW.println();
        headerPW.println( "#endif //"+headerDefName );
      }
      writeExtensionLines( classPW, extensionLinesClass );

      System.out.println("Generated class: " + getName());
    }
    catch (Exception ex) {
      String msg = "Error generation class: " + getName();
      Logger.error(msg, ex);
      throw new IllegalArgumentException(msg);
    }
    finally {
      if (headerPW != null) {
        headerPW.flush();
        headerPW.close();
      }
      if (classPW != null) {
        classPW.flush();
        classPW.close();
      }
    }
  }

}
