/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.cppgenerator;

import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.qnames.QName;

import java.io.PrintWriter;

public class CppGetChildMethod extends CppMethodGenerator {

  private QName memberName;
  private ComplexType type;

  public CppGetChildMethod( CppClassGenerator parentClass, QName memberName, ComplexType type ) {
    super( parentClass, "get" + upperCaseFirst( memberName.getName() ) );
    this.memberName = memberName;
    this.type = type;
  }

  protected void writeMethod( PrintWriter pw ) {
    String classForType;
    classForType = getClassForType( type, true );
    String variable = lowerCaseFirst( convertToValidlName( memberName.getName() ) );
    writeMethodDeclaration( parentClass.getName() + "::", pw, classForType );
    pw.println( " {" );
    pw.println( "  for (int i = 0; i < this->"+variable+"_count; i++) {" );
    pw.println( "    "+classForType+" "+variable+" = &(this->"+variable+"[i]);" );
    pw.print( "    if ");
    if (type.keyAttributeCount() > 1) pw.print( "(" );
    for (int i = 0; i < type.keyAttributeCount(); i++) {
      String keyVariable = lowerCaseFirst( convertToValidlName( type.getKeyAttribute( i ).getName() ) );
      SimpleType simpleType = type.getKeyAttributeType( i );
      String typeClassName = getClassForType( simpleType, true );
      if (i > 0) {
        pw.print( " && " );
      }
      if ("char*".equals( typeClassName )) {
        pw.print( "(strcmp("+keyVariable+", "+variable+"->get"+upperCaseFirst( keyVariable )+"()) == 0)" );
      }
      else {
        pw.print( "("+keyVariable+" == "+variable+"->get"+upperCaseFirst( keyVariable )+"())" );
      }
    }
    if (type.keyAttributeCount() > 1) pw.print( ")" );
    pw.println( " {" );
    pw.println( "      return "+variable+";" );
    pw.println( "    }" );
    pw.println( "  }" );
    pw.println( "  return NULL;" );
    pw.println( "}" );
  }

  private void writeMethodDeclaration( String prefix, PrintWriter pw, String classForType ) {
    parentClass.addImport( CPP_CLASS_QNAME, true );
    pw.print( classForType + " " + prefix + getName() + "( " );
    for (int i = 0; i < type.keyAttributeCount(); i++) {
      String keyVariable = lowerCaseFirst( convertToValidlName( type.getKeyAttribute( i ).getName() ) );
      SimpleType keyAttributeType = type.getKeyAttributeType( i );
      String keyAttType = getClassForType( keyAttributeType, true );
      String typeClassName = getClassForType( keyAttributeType, true );
      if (i > 0) {
        pw.print( ", " );
      }
      if ("char*".equals( typeClassName ) || "QName*".equals( typeClassName )) {
        pw.print( "const " );
      }
      pw.print( keyAttType + " " + keyVariable );
    }
    pw.print( " )" );
  }

  protected void writeMethodInterface( PrintWriter pw ) {
    String classForType;
    classForType = getClassForType( type, true );
    pw.print( "  " );
    writeMethodDeclaration( "", pw, classForType );
    pw.println( ";" );
  }
}
