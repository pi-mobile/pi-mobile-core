/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.cppgenerator;

import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.simple.IntegerType;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.Restriction;
import de.pidata.models.xml.schema.SimpleTypeDef;
import de.pidata.qnames.QName;

import java.io.PrintWriter;
import java.util.List;

public class CppCreateChildMethod extends CppMethodGenerator {
  private ComplexType type;

  public CppCreateChildMethod( CppClassGenerator parentClass, ComplexType type ) {
    super( parentClass, "createChild" );
    this.type = type;
  }

  protected void writeMethod( PrintWriter pw ) {
    writeMethodDeclaration( parentClass.getName() + "::", pw );
    pw.println( " {" );
    if (type.relationCount() > 0) {
      boolean first = true;
      for (QName relName : type.relationNames()) {
        Relation childRel = type.getRelation( relName );
        if (first) {
          pw.print( "  if " );
          first = false;
        }
        else {
          pw.print( "  else if " );
        }
        pw.println( "(tag == " + createIDConstant( relName ) + ") {" );
        String childTypeClassName = getClassForType( childRel.getChildType(), false );
        pw.println( "    return "+lowerCaseFirst( parentClass.getFactoryClassName() )+"->getNew" + childTypeClassName + "( this, tag );" );
        pw.println( "  }" );
      }
      ComplexType parentType = (ComplexType) type.getBaseType();
      if (parentType == null) {
        pw.println( "  else {" );
        pw.println( "    return Model::createChild( tag );" );
        pw.println( "  }" );
      }
      else {
        pw.println( "  else {" );
        String parentClassName = getClassForType( parentType, false );
        pw.println( "    return " + parentClassName + "::createChild( tag );" );
        pw.println( "  }" );
      }
    }
    pw.println( "}" );
  }

  protected void writeMethodInterface( PrintWriter pw ) {
    pw.print( "  virtual " );
    writeMethodDeclaration( "", pw );
    pw.println( ";" );
  }

  private void writeMethodDeclaration( String prefix, PrintWriter pw ) {
    pw.print( "Model* " + prefix + "createChild( const QName* tag )" );
  }

}
