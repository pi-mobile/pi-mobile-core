/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.cppgenerator;

import de.pidata.log.Logger;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.simple.*;
import de.pidata.models.xml.schema.SimpleTypeDef;
import de.pidata.models.xml.schema.TopLevelSimpleType;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.io.PrintWriter;
import java.util.List;

public class CppCodeGenerator {
  private static final Namespace NS_SCHEMA = Namespace.getInstance( "http://www.w3.org/2001/XMLSchema" );

  public static final String CPP_CLASS_QNAME = "QName";
  public static final String CPP_CLASS_NAMESPACE = "Namespace";
  public static final String CPP_CLASS_MODEL = "Model";

  public static final int PUBLIC = 0;
  public static final int NONE = 1;
  public static final int PROTECTED = 2;

  protected CppClassGenerator parentClass;
  private String name;
  private String header;

  private boolean isAbstract;
  private boolean isInterface;
  private int visibility;

  /**
   * Create generator.
   *
   * @param name name of code unit
   */
  public CppCodeGenerator( CppClassGenerator parentClass, String name ) {
    this.parentClass = parentClass;
    this.name = ensureNonColidingMemberName( convertToValidlName( name ) );
    header = "";
    isAbstract = false;
    isInterface = false;
    visibility = PUBLIC;
  }

  /**
   * Set visibility of class. Must be one of <code>PUBLIC</code>, <code>PROTECTED</code> or <code>NONE</code>.
   *
   * @param visibility
   */
  public void setVisibility( int visibility ) {
    this.visibility = visibility;
  }

  /**
   * Access class visibility.
   *
   * @return
   */
  public int getVisibility() {
    return this.visibility;
  }

  /**
   * Declare class to be abstract.
   *
   * @param isAbstract
   */
  public void setAbstract( boolean isAbstract ) {
    this.isAbstract = isAbstract;
  }

  /**
   * Check if class has been declared abstract.
   *
   * @return
   */
  public boolean isAbstract() {
    return isAbstract;
  }

  public void setInterface( boolean isInterface ) {
    this.isInterface = isInterface;
  }

  public boolean isInterface() {
    return isInterface;
  }

  /**
   * Access File header.
   *
   * @return
   */
  public String getHeader() {
    return header;
  }

  /**
   * Set file header.
   *
   * @param header
   */
  public void setHeader( String header ) {
    this.header = header;
  }

  /**
   * Access class name (without package).
   *
   * @return
   */
  public String getName() {
    return this.name;
  }

  /**
   * Add an import declaration. Duplicates are ignored.
   *
   * @param name Name of import declaration
   */
  public void addImport( String name, boolean headerFile ) {
    this.parentClass.addImport( name, headerFile );
  }

  public static String convertToValidlName( String name ) {
    StringBuilder result = new StringBuilder();
    for (int i = 0; i < name.length(); i++) {
      char ch = name.charAt( i );
      if ((ch >= '0') && (ch <= '9')) {
        result.append( ch );
      }
      else if ((ch >= 'A') && (ch <= 'Z')) {
        result.append( ch );
      }
      else if ((ch >= 'a') && (ch <= 'z')) {
        result.append( ch );
      }
      else {
        result.append( '_' );
      }
    }
    return result.toString();
  }

  public static String ensureNonColidingMemberName( String memberName ) {
    // the following memberNames will cause invalid getter methods
    // coliding with parent classes' getter methods
    if (memberName.equalsIgnoreCase( "Class" )
        || memberName.equals( "Factory" )
        || memberName.equals( "Parent" )
        || memberName.equals( "import" )
        || memberName.equals( "package" )
        || memberName.equals( "default" )
        || memberName.equals( "protected" )
        || memberName.equals( "public" )
        || memberName.equals( "private" )
        || memberName.equals( "Key" )
        || memberName.equals( "abstract" )
        || memberName.equals( "if" )
        || memberName.equals( "then" )
        || memberName.equals( "else" )
        || memberName.equals( "do" )
        || memberName.equals( "signed" )) {
      memberName = "_" + memberName;
    }
    return memberName;
  }

  public static String toConstantName( String name ) {
    String constName = convertToValidlName( name );
    return constName.toUpperCase();
  }

  public static String lowerCaseFirst( String value ) {
    return value.substring( 0, 1 ).toLowerCase() + value.substring( 1 );
  }

  public static String upperCaseFirst( String value ) {
    return value.substring( 0, 1 ).toUpperCase() + value.substring( 1 );
  }

  /**
   * Get an identifier to be used for that Namespace during code generation. For each namespace
   * a public static field will be created with this identifier.
   *
   * @param namespace
   * @return
   */
  protected String getNamespaceConstant( Namespace namespace ) {
    return parentClass.getNamespaceConstant( namespace );
  }

  /**
   * Generate code apropriate for representing this QName at runtime.
   *
   * @param id
   * @param pw
   */
  protected void writeQName( QName id, PrintWriter pw ) {
    pw.print( getNamespaceConstant( id.getNamespace() ) + ".getQName( \"" + id.getName() + "\" )" );
  }

  /**
   * Extract the class name out of a fully qualified classname. I.e "java.lang.String" --> "String"
   *
   * @param fqcn
   * @return
   */
  protected String getClassPart( String fqcn ) {
    int idx = fqcn.lastIndexOf( '.' );
    if (idx > 0) {
      return fqcn.substring( idx + 1 );
    }

    return fqcn;
  }

  /**
   * Extract package name out of a fully qualified classname. I.e. "java.lang.String" --> "java.lang"
   *
   * @param fqcn
   * @return
   */
  protected String getPackagePart( String fqcn ) {
    int idx = fqcn.lastIndexOf( '.' );
    if (idx > 0) {
      return fqcn.substring( 0, idx );
    }

    return "";
  }

  public String getClassForType( Type type, boolean usePointerForClass ) {
    if (type == null) {
      throw new IllegalArgumentException( "Type must not be null" );
    }
    try {
      if (type instanceof SimpleType) {
        return handleSimpleType( (SimpleType) type, usePointerForClass );
      }
      else {
        Class typeClass = type.getValueClass();
        QName typeName = type.name();
        if (typeName == null) {
          return null;
        }
        if ((typeClass == null) || (typeName.getNamespace() == parentClass.getTargetNamespace())) {
          String className = convertToValidlName( typeName.getName() );
          className = CppClassGenerator.upperCaseFirst( className );
          if (usePointerForClass) {
            className += "*";
          }
          return className;
        }
        else {
          parentClass.addImport( typeClass.getName(), true );
          String className = getClassPart( typeClass.getName() );
          if (usePointerForClass) {
            className += "*";
          }
          return className;
        }
      }
    }
    catch (Exception ex) {
      String msg = "Could not get class name for type=" + type.name();
      Logger.error( msg, ex );
      throw new IllegalArgumentException( msg );
    }
  }

  private String handleSimpleType( SimpleType type, boolean usePointerForClass ) {
    SimpleType simpleType = type;
    String result = getSimpleTypeDefinition( simpleType );
    if (result.equals( "char" )) {
      if (type instanceof SimpleTypeDef) {
        Restriction restriction = ((SimpleTypeDef) simpleType).getRestriction();
        if (restriction != null) {
          List<String> enumeration = restriction.enumeration();
          if ((enumeration != null) && (enumeration.size() > 0)) {
            return type.name().getName() + "_t";
          }
          else if (restriction.maxLength() == 1) {
            return "char";
          }
        }
      }
      if (usePointerForClass) {
        return "char*";
      }
    }
    return result;
  }

  /**
   * Create an identifier name for this classname. Simply turns the first letter into lower case.
   *
   * @param className
   * @return
   */
  protected String getIdentifierName( String className ) {
    StringBuffer buf = new StringBuffer();

    if (className.length() > 0) {
      buf.append( Character.toLowerCase( className.charAt( 0 ) ) );
      buf.append( className.substring( 1 ) );
    }

    return buf.toString();
  }

  public String createIDConstant( QName valueName ) {
    return "ID_" + CppClassGenerator.toConstantName( valueName.getName() );
  }

  public String getSimpleTypeDefinition( SimpleType attributeType ) {
    if (attributeType instanceof BooleanType) {
      return "bool";
    }
    else if (attributeType instanceof IntegerType) {
      QName typeID = attributeType.name();
      if (typeID == IntegerType.TYPE_INT) {
        return "int";
      }
      else if (typeID == IntegerType.TYPE_LONG) {
        return "long";
      }
      else if (typeID == IntegerType.TYPE_SHORT) {
        return "short";
      }
      else if (typeID == IntegerType.TYPE_BYTE) {
        return "signed byte";
      }
      else if (typeID == IntegerType.TYPE_UNSIGNED_LONG) {
        return "unsigned long";
      }
      else if (typeID == IntegerType.TYPE_UNSIGNED_INT) {
        return "unsigned int";
      }
      else if (typeID == IntegerType.TYPE_UNSIGNED_SHORT) {
        return "unsigned short";
      }
      else if (typeID == IntegerType.TYPE_UNSIGNED_BYTE) {
        return "byte"; // maps to unsigned char
      }
      else {
        return "int";
      }
    }
    else if (attributeType instanceof DateTimeType) {
      return "char";
      /* TODO QName type = ((DateTimeType) attributeType).getType();
      if (type == DateTimeType.TYPE_DATE) {
        return "time_t";
      }
      else if (type == DateTimeType.TYPE_DATETIME) {
        return "time_t";
      }
      else if (type == DateTimeType.TYPE_TIME) {
        return "time_t";
      } */
    }
    else if (attributeType instanceof QNameType) {
      addImport( CPP_CLASS_QNAME, true );
      return CPP_CLASS_QNAME + "*";
    }
    else if (attributeType instanceof StringType) {
      return "char";
    }
    else if (attributeType instanceof DecimalType) {
      //TODO return "double";
      return "char";
    }
    else if (attributeType instanceof BinaryType) {
      //TODO return "uint8_t";
      return "char";
    }
    else if (attributeType instanceof SimpleTypeDef) {
      return getSimpleTypeDefinition( attributeType.getRootType() );
    }

    if ((attributeType == null) || (attributeType.getClass() == null)) {
      throw new IllegalArgumentException( "Invalid type for attribute type=" + attributeType );
    }
    else {
      Logger.warn( "Unsupported SimpleType: " + attributeType.getClass().getName() );
      return attributeType.getClass().getName() + ".getInstance()";
    }
  }


  public String getTypeConstant( Type type ) {
    QName typeName = type.name();
    if (type instanceof ComplexType) {
      return getClassForType( type, false ).toUpperCase() + "_TYPE";
    }
    else {
      if (typeName.getNamespace() == NS_SCHEMA) {
        return getSimpleTypeDefinition( (SimpleType) type );
      }
      else {
        return createIDConstant( type.name() );
      }
    }
  }

  public String getClassName( ComplexType type, String packageName ) {
    return getName();
  }
}
