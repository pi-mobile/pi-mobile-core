package de.pidata.models.cppgenerator;

import de.pidata.models.types.ComplexType;

import java.io.PrintWriter;

public class CppTypeIDMethod extends CppMethodGenerator {

  private ComplexType type;

  public CppTypeIDMethod( CppClassGenerator parentClass, ComplexType type ) {
    super( parentClass, "typeID" );
    this.type = type;
  }

  private void writeMethodDeclaration( String prefix, PrintWriter pw ) {
    pw.print( "const QName* " + prefix + "typeID()" );
  }

  protected void writeMethod( PrintWriter pw ) {
    writeMethodDeclaration( parentClass.getName() + "::", pw );
    pw.println( " {" );
    String myTagContant = createIDConstant( type.name() );
    pw.println( "  return "+myTagContant+";" );
    pw.println( "}" );
  }

  protected void writeMethodInterface( PrintWriter pw ) {
    pw.print( "  virtual " );
    writeMethodDeclaration( "", pw );
    pw.println( ";" );
  }
}
