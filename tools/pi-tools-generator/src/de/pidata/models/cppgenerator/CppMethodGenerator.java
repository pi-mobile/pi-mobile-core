/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.cppgenerator;

import de.pidata.models.types.SimpleType;
import de.pidata.qnames.QName;

import java.io.PrintWriter;

public abstract class CppMethodGenerator extends CppCodeGenerator {

  public CppMethodGenerator(CppClassGenerator parentClass, String name) {
    super(parentClass, name);
    parentClass.addMethod(this );
  }

  @Override
  public String createIDConstant( QName valueName ) {
    return parentClass.createIDConstant( valueName );
  }

  public String getEnumStrArrayName( SimpleType attrType ) {
    return lowerCaseFirst( parentClass.getFactoryClassName() ) + "->" + attrType.name().getName() + "_names";
  }

  protected abstract void writeMethod( PrintWriter pw);

  protected abstract void writeMethodInterface(PrintWriter pw);
}
