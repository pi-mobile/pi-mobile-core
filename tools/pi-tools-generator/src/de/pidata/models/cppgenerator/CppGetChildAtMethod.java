/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.cppgenerator;

import de.pidata.models.tree.Model;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.Key;
import de.pidata.qnames.QName;

import java.io.PrintWriter;

public class CppGetChildAtMethod extends CppMethodGenerator {

  private QName memberName;
  private Type type;

  public CppGetChildAtMethod( CppClassGenerator parentClass, QName memberName, Type type ) {
    super( parentClass, "get" + upperCaseFirst( memberName.getName() ) + "At" );
    this.memberName = memberName;
    this.type = type;
  }

  protected void writeMethod( PrintWriter pw ) {
    String classForType;
    classForType = getClassForType( type, true );
    String variable = lowerCaseFirst( convertToValidlName( memberName.getName() ));
    writeMethodDeclaration( parentClass.getName() + "::", pw, classForType );
    pw.println( " {" );
    pw.println( "  if ((index < 0) || (index >= this->"+variable+"_count)) { ");
    pw.println( "    fatalError( \""+parentClass.getName()+"::"+getName()+" - Index out of range\" );" );
    pw.println( "  }");
    pw.println( "  return &(this->" + ensureNonColidingMemberName( variable ) + "[index]);" );
    pw.println( "}" );
  }

  private void writeMethodDeclaration( String prefix, PrintWriter pw, String classForType ) {
    parentClass.addImport( CPP_CLASS_QNAME, true );
    pw.print( classForType + " " + prefix + getName() + "( int index )" );
  }

  protected void writeMethodInterface( PrintWriter pw ) {
    String classForType = getClassForType( type, true );
    pw.print( "  " );
    writeMethodDeclaration( "", pw, classForType );
    pw.println( ";" );
  }
}
