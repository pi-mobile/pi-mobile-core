/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.cppgenerator;

import de.pidata.models.tree.SimpleModel;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

import java.io.PrintWriter;

public class CppSetterMethod extends CppMethodGenerator {

  private QName memberName;
  private Type type;
  private boolean attribute;

  public CppSetterMethod( CppClassGenerator parentClass, QName memberName, Type type, boolean attribute ) {
    super( parentClass, "set" + upperCaseFirst( memberName.getName() ) );
    this.memberName = memberName;
    this.type = type;
    this.attribute = attribute;
  }

  protected void writeMethod( PrintWriter pw ) {
    String typeClassName = getClassForType( type, true );
    String variable = lowerCaseFirst( convertToValidlName( memberName.getName() ) );
    String variableName = ensureNonColidingMemberName( variable );
    writeMethodDeclaration( parentClass.getName() + "::", pw, variableName, typeClassName );
    pw.println( " {" );
    if (attribute) {
      if ("char*".equals( typeClassName )) {
        pw.println( "  if ("+variableName+" == NULL) {" );
        pw.println( "    this->"+variableName+"[0] = 0;" );
        pw.println( "  }" );
        pw.println( "  else {" );
        pw.println( "    int count = strlen( "+variableName+" );" );
        pw.println( "    if (count >= "+variable.toUpperCase()+"_MAX_LEN) {" );
        pw.println( "      count = "+variable.toUpperCase()+"_MAX_LEN;" );
        pw.println( "    }" );
        pw.println( "    strncpy( this->"+variableName+", "+variableName+", count );" );
        pw.println( "    this->"+variableName+"[count] = 0;" );
        pw.println( "  }" );
      }
      else {
        pw.println( "  this->" + variableName + " = "+variableName+";" );
      }
    }
    else {
      // do nothing
    }
    pw.println( "}" );
  }

  protected void writeMethodInterface( PrintWriter pw ) {
    String typeClassName = getClassForType( type, true );
    String variable = ensureNonColidingMemberName( lowerCaseFirst( convertToValidlName( memberName.getName() ) ) );
    pw.print( "  " );
    writeMethodDeclaration( "", pw, variable, typeClassName );
    pw.println( ";" );
  }

  private void writeMethodDeclaration( String prefix, PrintWriter pw, String variable, String typeClassName ) {
    if ("QName*".equals( typeClassName )) {
      typeClassName = "const QName*";
    }
    pw.print( "void " + prefix + getName() + "( " + typeClassName + " " + variable + " )" );
  }
}
