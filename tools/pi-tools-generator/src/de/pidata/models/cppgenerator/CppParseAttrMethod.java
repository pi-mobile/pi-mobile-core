/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.cppgenerator;

import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DecimalType;
import de.pidata.models.types.simple.IntegerType;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.Restriction;
import de.pidata.models.xml.schema.SimpleTypeDef;
import de.pidata.qnames.QName;

import java.io.PrintWriter;
import java.util.List;

public class CppParseAttrMethod extends CppMethodGenerator {
  private ComplexType type;

  public CppParseAttrMethod( CppClassGenerator parentClass, ComplexType type ) {
    super( parentClass, "parseAttr" );
    this.type = type;
  }

  protected void writeMethod( PrintWriter pw ) {
    addImport( "XmlReader", true );
    writeMethodDeclaration( parentClass.getName() + "::", pw );
    pw.println( " {" );
    ComplexType parentType = (ComplexType) type.getBaseType();
    if (parentType != null) {
      String parentClassName = getClassForType( parentType, false );
      pw.println( "  " + parentClassName + "::parseAttr( xmlReader );" );
    }
    String myTagContant = createIDConstant( type.name() );
    for (int i = ((CppTypeClassGenerator) parentClass).firstAttrIndex(); i < type.attributeCount(); i++) {
      QName attrName = type.getAttributeName( i );
      SimpleType attrType = type.getAttributeType( i );
      QName attrTypeID = attrType.name();
      Object attrDefault = type.getAttributeDefault( i );
      String attrTypeName = getSimpleTypeDefinition( attrType );
      String memberName = ensureNonColidingMemberName( attrName.getName() );
      if (attrType.getRootType().getValueClass() == String.class) {
        boolean done = false;
        if (attrType instanceof SimpleTypeDef) {
          Restriction restriction = ((SimpleTypeDef) attrType).getRestriction();
          if (restriction != null) {
            List<String> enumeration = restriction.enumeration();
            if ((enumeration != null) && (enumeration.size() > 0)) {
              String enumStrArrayName = getEnumStrArrayName( attrType );
              pw.println( "  xmlReader->getEnumAttr( "+ createIDConstant( attrName ) + ", (char **) " + enumStrArrayName +", "+ enumeration.size()+ ", (int *) &(this->"+memberName + ") );");
              done = true;
            }
            else if (restriction.maxLength() == 1) {
              pw.println( "  xmlReader->getStringAttr( " + createIDConstant( attrName ) + ", (" + attrTypeName + " *) &(this->" + memberName + "), 1 );" );
              done = true;
            }
          }
        }
        else if (attrType instanceof QNameType) {
          pw.println( "  xmlReader->getQNameAttr( " + createIDConstant( attrName ) + ", (const QName**) &(this->" + memberName + ") );" );
          done = true;
        }
        if (!done) {
          pw.println( "  xmlReader->getStringAttr( " + createIDConstant( attrName ) + ", (" + attrTypeName + " *) &(this->" + memberName + "), " + attrName.getName().toUpperCase() + "_MAX_LEN );" );
        }
      }
      else if ((attrTypeID == IntegerType.TYPE_INTEGER) || (attrTypeID == IntegerType.TYPE_INT) || (attrTypeID == IntegerType.TYPE_UNSIGNED_INT)) {
        String defValue;
        if (attrDefault == null) {
          defValue = "0";
        }
        else {
          defValue = attrDefault.toString();
        }
        pw.println( "  xmlReader->getIntAttr( " + createIDConstant( attrName ) + ", (" + attrTypeName + " *) &(this->" + memberName + "), "+defValue+" );" );
      }
      else if ((attrTypeID == IntegerType.TYPE_LONG) || (attrTypeID == IntegerType.TYPE_UNSIGNED_LONG)
               || (attrTypeID == IntegerType.TYPE_NEGATIVEINTEGER) || (attrTypeID == IntegerType.TYPE_POSITIVEINTEGER)
               || (attrTypeID == IntegerType.TYPE_NONNEGATIVEINTEGER) || (attrTypeID == IntegerType.TYPE_NONPOSITIVEINTEGER)) {
        String defValue;
        if (attrDefault == null) {
          defValue = "0";
        }
        else {
          defValue = attrDefault.toString();
        }
        pw.println( "  xmlReader->getLongAttr( " + createIDConstant( attrName ) + ", (" + attrTypeName + " *) &(this->" + memberName + "), "+defValue+" );" );
      }
      else if ((attrTypeID == IntegerType.TYPE_SHORT) || (attrTypeID == IntegerType.TYPE_UNSIGNED_SHORT)) {
        String defValue;
        if (attrDefault == null) {
          defValue = "0";
        }
        else {
          defValue = attrDefault.toString();
        }
        pw.println( "  xmlReader->getShortAttr( " + createIDConstant( attrName ) + ", (" + attrTypeName + " *) &(this->" + memberName + "), "+defValue+" );" );
      }
      else if ((attrTypeID == IntegerType.TYPE_BYTE) || (attrTypeID == IntegerType.TYPE_UNSIGNED_BYTE)) {
        String defValue;
        if (attrDefault == null) {
          defValue = "0";
        }
        else {
          defValue = attrDefault.toString();
        }

        pw.println( "  xmlReader->getByteAttr( " + createIDConstant( attrName ) + ", (" + attrTypeName + " *) &(this->" + memberName + "), "+defValue+" );" );
      }
      else if (attrType.getValueClass() == Boolean.class) {
        pw.println( "  xmlReader->getBoolAttr( " + createIDConstant( attrName ) + ", (" + attrTypeName + " *) &(this->" + memberName + ") );" );
      }
      else if ((attrTypeID == DecimalType.TYPE_DECIMAL) || (attrTypeID == DecimalType.TYPE_DOUBLE)) {
        pw.println( "  xmlReader->getStringAttr( " + createIDConstant( attrName ) + ", (" + attrTypeName + " *) &(this->" + memberName + "), " + attrName.getName().toUpperCase() + "_MAX_LEN );" );
        //TODO pw.println( "  xmlReader->getDoubleAttr( " + createIDConstant( attrName ) + ", (" + attrTypeName + " *) &(this->" + memberName + ") );" );
      }
      else if (attrType.getValueClass() == DateObject.class) {
        pw.println( "  xmlReader->getStringAttr( " + createIDConstant( attrName ) + ", (" + attrTypeName + " *) &(this->" + memberName + "), " + attrName.getName().toUpperCase() + "_MAX_LEN );" );
        //TODO pw.println( "  xmlReader->getDateAttr( " + createIDConstant( attrName ) + ", (" + attrTypeName + " *) &(this->" + memberName + ") );" );
      }
      else {
        throw new IllegalArgumentException( "Unsupportet attribute type: "+attrTypeName );
      }
    }
    pw.println( "}" );
  }

  protected void writeMethodInterface( PrintWriter pw ) {
    pw.print( "  virtual " );
    writeMethodDeclaration( "", pw );
    pw.println( ";" );
  }

  private void writeMethodDeclaration( String prefix, PrintWriter pw ) {
    pw.print( "void " + prefix + "parseAttr( XmlReader* xmlReader )" );
  }

}
