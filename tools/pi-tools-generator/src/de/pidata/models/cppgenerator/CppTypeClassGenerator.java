/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.cppgenerator;

import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.AnyElement;
import de.pidata.qnames.QName;

import java.io.CharArrayWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class CppTypeClassGenerator extends CppClassGenerator {

  private List<QName> attributeNames = new ArrayList<QName>();
  private List<QName> childNames = new ArrayList<QName>();
  private ComplexType type;

  public CppTypeClassGenerator( QName typeName, String outputDirName, CppFactoryClassGenerator factoryClassGenerator, ComplexType type ) throws ClassNotFoundException {
    super( typeName.getName(), typeName.getNamespace(), factoryClassGenerator, outputDirName );
    this.type = type;

    ComplexType baseType = (ComplexType) type.getBaseType();
    if (baseType == null) {
      setExtension( CPP_CLASS_MODEL );
    }
    else if (baseType.name().getNamespace() == targetNamespace) {
      setExtension( upperCaseFirst( ensureNonColidingMemberName( convertToValidlName( baseType.name().getName() ) ) ) );
    }
    else {
      String ext = baseType.getValueClass().getName();
      addImport( ext, true );
      setExtension( ext );
    }
    addImport( factoryClassGenerator.getName(), true );

    //----- xml methods
    new CppTypeIDMethod( this, type );
    new CppParseAttrMethod( this, type );
    new CppWriteAttrMethod( this, type );
    if (type.relationCount() > 0) {
      new CppCreateChildMethod( this, type );
    }

    //----- attributes
    for (int i = firstAttrIndex(); i < type.attributeCount(); i++) {
      QName attributeName = type.getAttributeName( i );
      addAttribute( attributeName, type.getAttributeType( i ), false );
    }

    //----- children
    int i;
    if (baseType == null) i = 0;
    else i = baseType.relationCount();
    for (QNameIterator relIter = type.relationNames(); relIter.hasNext(); ) {
      QName relName = relIter.next();
      if (i <= 0) {
        Relation childRel = type.getRelation( relName );
        addChild( relName, childRel.getChildType(), false, childRel.getMinOccurs(), childRel.getMaxOccurs(), childRel.getCollection() );
      }
      else {
        // parent relations überspringen
        i--;
      }
    }

    //----- key references
    for (QNameIterator keyRefNames = type.keyRefNames(); keyRefNames.hasNext(); ) {
      QName refName = keyRefNames.next();
      createIDConstant( refName );
    }
  }

  protected int firstAttrIndex() {
    ComplexType baseType = (ComplexType) type.getBaseType();
    if (baseType != null) {
      return baseType.attributeCount();
    }
    else {
      return 0;
    }
  }

  protected void addAttribute( QName attrName, SimpleType attrType, boolean readOnly ) {
    if (!attributeNames.contains( attrName )) {
      attributeNames.add( attrName );
      CppMethodGenerator method;
      method = new CppGetterMethod( this, attrName, attrType, true );
      if (!readOnly) {
        method = new CppSetterMethod( this, attrName, attrType, true );
      }
    }
  }

  protected void addChild( QName childName, Type childType, boolean readOnly, int minOccurs, int maxOccurs, Class collection ) {
    if (childNames.contains( childName )) {
      return;
    }
    childNames.add( childName );
    if ((childName == AnyElement.ANY_ELEMENT) || (childType instanceof AnyElement)) {
      return;
    }
  }

  public void writeFields( PrintWriter pwHeader ) {
    AnyAttribute attrAny;
    Relation childRel = null;
    QName relName, attrName;
    String typeName = type.name().getName();

    pwHeader.println( "protected:" );
    for (int i = 0; i < attributeNames.size(); i++) {
      attrName = attributeNames.get( i );
      int index = type.indexOfAttribute( attrName );
      SimpleType attrType = type.getAttributeType( index );
      Object attrDefault = type.getAttributeDefault( index );
      int keyIndex = type.getKeyIndex( attrName );
      writeAttributeDef( pwHeader, keyIndex, attrName, attrType, attrDefault, getFactoryClassName() );
    }
    attrAny = type.getAnyAttribute();
    if (attrAny != null) {
      writeAttributeDef( pwHeader, -1, attrAny.name(), attrAny, attrAny.createDefaultValue(), getFactoryClassName() );
    }

    for (QNameIterator keyRefNames = type.keyRefNames(); keyRefNames.hasNext(); ) {
      QName refName = keyRefNames.next();
      ModelReference ref = type.getKeyReference( refName );
      writeReference( pwHeader, ref );
    }

    for (int i = 0; i < childNames.size(); i++) {
      relName = childNames.get( i );
      childRel = type.getRelation( relName );
      writeRelation( pwHeader, childRel, true, getFactoryClassGenerator() );
    }
    pwHeader.println();
  }

  private void writeReference( PrintWriter pw, ModelReference ref ) {
    pw.print( "    type.addKeyReference( " );
    pw.print( createIDConstant( ref.getName() ) + ", " );
    QName refTypeName = ref.getRefTypeName();
    if (refTypeName.getNamespace() == targetNamespace) {
      pw.print( "NAMESPACE.getQName( \"" + refTypeName.getName() + "\" ), " );
    }
    else {
      pw.print( "QName.getInstance( \"" + refTypeName.toString() + "\" ), " );
    }
    pw.println( getName() + "." + createIDConstant( ref.getRefAttribute( 0 ) ) + " );" );
  }

  protected void writeConstructor( PrintWriter headerPW, PrintWriter classPW ) {
    headerPW.println( "  " + getName()+"();" );
    classPW.println( getName() +"::"+getName()+"() {" );
    for (int i = 0; i < type.attributeCount(); i++) {
      String typeStr = getClassForType( type.getAttributeType( i ), true );
      if ("char*".equals( typeStr )) {
        String name = type.getAttributeName( i ).getName();
        classPW.println( "  this->" + ensureNonColidingMemberName( name ) + "[0] = 0;" );
      }
    }
    classPW.println( "}" );
    classPW.println();
  }

  /**
   * Write class body. Overwrite this method in subclasses to create your code. This method
   * is called before headers and import declarations are created so you can still add import
   * statements here.
   *
   */
  protected void writeClassBody( PrintWriter headerPW, PrintWriter classPW ) {
    CharArrayWriter headerCW = new CharArrayWriter();
    CharArrayWriter classCW = new CharArrayWriter();
    PrintWriter headerMem = new PrintWriter( headerCW );
    PrintWriter classMem = new PrintWriter( classCW );

    //-- Declare external error handler function
    classPW.println( "extern void fatalError( const __FlashStringHelper* msg );" );

    //-- Constructor
    writeConstructor( headerMem, classMem );

    //-- Method parse()
    headerMem.println();

    writeMethods( headerMem, classMem );

    headerMem.flush();
    headerMem.close();
    classMem.flush();
    classMem.close();

    classPW.println();

    headerPW.print( headerCW.toCharArray() );
    classPW.print( classCW.toCharArray() );
  }
}
