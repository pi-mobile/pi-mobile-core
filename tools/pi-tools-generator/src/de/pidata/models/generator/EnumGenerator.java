/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.generator;

import de.pidata.log.Logger;
import de.pidata.models.tree.SimpleModel;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.simple.IntegerType;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;
import de.pidata.models.xml.schema.Documentation;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;
import de.pidata.string.Helper;

import java.io.CharArrayWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generates the Java enums for the given type. The type definition's restriction base may be any simple type.
 * Each enumeration entry becomes an instance of the enum with a constructor holding the typed value.
 * The generator builds accessors and conversion methods to handle the typed values.
 * @implNote Currently only xsd:string and xsd:Qname are fully supported; prepared to implement other types.
 */
public class EnumGenerator extends ClassGenerator {

  List<String> enumValues = new ArrayList<String>();
  private Map<String, Object> enumDocumentations = new HashMap<String, Object>();
  private Map<String, String> enumDeprecations = new HashMap<String, String>();
  private SimpleType type;
  private SimpleType valueType;
  private String valueClassName;

  private NamespaceTable namespaceTable;
  private Namespace valueNamespace;
  private String valueNamespaceName;

  public EnumGenerator( QName typeName, String packageName, String outputDirName, FactoryClassGenerator factoryClassGenerator, SimpleType type, NamespaceTable namespaceTable ) {
    super( buildValidTypeClassName( typeName ), typeName.getNamespace(), packageName, factoryClassGenerator, outputDirName );
    this.type = type;
    this.namespaceTable = namespaceTable;
    this.valueType = (SimpleType) type.getBaseType();
    this.valueClassName = getClassForType( valueType, getPackageName() );
  }

  protected void addEnumValue( String enumValue, Object deprecatedAnnotation, Object documentation ) {
    if (enumValues.contains( enumValue )) {
      Logger.warn( "duplicate enum value: " + enumValue + "in " + getName() );
      return;
    }
    enumValues.add( enumValue );

    if (deprecatedAnnotation != null && deprecatedAnnotation instanceof String) {
      enumDeprecations.put( enumValue, (String) deprecatedAnnotation );
    }
    if (documentation != null) {
      enumDocumentations.put( enumValue, documentation );
    }
  }

  /**
   * Perform class file generation. Files will be generated in a directory
   * according to the package name.
   */
  @Override
  public void generate() {
    String packageName = getPackageName();
    try {
      PrintWriter pw = createPrintWriter();

      CharArrayWriter cw = new CharArrayWriter();
      PrintWriter mem = new PrintWriter( cw );
      writeClassBody( mem );
      mem.flush();
      mem.close();

      pw.println( getHeader() );
      pw.println();
      if (packageName != null) {
        pw.println( "package " + packageName + ";" );
        pw.println();
      }

      writeImports( pw );

      writeDocumentation( pw );

      writeAnnotations( pw );

      writeEnumDeclaration( pw );

      pw.print( cw.toCharArray() );

      writeExtensionLines( pw );

      pw.flush();
      pw.close();
      Logger.info( "Generated enum: " + packageName + "." + getName() );
    }
    catch (Exception ex) {
      String msg = "Error generation enum: " + packageName + "." + getName();
      Logger.error( msg, ex );
      throw new IllegalArgumentException( msg );
    }
  }

  /**
   * Adds the typed constructor for an .
   *
   * @param pw
   */
  protected void writeEnumDeclaration( PrintWriter pw ) {
    pw.print( "public enum " + getName() + " {" );
    pw.println();
  }

  /**
   * Write class body. Overwrite this method in subclasses to create your code. This method
   * is called before headers and import declarations are created so you can still add import
   * statements here.
   *
   * @param pw
   */
  protected void writeClassBody( PrintWriter pw ) {
    writeEnumValues( pw );

    // do not write static - will not be evaluated in time!
    if (valueNamespace != null) {
      writeValueNamespaceGetter( pw );
    }

    writeConstructor( pw );

    writeValueGetter( pw );

    writeFrom( pw, valueType );

    // always generate fromString / toString
    if (valueType != StringType.getDefString()) {
//      writeFrom( pw, StringType.getDefString() );
    }
    writeToString( pw );
  }

  /**
   * Write namespace getter for QName values instead of a constant (static final) field because the namespace
   * is needed in the constructor of the enum entry.
   *
   * @param pw
   */
  private void writeValueNamespaceGetter( PrintWriter pw ) {
    addImport( Namespace.class.getName() );
    pw.println( "  private static Namespace " + valueNamespaceName + ";" );
    pw.println( "  private static Namespace get" + upperCaseFirst( valueNamespaceName ) + "() {" );
    pw.println( "    if (" + valueNamespaceName + " == null) {" );
    pw.println( "      " + valueNamespaceName + " = Namespace.getInstance( \"" + valueNamespace.getUri() + "\" );" );
    pw.println( "    }" );
    pw.println( "    return " + valueNamespaceName + ";" );
    pw.println( "  }" );
    pw.println();
  }

  /**
   * Writes the enum entry's declarations with the typed value in the constructor.
   *
   * @param pw
   */
  private void writeEnumValues( PrintWriter pw ) {
    boolean isFirst = true;
    pw.println();
    for (String value : enumValues) {
      if (!isFirst) {
        pw.println( "," );
      }
      String enumName = convertToValidName( value );
      if (collidesWithReserved( enumName )) {
        enumName = "_" + enumName;
      }

      String enumDeprecated = enumDeprecations.get( value );
      Object enumDocumentation = enumDocumentations.get( value );
      if (enumDocumentation != null) {
        if (enumDocumentation instanceof Documentation) {
          pw.println( "  /** " );
          boolean isFirstDoc = true;
          for (SimpleModel paragraph : ((Documentation) enumDocumentation).paragraphIter()) {
            if (!isFirstDoc) {
              pw.println( "   * <p/>" );
            }
            isFirstDoc = false;
            pw.print( "   * " );
            pw.println( paragraph.getContent() );
          }
          if (!Helper.isNullOrEmpty( enumDeprecated )) {
            pw.println( "   * @deprecated " + enumDeprecated );
          }
          pw.println( "   */" );
        }
        else {
          boolean isDeprecated = !Helper.isNullOrEmpty( enumDeprecated );
          pw.print( "  /** " );
          if (isDeprecated) {
            pw.println();
            pw.print( "   * " );
          }
          pw.print( enumDocumentation.toString() );
          if (isDeprecated) {
            pw.println();
            pw.println( "   * @deprecated " + enumDeprecated );
            pw.println( "   */" );
          }
          else {
            pw.println( " */" );
          }
        }
      }
      if (!Helper.isNullOrEmpty( enumDeprecated )) {
        pw.println( "  @Deprecated" );
      }
      pw.print( "  " + enumName + "( " + getValueDeclaration( value ) + " )" );
      isFirst = false;
    }
    pw.println( ";" );
    pw.println();
  }

  /**
   * Returns a string representing the enum entry's typed declaration.
   *
   * @param value
   * @return
   * @implNote For QName values just one common namespace is supported.
   */
  private String getValueDeclaration( String value ) {
    if (valueType instanceof QNameType) {
      QName id = QName.getInstance( value, namespaceTable );
      if (valueNamespace == null) {
        valueNamespace = id.getNamespace();
        valueNamespaceName = getNamespaceIdentifier( valueNamespace ).toLowerCase();
      }
      else if (valueNamespace != id.getNamespace()) {
        throw new RuntimeException( "enum values with different namespaces is currently not supported: " + getName() );
      }
      return "get" + upperCaseFirst( valueNamespaceName ) + "().getQName( \"" + id.getName() + "\" )";
    }
    else if (valueType instanceof StringType) {
      return "\"" + value + "\"";
    }
    else {
      return value;
    }
  }

  @Override
  protected void writeConstructor( PrintWriter pw ) {
    writeValueConstructor( pw );
  }

  private void writeValueConstructor( PrintWriter pw ) {
    pw.println( "  private " + valueClassName + " value;" );
    pw.println();
    pw.println( "  " + getName() + "( " + valueClassName + " value ) {" );
    pw.println( "    this.value = value;" );
    pw.println( "  }" );
    pw.println();
  }

  /**
   * Creates the typed getter.
   *
   * @param pw
   */
  private void writeValueGetter( PrintWriter pw ) {
    pw.println( "  /**" );
    pw.println( "   * Returns the " + valueClassName + " value of the " + getName() + " instance." );
    pw.println( "   *" );
    pw.println( "   * @return The " + valueClassName + " value of the " + getName() + " instance." );
    pw.println( "   */" );
    pw.println( "  public " + valueClassName + " getValue() {" );
    pw.println( "    return this.value;" );
    pw.println( "  }" );
    pw.println();
  }

  /**
   * Creates the typed conversion method.
   *
   * @param pw
   * @param fromType
   */
  private void writeFrom( PrintWriter pw, SimpleType fromType ) {
    String fromClassName = getClassForType( fromType, getPackageName() );
    String fromValueName = lowerCaseFirst(fromClassName) + "Value";
    String valueName = lowerCaseFirst(valueClassName) + "Value";
    pw.println("  /**");
    pw.println("   * Returns the " + getName() + " instance associated with the given " + fromClassName + " value.");
    pw.println("   *");
    pw.println("   * @param " + fromValueName + " The value to associate with an " + getName() + " instance");
    pw.println("   * @return The " + getName() + " instance associated with the given " + fromClassName + " value.");
    pw.println("   */");
    pw.println("  public static " + getName() + " from" + fromClassName + "( " + fromClassName + " " + fromValueName + " ) {");
    pw.println("    if (" + fromValueName + " == null) {");
    pw.println("      return null;");
    pw.println("    }");
    if (fromType != valueType) {
      writeConvert(pw, fromType);
    }
    pw.println("    for (" + getName() + " entry : values()) {");
    pw.println("      if (entry.value.equals( " + valueName + " )) {");
    pw.println("        return entry;");
    pw.println("      }");
    pw.println("    }");
    pw.println("    return null;");
    pw.println("  }");
    pw.println();
  }

  /**
   * Creates the override of the toString method.
   *
   * @param pw
   */
  private void writeToString( PrintWriter pw ) {
    pw.println( "  /**" );
    pw.println( "   * Returns the name of this enum constant as declared in the schema." );
    pw.println( "   *" );
    pw.println( "   * @return The name of this enum constant as declared in the schema." );
    pw.println( "   */" );
    pw.println( "  @Override" );
    pw.println( "  public String toString() {" );
    pw.print( "    return " );
    if (valueType == StringType.getDefString()) {
      pw.print( "value" );
    }
    else if (valueType instanceof QNameType) {
      pw.print( "value.getName()" );
    }
    else if (valueType instanceof IntegerType) {
      pw.print( "Integer.toString( value )" );
    }
    else {
      pw.print( "value.toString()" );
    }
    pw.println( ";" );
    pw.println( "  }" );
    pw.println();
  }

  /**
   * Returns a string containing the command which will convert a value of the given type
   * into the enum's value type.
   *
   * @param pw
   * @param fromType
   * @implNote Currently only from String to QName/Integer is provided.
   */
  private void writeConvert( PrintWriter pw, SimpleType fromType ) {
    String fromClassName = getClassForType( fromType, getPackageName() );
    String fromValueName = lowerCaseFirst( fromClassName ) + "Value";
    String valueName = lowerCaseFirst( valueClassName ) + "Value";
    if (fromType == StringType.getDefString()) {
      if (valueType instanceof QNameType) {
        pw.println( "   " + valueClassName + " " + valueName + " = QName.getInstance( get" + upperCaseFirst( valueNamespaceName ) + "(), " + fromValueName + " );" );
        return;
      }
      else if (valueType instanceof IntegerType) {
        pw.println( "   " + valueClassName + " " + valueName + " = Integer.valueOf( " + fromValueName + " );" );
        return;
      }
    }
    Logger.error( "unknown enum convert combination from:" + fromType + " to:" + valueType );
  }

  /**
   * If documentation or deprecated mark exists in schema use it.
   * Use existing JavaDoc only if no documentation or deprecated mark exists in schema.
   *
   * @param pw
   */
  protected void writeDocumentation( PrintWriter pw ) {
    if (hasDocumentation() || isDeprecated()) {
      pw.println( "/**" );
      if (hasDocumentation()) {
        Object documentation = getDocumentationAnnotation();
        if (documentation instanceof Documentation) {
          for (SimpleModel paragraph : ((Documentation) documentation).paragraphIter()) {
            pw.println( " * <p>" + paragraph.getContent() + "</p>" );
          }
        }
      }
      if (isDeprecated()) {
        pw.println( " * @deprecated " + getDeprecatedAnnotation().toString() );
      }
      pw.println( " */" );
    }
    else if (hasJavadoc()) {
      pw.println( "/**" );
      List javadocLines = getJavadocLines();
      for (int i = 0; i < javadocLines.size(); i++) {
        pw.println( (String) javadocLines.get( i ) );
      }
      pw.println( " */" );
    }
  }
}
