/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.generator;

import de.pidata.log.Logger;
import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.AnyElement;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.xml.schema.*;
import de.pidata.qnames.Key;
import de.pidata.qnames.QName;
import de.pidata.string.Helper;
import de.pidata.tools.CodeGenFactory;

import java.io.CharArrayWriter;
import java.io.PrintWriter;
import java.util.List;
import java.util.*;

/**
 * Generates the Java class for the given type.
 */
public class TypeClassGenerator extends ClassGenerator {

  private final List<QName> attributeNames = new ArrayList<>();
  private final List<String> collectionFieldRows = new ArrayList<>();
  private final List<QName> childNames = new ArrayList<>();
  private final Map<QName, Object> memberDocumentations = new HashMap<>();
  private final Map<QName, String> memberDeprecations = new HashMap<>();
  private final List<QName> transientAttributeNames = new ArrayList<>();
  private final List<SimpleType> transientAttributeTypes = new ArrayList<>();
  private final List<QName> transientChildNames = new ArrayList<>();
  private final List<Relation> transientChildTypes = new ArrayList<>();
  private boolean isMixed;
  private String typeClass;

  private final ComplexType type;

  public TypeClassGenerator(QName typeName, String packageName, String outputDirName, FactoryClassGenerator factoryClassGenerator, ComplexType type) throws ClassNotFoundException {
    super( buildValidTypeClassName( typeName ), typeName.getNamespace(), packageName, factoryClassGenerator, outputDirName );
    this.type = type;

    if (type instanceof ComplexTypeDef) {
      typeClass = (String) ((ComplexTypeDef) type).get( CodeGenFactory.ID_TYPE_CLASS );
    }

    Type baseType = type.getBaseType();
    if (baseType instanceof ComplexType) {
      if (baseType.name().getNamespace() == targetNamespace) {
        setExtension( buildValidTypeClassName( baseType.name() ) );
      }
      else {
        String ext = baseType.getValueClass().getName();
        addImport( ext );
        setExtension( ext );
      }
    }
    else if (Helper.isNullOrEmpty( getExtension() )) {
      setExtension( SequenceModel.class.getName() );
    }

    //----- attributes
    for (int i = firstAttrIndex(); i < type.attributeCount(); i++) {
      QName attributeName = type.getAttributeName( i );
      int keyIndex = type.getKeyIndex( attributeName );
      boolean readOnly = (keyIndex != -1);
      Object attrDeprecatedAnnotation = null;
      Object attrDocumentation = null;
      if (type instanceof ComplexTypeDef) {
        // declarations hold own attributes only so index may differ TODO: here or in Schema2Class?
        AttrDecls attrDecls = ((ComplexTypeDef) type).getAttrDecls();
        int declIndex = attrDecls.indexOfAttribute( attributeName );
        Attribute attribute = attrDecls.getAttribute( declIndex );
        attrDeprecatedAnnotation = attribute.get( CodeGenFactory.ID_DEPRECATED );
        Annotation annotation = attribute.getAnnotation();
        if (annotation != null) {
          attrDocumentation = annotation.getDocumentation();
        }
        if (attrDocumentation == null) {
          attrDocumentation = attribute.get( CodeGenFactory.ID_DOC );
        }
      }
      addAttribute( attributeName, type.getAttributeType( i ), readOnly, attrDeprecatedAnnotation, attrDocumentation );
    }

    //----- children
    int i;
    if (baseType instanceof ComplexType) {
      i = ((ComplexType) baseType).relationCount();
    }
    else {
      i = 0;
    }
    for (QNameIterator relIter = type.relationNames(); relIter.hasNext(); ) {
      QName relName = relIter.next();
      if (i <= 0) {
        Relation childRel = type.getRelation( relName );
        Object documentation = null;
        Object deprecatedAnnotation = null;
        if (childRel instanceof Annotated) {
          Annotated annotatedChildRel = (Annotated) childRel;
          deprecatedAnnotation = annotatedChildRel.get( CodeGenFactory.ID_DEPRECATED );
          Annotation annotation = annotatedChildRel.getAnnotation();
          if (annotation != null) {
            documentation = annotation.getDocumentation();
          }
        }
        if (documentation == null && childRel instanceof LocalElement) {
          documentation = ((LocalElement) childRel).get( CodeGenFactory.ID_DOC );
        }
        Type childType = childRel.getChildType();
        // generate subtypes for named local elements
        if (childRel instanceof LocalElement && childType instanceof LocalComplexType) {
          Object childRelName = ((LocalElement) childRel).get( Element.ID_NAME );
          if (childRelName != null) {
            expandType( (ComplexTypeDef) childType );
          }
        }

        int minOccurs = getEffectiveMinOccurs( childRel );
        int maxOccurs = getEffectiveMaxOccurs( childRel );
        addChild( relName, childType, false, minOccurs, maxOccurs, childRel.getCollection(), deprecatedAnnotation, documentation );
      }
      else {
        // parent relations überspringen
        i--;
      }
    }

    //----- key references
    for (QNameIterator keyRefNames = type.keyRefNames(); keyRefNames.hasNext(); ) {
      QName refName = keyRefNames.next();
      buildValidIDConstant( refName );
    }
  }

  protected List<QName> getTransientChildNames() {
    return transientChildNames;
  }

  protected List<Relation> getTransientChildTypes() {
    return transientChildTypes;
  }

  /**
   * Mark mixed content allowed for XML data.
   * @param isMixed
   */
  public void setMixed( boolean isMixed ) {
    this.isMixed = isMixed;
  }

  /**
   * Check if mixed content is allowed for XML data.
   * @return
   */
  public boolean isMixed() {
    return isMixed;
  }

  /**
   * Generates a class for local complexType. The name of the local class is constructed as
   * eparentEementName +  '.local.' + elementName, see {@link LocalComplexType#name()}
   .   *
   * @param type the local complexType
   * @throws ClassNotFoundException
   */
  private void expandType( ComplexTypeDef type ) throws ClassNotFoundException {
    QName typeName = type.name();
    FactoryClassGenerator factoryClassGenerator = getFactoryClassGenerator();
    factoryClassGenerator.addLocalType( type, true );

    TypeClassGenerator classGen = new TypeClassGenerator( typeName, getPackageName(), getOutputDirName(), factoryClassGenerator, type );

    // TODO: get annotations/documentation?
    classGen.generate();
    classGen.writeStaticType( factoryClassGenerator.getConstantsPW(), factoryClassGenerator.getStaticPW(), factoryClassGenerator );
  }

  protected int firstAttrIndex() {
    Type baseType = type.getBaseType();
    if (baseType instanceof ComplexType) {
      return ((ComplexType) baseType).attributeCount();
    }
    else {
      return 0;
    }
  }

  /**
   * Creates getter and, if not readonly, setter methods for the given attribute.
   * @param attrName
   * @param attrType
   * @param readOnly
   * @param deprecatedAnnotation
   * @param documentation
   */
  protected void addAttribute( QName attrName, SimpleType attrType, boolean readOnly, Object deprecatedAnnotation, Object documentation ) {
    if (!attributeNames.contains(attrName)) {
      attributeNames.add( attrName );
      MethodGenerator method = new GetterMethod(this, attrName, attrType, true);
      if (!readOnly) {
        method = new SetterMethod(this, attrName, attrType, true);
      }
    }

    if (deprecatedAnnotation != null && deprecatedAnnotation instanceof String) {
      memberDeprecations.put( attrName, (String) deprecatedAnnotation );
    }
    if (documentation != null) {
      memberDocumentations.put( attrName, documentation );
    }
  }

  /**
   * Creates get/add/remove/count/iterator methods for multiple element relation.
   * Creates get/set method if relation holds only one element.
   * @param childName
   * @param childType
   * @param readOnly
   * @param minOccurs
   * @param maxOccurs
   * @param collection
   * @param deprecatedAnnotation
   * @param documentation
   */
  protected void addChild( QName childName, Type childType, boolean readOnly, int minOccurs, int maxOccurs, Class collection, Object deprecatedAnnotation, Object documentation ) {
    MethodGenerator method;
    if (childNames.contains( childName )) {
      Logger.warn( "duplicate child name: " + childName + " in " + getName() );
      return;
    }
    childNames.add( childName );
    if ((childName == AnyElement.ANY_ELEMENT) || (childType instanceof AnyElement)) {
      return;
    }

    if (deprecatedAnnotation != null && deprecatedAnnotation instanceof String) {
      memberDeprecations.put( childName, (String) deprecatedAnnotation );
    }
    if (documentation != null) {
      memberDocumentations.put( childName, documentation );
    }

    QName memberName = childName.getNamespace().getQName( childName.getName() );
    if (maxOccurs == 1) {
      method = new GetterMethod( this, memberName, childType, false );
      method = new SetterMethod( this, memberName, childType, false );
    }
    // write add/remove/count/iterator methods
    else {
      addImport( ModelIterator.class.getName() );
      method = new GetChildMethod( this, memberName, childType );
      method = new AddMethod( this, memberName, childType );
      method = new RemoveMethod( this, memberName, childType );
      method = new IteratorMethod( this, memberName, childType );
      method = new CountMethod( this, memberName );

      Class collectionImpl = getCollectionImpl( collection );
      String collPostfix = getCollectionPostfix( collection );

      addImport( collection.getName() );
      addImport( collectionImpl.getName() );
      String typeName;
      if (childType instanceof SimpleType) {
        typeName = "SimpleModel";
      }
      else {
        typeName = getClassForType( childType, getPackageName() );
      }
      String idConst = buildValidIDConstant( childName );
      String collectionName = buildValidCollectionName( memberName, collPostfix );
      collectionFieldRows.add( "  private final " + collection.getSimpleName() + "<" + typeName + "> " + collectionName + " = new " + collectionImpl.getSimpleName() + "<>( " + idConst + ", this );" );
      method = new CollectionMethod( this, memberName, collection.getSimpleName() + "<" + typeName + ">", collPostfix );
    }
  }

  public void addTransientAttribute( QName attrName, SimpleType attrType ) {
    if (!transientAttributeNames.contains(attrName)) {
      buildValidIDConstant( attrName );
      transientAttributeNames.add(attrName);
      transientAttributeTypes.add( attrType );
    }
  }

  public void addTransientChild( QName childName, Relation childRel ) {
    if (transientChildNames.contains(childName)) {
      return;
    }
    buildValidIDConstant( childName );
    transientChildNames.add( childName );
    transientChildTypes.add( childRel );
    if ((childName == AnyElement.ANY_ELEMENT) || (childRel instanceof AnyElement)) {
      return;
    }

    addImport( ModelIterator.class.getName() );
  }

  /**
   * Creates the static type declaration hold in the factory.
   * @param pwConsts
   * @param pwStatic
   * @param factoryClassGenerator
   */
  public void writeStaticType( PrintWriter pwConsts, PrintWriter pwStatic, FactoryClassGenerator factoryClassGenerator ) {
    AnyAttribute attrAny;
    Relation childRel = null;
    QName relName, attrName;
    String typeName = type.name().getName();

    //--- Static type definition
    addImport( ComplexType.class.getName() );
    addImport( DefaultComplexType.class.getName() );

    String staticTypeName = buildValidTypeConstant( type );
    if (!Helper.isNullOrEmpty( typeClass )) {
      addImport( typeClass );
      pwConsts.print( "  public static final ComplexType " + staticTypeName + " = new " + getClassPart( typeClass ) + "( NAMESPACE.getQName( \"" );
    }
    else {
      pwConsts.print( "  public static final ComplexType " + staticTypeName + " = new DefaultComplexType( NAMESPACE.getQName( \"" );
    }
    pwConsts.print( typeName );
    pwConsts.print( "\" ), " );
    if (collidesWithInternalClass()) {
      pwConsts.print( getPackageName() + "." );
    }
    pwConsts.print( getName() + ".class.getName(), " );
    int keyAttrCount = type.keyAttributeCount();
    Type baseType = type.getBaseType();
    if (baseType instanceof ComplexType) {
      keyAttrCount = keyAttrCount - ((ComplexType) baseType).keyAttributeCount();
    }
    pwConsts.print( keyAttrCount );
    boolean isSequenceModel = getExtension().equals( SequenceModel.class.getName() );
    if ( /* TODO: !isAbstract() && */ !isMixed() && !isDynamicType()) {
      // short constructor
      if (!isSequenceModel) {
        String parentTypeName = getTypeConstant( type.getBaseType(), factoryClassGenerator );
        pwConsts.print( ", " + parentTypeName );
      }
    }
    else  {
      if (isSequenceModel) {
        pwConsts.print( ", null " );
      }
      else {
        String parentTypeName = getTypeConstant( type.getBaseType(), factoryClassGenerator );
        pwConsts.print( ", " + parentTypeName );
      }

//      if (isAbstract()) { // TODO
//        pwConsts.print( ", true " );
//      }
//      else {
        pwConsts.print( ", false " );
//      }

      if (isMixed() || isDynamicType()) {
        pwConsts.print( ", true " );
      }
      else {
        pwConsts.print( ", false " );
      }

    }
    pwConsts.println( " );" );

    pwStatic.println( "    type = (DefaultComplexType) "+staticTypeName+";" );

    for (int i = 0; i < attributeNames.size(); i++) {
      attrName = attributeNames.get(i);
      int index = type.indexOfAttribute(attrName);
      SimpleType attrType = type.getAttributeType(index);
      Object attrDefault = type.getAttributeDefault(index);
      int keyIndex = type.getKeyIndex(attrName);
      writeAttributeDef( pwStatic, keyIndex, attrName, attrType, attrDefault, getFactoryClassName() );
    }
    attrAny = type.getAnyAttribute();
    if (attrAny != null) {
      writeAttributeDef( pwStatic, -1, attrAny.name(), attrAny, attrAny.createDefaultValue(), getFactoryClassName() );
    }

    for (QNameIterator keyRefNames = type.keyRefNames(); keyRefNames.hasNext(); ) {
      QName refName = keyRefNames.next();
      ModelReference ref = type.getKeyReference( refName );
      writeReference( pwStatic, ref );
    }

    for (int i = 0; i < childNames.size(); i++) {
      relName = childNames.get(i);
      childRel = type.getRelation( relName );
      writeRelation( pwStatic, childRel, true, getFactoryClassGenerator() );
    }
    pwStatic.println();
  }

  private boolean isDynamicType() {
    return (getFactoryClassGenerator().isDynamicFactory() && DynamicType.class.getName().equals( typeClass ));
  }

  private void writeTransientType( PrintWriter pw ) {
    addImport( DefaultComplexType.class.getName() );

    AnyAttribute attrAny;
    Relation childRel = null;
    QName  relName;
    QName attrName;
    String typeName = type.name().getName() + "_Transient";

    //----- declaration of constant type definition for the generated class
    pw.println( "  @Override" );
    pw.println( "  public ComplexType transientType() {" );
    pw.println( "    if (TRANSIENT_TYPE == null) {" );
    String className = this.getName();
    String classNameLCI = lowerCaseFirst( className );
    pw.println( "      TRANSIENT_TYPE = new DefaultComplexType( NAMESPACE.getQName( \"" + classNameLCI + "_transient\" ), " + className + ".class.getName(), 0, super.transientType() );" );

    for (int i = 0; i < transientAttributeNames.size(); i++) {
      attrName = transientAttributeNames.get( i );
      SimpleType attrType = transientAttributeTypes.get( i );
      writeAttributeDef( pw, -1, attrName, attrType, null, getFactoryClassName() );
    }

    for (int i = 0; i < transientChildNames.size(); i++) {
      childRel = transientChildTypes.get( i );
      writeTransientRelation( pw, childRel, true, getFactoryClassGenerator() );
    }

    pw.println( "      transientTypeManual" + getName() + "();" );
    pw.println( "    }" );
    pw.println( "    return TRANSIENT_TYPE;" );
    pw.println( "  }" );
  }

  protected void writeTransientRelation( PrintWriter pw, Relation childRel, boolean useIDConstants, FactoryClassGenerator factoryClassGenerator ) {
    throw new RuntimeException( "TODO: writeTransientRelation() not (yet) implemented in default generator." );
  }

  protected void writeTransientChildIter( PrintWriter pw ) {
    throw new RuntimeException( "TODO: writeTransientChildIter() not (yet) implemented in default generator." );
  }

  private void writeReference( PrintWriter pw, ModelReference ref ) {
    pw.print( "    type.addKeyReference( " );
    pw.print( buildValidIDConstant( ref.getName() ) + ", " );
    QName refTypeName = ref.getRefTypeName();
    if (refTypeName.getNamespace() == targetNamespace) {
      pw.print( "NAMESPACE.getQName( \"" + refTypeName.getName() + "\" ), " );
    }
    else {
      pw.print( "QName.getInstance( \"" + refTypeName.toString() + "\" ), " );
    }
    pw.println( getName() + "." + buildValidIDConstant( ref.getRefAttribute( 0 ) ) + " );" );
  }

  /**
   * Adds the class constructors.
   * @param pw
   */
  @Override
  protected void writeConstructor( PrintWriter pw ) {
    addImport( ChildList.class.getName() );
    addImport( Hashtable.class.getName() );
    addImport( Key.class.getName() );
    addImport( ComplexType.class.getName() );

    String staticType = getFactoryClassName() + "." + buildValidTypeConstant( type );
    if (type.keyAttributeCount() == 0) {
      pw.println( "  public " + getName() + "() {" );
      pw.println( "    super( null, " + staticType + ", null, null, null );" );
      pw.println( "  }" );
      pw.println();
    }
    else {
      pw.println( "  public " + getName() + "( Key id ) {" );
      pw.println( "    super( id, " + staticType + ", null, null, null );" );
      pw.println( "  }" );
      pw.println();
    }  
    pw.println( "  public " + getName() + "( Key key, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {");
    pw.println( "    super( key, " + staticType + ", attributeNames, anyAttribs, childNames );");
    pw.println( "  }" );
    pw.println();
    pw.println( "  protected " + getName() + "( Key key, ComplexType type, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {" );
    pw.println( "    super( key, type, attributeNames, anyAttribs, childNames );" );
    pw.println( "  }");
    pw.println();
  }

  /**
   * Write class body. Overwrite this method in subclasses to create your code. This method
   * is called before headers and import declarations are created so you can still add import
   * statements here.
   *
   * @param pw
   */
  @Override
  protected void writeClassBody( PrintWriter pw ) {
    boolean hasTransient =  ((!transientAttributeNames.isEmpty()) || (!transientChildNames.isEmpty()));
    CharArrayWriter cw = new CharArrayWriter();
    PrintWriter mem = new PrintWriter( cw );
    writeMethods( mem );
    mem.flush();
    mem.close();

    if (hasTransient) {
      for (int i = 0; i < transientChildNames.size(); i++) {
        Relation childRel = transientChildTypes.get( i );
        addTransientRelationId( childRel );
      }
    }

    writeIDConstants( pw );

    //--- Static transient type definition and attributes
    if (hasTransient) {
      pw.println( "  private static DefaultComplexType TRANSIENT_TYPE;" );
      pw.println();
    }

    //-- Collection fields
    for (String row : collectionFieldRows) {
      pw.println( row );
    }
    if (!collectionFieldRows.isEmpty()) {
      pw.println();
    }

    //-- Constructor
    writeConstructor( pw );

    //-- Methods for transient fields
    if (hasTransient) {
      writeTransientType( pw );
      pw.println();
      writeTransientChildIter( pw );
      pw.println();
    }

    pw.print( cw.toCharArray() );
  }

  @Override
  protected void writeMemberDocumentation( PrintWriter pw, QName memberName ) {
    Object documentation = memberDocumentations.get( memberName );
    if (documentation != null) {
      if (documentation instanceof Documentation) {
        for (SimpleModel paragraph : ((Documentation) documentation).paragraphIter()) {
          pw.println( "   * <p>" + paragraph.getContent() + "</p>" );
        }
      }
      else {
        pw.println( "   * <p>" + documentation.toString() + "</p>" );
      }
    }
  }

  @Override
  protected void writeMemberDeprecated( PrintWriter pw, QName memberName ) {
    String childDeprecated = memberDeprecations.get( memberName );
    if (!Helper.isNullOrEmpty( childDeprecated )) {
      pw.println( "   * @deprecated " + childDeprecated );
    }
  }

  @Override
  protected void writeMemberAnnotations( PrintWriter pw, QName memberName ) {
    String childDeprecated = memberDeprecations.get( memberName );
    if (!Helper.isNullOrEmpty( childDeprecated )) {
      pw.println( "  @Deprecated" );
    }
  }

  /**
   * If documentation or deprecated mark exists in schema use it.
   * Use existing JavaDoc only if no documentation or deprecated mark exists in schema.
   *
   * @param pw
   */
  @Override
  protected void writeDocumentation( PrintWriter pw ) {
    if (hasDocumentation() || isDeprecated()) {
      pw.println( "/**" );
      if (hasDocumentation()) {
        Object documentation = getDocumentationAnnotation();
        if (documentation instanceof Documentation) {
          for (SimpleModel paragraph : ((Documentation) documentation).paragraphIter()) {
            pw.println( " * <p>" + paragraph.getContent() + "</p>" );
          }
        }
      }
      if (isDeprecated()) {
        pw.println( " * @deprecated " + getDeprecatedAnnotation().toString() );
      }
      pw.println( " */" );
    }
    else if (hasJavadoc()) {
      pw.println( "/**" );
      List javadocLines = getJavadocLines();
      for (int i = 0; i < javadocLines.size(); i++) {
        pw.println( (String) javadocLines.get( i ) );
      }
      pw.println( " */" );
    }
  }
}
