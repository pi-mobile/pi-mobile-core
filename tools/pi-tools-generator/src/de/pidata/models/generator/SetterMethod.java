/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.generator;

import de.pidata.models.tree.SimpleModel;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

import java.io.PrintWriter;

public class SetterMethod extends MethodGenerator {

  private QName memberName;
  private Type    type;
  private boolean attribute;

  public SetterMethod( ClassGenerator parentClass, QName memberName, Type type, boolean attribute ) {
    super( parentClass, buildValidMethodName( "set", memberName.getName(), "" ) );
    this.memberName = memberName;
    this.type = type;
    this.attribute = attribute;
  }

  protected void writeMethod(PrintWriter pw) {
    String typeClassName = getClassForType( type, parentClass.getPackageName() );
    String variable = buildValidVariableName( memberName );
    writeMethodDeclaration(pw, variable, typeClassName);
    pw.println(" {");
    if (attribute) {
      pw.println( "    set( " + parentClass.buildValidIDConstant( memberName ) + ", " + variable + " );" );
    }
    else {
      String idConst = parentClass.buildValidIDConstant( memberName );
      if (type instanceof SimpleType) {
        parentClass.addImport( SimpleModel.class.getName());
        String typeDef = getSimpleTypeDefinition( (SimpleType) type, parentClass.getTargetNamespace(), parentClass.getFactoryClassName() );
        pw.println( "    setChild( " + idConst + ", new SimpleModel( " + typeDef + ", " + variable + " ) );" );
      }
      else {
        pw.println( "    setChild( " + idConst + ", " + variable + " );" );
      }
    }
    pw.println( "  }" );
  }

  protected void writeMethodInterface(PrintWriter pw) {
    String typeClassName = getClassForType( type, parentClass.getPackageName() );
    String variable = buildValidVariableName( memberName );
    writeMethodDeclaration(pw, variable, typeClassName);
    pw.println(";");
  }

  private void writeMethodDeclaration(PrintWriter pw, String variable, String typeClassName) {
    String member;
    if (attribute) {
      member = "attribute";
    }
    else {
      member = "element";
    }
    pw.println( "  /**" );
    pw.println( "   * Set the " + member + " " + memberName.getName() + "." );
    parentClass.writeMemberDocumentation( pw, memberName );
    pw.println( "   *" );
    pw.println( "   * @param " + variable + " new value for " + member + " " + memberName.getName() );
    parentClass.writeMemberDeprecated( pw, memberName );
    pw.println( "   */" );
    parentClass.writeMemberAnnotations( pw, memberName );
    pw.print( "  public void " + getName() + "( " + typeClassName + " " + variable + " )" );
    if (hasThrowException()) {
      pw.print(" throws " + getThrowException());
    }
  }

}
