/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.generator;

import de.pidata.qnames.QName;

import java.io.PrintWriter;

public class CountMethod extends MethodGenerator {

  private QName memberName;

  public CountMethod(ClassGenerator parentClass, QName memberName) {
    super( parentClass, buildValidMethodName( "", memberName.getName(), "Count" ) );
    this.memberName = memberName;
  }

  protected void writeMethod(PrintWriter pw) {
    writeMethodDeclaration( pw );
    pw.println(" {" );
    pw.println( "    return childCount( " + parentClass.buildValidIDConstant( memberName ) + " );" );
    pw.println( "  }" );
  }

  protected void writeMethodInterface(PrintWriter pw) {
    writeMethodDeclaration( pw );
    pw.println( ";" );
  }

  private void writeMethodDeclaration( PrintWriter pw ) {
    pw.println( "  /**" );
    pw.println( "   * Returns the number of " + memberName.getName() + "s." );
    parentClass.writeMemberDocumentation( pw, memberName );
    pw.println( "   *" );
    pw.println( "   * @return the number of " + memberName.getName() + "s" );
    parentClass.writeMemberDeprecated( pw, memberName );
    pw.println( "   */" );
    parentClass.writeMemberAnnotations( pw, memberName );
    pw.print( "  public int " + getName() + "()" );
    if (hasThrowException()) {
      pw.print(" throws " + getThrowException());
    }
  }
}
