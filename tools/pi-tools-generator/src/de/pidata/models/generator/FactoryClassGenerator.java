/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.generator;

import de.pidata.log.Logger;
import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.*;
import de.pidata.models.xml.schema.SimpleTypeDef;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.string.Helper;
import de.pidata.tools.CodeGenFactory;

import java.io.CharArrayWriter;
import java.io.PrintWriter;
import java.util.*;

/**
 * Generates the factory class which holds and builds the defined types and elements.
 */
public class FactoryClassGenerator extends ClassGenerator {

  private final List<SimpleTypeDef> simpleTypes = new ArrayList<>();
  private final List<ComplexType> complexTypes = new ArrayList<>();
  private final List<ComplexType> localTypes = new ArrayList<>();
  private final List<Relation> rootElements = new ArrayList<>();
  private final Hashtable rootAttributes = new Hashtable();

  private final String schemaLocation;
  private final String schemaVersion;

  private boolean isDynamicFactory = false;

  private CharArrayWriter contantsCW;
  private CharArrayWriter staticCW;

  public FactoryClassGenerator( String className, Namespace targetNamespace, String packageName, String outputDirName, String schemaLocation, String schemaVersion, String factoryImplementation ) {
    super( className, targetNamespace, packageName, null, outputDirName );
    this.schemaLocation = schemaLocation;
    this.schemaVersion = schemaVersion;

    addImport( Namespace.class.getName() );
    addImport( QName.class.getName() );
    addImport( Key.class.getName() );
    addImport( de.pidata.models.types.Type.class.getName() );
    addImport( java.util.Hashtable.class.getName() );

    // first check manual extension, then implementation hint from schema
    String factoryExtension = getExtension();
    if (Helper.isNullOrEmpty( factoryExtension ) || AbstractModelFactory.class.getName().equals( factoryExtension )) {
      if (Helper.isNullOrEmpty( factoryImplementation )) {
        setExtension( AbstractModelFactory.class.getName() );
      }
      else {
        setExtension( factoryImplementation );
        factoryExtension = factoryImplementation;
      }
    }
    if (DynamicModelFactory.class.getName().equals( factoryExtension )) {
      isDynamicFactory = true;
    }
    addNamespace( targetNamespace, "NAMESPACE" );
  }

  @Override
  public FactoryClassGenerator getFactoryClassGenerator() {
    return this;
  }

  public boolean isDynamicFactory() {
    return isDynamicFactory;
  }

  public void addSimpleType( SimpleTypeDef simpleType, boolean asConstant) {
    if (this.simpleTypes.contains( simpleType )) {
      return;
    }
    addImport( simpleType.getBaseType().getClass().getName());
    this.simpleTypes.add( simpleType );
  }

  public void addComplexType(ComplexType complexType, boolean asConstant) {
    if (this.complexTypes.contains( complexType )) {
      Logger.warn( "ComplexType already added: " + complexType.name() ); // TODO
    }
    this.complexTypes.add( complexType );
  }

  public void addLocalType(ComplexType complexType, boolean asConstant) {
    if (this.localTypes.contains( complexType )) {
      Logger.warn( "local ComplexType already added: " + complexType.name() ); // TODO
    }
    this.localTypes.add( complexType );
  }

  public void addRootElement(Relation rootRel) {
    for (Relation relation : this.rootElements) {
      if (relation.getRelationID() == rootRel.getRelationID()) {
        Logger.warn( "root ELement already added: " + rootRel.getRelationID() ); // TODO
      }
    }
    this.rootElements.add( rootRel );
    buildValidIDConstant( rootRel.getRelationID() );
  }

  public void addRootAttribute(QName name, SimpleType type) {
    this.rootAttributes.put( name, type );
    buildValidIDConstant( name );
  }

  private String createSimpleType( String typeName, SimpleTypeDef type, boolean asConstant ) { // TODO: almost duplicate in CodeGenerator?
    QName typeID = type.name();
    String baseTypeName = null;
    String params = null;
    SimpleType baseType = (SimpleType) type.getBaseType();
    boolean minIncl = true;
    boolean maxIncl = true;
    Restriction restriction = type.getRestriction();
    String temp;
    String implementation = (String) type.get( CodeGenFactory.ID_IMPLEMENTATION );

    while (baseType instanceof SimpleTypeDef) {
      baseType = (SimpleType) baseType.getBaseType();
    }
    // handle enums first
    if (type.isEnumDef()) {
      baseTypeName = "StringType";
      params = getEnumParams( type, baseType, implementation );
    }
    else {
      if (baseType instanceof QNameType) {
        baseTypeName = "QNameType";
        int min, max;
        if (restriction == null) {
          min = 0;
          max = Integer.MAX_VALUE;
        }
        else {
          min = restriction.minLength();
          max = restriction.maxLength();
        }
        params = Integer.toString( min ) + ", ";
        if (max == Integer.MAX_VALUE) {
          params += "Integer.MAX_VALUE";
        }
        else {
          params += Integer.toString( max );
        }
      }
      else if (baseType instanceof IntegerType) {
        baseTypeName = "IntegerType";
//      if (valueClass.getName().endsWith("Integer")) {
//        result = new Integer((int)resultSet.getInt(columnName));
//      }
//      if (valueClass.getName().endsWith("Long")) {
//        result = new Long(resultSet.getInt(columnName));
//      }
//      if (valueClass.getName().endsWith("Short")) {
//        result = new Short((short) resultSet.getInt(columnName));
//      }
//      if (valueClass.getName().endsWith("Byte")) {
//        result = new Byte((byte) resultSet.getInt(columnName));
//      }

        int min, max;
        if (restriction == null) {
          min = 0;
          max = Integer.MAX_VALUE;
          minIncl = true;
          maxIncl = true;
        }
        else {
          min = restriction.minLength();
          max = restriction.maxLength();
          //TODO minIncl = restriction.minInclusive();
          //TODO maxIncl = restriction.maxInclusive();
        }
        params = "Integer.class, IntegerType.getDefInt(), "; //todo: unterscheiden in long/integer/...
        //if (max == Integer.MAX_VALUE) {
        params += "Integer.MAX_VALUE";
        //}
        //else {
        //  params += Integer.toString(max);
        //}
        params += ", " + Integer.toString( min ) + ", ";
        params += Boolean.toString( maxIncl ) + ", " + Boolean.toString( minIncl );
      }
      else if (baseType instanceof DecimalType) {
        baseTypeName = "DecimalType";
        DecimalType decType = (DecimalType) baseType;
        int min, max, totalDigits, fractionDigits;
        if (restriction == null) {
          totalDigits = decType.getTotalDigits();
          fractionDigits = decType.getFractionDigits();
        }
        else {
          totalDigits = restriction.totalDigits();
          fractionDigits = restriction.fractionDigits();
        }
        params = Integer.toString( totalDigits ) + ", " + Integer.toString( fractionDigits );
      }
      else if (baseType instanceof BooleanType) {
        baseTypeName = "BooleanType";
      }
      else if (baseType instanceof DateTimeType) {
        baseTypeName = "DateTimeType";
        params = "DateTimeType.getDefDateTime()";
      }
      //TODO weitere restrictions implementieren, StringType als letzten lassen
      else if (baseType instanceof StringType) {
        baseTypeName = "StringType";
        if (restriction == null) {
          params = "0, Integer.MAX_VALUE";
        }
        else {
          int min = restriction.minLength();
          int max = restriction.maxLength();
          params = Integer.toString( min ) + ", ";
          if (max == Integer.MAX_VALUE) {
            params += "Integer.MAX_VALUE";
          }
          else {
            params += Integer.toString( max );
          }
        }
      }
    }

    if (baseTypeName == null) {
      throw new IllegalArgumentException("Unsupported simple type ID="+typeID+", baseType ID="+baseType.name());
    }

    StringBuffer buffer = new StringBuffer();
    if (asConstant) {
      buffer.append("  public static final ").append(baseTypeName).append(" ");
      buffer.append( buildValidTypeConstant( type ) ).append(" = ");
      typeName = "NAMESPACE.getQName(\"" + typeID.getName() + "\")";
    }
    buffer.append( "new " ).append( baseTypeName ).append( "( " ).append( typeName );
    if (params != null) {
      buffer.append( ", " ).append( params );
    }
    buffer.append( " )" );
    if (asConstant) {
      buffer.append( ";\n" );
    }
    return buffer.toString();
  }

  private String getEnumParams( SimpleTypeDef type, SimpleType baseType, String implementation ) {
    String params;
    String valueTypeDefinition = "StringType.getDefString()"; // TODO??? getSimpleTypeDefinition( baseType );
    if ((implementation == null) || (implementation.length() == 0)) {
      String classname = buildValidTypeClassName( type.name() );
      if (collidesWithInternalClass( classname )) {
        classname = getPackageName() + "." + classname;
      }
      params = valueTypeDefinition + ", " + classname + ".class";
    }
    else {
      params = valueTypeDefinition + ", " + implementation + ".class";
    }
    return params;
  }

  protected void writeConstants(PrintWriter pw) {
    super.writeConstants(pw);
    writeIDConstants( pw );
    for (int typeNum = 0; typeNum < this.simpleTypes.size(); typeNum++) {
      SimpleTypeDef simpleType = simpleTypes.get( typeNum );
      pw.println( createSimpleType(simpleType.name().getName(), simpleType, true) );
    }
  }

  private void insertTypeDef( PrintWriter pw, ComplexType type, boolean typeDeclared, FactoryClassGenerator factoryClassGenerator ) {
    if (!typeDeclared) {
      addImport(DefaultComplexType.class.getName());
      pw.println( "    DefaultComplexType type;");
    }
    addImport(DefaultModel.class.getName());
    pw.println( "    type = new DefaultComplexType( null, \"" + DefaultModel.class.getName() + "\" );" );

    int count = type.attributeCount();
    for (int i = 0; i < type.attributeCount(); i++) {
      QName attrName = type.getAttributeName(i);
      SimpleType attrType = type.getAttributeType(i);
      Object attrDefault = type.getAttributeDefault( i );
      int keyIndex = type.getKeyIndex(attrName);
      writeAttributeDef( pw, keyIndex, attrName, attrType, attrDefault, getName() );
    }
    AnyAttribute attrAny = type.getAnyAttribute();
    if (attrAny != null) {
      writeAttributeDef( pw, -1, attrAny.name(), attrAny, attrAny.createDefaultValue(), getName() );
    }

    for (QNameIterator relIter = type.relationNames(); relIter.hasNext(); ) {
      QName relName = relIter.next();
      Relation childRel = type.getRelation(relName);
      writeRelation( pw, childRel, false, factoryClassGenerator );
    }
  }

  /**
   * Adds the factory constructor. Within the constructor all root types/elements/attributes will be added to the factory.
   * @param pw
   */
  protected void writeConstructor( PrintWriter pw ) {
    boolean typeDeclared = false;
    //----- constructor
    pw.print( "  public " + getName() + "() {\n" );
    pw.print( "    super( NAMESPACE, \"" );
    pw.print( this.schemaLocation + "\", \"" + this.schemaVersion + "\" );\n" );
    for (int typeNum = 0; typeNum < this.simpleTypes.size(); typeNum++) {
      SimpleTypeDef simpleType = simpleTypes.get( typeNum );
      pw.print( "    addType( " + buildValidTypeConstant( simpleType ) + " );\n" );
    }
    for (ComplexType complexType : this.complexTypes) {
      pw.print( "    addType( " + buildValidTypeConstant( complexType ) + " );\n" );
    }
    for (ComplexType complexType : this.localTypes) {
      pw.print( "    addType( " + buildValidTypeConstant( complexType ) + " );\n" );
    }
    for (Relation relation : rootElements) {
      Type childType = relation.getChildType();
      String typeName;
      if (childType.name() == null) {
        insertTypeDef( pw, (ComplexType) childType, typeDeclared, this );
        typeDeclared = true;
        typeName = "type";
      }
      else {
        if (childType.name().getNamespace() == getTargetNamespace()) {
          typeName = buildValidTypeConstant( childType );
        }
        else if (childType instanceof SimpleType) {
          typeName = getSimpleTypeDefinition( (SimpleType) childType, targetNamespace, this.getFactoryClassName() );
        }
        else {
          typeName = getTypeConstant( childType, this );
        }
      }
      pw.print( "    addRootRelation( " + buildValidIDConstant( relation.getRelationID() ) + ", " );
      pw.print( typeName + ", " + relation.getMinOccurs() );
      pw.print( ", " + relation.getMaxOccurs() );
      pw.print( ", " + buildValidIDConstant( relation.getSubstitutionGroup() ) + " );\n" );
    }
    if (this.rootAttributes.size() > 0) {
      pw.print( "    attributes = new Hashtable();\n" );
      for (Enumeration attrEnum = this.rootAttributes.keys(); attrEnum.hasMoreElements(); ) {
        QName attrName = (QName) attrEnum.nextElement();
        SimpleType attrType = (SimpleType) this.rootAttributes.get( attrName );
        QName typeName = attrType.name();
        if (attrType.name().getNamespace() == getTargetNamespace()) {
          pw.print( "    attributes.put( " + buildValidIDConstant( attrName ) );
          pw.print( ", " + buildValidTypeConstant( attrType ) + ");\n" );
        }
        else {
          // find the declaration within the other factory
          pw.print( "    attributes.put( " + buildValidIDConstant( attrName ) );
          pw.print( ", " + getSimpleTypeDefinition( attrType, targetNamespace, this.getFactoryClassName() ) + ");\n" );
        }
      }
    }
    pw.println( "  }" );
    pw.println();
  }

  /**
   * Adds the factory method 'createInstance' which will be used to create instances of all managed types within the factory.
   * @param pw
   */
  private void writeCreateInstance(PrintWriter pw) {
    addImport( de.pidata.models.tree.Model.class.getName() );
    addImport( de.pidata.models.tree.ChildList.class.getName() );
    pw.print("  public Model createInstance( Key key, Type typeDef, Object[] attributes, Hashtable<QName, Object> anyAttribs, ChildList children ) {\n");
    pw.print("    Class modelClass = typeDef.getValueClass();\n");
    for (ComplexType complexType : this.complexTypes) {
      if (!complexType.isAbstract()) {
        String classname = getClassForType( complexType, getPackageName() );
        pw.print( "    if (modelClass == " + classname + ".class) {\n" );
        pw.print( "      return new " + classname + "( key, attributes, anyAttribs, children );\n    }\n" );
      }
    }
    for (ComplexType complexType : this.localTypes) {
      if (!complexType.isAbstract()) {
        String classname = getClassForType( complexType, getPackageName() );
        pw.print( "    if (modelClass == " + classname + ".class) {\n" );
        pw.print( "      return new " + classname + "( key, attributes, anyAttribs, children );\n    }\n" );
      }
    }
    pw.print("    return super.createInstance( key, typeDef, attributes, anyAttribs, children );\n");
    pw.print("  }\n\n");
  }

  protected void writeMethods(PrintWriter pw) {
    writeCreateInstance(pw);
    super.writeMethods(pw);
  }

  public PrintWriter getConstantsPW() {
    if (contantsCW == null) {
      contantsCW = new CharArrayWriter();
    }
    return new PrintWriter( contantsCW );
  }

  public PrintWriter getStaticPW() {
    if (staticCW == null) {
      staticCW = new CharArrayWriter();
    }
    return new PrintWriter( staticCW );
  }

  /**
   * Perform class file generation. Files will be generated in a directory
   * according to the package name.
   */
  public void generate() {
    try {
      PrintWriter pw = createPrintWriter();

      CharArrayWriter cw = new CharArrayWriter();
      PrintWriter mem = new PrintWriter( cw );
      writeClassBody( mem );
      mem.flush();
      mem.close();

      addImport( Namespace.class.getName() );

      pw.println( getHeader() );
      pw.println();
      if (getPackageName() != null) {
        pw.println( "package " + getPackageName() + ";" );
        pw.println();
      }
      writeImports( pw );

      writeClassDeclaration(pw);

      writeConstants( pw );
      pw.print( contantsCW.toCharArray() );
      pw.println();
      pw.println( "  static {" );
      pw.println( "    DefaultComplexType type;" );
      pw.print( staticCW.toCharArray() );
      pw.println( "  }" );
      pw.println();

      pw.print( cw.toCharArray() );

      writeExtensionLines( pw );

      pw.flush();
      pw.close();
      System.out.println("Generated class: "+getPackageName()+"."+getName());
    }
    catch (Exception ex) {
      String msg = "Error generation class: "+getPackageName()+"."+getName();
      Logger.error(msg, ex);
      throw new IllegalArgumentException(msg);
    }
  }
}
