/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.generator;

import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

import java.io.PrintWriter;

public class IteratorMethod extends MethodGenerator {

  private QName memberName;
  private Type childType;

  public IteratorMethod( ClassGenerator parentClass, QName memberName, Type childType) {
    super( parentClass, buildValidMethodName( "", memberName.getName(), "Iter" ) );
    this.memberName = memberName;
    this.childType = childType;
  }

  protected void writeMethod(PrintWriter pw) {
    writeMethodDeclaration( pw );
    pw.println(" {" );
    pw.println( "    return iterator( " + parentClass.buildValidIDConstant( memberName ) + ", null );" );
    pw.println( "  }" );
  }

  protected void writeMethodInterface(PrintWriter pw) {
    writeMethodDeclaration( pw );
    pw.println(";");
  }

  private void writeMethodDeclaration( PrintWriter pw ) {
    pw.println( "  /**" );
    pw.println( "   * Returns the " + memberName.getName() + " iterator." );
    parentClass.writeMemberDocumentation( pw, memberName );
    pw.println( "   *" );
    pw.println( "   * @return the " + memberName.getName() + " iterator" );
    parentClass.writeMemberDeprecated( pw, memberName );
    pw.println( "   */" );
    parentClass.writeMemberAnnotations( pw, memberName );
    if (childType instanceof SimpleType) {
      pw.print( "  public ModelIterator<SimpleModel> " + getName() + "()" );
    }
    else {
      String classForType = getClassForType( childType, parentClass.getPackageName() );
      pw.print( "  public ModelIterator<" + classForType + "> " + getName() + "()" );
    }
    if (hasThrowException()) {
      pw.print( " throws " + getThrowException() );
    }
  }
}
