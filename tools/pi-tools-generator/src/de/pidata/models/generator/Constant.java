/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.generator;

import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

public class Constant {

  private final QName constName;
  private final Type type;
  private final Object value;

  public Constant( QName constName, Type type, Object value ) {
    this.constName = constName;
    this.type = type;
    this.value = value;
  }

  public QName getConstName() {
    return constName;
  }

  public Type getType() {
    return type;
  }

  public Object getValue() {
    return value;
  }
}
