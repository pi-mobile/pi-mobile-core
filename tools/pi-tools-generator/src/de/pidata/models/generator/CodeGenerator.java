/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.generator;

import de.pidata.log.Logger;
import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.simple.*;
import de.pidata.models.xml.schema.ExplicitGroup;
import de.pidata.models.xml.schema.LocalElement;
import de.pidata.models.xml.schema.SimpleTypeDef;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.string.Helper;
import de.pidata.tools.CodeGenFactory;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Code generator. Provides basic functionality and features to add imports as well
 * as Namespace and string id handling.
 */
public class CodeGenerator {

  private static final boolean DEBUG = false;

  public static final Namespace NS_SCHEMA = Namespace.getInstance("http://www.w3.org/2001/XMLSchema");

  public static final int PUBLIC = 0;
  public static final int NONE = 1;
  public static final int PROTECTED = 2;

  protected ClassGenerator parentClass;
  private String  name;
  private String  header;

  private boolean isAbstract;
  private boolean isInterface;
  private int     visibility;

  /** holds all classnames which are present in the internal packages (de.pidata.models.tree, de.pidata.models.types, de.pidata.qnames) */
  protected static Set<String> internalClassNames;

  /** holds all methodnames which are present in the internal packages (de.pidata.models.tree, de.pidata.models.types, de.pidata.qnames) */
  protected static Set<String> internalMethodNames;

  /** hold all type ids to avoid collision, e.g. case sensitivity */
  protected static Map<String, String> typeIdsMap = new HashMap<>();

  /** hold all member names to avoid collision, e.g. case sensitivity */
  protected static Map<String, String> membersMap = new HashMap<>();

  protected HashMap<String,QName> idTable = new HashMap<String, QName>();

  /**
   * Create generator.
   * @param name name of code unit; IMPORTANT: valid, non-colliding must be ensured before!
   */
  public CodeGenerator( ClassGenerator parentClass, String name ) {
    this.parentClass = parentClass;
    this.name = name;
    header = "";
    isAbstract = false;
    isInterface = false;
    visibility = PUBLIC;
  }

  public boolean collidesWithInternalClass() {
    ensureInternalClassNamesInitialized();
    return internalClassNames.contains( getName() );
  }

  public static boolean collidesWithInternalClass( String className ) {
    ensureInternalClassNamesInitialized();
    return internalClassNames.contains( className );
  }

  public static boolean collidesWithInternalMethod( String methodName ) {
    ensureInternalClassNamesInitialized();
    return internalMethodNames.contains( methodName );
  }

  private static void ensureInternalClassNamesInitialized() {
    if (internalClassNames == null) {
      internalClassNames = new HashSet<>();
      getClassesForPackage( "de.pidata.models.tree" );
      getClassesForPackage( "de.pidata.models.types" );
      getClassesForPackage( "de.pidata.qnames" );

      if (DEBUG) {
        Logger.info( "handle internal classes:" );
        for (String className : internalClassNames) {
          Logger.info( className );
        }
      }

      // analyze common base class
      internalMethodNames = new HashSet<>();
      for (Method declaredMethod : SequenceModel.class.getMethods()) {
        internalMethodNames.add( declaredMethod.getName() );
      }
    }
  }

  /**
   *
   * @implNote inspired from https://stackoverflow.com/questions/10910510/get-a-array-of-class-files-inside-a-package-in-java
   * @param packageName
   */
  private static void getClassesForPackage( String packageName) {
    // get a File object for the package
    File directory = null;
    String relPath = packageName.replace( '.', '/' );
    URL resource = ClassLoader.getSystemClassLoader().getResource( relPath );
    if (resource == null) {
      throw new RuntimeException( "No resource for " + relPath );
    }

    String fullPath = resource.getFile();
    try {
      directory = new File( resource.toURI() );
    }
    catch (URISyntaxException e) {
      throw new RuntimeException( packageName + " (" + resource + ") does not appear to be a valid URL / URI.  Strange, since we got it from the system...", e );
    }
    catch (IllegalArgumentException e) {
      // if executed from within a jar no directory can be detected
      directory = null;
    }

    // find classes either in directory or in JAR
    if (directory != null && directory.exists()) {
      addClassesFromDirectory( directory );
    }
    else {
      addClassesFromJar( packageName, relPath,fullPath );
    }
  }

  /**
   * Finds the classes contained in the JAR matching the given package name and adds their names to the internal class names.
   * @param packageName the package to examine
   * @param relPath relative path identifying the package
   * @param fullPath resource path identifying the package within the JAR
   */
  private static void addClassesFromJar(String packageName, String relPath, String fullPath ) {
    try {
      String jarPath = fullPath.replaceFirst( "[.]jar[!].*", ".jar" ).replaceFirst( "file:", "" );
      JarFile jarFile = new JarFile( jarPath );
      Enumeration<JarEntry> entries = jarFile.entries();
      while (entries.hasMoreElements()) {
        JarEntry entry = entries.nextElement();
        String entryName = entry.getName();
        if (entryName.startsWith( relPath ) && entryName.length() > (relPath.length() + "/".length()) && entryName.endsWith( ".class" )) {
          String className = entryName.substring( entryName.lastIndexOf( "/" ) + 1, entryName.length() - 6 );
          internalClassNames.add( className );
        }
      }
    }
    catch (IOException e) {
      throw new RuntimeException( packageName + " does not appear to be a valid package", e );
    }
  }

  /**
   * Finds the class files contained in the directory and adds their names to the internal class names.
   * @param directory the directory to search
   */
  private static void addClassesFromDirectory( File directory ) {
    File[] files = directory.listFiles();
    for (int i = 0; i < files.length; i++) {
      // we are only interested in .class files
      String fileName = files[i].getName();
      if (fileName.endsWith( ".class" )) {
        // removes the .class extension
        String className = fileName.substring( 0, fileName.length() - 6 );
        internalClassNames.add( className );
      }
      else if (files[i].isDirectory()) {
        addClassesFromDirectory( files[i] );
      }
    }
  }

  /**
   * Gets the minOccurs value. If the childRel is part of a group, adjusts the value accordingly:
   * the smaller of the two is taken.
   *
   * @param childRel
   * @return
   */
  protected int getEffectiveMinOccurs( Relation childRel ) {
    int minOccurs = childRel.getMinOccurs();
    if (childRel instanceof LocalElement) {
      Model parent = ((LocalElement) childRel).getParent( false );
      if (parent instanceof ExplicitGroup) {
        int groupMinOccurs = ((ExplicitGroup) parent).minOccurs();
        if(groupMinOccurs == 0) {
          minOccurs = 0;
        }
      }
    }
    return minOccurs;
  }

  /**
   * Gets the maxOccurs value. If the childRel is part of a group, adjusts the value accordingly:
   * the larger of the two is taken.
   *
   * @param childRel
   * @return
   */
  protected int getEffectiveMaxOccurs( Relation childRel ) {
    int maxOccurs = childRel.getMaxOccurs();
    if (childRel instanceof LocalElement) {
      Model parent = ((LocalElement) childRel).getParent( false );
      if (parent instanceof ExplicitGroup) {
        int groupMaxOccurs = ((ExplicitGroup) parent).maxOccurs();
        if(groupMaxOccurs > maxOccurs) {
          maxOccurs = groupMaxOccurs;
        }
      }
    }
    return maxOccurs;
  }

  /**
   * Set visibility of class. Must be one of <code>PUBLIC</code>, <code>PROTECTED</code> or <code>NONE</code>.
   * @param visibility
   */
  public void setVisibility( int visibility ) {
    this.visibility = visibility;
  }

  /**
   * Access class visibility.
   * @return
   */
  public int getVisibility() {
    return this.visibility;
  }

  /**
   * Declare class to be abstract.
   * @param isAbstract
   */
  public void setAbstract( boolean isAbstract ) {
    this.isAbstract = isAbstract;
  }

  /**
   * Check if class has been declared abstract.
   * @return
   */
  public boolean isAbstract() {
    return isAbstract;
  }

  /**
   * Declare type to be an interface.
   * @param isInterface
   */
  public void setInterface(boolean isInterface) {
    this.isInterface = isInterface;
  }

  /**
   * Check if type has been declared to be an interface.
   * @return
   */
  public boolean isInterface() {
    return isInterface;
  }

  /**
   * Access File header.
   * @return
   */
  public String getHeader() {
    return header;
  }

  /**
   * Set file header.
   * @param header
   */
  public void setHeader( String header ) {
    this.header = header;
  }

  /**
   * Access class name (without package).
   * @return
   */
  public String getName() {
    return this.name;
  }

  /**
   * Add an import declaration. Duplicates are ignored.
   * @param name Name of import declaration
   */
  public void addImport( String name ) {
    this.parentClass.addImport(name);
  }

  /**
   * Builds a valid identifier from the given name. Replaces invalid characters with an underscore.
   * If the given name starts with a number precedes it with an underscore too.
   * @param name
   * @return the valid identifier build from the given name
   */
  public static String convertToValidName( String name ) {
    StringBuilder result = new StringBuilder();
    for (int i = 0; i < name.length(); i++) {
      char ch = name.charAt( i );
      if ((ch >= '0') && (ch <= '9')) {
        if (i == 0) {
          result.append( "_" );
        }
        result.append( ch );
      }
      else if ((ch >= 'A') && (ch <= 'Z')) {
        result.append( ch );
      }
      else if ((ch >= 'a') && (ch <= 'z')) {
        result.append( ch );
      }
      else {
        result.append( '_' );
      }
    }
    return result.toString();
  }

  /**
   * the following memberNames will cause invalid getter methods
   * colliding with parent classes' getter methods
   * or invalid enumeration entries colliding with reserved words
   */
  protected static final ArrayList<String> reservedWords = new ArrayList<String>( Arrays.asList(
      "Object",
      "object",
      "Class",
      "class",
      "Module",
      "module",
      "Factory",
      "Parent",
      "Type",
      "import",
      "package",
      "default",
      "protected",
      "public",
      "private",
      "static",
      "Key",
      "abstract",
      "void",
      "boolean",
      "char",
      "int",
      "long",
      "byte",
      "short",
      "float",
      "double",
      "if",
      "then",
      "else",
      "do",
      "for",
      "return"
  ) );

  protected static boolean collidesWithReserved( String memberName ) {
    return (reservedWords.contains( memberName ));
  }

  public static String lowerCaseFirst( String value ) {
    return value.substring( 0, 1 ).toLowerCase() + value.substring( 1 );
  }

  public static String upperCaseFirst( String value ) {
    return value.substring( 0, 1 ).toUpperCase() + value.substring( 1 );
  }

  /**
   * Get an identifier to be used for that Namespace during code generation. For each namespace
   * a public static field will be created with this identifier.
   * @param namespace
   * @return
   */
  protected String getNamespaceIdentifier( Namespace namespace ) {
    return parentClass.getNamespaceIdentifier(namespace);
  }

  /**
   * Use this to get identical naming for type class.
   *
   * Requirements:
   * <ul>
   *   <li>the type name must be converted to a valid name with only allowed characters, see {@link #convertToValidName(String)}</li>
   *   <li>the type name's first character is changed to upper case; the resulting name must be mapped if a similar member exists where the name only differs in upper/lower case, see {@link #getMappedMemberName(String)}</li>
   * </ul>
   *
   * @param typeName
   * @return
   */
  protected static String buildValidTypeClassName( String typeName ) {
    String className = convertToValidName( typeName );
    // avoid collision with internal constants
    if (className.endsWith( "_TYPE" )) {
      className = className + "_";
    }
    if (collidesWithReserved(typeName)) {
      className = className + "_";
    }
    className = getMappedTypeName( className );
    className = ClassGenerator.upperCaseFirst( className );
    return className;
  }

  protected static String buildValidTypeClassName( QName typeName ) {
    return buildValidTypeClassName( typeName.getName() );
  }

  /**
   * Find the attributes type within its foreign factory.
   *
   * @param attributeType
   * @return
   */
  protected String getForeignFactoryTypeName( Type attributeType ) {
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( attributeType.name().getNamespace() );
    Type foreignType = factory.getType( attributeType.name() );
    String factoryType = null;
    Field[] declaredFields = factory.getClass().getDeclaredFields();
    for (Field field : declaredFields) {
      try {
        Object fieldValue = field.get( factory );
        if (foreignType == fieldValue) {
          factoryType = factory.getClass().getName() + "." + field.getName();
          break;
        }
      }
      catch (IllegalAccessException e) {
        throw new IllegalArgumentException( "attribute type not declared in foreign Factory: " + attributeType );
      }
    }
    return factoryType;
  }

  /**
   * Generate code appropriate for representing this QName at runtime.
   * @param id
   * @param pw
   */
  protected void writeQName( QName id, PrintWriter pw ) {
    pw.print( getNamespaceIdentifier( id.getNamespace() ) + ".getQName( \"" + id.getName() + "\" )" );
  }

  /**
   * Extract the class name out of a fully qualified classname. I.e "java.lang.String" --> "String"
   * @param fqcn
   * @return
   */
  protected String getClassPart( String fqcn ) {
    int idx = fqcn.lastIndexOf( '.' );
    if (idx > 0) {
      return fqcn.substring( idx+1 );
    }

    return fqcn;
  }

  /**
   * Extract package name out of a fully qualified classname. I.e. "java.lang.String" --> "java.lang"
   * @param fqcn
   * @return
   */
  protected String getPackagePart( String fqcn ) {
    int idx = fqcn.lastIndexOf( '.' );
    if (idx > 0) {
      return fqcn.substring( 0, idx );
    }

    return "";
  }

  /**
   * Get collision free type identifier. Avoids problem from e.g. case sensitive type names.
   * Returns a modified identifier build from the given typeName if there already exists an equally named type
   * in the current schema. Adds an underscore to the name if it already exists in the map.
   *
   * @param typeName
   * @return
   */
  protected static String getMappedTypeName( String typeName ) {
    String nonCollidingTypeName = typeName;
    if (typeIdsMap.containsKey( typeName )) {
      return typeIdsMap.get( typeName );
    }
    // map case sensitive ids
    nonCollidingTypeName = getNonCollidingName( typeName, typeIdsMap );
    typeIdsMap.put( typeName, nonCollidingTypeName );
    return nonCollidingTypeName;
  }

  /**
   * Get collision free member names. Avoids problem from e.g. case sensitive member names.
   * Returns a modified identifier build from the given name if there already exists an equally named member
   * in the current schema. Adds an underscore to the name if it already exists in the map.
   *
   * @param memberName
   * @return
   */
  protected static String getMappedMemberName( String memberName ) {
    String nonCollidingMemberName = memberName;
    if (membersMap.containsKey( memberName )) {
      return membersMap.get( memberName );
    }
    // map case sensitive ids
    nonCollidingMemberName = getNonCollidingName( memberName, membersMap );
    membersMap.put( memberName, nonCollidingMemberName );
    return nonCollidingMemberName;
  }

  /**
   * Iterate through known mapped names to find collisions.
   *
   * @param typeName
   * @param namesMap
   * @return
   */
  private static String getNonCollidingName( String typeName, Map<String, String> namesMap ) {
    String nonCollidingMemberName = typeName;
    for (String mappedId : namesMap.keySet()) {
      if (mappedId.equalsIgnoreCase( typeName )) {
        nonCollidingMemberName = typeName.substring( 0, 1 ) + "_" + typeName.substring( 1 );
        // check if modified name already exists
        nonCollidingMemberName = getNonCollidingName( nonCollidingMemberName, namesMap );
      }
    }
    return nonCollidingMemberName;
  }

  /**
   * Returns the string identifying the Java class associated with the given {@link Type}.
   * Takes into account the namespace/package. Used to generate Java method/variable/field declarations.
   * @param type
   * @return
   */
  public String getClassForType( Type type, String packageName ) {
    if (type == null) {
      throw new IllegalArgumentException( "Type must not be null" );
    }
    try {
      Class typeClass = type.getValueClass();
      QName typeName = type.name();
      if (typeName == null) {
        return null;
      }
      if ((typeClass == null) || (typeName.getNamespace() == parentClass.getTargetNamespace())) { // TODO???
        String classPart;
        if (type instanceof SimpleType) {
          if (type instanceof SimpleTypeDef && ((SimpleTypeDef) type).isEnumDef()) {
            // enums have their own implementation which is not derived from its base class
            classPart = buildValidTypeClassName( type.name() );
          }
          else {
            if (typeClass != null) {
              classPart = getClassPart( typeClass.getName() );
            }
            else {
              throw new IllegalArgumentException( "no typeClass found" );
            }
          }
        }
        else {
          classPart = buildValidTypeClassName( type.name() );
        }
        if (collidesWithInternalClass( classPart )) {
          return packageName + "." + classPart;
        }
        else {
          return classPart;
        }
      }
      else {
        String typeClassName = typeClass.getName();
        String packagePart = getPackagePart( typeClassName );
        String classPart = getClassPart( typeClassName );
        if ("java.lang".equals( packagePart )) {
          // No package prefix and no import need for java.lang.*
          return classPart;
        }
        else if (parentClass.containsImport( typeClassName )) {
          // full type already known so just use short class name
          return classPart;
        }
        else {
          if (collidesWithInternalClass( classPart )) {
            // Use name with package for class names already used internally by PI-Mobile, e.g. "Type" or "Model"
            return typeClassName;
          }
          else {
            // Use import and short class name if no collision with internal class names
            parentClass.addImport( typeClassName );
            return classPart;
          }
        }
      }
    }
    catch (Exception ex) {
      String msg = "Could not get class name for type="+type.name(); 
      Logger.error(msg, ex);
      throw new IllegalArgumentException(msg);
    }
  }

  /**
   * Creates the String representing the given {@link Type} defined in the current schema as constant.
   *
   * @param type
   * @return
   */
  protected String buildValidTypeConstant( Type type ) {
    return buildValidTypeConstant( type, null );
  }

  /**
   * Creates the String representing the given {@link Type} (without REF) defined in the current schema as constant.
   *
   * @param type
   * @return
   */
  protected String buildValidTypeConstantWithoutREF( Type type ) {
    return buildValidTypeConstant( type, "REF" );
  }

  /**
   * Creates the String representing the given  {@link Type} defined in the current schema as constant.
   * Optionally removes the REF at the end
   *
   * @param type
   * @return
   */
  protected String buildValidTypeConstant( Type type, String suffixToRemove ) {
    String constant = convertToValidName( type.name().getName() );
    constant = getMappedTypeName( constant );
    constant = constant.toUpperCase();
    if (suffixToRemove != null && constant.endsWith( suffixToRemove )) {
      constant = constant.substring( 0, constant.length() - suffixToRemove.length() );
    }
    if (type instanceof ComplexType) {
      constant = constant + "_TYPE";
    }
    else if (constant.endsWith( "_TYPE" )) {
      // avoid collision with internal constants
      constant = constant + "_";
    }
    return constant;
  }

  /**
   * Used for root elements and attributes as well as for type relations and attributes.
   *
   * @param memberName
   * @return
   */
  public String buildValidIDConstant( QName memberName ) {
    if (memberName == null) {
      return "null";
    }
    String constant = convertToValidName( memberName.getName() );
    constant = "ID_" + constant.toUpperCase();
    this.idTable.put( constant, memberName );
    return constant;
  }

  /**
   *
   * @param memberName
   * @param collPostfix
   * @return
   */
  public String buildValidCollectionName( QName memberName, String collPostfix ) {
    String constant = convertToValidName( memberName.getName() );
    constant = constant + collPostfix;
    if (collidesWithReserved( constant )) {
      constant = constant + "_";
    }
    return constant;
  }

  protected String getCollectionPostfix( Class collection ) {
    String collPostfix;
    if (collection == java.util.Collection.class) {
      collPostfix = "s";
    }
    else if (collection == java.util.Set.class) {
      collPostfix = "Set";
    }
    else if (collection == java.util.List.class) {
      collPostfix = "List";
    }
    else {
      throw new IllegalArgumentException( "Unsupported Collection interface=" + collection.getName() );
    }
    return collPostfix;
  }

  protected Class getCollectionImpl( Class collection ) {
    Class collectionImpl;
    if (collection == java.util.Collection.class) {
      collectionImpl = ModelCollection.class;
    }
    else if (collection == java.util.Set.class) {
      collectionImpl = ModelSet.class;
    }
    else if (collection == java.util.List.class) {
      collectionImpl = ModelList.class;
    }
    else {
      throw new IllegalArgumentException( "Unsupported Collection interface=" + collection.getName() );
    }
    return collectionImpl;
  }

  /**
   * Returns a string representing the {@link SimpleType} to be used in the factory. If the given type is
   * a basic type such as {@link StringType} returns its default instance. If the given type is not a basic type returns
   * a string to create a new instance of it, see {@link #createSimpleType(SimpleTypeDef)}.
   * @param attributeType
   * @return
   */
  public String getSimpleTypeDefinition( SimpleType attributeType, Namespace targetNamespace, String factoryClassName ) {
    if (attributeType instanceof BooleanType) {
      addImport( BooleanType.class.getName() );
      return "BooleanType.getDefault()";
    }
    else if (attributeType instanceof IntegerType) {
      addImport( IntegerType.class.getName() );
      QName typeID = attributeType.name();
      if (typeID == IntegerType.TYPE_INT) {
        return "IntegerType.getDefInt()";
      }
      else if (typeID == IntegerType.TYPE_LONG) {
        return "IntegerType.getDefLong()";
      }
      else if (typeID == IntegerType.TYPE_SHORT){
        return "IntegerType.getDefShort()";
      }
      else if (typeID == IntegerType.TYPE_BYTE) {
        return "IntegerType.getDefByte()";
      }
      else if (typeID == IntegerType.TYPE_UNSIGNED_LONG) {
        return "IntegerType.getDefUnsignedLong()";
      }
      else if (typeID == IntegerType.TYPE_UNSIGNED_INT) {
        return "IntegerType.getDefUnsignedInt()";
      }
      else if (typeID == IntegerType.TYPE_UNSIGNED_SHORT) {
        return "IntegerType.getDefUnsignedShort()";
      }
      else if (typeID == IntegerType.TYPE_UNSIGNED_BYTE) {
        return "IntegerType.getDefUnsignedByte()";
      }
      else {
        return "IntegerType.getDefInt()";
      }
    }
    else if (attributeType instanceof DateTimeType) {
      DateTimeType dateType = (DateTimeType) attributeType;
      addImport( DateTimeType.class.getName() );
      addImport( DateObject.class.getName() );
      QName type = ((DateTimeType) attributeType).getType();
      if (type == DateTimeType.TYPE_DATE) {
        return "DateTimeType.getDefDate()";
      }
      else if (type == DateTimeType.TYPE_DATETIME) {
        return "DateTimeType.getDefDateTime()";
      }
      else if (type == DateTimeType.TYPE_TIME) {
        return "DateTimeType.getDefTime()";
      }
    }
    else if (attributeType instanceof QNameType) {
      addImport( QNameType.class.getName() );
      return "QNameType.getInstance()";
    }
    else if (attributeType instanceof StringType) {
      addImport( StringType.class.getName() );
      return "StringType.getDefString()";
    }
    else if (attributeType instanceof IntegerType) {
      addImport( IntegerType.class.getName() );
      return "IntegerType.getDefInteger()";
    }
    else if (attributeType instanceof DecimalType) {
      addImport( DecimalType.class.getName() );
      addImport( DecimalObject.class.getName() );
      return "DecimalType.getDefault()";
    }
    else if (attributeType instanceof BinaryType) {
      addImport( BinaryType.class.getName() );
      return "BinaryType.base64Binary";
    }
    else if (attributeType instanceof SimpleTypeDef) {
      if (((SimpleTypeDef) attributeType).isEnumDef()) {
        // handle enum
        String implementation = (String) ((SimpleTypeDef) attributeType).get( CodeGenFactory.ID_IMPLEMENTATION );
        if (!Helper.isNullOrEmpty( implementation )) {
          addImport( implementation );
          return implementation;
        }
        else {
          QName typeID = attributeType.name();
          if (typeID.getNamespace() == targetNamespace) {
            return buildValidTypeConstant( attributeType );
          }
          else {
            String typeClassName = getForeignFactoryTypeName( attributeType );
            addImport( typeClassName );
            return typeClassName;
          }
        }
      }
      else {
        return createSimpleType( (SimpleTypeDef) attributeType );
      }
    }

    if ((attributeType == null) || (attributeType.getClass() == null)) {
      throw new IllegalArgumentException("Invalid type for attribute type="+attributeType);
    }
    else {
      Logger.warn("Unsupported SimpleType: " + attributeType.getClass().getName());
      return attributeType.getClass().getName() + ".getInstance()";
    }
  }

  /**
   * Returns a string which will be used in the factory to create a new instance of the given {@link SimpleType}.
   * @param type
   * @return
   */
  private String createSimpleType( SimpleTypeDef type) {  // TODO: almost duplicate in FactoryClassGenerator?
    QName typeID = type.name();
    String baseTypeName = null;
    String baseTypeDef = "null";
    String params = null;
    Type baseType = type.getBaseType();
    int min, max, totalDigits, fractionDigits;
    boolean minIncl = true;
    boolean maxIncl = true;
    Restriction restriction;
    String temp;

    while (baseType instanceof SimpleTypeDef) {
      baseType = ((SimpleTypeDef) baseType).getBaseType();
    }
    if (baseType instanceof QNameType) {
      addImport( QNameType.class.getName() );
      baseTypeName = "QNameType";
      baseTypeDef = "QNameType.getQName()";
      restriction = type.getRestriction();
      min = restriction.minLength();
      if (min < 0) min = 0;
      max = restriction.maxLength();
      if (max < 0) max = Integer.MAX_VALUE;
      if (max == Integer.MAX_VALUE) {
        params = Integer.toString( min ) + ", Integer.MAX_VALUE";
      }
      else {
        params = Integer.toString( min ) + ", " + Integer.toString( max );
      }
    }
    else if (baseType instanceof IntegerType) {
      min = Integer.MIN_VALUE;
      max = Integer.MAX_VALUE;
      minIncl = true;
      maxIncl = true;
      addImport( IntegerType.class.getName() );
      baseTypeName = "IntegerType";
      baseTypeDef = "IntegerType.getDefInteger()";
      restriction = type.getRestriction();
      temp = restriction.minExclusive();
      if (temp != null) {
        min = Integer.parseInt(temp);
        minIncl = false;
      }
      else {
        temp = restriction.minInclusive();
        if (temp != null) {
          min = Integer.parseInt(temp);
        }
      }
      temp = restriction.maxExclusive();
      if (temp != null) {
        max = Integer.parseInt(temp);
        maxIncl = false;
      }
      else {
        temp = restriction.maxInclusive();
        if (temp != null) {
          max = Integer.parseInt(temp);
        }
      }
      if (max == Integer.MAX_VALUE) {
        params = "Integer.MAX_VALUE, " + Integer.toString( min ) + ", "
            + Boolean.toString( maxIncl ) + ", " + Boolean.toString( minIncl );
      }
      else {
        params = Integer.toString( max ) + ", " + Integer.toString( min ) + ", "
            + Boolean.toString( maxIncl ) + ", " + Boolean.toString( minIncl );
      }
    }
    else if (baseType instanceof DecimalType) {
      addImport( DecimalType.class.getName() );
      addImport( DecimalObject.class.getName() );
      baseTypeName = "DecimalType";
      baseTypeDef = "DecimalType.getDefault()";
      restriction = type.getRestriction();
      totalDigits = restriction.totalDigits();
      fractionDigits = restriction.fractionDigits();
      params = Integer.toString(totalDigits) + ", " + Integer.toString(fractionDigits);
    }
    else if (baseType instanceof StringType) {
      addImport( StringType.class.getName() );
      baseTypeName = "StringType";
      baseTypeDef = "StringType.getDefString()";
      restriction = type.getRestriction();
      min = restriction.minLength();
      if (min < 0) min = 0;
      max = restriction.maxLength();
      if (max < 0) max = Integer.MAX_VALUE;
      if (max == Integer.MAX_VALUE) {
        params = Integer.toString( min ) + ", Integer.MAX_VALUE";
      }
      else {
        params = Integer.toString( min ) + ", " + Integer.toString( max );
      }
    }
    //TODO weitere restrictions implementieren

    if (baseTypeName == null) {
      throw new IllegalArgumentException("Unsupported simple type ID="+typeID+", baseType ID="+baseType.name());
    }

    return "new " + baseTypeName + "( NAMESPACE.getQName(\""+ typeID.getName() + "\"), "+ baseTypeDef +", " + params + " )";
  }

  public String getClassName( ComplexType type, String packageName ) {
    return packageName + "." + getName();
  }

}

