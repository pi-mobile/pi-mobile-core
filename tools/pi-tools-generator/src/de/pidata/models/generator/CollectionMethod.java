/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.generator;

import de.pidata.qnames.QName;

import java.io.PrintWriter;

public class CollectionMethod extends MethodGenerator {

  private QName memberName;
  private String typeClassName;
  private String collPostfix;

  public CollectionMethod( ClassGenerator parentClass, QName memberName, String typeClassName, String methodPostfix ) {
    super( parentClass, buildValidMethodName( "get", memberName.getName(), methodPostfix ) );
    this.memberName = memberName;
    this.typeClassName = typeClassName;
    this.collPostfix = methodPostfix;
  }

  protected void writeMethod( PrintWriter pw ) {
    writeMethodDeclaration( pw, typeClassName );
    pw.println( " {" );
    pw.println( "    return " + buildValidCollectionName( memberName, collPostfix ) + ";" );
    pw.println( "  }" );
  }

  protected void writeMethodInterface( PrintWriter pw ) {
    writeMethodDeclaration( pw, typeClassName );
    pw.println( ";" );
  }

  private void writeMethodDeclaration( PrintWriter pw, String typeClassName ) {
    pw.println( "  /**" );
    pw.println( "   * Returns the " + memberName.getName() + " collection." );
    parentClass.writeMemberDocumentation( pw, memberName );
    pw.println( "   *" );
    pw.println( "   * @return the " + memberName.getName() + " collection" );
    parentClass.writeMemberDeprecated( pw, memberName );
    pw.println( "   */" );
    parentClass.writeMemberAnnotations( pw, memberName );
    pw.print( "  public " + typeClassName + " " + getName() + "()" );
    if (hasThrowException()) {
      pw.print( " throws " + getThrowException() );
    }
  }
}
