/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.generator;

import de.pidata.models.tree.SimpleModel;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

import java.io.PrintWriter;

public class RemoveMethod extends MethodGenerator {

  private QName memberName;
  private Type   type;

  public RemoveMethod( ClassGenerator parentClass, QName memberName, Type type) {
    super(parentClass, buildValidMethodName( "remove", memberName.getName(), "" ) );
    this.memberName = memberName;
    this.type = type;
  }

  protected void writeMethod(PrintWriter pw) {
    String classForType;
    classForType = getClassForType( type, parentClass.getPackageName() );
    String variable = buildValidVariableName( memberName );
    writeMethodDeclaration(pw, variable, classForType);
    pw.println(" {");
    pw.print( "    remove( " );
    pw.println( parentClass.buildValidIDConstant( memberName ) + ", " + variable + " );" );
    pw.println( "  }" );
  }

  protected void writeMethodInterface(PrintWriter pw) {
    String classForType;
    classForType = getClassForType( type, parentClass.getPackageName() );
    String variable = buildValidVariableName( memberName );
    writeMethodDeclaration(pw, variable, classForType);
    pw.println(";");
  }

  private void writeMethodDeclaration(PrintWriter pw, String variable, String classForType) {
    pw.println( "  /**" );
    pw.println( "   * Removes the " + memberName.getName() + " element." );
    parentClass.writeMemberDocumentation( pw, memberName );
    pw.println( "   *" );
    pw.println( "   * @param " + variable + " the " + memberName.getName() + " element to remove" );
    parentClass.writeMemberDeprecated( pw, memberName );
    pw.println( "   */" );
    parentClass.writeMemberAnnotations( pw, memberName );
    if (type instanceof SimpleType) {
      addImport(SimpleModel.class.getName());
      pw.print( "  public void " + getName() + "( SimpleModel " + variable + " )");
    }
    else {
      pw.print( "  public void " + getName() + "( " + classForType + " " + variable + " )");
    }
    if (hasThrowException()) {
      pw.print(" throws " + getThrowException());
    }
  }
}

