/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.generator;

import de.pidata.qnames.Key;
import de.pidata.models.tree.Model;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

import java.io.PrintWriter;

public class GetChildMethod extends MethodGenerator {

  private QName memberName;
  private Type   type;

  public GetChildMethod( ClassGenerator parentClass, QName memberName, Type type ) {
    super(parentClass, buildValidMethodName( "get", memberName.getName(), "" ) );
    this.memberName = memberName;
    this.type = type;
  }

  protected void writeMethod(PrintWriter pw) {
    parentClass.addImport( Key.class.getName() );
    String classForType;
    classForType = getClassForType( type, parentClass.getPackageName() );
    String variable = buildValidVariableName( memberName );
    writeMethodDeclaration( pw, classForType, "Key", variable );
    pw.println( " {" );
    if (type instanceof SimpleType) {
      parentClass.addImport(Model.class.getName());
      pw.println( "    Model m = get( " + parentClass.buildValidIDConstant( memberName ) +", " + variable + " );");
      pw.println( "    if (m == null) return null;");
      pw.println( "    else return (" + classForType + ") m.getContent();" );
    }
    else {
      pw.println( "    return ("+classForType+") get( " + parentClass.buildValidIDConstant( memberName ) + ", " + variable + " );" );
    }
    pw.println( "  }" );
  }

  private void writeMethodDeclaration(PrintWriter pw, String classForType, String variableType, String variable) {
    parentClass.addImport(QName.class.getName());
    pw.println( "  /**" );
    pw.println( "   * Returns the " + memberName.getName() + " element identified by the given key." );
    parentClass.writeMemberDocumentation( pw, memberName );
    pw.println( "   *" );
    pw.println( "   * @return the " + memberName.getName() + " element identified by the given key" );
    parentClass.writeMemberDeprecated( pw, memberName );
    pw.println( "   */" );
    parentClass.writeMemberAnnotations( pw, memberName );
    pw.print( "  public "+classForType+" " + getName() + "( "+variableType+" "+variable+" )");
    if (hasThrowException()) {
      pw.print(" throws " + getThrowException());
    }
  }

  protected void writeMethodInterface(PrintWriter pw) {
    String classForType;
    String variableType;
    classForType = getClassForType( type, parentClass.getPackageName() );
    variableType = "Key";
    String variable = buildValidVariableName( memberName );
    writeMethodDeclaration( pw, classForType, variableType, variable );
    pw.println(";");
  }

  /**
   * Use this to get identically named method non-colliding variables.
   *
   * Requirements:
   * <ul>
   *   <li>the member name must be converted to a valid name with only allowed characters, see {@link #convertToValidName(String)}</li>
   *   <li>the member name's first character is changed to lower case; the resulting name must not be a reserved word, see {@link #collidesWithReserved(String)}</li>
   * </ul>
   *
   * Appends 'ID' to the variable used here.

   * @param memberName
   * @return
   */
  protected String buildValidVariableName( QName memberName  ) {
    String validMemberName = convertToValidName( memberName.getName() );
    validMemberName = lowerCaseFirst( validMemberName );
    validMemberName = validMemberName + "ID";
    if (collidesWithReserved( validMemberName )) {
      validMemberName = "_" + validMemberName;
    }
    return validMemberName;
  }
}
