/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.generator;

import de.pidata.models.types.Type;
import de.pidata.qnames.QName;

import java.io.PrintWriter;

public class GetConstMethod extends MethodGenerator {

  private final QName constName;
  private final Type type;

  public GetConstMethod( ClassGenerator parentClass, QName constName, Type type, Object constValue ) {
    super( parentClass, buildValidMethodName( "get", constName.getName(), "" ) );
    this.constName = constName;
    this.type = type;
  }

  @Override
  protected void writeMethod( PrintWriter pw ) {
    String typeClassName = getClassForType( type, parentClass.getPackageName() );
    writeMethodDeclaration( pw, typeClassName );
    pw.println( " {" );
    pw.println( "    return " + parentClass.buildValidIDConstant( constName ) + ";" );
    pw.println( "  }" );
  }

  @Override
  protected void writeMethodInterface( PrintWriter pw ) {
    throw new IllegalStateException( "Interface not valid for constant " + constName );
  }

  private void writeMethodDeclaration( PrintWriter pw, String typeClassName ) {
    pw.println( "  /**" );
    pw.println( "   * Returns the constant " + constName.getName() + "." );
    pw.println( "   *" );
    pw.println( "   * @return The constant " + constName.getName() );
    pw.println( "   */" );
    pw.print( "  public " + typeClassName + " " + getName() + "()" );
    if (hasThrowException()) {
      pw.print( " throws " + getThrowException() );
    }
  }
}
