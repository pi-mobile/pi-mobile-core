/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.generator;

import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Type;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.string.Helper;

import java.io.PrintWriter;

public abstract class MethodGenerator extends CodeGenerator {

  private String throwException;

  public boolean hasThrowException() {
    return (throwException != null && throwException.length() > 0);
  }

  public String getThrowException() {
    return throwException;
  }

  public void setThrowException(String throwException) {
    this.throwException = throwException;
  }

  public MethodGenerator(ClassGenerator parentClass, String name) {
    super(parentClass, name);
    parentClass.addMethod(this);
  }

  protected abstract void writeMethod(PrintWriter pw);

  protected abstract void writeMethodInterface( PrintWriter pw );

  /**
   * Use fully qualified class identifier if type name exists in internal packages already.
   * @param type
   * @return
   */
  @Override
  public String getClassForType( Type type, String packageName ) {
    String classForType = super.getClassForType( type, packageName ); // TODO: duplicate?
    if (type instanceof ComplexType) {
      Namespace typeNamespace = type.name().getNamespace();
      if ((typeNamespace == parentClass.getTargetNamespace()) && collidesWithInternalClass( classForType )) {
        return parentClass.getPackageName() + "." + classForType;
      }
    }
    return classForType;
  }

  /**
   * Avoid collision with existing - extend as needed.
   *
   * Requirements:
   * <ul>
   *   <li>the member name must be converted to a valid name with only allowed characters, see {@link #convertToValidName(String)}</li>
   *   <li>if the resulting method name collides with globally used methods a '_' will be appended, see {@link #collidesWithInternalMethod(String)}</li>
   * </ul>
   *
   * @param prefix
   * @param memberName
   * @return
   */
  protected static String buildValidMethodName( String prefix, String memberName, String postfix ) {
    String methodName;
    String validMemberName = convertToValidName( memberName );
    if (Helper.isNullOrEmpty( prefix )) {
      methodName = lowerCaseFirst( validMemberName ) + postfix;
    }
    else {
      methodName = prefix + upperCaseFirst( validMemberName ) + postfix;
    }
    if (collidesWithInternalMethod( methodName )) {
      methodName = methodName + "_";
    }
    return methodName;
  }

  /**
   * Use this to get identically named method non-colliding variables.
   *
   * Requirements:
   * <ul>
   *   <li>the member name must be converted to a valid name with only allowed characters, see {@link #convertToValidName(String)}</li>
   *   <li>the member name's first character is changed to lower case; the resulting name must not be a reserved word, see {@link #collidesWithReserved(String)}</li>
   * </ul>

   * @param memberName
   * @return
   */
  protected String buildValidVariableName( QName memberName  ) {
    String validMemberName = convertToValidName( memberName.getName() );
    validMemberName = lowerCaseFirst( validMemberName );
    if (collidesWithReserved( validMemberName )) {
      validMemberName = "_" + validMemberName;
    }
    return validMemberName;
  }
}
