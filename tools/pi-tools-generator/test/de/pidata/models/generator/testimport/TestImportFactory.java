// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.models.generator.testimport;

import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.AnyAttribute;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.*;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.util.Hashtable;

public class TestImportFactory extends de.pidata.models.tree.AbstractModelFactory {

  public static final Namespace NAMESPACE = Namespace.getInstance( "de.pidata.testimport" );

  public static final QName ID_DEMOIMPORTELEMENT = NAMESPACE.getQName("demoImportElement");

  public static final StringType DEMOIMPORTTEXTTYPE = new StringType(NAMESPACE.getQName("demoImportTextType"), 0, Integer.MAX_VALUE);

  public static final ComplexType DEMOIMPORTTYPE_TYPE = new DefaultComplexType( NAMESPACE.getQName( "demoImportType" ), DemoImportType.class.getName(), 0 );
  static {
    DefaultComplexType type;
    type = (DefaultComplexType) DEMOIMPORTTYPE_TYPE;
    type.addAttributeType( DemoImportType.ID_DEMOIMPORTATTR, StringType.getDefString() );

  }
  public TestImportFactory() {
    super( NAMESPACE, "de.pidata.testimport", "3.0" );
    addType( DEMOIMPORTTEXTTYPE );
    addType( DEMOIMPORTTYPE_TYPE );
    addRootRelation( ID_DEMOIMPORTELEMENT, DEMOIMPORTTYPE_TYPE, 1, 1);
  }

  public Model createInstance(Key key, Type typeDef, Object[] attributes, Hashtable anyAttribs, ChildList children) {
    Class modelClass = typeDef.getValueClass();
    if (modelClass == DemoImportType.class) {
      return new DemoImportType(key, attributes, anyAttribs, children);
    }
    return super.createInstance(key, typeDef, attributes, anyAttribs, children);
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
