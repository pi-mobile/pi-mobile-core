// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.models.generator.testgen;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.lang.String;
import java.util.Hashtable;

public class DemoInclude2Type extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "de.pidata.test" );

  public static final QName ID_DEMOINCLUDE2ATTR = NAMESPACE.getQName( "demoInclude2Attr" );

  public DemoInclude2Type() {
    super( null, TestFactory.DEMOINCLUDE2TYPE_TYPE, null, null, null );
  }

  public DemoInclude2Type( Key key, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, TestFactory.DEMOINCLUDE2TYPE_TYPE, attributeNames, anyAttribs, childNames );
  }

  protected DemoInclude2Type( Key key, ComplexType type, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, type, attributeNames, anyAttribs, childNames );
  }

  /**
   * Returns the attribute demoInclude2Attr.
   *
   * @return The attribute demoInclude2Attr
   */
  public String getDemoInclude2Attr() {
    return (String) get( ID_DEMOINCLUDE2ATTR );
  }

  /**
   * Set the attribute demoInclude2Attr.
   *
   * @param demoInclude2Attr new value for attribute demoInclude2Attr
   */
  public void setDemoInclude2Attr( String demoInclude2Attr ) {
    set( ID_DEMOINCLUDE2ATTR, demoInclude2Attr );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
