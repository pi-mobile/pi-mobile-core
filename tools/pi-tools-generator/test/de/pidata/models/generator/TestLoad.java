/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.generator;

import de.pidata.log.Logger;
import de.pidata.models.generator.testgen.DemoMixedType;
import de.pidata.models.generator.testgen.RootContainer;
import de.pidata.models.generator.testgen.TestFactory;
import de.pidata.models.tree.*;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.models.xml.binder.XmlWriter;
import de.pidata.system.desktop.DesktopSystem;

import java.io.FileInputStream;
import java.io.InputStream;

public class TestLoad {

  public static void main( String[] args ) {

    try {
      String testDataFilename = "testdata/testdata.xml";

      Logger.info( "loading  " + testDataFilename );

      ModelFactoryTable.getInstance().getOrSetFactory( BaseFactory.NAMESPACE, BaseFactory.class );
      ModelFactoryTable.getInstance().getOrSetFactory( TestFactory.NAMESPACE, TestFactory.class );

      InputStream inputStream = new FileInputStream( testDataFilename );
      XmlReader xmlReader = new XmlReader();
      Model model = xmlReader.loadData( inputStream, null );
      RootContainer rootModel = (RootContainer) model;

      Logger.info( "...loaded " + testDataFilename );

      for (DemoMixedType mixedChild : rootModel.getMixedElements()) {
        Logger.info( "MIXED CHILD" );
        for (Object contentPart : mixedChild.iterator( null, null )) {
          if (contentPart instanceof SimpleModel) {
            Logger.info( "content:" + ((SimpleModel)contentPart).getContent() );
          }
          else if (contentPart instanceof SequenceModel) {
            Logger.info( "sub:" + ((SequenceModel) contentPart).getParentRelationID() );
          }
          else {
            Logger.info( "unknown part:" + contentPart.getClass().getName() );
          }
        }
      }

      // check roundtrip
      new DesktopSystem();
      XmlWriter xmlWriter = new XmlWriter( null );
      StringBuilder xmlContent = xmlWriter.writeXML( rootModel, true );
      Logger.info( "XML content:\n" + xmlContent );

      //should contain the test data
      Logger.info( "...here we go" );

    }
    catch (Exception ex) {
      Logger.error( "oops", ex );
    }
  }
}
