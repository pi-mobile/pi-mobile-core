/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.models.generator;

import de.pidata.log.Logger;
import de.pidata.models.generator.testgen.IntegerEnum;
import de.pidata.models.generator.testgen.QNameEnum;
import de.pidata.models.generator.testgen.StringEnum;
import de.pidata.models.generator.testgen.TestFactory;
import de.pidata.models.xml.schema.RootSchema;
import de.pidata.models.xml.schema.Schema;
import de.pidata.system.desktop.DesktopSystem;

import java.io.File;

/**
 * Test and demo of enum generation.
 */
public class Schema2ClassTest {

  public Schema2ClassTest() {
    new DesktopSystem();

    // -schema gui.xsd -srcpath ../src -package de.pidata.gui.guidef
    try {
      RootSchema importSchema = Schema2Class.loadSchema( "testimport.xsd" );
      new Schema2Class( importSchema, "de.pidata.models.generator.testimport", null, "../test" );

      //String importFactoryClassName = "de.pidata.models.generator.testimport.TestImportFactory";
      //Class.forName( importFactoryClassName ).newInstance();

      RootSchema testSchema = Schema2Class.loadSchema( "testgen.xsd" );
      new Schema2Class( testSchema, "de.pidata.models.generator.testgen", null, "../test" );
    }
    catch (Exception e) {
      e.printStackTrace();
      System.exit( 1 );
    }
  }

  private void checkResult() {
    StringEnum stringEnum = StringEnum.fromString( "none" );
    Logger.info( stringEnum.toString() );
    stringEnum = StringEnum.none;

    QNameEnum qNameEnum = QNameEnum.fromString( "SINGLE" );
    Logger.info( qNameEnum.getValue().toString() );

    qNameEnum = QNameEnum.fromQName( TestFactory.NAMESPACE.getQName( "MULTIPLE" ) );
    Logger.info( qNameEnum.toString() );

    IntegerEnum integerEnum = IntegerEnum.fromInteger( 5 );
    Logger.info( integerEnum.toString() );

    integerEnum = IntegerEnum.fromString( "7" );
    Logger.info( integerEnum.getValue().toString() );
  }

  public static void main( String[] args ) {
    Schema2ClassTest schema2ClassTest = new Schema2ClassTest();
    schema2ClassTest.checkResult();
  }


}