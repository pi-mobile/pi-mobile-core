package de.pidata.tools.ki.chatgpt;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class ChatGPT {

  private static final String DEFAULT_API_TOKEN = "";


  // LATEST (the most recent gpt version)
  private static final String GPT_LATEST = "gpt-4o";
  private static final String OPENAI_CHAT_URL_LATEST = "https://api.openai.com/v1/chat/completions";

  // GPT MODELS
  private static final String GPT_4o = "gpt-4o";
  private static final String GPT_4 = "gpt-4";

  // URLS
  private static final String OPENAI_CHAT_COMPLETIONS_URL = "https://api.openai.com/v1/chat/completions";

  private String apiToken;
  private String apiUrl;
  private String gptModel;


  /**
   * returns the latest ChatGPT model
   */
  public ChatGPT() {
    apiToken = DEFAULT_API_TOKEN;
    apiUrl = OPENAI_CHAT_URL_LATEST;
    gptModel = GPT_LATEST;
  }

  /**
   * if possible use static getters instead
   *
   * @param apiToken
   * @param apiUrl
   * @param gptModel
   */
  public ChatGPT( String apiToken, String apiUrl, String gptModel ) {
    this.apiToken = apiToken;
    this.apiUrl = apiUrl;
    this.gptModel = gptModel;
  }


  public static ChatGPT getLatest() {
    return new ChatGPT( DEFAULT_API_TOKEN, OPENAI_CHAT_URL_LATEST, GPT_LATEST );
  }

  public static ChatGPT getLatest( String apiToken ) {
    return new ChatGPT( apiToken, OPENAI_CHAT_URL_LATEST, GPT_LATEST );
  }

  public static ChatGPT getChatGPT_v4o() {
    return new ChatGPT( DEFAULT_API_TOKEN, OPENAI_CHAT_COMPLETIONS_URL, GPT_4o );
  }

  public static ChatGPT getChatGPT_v4o( String apiToken ) {
    return new ChatGPT( apiToken, OPENAI_CHAT_COMPLETIONS_URL, GPT_4o );
  }

  public static ChatGPT getChatGPT_v4() {
    return new ChatGPT( DEFAULT_API_TOKEN, OPENAI_CHAT_COMPLETIONS_URL, GPT_4 );
  }

  public static ChatGPT getChatGPT_v4( String apiToken ) {
    return new ChatGPT( apiToken, OPENAI_CHAT_COMPLETIONS_URL, GPT_4 );
  }


  public String ask( String systemPrompt, String contentPrompt ) {


    try {
      HttpClient client = HttpClient.newHttpClient();

      JSONObject requestBody = new JSONObject();
      requestBody.put("model", gptModel);
      requestBody.put("messages", new org.json.JSONArray()
              .put(new JSONObject().put("role", "system").put("content", systemPrompt))
              .put(new JSONObject().put("role", "user").put("content", contentPrompt))
      );



      HttpRequest request = HttpRequest.newBuilder()
              .uri(URI.create(apiUrl))
              .header("Content-Type", "application/json")
              .header("Authorization", "Bearer " + apiToken)
              .POST(HttpRequest.BodyPublishers.ofString(requestBody.toString()))
              .build();

      HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

      System.out.println("ChatGPT response code: ["+response.statusCode()+"]");
      //System.out.println(response.body());

      if(response.statusCode() != 200) {
        System.out.println("ChatGPT response body: ["+response.body()+"]");
        return null;
      }

      JSONObject jsonResponse = new JSONObject(response.body());

      // Get the "choices" array
      JSONArray choicesArray = jsonResponse.getJSONArray("choices");

      // Get the first object in the "choices" array (index 0)
      JSONObject firstChoice = choicesArray.getJSONObject(0);

      // Get the "message" object
      JSONObject messageObject = firstChoice.getJSONObject("message");

      // Get the "content" field from the "message" object
      String content = messageObject.getString("content");

      return content;

    }
    catch (Exception e) {
      throw new RuntimeException( e );
    }
  }

  /**
   * Enriches the given prompt by a prefix, which tells chatGPT how to translate certain words given by the translation table
   * @param prompt
   * @param translationTable
   * @return
   */
  public String ask4Translation( String prompt, HashMap<String, String> translationTable ) {
    return ask( "You are a helpful ai translator", getDictionaryEntriesAsPrompt( translationTable ) + prompt );
  }


  private String extractMessageFromJSONResponse( String response ) {
    int start = response.indexOf( "content" ) + 11;

    int end = response.indexOf( "\"", start );

    return response.substring( start, end );

  }


  private static String getDictionaryEntriesAsPrompt(HashMap<String, String> dictionary) {
    if(dictionary == null) {
      return "";
    } else {

      String promptPart = "In the following you should translate text where some words need special translation, use the following translations if necessary: ";
      for (Map.Entry<String, String> entry : dictionary.entrySet()) {
        String sourceLanguageWord = entry.getKey();
        String targetLanguageWord = entry.getValue();
        promptPart = promptPart + sourceLanguageWord + "-->" + targetLanguageWord + ",";
      }

      promptPart = promptPart.substring(0, promptPart.length() - 1) + ". ";

      return promptPart;
    }
  }


}