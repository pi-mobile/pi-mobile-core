/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.tools.gui;

import de.pidata.gui.guidef.ControllerFactory;
import de.pidata.log.Level;
import de.pidata.log.Logger;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.system.desktop.DesktopSystem;
import de.pidata.tools.gui.util.GuiChecker;
import de.pidata.tools.gui.util.SimpleLogger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CheckGui {

  /**
   * Checks if the given arguments represent valid GUI definition files.
   * Checks if contained GUI definitions use valid bindings.
   *
   * <ul>
   *   <li>if the argument represents a file path: checks the file</li>
   *   <li>if the argument represents a directory: checks all files directly within the directory</li>
   *   <li>if the argument equals '-DEBUG': does more detailed output</li>
   * </ul>
   * @param args
   */
  public static void main( String[] args ) {

    SimpleLogger simpleLogger = new SimpleLogger();
    Logger.setLogger( simpleLogger );

    List<String> listedFiles = new ArrayList<>();
    for (int i = 0; i < args.length; i++) {
      String arg = args[i];
      if ("-DEBUG".equals( arg )) {
        Logger.setLogLevel( Level.DEBUG );
      }
      else {
        listedFiles.add( arg );
      }
    }

    List<String> guiFilesList = new ArrayList<>();
    for (String listedFileName : listedFiles) {
      File listedFile = new File( listedFileName );
      if (listedFile.exists()) {
        if (listedFile.isDirectory()) {
          String[] files = listedFile.list();
          for (int i = 0; i < files.length; i++) {
            guiFilesList.add( listedFileName + "/" + files[i] );
          }
        }
        else {
          guiFilesList.add( listedFileName );
        }
      }
      else {
        Logger.info( "skipped not existing: " + listedFileName );
      }
    }

    if (guiFilesList.size() < 1) {
      System.out.println( "no GUI xml files given" );
      System.exit( 1 );
    }

    // be sure model factories are loaded: e.g. provide a system.properties file
    new DesktopSystem();
    ModelFactoryTable.getInstance().getOrSetFactory( ControllerFactory.NAMESPACE, ControllerFactory.class );

    GuiChecker checkGui = new GuiChecker();
    for (String guiFilename : guiFilesList) {
      try {
        int filenameSepIndex = guiFilename.lastIndexOf( "/" );
        String simpleFilename;
        if (filenameSepIndex >= 0 && filenameSepIndex < guiFilename.length() ) {
          simpleFilename = guiFilename.substring( filenameSepIndex + 1 );
        }
        else {
          simpleFilename = guiFilename;
        }
        Logger.info( "\n===== " + simpleFilename + " =====");
        Logger.debug( "   " + guiFilename );
        checkGui.check( guiFilename );
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
}
