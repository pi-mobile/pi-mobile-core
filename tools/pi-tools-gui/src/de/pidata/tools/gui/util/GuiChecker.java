/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.tools.gui.util;

import de.pidata.gui.guidef.*;
import de.pidata.log.Level;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelFactory;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Type;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;
import de.pidata.string.Helper;
import de.pidata.tools.gui.xpath.XpathValidationException;
import de.pidata.tools.gui.xpath.XpathValidator;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Helper to check GUI definition files prior to using them in complex implementations.
 */
public class GuiChecker {

  private Map<QName, String> dataSourceModelPaths;

  /**
   * Checks one GUI file.
   *
   * @param guiFilename
   * @return
   * @throws Exception
   */
  public boolean check( String guiFilename ) throws Exception {
    boolean result = false;
    Model guiModel = checkLoad( guiFilename );
    if (guiModel != null) {
      result = checkBindingPaths( guiModel );
    }
    return result;
  }

  /**
   * Checks the bindings used within the GUI definition.
   *
   * @param guiModel the model loaded from a GUI file.
   * @return
   */
  public boolean checkBindingPaths( Model guiModel ) {
    boolean success = true;
    dataSourceModelPaths = new HashMap<>();

    QName modelTypeID = (QName) guiModel.get( BindingBaseType.ID_MODELTYPEID );
    if (modelTypeID == null) {
      Logger.info( "no modelTypeID given - no bindings checked" );
    }
    else {
      Logger.info( "modelTypeID: " + modelTypeID.getName() + " [" + modelTypeID.getNamespace() + "]" );
      ModelFactoryTable factoryTable = ModelFactoryTable.getInstance();
      ModelFactory modelFactory = factoryTable.getFactory( modelTypeID.getNamespace() );
      if (modelFactory != null) {
        Type modelType = modelFactory.getType( modelTypeID );
        if (modelType != null) {
          if (modelType instanceof ComplexType) {
            NamespaceTable namespaceTable = guiModel.namespaceTable();
            success = checkElement( guiModel, null, (ComplexType) modelType, namespaceTable, 0 );
            if (success) {
              Logger.info( "OK" );
            }
            else {
              Logger.info( "ERRORS detected" );
            }
          }
          else {
            Logger.error( "modelTypeID is not a ComplexType: " + modelTypeID );
          }
        }
        else {
          Logger.error( "modelTypeID not found in Factory: " + modelTypeID );
        }
      }
      else {
        Logger.error( "cannot find Factory for modelTypeID: " + modelTypeID );
      }
    }
    return success;
  }

  /**
   * Checks one element definition.
   *
   * @param defElement
   * @param parentModelPath
   * @param namespaceTable
   */
  private boolean checkElement( Model defElement, String parentModelPath, ComplexType modelType, NamespaceTable namespaceTable, int level ) {
    boolean success = true;
    String elementInfo = buildElementInfo( defElement, level );
    try {
      if (defElement instanceof GroupCtrlType || defElement instanceof SelectionCtrlType || defElement instanceof TreeCtrlType) {
        success = checkGroup( defElement, parentModelPath, modelType, namespaceTable, level );
      }
      else if (defElement instanceof BindingBaseType) {
        checkBinding( (BindingBaseType) defElement, parentModelPath, modelType, namespaceTable, level );
      }
      else if (defElement instanceof NodeType) {
        Logger.info( elementInfo );
        // TODO: implement node check
        Logger.warn( "TODO: check not yet implemented" );
      }
      else {
        if (defElement.childCount( null ) > 0) {
          Logger.info( elementInfo );
        }
        else {
          Logger.debug( elementInfo + " => no children" );
        }
        // descend into definition parts
        for (Model part : defElement.iterator( null, null )) {
          success = checkElement( part, parentModelPath, modelType, namespaceTable, level + 1 ) && success;
        }
      }
    }
    catch (XpathValidationException ex) {
      success = false;
      ex.printError( namespaceTable );
    }
    return success;
  }

  private String buildElementInfo( Model defElement, int level ) {
    StringBuilder elementInfo = new StringBuilder();
    for (int i = 0; i <= level; i++) {
      elementInfo.append( "  " );
    }
    String parentInfo;
    Model parent = defElement.getParent( false );
    if (parent != null) {
      Object parentID = parent.get( CtrlType.ID_ID );
      parentInfo = parent.getParentRelationID().getName() + ((parentID != null && parentID instanceof QName) ? " [" + ((QName) parentID).getName() + "]" : "");
    }
    else {
      parentInfo = "root";
    }
    String elementID = null;
    Object id = defElement.get( ControllerFactory.NAMESPACE.getQName( "ID" ) );
    if (id == null) {
      id = defElement.get( ControllerFactory.NAMESPACE.getQName( "name" ) );
    }
    if (id != null && id instanceof QName) {
      elementID = ((QName) id).getName();
    }
    if (elementID != null) {
      elementInfo.append( elementID ).append( " " );
    }
    elementInfo.append( "[" ).append( defElement.getParentRelationID().getName() ).append( "]" );
    return elementInfo.toString();
  }

  private String checkBinding( BindingBaseType bindingDef, String parentModelPath, ComplexType modelType, NamespaceTable namespaceTable, int level ) {
    String elementInfo = buildElementInfo( bindingDef, level );
    Logger.info( elementInfo );

    String childModelPath = null;

    // combine/check with dataSource
    QName dataSource = bindingDef.getDataSource();
    if (dataSource != null) {
      Logger.debug( "   dataSource: " + dataSource );
      if (dataSourceModelPaths.containsKey( dataSource )) {
        String dataSourceModelPath = dataSourceModelPaths.get( dataSource );
        if (dataSourceModelPath == null) {
          throw new XpathValidationException( "no dataSource model path available", modelType, dataSource );
        }
        else {
          Logger.debug( "   dataSource model path: " + dataSourceModelPath );
          parentModelPath = dataSourceModelPath;
        }
      }
      else {
        throw new XpathValidationException( "dataSource element missing", modelType, dataSource );
      }
    }

    // check paths
    String modelPath = bindingDef.getPath();
    if (modelPath != null) {
      if (parentModelPath == null) {
        childModelPath = modelPath;
      }
      else {
        childModelPath = parentModelPath + "/" + modelPath;
      }
      Logger.debug( "   path: " + modelPath + " => " + childModelPath );
      checkModelPath( childModelPath, modelType, namespaceTable );
    }

    if (bindingDef instanceof AttrBindingType) {
      QName attrName = ((AttrBindingType) bindingDef).getAttrName();
      if (attrName != null) {
        String attrPath;
        if (childModelPath == null) {
          attrPath = "./@" + attrName.toString( namespaceTable );
        }
        else {
          attrPath = childModelPath + "/@" + attrName.toString( namespaceTable );
        }
        Logger.debug( "   attrName: " + attrName + " => " + attrPath );
        checkModelPath( attrPath, modelType, namespaceTable );
      }
      else {
        Logger.error( "attrName missing" );
      }
    }
    else if (bindingDef instanceof ModelBindingType) {
      QName relationID = ((ModelBindingType) bindingDef).getRelationID();
      if (relationID != null) {
        if (childModelPath == null) {
          childModelPath = relationID.toString( namespaceTable );
        }
        else {
          childModelPath = childModelPath + "/" + relationID.toString( namespaceTable );
        }
        Logger.debug( "   relationID: " + relationID + " => " + childModelPath );
        checkModelPath( childModelPath, modelType, namespaceTable );
      }
    }
    else if (bindingDef instanceof ListBindingType) {
      QName relationID = ((ListBindingType) bindingDef).getRelationID();
      if (relationID != null) {
        if (childModelPath == null) {
          childModelPath = relationID.toString( namespaceTable );
        }
        else {
          childModelPath = childModelPath + "/" + relationID.toString( namespaceTable );
        }
        Logger.debug( "   relationID: " + relationID + " => " + childModelPath );
        checkModelPath( childModelPath, modelType, namespaceTable );

        String displayModelPath = null;
        String displayPath = ((ListBindingType) bindingDef).getDisplayPath();
        if (!Helper.isNullOrEmpty( displayPath )) {
          displayModelPath = childModelPath + "/" + displayPath;
          Logger.debug( "   displayPath: " + displayPath + " => " + displayModelPath );
          checkModelPath( displayModelPath, modelType, namespaceTable );
        }
        else {
          displayModelPath = childModelPath;
        }

        QName displayID = ((ListBindingType) bindingDef).getDisplayID();
        if (displayID != null) {
          String displayAttrPath;
          displayAttrPath = displayModelPath + "/@" + displayID.toString( namespaceTable );
          Logger.debug( "   displayID: " + displayID + " => " + displayAttrPath );
          checkModelPath( displayAttrPath, modelType, namespaceTable );
        }

        QName keyAttrID = ((ListBindingType) bindingDef).getKeyAttrID();
        if (keyAttrID != null) {
          String keyAttrPath;
          keyAttrPath = childModelPath + "/@" + keyAttrID.toString( namespaceTable );
          Logger.debug( "   keyAttrID: " + keyAttrID + " => " + keyAttrPath );
          checkModelPath( keyAttrPath, modelType, namespaceTable );
        }
      }
      else {
        Logger.warn( "relationID missing" );
        if (parentModelPath == null) {
          childModelPath = "./*";
        }
        else {
          childModelPath = parentModelPath + "/*";
        }
        Logger.debug( "   all relations: => " + childModelPath );
      }
    }
    else {
      Logger.warn( "not yet implemented for " + bindingDef );
    }
    return childModelPath;
  }

  /**
   * Needs to check group model binding prior to other definition parts.
   *
   * @param defElement
   * @param parentModelPath
   * @param modelType
   * @param namespaceTable
   * @param level for debug output only
   */
  private boolean checkGroup( Model defElement, String parentModelPath, ComplexType modelType, NamespaceTable namespaceTable, int level ) {
    String elementInfo = buildElementInfo( defElement, level );
    Logger.info( elementInfo );

    boolean success = true;
    // find and check container binding first
    BindingBaseType containerBinding = null;
    if (defElement instanceof SelectionCtrlType) {
      containerBinding = ((SelectionCtrlType) defElement).getListBinding();
    }
    else if (defElement instanceof TreeCtrlType) {
      containerBinding = ((TreeCtrlType) defElement).getListBinding();
    }
    else if (defElement instanceof GroupCtrlType) {
      containerBinding = ((GroupCtrlType) defElement).getModelBinding();
    }
    else if (defElement instanceof ModuleCtrlType) {
      containerBinding = ((ModuleCtrlType) defElement).getModelBinding();
    }

    // check other children with binding path
    boolean containerModelPathOk = true;
    String containerModelPath = parentModelPath;
    if (containerBinding != null) {
      try {
        containerModelPath = checkBinding( containerBinding, parentModelPath, modelType, namespaceTable, level + 1 );
      }
      catch (XpathValidationException ex) {
        containerModelPathOk = false;
        success = false;
        ex.printError( namespaceTable );
      }
    }

    // remember for evaluation of data source
    if (defElement instanceof GroupCtrlType || defElement instanceof SelectionCtrlType || defElement instanceof TreeCtrlType) {
      Object dataSourceId = defElement.get( ControllerFactory.NAMESPACE.getQName( "ID" ) );
      Logger.debug( "   dataSource candidate => " + containerModelPath );
      if (dataSourceId != null && dataSourceId instanceof QName) {
        if (containerModelPathOk) {
          dataSourceModelPaths.put( (QName) dataSourceId, containerModelPath );
        }
        else {
          Logger.warn( "=> cannot be checked if used as dataSource" );
        }
      }
    }

    for (Model child : defElement.iterator( null, null )) {
      if (child instanceof BindingBaseType) {
        if (child != containerBinding) {
          try {
            checkBinding( (BindingBaseType) child, parentModelPath, modelType, namespaceTable, level + 1 );
          }
          catch (XpathValidationException ex) {
            success = false;
            Logger.debug( "continue on error" );
          }
        }
      }
      else {
        if (containerModelPathOk) {
          success = checkElement( child, containerModelPath, modelType, namespaceTable, level + 1 ) && success;
        }
        else {
          String childInfo = buildElementInfo( child, level + 1 );
          Logger.info( childInfo );
          Logger.warn( "SKIP check for child" );
        }
      }
    }
    return success;
  }

  /**
   * Evaluates the given XPath using the given modelType. Throws an {@link XpathValidationException} on error.
   *
   * @param modelPath
   * @param modelType
   * @param namespaceTable
   * @throws if validation fails passes the respective exception containing the reason
   */
  private void checkModelPath( String modelPath, ComplexType modelType, NamespaceTable namespaceTable ) throws XpathValidationException {
    XpathValidator xpathValidator = new XpathValidator( namespaceTable, modelPath );
    xpathValidator.validate( modelType );
  }

  /**
   * Tries to load one GUI file. Returns the loaded model.
   *
   * @param guiFilename
   * @return
   * @throws Exception
   */
  public Model checkLoad( String guiFilename ) throws Exception {
    Model model = null;
    File xmFile = new File( guiFilename );
    if (xmFile.exists()) {
      InputStream inputStream = new BufferedInputStream( new FileInputStream( xmFile ) );
      XmlReader xmlReader = new XmlReader();
      try {
        model = xmlReader.loadData( inputStream, null );
      }
      catch (Exception ex) {
        Logger.info( "File does not contain valid XML: " + ex.getMessage() );
        return null;
      }
      if (model instanceof DialogType) {
        QName id = ((DialogType)model).getID();
        Logger.debug( "   dialog ID=" + id.getName() );
      }
      else if (model instanceof Application) {
        QName id = ((Application)model).getID();
        Logger.debug( "   application ID=" + id.getName() );
      }
      else if (model instanceof DialogModuleType) {
        QName name = ((DialogModuleType)model).getName();
        Logger.debug( "   dialogModule ID=" + name.getName() );
      }
      else {
        Logger.info( "File does not contain a GUI2 definition." );
        model = null;
      }
    }
    else {
      Logger.error( "Cannot find GUI xml file " + xmFile.getCanonicalPath() );
    }
    return model;
  }
}
