/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.tools.gui.util;

import de.pidata.date.DateHelper;
import de.pidata.log.Level;
import de.pidata.log.LoggerInterface;

/**
 * Logger writing to System.out without any nice formatting.
 */
public class SimpleLogger implements LoggerInterface {

  private Level logLevel = Level.INFO;

  public SimpleLogger() {
  }

  /**
   * Do logging
   *
   * @param logLevel the logging level, one of the constants LOG_*
   * @param message  the message to log
   * @param cause    the Throwable which led to this log entry
   */
  @Override
  public void log( Level logLevel, String message, Throwable cause ) {
    if (logLevel.getLevelValue() >= getLogLevel().getLevelValue()) {
      String levelName = logLevel.getLevelName();
      try {
        if (logLevel.getLevelValue() != Level.INFO.getLevelValue()) {
          System.out.print( levelName + " " );
        }
        System.out.println( message );

        if (cause != null) {
          cause.printStackTrace();
        }
        System.out.flush();
      }
      catch (Exception ex) {
        // Print out the exception anyway
        System.out.println( "[" + levelName + "] " + DateHelper.timestamp() + " - " + message );
        if (cause != null) {
          cause.printStackTrace( System.out );
        }
      }
    }
  }

  @Override
  public void setLogLevel( Level loglevel ) {
    this.logLevel = loglevel;
  }

  @Override
  public Level getLogLevel() {
    return logLevel;
  }

  @Override
  public void close() {
    // never close System.out
  }
}
