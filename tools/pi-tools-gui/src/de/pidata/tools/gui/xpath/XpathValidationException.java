/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.tools.gui.xpath;

import de.pidata.log.Logger;
import de.pidata.models.tree.XPath;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;

/**
 * Used from {@link XPath} during validation.
 */
public class XpathValidationException extends IllegalArgumentException {

  private final ComplexType modelType;
  private final QName testPart;

  public XpathValidationException( String message, ComplexType modelType, QName testPart ) {
    super( message );
    this.modelType = modelType;
    this.testPart = testPart;
  }

  public ComplexType getModelType() {
    return modelType;
  }

  public QName getTestPart() {
    return testPart;
  }

  public void printError( NamespaceTable namespaceTable ) {
    QName testPart = getTestPart();
    String testPartStr = "";
    if (testPart != null) {
      testPartStr = testPart.toString( namespaceTable );
    }
    Logger.error( "[" + testPartStr + "]: " + getMessage() );
  }
}
