/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.tools.gui.xpath;

import de.pidata.models.tree.XPathExpression;
import de.pidata.models.tree.XPathValue;
import de.pidata.models.types.ComplexType;

public class XPathExpressionValidator {

  private XPathExpression xPathExpression;

  public XPathExpressionValidator( XPathExpression xPathExpression ) {
    this.xPathExpression = xPathExpression;
  }

  public void validate( ComplexType modelType ) {
    XPathValue left = xPathExpression.getLeft();
    if (left != null) {
      new XPathValueValidator( left ).validate( modelType );
    }
    XPathValue right = xPathExpression.getRight();
    if (right != null) {
      new XPathValueValidator( right ).validate( modelType );
    }
  }
}
