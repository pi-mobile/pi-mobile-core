/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.tools.gui.xpath;

import de.pidata.models.tree.XPath;
import de.pidata.models.tree.XPathLocationStep;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.NamespaceTable;

public class XpathValidator extends XPath {

  /**
   * Setup and parse the XPath expression.
   *
   * @param namespaceTable
   * @param pathExpression
   */
  public XpathValidator( NamespaceTable namespaceTable, String pathExpression ) {
    super( namespaceTable, pathExpression );
  }

  /**
   * Evaluates the path using the given {@link ComplexType}. No check for existing data is done.
   *
   * @param modelType
   */
  public void validate( ComplexType modelType ) {
    XPathLocationStep firstLocation = getFirstLocation();
    if (firstLocation != null) {
      new XPathLocationStepValidator( firstLocation ).validate( modelType );
    }
  }
}
