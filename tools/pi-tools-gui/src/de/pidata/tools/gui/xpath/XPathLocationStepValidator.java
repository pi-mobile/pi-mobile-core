/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.tools.gui.xpath;

import de.pidata.log.Logger;
import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import static de.pidata.models.tree.XPathLocationStep.*;

public class XPathLocationStepValidator {

  private XPathLocationStep xPathLocationStep;

  public XPathLocationStepValidator( XPathLocationStep xPathLocationStep ) {
    this.xPathLocationStep = xPathLocationStep;
  }

  /**
   * Evaluates the step by using the given {@link ComplexType}. No check for existing data is done.
   * <p>
   * CAUTION: copied from {@link XPathLocationStep#evaluate(Model, Context, Model, boolean)}! Only partially implemented.
   *
   * @param modelType
   */
  protected void validate( ComplexType modelType ) {
    QName axis = xPathLocationStep.getAxis();
    QName type = (QName) XPathLocationStep.keywords.get( axis );
    if (type == null) { // axis is a relationID
      ComplexType childType = null;
      try {
        childType = (ComplexType) modelType.getChildType( axis );
      }
      catch (Exception ex) {
        // skip, might be transient
      }
      if (childType == null) {
        // try transient
        try {
          ComplexType transientType = getTransientType( modelType.name() );
          childType = (ComplexType) transientType.getChildType( axis );
        }
        catch (Exception ex) {
          throw new XpathValidationException( "check transient relation, " + ex.getMessage(), modelType, axis );
        }
      }
      if (childType == null) {
        throw new XpathValidationException( "Relation not known for complexType", modelType, axis );
      }
      validateNode( childType );
    }
    else if ((axis == XPathLocationStep.AXIS_S_SELF) || (axis == AXIS_SELF)) {
      validateNode( modelType );
    }
    else if ((axis == AXIS_S_PARENT) || (axis == AXIS_PARENT)) {
      //TODO
      throw new RuntimeException( "TODO: validate parent not yet implemented" );
      // TODO: ComplexType parentType = ;
    }
    else if ((axis == AXIS_S_ATTRIBUTE) || (axis == AXIS_ATTRIBUTE)) {
      validateAttribute( modelType );
    }
    else if (axis == AXIS_VARIABLE) {
      //TODO
      throw new RuntimeException( "TODO: validate variable not yet implemented" );
//
//      Object variable = context.get( nodeTest );
//      if (variable != null) {
//        if (variable instanceof Model) {
//          processNodeTest( (Model) variable, 1, context, result, readOnly );
//        }
//        else {
//          SimpleType simpleType;
//          if (variable instanceof Integer) simpleType = IntegerType.getDefInt();
//          else if (variable instanceof Long) simpleType = IntegerType.getDefLong();
//          else if (variable instanceof Byte) simpleType = IntegerType.getDefByte();
//          else if (variable instanceof Short) simpleType = IntegerType.getDefShort();
//          else if (variable instanceof Boolean) simpleType = BooleanType.getDefault();
//          else if (variable instanceof DateObject) simpleType = DateTimeType.getDefDateTime();
//          else if (variable instanceof Boolean) simpleType = BooleanType.getDefault();
//          else if (variable instanceof DecimalObject) simpleType = DecimalType.getDefault();
//          else {
//            simpleType = StringType.getDefString();
//            variable = variable.toString();
//          }
//          SimpleModel attrModel = new SimpleModel(simpleType, variable);
//          add( attrModel, result, readOnly );
//        }
//      }
    }
    else if ((axis == AXIS_S_DESCENDANT_OR_SELF) || (axis == AXIS_DESCENDANT_OR_SELF)) {
      validateNode( modelType );
    }
    else if (axis == AXIS_CHILD) {
      validateNode( modelType );
    }
    else if (axis == AXIS_DESCENDANT) {
      // TODO? already valid
      throw new RuntimeException( "TODO" );
    }
    else if (axis == AXIS_ANCESTOR) {
      // TODO? already valid
      throw new RuntimeException( "TODO" );
    }
    else if (axis == AXIS_ANCESTOR_OR_SELF) {
      validateNode( modelType );
    }
    else if (axis == AXIS_FOLLOWING_SIBLING) {
      //TODO
      throw new RuntimeException( "TODO" );
    }
    else if (axis == AXIS_PRECEDING_SIBLING) {
      //TODO
      throw new RuntimeException( "TODO" );
    }
    else if (axis == AXIS_FOLLOWING) {
      //TODO
      throw new RuntimeException( "TODO" );
    }
    else if (axis == AXIS_PRECEDING) {
      //TODO
      throw new RuntimeException( "TODO" );
    }
    else if (axis == AXIS_NAMESPACE) {
      //TODO
      throw new XpathValidationException( "TODO: not yet implemented", modelType, axis );
    }
  }

  /**
   * Evalutes the attribute test by using the given {@link ComplexType}. No check for existing data is done.
   * <p>
   * CAUTION: copied from XPathLocationStep#processAttribute(Model, Model, boolean)! Only partially implemented.
   *
   * @param modelType
   */
  private void validateAttribute( ComplexType modelType ) {
    QName attributeID = xPathLocationStep.getNodeTest();
    NamespaceTable namespaceTable = xPathLocationStep.getNamespaceTable();
    if (attributeID.getNamespace() == XPathLocationStep.NAMESPACE) {
      attributeID = QName.getInstance( attributeID.getName(), namespaceTable );
    }
    SimpleType type = null;
    int i = modelType.indexOfAttribute( attributeID );
    if (i >= 0) {
      try {
        type = modelType.getAttributeType( i );
      }
      catch (Exception ex) {
        throw new XpathValidationException( ex.getMessage(), modelType, attributeID );
      }
    }
    else {
      try {
        ComplexType transientType = getTransientType( modelType.name() );
        int k = transientType.indexOfAttribute( attributeID );
        if (k >= 0) {
          type = transientType.getAttributeType( k );
        }
      }
      catch (Exception ex) {
        throw new XpathValidationException( "check transient attribute, " + ex.getMessage(), modelType, attributeID );
      }
    }
    if (type == null) {
      throw new XpathValidationException( "Attribute not known for complexType", modelType, attributeID );
    }
  }

  /**
   * Currently used by XPathValidator to get access to the transient type declaration.
   * <p/>
   * @implNote uses the convention that a transient type should be declared and bound
   * to a static field with the name 'TRANSIENT_TYPE'
   * TODO: Consider implementing some kind of registration / access from the generated factory directly.
   *
   * @param typeName
   * @return
   * @throws IOException
   */
  private ComplexType getTransientType (QName typeName) throws IOException {
    ComplexType transientType = null;
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( typeName.getNamespace() );
    if (factory == null) {
      throw new IOException( "ModelFactory missing for type name=" + typeName );
    }
    Type type = factory.getType( typeName );
    if (type == null) {
      throw new IOException( "Type definition unknown by factory, type name=" + typeName );
    }
    if (type instanceof ComplexType) {
      Class modelClass = type.getValueClass();
      if (modelClass != null && Transient.class.isAssignableFrom( modelClass )) {

        // not reliable
        try {
          Field transientTypeStatic = modelClass.getField( "TRANSIENT_TYPE" );
          Object transientTypeDecl = transientTypeStatic.get( null );
          if (transientTypeDecl != null) {
            if (transientTypeDecl instanceof ComplexType) {
              transientType = (ComplexType) transientTypeDecl;
            }
          }
        }
        catch (Exception ex) {
          // skip, try instance
          Logger.debug( "field TRANSIENT_TYPE missing, try instance: " + ex.getMessage() );
        }

        if (transientType == null) {
          // TODO: might fire...
          boolean isAbstract = Modifier.isAbstract( modelClass.getModifiers() );
          if (isAbstract) {
            throw new IOException( "abstract class cannot be instantiated, type name=" + typeName + ", class name=" + modelClass.getName() );
          }
          else {
            Model typeInstance = factory.createInstance( (ComplexType) type );
            if (typeInstance != null && typeInstance instanceof Transient) {
              transientType = ((Transient) typeInstance).transientType();
            }
          }
        }
      }
    }
    return transientType;
  }

  /**
   * Evaluates the nodeTest by using the given {@link ComplexType}. No check for existing data is done.
   * <p>
   * CAUTION: copied from XPathLocationStep#processNodeTest(Model, int, Context, Model, boolean)! Only partially implemented.
   *
   * @param modelType
   */
  private void validateNode( ComplexType modelType ) {
    boolean accepted = false;
    QName nodeTest = xPathLocationStep.getNodeTest();
    if ((nodeTest == null) || (nodeTest == NODE_ANY)) {
      accepted = true;
    }
    else if (nodeTest == NODE_NODE) {
      accepted = (modelType instanceof ComplexType); // TODO: always true
    }
    else if (nodeTest == NODE_COMMENT) {
      //TODO
      throw new RuntimeException( "TODO" );
    }
    else if (nodeTest == NODE_PROCESSING_INSTRUCTION) {
      //TODO
      throw new RuntimeException( "TODO" );
    }
    else if (nodeTest == NODE_TEXT) {
      //TODO
      throw new RuntimeException( "TODO" );
    }

    if (accepted) {
      XPathExpression predicate = xPathLocationStep.getPredicate();
      if (predicate != null) {
        new XPathExpressionValidator( predicate ).validate( modelType );
      }
      XPathLocationStep nextLocationStep = xPathLocationStep.getNextLocationStep();
      if (nextLocationStep != null) {
        new XPathLocationStepValidator( nextLocationStep ).validate( modelType );
      }
    }
  }
}
