/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.xml;

import de.pidata.log.Logger;
import de.pidata.models.config.Binding;
import de.pidata.models.config.Configurator;
import de.pidata.models.service.PortType;
import de.pidata.models.service.Service;
import de.pidata.service.base.ServiceException;
import de.pidata.models.service.ServiceManager;
import de.pidata.models.service.SimplePortType;
import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.models.xml.binder.XmlWriter;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.service.log.MessageType;
import de.pidata.system.base.SystemManager;

import java.io.IOException;

public class XmlData implements EventListener, Service {

  public static final Namespace NAMESPACE = Namespace.getInstance("http://www.pidata.de/res/service/log");
  public static final QName OP_UPDATE = NAMESPACE.getQName("update");
  public static final QName OP_REMOVE = NAMESPACE.getQName("remove");
  public static final QName PART_MODELID = NAMESPACE.getQName("key");
  public static final QName PART_RELATION = NAMESPACE.getQName("relation");

  public static final ComplexType UPDATE_MSG_TYPE;
  static {
    UPDATE_MSG_TYPE = MessageType.TYPE;
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( NAMESPACE );
    factory.addRootRelation(OP_UPDATE, MessageType.TYPE, 0, 1, null);
    factory.addRootRelation(PART_MODELID, QNameType.getQName(), 0, 1, null);
    factory.addRootRelation(PART_RELATION, QNameType.getQName(), 0, 1, null);
  }

  private String path;
  private Model dataModel;
  private PortType portType;
  
  /**
   * Creates a new XML data manager using file at path to load/stora data and providing
   * service port portTypeName for updates from remote backoffice.
   * @param portTypeName the service providesd for updates from backoffice
   * @param path         the path to the file used as storage
   * @param dataRoot     the root to add this XmlData's models
   * @throws IOException if loading XML data form path fails
   */
  public XmlData(QName portTypeName, String path, Root dataRoot) throws IOException {
    this.path = path;
    dataModel = XmlReader.loadData(path);
    dataRoot.add(dataModel.getParentRelationID(), dataModel);
    dataModel.addListener(this);
    addChildListeners(dataModel);
    if (portTypeName != null) {
      this.portType = new SimplePortType( portTypeName );
      //TODO add aoperations
      ServiceManager.getInstance().registerService(portTypeName, this);
    }
  }

  public void init( ServiceManager serviceMgr, Configurator configurator, Binding binding ) throws Exception {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  private void addChildListeners(Model model) {
    for (ModelIterator iter = model.iterator(null, null); iter.hasNext(); ) {
      Model childModel = iter.next();
      childModel.addListener(this);
      addChildListeners(childModel);
    }
  }

  private void removeChildListeners(Model model) {
    for (ModelIterator iter = model.iterator(null, null); iter.hasNext(); ) {
      Model childModel = iter.next();
      childModel.removeListener(this);
      removeChildListeners(childModel);
    }
  }

  /**
   * An event occurred.
   *
   * @param eventSender the sender of this event
   * @param eventID     the id of the event
   * @param source      the instance where the event occurred on, may be a child of eventSender
   * @param modelID     the id of the affected model
   * @param oldValue    the old value of the model
   * @param newValue    the new value of the model
   */
  @Override
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {
    Model sourceModel = (Model) source;
    //TODO Ein- und Aushängestrategie prüfen --> memory leaks möglich!!
    if (eventID == EventListener.ID_MODEL_DATA_REMOVED) {
      if (oldValue == null) {
        removeChildListeners(sourceModel);
      }
      else {
        Model childModel = (Model) oldValue;
        childModel.removeListener(this);
        removeChildListeners(childModel);
      }
    }
    else if (eventID == EventListener.ID_MODEL_DATA_ADDED) {
      if (newValue == null) {
        addChildListeners(sourceModel);
      }
      else {
        Model childModel = (Model) newValue;
        childModel.addListener(this);
        addChildListeners(childModel);
      }
    }
    try {
      XmlWriter writer = new XmlWriter(SystemManager.getInstance().getStorage(null));
      writer.write(path, dataModel, dataModel.getParentRelationID());
    }
    catch (IOException ex) {
      Logger.error("Could not store data model", ex);
      //TODO Meldung an Anwender!!!!
      throw new RuntimeException("Could not store data model");
    }
  }

  public PortType portType() {
    return this.portType;
  }

  public Model invoke(Context caller, QName operation, Model input) throws ServiceException {
    if (operation == OP_UPDATE) {
      Model change = input.get(null, null);
      this.dataModel.replace(change.getParentRelationID(), change);
    }
    else if (operation == OP_REMOVE) {
      SimpleModel remove = (SimpleModel) input.get(PART_MODELID, null);
      SimpleModel relation = (SimpleModel) input.get(PART_RELATION, null);
      QName relationID = (QName) relation.getContent();
      QName modelID = (QName) remove.getContent();
      Model oldChild = this.dataModel.get(relationID, modelID);
      if (oldChild != null) {
        this.dataModel.remove(relationID, oldChild);
      }
    }
    else {
      throw new IllegalArgumentException("Unsupported operation, opID="+operation);
    }
    return null;
  }

  /**
   * Tells this service to shut down, e.g. close database connections
   */
  public void shutdown() {
    // do nothing
  }
}
