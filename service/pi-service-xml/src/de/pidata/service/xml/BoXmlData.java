/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.xml;

import de.pidata.bo.base.BoStorage;
import de.pidata.bo.base.Businessobject;
import de.pidata.bo.xml.BoSequenceXml;
import de.pidata.bo.xml.BoStorageXml;
import de.pidata.log.Logger;
import de.pidata.models.config.Binding;
import de.pidata.models.config.Configurator;
import de.pidata.models.service.PortType;
import de.pidata.models.service.Service;
import de.pidata.service.base.ServiceException;
import de.pidata.models.service.ServiceManager;
import de.pidata.models.service.SimplePortType;
import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.complex.DefaultRelation;
import de.pidata.models.types.simple.QNameType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.service.log.MessageType;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;

import java.io.IOException;
import java.util.Collection;

public class BoXmlData  implements EventListener, Service {

  public static final Namespace NAMESPACE = Namespace.getInstance("http://www.pidata.de/res/service/log");
  public static final QName OP_UPDATE = NAMESPACE.getQName("update");
  public static final QName OP_REMOVE = NAMESPACE.getQName("remove");
  public static final QName PART_MODELID = NAMESPACE.getQName("key");
  public static final QName PART_RELATION = NAMESPACE.getQName("relation");

  public static final ComplexType UPDATE_MSG_TYPE;
  static {
    UPDATE_MSG_TYPE = MessageType.TYPE;
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( NAMESPACE );
    factory.addRootRelation(OP_UPDATE, MessageType.TYPE, 0, 1, null);
    factory.addRootRelation(PART_MODELID, QNameType.getQName(), 0, 1, null);
    factory.addRootRelation(PART_RELATION, QNameType.getQName(), 0, 1, null);
  }

  private PortType portType;
  private BoStorage boStorage;
  private DefaultModel boContainer;

  /**
   * Creates a new XML data manager using file at path to load/stora data and providing
   * service port portTypeName for updates from remote backoffice.
   * @param portTypeName the service provided for updates from backoffice;
   *                     also used for BO container model's type name and root relation
   * @param path         the path to the file used as storage
   * @param dataRoot     the root to add this XmlData's models
   * @throws java.io.IOException if loading XML data form path fails
   */
  public BoXmlData(QName portTypeName, String path, Root dataRoot, Namespace ns ) throws IOException {
    if (portTypeName == null) {
      throw new IllegalArgumentException( "portTypeName must not be null for storage BO-XML");
    }

    Storage xmlStorage = SystemManager.getInstance().getStorage( path );
    this.boStorage = new BoStorageXml( xmlStorage );

    //----- Create BO container and add it to dataRoot
    DefaultComplexType boContainerType = new DefaultComplexType( portTypeName, DefaultModel.VALUE_CLASS.getName(), 0 );
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( ns );
    for (QNameIterator relIter = factory.relationNames(); relIter.hasNext(); ) {
      boContainerType.addRelation( factory.getRootRelation( relIter.next() ));
    }
    dataRoot.addRelation( new DefaultRelation( portTypeName, boContainerType, 0, 1, Collection.class, null ) );
    boContainer = new DefaultModel( null, boContainerType );
    dataRoot.add( portTypeName, boContainer );

    //----- Load existing BOs from data directory
    String[] boFiles = xmlStorage.list();
    for (int i = 0; i < boFiles.length; i++) {
      int posExt = boFiles[i].lastIndexOf( '.' );
      if (posExt > 0) {
        String ext = boFiles[i].substring( posExt+1 );
        if ("xml".equals( ext )) {
          int posID = boFiles[i].lastIndexOf( BoStorageXml.ID_DELMIITER );
          QName typeName = ns.getQName( boFiles[i].substring( 0, posID ) );
          ComplexType type = (ComplexType) factory.getType( typeName );
          Key id = AbstractModelFactory.createKey( type, boFiles[i].substring( posID+1, posExt ), boStorage.getNamespaces() );
          Businessobject bo = this.boStorage.load( type, id );
          boContainer.add( typeName, bo );
        }
        else if ("seq".equals( ext )) {
          String sequenceName = boFiles[i].substring( 0, posExt );
          BoSequenceXml sequence = new BoSequenceXml( sequenceName, xmlStorage );
          SystemManager.getInstance().addSequence( sequenceName, sequence );
        }
      }
    }
    boContainer.addListener( this );

    //----- Create service
    this.portType = new SimplePortType( portTypeName );
    //TODO add aoperations
    ServiceManager.getInstance().registerService(portTypeName, this);
  }

  public void init( ServiceManager serviceMgr, Configurator configurator, Binding binding ) throws Exception {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  private void storeBO( Businessobject bo ) {
    try {
      this.boStorage.store( bo );
    }
    catch (IOException ex) {
      Logger.error("Could not store business object", ex);
      //TODO Meldung an Anwender!!!!
      throw new RuntimeException("Could not store business object");
    }
  }

  private void deleteBO( Businessobject bo ) {
    try {
      this.boStorage.delete( bo );
    }
    catch (IOException ex) {
      Logger.error("Could not delete business object", ex);
      //TODO Meldung an Anwender!!!!
      throw new RuntimeException("Could not delete business object");
    }
  }

  /**
   * An event occurred.
   *
   * @param eventSender the sender of this event
   * @param eventID     the id of the event
   * @param source      the instance where the event occurred on, may be a child of eventSender
   * @param modelID     the id of the affected model
   * @param oldValue    the old value of the model
   * @param newValue    the new value of the model
   */
  @Override
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {
    if (source == boContainer) {
      if (eventID == EventListener.ID_MODEL_DATA_REMOVED) {
        deleteBO( (Businessobject) oldValue );
      }
      else if (eventID == EventListener.ID_MODEL_DATA_ADDED) {
        storeBO( (Businessobject) newValue );
      }
    }
    else if (source instanceof Businessobject) {
      storeBO( (Businessobject) source );
    }
    else if (source instanceof Model) {
      Model parent = ((Model) source).getParent( false );
      while ((parent != null) && !(parent instanceof Businessobject)) {
        parent = parent.getParent( false );
      }
      if (parent != null) {
        storeBO( (Businessobject) parent );
      }
    }
  }

  public PortType portType() {
    return this.portType;
  }

  public Model invoke( Context caller, QName operation, Model input) throws ServiceException {
    try {
      if (operation == OP_UPDATE) {
        Model change = input.get(null, null);
        boStorage.store( (Businessobject) change );
        return null;
      }
      else if (operation == OP_REMOVE) {
        SimpleModel remove = (SimpleModel) input.get(PART_MODELID, null);
        SimpleModel relation = (SimpleModel) input.get(PART_RELATION, null);
        ComplexType boType = (ComplexType) AbstractModelFactory.findType( (QName) relation.getContent() );
        String keyString = (String) remove.getContent();
        Key key = AbstractModelFactory.createKey( boType, keyString, boStorage.getNamespaces() );
        boStorage.delete( boType, key );
        return null;
      }
    }
    catch (IOException e) {
      Logger.error( "BoXmlData service failed", e );
      throw new ServiceException( ServiceException.SERVICE_FAILED, e.getMessage() );
    }
    throw new ServiceException( ServiceException.UNKNOWN_OPERATION, "Unsupported operation, opID="+operation);
  }

  /**
   * Tells this service to shut down, e.g. close database connections
   */
  public void shutdown() {
    if (boContainer != null) {
      boContainer.removeListener( this );
    }
  }
}

