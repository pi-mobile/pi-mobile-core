/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.mobilserver.jetty.servlet;

import de.pidata.file.FilebasedRollingLogger;
import de.pidata.log.Level;
import de.pidata.log.Logger;
import de.pidata.mobilserver.servlet.ServletConnector;
import de.pidata.system.base.SystemManager;
import org.mortbay.http.HttpContext;
import org.mortbay.http.HttpServer;
import org.mortbay.jetty.servlet.ServletHandler;
import org.mortbay.jetty.servlet.ServletHolder;

import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.Properties;

public class JettyStarter {

  private HttpServer server;

  private String getPropertyChecked( String propertyName) {
    String value = SystemManager.getInstance().getProperty( propertyName, null );
    if ((value == null) || (value.length() == 0)) {
      throw new IllegalArgumentException("Property '" + propertyName + "' ist nicht definiert in system.properties" );
    }
    return value;
  }

  public Integer start( String[] args ) {
    String propFile;
    try {
      if (args.length > 0) propFile = args[args.length - 1];
      else propFile = "system.properties";

      Properties sysProps = new Properties();
      sysProps.load( new FileInputStream( propFile ) );
      String servletPort = sysProps.getProperty( "servlet.port" );
      String servletName = sysProps.getProperty( "servlet.name" );
      String servletPath = sysProps.getProperty( "servlet.path" );

      initLogging( sysProps );

      server = new HttpServer();
      server.addListener( ":" + servletPort );

      HttpContext context = server.getContext( "/" );
      ServletHandler servletHandler = new ServletHandler();
      ServletHolder holder = servletHandler.addServlet( servletName, servletPath, ServletConnector.class.getName() );
      for ( Enumeration propKeys = sysProps.keys(); propKeys.hasMoreElements(); ) {
        String key = (String) propKeys.nextElement();
        holder.setInitParameter( key, sysProps.getProperty( key ) );
      }
      servletHandler.addServletHolder( holder );
      context.addHandler( servletHandler );

      server.start();

      String msg = "Jetty startup finished, listening on port=" + servletPort + ", path=" + servletPath;
      Logger.info( msg );
    }
    catch (Exception ex) {
      Logger.fatal( "Could not start Jetty", ex );
      ex.printStackTrace();
      return new Integer( 1 );
    }
    return null;
  }

  private void initLogging( Properties sysProps ) {
    String logfilePrefix = sysProps.getProperty( FilebasedRollingLogger.KEY_LOGFILE );
    if ((logfilePrefix == null) || (logfilePrefix.length() == 0)) {
      Logger.warn( "INFO: logfile not configured" );
      return;
    }

    int logfileExpireDays = 1;
    String expireStr = sysProps.getProperty( FilebasedRollingLogger.KEY_LOGFILE_EXPIREDAYS );
    if (expireStr != null) {
      try {
        logfileExpireDays = Integer.parseInt(expireStr);
      }
      catch (Exception ex) {
        Logger.warn("Error parsing value for property="+FilebasedRollingLogger.KEY_LOGFILE_EXPIREDAYS+", value="+expireStr);
      }
    }

    int logfilePurgeDays = 0;
    String purgeStr = sysProps.getProperty( FilebasedRollingLogger.KEY_LOGFILE_PURGEDAYS );
    if (purgeStr != null) {
      try {
        logfilePurgeDays = Integer.parseInt(purgeStr);
      }
      catch (Exception ex) {
        Logger.warn("Error parsing value for property="+FilebasedRollingLogger.KEY_LOGFILE_PURGEDAYS+", value="+purgeStr);
      }
    }

    Level logLevel = Level.INFO;
    String logLevelProp = sysProps.getProperty( Logger.KEY_LOGLEVEL );
    if (logLevelProp != null) {
      System.out.print( "loglevel=" + logLevelProp );
      logLevel = Level.fromName( logLevelProp );
      System.out.println( " (" + logLevel + ")" );
    }

    Logger.setLogger( new FilebasedRollingLogger( logfilePrefix, logfileExpireDays, logfilePurgeDays, logLevel ) );
  }

  public int stop( int exitCode ) {
    try {
      server.stop();
    }
    catch (InterruptedException e) {
      Logger.error( "Error stopping Jetty", e );
      return 1;
    }
    return exitCode;
  }

  public static void main( String[] args ) {
    try {
      new JettyStarter().start( args );
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
