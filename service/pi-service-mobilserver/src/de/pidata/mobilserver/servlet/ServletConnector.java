/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.mobilserver.servlet;

import de.pidata.comm.client.base.SoapSender;
import de.pidata.log.Logger;
import de.pidata.service.server.CommServer;
import de.pidata.service.server.StreamProcessor;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.SystemManager;
import de.pidata.system.server.ServerSystem;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Properties;

//TODO Klasse ueberarbeiten
public class ServletConnector extends HttpServlet {

  public static final String SERVLET_PROCESSOR = "servlet.processor";

  //private static boolean initialized = false;

  private static StreamProcessor streamProcessor;

  public synchronized void init( ServletConfig config ) throws ServletException {
    super.init( config );
    Logger.info( "init servlet class: "+toString() );
    //if (initialized) {
    if (streamProcessor != null) {
      return;
      //logger.error( "servlet connector already initilized." );
      //throw new ServletException( "servlet connector already initilized." );
    }

    //initialized = true;

    Enumeration names = config.getInitParameterNames();
    Logger.debug( "init-parameternames:" );
    while (names.hasMoreElements()) {
      Logger.debug( names.nextElement().toString() );
    }

    try {
      if (SystemManager.getInstance() == null) {
          new ServerSystem( "." );
      }
      String streamProcClass = SystemManager.getInstance().getProperty( SERVLET_PROCESSOR, null );
      if (streamProcClass == null) {
        streamProcClass = CommServer.class.getName();
      }
      streamProcessor = (StreamProcessor) Class.forName( streamProcClass ).newInstance();
    }
    catch (Exception ex) {
      // in case application could not be initialized
      // this servlet is invalid and may not be loaded again from
      // the same class loader
      Logger.error( "FATAL ERROR WHILE SERVLET INITILIZATION", ex );
      System.out.println("========================================================================");
      System.out.println("===  FATAL ERROR WHILE SERVLET INITILIZATION.");
      System.out.println("===  Message: '" + ex.getMessage() + "");
      System.out.println("Stack trace:" );
      ex.printStackTrace();
      System.out.println("========================================================================");
      throw new ServletException( "FATAL ERROR WHILE SERVLET INITILIZATION: '" + ex.getMessage() + "'" );
    }
    Logger.info( "finished init servlet class: "+toString() );
  }

  public void destroy() {
    if (streamProcessor != null) {
      try {
        streamProcessor.shutdown();
      }
      catch (Exception ex) {
        Logger.error( "Exception while StreamProcessor.shutdown()", ex );
      }
      streamProcessor = null;
    }
    SystemManager sysMan = SystemManager.getInstance();
    if (sysMan != null) {
      try {
        SystemManager.getInstance().exit();
      }
      catch (Exception ex) {
        Logger.error( "Exception while SystemManager.exit()", ex );
      }
    }
  }

  protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    InputStream inStream = null;
    OutputStream outStream = null;
    try {
      //----- Header lesen
      Properties header = new Properties();
      for (Enumeration propEnum = request.getHeaderNames(); propEnum.hasMoreElements(); ) {
        String propName = (String) propEnum.nextElement();
        String value = request.getHeader( propName );
        header.setProperty( propName.toLowerCase(), value );
      }

      Logger.debug("--> service( " + header.getProperty( SoapSender.HEADER_SOAP_ACTION ) + " ), clientID="
                   + header.getProperty( SoapSender.HEADER_SENDER_ID ) );
      inStream = request.getInputStream();


      //----- Request verarbeiten
      outStream = response.getOutputStream();
      streamProcessor.process( header, inStream, outStream );
      Logger.debug("<-- service( " + header.getProperty( SoapSender.HEADER_SOAP_ACTION ) + " )");
    }
    catch (Exception e) {
      Logger.error("Could not delegate request", e);
    }
    finally {
      StreamHelper.close( inStream );
      StreamHelper.close( outStream );
    }
  }
}
