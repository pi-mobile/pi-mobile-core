/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.mobilserver.servlet;

import de.pidata.file.FilebasedRollingLogger;
import de.pidata.log.Logger;
import de.pidata.models.types.simple.DateObject;
import de.pidata.system.base.SystemManager;

import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Calendar;

public class LogFileServlet  extends GenericServlet {

  private BufferedInputStream br;
  private String line = null;
  private StringBuffer lineBuf = new StringBuffer();
  private String filename;

  public void init( ServletConfig config ) throws ServletException {
    super.init( config );
    this.filename = config.getServletName();
  }

  private String todayStr() {
    Calendar cal = DateObject.getCalendar();
    cal.setTimeInMillis(System.currentTimeMillis());
    StringBuffer buf = new StringBuffer();
    buf.append(cal.get(Calendar.YEAR)).append("-");
    int month = cal.get(Calendar.MONTH) + 1;
    if (month < 10) buf.append("0");
    buf.append(month).append("-");
    int day = cal.get(Calendar.DAY_OF_MONTH);
    if (day < 10) buf.append("0");
    buf.append(day);
    return buf.toString();
  }

  public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
    String line;
    File logfile;
    String path;

    Logger.info( "LogFileServlet: Log file requested from "+servletRequest.getRemoteAddr() );

    //----- path erst hier zusammenbauen, da sonst SystemManager noch nicht initialisiert ist
    if (filename.equals("catalina")) {
      path = "../logs/catalina.log";
    }
    else if (filename.equals("localhost")) {
      path = "../logs/localhost."+todayStr()+".log";
    }
    else if (filename.equals("logfile")) {
      path = SystemManager.getInstance().getProperty( FilebasedRollingLogger.KEY_LOGFILE, null );
    }
    else {
      path = SystemManager.getInstance().getBasePath() + "/logs/" + filename + ".log";
    }

    //----- Datei lesen und als HTML zurückgeben
    OutputStreamWriter writer = new OutputStreamWriter(servletResponse.getOutputStream());
    writer.write("<html><body><pre>");
    if (path != null) {
      logfile = new File( path );
      if (logfile.exists()) {
        br = new BufferedInputStream( new FileInputStream( logfile ) );
        do {
          line = next();
          if (line != null) writer.write( line );
          writer.write( "\n" );
        }
        while (hasNext());
      }
      else {
        writer.write( "Logfile does not exist: " + logfile.getAbsolutePath() );
      }
    }
    else {
      writer.write( "Property '"+FilebasedRollingLogger.KEY_LOGFILE+"' is missing" );
    }
    writer.write("</pre></body></html>");
    writer.flush();
    writer.close();
  }

  public boolean hasNext() {
    return line != null;
  }

  public String next() {
    String lastLine = line;

    try {
      line = readline(); //br.readLine();
    }
    catch (IOException e) {
      line = e.getMessage();
    }

    if (line == null) {
      try {
        close();
      }
      catch (IOException e) {
        e.printStackTrace();
      }
    }
    else {
      if (line.length() == 0) line = " ";
    }

    return lastLine;
  }

  private String readline() throws IOException {
    lineBuf.setLength(0);
    int chInt = br.read();
    while ((chInt >= 0) && (chInt < ' ')) chInt = br.read();
    while (chInt >= ' ') {
      if (chInt == '>') lineBuf.append("&gt;");
      else if (chInt == '<') lineBuf.append("&lt;");
      else lineBuf.append((char)chInt);
      chInt = br.read();
    }
    if ((chInt < 0) && (lineBuf.length() == 0)) {
      return null;
    }
    else {
      return lineBuf.toString();
    }
  }

  public void remove() {
    throw new RuntimeException("remove not implemented");
  }

  public void close() throws IOException {
    if (br != null) {
      br.close();
    }
    br = null;
    line = null;
  }
}
