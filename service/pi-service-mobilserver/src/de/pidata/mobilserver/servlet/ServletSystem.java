/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.mobilserver.servlet;

import de.pidata.log.Logger;
import de.pidata.models.xml.schema.Schema;
import de.pidata.system.base.Storage;
import de.pidata.system.base.WifiManager;
import de.pidata.system.filebased.FilebasedStorage;
import de.pidata.system.filebased.FilebasedSystem;

import javax.servlet.ServletConfig;
import java.io.IOException;
import java.util.*;

public class ServletSystem extends FilebasedSystem {

  private static final boolean DEBUG = false;

  protected ServletConfig config;

  /**
   * Initialize SystemManager for Servlet use.
   * Initializes SystemReader and Storage.
   */
  public ServletSystem( ServletConfig config ) {
    super();
    this.config = config;
    initProperties();
    initLogging();
    loadFactories();
    Schema.loadSchemas( this );
    Logger.debug( "factories and schemas loaded" );
  }

  public void initProperties() {
    this.programName = config.getServletName();
    Properties systemProps = getSystemProps();
    for (Enumeration paramNames = config.getInitParameterNames(); paramNames.hasMoreElements();) {
      String paramName = (String) paramNames.nextElement();
      systemProps.setProperty( paramName, config.getInitParameter( paramName ) );
    }
    // set system base path
    basePath = systemProps.getProperty( KEY_BASEPATH );
    if (basePath == null) {
      basePath = ".";
    }
    this.systemStorage = new FilebasedStorage( null, basePath );
    Logger.debug( "init parameters read, basePath='" + basePath + "'" );
  }

  @Override
  public Storage getStorage( String storageType, String storagePath ) {
    if (storageType.equals( STORAGE_CLASSPATH )) {
      return getStorage( storagePath );
    }
    else {
      String path = getProperty( "storage."+storageType, null );
      if (path == null) {
        throw new IllegalArgumentException( "Unknown storage type=" + storageType + " - use property 'storage."+storageType+"' to define path" );
      }
      else {
        return getStorage( path + '/' + storagePath );
      }
    }
  }

  @Override
  public WifiManager getWifiManager() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Retrieve a system wide application property. Properties are not bound to the lifetime
   * of one instance. The implementing class has to supply a mechanism to make
   * them persistent, so they can be accessed across several sessions.
   *
   * @param key The key identifying this property.
   * @return A property bound to that key or null if none has been defined.
   */
  protected String getApplicationProperty( String key ) {
    return null; // No application properties for ServletSystem
  }

  /**
   * Set a system wide application property. The system has to ensure properties are made persistent.
   *
   * @param key   The key identifying this property.
   * @param value The value to be bound to that key.
   * @param save  indicates if this property should be made persistent immediately or not.
   * @throws java.io.IOException If properties could not be saved and save is true.
   */
  public void setProperty( String key, String value, boolean save ) throws IOException {
    throw new UnsupportedOperationException( "ServletSystem currently does not support application properties" );
  }

  /**
   * Commit all application properties to some persistent storage, e.g. file system.
   *
   * @throws java.io.IOException If properties could not be saved.
   */
  public void saveProperties() throws IOException {
     // No application properties for ServletSystem
  }

  /**
   * Perform some cleanup and exit runtime system.
   */
  public void exit() {
    doExit();
  }

  /**
   * Returns a new Calendar instance for the current TimeZone, specified in application.properties, e.g:
   * timezone=Germany/Berlin
   * @return the Calender instance
   */
  public Calendar getCalendar() {
    String timeZoneString = getProperty( "timezone", "Europe/Berlin" );
    String localeLanguage = getProperty("localeLanguage", "de" );
    String localeCountry = getProperty("localeCountry", "DE");

    TimeZone timeZone = TimeZone.getTimeZone( timeZoneString );
    Locale locale = new Locale( localeLanguage, localeCountry );
    Calendar calendar = Calendar.getInstance( timeZone, locale );

    if (LOG_CALENDARINFO) {
      Logger.info( "----- available TimeZones" );
      String[] availableTimeZones = TimeZone.getAvailableIDs();
      for (int i = 0; i<availableTimeZones.length; i++) {
        Logger.info( availableTimeZones[i] );
      }

      Logger.info( "----- available Locales" );
      Locale[] availableLocales = Locale.getAvailableLocales();
      for (int i=0; i<availableLocales.length; i++) {
        Logger.info( availableLocales[i].toString() );
      }
    }

    if (calendar == null) {
      Logger.warn( "No Calendar instance found for ["+timeZoneString+"/"+localeLanguage+"_"+localeCountry+"]" );
    }
    else if (DEBUG) {
      Logger.info( "Use Calendar ["+calendar.toString()+"]" );
    }
    return calendar;
  }

}
