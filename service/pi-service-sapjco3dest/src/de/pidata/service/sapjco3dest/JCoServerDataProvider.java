/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.sapjco3dest;

import com.sap.conn.jco.ext.Environment;
import com.sap.conn.jco.ext.ServerDataEventListener;
import com.sap.conn.jco.ext.ServerDataProvider;

import java.util.HashMap;
import java.util.Properties;

/**
 * Created by pru on 22.04.15.
 */
public class JCoServerDataProvider implements ServerDataProvider {

  private static JCoServerDataProvider serverProvider;

  private HashMap<String,Properties> jcoServerProps = new HashMap<String, Properties>();
  private ServerDataEventListener serverDataEventListener = null;

  public JCoServerDataProvider() {
    Environment.registerServerDataProvider( this );
  }

  public void setServerProperties( String serverName, Properties jcoServerProps ) {
    this.jcoServerProps.put( serverName, jcoServerProps );
    if (serverDataEventListener != null) {
      serverDataEventListener.updated( serverName );
    }
  }

  public Properties getServerProperties( String serverName ) {
    return jcoServerProps.get( serverName );
  }

  public boolean supportsEvents() {
    return true;
  }

  public void setServerDataEventListener( ServerDataEventListener serverDataEventListener ) {
    this.serverDataEventListener = serverDataEventListener;
  }

  public static void initialize( String serverName, String gwHost, String gwServ, String replyDestName, int connectionCount ) {
    System.out.println( "JCO adding server name=" + serverName + ", gwHost=" + gwHost + ", gwServ=" + gwServ );
    Properties serverProperties = new Properties();
    serverProperties.setProperty( ServerDataProvider.JCO_PROGID, serverName );
    serverProperties.setProperty( ServerDataProvider.JCO_GWHOST, gwHost );
    serverProperties.setProperty( ServerDataProvider.JCO_GWSERV, gwServ );
    serverProperties.setProperty( ServerDataProvider.JCO_REP_DEST, replyDestName );
    serverProperties.setProperty( ServerDataProvider.JCO_CONNECTION_COUNT, Integer.toString( connectionCount ) );

    if (serverProvider == null) {
      if (Environment.isServerDataProviderRegistered()) {
        // If existing ServerDataProvider is not instance of us, following line will throw an exception.
        // Then there is no chance for us to register our connection!
        throw new RuntimeException( "JCo: ServerDataProvider already registered - Hint: this class must be located in common libs (like sapjco3)" );
      }
      else {
        serverProvider = new JCoServerDataProvider();
        System.out.println( "JCoServerDataProvider created for servername=" + serverName );
      }
    }
    serverProvider.setServerProperties( serverName, serverProperties );
  }
}
