/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.sapjco3dest;

import com.sap.conn.jco.JCoDestinationManager;
import com.sap.conn.jco.ext.DestinationDataEventListener;
import com.sap.conn.jco.ext.DestinationDataProvider;
import com.sap.conn.jco.ext.Environment;

import java.util.HashMap;
import java.util.Properties;

/**
 * Extend this
 */
public class JCoDestinationDataProvider implements DestinationDataProvider {

  private static JCoDestinationDataProvider destProvider;

  private HashMap<String,Properties> jcoDestProps = new HashMap<String, Properties>();
  private DestinationDataEventListener destinationDataEventListener = null;

  public JCoDestinationDataProvider() {
    Environment.registerDestinationDataProvider( this );
  }

  public void setDestinationProperties( String destinationName, Properties jcoDestProps ) {
    this.jcoDestProps.put( destinationName, jcoDestProps );
    if (destinationDataEventListener != null) {
      destinationDataEventListener.updated( destinationName );
    }
  }

  public Properties getDestinationProperties( String destinationName ) {
    return jcoDestProps.get( destinationName );
  }

  public boolean supportsEvents() {
    return true;
  }

  public void setDestinationDataEventListener( DestinationDataEventListener destinationDataEventListener ) {
    this.destinationDataEventListener = destinationDataEventListener;
  }

  /**
   * Initializeds the connection pool used by all instances of this class
   *
   * @param poolName
   * @param conn
   * @param prmMandant
   * @param prmUsr
   * @param prmPwd
   * @param language
   * @param prmSapSystem
   * @param sysNR
   */
  public static void initialize( String poolName, int conn, String prmMandant, String prmUsr,
                                 String prmPwd, String language, String prmSapSystem, String sysNR ) {

    //----- Check parameters:
    int i;
    if (null == prmUsr || 0 >= prmUsr.length()) {
      throw new IllegalArgumentException( "prmUser must not be empty or null" );
    }
    if (null == prmPwd || 0 >= prmPwd.length()) {
      throw new IllegalArgumentException( "prmPwd must not be empty or null" );
    }
    if (null == prmSapSystem || 0 >= prmSapSystem.length()) {
      throw new IllegalArgumentException( "prmSapSystem must not be empty or null" );
    }
    if (null == prmMandant || 0 >= prmMandant.length()) prmMandant = "100";
    if (0 <= (i = prmSapSystem.indexOf( '(' ))) {
      prmSapSystem = prmSapSystem.substring( i + 1 );
      if (0 <= (i = prmSapSystem.indexOf( ')' )))
        prmSapSystem = prmSapSystem.substring( 0, i );
    }
    prmSapSystem = prmSapSystem.trim();
    if (0 >= prmSapSystem.length()) {
      throw new IllegalArgumentException( "prmSapSystem must be at least one character" );
    }

    //----- Unregister possible existing
    try {
      JCoDestinationDataProvider destProvider = (JCoDestinationDataProvider) JCoDestinationManager.getDestination( poolName );
      if (destProvider != null) {
        Environment.unregisterDestinationDataProvider( destProvider );
      }
    }
    catch (Exception e) {
      // do nothing
    }

    //----- Create connection pool
    System.out.println( "JCO adding client pool name=" + poolName + ", conn=" + conn + ", prmMandant=" + prmMandant
        + ", prmUsr=" + prmUsr + ", prmSapSystem=" + prmSapSystem + ", sysNR=" + sysNR );
    Properties connectProperties = new Properties();
    connectProperties.setProperty( DestinationDataProvider.JCO_ASHOST, prmSapSystem );
    connectProperties.setProperty( DestinationDataProvider.JCO_SYSNR, sysNR );
    connectProperties.setProperty( DestinationDataProvider.JCO_CLIENT, prmMandant );
    connectProperties.setProperty( DestinationDataProvider.JCO_USER, prmUsr );
    connectProperties.setProperty( DestinationDataProvider.JCO_PASSWD, prmPwd );
    connectProperties.setProperty( DestinationDataProvider.JCO_LANG, language );
    connectProperties.setProperty( DestinationDataProvider.JCO_POOL_CAPACITY, Integer.toString( conn ) );
    connectProperties.setProperty( DestinationDataProvider.JCO_PEAK_LIMIT, "10" );

    if (destProvider == null) {
      if (Environment.isDestinationDataProviderRegistered()) {
        // If existing DestinationDataProvider is not instance of us, following line will throw an exception.
        // Then there is no chance for us to register our connection!
        throw new RuntimeException( "JCo: DestinationDataProvider already registered - Hint: this class must be located in common libs (like sapjco3)" );
      }
      else {
        destProvider = new JCoDestinationDataProvider();
        System.out.println( "JCoDestinationDataProvider created for poolname=" + poolName );
      }
    }
    destProvider.setDestinationProperties( poolName, connectProperties  );
  }
}
