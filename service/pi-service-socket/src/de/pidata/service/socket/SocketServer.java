/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.socket;

import de.pidata.connect.stream.*;
import de.pidata.log.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by pru on 28.04.17.
 */
public class SocketServer implements Runnable {

  public static final int TIMEOUT = 10000;
  public static final int SOCKET_INIT_RETRY_MILLIS = 1000;
  
  private int port;
  private StreamHandlerFactory streamHandlerFactory;
  private ServerSocket serverSocket = null;
  private boolean active = false;
  private boolean running = false;
  private Thread socketThread = null;

  private List<SocketServerConnection> connectionList = new LinkedList<SocketServerConnection>();

  private static int NEXT_CONNECTION_ID = 1;

  public SocketServer( int port ) {
    this.port = port;
  }

  /**
   *
   * Wird nach dem Konstruktor aufgerufen, um die Parameter zu übergeben. Es wird
   * sichergestellt, dass init() genau einmal je Instanz aufgerufen wird.
   *
   * @param props Liste mit Parametern
   * @throws Exception
   */
  public void init( StreamHandlerFactory streamHandlerFactory, Hashtable props ) throws Exception {
    if (this.streamHandlerFactory != null) {
      throw new IllegalArgumentException( "Must not call init() more than once" );
    }
    this.streamHandlerFactory = streamHandlerFactory;
  }

  public synchronized int connectionCount() {
    return connectionList.size();
  }

  public synchronized SocketServerConnection getConnection( int index ) {
    return connectionList.get( index );
  }

  public synchronized SocketServerConnection getConnection( String connectionID ) {
    for (int i = 0; i < connectionList.size(); i++) {
      SocketServerConnection conn = connectionList.get( i );
      if (conn.getConnectionID().equals( connectionID ))  {
        return conn;
      }
    }
    return null;
  }

  public synchronized void start() throws IOException {
    active = true;
    socketThread = new Thread( this );
    socketThread.start();
  }

  public synchronized void stop() {
    Logger.info( "SocketServer on port="+port+" stopping..." );
    active = false;
    for (int i = connectionList.size()-1; i >= 0; i--) {
      SocketServerConnection conn = connectionList.get( i );
      conn.close();
    }
    try {
      if (serverSocket != null) {
        serverSocket.close();
      }
    }
    catch (IOException e) {
      // do nothing
    }
    // make sure no connection remains
    connectionList.clear();
    serverSocket = null;
    Logger.info( "Socket server closed for port="+port );
    while (running) {
      try {
        Thread.sleep( 1000 );
      }
      catch (InterruptedException e) {
        // do nothing
      }
      if (socketThread != null) {
        socketThread.interrupt();
      }
    }
  }

  public boolean isRunning() {
    return running;
  }

  @Override
  public void run() {
    running = true;
    while (active) {
      if (serverSocket == null) {
        //--- initialize server socket
        try {
          serverSocket = new ServerSocket( port );
          //server.setSoTimeout( TIMEOUT );
          Logger.info( "Waiting for socket connections on port="+port );
        }
        catch (IOException ex) {
          Logger.error( "Error connecting to socket on port=" + port, ex );
        }
      }

      if (serverSocket == null) {
        //--- Pause before retry
        try {
          Thread.sleep( SOCKET_INIT_RETRY_MILLIS );
        }
        catch (InterruptedException e) {
          // do nothing
        }
      }
      else {
        //--- Wait for client connections
        try {
          Socket socket = serverSocket.accept();
          synchronized (this) {
            String connectionID = "Socket_" + port + "[" + NEXT_CONNECTION_ID + "]";
            NEXT_CONNECTION_ID++;
            StreamHandler streamHandler = streamHandlerFactory.createStreamHandler( connectionID );
            SocketServerConnection conn = new SocketServerConnection( this, socket, connectionID, streamHandler );
            connectionList.add( conn );
            conn.start();
            Logger.info( "IP Connection successful on port="+port+", ID="+connectionID );
          }
        }
        catch (SocketTimeoutException e) {
          // do nothing
        }
        catch (Exception ex) {
          Logger.error( "Error establishing connection on port="+port, ex );
        }
      }
    }
    Logger.info( "SocketServer on port="+port+" stopped" );
    running = false;
  }

  public synchronized void connectionClosed( SocketServerConnection socketConnection ) {
    synchronized (this) {
      this.connectionList.remove( socketConnection );
      Logger.info( "SocketServer: removed connection ID="+socketConnection.getConnectionID()+ ", remaining="+connectionList.size() );
    }
  }

  public static void main( String[] args ) {
    try {
      int port = Integer.parseInt( args[0] );
      final MessageHandler msgHandler = new MessageHandler() {
        @Override
        public void connected( StreamHandler requestHandler ) {
          System.out.println( "Connected" );
        }

        @Override
        public void disconnected( StreamHandler requestHandler ) {
          System.out.println( "Disconnected" );
        }

        @Override
        public void received( StringBuilder messageBuffer, StreamHandler requestHandler ) {
          System.out.println( "Received: "+messageBuffer.toString() );
          SocketServerConnection serverConnection = (SocketServerConnection) requestHandler.getStreamReceiver().getConnection();
          try {
            OutputStream out = serverConnection.getOutputStream();
            out.write( "pong\n".getBytes() );
            out.flush();
          }
          catch (Exception ex) {
            ex.printStackTrace();
          }
        }
      };
      StreamHandlerFactory streamHandlerFactory = new StreamHandlerFactory() {
        @Override
        public StreamHandler createStreamHandler( String connectionID ) {
          return new MessageSplitter( connectionID, null, "\n", msgHandler );
        }
      };
      SocketServer socketServer = new SocketServer( port );
      socketServer.init( streamHandlerFactory, null  );
      socketServer.start();
      while (true) {
        // do nothing
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
