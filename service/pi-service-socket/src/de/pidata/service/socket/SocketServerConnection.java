/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.socket;

import de.pidata.connect.base.AbstractConnection;
import de.pidata.connect.base.ConnectionListener;
import de.pidata.connect.stream.StreamConnection;
import de.pidata.connect.stream.StreamHandler;
import de.pidata.connect.stream.StreamReceiver;
import de.pidata.log.Logger;
import de.pidata.stream.StreamHelper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by pru on 28.04.17.
 */
public class SocketServerConnection extends AbstractConnection implements StreamConnection {

  public static final int READ_LOOP_IDLE_SLEEP_MILLIS = 50;
  public static final int TIMEOUT_MILLIS = 30000;

  private SocketServer socketServer;
  private Socket socket;
  private StreamHandler streamHandler;
  private String connectionID;
  private InputStream inputStream;
  private OutputStream outputStream;

  public SocketServerConnection( SocketServer socketServer, Socket socket, String connectionID, StreamHandler streamHandler ) {
    this.socketServer = socketServer;
    this.socket = socket;
    this.connectionID = connectionID;
    this.streamHandler = streamHandler;
  }

  public void start() throws IOException {
    resetConnectionSteps();
    InetAddress clientAddr = socket.getInetAddress();
    Logger.info( "SocketServer["+connectionID+"]: connect from address=" + clientAddr.getHostAddress() + ", name=" + clientAddr.getHostName() );
    StreamReceiver streamReceiver = new StreamReceiver( this, READ_LOOP_IDLE_SLEEP_MILLIS, TIMEOUT_MILLIS );

    inputStream = socket.getInputStream();
    setStateConnected();
    streamReceiver.startReceive( clientAddr.getHostAddress(), inputStream, streamHandler );
  }

  /**
   * Called by connectionListener to get all connection steps in order.
   * Implementation has to call connectionListener.addStep() in correct
   * order for each step.
   * <p>
   * The list of steps are exact that steps passed when establishing a
   * connection. While connection each registered listener will be called
   * on success or failure of these steps.
   *
   * @param connectionListener listener to add steps to
   */
  @Override
  public void getConnectionSteps( ConnectionListener connectionListener ) {
    //TODO: connectionSteps
  }

  @Override
  public OutputStream getOutputStream() throws IOException {
    if (outputStream == null) {
      this.outputStream = socket.getOutputStream();
    }
    return outputStream;
  }

  /**
   * Returns unique ID for this connection, e.g. URL plus counter
   *
   * @return unique ID for this connection
   */
  @Override
  public String getConnectionID() {
    return connectionID;
  }

  /**
   * Closes this connection.
   */
  @Override
  public void close() {
    Logger.info( "SocketServer["+connectionID+"]: closing connection" );
    StreamHelper.close( inputStream );
    inputStream = null;
    StreamHelper.close( outputStream );
    outputStream = null;
    if (socket != null) {
      try {
        socket.close();
      }
      catch (IOException e) {
        // ignore errors
      }
    }
    socket = null;
    setStateDisconnected();
    socketServer.connectionClosed( this );
  }
}
