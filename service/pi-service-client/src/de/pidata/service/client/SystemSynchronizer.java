/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.client;

import de.pidata.comm.client.base.Sender;
import de.pidata.comm.client.base.SoapSender;
import de.pidata.comm.soap.Body;
import de.pidata.comm.soap.Envelope;
import de.pidata.comm.soap.Fault;
import de.pidata.comm.soap.Header;
import de.pidata.comm.soap.SoapFactory;
import de.pidata.log.Logger;
import de.pidata.service.base.ServiceException;
import de.pidata.models.service.ServiceManager;
import de.pidata.models.tree.*;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.models.xml.binder.XmlWriter;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.service.log.LogEntry;
import de.pidata.service.log.LogFactory;
import de.pidata.service.log.LogManager;
import de.pidata.service.log.LogState;
import de.pidata.service.log.MessageType;
import de.pidata.service.log.SyncListener;
import de.pidata.connect.base.ConnectionController;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.BackgroundSynchronizer;
import de.pidata.system.base.SystemManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;
import java.util.Vector;

/**
 * REQUIRED INITIALIZED:
 *     de.slab.comm.client.Communication
 *     de.slab.models.system.base.SystemReader
 *     de.slab.models.system.base.Storage
 */
public class SystemSynchronizer extends BackgroundSynchronizer {

  private static final boolean DEBUG = false;
  
  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.pidata.de/res/service/log" );

  public static final QName SYNC_AUTO       = NAMESPACE.getQName("auto");
  public static final QName SYNC_CYCLIC     = NAMESPACE.getQName("cyclic");
  public static final QName SYNC_MANUAL     = NAMESPACE.getQName("manual");
  public static final QName SYNC_ACTIVESYNC = NAMESPACE.getQName("ActiveSync");

  private static final int DEFAULT_INTERVAL = 60;
  private static final int DEFAULT_CONNECT_RETRIES = 3;
  private static final int DEFAULT_DISCONNECT_INTERVAL = 3600;

  private static Hashtable instanceMap = new Hashtable();

  protected String targetID = null;

  private Model statusModel;
  private QName state;
  private boolean running = false;

  private SyncListener syncListener;
  private int synchInterval;
  private int disconnectInterval;
  private Sender comm;
  private int connectRetries;
  private int connectRetryCount;

  private boolean checkPendingLogs;
  private boolean defaultCheckPendingLogs;
  private boolean externalSync = false;
  private QName syncMode = SYNC_AUTO;

  private int lastServerLogIndex = Integer.MAX_VALUE;

  public SystemSynchronizer() {
    super();
    init();
    this.checkPendingLogs = this.defaultCheckPendingLogs;
  }

  public SyncListener getSyncListener() {
    return syncListener;
  }

  public void setSyncListener(SyncListener syncListener) {
    this.syncListener = syncListener;
  }

  /**
   * Set default synchronization behaviuor. Either sync is only started, with pending
   * logs on client side (true) or a sync request is sent in any case (false).
   *
   * @param check
   */
  public synchronized void setDefaultCheckPendingLogs( boolean check ) {
    defaultCheckPendingLogs = check;
  }

  /*
  // TODO: support multiple servers --> make targeID configurable
  protected SystemSynchronizer( int interval, QName targetID ) {
    init();
  }
  */


  @Override
  public Model getStatusModel() {
    if (statusModel == null) {
      ModelFactoryTable.getInstance().getOrSetFactory( LogFactory.NAMESPACE, LogFactory.class );
      statusModel = new DefaultModel( LogFactory.ID_STATUS_MODEL, LogFactory.STATUS_MODEL_TYPE, null, null, null );
      //context.getDataRoot().add( statusModel.type().name(), statusModel );
    }
    return statusModel;
  }

  /**
   * Retunrs ID of attribute used to identify BackgroundSynchronizer's state within
   * model returned by getStatusModel()
   *
   * @return state attribute ID
   */
  @Override
  public QName getStateAttrID() {
    return LogFactory.ID_STATE;
  }

  /**
   * Retruns true if stateValue is identifying an active state of state attribute.
   *
   * @param stateValue the state value to check
   * @return true for an active state
   */
  @Override
  public boolean isStateActive( QName stateValue ) {
    return Sender.isStateActive( stateValue );
  }

  /**
   * Synchronize service logs with server. This method uses the default
   * values for checking of pending logs and request mode.
   */
  public void synchronize() {
    synchronize( defaultCheckPendingLogs );
  }


  /**
   * Synchronize service logs with server.
   *
   * @param checkPendingLogs - <code>true</code>: synchronize only when pending service logs are present
   */
  @Override
  public void synchronize( boolean checkPendingLogs ) {
    synchronized(this) {
      this.checkPendingLogs = checkPendingLogs;
      if (running) {
        externalSync = true;
        if ((syncMode == SYNC_AUTO) || (syncMode == SYNC_CYCLIC)) {
          connectRetryCount = connectRetries;
        }
        this.notify();
      }
    }
    if (!running) {
      doSynchronize();
    }
  }

  private void processServerLogState(LogState serverLogState) throws IOException {
    LogManager logManager = LogManager.getInstance( targetID );
    if (serverLogState.getSync().booleanValue()) {
      String currentSyncTime = serverLogState.getLastSyncTime();
      saveCurrentSyncTime(logManager, currentSyncTime);

      Integer rsp = serverLogState.getReceived();
      Logger.debug( "remote processed: " + rsp );
      if (rsp != null) {
        logManager.confirmReceival(rsp.intValue());
      }
      lastServerLogIndex = serverLogState.getNext().intValue() - 1;

      //----- Allow repeat of last message from a given byte offset
      String repeatName = serverLogState.getRepeatName();
      if (repeatName != null) {
        int repeatIndex = serverLogState.getRepeatIndex().intValue();
        InputStream repeatStream = null;
        Envelope reply;
        Logger.debug( "begin repeat name="+repeatName+" from index=" + repeatIndex );
        try {
          repeatStream = logManager.readBuffer( repeatName );
          SystemManager sysMan = SystemManager.getInstance();
          Context context = sysMan.createContext();
          reply = (Envelope) comm.repeat( context, repeatName, repeatStream, repeatIndex );
        }
        catch (ServiceException ex) {
          String msg = "ServiceException while syncing";
          Logger.error( msg, ex );
          throw new IOException( msg );
        }
        finally {
          StreamHelper.close( repeatStream );
        }

        if (reply != null) {
          processResponse( reply );
        }
        logManager.removeBuffer( repeatName );
      }
    }
    else {
      processOutOfSync(serverLogState);
    }
  }

  private void processOutOfSync(LogState serverLogState)
  throws IOException {
    Logger.warn( "server and client are out of sync" );
    setState(Sender.STATE_PROCESS);

    LogManager logManager = LogManager.getInstance( targetID );
    String lastSyncTime = logManager.getLastSyncTime();
    String currentSyncTime = serverLogState.getLastSyncTime();

    logManager.outOfSync();
    if (syncListener != null) {
      this.syncListener.outOfSync(logManager, lastSyncTime, currentSyncTime);
    }

    saveCurrentSyncTime(logManager, currentSyncTime);
  }


  private void processServerLog(LogEntry logEntry) throws IOException {
    Model input;
    setState( Sender.STATE_PROCESS );

    LogManager logManager = LogManager.getInstance( targetID );
    ServiceManager serviceManager = ServiceManager.getInstance( targetID );
    int logIndex = logEntry.getSEQ().intValue();

    if (logIndex > logManager.getSeqReceived()) {

      logManager.confirmProcessing(logIndex);
      setState( Sender.NAMESPACE.getQName(Sender.STATE_PROCESS.getName()+" "+logIndex) );
      try {
        //TODO wenn jetzt der processLog abstürzt ist logEntry für uns verloren, aber das System lebt weiter
        if (DEBUG) {
          StringBuilder buf = new XmlWriter(null).writeXML(logEntry, logEntry.type().name());
          Logger.debug("Received Log:\n"+buf);
        }
        QName opID = logEntry.getOperation();
        QName serviceID = logEntry.getService();
        //TODO Context sinnvoll initialisieren
        MessageType message = logEntry.getInput();
        ModelIterator iter = message.iterator(null, null);
        if (iter.hasNext()) input = iter.next();
        else input = null;
        Model result = serviceManager.invokeService( SystemManager.getInstance().createContext(), serviceID, opID, input );
        //TODO result als result kennzeichnen
        if (result != null) {
          LogManager.createLogEntry(serviceID, opID, result);
        }
      }
      catch (Exception ex) {
        Logger.error("Could not process log index="+logIndex, ex);
      }
    }
  }

  /**
   * Handle a result which contains a fault.
   * If the fault respresents an out of sync, process the out of sync operations.
   * Otherwise there is is a major problem, so reset the log state to disconnected.
   * @param serverLogState
   * @param fault
   * @throws IOException
   */
  private void processFault(LogState serverLogState, Fault fault) throws IOException {
    String faultCode = fault.getFaultcodeStr();
    if (faultCode == null) {
      Logger.warn( "Server returned a fault without fault information" );
      setState(Sender.STATE_SERVER_ERROR);
    }
    else {
      if (faultCode.toUpperCase().equals(LogManager.OUT_OF_SYNC)) {
        processOutOfSync(serverLogState);
      }
      else{
        Logger.warn( "Server returned a fault: "+faultCode.toUpperCase() );
        setState(Sender.STATE_SERVER_ERROR);
      }
    }
  }

  public boolean processResponse(Envelope response) throws IOException {
    boolean syncReceived = false;
    boolean success = true;

    LogState serverLogState;
    if ((response != null) && (response instanceof Envelope)) {
      Envelope envelope = (Envelope) response;

      //----- Process header: must contain server's log state
      Header header = envelope.getHeader();
      if (header == null) {
        throw new IOException("Envelope did not have a header");
      }
      ModelIterator stateIter = header.iterator(LogState.TYPE.name(), null);
      if (stateIter.hasNext()) {
        serverLogState = (LogState) stateIter.next();
        processServerLogState(serverLogState);
        syncReceived = true;
      }
      else {
        throw new IOException("Header did not contain server's log state");
      }

      Body body = envelope.getBody();
      for (ModelIterator bodyIter = body.iterator(null, null); bodyIter.hasNext(); ) {
        Model log = bodyIter.next();
        if (log instanceof Fault) {
          processFault(serverLogState, (Fault) log);
          setState( Sender.STATE_SERVER_ERROR );
          success = false;
        }
        else if (log instanceof LogState) {
          processServerLogState((LogState) log);
          syncReceived = true;
        }
        else if (log instanceof LogEntry) {
          if (syncReceived) {
            processServerLog((LogEntry) log);
          }
          else {
            throw new IOException("Must receive LogState before first LogEntry!");
          }
        }
      }
      if (success) setState( Sender.STATE_IDLE );
    }
    else {
      Logger.warn( "Server reply was not a SOAP envelope: "+response );
      setState(Sender.STATE_DISCONNECTED);
      success = false;
    }
    return success;
  }

  private void synchronizeLogManager( LogManager logManager, Context context ) throws IOException {
    Envelope envelope = createEnvelope( logManager.createLogState( true ), context );
    try {
      doSend( envelope, null, null );
    }
    catch (ServiceException ex) {
      String msg = "ServiceException while syncing";
      Logger.error( msg, ex );
      throw new IOException( msg );
    }
  }

  private Envelope createEnvelope( LogState logState, Context context ) {
    Envelope envelope = Envelope.createEnvelope();
    Header header = envelope.getHeader();
    if (logState != null) {
      if (header == null) {
        header  = (Header) ModelFactoryTable.getInstance().getFactory( SoapFactory.NAMESPACE ).createInstance( SoapFactory.HEADER_TYPE );
        envelope.setHeader(header);
      }
      String clientID =  context.getClientID();
      logState.setSystemID( LogFactory.NAMESPACE.getQName( clientID ) );
      header.add(LogState.TYPE.name(), logState);
    }
    return envelope;
  }

  private void doSynchronize() {
    LogManager logManager = LogManager.getInstance( targetID );
    if (checkPendingLogs && !logManager.hasPendingLogs()) {
      // We have to exit here to avoid opening a connection even if nothing has to be done
      return;
    }

    setState( Sender.STATE_BUSY );
    SystemManager systemManager = SystemManager.getInstance();
    ConnectionController connectionController = systemManager.getConnectionController();
    if (connectionController == null) {
      Logger.warn( "No connection controller. cannot synchronize." );
      setState( Sender.STATE_DISCONNECTED );
      return;
    }

    Context context = systemManager.createContext();
    if (!connectionController.isConnected()) {
      try {
        context.setClientID( connectionController.connect() );
      }
      catch (Exception e) {
        Logger.error( "could not connect log manager. '" + e.getMessage() + "'", e);
        setState( Sender.STATE_DISCONNECTED );
        return;
      }
    }
    if (!connectionController.isConnected()) {
      setState( Sender.STATE_DISCONNECTED );
      return;
    }

    // Hook for activities just before sync starts
    if (this.syncListener != null) {
      this.syncListener.beforeSync( context, logManager );
    }

    // check if log manager's log index is synchronized with server
    Logger.info( "synchronizing log manager." );
    setState( Sender.STATE_RECOVERING );
    try {
      synchronizeLogManager( logManager, context );
      setState( Sender.STATE_IDLE );
    }
    catch (Exception e) {
      Logger.error( "could not synchronize log manager. '" + e.getMessage() + "'", e);
      setState( Sender.STATE_DISCONNECTED );
      return;
    }

    try {
      boolean success = true;
      int maxLogsPerEnvelope = SystemManager.getInstance().getPropertyInt( SoapSender.PROP_HEADER_MAX_LOGS, Integer.MAX_VALUE );
      while ((logManager.hasPendingLogs() || (lastServerLogIndex > logManager.getSeqReceived())) && success) {
        setState( Sender.STATE_BUSY );

        Envelope envelope;
        synchronized(this) {
          envelope = createEnvelope( logManager.createLogState( true ), context );
          Vector logs = logManager.getPendingLogs();
          Model body = envelope.getBody();
          for (int i = 0; (i < logs.size()) && (i < maxLogsPerEnvelope); i++) {
            LogEntry log = (LogEntry) logs.elementAt(i);
            body.add(LogEntry.TYPE.name(), log);
          }
        }

        String repeatName = DateTimeType.toTimeString( new DateObject( DateTimeType.TYPE_DATETIME, System.currentTimeMillis()) );
        repeatName = repeatName.replace(':', '_');


        OutputStream bufStream = null;
        try {
          bufStream = logManager.writeBuffer( repeatName );
          success = doSend(envelope, repeatName, bufStream);
        }
        finally {
          StreamHelper.close( bufStream );
        }
        logManager.removeBuffer( repeatName );
      }
      setState( Sender.STATE_IDLE );
    }
    catch (IOException e) {
      Logger.warn( "Sender error: '" + e.getMessage() + "'");
      setState( Sender.STATE_DISCONNECTED );
    }
    catch (Exception e) {
      Logger.error( "Sender error: '" + e.getMessage() + "'" , e);
      setState( Sender.STATE_DISCONNECTED );
    }

    // Hook for activities just after sync finished
    if (this.syncListener != null) {
      this.syncListener.afterSync( context, logManager );
    }
  }

  private boolean doSend(Envelope envelope, String repeatName, OutputStream repeatBuffer) throws IOException, ServiceException {
    Envelope response;
    synchronized(comm) {
      SystemManager sysMan = SystemManager.getInstance();
      Context context = sysMan.createContext();
      response = comm.invokeSynchron( LogManager.SERVICE_LOG_MANAGER, LogManager.OP_SYNC, context, envelope, repeatName, repeatBuffer );
    }
    return processResponse(response);
  }

  private synchronized void saveCurrentSyncTime(LogManager logManager, String currentSyncTime) throws IOException {
    Logger.debug("Received syncTime="+currentSyncTime);
    if (currentSyncTime != null) {
      logManager.setLastSyncTime(currentSyncTime);
      logManager.setInSync();
    }
  }

  public QName getSyncState() {
    return state;
  }

  public void run() {
    running = true;
    while (true) {
      try {
        synchronized(this) {
          externalSync = false;

          if ((syncMode == SYNC_AUTO) || (syncMode == SYNC_CYCLIC)) {
            if (connectRetryCount > 0) {
              for (int i = 0; i < synchInterval; i++) {
                wait(1000);
                if (externalSync) break;
              }
            }
            else {
              setState(Sender.STATE_AUTO_OFF);
              for (int i = 0; i < disconnectInterval; i++) {
                wait(1000);
                if (externalSync) break;
              }
              connectRetryCount = connectRetries;
            }
          }
          else if (syncMode == SYNC_MANUAL) {
            while (!externalSync) {
              wait(1000);
            }
          }
          else if (syncMode == SYNC_ACTIVESYNC) {
            throw new IllegalArgumentException("syncModel 'ActiveSync' currently not supported");
            /*
            while ((!externalSync) && (!syncFile.exists())) {
              wait(synchInterval*1000);
            }
            if (syncFile.exists()) {
              Logger.debug("Found sync file: "+syncFile.getAbsolutePath());
              if (!syncFile.delete()) {
                Logger.warn("Could not delete sync file: "+syncFile.getAbsolutePath());
              }
            }
            */
          }
        }
        doSynchronize();
        synchronized(this) {
          this.checkPendingLogs = this.defaultCheckPendingLogs;
        }
      }
      catch (InterruptedException e) {
        // nothing to be done.
      }
    }
  }


  /**
   * Set synchronization interval in seconds.
   * @param sec
   */
  protected void setSynchInterval( int sec ) {
    synchInterval = sec;
  }

  private synchronized void setState(QName state) {
    if ((syncMode == SYNC_AUTO) || (syncMode == SYNC_CYCLIC)) {
      if (state == Sender.STATE_DISCONNECTED) {
        connectRetryCount--;
       //TODO Zeitpunkt des nächsten connect-versuchs ausgeben
      }
    }
    this.state = state;
    getStatusModel().set( LogFactory.ID_STATE, state );
  }

  private void init() {
    String syncModeStr;
    SystemManager sysMan = SystemManager.getInstance();

    setState( Sender.STATE_IDLE );

    this.comm = Sender.initComm();
    this.comm.setStateModel( getStatusModel(), LogFactory.ID_STATE );

    syncModeStr = sysMan.getProperty("sync.mode", null );
    if ((syncModeStr != null) && (syncModeStr.length() > 0)) {
      this.syncMode = NAMESPACE.getQName(syncModeStr);
    }
    synchInterval = sysMan.getPropertyInt("syncInterval", DEFAULT_INTERVAL);
    if ((syncMode == SYNC_AUTO) || (syncMode == SYNC_CYCLIC)) {
      connectRetries = sysMan.getPropertyInt("connectRetries", DEFAULT_CONNECT_RETRIES);
      connectRetryCount = connectRetries;
      disconnectInterval = sysMan.getPropertyInt("disconnectInterval", DEFAULT_DISCONNECT_INTERVAL);
      defaultCheckPendingLogs = (syncMode == SYNC_AUTO);
      Logger.info("SystemSynchronizer.init: sync.mode="+syncMode.getName()+", syncInterval="+synchInterval+", connectRetries="+connectRetries
                         + ", disconnectInterval="+disconnectInterval);
      if ( !(LogManager.getInstance( null ).isSynchronized()) ) {
        synchronize();
      }
    }
    else if (syncMode == SYNC_MANUAL) {
      Logger.info("SystemSynchronizer.init: sync.mode=manual");
    }
    else if (syncMode == SYNC_ACTIVESYNC) {
      throw new IllegalArgumentException("syncModel 'ActiveSync' currently not supported");
      /*
      syncFile = new File(sysMan.getSystemProperty("sync.event.file"));
      this.defaultCheckPendingLogs = false;
      Logger.info("SystemSynchronizer.init: sync.mode=ActiveSync, syncInterval="+synchInterval+", sync.event.file="+syncFile.getAbsolutePath());
      */
    }
    else {
      throw new RuntimeException("Invalid sync mode: "+syncModeStr);
    }
  }

}
