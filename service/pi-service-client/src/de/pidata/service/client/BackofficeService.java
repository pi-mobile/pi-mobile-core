/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.client;

import de.pidata.models.service.Operation;
import de.pidata.service.base.ServiceException;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.Model;
import de.pidata.service.log.LogEntry;
import de.pidata.service.log.LogManager;
import de.pidata.system.base.BackgroundSynchronizer;
import de.pidata.wsdl.WsdlService;

public class BackofficeService extends WsdlService {

  protected Model invoke( Context caller, Operation operation, Model input) throws ServiceException {
    try {
      // todo test mit altem client! QName messageName = operation.getInput().getMessage();
      LogEntry log = LogManager.createLogEntry( portType().getName(), operation.getName(), input);
      LogManager.getInstance(null).addLog(log);
    }
    catch (Exception ex) {
      throw new ServiceException(ServiceException.SERVICE_FAILED, "Service failed: opID="+operation.getName(), ex);
    }
    BackgroundSynchronizer.getInstance().synchronize();
    return null;
  }
}
