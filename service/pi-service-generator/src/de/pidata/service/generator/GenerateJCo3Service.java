/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.generator;

import de.pidata.comm.soap.SoapFactory;
import de.pidata.models.generator.Schema2Class;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.xml.schema.RootSchema;
import de.pidata.models.xml.schema.Schema;
import de.pidata.models.xml.schema.SchemaFactory;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.system.desktop.DesktopSystem;
import de.pidata.wsdl.*;

import java.util.Enumeration;

/**
 * Created by pru on 16.01.15.
 */
public class GenerateJCo3Service extends GeneratePortType {

  public GenerateJCo3Service( TDefinitions wsdlDef, QName portTypeName, String packageName, String dataobjectsPackage,
                              String implementsInterface, String outputDirName ) throws ClassNotFoundException {
    super(portTypeName.getName() + "SAP", portTypeName.getNamespace(), packageName, null, outputDirName, wsdlDef);
    writeNamespaces = false;
    addImplements( implementsInterface, true );
    addImport( dataobjectsPackage+".*" );
    addImport( "de.pidata.service.sapjco3.JCoService" );
    setExtension( "JCoService" );
    portType = (TPortType) wsdlDef.get(TDefinitions.ID_PORTTYPE, portTypeName);
    if (portType == null) {
      throw new IllegalArgumentException("Port type name="+portTypeName+" not found in WSDL file");
    }
    new WsdlServiceInstanceMethod(this, "getInstance", portType );
    for (ModelIterator opIter = portType.operationIter(); opIter.hasNext(); ) {
      TOperation op = (TOperation) opIter.next();
      new WsdlJCo3Method(this, op);
    }
    for (Enumeration typeEnum = returnTypes.elements(); typeEnum.hasMoreElements(); ) {
      ComplexType returnType = (ComplexType) typeEnum.nextElement();
      createReturnClass(returnType, packageName, outputDirName);
    }
  }

  private static void printUsage() {
    System.out.println("Usage: GenerateSAPService wsdlFile portTypeName srcDir destinationPackageName dataobjectsPackage implementsInterface {factoryClass}");
  }

  public static void main(String[] args) {
    try {
      if (args.length < 6) {
        printUsage();
        System.exit(0);
      }
      String packageName = args[3];
      String dirName = args[2];

      new DesktopSystem();
      if (ModelFactoryTable.getInstance().getFactory( SchemaFactory.NAMESPACE ) == null) new SchemaFactory();
      if (ModelFactoryTable.getInstance().getFactory( WSDLFactory.NAMESPACE ) == null) new WSDLFactory();
      if (ModelFactoryTable.getInstance().getFactory( SoapFactory.NAMESPACE ) == null) new SoapFactory();

      for (int i = 6; i < args.length; i++) {
        String factoryClassName = args[i];
        Class.forName( factoryClassName ).newInstance();
      }

      TDefinitions wsdlDef = WsdlLoader.loadWSDL( args[0] );
      if (!WsdlLoader.validate(wsdlDef)) {
        System.exit(1);
      }

      for (ModelIterator typesIter = wsdlDef.typesIter(); typesIter.hasNext(); ) {
        TTypes types = (TTypes) typesIter.next();
        for (ModelIterator iter = types.iterator(null, null); iter.hasNext(); ) {
          Model model = iter.next();
          if (model instanceof Schema) {
            new Schema2Class((RootSchema) model, packageName, "name", dirName );
          }
          else {
            throw new IllegalArgumentException("Unsupported child inside types definition: typeName="+model.type().name());
          }
        }
      }
      String dataobjectsPackage = args[4];

      Namespace targetNS = Namespace.getInstance(wsdlDef.getTargetNamespace());
      String implementsInterface = args[5];

      GenerateJCo3Service genProxy = new GenerateJCo3Service(wsdlDef, targetNS.getQName(args[1]), packageName, dataobjectsPackage, implementsInterface, dirName);
      genProxy.generate();
    }
    catch (Exception ex) {
      ex.printStackTrace();
      System.exit(1);
    }
  }
}
