/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.generator;

import de.pidata.models.service.ServiceManager;
import de.pidata.wsdl.TPortType;

import java.io.PrintWriter;

public class WsdlServiceInstanceMethod extends WsdlMethod {

  private TPortType portType;

  public WsdlServiceInstanceMethod(GeneratePortType parentClass, String name, TPortType portType) {
    super(parentClass, name);
    this.portType = portType;
    addImport( ServiceManager.class.getName() );
  }
  protected void writeMethod(PrintWriter pw) {
    String className = parentClass.getName();
    pw.println("  public static " + className + " getInstance() {");
    pw.println("    return (" + className + ") ServiceManager.getInstance().getService(SERVICE);");
    pw.println("  }");
  }


  protected void writeMethodInterface(PrintWriter pw) {
    pw.println("  public static I" + portType.getName().getName() + " getInstance();");
  }

}
