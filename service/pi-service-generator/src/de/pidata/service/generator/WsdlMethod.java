/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.generator;

import de.pidata.comm.soap.Body;
import de.pidata.comm.soap.SoapFactory;
import de.pidata.models.generator.MethodGenerator;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.simple.*;
import de.pidata.qnames.QName;
import de.pidata.service.base.ServiceException;
import de.pidata.wsdl.TMessage;
import de.pidata.wsdl.TOperation;
import de.pidata.wsdl.TParam;
import de.pidata.wsdl.TPart;

import java.io.PrintWriter;
import java.util.Vector;

public abstract class WsdlMethod extends MethodGenerator {

  private Type returnType;
  protected boolean singleReturnValue = false;
  protected Vector inputTypes = new Vector();
  protected Vector inputNames = new Vector();
  protected String inputMsgName;

  public WsdlMethod( GeneratePortType parentClass, String name ) {
    super( parentClass, name );
    setThrowException( "ServiceException" );
    addImport( ServiceException.class.getName() );
    addImport( QName.class.getName() );
    addImport( Context.class.getName() );
  }

  public WsdlMethod( GeneratePortType parentClass, TOperation op ) {
    super( parentClass, op.getName().getName() );
    setThrowException( "ServiceException" );
    QName msgName;

    TParam input = op.getInput();
    if (input != null) {
      msgName = input.getMessage();
      if (msgName != null) {
        inputMsgName = msgName.getName();
        TMessage msgDef = parentClass.wsdlDef.findMessageDef( msgName );
        if (msgDef == null) {
          throw new IllegalArgumentException( "Input message definition not found: name=" + msgName );
        }
        extractMsgParts( msgDef, inputNames, inputTypes );
      }
    }

    TParam output = op.getOutput();
    if (output != null) {
      msgName = output.getMessage();
      if (msgName != null) {
        TMessage msgDef = parentClass.wsdlDef.findMessageDef( msgName );
        if (msgDef == null) {
          throw new IllegalArgumentException( "Output message definition not found: name=" + msgName );
        }
        if (msgDef.partCount() == 1) {
          TPart part = msgDef.getPart( null );
          returnType = getType( part );
          singleReturnValue = true;
        }
        else {
          returnType = SoapFactory.BODY_TYPE;
        }
        parentClass.addImport( returnType.getValueClass().getName() );
      }
    }
  }

  public Type getReturnType() {
    return this.returnType;
  }

  protected void extractMsgParts( TMessage msgDef, Vector inputNames, Vector inputTypes ) {
    TPart part;
    Type type;
    for (ModelIterator partIter = msgDef.partIter(); partIter.hasNext(); ) {
      part = (TPart) partIter.next();
      type = getType( part );
      inputNames.addElement( part.getName().getName() );
      inputTypes.addElement( type );
      this.parentClass.addImport( type.getValueClass().getName() );
    }
  }

  protected Type getType( TPart part ) {
    QName typeName;
    Type type;
    typeName = part.getType();
    if (typeName == null) {
      typeName = part.getElement();
      type = ModelFactoryTable.getInstance().getFactory( typeName.getNamespace() ).getRootRelation( typeName ).getChildType();
    }
    else {
      type = ModelFactoryTable.getInstance().getFactory( typeName.getNamespace() ).getType( typeName );
    }
    return type;
  }

  protected String getReturnTypeName() {
    if (returnType == null) return "void";
    else return getClassForType( returnType );
  }

  protected void writeMethodInterface( PrintWriter pw ) {
    writeMethodDeclaration( pw );
    pw.println( ";" );
  }

  protected void writeMethodDeclaration( PrintWriter pw ) {
    //----- Method header
    String returnType = getReturnTypeName();
    pw.print( "  public " + returnType + " " + getName() + "( Context caller" );
    for (int i = 0; i < inputNames.size(); i++) {
      String typeName = getClassForType( (Type) inputTypes.elementAt( i ) );
      String varName = inputNames.elementAt( i ).toString().toLowerCase();
      pw.print( ", " + typeName + " " + varName );
    }
    pw.print( " )" );
    if (hasThrowException()) {
      pw.print( " throws " + getThrowException() );
    }
  }

  protected String getSimpleTypeDef( SimpleType type ) {
    String typeName = getClassForType( type );
    if (typeName.equals( "QName" )) {
      addImport( QNameType.class.getName() );
      return "QNameType.getInstance()";
    }
    else if (typeName.equals( "Integer" )) {
      addImport( IntegerType.class.getName() );
      return "IntegerType.getDefInt()";
    }
    else if (typeName.equals( "Long" )) {
      addImport( IntegerType.class.getName() );
      return "IntegerType.getDefLong()";
    }
    else if (typeName.equals( "DecimalObject" )) {
      addImport( DecimalType.class.getName() );
      return "DecimalType.getDefault()";
    }
    else if (typeName.equals( "DateObject" )) {
      return "DateTimeType.getDefDateTime()";
    }
    else {
      addImport( StringType.class.getName() );
      return "StringType.getDefString()";
    }
  }
}
