/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.generator;

import de.pidata.comm.soap.Body;
import de.pidata.log.Logger;
import de.pidata.models.service.Operation;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.tree.SimpleModel;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;
import de.pidata.system.base.SystemManager;
import de.pidata.wsdl.TMessage;
import de.pidata.wsdl.TOperation;
import de.pidata.wsdl.TParam;
import de.pidata.wsdl.TPart;
import de.pidata.wsdl.TPortType;

import java.io.PrintWriter;
import java.util.Vector;

import static de.pidata.models.cppgenerator.CppCodeGenerator.ensureNonColidingMemberName;

public class WsdlStubMethod extends WsdlMethod {

  private TPortType portType;

  public WsdlStubMethod( GeneratePortType parentClass, String name, TPortType portType ) {
    super( parentClass, name );
    addImport( SystemManager.class.getName() );
    addImport( Context.class.getName() );
    addImport( Model.class.getName() );
    addImport( Operation.class.getName() );
    addImport( Logger.class.getName() );
    this.portType = portType;
  }

  protected void writeMethod(PrintWriter pw) {
    pw.println("  protected Model invoke(Context caller, Operation operation, Model input) throws ServiceException {");
    pw.println("    Model temp;");
    pw.println("    QName opName = operation.getName();");
    pw.println("    try {");
    pw.println("      Operation op = portType().getOperation( opName );" );

    for (ModelIterator opIter = portType.operationIter(); opIter.hasNext(); ) {
      TOperation op = (TOperation) opIter.next();
      String opID = "OP_" + op.getName().getName().toUpperCase();
      pw.println("      if (opName == " + opID + ") {");
      QName msgName;
      TParam input = op.getInput();
      TParam output = op.getOutput();
      Vector inputTypes = new Vector();
      Vector inputNames = new Vector();
      if (input != null) {
        msgName = input.getMessage();
        if (msgName != null) {
          TMessage msgDef = op.getDefinitions().findMessageDef(msgName);
          if (msgDef == null) {
            throw new IllegalArgumentException("Message definition not found: name="+msgName);
          }
          extractMsgParts(msgDef, inputNames, inputTypes);
        }
      }
      for(int i = 0; i < inputNames.size(); i++) {
        String partName = inputNames.elementAt(i).toString();
        Type inType = (Type) inputTypes.elementAt(i);
        String partType = getClassForType( inType );
        pw.println("        QName " + partName + "Name = NAMESPACE.getQName(\"" + partName + "\");");
        if (inType instanceof SimpleType) {
          pw.println("        temp = op.fetchInputPart( input, " + partName + "Name);");
          pw.println("        " + partType + " " + partName + ";");
          pw.println("        if (temp == null) " + partName + " = null;");
          pw.println("        else " + partName + " = (" + partType + ") temp.getContent();");
        }
        else {
          pw.println("        " + partType + " " + partName + " = (" + partType + ") op.fetchInputPart( input, " + partName + "Name);");
        }
      }
      TMessage msgDef = null;
      Type returnType = null;
      String returnPartName = null;
      if (output != null) {
        msgName = output.getMessage();
        if (msgName == null) {
          throw new IllegalArgumentException("Output message has no name, op name="+op.getName());
        }
        msgDef = op.getDefinitions().findMessageDef(msgName);
        if (msgDef == null) {
          throw new IllegalArgumentException("Message definition not found: name="+msgName);
        }
        if (msgDef.partCount() == 1) {
          TPart part = msgDef.getPart( null );
          returnType = getType( part );
          returnPartName = part.getName().getName();
          if (returnType instanceof SimpleType) {
            pw.print("        "+getClassForType(returnType)+" result = ");
          }
          else {
            pw.print("        Model result = ");
          }
        }
        else {
          pw.print("        Body result = ");
        }
      }
      else {
        pw.print("        ");
      }
      pw.print( ensureNonColidingMemberName( op.getName().getName() ) + "( caller" );
      for(int i = 0; i < inputNames.size(); i++) {
        pw.print(", ");
        pw.print(inputNames.elementAt(i).toString() + " ");
      }
      pw.println(");");
      if (output != null) {
        if (msgDef.partCount() == 1) {
          addImport( Body.class.getName() );
          pw.print("        Body resultBody = new Body();\n");
          pw.print("        resultBody.add( NAMESPACE.getQName(\"" + returnPartName + "\"), ");
          if (returnType instanceof SimpleType) {
            addImport( SimpleModel.class.getName());
            pw.print("new SimpleModel("+ getSimpleTypeDefinition((SimpleType) returnType, parentClass.getTargetNamespace(), null) + ", result ));\n");
          }
          else {
            pw.print("result.clone( null, true, false ) );\n");
          }
          pw.print("        return resultBody;\n");
        }
        else {
          pw.println("        return result;");
        }
      }
      else {
        pw.println("        return null;");
      }
      pw.println("      }");
    }

    pw.println("    }");
    pw.println("    catch (Exception ex) {");
    pw.println("      Logger.error(\"Error executing Service\", ex);");
    pw.println("      throw new ServiceException(ServiceException.SERVICE_FAILED, \"Error executing operation name=\"+opName);");
    pw.println("    }");
    pw.println("    throw new ServiceException(ServiceException.UNKNOWN_OPERATION, \"Unsupported operation, opName=\"+opName);");
    pw.println("  }");
  }

  protected void writeMethodInterface(PrintWriter pw) {
    pw.println("  protected Model invoke( Context caller, Operation operation, Model input ) throws ServiceException;");
  }

}
