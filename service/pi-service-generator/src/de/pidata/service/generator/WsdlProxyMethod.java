/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.generator;

import de.pidata.comm.soap.Body;
import de.pidata.models.service.Operation;
import de.pidata.models.service.ServiceManager;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.DefaultModel;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.SimpleModel;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.QName;
import de.pidata.service.base.ServiceException;
import de.pidata.wsdl.TOperation;

import java.io.PrintWriter;

public class WsdlProxyMethod extends WsdlMethod {

  public WsdlProxyMethod(GeneratePortType parentClass, TOperation op) {
    super(parentClass, op);
    addImport( ComplexType.class.getName() );
    addImport( QName.class.getName() );
    addImport( ServiceManager.class.getName() );
    addImport( ServiceException.class.getName() );
    addImport( Context.class.getName() );
    addImport( Model.class.getName() );
    addImport( DefaultModel.class.getName() );
    addImport( Operation.class.getName() );
  }

  protected void writeMethod(PrintWriter pw) {
    writeMethodDeclaration(pw);
    pw.println(" {");

    String opID = "OP_" + getName().toUpperCase();

    //----- Method body
    pw.println("    Operation op = portType().getOperation(" + opID + ");");
    pw.println("    Model params = op.getInputContainer();");

    for (int i = 0; i < inputNames.size(); i++) {
      String partName = inputNames.elementAt(i).toString();
      String partValue;
      Object partType = inputTypes.elementAt(i);
      if (partType instanceof ComplexType) {
        partValue = partName.toLowerCase();
      }
      else if (partType instanceof SimpleType) {
        partValue = "new SimpleModel("+ getSimpleTypeDefinition((SimpleType) partType, parentClass.getTargetNamespace(), null) + ", " + partName.toLowerCase() + ")";
        addImport(SimpleModel.class.getName());
      }
      else {
        throw new RuntimeException("dont know how to generate input part type '" + partType.toString() + "'");
      }
      pw.println("    op.addInputPart( params, NAMESPACE.getQName( \"" + partName + "\" ), " + partValue + ");");
    }
    pw.print("    ");
    if (getReturnType() != null) {
      if (singleReturnValue) {
        addImport( Body.class.getName() );
        pw.print("Body resultBody = (Body) ");
      }
      else {
        pw.print("return (" + getReturnTypeName() + ") ");
      }
    }
    pw.println("invoke( caller, " + opID + ", params);");
    if (singleReturnValue) {
      pw.print("    Model result = resultBody.get( null, null );\n");
      Type returnType = getReturnType();
      if (returnType instanceof SimpleType) {
        pw.print("    return (" + getReturnTypeName() + ") result.getContent();\n");
      }
      else {
        pw.print("    return (" + getReturnTypeName() + ") result;\n");
      }
    }
    pw.println("  }");
  }

}
