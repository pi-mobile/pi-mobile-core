/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.generator;

import de.pidata.log.Logger;
import de.pidata.models.service.ServiceManager;
import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.Model;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;
import de.pidata.wsdl.TOperation;

import java.io.PrintWriter;
import java.util.Stack;

/**
 * Created by pru on 16.01.15.
 */
public class WsdlJCo3Method extends WsdlMethod {

  public WsdlJCo3Method( GeneratePortType parentClass, TOperation op ) {
    super( parentClass, op );
    addImport( ServiceManager.class.getName() );
    addImport( ServiceException.class.getName() );
    addImport( Context.class.getName() );
    addImport( ChildList.class.getName() );
    addImport( SystemManager.class.getName() );
    addImport( "com.sap.conn.jco.*" );
    addImport( Stack.class.getName() );
    addImport( Logger.class.getName() );
  }

  protected void writeMethod(PrintWriter pw) {
    writeMethodDeclaration( pw );
    pw.println( " {" );

    String opID = "OP_" + getName().toUpperCase();
    pw.println( "    Logger.debug( \"Calling \"+" + opID + " );" );
    pw.println( "    JCoDestination connection = jCoAdapter.getSapConnection();" );
    pw.println( "    JCoFunction function = prepareFunction( connection, " + opID + " );" );
    pw.println();

    boolean inputCreated = false;
    for (int i = 0; i < inputNames.size(); i++) {
      String partName = inputNames.elementAt( i ).toString();
      Object partType = inputTypes.elementAt( i );
      if (partType instanceof SimpleType) {
        if (!inputCreated) {
          pw.println( "    JCoParameterList input = function.getImportParameterList();" );
          inputCreated = true;
        }
        String sapParamName = partName.toLowerCase();
        String partTypeDef = getSimpleTypeDef( (SimpleType) partType );
        pw.println( "    input.setValue( \"" + partName + "\", jCoAdapter.value2jco( " + partTypeDef + ", " + sapParamName + "  ) );" );
      }
    }
    pw.println();

    for (int i = 0; i < inputNames.size(); i++) {
      String partName = inputNames.elementAt( i ).toString();
      Object partType = inputTypes.elementAt( i );
      if (partType instanceof ComplexType) {
        pw.println( "    if (" + partName.toLowerCase() + " != null) {" );
        if (((ComplexType) partType).name().getName().endsWith( "_TABLE" )) {
          pw.println( "      jCoAdapter.writeTable( function, \"" + inputNames.elementAt( i ) + "\", " + partName.toLowerCase() + ".iterator( null, null )" + ", new Stack(), new Stack() );" );
        }
        else {
          pw.println( "      jCoAdapter.writeInputRecord( function, \"" + inputNames.elementAt( i ) + "\", " + partName.toLowerCase() + " );" );
        }
        pw.println( "    }" );
      }
    }
    pw.println();

    pw.println( "    jCoAdapter.execute( function, connection );" );

    Type returnType = getReturnType();
    if (returnType != null) {
      pw.println();
      pw.println( "    ChildList outChilds = readOutput( function, " + opID + " );" );
      String returnTypeName = getReturnTypeName();
      if (returnTypeName.equalsIgnoreCase( "body" )) {
        pw.println( "    Body output = new Body( null, null, null, outChilds );" );
        pw.println( "    jCoAdapter.closeSapConnection(connection);" );
        pw.println( "    return output;" );
      }
      else {
        addImport( Model.class.getName() );
        pw.println( "    Model output = outChilds.getFirstChild( null );" );
        pw.println( "    jCoAdapter.closeSapConnection(connection);" );
        if (returnType instanceof ComplexType) {
          pw.println( "    return (" + returnTypeName + ") output;" );
        }
        else {
          pw.println( "    return (" + returnTypeName + ") output.getContent();" );
        }
      }
    }
    pw.println( "  }" );
  }
}
