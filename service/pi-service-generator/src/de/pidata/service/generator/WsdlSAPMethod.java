/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.generator;

import de.pidata.log.Logger;
import de.pidata.models.service.ServiceManager;
import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.Context;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;
import de.pidata.wsdl.TOperation;

import java.io.PrintWriter;

/**
 * Created by pru on 16.01.15.
 */
public class WsdlSAPMethod extends WsdlMethod {

  public WsdlSAPMethod(GeneratePortType parentClass, TOperation op) {
    super( parentClass, op );
    addImport( ServiceManager.class.getName() );
    addImport( ServiceException.class.getName() );
    addImport( Context.class.getName() );
    addImport( ChildList.class.getName() );
    addImport( SystemManager.class.getName() );
    addImport( "com.sap.mw.jco.JCO" );
    addImport( java.util.Stack.class.getName() );
    addImport( Logger.class.getName() );
  }

  protected void writeMethod(PrintWriter pw) {
    writeMethodDeclaration( pw );
    pw.println( " {" );

    String opID = "OP_" + getName().toUpperCase();
    pw.println( "    Logger.debug( \"Calling \"+" + opID + " );" );
    pw.println( "    JCO.Client connection = jCoAdapter.getSapConnection();" );
    pw.println( "    JCO.Function function = prepareFunction( connection, " + opID + " );" );
    pw.println();

    boolean inputCreated = false;
    for (int i = 0; i < inputNames.size(); i++) {
      String partName = inputNames.elementAt( i ).toString();
      Object partType = inputTypes.elementAt( i );
      if (partType instanceof SimpleType) {
        if (!inputCreated) {
          pw.println( "    JCO.ParameterList input = function.getImportParameterList();" );
          inputCreated = true;
        }
        String sapParamName = partName.toLowerCase();
        String partTypeDef = getSimpleTypeDef( (SimpleType) partType );
        pw.println( "    input.setValue( \"" + partName + "\", jCoAdapter.value2jco( " + partTypeDef + ", " + sapParamName + "  ) );" );
      }
    }
    pw.println();

    for (int i = 0; i < inputNames.size(); i++) {
      String partName = inputNames.elementAt( i ).toString();
      Object partType = inputTypes.elementAt( i );
      if (partType instanceof ComplexType) {
        pw.println( "    jCoAdapter.writeTable( function, \"" + inputNames.elementAt(i) + "\", " + partName.toLowerCase() + ", new Stack(), new Stack() );" );
      }
    }
    pw.println();

    pw.println( "    jCoAdapter.execute( function, connection );" );

    if (getReturnType() != null) {
      pw.println();
      pw.println( "    ChildList outChilds = readOutput( function, " + opID + " );" );
      String returnType = getReturnTypeName();
      if (returnType.equalsIgnoreCase( "body" )) {
        pw.println( "    Body output = new Body( null, null, null, outChilds );" );
      }
      else {
        pw.println( "    "+returnType+" output = ("+returnType+") outChilds.getFirstChild( null );" );
      }
      pw.println( "    jCoAdapter.closeSapConnection(connection);" );
      pw.println( "    return output;" );
    }
    pw.println( "  }" );
  }
}
