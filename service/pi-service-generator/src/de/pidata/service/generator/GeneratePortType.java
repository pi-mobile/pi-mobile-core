/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.generator;

import de.pidata.models.generator.ClassGenerator;
import de.pidata.models.generator.FactoryClassGenerator;
import de.pidata.models.generator.TypeClassGenerator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.service.base.ServiceException;
import de.pidata.wsdl.TDefinitions;
import de.pidata.wsdl.TPortType;

import java.util.Hashtable;

public class GeneratePortType  extends ClassGenerator {

  protected TDefinitions wsdlDef;
  protected TPortType portType;
  protected Hashtable returnTypes = new Hashtable();

  public GeneratePortType( String className, Namespace targetNamespace, String packageName, FactoryClassGenerator factoryClassGenerator, String outputDirName, TDefinitions wsdlDef ) {
    super( className, targetNamespace, packageName, factoryClassGenerator, outputDirName );
    this.wsdlDef = wsdlDef;
    addImport( ServiceException.class.getName() );
  }

  protected void createReturnClass( ComplexType returnType, String packageName, String outputDirName) throws ClassNotFoundException {
    ClassGenerator classGen = new TypeClassGenerator(returnType.name(), packageName, outputDirName, null, returnType);
    classGen.generate();
  }

  public void addReturnType(ComplexType returnType) {
    this.returnTypes.put(returnType.name(), returnType);
  }

}
