/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.generator;

import de.pidata.comm.soap.SoapFactory;
import de.pidata.models.generator.Schema2Class;
import de.pidata.models.tree.BaseFactory;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.xml.schema.RootSchema;
import de.pidata.models.xml.schema.Schema;
import de.pidata.models.xml.schema.SchemaFactory;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.service.log.LogFactory;
import de.pidata.system.desktop.DesktopSystem;
import de.pidata.wsdl.TDefinitions;
import de.pidata.wsdl.TTypes;
import de.pidata.wsdl.WSDLFactory;
import de.pidata.wsdl.WsdlLoader;

public class GenerateWebServiceClasses {

  private static void printUsage() {
    System.out.println("Usage: GenerateWebServiceClasses");
    System.out.println("\t-wsdl <wsdlFile>");
    System.out.println("\t-porttype <portTypeName>");
    System.out.println("\t-path <project-path>");
    System.out.println("\t-interface <path-in-project> <interfacePackageName>");
    System.out.println("\t-implement <path-in-project> <implementationPackageName>");
    System.out.println("\t-dataPkg <dataPackageName>");
    System.out.println("\t-proxy <path-in-project> <proxyPackageName>");
    System.out.println();
    System.out.println("Optional:");
    System.out.println("\t-proxybase <extensionForProxy>");
  }

  public static void main(String[] args) {
    int i = 0;
    if (args.length < 14 || args[0].equals( "-h" )) {
      printUsage();
      System.exit(0);
    }

    try {
      new DesktopSystem();
      if (ModelFactoryTable.getInstance().getFactory( SchemaFactory.NAMESPACE ) == null) new SchemaFactory();
      if (ModelFactoryTable.getInstance().getFactory( WSDLFactory.NAMESPACE ) == null) new WSDLFactory();
      if (ModelFactoryTable.getInstance().getFactory( SoapFactory.NAMESPACE ) == null) new SoapFactory();
      if (ModelFactoryTable.getInstance().getFactory( BaseFactory.NAMESPACE ) == null) new BaseFactory();
      if (ModelFactoryTable.getInstance().getFactory( LogFactory.NAMESPACE ) == null) new LogFactory();

      String wsdlFile = null;
      String portType = null;
      String destPath = null;
      String interfaceSourcePath = null;
      String implSourcePath = null;
      String proxySourcePath = null;
      String interfacePackage = null;
      String implPackage = null;
      String proxyPackage = null;
      String extensionName = null;
      String dataPackageName = null;

      while (i < args.length) {
        if (args[i].equals("-wsdl")) {
          wsdlFile = args[++i];
        }
        else if (args[i].equals("-porttype")) {
          portType = args[++i];
        }
        else if (args[i].equals("-path")) {
          destPath = args[++i];
        }
        else if (args[i].equals("-interface")) {
          interfaceSourcePath = args[++i];
          interfacePackage = args[++i];
        }
        else if (args[i].equals("-implement")) {
          implSourcePath = args[++i];
          implPackage = args[++i];
        }
        else if (args[i].equals("-dataPkg")) {
          dataPackageName = args[++i];
        }
        else if (args[i].equals("-proxy")) {
          proxySourcePath = args[++i];
          proxyPackage = args[++i];
        }
        else if (args[i].equals("-proxybase")) {
          extensionName = args[++i];
        }
        i++;
      }

      //TODO: defaults!
      if (wsdlFile == null || portType == null || destPath == null || interfacePackage == null
              || implPackage == null || proxyPackage == null) {
        printUsage();
        System.exit( 0 );
      }
      interfaceSourcePath = destPath + "/" + interfaceSourcePath;
      implSourcePath = destPath + "/" + implSourcePath;
      proxySourcePath = destPath + "/" + proxySourcePath;

      TDefinitions wsdlDef = WsdlLoader.loadWSDL(wsdlFile);
      if (!WsdlLoader.validate(wsdlDef)) {
        System.exit(1);
      }
      
      for (ModelIterator typesIter = wsdlDef.typesIter(); typesIter.hasNext(); ) {
        TTypes types = (TTypes) typesIter.next();
        for (ModelIterator iter = types.iterator(null, null); iter.hasNext(); ) {
          Model model = iter.next();
          if (model instanceof Schema) {
            new Schema2Class((RootSchema) model, interfacePackage, "name", interfaceSourcePath );
          }
          else {
            throw new IllegalArgumentException("Unsupported child inside types definition: typeName="+model.type().name());
          }
        }
      }
      Namespace targetNS = Namespace.getInstance(wsdlDef.getTargetNamespace());

      QName portTypeName = targetNS.getQName(portType);
      GeneratePortTypeInterface ifGenerator = new GeneratePortTypeInterface( wsdlDef, portTypeName, interfacePackage,
                                                                             dataPackageName, interfaceSourcePath );
      ifGenerator.generate();
      String interfaceName = interfacePackage + "." + portTypeName.getName();

      String ifPackageImport = ifGenerator.getPackageName() + "." + ifGenerator.getName();
      String implPackageName = implPackage;
      GenerateWebServiceStub stubGenerator = new GenerateWebServiceStub( wsdlDef, portTypeName, implPackageName,
                                                                  dataPackageName, interfaceName, implSourcePath );
      stubGenerator.addImplements( ifPackageImport, true );
      stubGenerator.generate();

      GenerateWebServiceImplement implGenerator = new GenerateWebServiceImplement( wsdlDef, portTypeName,
                                                                                   implPackageName, implSourcePath );
      // Do not overwrite existing implementation!
      if (!implGenerator.isFilePresent()) {
        implGenerator.addImplements( ifPackageImport, true );
        implGenerator.generate();
      }

      String proxyPackageName = proxyPackage;
      GenerateWebServiceProxy proxyGenerator = new GenerateWebServiceProxy( wsdlDef, portTypeName, proxyPackageName,
                                                     dataPackageName, interfaceName, extensionName, proxySourcePath );
      proxyGenerator.addImplements( ifPackageImport, true );
      proxyGenerator.generate();
    }
    catch (Exception ex) {
      ex.printStackTrace();
      System.exit(1);
    }
  }

}
