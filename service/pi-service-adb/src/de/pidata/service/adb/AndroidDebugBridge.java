/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.adb;

import de.pidata.log.Logger;

import java.io.*;
import java.util.Scanner;

/**
 * Created by petrudo on 03.05.2017.
 */
public class AndroidDebugBridge {

  private static String OS = System.getProperty("os.name").toLowerCase();

  private String adbPath;

  public AndroidDebugBridge( String adbPath ) {
    this.adbPath = adbPath;
    File adbFile = new File( adbPath );
    if (!adbFile.exists()) {
      throw new IllegalArgumentException( "ADB is missing, path="+adbPath );
    }
  }

  public boolean isWindows() {
    return (OS.contains( "win" ));
  }

  public boolean isMac() {
    return (OS.contains( "mac" ));
  }

  public boolean isUnix() {
    return (OS.contains( "nix" ) || OS.contains( "nux" ) || OS.indexOf("aix") > 0 );
  }

  public boolean isSolaris() {
    return (OS.contains( "sunos" ));
  }

  public void createPortReverse( int port ) throws IOException {
    Logger.info( "ADB creating port reverse on port="+port );
    Process process = Runtime.getRuntime().exec( adbPath + " reverse tcp:"+port+" tcp:"+port );
    Scanner sc = new Scanner( process.getErrorStream() );
    if (sc.hasNext()) {
      StringBuilder adbMsg = new StringBuilder();
      while (sc.hasNext()) {
        adbMsg.append( sc.next() );
      }
      Logger.warn( "Cannot create ADB port reverse, ADB output: " + adbMsg.toString() );
    }
    sc.close();
  }

  public void removePortReverse( int port ) throws IOException {
    Logger.info( "ADB removing port reverse on port="+port );
    Process process = Runtime.getRuntime().exec( adbPath + " reverse --remove tcp:"+port );
    Scanner sc = new Scanner( process.getErrorStream() );
    if (sc.hasNext()) {
      StringBuilder adbMsg = new StringBuilder();
      while (sc.hasNext()) {
        adbMsg.append( sc.next() );
      }
      Logger.warn( "Cannot stop ADB port reverse, ADB output: " + adbMsg.toString() );
    }
    sc.close();
  }

  public String execute( String command ) throws IOException {
    String[] commands;
    if (isWindows()) {
      commands = new String[]{"cmd", "/C", command};
    }
    else {
      commands = new String[]{"/bin/sh", "-c", command};
    }
    Process proc = new ProcessBuilder( commands ).start();
    InputStream stdInput = proc.getInputStream();
    InputStream stdError = proc.getErrorStream();
    int ch;
    StringBuilder sb = new StringBuilder();
    do {
      ch = stdInput.read();
      sb.append( (char) ch );
    }
    while (ch >= 0);
    do {
      ch = stdError.read();
      sb.append( (char) ch );
    }
    while (ch >= 0);
    return sb.toString();
  }

  /**
   * Extracting Device Id through ADB
   *
   * @return ID of first device
   * @throws IOException
   */
  public String getDeviceID() throws IOException {
    String[] device_list = execute( adbPath + " devices" ).split( "\\r?\\n" );
    for (int i = 0; i < device_list.length; i++) {
      if (device_list[i].startsWith( "*" )) {
        // comment like "* deamon not running..."
      }
      else {
        int pos = device_list[i].indexOf( "\t" );
        if (pos > 0 ) {
          if (device_list[i].substring( pos+1 ).startsWith( "device" )) {
            /*String device_name = "" + execute( adbPath + " -s " + device_id[0] + " shell getprop ro.product.manufacturer" )
                + execute( adbPath + " -s " + device_id[0] + " shell getprop ro.product.model" );
            device_name = device_name.replaceAll( "\\s+", " " );*/
            return device_list[i].substring( 0, pos );
          }
        }
      }
    }
    return null;
  }
}
