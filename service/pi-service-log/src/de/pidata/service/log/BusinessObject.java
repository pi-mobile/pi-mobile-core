/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.log;

import de.pidata.models.service.Service;
import de.pidata.service.base.ServiceException;
import de.pidata.models.tree.Model;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.QName;

public interface BusinessObject {

  /**
   * Initializes this BusinessObject's type, which defines the BO's internal structure.
   * That type is used for synchronization between backoffice and clients.
   * @param type this BusinessObject's type
   */
  public void init(ComplexType type);

  public void setAuthentification( String username, String password);

  public QName getName();

  public ComplexType getBOType();

  public int viewCount();

  /**
   * Retrieve all data from the main view.
   * @return the container model holding all data retrieved from the main view
   */
  public Model getView();

  /**
   * Retrieve the requsted fields from the view by applying the requested filters.
   * @param dataContainer the container in which the retrieved data will be collected
   * @param requestedFields an object representing the requested fields
   * @param requestedFilters an object representing the requested filters
   */
  public void retrieveData( Model dataContainer, Object requestedFields, Object requestedFilters);

  public Service getClientService();

  public Service getServerService();

  public Model create();

  /**
   * Called to update/insert the given businessObject to client identified
   * by targetID
   * @param logMgr          the client's log manager
   * @param businessObject  the businessObject to be updated/inserted
   * @throws ServiceException
   */
  public void update(LogManager logMgr, Model businessObject) throws ServiceException;

  /**
   * Called whenever client connects to server. Server may look for updates and
   * change client's state. After return boState is stored in log index.
   * To avoid problems do not store large amount of data within boState!
   * @param logMgr   the client's log manager
   * @param boState  the client state stored in log index for client, may be null
   * @return the new boState or null 
   * @throws Exception may throw any exception: in that case boState is not stored!
   */
  public BOStateType check4Updates(LogManager logMgr, BOStateType boState) throws Exception;
}
