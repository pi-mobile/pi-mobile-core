/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.log;

import de.pidata.log.Level;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelFactory;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.models.types.simple.BooleanType;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.models.xml.binder.XmlWriter;
import de.pidata.qnames.QName;
import de.pidata.system.base.Storage;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.SystemManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Vector;

/**
 * Verwaltet die Log-Dateien und kümmert sich um Transaktionen auf diesen Dateien.
 * Auf dem Client übernimmt der SystemSynchronizer das abfahren und versenden der Logs.
 * Das Gegenstück auf dem Server ist der CommServer.
 * Die Logs werden in einem eigenen Verezeichnis je Kommunikationspartner gespeichert.
 * Dabei ist in die index.xml der Verarbeitungszustand gespeichert. Die einzelnen (zu versendenden)
 * Log-Sätze sind in Dateien S-LOG-xxxx gespeichert wobei xxx die Sequenznummer des Logs ist.
 * Attribute von LogState:
 *  next = nöchste (nicht verwendete) Sequenznummer für neue Log-Sätze
 *  sent = letzter versendeter und von Gegenstelle bestätigter Log-Satz
 *  received = letzter von der Gegenstelle empfangener und erfolgreich verarbeiterter Log-Satz
 *  syncTime = letzter Sync-Zeitpunt, wird immer vom Server gesetzt
 */
public class LogManager {
  private static final boolean DEBUG = false;

  private static HashMap<QName,LogManager> instances = new HashMap<QName,LogManager>();
  private static LogManager defaultInstance = null;

  private QName targetID;

  public static final String LOG_DIR = "log";
  public static final String LOG_STORE_RESOURCE = "index.xml";
  public static final String LOGFILE_PREFIX = "S_LOG_";
  public static final String BUFFERFILE_PREFIX = "BUFFER_";
  public static final String LOGFILE_EXTENSION = ".xml";

  public static final QName KEY_PROCESSED_SEQ = LogFactory.NAMESPACE.getQName( "processedSeq" );
  public static final QName KEY_LAST_SYNC_TIME = LogFactory.NAMESPACE.getQName( "lastSyncTime" );;

  public static final String OUT_OF_SYNC = "OUT_OF_SYNC";

  private LogState logStore;

  private Storage storage;
  private XmlWriter xmlWriter;
  public static final String SERVICE_LOG_MANAGER = "LogManager";
  public static final QName OP_SYNC = LogFactory.NAMESPACE.getQName( "sync" );

  //private LogPipe logPipe = null;

  /**
   * Returns the LogManager for the given targetID. If targetID is null the
   * default LogManager is returned.
   * @param targetID the target for which a LogManager is returned, may be null
   * @return the LogManager for the given targetID
   */
  public synchronized static LogManager getInstance( String targetID ) {
    if (targetID == null) {
      if (defaultInstance == null) {
        defaultInstance = new LogManager( null );
      }
      return defaultInstance;
    }
    else {
      //----- Remove namespace if existing
      int pos = targetID.lastIndexOf( ':' );
      if (pos >= 0) {
        targetID = targetID.substring( pos+1 );
      }

      QName qTargetID = LogFactory.NAMESPACE.getQName( targetID );
      LogManager result = instances.get( qTargetID );
      if (result == null) {
        result = new LogManager( qTargetID );
        instances.put( qTargetID, result );
      }
      return result;
    }
  }

  private LogManager( QName targetID ) {
    this.targetID = targetID;
    init();
  }


  /**
   * Returns a list of all LogManagers that have been installed via getInstance(QName) method.
   * The default LogManager is not included and can be accessed via getInstance method.
   */
  public static Vector getInstalledManagers() {
    Vector managers = new Vector();
    for (LogManager logMgr : instances.values()) {
      managers.addElement( logMgr );
    }
    return managers;
  }

  /**
   * Creates a log entry.
   * Remember that input cannot be null since output is always null for service calls
   * via logging and WSDL requires to have either input or output
   * @param portTypeName   the backoffice portType
   * @param operationName  the backoffice operation
   * @param input          must not be null
   * @return the log entry
   */
  public static LogEntry createLogEntry(QName portTypeName, QName operationName, Model input) {
    ModelFactory logFactory = ModelFactoryTable.getInstance().getFactory( LogFactory.NAMESPACE );
    LogEntry logEntry = (LogEntry) logFactory.createInstance(LogEntry.TYPE);
    logEntry.namespaceTable().addNamespace( LogFactory.NAMESPACE, "log" );
    logEntry.setService(portTypeName);
    logEntry.setOperation(operationName);
    MessageType inputMsg = (MessageType) logFactory.createInstance(MessageType.TYPE);
    inputMsg.add(input.getParentRelationID(), input);
    logEntry.setInput(inputMsg);
    return logEntry;
  }

  /**
   * Creates a log entry.
   * Remember that input cannot be null since output is always null for service calls
   * via logging and WSDL requires to have either input or output
   * @param portTypeName   the backoffice portType
   * @param operationName  the backoffice operation
   * @param input          must not be null
   * @param messageName    the message name used as relation when addind input model
   * @return teh log entry
   */
  public static LogEntry createLogEntry(QName portTypeName, QName operationName, Model input, QName messageName) {
    ModelFactory logFactory = ModelFactoryTable.getInstance().getFactory( LogFactory.NAMESPACE );
    LogEntry logEntry = (LogEntry) logFactory.createInstance(LogEntry.TYPE);
    logEntry.namespaceTable().addNamespace( LogFactory.NAMESPACE, "log" );
    logEntry.setService(portTypeName);
    logEntry.setOperation(operationName);
    MessageType inputMsg = (MessageType) logFactory.createInstance(MessageType.TYPE);
    inputMsg.add(messageName, input);
    logEntry.setInput(inputMsg);
    return logEntry;
  }

  // -----------------------------------------------------------------------------------

  public String getTargetID() {
    if (targetID == null) return null;
    else return targetID.getName();
  }

  public QName getTargetQName() {
    return targetID;
  }

  public int getSeqReceived() {
    return logStore.getReceived().intValue();
  }

  public int getSeqSent() {
    return logStore.getSent().intValue();
  }

  public int getSeqNext() {
    return logStore.getNext().intValue();
  }

 /**
   * Returns the last time client and server did synchronization
   */
  public String getLastSyncTime() {
    return logStore.getLastSyncTime();
  }

  public void setLastSyncTime(String lastSyncTime) throws IOException {
    logStore.setLastSyncTime(lastSyncTime);
    flushLogIndex();
  }

  /**
    * Returns the new server sync time. this file is only used by server
    */
  public String getNewSyncTime() {
     return logStore.getNewSyncTime();
   }

  public void setNewSyncTime(String syncTime) throws IOException {
    logStore.setNewSyncTime(syncTime);
    flushLogIndex();
  }

  /**
   * Check if this LogManager has a state that has been approved beeing consistent
   * with it's partner log manager.
   *
   * @return <code>true</code> if in sync with partner, <code>false</code> after reset/failure or initial startup.
   */
  public boolean isSynchronized() {
    return logStore.getSync().booleanValue();
  }

  public synchronized void outOfSync() {
    removePendingLogs();
    resetAll();
  }

  public synchronized void setInSync() throws IOException {
    if (!isSynchronized()) {
      logStore.setSync(BooleanType.TRUE);
      flushLogIndex();
    }  
  }

 /* public synchronized void setLogPipe( LogPipe logPipe ) throws Exception {
    if (this.logPipe != null) {
      throw new RuntimeException( "LogManager already has a log pipe set." );
    }

    Vector logs = getPendingLogs();

    Enumeration elems = logs.elements();
    while (elems.hasMoreElements()) {
      Model sLog = (Model) elems.nextElement();
      logPipe.writeLog( sLog );
    }
    logPipe.writeStatus(createLogState());

    this.logPipe = logPipe;
  }
*/

  /**
   * Creates a clone of this logmanager's LogState. If this is a server log manager
   * (i.e. targetID != null) result's lastSyncTime is replayced by newSyncTime and
   * newSyncTime is cleared.
   * @param includeRepeat if flase repeatName and repeatIndex are cleared to support Clients having older
   *                      LogState versions
   * @return a clone of this logmanager's LogState 
   */
  public synchronized LogState createLogState( boolean includeRepeat ) {
    LogState state = (LogState) logStore.clone(logStore.key(), true, false );
    if (targetID != null) {
      state.setLastSyncTime( state.getNewSyncTime() );
      state.setNewSyncTime(null);
    }
    if (!includeRepeat) {
      state.setRepeatName( null );
      state.setRepeatIndex( null );
    }
    return state;
  }

 /* public synchronized void removeLogPipe() {
    logPipe = null;
  } */

  /**
   * Adds selviceLog to this log manager and increments seqNext.
   * @param serviceLog the log to be added
   * @throws IOException if writing of log or log index fails
   */
  public synchronized void addLog( LogEntry serviceLog ) throws IOException {
    if (DEBUG) {
      Logger.debug("addLog --> targetID="+this.targetID);
      Logger.debug("   seqNext="+getSeqNext()+" seqSent="+getSeqSent()+" seqReceived="+getSeqReceived());
    }
    Integer seqInt = logStore.getNext();
    String resourceName = createResourceName( seqInt.intValue() );
    serviceLog.setSEQ(seqInt);

    xmlWriter.write(resourceName, serviceLog, serviceLog.type().name() );
    logStore.setNext(new Integer(seqInt.intValue()+1));
    flushLogIndex();

/*    try {
      if (logPipe != null) {
        Logger.debug( "piping service log." );
        logPipe.writeLog( serviceLog );
      }
    }
    catch (Exception e) {
      removeLogPipe();
      Logger.warn( "LogManager.addLog() could not write log to network. Exception: '" + e.getMessage() + "'" );
    }
*/
    if (Logger.getLogLevel() == Level.DEBUG) {
      StringBuilder buf = xmlWriter.writeXML(serviceLog, serviceLog.type().name());
      Logger.debug("Wrote log for targetID=\""+this.targetID+"\":\n"+buf);
    }  
    if (DEBUG) {
      Logger.debug("   seqNext="+getSeqNext()+" seqSent="+getSeqSent()+" seqReceived="+getSeqReceived());
      Logger.debug("addLog <-- targetID="+this.targetID);
    }
  }

  public synchronized boolean hasPendingLogs() {
    return (getPendingLogNames().length > 0);
  }

  public synchronized Vector getPendingLogs() throws IOException {
    InputStream in = null;
    Vector logs = new Vector();
    XmlReader reader = new XmlReader();

    String[] resources = getPendingLogNames();
    for (int i=0; i<resources.length; i++) {
      try {
        in = storage.read(resources[i]);
        logs.addElement( reader.loadData( in, null ) );
      }
      finally {
        StreamHelper.close( in );
      }
    }
    return logs;
  }

  /**
   * Called after counter part's log with seqNumber has been successfully processed.
   * With next communication counter part will get confirmation for seqNumber.
   * @param seqNumber the log sequnce number successfully processed
   * @throws IOException if log could not be processed
   */
  public synchronized void confirmProcessing( int seqNumber ) throws IOException {
    if (DEBUG) {
      Logger.debug("confirmProcessing --> targetID="+this.targetID);
      Logger.debug("   seqNext="+getSeqNext()+" seqSent="+getSeqSent()+" seqReceived="+getSeqReceived());
    }
    logStore.setReceived(new Integer(seqNumber));
    flushLogIndex();

/*    try {
      if (logPipe != null) {
        Logger.debug( "piping service log." );
        logPipe.writeStatus( createLogState() );
      }
    }
    catch (Exception e) {
      removeLogPipe();
      Logger.warn( "LogManager.addLog() could not write log to network. Exception: '" + e.getMessage() + "'" );
    }
*/
    if (DEBUG) {
      Logger.debug("   seqNext="+getSeqNext()+" seqSent="+getSeqSent()+" seqReceived="+getSeqReceived());
      Logger.debug("confirmProcessing <-- targetID="+this.targetID);
    }
  }

  /**
   * Tells this log manager that its counter part has received logs having
   * sequence number up to number inclusive.
   * This method updates seqSent, flushed log index an then removes all
   * log files having sequence <= number.
   * @param number the last log sequence number received
   */
  public synchronized void confirmReceival( int number ) {
    String filename;
    if (DEBUG) {
      Logger.debug("confirmReceival --> targetID="+this.targetID);
      Logger.debug("   seqNext="+getSeqNext()+" seqSent="+getSeqSent()+" seqReceived="+getSeqReceived());
    }
    try {
      setSeqSent(number);
    }
    catch (IOException e) {
      // If log cannot be flushed internal state is chnaged but log files
      // are not deleted. They will be resnet later which is recognized by the
      // server. Hopefully next attempt to flush is successful.
      Logger.error("LogManager ID="+this.targetID+" could not flush log index", e);
      return;
    }

    removeLogs(getSeqSent()+1, getSeqNext()-1);

    if (DEBUG) {
      Logger.debug("   seqNext="+getSeqNext()+" seqSent="+getSeqSent()+" seqReceived="+getSeqReceived());
      Logger.debug("confirmReceival <-- targetID="+this.targetID);
    }
  }

  /**
   * Remove all logs except the ones between keepFromIndex to keepToIndex (both included)
   * @param keepFromIndex the first index not to be deleted
   * @param keepToIndex   the last index not to be deleted
   * Must be callled in synchronized context
   */
  private void removeLogs(int keepFromIndex, int keepToIndex) {
    String filename;
    int number;
    String[] names;
    try {
      names = storage.list();
    }
    catch (IOException e) {
      Logger.error("LogManager ID="+this.targetID+" could not read log directory", e);
      return;
    }
    for (int i = 0; i < names.length; i++) {
      filename = names[i];
      number = extractSeqNumber( filename );
      if (number >= 0) {
        if ((number < keepFromIndex) || (number > keepToIndex)) {
          try {
            storage.delete(filename);
          }
          catch (IOException e) {
            Logger.error("LogManager ID="+this.targetID+" could not delete log file: "+filename, e);
            // we don't need to worry about logs that could not be deleted.
            // seqSent has been set before so they won't be resent.
          }
        }
      }
    }
  }

  /**
   * Extracts seq number form filename.
   * If filename is no log file -1 is returned
   * @param filename
   * @return seq number or -1
   */
  private int extractSeqNumber(String filename) {
    if (filename.startsWith(LOGFILE_PREFIX)) {
      return Integer.parseInt(filename.substring(LOGFILE_PREFIX.length(), filename.length()-LOGFILE_EXTENSION.length()));
    }
    else {
      return -1;
    }
  }

  private String[] getPendingLogNames() {
    if (DEBUG) {
      Logger.debug("getPendingResources --> targetID="+this.targetID);
      Logger.debug("   seqNext="+getSeqNext()+" seqSent="+getSeqSent()+" seqReceived="+getSeqReceived());
    }
    int next = getSeqNext();
    int sent = getSeqSent();
    String[] resources = new String[next-(sent+1)];
    int number;
    String filename;

    String[] names;
    try {
      names = storage.list();
    }
    catch (IOException e) {
      Logger.error("LogManager ID="+this.targetID+" could not read log directory", e);
      return null;
    }
    if (names != null) {
      for (int i = 0; i < names.length; i++) {
        filename = names[i];
        number = extractSeqNumber( filename );
        if (number >= 0) {
          if ((number > getSeqSent()) && (number < getSeqNext())) {
            resources[number-(sent+1)] = filename;
          }
        }
      }  
    }
    if (DEBUG) Logger.debug("getPendingResources <-- targetID="+this.targetID);
    return resources;
  }

  private String createResourceName( int seq ) {
    return LOGFILE_PREFIX + seq + LOGFILE_EXTENSION;
  }

  /**
   * Set seqSent and make sure that seqNext is larger that seqSent. Then flush log index.
   * If number is less than seqSent log may be lost. But in that cas we must have been
   * out of sync and dropped all logs anyway.
   * @param number the value to set seqNext to
   * @throws IOException if flushj was not successful
   */
  private void setSeqSent(int number) throws IOException {
    // If a client has beeen reseted number may be less than seqSent on server. We ignore that
    // and on return client get automatically back in sync.
    if (number > getSeqSent()) {
      logStore.setSent(new Integer(number));
      if (getSeqNext() <= number) {
        logStore.setNext(new Integer(number + 1));
      }
      flushLogIndex();
    }
  }

  private void checkLogStore() {
    int sent = getSeqSent();
    int num;
    String[] pending = getPendingLogNames();
    for (int i = 0; i < pending.length; i++) {
      if ((pending[i] == null) || (pending[i].length() == 0)) {
        num = sent + 1 + i;
        throw new RuntimeException("LogStore is inkonsistent: missing seqNr="+num);
      }
    }
  }

  private synchronized void flushLogIndex() throws IOException{
    if (DEBUG) {
      Logger.debug("   flushLogIndex --> targetID="+this.targetID);
      Logger.debug("      seqNext="+getSeqNext()+" seqSent="+getSeqSent()+" seqReceived="+getSeqReceived());
    }
    checkLogStore();
    xmlWriter.write(LOG_STORE_RESOURCE, logStore, logStore.type().name());
    if (DEBUG) {
      Logger.debug("      seqNext="+getSeqNext()+" seqSent="+getSeqSent()+" seqReceived="+getSeqReceived());
      Logger.debug("   flushLogIndex <-- targetID="+this.targetID);
    }
  }

  /**
   * Initialize reader andf writer, then try to read log index. If not successful initialize
   * log index.
   */
  private void init() {
    XmlReader reader;
    InputStream in = null;
    String    path;

    try {
      if (targetID == null) path = LOG_DIR;
      else path = targetID.getName() + "/" + LOG_DIR;
      storage = SystemManager.getInstance().getStorage( path );
      xmlWriter = new XmlWriter(storage);
      reader = new XmlReader();
      in =  storage.read(LOG_STORE_RESOURCE);
      logStore = (LogState) reader.loadData( in, null );
    }
    catch (Exception e) {
      //TODO: Wenn während des Betriebs ein Client seinen Log-Index verliert hat das momentan folgende
      // Auswirkungen:
      // - seqNext beginnt wieder bei 0, was dazu führt, dass einige Logs verloren gehen, nämlich die
      //   deren seq < seqReceived des Servers ist
      // - bei der nächsten Kommunikation wird mit dem Setzen von seqSent auch seqNext angepaßt und
      //   die anschließend erzeugten Logs werden wieder vom Server verarbeitet
      // --  obige zwei punkte können durch outOfSync berenigt werden.
      Logger.warn("LogManager ID="+this.targetID+" could not read log index, using initial values.");
      resetAll();
    }
    finally {
      StreamHelper.close( in );
    }
  }

  /**
   * Resets all LogManager variables an so looses all existing logs
   */
  public synchronized void resetAll() {
    Logger.warn("LogManager ID="+this.targetID+" resetting all status variables.");
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( LogFactory.NAMESPACE );
    logStore = (LogState) factory.createInstance(LogState.TYPE.name(), LogState.TYPE, null, null, null );
    logStore.namespaceTable().addNamespace( LogFactory.NAMESPACE, "log" );
    logStore.setSync(BooleanType.FALSE);
    logStore.setSent(new Integer(-1) );
    logStore.setReceived(new Integer(-1) );
    logStore.setNext(new Integer(0) );
    logStore.setLastSyncTime(null);
    logStore.setNewSyncTime(null);
    logStore.setRepeatName(null);
    logStore.setRepeatIndex(null);
    try {
      flushLogIndex();
    }
    catch (IOException e) {
      Logger.error("LogManager ID="+this.targetID+" could not flush log index", e);
    }
  }

  /**
   * Removes all local existing and not transferred logs and clears lastSyncTime
   */
  public synchronized void removePendingLogs() {
    logStore.setNext(new Integer(getSeqSent() + 1));
    logStore.setLastSyncTime(null);
    try {
      flushLogIndex();
      removeLogs(getSeqSent()+1, getSeqNext()-1);
    }
    catch (IOException e) {
      Logger.error("LogManager ID="+this.targetID+" could not flush log index", e);
    }
  }

  /**
   * Removes this LogManager's target, i.e. deletes all logs and the log index and
   * removes this instance for instances.
   * @throws IOException if log index could not be deleted
   */
  public synchronized void removeTarget() throws IOException {
    Logger.info("Removing LogManager and all logs for target ID="+this.targetID);
    removePendingLogs();
    instances.remove(this.targetID);
    storage.delete(LOG_STORE_RESOURCE);
    SystemManager sysMan = SystemManager.getInstance();
    if (targetID == null) {
      sysMan.getStorage( null ).delete( storage.getName() );
    }
    else {
      sysMan.getStorage( targetID.getName() ).delete( LOG_DIR );
      sysMan.getStorage( null ).delete( targetID.getName() );
    }
  }

  public BOStateType getBOState(QName boName) {
    return logStore.getBOState(boName);
  }

  public synchronized void setBOState(QName boName, BOStateType boState) {
    if (boState == null) {
      BOStateType oldBOState = getBOState(boName);
      if (oldBOState != null) logStore.removeBOState(oldBOState);
    }
    else {
      logStore.replace(LogState.ID_BOSTATE, boState);
    }
    try {
      flushLogIndex();
    }
    catch (IOException e) {
      Logger.error("LogManager ID="+this.targetID+" could not flush log index", e);
    }
  }

  public synchronized void setRepeat( String repeatName, int repeatIndex ) {
    logStore.setRepeatName( repeatName );
    logStore.setRepeatIndex( new Integer(repeatIndex) );
    try {
      flushLogIndex();
    }
    catch (IOException e) {
      Logger.error("LogManager ID="+this.targetID+" could not flush log index", e);
    }
  }

  public synchronized InputStream readBuffer( String bufferName ) throws IOException {
    bufferName = BUFFERFILE_PREFIX + bufferName;
    if (storage.exists( bufferName )) {
      return storage.read( bufferName );
    }
    else {
      return null;
    }
  }

  public synchronized OutputStream writeBuffer( String bufferName ) throws IOException {
    bufferName = BUFFERFILE_PREFIX + bufferName;
    return storage.write( bufferName, true, false );
  }

  public void removeBuffer( String bufferName ) throws IOException {
    bufferName = BUFFERFILE_PREFIX + bufferName;
    storage.delete( bufferName );
  }
}
