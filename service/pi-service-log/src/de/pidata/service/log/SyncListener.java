/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.log;

import de.pidata.models.tree.Context;

/**
 * Interface for the client's data access class.
 */
public interface SyncListener {

  /**
   * Called by SystemSynchronizer if server and client went out of sync (different
   * time stamps for last sync).
   * @param logMgr the logManger on which the event happened
   * @param clientSyncTime  client's syncTime value
   * @param serverSyncTime  server's syncTime value
   */
  public void outOfSync(LogManager logMgr, String clientSyncTime, String serverSyncTime);

  /**
   * Called before sync is started and after connection to server has been established.
   * Implementing this method allows creating log entries just before sync is started
   * @param context Context of the synchronizing client
   * @param logMgr  the logManger on which the event happened
   */
  public void beforeSync( Context context, LogManager logMgr );

  /**
   * Called after sync and log processing has finished and before sending reply envelope
   * @param context Context of the synchronizing client
   * @param logMgr the logManger on which the event happened
   */
  public void afterSync( Context context, LogManager logMgr );
}
