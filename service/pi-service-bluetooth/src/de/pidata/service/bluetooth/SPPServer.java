/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.bluetooth;

import de.pidata.connect.stream.StreamHandler;
import de.pidata.log.Logger;
import de.pidata.connect.stream.StreamHandlerFactory;

import javax.bluetooth.*;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;
import java.io.*;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

/**
 * On Ubuntu 15.10 first install libbluetooth-dev (contains libbluetooth.so)
 *
 * Bug fix for Ubuntu 15.10 (based on http://ryanglscott.github.io/2014/07/17/bluetooth-sdp-on-fedora):
 * As an example, the command sdptool browse local will find all Bluetooth services registered on your machine.
 * If I ran it, I got the error: "Failed to connect to SDP server on FF:FF:FF:00:00:00: No such file or directory"
 *
 * As it turns out, the culprit is bluetoothd, the Bluetooth daemon. Using SDP with bluetoothd requires deprecated
 * features for some silly reason, so to fix this, the daemon must be started with bluetoothd -C (or bluetooth --compat).
 *
 * To have Ubuntu do this on startup:<ul>
 *   <li> Edit "/etc/systemd/system/bluetooth.target.wants/bluetooth.service"
 *   <li> And look for "ExecStart=/usr/libexec/bluetooth/bluetoothd".
 *   <li> Put ` -C at the end of this line
 *   <li> save
 *   <li> Then run "service bluetooth restart".
 * </ul>
 * If all goes well, you should be able to run sdptool browse local successfully.
 *
 * Then I got the error: "Failed to connect to SDP server on FF:FF:FF:00:00:00: Permission denied"
 * In Terminal type "sudo chmod a+rw /var/run/sdp"
 */
public class SPPServer implements Runnable {

  private static final boolean DEBUG = false;
  public static final int BT_INIT_RETRY_MILLIS = 1000;

  private UUID uuid = new UUID( "00000000000000000000000000001101", false );
  private String connectionString = "btspp://localhost:1101;name=PIMobile";

  private StreamConnectionNotifier streamConnNotifier = null;
  private StreamHandlerFactory streamHandlerFactory;
  private List<SPPServerConnection> sppServerConnections = new LinkedList<SPPServerConnection>();

  private boolean active = false;
  private boolean running = false;
  private boolean bluetoothActive = false;
  private int connectionCounter = 1;
  private Thread btThread = null;

  /**
   *
   * Wird nach dem Konstruktor aufgerufen, um die Parameter zu übergeben. Es wird
   * sichergestellt, dass init() genau einmal je Instanz aufgerufen wird.
   *
   * @param props Liste mit Parametern
   * @throws Exception
   */
  public void init( StreamHandlerFactory streamHandlerFactory, Hashtable props ) throws Exception {
    if (this.streamHandlerFactory != null) {
      throw new IllegalArgumentException( "Must not call init() more than once" );
    }
    this.streamHandlerFactory = streamHandlerFactory;
  }

  public synchronized int connectionCount() {
    return sppServerConnections.size();
  }

  public synchronized SPPServerConnection getConnection( int index ) {
    return sppServerConnections.get( index );
  }

  public synchronized SPPServerConnection getConnection( String connectionID ) {
    for (int i = 0; i < sppServerConnections.size(); i++) {
      SPPServerConnection conn = sppServerConnections.get( i );
      if (conn.getConnectionID().equals( connectionID ))  {
        return conn;
      }
    }
    return null;
  }

  public synchronized void start() throws IOException {
    active = true;
    btThread = new Thread( this );
    btThread.start();
  }

  public synchronized void stop() {
    Logger.info( "SPPServer stopping..." );
    active = false;
    bluetoothActive = false;
    int wait = 10; // StreamConnectionNotifier.acceptAndOpen() ignores interrupt and has no timeout
    while (running && (wait > 0)) {
      try {
        Thread.sleep( 1000 );
      }
      catch (InterruptedException e) {
        // do nothing
      }
      if (btThread != null) {
        btThread.interrupt();
      }
      wait--;
    }
  }

  public void run() {
    running = true;
    bluetoothActive = false;
    while (active) {
      try {
        LocalDevice localDevice = LocalDevice.getLocalDevice();
        Logger.info( "SPPServer init, local device address=" + localDevice.getBluetoothAddress() + ", name=" + localDevice.getFriendlyName() );

        //-- open server url
        Logger.info( "Trying to start Bluetooth SPP server for: " + connectionString );
        streamConnNotifier = (StreamConnectionNotifier) Connector.open( connectionString );
        Logger.info( "SPPServer Started. Waiting for clients to connect..." );
        bluetoothActive = true;
      }
      catch (Exception ex) {
        bluetoothActive = false;
        try {
          Thread.sleep( BT_INIT_RETRY_MILLIS );
        }
        catch (InterruptedException e) {
          // do nothing
        }
      }

      try {
        while (bluetoothActive) {
          //-- Wait for client connection
          StreamConnection connection = streamConnNotifier.acceptAndOpen();
          String connectionID = connectionString + "[" + connectionCounter + "]";
          StreamHandler streamHandler = streamHandlerFactory.createStreamHandler( connectionID );
          connectionCounter++;
          SPPServerConnection conn = new SPPServerConnection( this, connection, connectionID, streamHandler );
          synchronized (this) {
            this.sppServerConnections.add( conn );
          }
          conn.start();
        }
      }
      catch (Exception ex) {
        Logger.error( "SPPServer: Exception while bluetooth communication", ex );
        bluetoothActive = false;
      }
      finally {
        if (streamConnNotifier != null) {
          Logger.info( "SPPServer: closing connection..." );
          try {
            streamConnNotifier.close();
            streamConnNotifier = null;
          }
          catch (IOException e) {
            // ignore errors
          }
        }
      }
    }
    Logger.info( "SPPServer stopped" );
    running = false;
  }

  public void connectionClosed( SPPServerConnection sppServerConnection ) {
    synchronized (this) {
      this.sppServerConnections.remove( sppServerConnection );
      Logger.info( "SPPServer: removed connection ID="+sppServerConnection.getConnectionID()+ ", remaining="+sppServerConnections.size() );
    }
  }
}
