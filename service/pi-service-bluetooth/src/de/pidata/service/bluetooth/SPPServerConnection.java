/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.bluetooth;

import de.pidata.connect.base.AbstractConnection;
import de.pidata.connect.base.ConnectionListener;
import de.pidata.connect.stream.StreamHandler;
import de.pidata.connect.stream.StreamReceiver;
import de.pidata.log.Logger;
import de.pidata.stream.StreamHelper;

import javax.bluetooth.RemoteDevice;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Handler class for a single connection. SPPServer creates a new instance of this class for each
 * new connected client.
 */
public class SPPServerConnection extends AbstractConnection implements de.pidata.connect.stream.StreamConnection {

  public static final int READ_LOOP_IDLE_SLEEP_MILLIS = 50;
  public static final int TIMEOUT_MILLIS = 30000;

  private SPPServer sppServer;
  private javax.microedition.io.StreamConnection connection;
  private StreamHandler streamHandler;
  private String connectionID;
  private InputStream inputStream;
  private OutputStream outputStream;

  public SPPServerConnection( SPPServer sppServer, javax.microedition.io.StreamConnection connection, String connectionID, StreamHandler streamHandler ) throws IOException {
    this.sppServer = sppServer;
    this.connection = connection;
    this.connectionID = connectionID;
    this.streamHandler = streamHandler;
  }

  /**
   * Called by connectionListener to get all connection steps in order.
   * Implementation has to call connectionListener.addStep() in correct
   * order for each step.
   * <p>
   * The list of steps are exact that steps passed when establishing a
   * connection. While connection each registered listener will be called
   * on success or failure of these steps.
   *
   * @param connectionListener listener to add steps to
   */
  @Override
  public void getConnectionSteps( ConnectionListener connectionListener ) {
    //TODO: connection steps
  }

  public void start() throws IOException {
    resetConnectionSteps();
    RemoteDevice dev = RemoteDevice.getRemoteDevice( connection );
    String clientID = dev.getBluetoothAddress();
    Logger.info( "SPPServer["+connectionID+"]: connect from address=" + clientID + ", name=" + dev.getFriendlyName( true ) );
    StreamReceiver streamReceiver = new StreamReceiver( this, READ_LOOP_IDLE_SLEEP_MILLIS, TIMEOUT_MILLIS );

    inputStream = connection.openInputStream();
    setStateConnected();
    streamReceiver.startReceive( clientID, inputStream, streamHandler );
  }

  public SPPServer getSppServer() {
    return sppServer;
  }

  public StreamHandler getStreamHandler() {
    return streamHandler;
  }

  public String getConnectionID() {
    return connectionID;
  }

  public OutputStream getOutputStream() throws IOException {
    if (outputStream == null) {
      this.outputStream = connection.openOutputStream();
    }
    return outputStream;
  }

  @Override
  public void close() {
    Logger.info( "SPPServer["+connectionID+"]: closing connection" );
    StreamHelper.close( inputStream );
    inputStream = null;
    StreamHelper.close( outputStream );
    outputStream = null;
    if (connection != null) {
      try {
        connection.close();
      }
      catch (IOException e) {
        // ignore errors
      }
    }
    connection = null;
    setStateDisconnected();
    sppServer.connectionClosed( this );
  }
}
