/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.wsdl;

import de.pidata.log.Logger;
import de.pidata.models.config.Configurator;
import de.pidata.models.config.Instance;
import de.pidata.models.config.ModelLoader;
import de.pidata.service.base.ServiceException;
import de.pidata.models.tree.*;
import de.pidata.models.types.Relation;
import de.pidata.models.types.Type;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.system.base.Storage;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.SystemManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

public class WsdlLoader extends ModelLoader {

  private static Root WSDLroot = new Root();

  public void init( Configurator configurator, Instance instanceDef ) throws IOException {
    super.init( configurator, instanceDef );
    processTypes( configurator );
    //TODO kann das weg?
  }

  private void processTypes( Configurator configurator ) throws IOException {
    TDefinitions defs = (TDefinitions) getModel();
    validate( defs );
    for (ModelIterator ptIter = defs.portTypeIter(); ptIter.hasNext(); ) {
      TPortType portType = (TPortType) ptIter.next();
      checkPortType( portType, defs );
      configurator.addPortType( portType );
    }
    if (WSDLroot.get( null, defs.key() ) == null) {
      WSDLroot.add( defs.getParentRelationID(), defs );
    }
  }

  //-----------static code ------------------------------------------------------------------

  public static ModelIterator definitionsIter() {
    return WSDLroot.iterator(WSDLFactory.NAMESPACE.getQName("definitions"), null);
  }

  public static TMessage findMessageDef (QName msgName) {
    for (ModelIterator defIter = definitionsIter(); defIter.hasNext(); ) {
      TDefinitions def = (TDefinitions) defIter.next();
      if (Namespace.getInstance(def.getTargetNamespace()) == msgName.getNamespace()) {
        return def.findMessageDef( msgName );
      }
    }
    return null;
  }

  public static TPortType findPortType (QName portTypeName) { //todo: nur für process verwendet?
    for (ModelIterator defIter = definitionsIter(); defIter.hasNext(); ) {
      TDefinitions def = (TDefinitions) defIter.next();
      if (Namespace.getInstance(def.getTargetNamespace()) == portTypeName.getNamespace()) {
        return def.findPortType( portTypeName );
      }
    }
    return null;
  }

  //----------- public methods ------------------------------------------------------------------

  /**
   * Perform basic checks after loading a WSDL nition file.
   * @param defs the loaded WSDL definitions
   * @return true if all checks are successfull
   * @throws ServiceException forward exceptions thrown by the checks
   */
  public static boolean validate(TDefinitions defs) throws IOException {
    checkOperations(defs);
    return true;
  }

  /**
   * Creates root relations having name of message. These are needed for compatibility with old
   * message format used until 2006-11-10
   * @param messageDef the message definition
   */
  public static void registerMessageRelation( TMessage messageDef ) throws IOException {
    TPart msgPart;
    QName partTypeName;
    Type partType;
    Namespace msgNS = messageDef.getName().getNamespace();
    ModelFactory msgFactory = ModelFactoryTable.getInstance().getFactory( msgNS );
    if (msgFactory == null) {
      Logger.warn( "No factory defined for message name=" + messageDef.getName()+" - creating DefaultModelFactory" );
      // Create a factory: needed for registering message definitions
      msgFactory = new DefaultModelFactory( msgNS, null, null );
    }

    if (msgFactory.getRootRelation( messageDef.getName() ) == null) {
      DefaultComplexType msgType = new DefaultComplexType(messageDef.getName(), SequenceModel.class.getName(), 0);
      for (ModelIterator partIter = messageDef.partIter(); partIter.hasNext(); ) {
        msgPart = (TPart) partIter.next();
        partTypeName = msgPart.getType();
        if (partTypeName == null) {
          QName elemName = msgPart.getElement();
          ModelFactory factory = ModelFactoryTable.getInstance().getFactory( elemName.getNamespace() );
          if (factory == null) {
            throw new IllegalArgumentException("Factory not found for element name="+elemName);
          }
          Relation rel = factory.getRootRelation(elemName);
          if (rel == null) {
            throw new IllegalArgumentException("Root element not found, name="+elemName);
          }
          partType = rel.getChildType();
        }
        else {
          ModelFactory factory = ModelFactoryTable.getInstance().getFactory( partTypeName.getNamespace() );
          if (factory == null) {
            throw new IllegalArgumentException("Factory not found for type name="+partTypeName);
          }
          partType = factory.getType(partTypeName);
          if (partType == null) {
            throw new IllegalArgumentException("Type definition not found, name="+partTypeName);
          }
        }
        msgType.addRelation(msgPart.getName(), partType, 0, 1);
      }
      msgFactory.addRootRelation( messageDef.getName(), msgType, 0, Integer.MAX_VALUE, null);
    }
  }

  /**
   * Only parts with element are supported by now.
   * @param defs
   * @throws ServiceException
   */
  private static void checkMessage(TMessage msg, TDefinitions defs) throws IOException {
    for (ModelIterator partIter = msg.partIter(); partIter.hasNext(); ) {
      TPart part = (TPart) partIter.next();
      getPartRelation( part );
    }
    // pru, 2007-04-11 Called only for compatibility reasons to old message format used until 2006-11-10
    registerMessageRelation( msg );
  }

  public static Relation getPartRelation( TPart part ) throws IOException {
    Relation relation;
    QName partName = part.getName();
    QName typeName = part.getType();
    QName elemName = part.getElement();

    ModelFactory partFactory = ModelFactoryTable.getInstance().getFactory( partName.getNamespace() );
    if (partFactory == null) {
      throw new IOException( "No factory defined for part name=" + partName );
    }
    relation = partFactory.getRootRelation( partName );
    if (relation == null) {
      //----- Find/create dummy relation for part
      Type type;
      if (elemName == null) {
        if (typeName == null) {
          throw new IOException( "Neither type nor element defined for part name=" + partName );
        }
        ModelFactory typeFactory = ModelFactoryTable.getInstance().getFactory( typeName.getNamespace() );
        if (typeFactory == null) {
          throw new IOException( "No factory defined for type name=" + typeName );
        }
        type = ModelFactoryTable.getInstance().getFactory( typeName.getNamespace() ).getType( typeName );
        if (type == null) {
          throw new IOException( "Type is not supported by factory, type name=" + typeName );
        }
      }
      else {
        ModelFactory elementFactory = ModelFactoryTable.getInstance().getFactory( elemName.getNamespace() );
        if (elementFactory == null) {
          throw new IOException( "No factory defined for element name=" + elemName );
        }
        Relation elemRelation = elementFactory.getRootRelation( elemName );
        if (elemRelation == null) {
          throw new IOException( "Element is not supported by factory, type name=" + elemName );
        }
        type = elemRelation.getChildType();
      }
      relation = partFactory.addRootRelation( partName, type, 0, Integer.MAX_VALUE, null );
    }
    else {
      if (typeName == null) {
        if (elemName == null) {
          throw new IOException( "Neither type nor element defined for part name=" + partName );
        }
        ModelFactory elementFactory = ModelFactoryTable.getInstance().getFactory( elemName.getNamespace() );
        if (elementFactory == null) {
          throw new IOException( "No factory defined for element name=" + elemName );
        }
        Relation elemRelation = elementFactory.getRootRelation( elemName );
        if (elemRelation == null) {
          throw new IOException( "Element is not supported by factory, type name=" + elemName );
        }
        typeName = elemRelation.getChildType().name();
      }
      if (relation.getChildType().name() != typeName) {
        throw new IOException( "Part relation already exists in factory with different type="
            + relation.getChildType().name() + ", part name=" + partName + ", part type=" + typeName );
      }
    }
    return relation;
  }

  private static void checkOperations(TDefinitions defs) throws IOException {
    TPortType portType;
    for (ModelIterator portIter=defs.portTypeIter(); portIter.hasNext();) {
      portType = (TPortType) portIter.next();
      checkPortType( portType, defs );
    }
  }

  private static void checkPortType( TPortType portType, TDefinitions defs ) throws IOException {
    for (ModelIterator opIter=portType.operationIter(); opIter.hasNext();) {
      TOperation op = (TOperation) opIter.next();
      TParam param = op.getInput();
      if (param != null) {
        checkParam(param, op, portType, defs);
      }
      param = op.getOutput();
      if (param != null) {
        checkParam(param, op, portType, defs);
      }
    }
  }

  private static void checkParam(TParam param, TOperation op, TPortType portType, TDefinitions defs) throws IOException {
    QName msgName = param.getMessage();
    TMessage msg = defs.findMessageDef(msgName);
    if (msg == null) {
      throw new IOException( "unknown message '" + msgName + "' for operation '" + op.getName() + "' in service '" + portType.getName() + "'");
    }
    checkMessage(msg, defs);
  }

  public static void loadImports( TDefinitions defs, Vector wsdlDefinitions ) throws IOException {
    ModelIterator it = defs.importIter();
    while (it.hasNext()) {
      TImport imporT = (TImport) it.next();
      String location = imporT.getLocation();
      if (location.endsWith(".xsd")) {
        ModelFactory factory = ModelFactoryTable.getInstance().getFactory( Namespace.getInstance(imporT.getNamespace()) );
        if (factory == null) {
          //-- We must not use Schema.loadXSD because we cannot expect that all platforms contain Schema support.
          //   Compiler will fail for those  platforms, if reference to Schema exsists.
          XmlReader reader = new XmlReader();
          InputStream in = null;
          try {
            in = SystemManager.getInstance().getStorage(null).read(location);
            Model schema = reader.loadData( in, null );
          }
          finally {
            StreamHelper.close( in );
          }
        }
      }
      else if (location.endsWith(".wsdl")) {
        try {
          TDefinitions childDefs = loadWSDL(location);
          wsdlDefinitions.addElement( childDefs );
        }
        catch (Exception ex) {
          throw new IOException("Error loading import from location="+location+" msg="+ex.getMessage());
        }
      }
      else {
        throw new IOException("Unknown import file type: location="+location);
      }
    }
  }

  /**
   * Loads WSDL definitions and imported WSDL definitions.
   * @param fileName      the file or resource conatainig WSDL
   * @return              the WSDL definitions contained in fileName
   * @throws IOException if WSDL file could not be loaded
   */
  public static TDefinitions loadWSDL( String fileName ) throws IOException {
    Vector      wsdlDefinitions = new Vector();
    Storage storage = SystemManager.getInstance().getStorage(null);
    InputStream in = null;
    TDefinitions defs;
    try {
      in = storage.read(fileName);
      if (in == null) {
        throw new IOException( "resource not found: "+fileName );
      }
      XmlReader reader = new XmlReader();
      defs = (TDefinitions) reader.loadData( in, null );
    }
    finally {
      StreamHelper.close( in );
    }
    WSDLroot.add(defs.getParentRelationID(), defs);
    loadImports( defs, wsdlDefinitions );

    return defs;
  }
}
