/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.wsdl;

import de.pidata.models.config.Binding;
import de.pidata.models.config.ConfigFactory;
import de.pidata.models.config.Configurator;
import de.pidata.models.config.ModelLoader;
import de.pidata.models.config.Parameter;
import de.pidata.models.service.Operation;
import de.pidata.models.service.PortType;
import de.pidata.models.service.Service;
import de.pidata.service.base.ServiceException;
import de.pidata.models.service.ServiceManager;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.Model;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

/**
 * @see <a href="http://www.w3.org/TR/wsdl.html>WSDL W3C Note</a>
 * @see <a href="http://schemas.xmlsoap.org/soap/envelope>SOAP Envelope Schema</a>
 * This WSDL service implements a default binding for SOAP. Regarding to the
 * official SOAP envelope schema this will be the only one which is valid!
 * Restrictions for this implementation:
 * - only operation style 'document' is supported - no chance for 'rpc'
 * - only 'use=literal' is supported - no processing of 'use=encoded'
 * - only part definitions with attribute 'element' are supported - no chance for 'type'
 * - no support for overloading - an operation name must be unique within a port type
 */
public abstract class WsdlService implements Service {

  private static final QName paramWSDL = ConfigFactory.NAMESPACE.getQName( "wsdl" );

  protected TDefinitions   defs;
  protected PortType      portType;
  protected Namespace namespace;
  protected ServiceManager serviceManager;

  public void init( ServiceManager serviceManager, Configurator configurator, Binding binding ) throws Exception {
    this.serviceManager = serviceManager;
    Parameter wsdlParam = binding.getParameter( paramWSDL );
    if (wsdlParam == null) {
      throw new IllegalArgumentException( "Missing config parameter: "+paramWSDL+", serviceName="+binding.getServiceName() );
    }
    QName wsdlName = ConfigFactory.NAMESPACE.getQName( wsdlParam.getValue() ); 
    ModelLoader wsdlLoader = (ModelLoader) configurator.getInstance( wsdlName );
    if (wsdlLoader == null) {
      throw new IllegalArgumentException( "WSDL not loaded, name="+wsdlName );
    }
    this.defs = (TDefinitions) wsdlLoader.getModel();
    QName portTypeName = binding.getPortType();
    if (portTypeName == null) {
      throw new IllegalArgumentException( "Binding definition is missing portTypeName for serviceName="+binding.getServiceName() );
    }
    this.portType = configurator.getPortType( portTypeName );
    if (this.portType == null) {
      throw new IllegalArgumentException( "PortType definition not found, name="+portTypeName+", wsdlName="+wsdlName );
    }
    this.namespace = Namespace.getInstance( defs.getTargetNamespace() );
  }

 public PortType portType() {
    return portType;
  }

  public String getTargetID() {
    return this.serviceManager.getTargetID();
  }

  protected abstract Model invoke(Context caller, Operation operation, Model input) throws ServiceException;

  public final Model invoke(Context caller, QName operationID, Model input) throws ServiceException {
    Operation operation = findOperation(operationID);
    return invoke(caller, operation, input);
  }

  public Operation findOperation(QName operationID) {
    Operation operation;
    operation = this.portType.getOperation( operationID );
    if (operation != null) {
      return operation;
    }
    throw new IllegalArgumentException("Operation not defined: opID="+operationID);
  }

  /**
   * Tells this service to shut down, e.g. close database connections
   */
  public void shutdown() {
    //do nothing
  }
}
