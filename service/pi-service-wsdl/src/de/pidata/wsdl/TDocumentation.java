// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)
// Tue Oct 25 14:45:35 CEST 2005

package de.pidata.wsdl;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.complex.AnyElement;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.Hashtable;

public class TDocumentation extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://schemas.xmlsoap.org/wsdl/" );


  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "tDocumentation" ), TDocumentation.class.getName(), 0 );
    TYPE = type;
    type.addRelation( AnyElement.ANY_ELEMENT, AnyElement.ANY_TYPE, 0, Integer.MAX_VALUE);
  }

  public TDocumentation() {
    this( null );
  }

  public TDocumentation( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public TDocumentation(QName modelID, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(modelID, TYPE, attributeNames, anyAttribs, childNames);
  }

  protected TDocumentation(QName modelID, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(modelID, type, attributeNames, anyAttribs, childNames);
  }

}
