// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)
// Tue Oct 25 14:45:35 CEST 2005

package de.pidata.wsdl;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.QNameType;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.Hashtable;

public class TService extends TExtensibleDocumented {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://schemas.xmlsoap.org/wsdl/" );

  public static final QName ID_PORT = NAMESPACE.getQName("port");
  public static final QName ID_NAME = NAMESPACE.getQName("name");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "tService" ), TService.class.getName(), 1, TExtensibleDocumented.TYPE );
    TYPE = type;
    type.addKeyAttributeType( 0, ID_NAME, QNameType.getNCName() );
    type.addRelation( ID_PORT, TPort.TYPE, 0, Integer.MAX_VALUE);
  }

  public TService() {
    this( null );
  }

  public TService( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public TService(QName modelID, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(modelID, TYPE, attributeNames, anyAttribs, childNames);
  }

  protected TService(QName modelID, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(modelID, type, attributeNames, anyAttribs, childNames);
  }

  /**
   * Returns the attribute name.
   *
   * @return The attribute name
   */
  public QName getName() {
    return (QName) get( ID_NAME );
  }

  /**
   * Set the attribute name.
   *
   * @param name new value for attribute name
   */
  public void setName( QName name ) {
    set( ID_NAME, name );
  }

  public TPort getPort( QName portID ) {
    return (TPort) get( ID_PORT, portID );
  }

  public void addPort( TPort port ) {
    add( ID_PORT, port );
  }

  public void removePort( TPort port ) {
    remove( ID_PORT, port );
  }

  public ModelIterator portIter() {
    return iterator( ID_PORT, null );
  }

  public int portCount() {
    return childCount( ID_PORT );
  }

}
