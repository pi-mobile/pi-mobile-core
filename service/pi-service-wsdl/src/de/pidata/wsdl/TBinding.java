// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)
// Tue Oct 25 14:45:35 CEST 2005

package de.pidata.wsdl;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.QNameType;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.Hashtable;

public class TBinding extends TExtensibleDocumented {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://schemas.xmlsoap.org/wsdl/" );

  public static final QName ID_NAME = NAMESPACE.getQName("name");
  public static final QName ID_TYPE = NAMESPACE.getQName("type");
  public static final QName ID_OPERATION = NAMESPACE.getQName("operation");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "tBinding" ), TBinding.class.getName(), 1, TExtensibleDocumented.TYPE );
    TYPE = type;
    type.addKeyAttributeType( 0, ID_NAME, QNameType.getNCName() );
    type.addAttributeType( ID_TYPE, QNameType.getInstance());
    type.addRelation( ID_OPERATION, TBindingOperation.TYPE, 0, Integer.MAX_VALUE);
  }

  public TBinding() {
    this( null );
  }

  public TBinding( QName id ) {
    super( id, TYPE, null, null, null );
  }

  public TBinding(QName modelID, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(modelID, TYPE, attributeNames, anyAttribs, childNames);
  }

  protected TBinding(QName modelID, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(modelID, type, attributeNames, anyAttribs, childNames);
  }

  /**
   * Returns the attribute name.
   *
   * @return The attribute name
   */
  public QName getName() {
    return (QName) get( ID_NAME );
  }

  /**
   * Set the attribute name.
   *
   * @param name new value for attribute name
   */
  public void setName( QName name ) {
    set( ID_NAME, name );
  }

  /**
   * Returns the attribute type.
   *
   * @return The attribute type
   */
  public QName getType() {
    return (QName) get( ID_TYPE );
  }

  /**
   * Set the attribute type.
   *
   * @param type new value for attribute type
   */
  public void setType( QName type ) {
    set( ID_TYPE, type );
  }

  public TBindingOperation getOperation( QName operationID ) {
    return (TBindingOperation) get( ID_OPERATION, operationID );
  }

  public void addOperation( TBindingOperation operation ) {
    add( ID_OPERATION, operation );
  }

  public void removeOperation( TBindingOperation operation ) {
    remove( ID_OPERATION, operation );
  }

  public ModelIterator operationIter() {
    return iterator( ID_OPERATION, null );
  }

  public int operationCount() {
    return childCount( ID_OPERATION );
  }

}
