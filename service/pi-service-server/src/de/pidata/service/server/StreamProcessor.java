/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public interface StreamProcessor {

    /**
     * Wird aufgerufen, wenn die Umgebung angehelten wird.
     */
    public void shutdown();

    /**
     * Wird aufgerufen, wenn ein neuer Request rein kommt
     * @param props        Header-properties, z.B. aus HTTP-Request
     * @param inStream     input-Stream
     * @param outputStream outputStream
     */
    public void process( Properties props, InputStream inStream, OutputStream outputStream ) throws IOException;
}
