/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.server;

import de.pidata.comm.soap.Body;
import de.pidata.comm.soap.Envelope;
import de.pidata.comm.soap.SoapFactory;
import de.pidata.log.Logger;
import de.pidata.models.config.Configurator;
import de.pidata.models.service.Service;
import de.pidata.models.service.ServiceManager;
import de.pidata.models.tree.*;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.models.xml.binder.XmlWriter;
import de.pidata.qnames.QName;
import de.pidata.service.log.BOStateType;
import de.pidata.service.log.BusinessObject;
import de.pidata.service.log.LogManager;
import de.pidata.system.base.Storage;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.SystemManager;
import de.pidata.system.desktop.DesktopSystem;

import java.io.InputStream;
import java.util.List;
import java.util.Vector;

public class ServerController extends ServiceManager {

  private Configurator config;
  private List processes;

  public ServerController( Context context ) throws Exception {
    super();
    this.config = Configurator.loadConfig( null, context );
  }

  public ServerController( InputStream configStream, Context context ) throws Exception {
    super();
    this.config = Configurator.loadConfig( configStream, context );
  }

  protected ServerController( String targetID ) {
    super( targetID );
  }

  protected ServiceManager createInstance( String targetID ) {
    ServerController controller = new ServerController( targetID );
    controller.config = config;
    controller.processes = processes;
    return controller;
  }

  /**
   * Lookup Service with name <code>serviceName</code>
   *
   * @param serviceName  service name
   * @return the provider that has been registered with serviceName or null if none can be found.
   */
  public Service getService(String serviceName) {
    Service service = super.getService(serviceName);
    if ((service == null) && (this.targetID != null)) {
      service = config.createClientService( this, serviceName );
    }
    return service;
  }

  public Configurator getConfigurator() {
    return config;
  }

  public void check4Updates(LogManager logMgr) {
    BusinessObject bizObject;
    QName          boName;
    BOStateType    boState;

    Vector businessObjects = config.getBusinessObjects();
    for (int i = 0; i < businessObjects.size(); i++) {
      bizObject = (BusinessObject) businessObjects.elementAt(i);
      boName = bizObject.getName();
      boState = logMgr.getBOState(boName);
      if (boState == null) {
        BOStateType clientState = logMgr.getBOState( logMgr.getTargetQName() );
        if (clientState == null) {
          Logger.info("No client BOState defined yet...");
          return;
        }
        boState = new BOStateType(boName);
        boState.setUserID(clientState.getUserID());
      }
      try {
        bizObject.setAuthentification(boState.getUserID().getName(), SystemManager.getInstance().getProperty("backoffice.password", null )); //todo password woher
        boState = bizObject.check4Updates(logMgr, boState);
        logMgr.setBOState(boName, boState);
      }
      catch (Exception ex) {
        Logger.error("Error while checking BO for updates, boName="+boName, ex);
      }
    }
  }

  /**
   * Creates a new SOAP Envelope
   * @return the new SOAP Envelope
   */
  private static Envelope createEnvelope() {
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( SoapFactory.NAMESPACE );
    Envelope envelope = (Envelope) factory.createInstance( SoapFactory.ENVELOPE_TYPE.name(), SoapFactory.ENVELOPE_TYPE, null, null, null);
    envelope.namespaceTable().addNamespace( SoapFactory.NAMESPACE, "sop" );
    Body body = (Body) factory.createInstance( SoapFactory.BODY_TYPE );
    envelope.setBody(body);
    return envelope;
  }

  public static void main(String[] args) {
    try {
      new DesktopSystem();
      if (args.length != 1) {
        System.out.println("Usage: ServerController messageFile.xml");
      }
      Storage storage = SystemManager.getInstance().getStorage(null);
      InputStream cfgIn = null;
      ServerController srvCtrl; 
      try {
        cfgIn = storage.read("config.xml");
        srvCtrl = new ServerController( cfgIn, SystemManager.getInstance().createContext() );
      }
      finally {
        StreamHelper.close( cfgIn );
      }
      XmlReader reader = new XmlReader();
      InputStream in = null;
      Envelope envelope;
      try {
        in = storage.read(args[0]);
        envelope = (Envelope) reader.loadData( in, null );
      }
      finally {
        StreamHelper.close( in );
      }
        
      Body body = envelope.getBody();
      Envelope replyEnvelope = createEnvelope();
      Model replyBody = replyEnvelope.getBody();
      for (ModelIterator serviceIter = body.iterator(null, null); serviceIter.hasNext(); ) {
        Model service = serviceIter.next();
        QName portTypeName = null;
        QName opName = null; // todo
        Model output = srvCtrl.invokeService( SystemManager.getInstance().createContext(), portTypeName, opName, service );
        if (output != null) {
          replyBody.add(output.getParentRelationID(), output);
        }
      }
      XmlWriter writer = new XmlWriter(storage);
      writer.write("ServerOutput.xml", replyEnvelope, envelope.type().name());
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
