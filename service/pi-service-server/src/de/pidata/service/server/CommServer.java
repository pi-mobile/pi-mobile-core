/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.server;

import de.pidata.comm.client.base.SoapSender;
import de.pidata.comm.soap.Body;
import de.pidata.comm.soap.Envelope;
import de.pidata.comm.soap.Fault;
import de.pidata.comm.soap.FunctionType;
import de.pidata.comm.soap.Header;
import de.pidata.comm.soap.SoapFactory;
import de.pidata.log.Logger;
import de.pidata.models.config.ConfigFactory;
import de.pidata.qnames.NamespaceTable;
import de.pidata.service.base.ServiceException;
import de.pidata.models.service.ServiceManager;
import de.pidata.models.tree.*;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.models.xml.binder.XmlWriter;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.service.log.LogEntry;
import de.pidata.service.log.LogManager;
import de.pidata.service.log.LogState;
import de.pidata.service.log.MessageType;
import de.pidata.service.log.SyncListener;
import de.pidata.system.base.Storage;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.SystemManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringBufferInputStream;
import java.util.Properties;
import java.util.Vector;

public class CommServer implements StreamProcessor {

  private static final boolean DEBUG = false;

  public static final Namespace NAMESPACE = Namespace.getInstance( "de.pidata.comm" );

  private ServerController serviceManager;
  private SyncListener syncListener;

  public CommServer() throws Exception {
    this( new ServerController( SystemManager.getInstance().createContext() ) );
  }

  public CommServer(ServerController serviceManager) {
    this.serviceManager = serviceManager;
    this.syncListener = (SyncListener) serviceManager.getConfigurator().getInstance( ConfigFactory.NAMESPACE.getQName( "SyncListener" ) );
  }

  /**
   *
   *
   * <pre>
   * Sync scenario: client calling a second time while processing first request has not finished
   *                   lastSyncClient   lastSyncServer    newSyncServer
   *     1st call in       t1               t0                 t1
   *              out   -- hangup --        t1                 t2
   *     2nd call in       t1               t1                 t2
   *              out      t3               t1                 t3
   * </pre>
   * @param clientLogState
   * @return
   * @throws IOException
   */
  private String processLogSync( LogState clientLogState)
  throws IOException {
    String clientID = clientLogState.getSystemID().getName();
    LogManager logManager = LogManager.getInstance( clientID );
    DateObject temp = new DateObject( DateTimeType.TYPE_DATETIME, System.currentTimeMillis() );
    String syncTime = DateTimeType.toDateTimeString( temp, true );

    //----- Check if client's and server's lastSyncDate are equal. If not ignore requests and
    //      remove all pending logs.
    //      ATTENTION: Do not use logManager.createLogState(), it modifies lastSyncTime and newSyncTime
    String lastSyncClient = clientLogState.getLastSyncTime();
    String lastSyncServer = logManager.getLastSyncTime();
    String newSyncServer = logManager.getNewSyncTime();
    Logger.info("Process Log Sync for clientID="+clientID+", lastSyncServer="+lastSyncServer
        +", lastSyncClient="+lastSyncClient+", newSyncServer="+newSyncServer+", syncTime="+syncTime );

    //-- Übernehme die bei letzter Comm an den client übermittelte Zeit als neuen LastSync-Zeitstempel
    if ((lastSyncClient != null) && lastSyncClient.equals( newSyncServer )) {
      logManager.setLastSyncTime(newSyncServer);
      lastSyncServer = newSyncServer;
    }
    else {
      Logger.info("Process Log Sync: lost last server reply (lastSyncClientMilli != newSyncServerMilli)");
    }

    //-- Out of sync wenn erster Kontakt mit Client oder lastSyncClient ist weder newSyncServer noch lastSyncServer
    if ((lastSyncClient == null) || (!lastSyncClient.equals( lastSyncServer ))) {
      Logger.info("Server and client are out of sync: lastSyncServer="+lastSyncServer+" lastSyncClient="+lastSyncClient+" newSyncServer="+newSyncServer);
      logManager.outOfSync();
      if (syncListener != null) syncListener.outOfSync(logManager, lastSyncClient, lastSyncServer);
    }
    else {
      logManager.setInSync();
      Integer received = clientLogState.getReceived();
      if (received != null) {
        logManager.confirmReceival(received.intValue());
      }
    }
    logManager.setNewSyncTime(syncTime);
    Logger.info("    Process Log Sync finished for clientID="+clientID+", newSyncTime="+syncTime );
    return clientID;
  }

  private void processLog( Context context, LogManager logMgr, LogEntry logEntry ) throws IOException, ServiceException {
    Model input;
    int logNr = logEntry.getSEQ().intValue();
    if (logNr > logMgr.getSeqReceived()) {
      QName opID = logEntry.getOperation();
      QName serviceID = logEntry.getService();

      //TODO Context sinnvoll initialisieren
      MessageType message = logEntry.getInput();
      ModelIterator iter = message.iterator(null, null);
      if (iter.hasNext()) input = iter.next();
      else input = null;

      Model result = serviceManager.invokeService(context, serviceID, opID, input);
      //TODO result als result kennzeichnen
      if (result != null) {
        LogManager.createLogEntry(serviceID, opID, result);
      }
      logMgr.confirmProcessing(logEntry.getSEQ().intValue());
    }
  }

  private void processFunctionType( String soapAction, FunctionType functionType ) throws IOException, ServiceException { //todo...
    QName serviceID = functionType.getParentRelationID();
    QName opID = null;
    if (soapAction != null) {
      opID = serviceID.getNamespace().getQName(soapAction);
    }

    //TODO Context sinnvoll initialisieren
    Model result = serviceManager.invokeService( SystemManager.getInstance().createContext(), serviceID, opID, functionType );

    //TODO result verarbeiten

  }

  private String extractContentType( String contentType, String mimeAttrName ) {
    int pos = contentType.indexOf( mimeAttrName );
    if (pos < 0) {
      return null;
    }
    pos += mimeAttrName.length();
    int start = contentType.indexOf( '=', pos );
    if (start < 0) {
      throw new IllegalArgumentException( "HTTP property 'Content-Type' is missing '=' after index="+pos+ "', Content-Type="+contentType);
    }
    start++;
    int end = contentType.indexOf( ';', start );
    if (end < 0) end = contentType.length();
    return contentType.substring( start, end ).trim();
  }

  public void process( Properties props, InputStream in, OutputStream out ) throws IOException {
    String   soapAction = props.getProperty( SoapSender.HEADER_SOAP_ACTION );
    String   mimeType   = props.getProperty( SoapSender.HEADER_CONTENT_TYPE );
    String   senderID   = props.getProperty( SoapSender.HEADER_SENDER_ID );
    String   from       = props.getProperty( SoapSender.HEADER_FROM );
    String   userAgent  = props.getProperty( SoapSender.HEADER_USER_AGENT );
    String   headerMaxLogs = props.getProperty( SoapSender.HEADER_MAX_LOGS );
    Header   header;
    Body     body;
    Model    service;
    String   clientID = null;
    QName    userID = null;
    LogManager logMgr = null;
    Model      output;
    boolean stateReceived = false;
    String  serviceName = null;
    String  opName = null;
    boolean clientSupportsRepeat = (senderID != null);
    int     crashPos = Integer.MAX_VALUE;
    boolean repeating = false;

    if (soapAction != null) {
      int pos = soapAction.indexOf( "/" );
      serviceName = soapAction.substring( 0, pos );
      opName = soapAction.substring( pos+1 );
    }

    if ((from != null) && (from.length() > 0)) {
      userID = NAMESPACE.getQName( from );
    }

    if (senderID != null) {
      //----- Remove namespace if existing
      int pos = senderID.lastIndexOf( ':' );
      if (pos >= 0) {
        clientID = senderID.substring( pos+1 );
      }
      else {
        clientID = senderID;
      }

      //----- Allow simulate stream truncation at specific char position for specific device 
      String path = clientID;
      String file = "crash.properties";
      Storage storage = SystemManager.getInstance().getStorage( path );
      if (storage.exists( file )) {
        InputStream crashPropFile = null;
        Properties crashProps;
        try {
          crashPropFile = storage.read( file );
          crashProps = new Properties();
          crashProps.load( crashPropFile );
        }
        finally {
          StreamHelper.close( crashPropFile );
        }
        crashPos = Integer.parseInt( crashProps.getProperty( "crashPosition" ) );
      }
    }

    StringBuilder inBuffer = new StringBuilder();
    String charset = extractContentType( mimeType, SoapSender.MIME_ATTR_CHARSET );

    //----- Message continues from index transmitted in mimeType
    String repeatName = extractContentType( mimeType, SoapSender.MIME_ATTR_REPEAT );
    int    fromIndex = 0;
    if (repeatName != null) {
      fromIndex  = Integer.parseInt( extractContentType( mimeType, SoapSender.MIME_ATTR_INDEX ) );
      if (fromIndex > 0) {
        Logger.info( "Continuing message '" + repeatName + "' from index=" + fromIndex );
        repeating = true;
        InputStream lastMsgIn = null;
        try {
          lastMsgIn = LogManager.getInstance( clientID ).readBuffer( repeatName );

          InputStreamReader inReader = new InputStreamReader( lastMsgIn );
          BufferedReader reader = new BufferedReader( inReader );
          soapAction = reader.readLine();
          StreamHelper.stream2Buffer( reader, inBuffer, fromIndex, false);
          reader.close();
          inReader.close();
        }
        catch (Exception ex) {
          String msg = "Could not read the buffered message name=" + repeatName;
          LogManager.getInstance( clientID ).setRepeat( null, -1 );
          Logger.error( msg, ex );
          throw new IOException( msg );
        }
        finally {
          StreamHelper.close( lastMsgIn );
        }
      }
    }

    //----- Read input into buffer. If connection gets lost dump received bytes into a file
    boolean success = StreamHelper.stream2Buffer( in, inBuffer, crashPos, true);
    if (success) {
      in = new StringBufferInputStream( inBuffer.toString() );
      if (repeatName != null) {
        try {
          LogManager.getInstance( clientID ).setRepeat( null, -1 );
          LogManager.getInstance( clientID ).removeBuffer( repeatName );
        }
        catch (IOException ex) {
          Logger.error( "Could not delete repeat buffer name=" + repeatName, ex);
        }
      }
    }
    else {
      if (clientSupportsRepeat && (repeatName != null)) {
        OutputStream bufStream = null;
        fromIndex = inBuffer.length() - soapAction.length() - 1;
        if (fromIndex > 0) {
          try {
            bufStream = LogManager.getInstance( clientID ).writeBuffer( repeatName );
            OutputStreamWriter writer = new OutputStreamWriter( bufStream, charset );
            writer.write( soapAction + "\n" );
            StreamHelper.buffer2Stream( inBuffer, writer, Integer.MAX_VALUE, null );
            writer.flush();
          }
          finally {
            StreamHelper.close( bufStream );
          }
          LogManager.getInstance( clientID ).setRepeat( repeatName, fromIndex );
          Logger.info( "Transfer interrupted, storing message '" + repeatName + "' received chars="+fromIndex  );
        }
        else {
          LogManager.getInstance( clientID ).setRepeat( null, -1 );
          Logger.info( "Transfer interrupted at beginning (fromIndex="+fromIndex+") - message not stored" );
        }
      }
      return;
    }

    //----- Parse (now complete) received XML-Mesage
    XmlReader xmlReader = new XmlReader();
    Envelope envelope = (Envelope) xmlReader.loadData( in, null );

    //----- Look for sync information in header
    header = envelope.getHeader();
    if (header != null) {
      LogState logState = (LogState) header.get( LogState.TYPE.name(), null );
      
      //----- LogService call
      if (logState != null) {
        // We must not process sync times if message was repeated: Each repeat starts
        // with a log sync changing sync time. So the repeated message may contain
        // a too old sync time (think of at least three repeats of the same message).
        // Since we synced immediately before repeat we do not need processing client's
        // received count.
        if (!repeating) {
          clientID = processLogSync( logState );
        }

        logMgr = LogManager.getInstance( clientID );
        stateReceived = true;
        
        // set servicename and operation for compatibility with older clients
        serviceName = LogManager.SERVICE_LOG_MANAGER;
        opName = LogManager.OP_SYNC.getName();
      }
    }

    //----- Create context
    String programName = null;
    String programVersion = null;
    if (userAgent != null) {
      int pos = userAgent.indexOf( " " );
      if (pos > 0) {
        userAgent = userAgent.substring( 0, pos );
      }
      pos = userAgent.indexOf( "/" );
      if (pos >= 0) {
        programName = userAgent.substring( 0, pos );
        programVersion = userAgent.substring( pos+1 );
      }
      else {
        programName = userAgent;
      }
    }
    Context context = new Context( clientID, userID, programName, programVersion );

    body = envelope.getBody();
    if (LogManager.SERVICE_LOG_MANAGER.equals( serviceName )) {
      try {
        //----- Following synchronized block makes shure we are processing maximum one request at a time
        //      per client. This is necessary because a client might give up waiting for us while we
        //      are processing a request. If the the client calls us again we must not start processing
        //      te retrying client request until the older one has finished processing.
        Envelope response;
        synchronized (logMgr) {
          // TODO Logs in eigenem Thread verarbeiten und hier die Client-Verbindung halten bzw. nach timeout kappen
          if (syncListener != null) syncListener.beforeSync( context, logMgr );
          if (body != null) {
            Logger.info( "Start processing logs for clientID="+clientID );
            for (ModelIterator serviceIter = body.iterator(null, null); serviceIter.hasNext(); ) {
              service = serviceIter.next();
              if (logMgr.isSynchronized()) {
                if (service instanceof LogState) {
                  processLogSync((LogState) service);
                }
                else if (service instanceof LogEntry) {
                  processLog( context, logMgr, (LogEntry) service );
                }
                else if (service instanceof FunctionType) {
                  processFunctionType( soapAction, (FunctionType) service);
                }
                else {
                  throw new IllegalArgumentException("Service not supported: type="+service.type().name());
                }
              }
              else {
                // immediately stop processing and return if out of sync
                break;
              }
            }
            Logger.info( "    finished processing logs for clientID="+clientID );
          }
          if (logMgr.isSynchronized()) {
            serviceManager.check4Updates(logMgr);
          }
          response = createResponse( logMgr, clientSupportsRepeat, headerMaxLogs );
          Logger.info( "    finished creating response for clientID="+clientID );
          if (syncListener != null) syncListener.afterSync( context, logMgr );
        }
        doSend( out, response );
      }
      catch (ServiceException ex) {
        Logger.error("Error processing service log", ex);
        doSend( out, createFault(logMgr, ex, headerMaxLogs) );
      }
    }

    //----- Normal Service call
    else {
      //throw new IOException("Rejecting SOAP envelope: client's log state is missing in header");
      try {
        if (soapAction == null) {
          throw new ServiceException(ServiceException.UNKNOWN_OPERATION, "no SOAP action found");
        }
        Body replyBody = (Body) ServiceManager.getInstance().invokeService( context, serviceName, opName, body);
        Envelope replyEnvelope = createEnvelope(null);
        if (replyBody == null) {
          replyEnvelope.setBody( new Body() );
        }
        else {
          replyEnvelope.setBody( replyBody );
        }
        doSend( out, replyEnvelope );
      }
      catch (ServiceException ex) {
        Logger.error("Error processing service call", ex);
        doSend( out, createFault(null, ex, headerMaxLogs) );
      }
    }
  }

  public void doSend(OutputStream outStream, Envelope message) throws IOException {
    int sum;
    try {
      XmlWriter xmlWriter = new XmlWriter(null);
      OutputStreamWriter writer = new OutputStreamWriter(outStream);
      if (DEBUG) Logger.debug(xmlWriter.writeXML(message, message.type().name()).toString());
      sum = xmlWriter.write(message, writer, null);
      writer.flush();
      outStream.flush();
      writer.close();
    }
    catch (Exception e) {
      Logger.error("could not send ", e);
      throw new IOException(e.getMessage());
    }
  }

  /**
   * Creates a new SOAP Envelope
   * @param logState may be null
   * @return the new SOAP Envelope
   */
  private Envelope createEnvelope(LogState logState) {
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( SoapFactory.NAMESPACE );
    Envelope envelope = (Envelope) factory.createInstance( SoapFactory.ENVELOPE_TYPE.name(), SoapFactory.ENVELOPE_TYPE, null, null, null);
    envelope.namespaceTable().addNamespace( SoapFactory.NAMESPACE, "sop" );

    Header header  = (Header) factory.createInstance( SoapFactory.HEADER_TYPE );
    if (logState != null) {
      header.add(LogState.TYPE.name(), logState);
    }
    envelope.setHeader(header);

    Body body = (Body) factory.createInstance( SoapFactory.BODY_TYPE );
    envelope.setBody(body);
    return envelope;
  }

  private Envelope createResponse( LogManager logManager, boolean clientSupportsRepeat, String headerMaxLogs ) throws IOException {
    LogState logState;
    if (logManager == null) {
      logState = null;
    }
    else {
      logState = logManager.createLogState( clientSupportsRepeat );
    }
    Envelope envelope = createEnvelope(logState);
    if (logManager != null) {
      Vector logs = logManager.getPendingLogs();
      Model body = envelope.getBody();
      int maxLogsPerEnvelope = Integer.MAX_VALUE;
      if (headerMaxLogs != null) {
        try {
          maxLogsPerEnvelope = Integer.parseInt( headerMaxLogs );
        }
        catch (Exception ex) {
          Logger.warn( "Invalid header property 'max-logs', value='"+headerMaxLogs+"'" );
        }
      }
      for (int i = 0; (i < logs.size()) && (i < maxLogsPerEnvelope); i++) {
        LogEntry log = (LogEntry) logs.elementAt(i);
        body.add(LogEntry.TYPE.name(), log);
      }
    }
    return envelope;
  }

  /**
   * Creates a new Envelope conatining all log entries to be send to the client and
   * as last entry the fault element for given exception
   * @param logMgr teh logManager to use
   * @param ex the exception to be plaaced as fault into envelope
   * @return the new Envelope conatining a fault
   */
  private Envelope createFault(LogManager logMgr, ServiceException ex, String headerMaxLogs ) throws IOException {
    Envelope env = createResponse(logMgr, false, headerMaxLogs);
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( SoapFactory.NAMESPACE );
    Fault fault = (Fault) factory.createInstance( SoapFactory.FAULT_TYPE.name(), SoapFactory.FAULT_TYPE, null, null, null );
    fault.setFaultactor("ServiceException");
    fault.setFaultcode( SoapFactory.NAMESPACE.getQName( ex.getCode() ) );
    fault.setFaultstring(ex.getMessage());
    env.getBody().add( SoapFactory.FAULT_TYPE.name(), fault);
    return env;
  }

  public void shutdown() {
    this.serviceManager.shutdown();
  }
}
