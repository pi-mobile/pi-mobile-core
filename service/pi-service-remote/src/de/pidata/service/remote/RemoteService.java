/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.remote;

import de.pidata.comm.client.base.Sender;
import de.pidata.comm.soap.Envelope;
import de.pidata.models.config.Binding;
import de.pidata.models.config.Configurator;
import de.pidata.models.config.Parameter;
import de.pidata.models.service.PortType;
import de.pidata.models.service.Service;
import de.pidata.service.base.ServiceException;
import de.pidata.models.service.ServiceManager;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

import java.io.IOException;

public class RemoteService implements Service {

  public static final QName SENDER = Binding.NAMESPACE.getQName( "sender" );

  protected Sender sender;
  protected String serviceName = null;
  protected boolean appendOpName = true;
  protected PortType portType;
  protected boolean syncron = true;
  protected int retryCount = 3;

  public RemoteService() {
  }

  public void init( ServiceManager serviceManager, Configurator configurator, Binding binding ) throws Exception {
    if (this.serviceName == null) {
      this.serviceName = binding.getServiceName();
    }
    if (this.portType == null) {
      QName portTypeName = binding.getPortType();
      if (portTypeName == null) {
        throw new IllegalArgumentException( "Binding definition is missing portTypeName for serviceName="+serviceName );
      }
      portType = configurator.getPortType( portTypeName );
      if (portType == null) {
        throw new IllegalArgumentException( "PortType not found for serviceName="+serviceName+", portType name="+portTypeName );
      }
    }
    Parameter senderParam = binding.getParameter( SENDER );
    if (senderParam == null) {
      this.sender = Sender.initComm();
    }
    else {
      String senderName = senderParam.getValue();
      throw new RuntimeException("TODO");
    }
  }

  /**
   * Creates a new remote service
   * @param sender
   * @param serviceName   the name of this service
   * @param appendOpName  if true operation's name is appended to serviceName
   */
  public RemoteService( PortType portType, Sender sender, String serviceName, boolean appendOpName, boolean syncron ) {
    this.portType = portType;
    this.sender = sender;
    this.serviceName = serviceName;
    this.appendOpName = appendOpName;
    this.syncron = syncron;
  }

  public PortType portType() {
    return this.portType;
  }

  public Model invoke( Context caller, QName operation, Model input ) throws ServiceException {
    Model result;
    Envelope request = Envelope.wrapRequest( input );
    int retry = this.retryCount;
    while (true) {
      try {
        if (syncron) {
          Envelope response = sender.invokeSynchron( serviceName, operation, caller, request, null, null );
          result = response.getBody();
        }
        else {
          sender.invoke( serviceName, operation, caller, request, null, null );
          result = null;
        }
        return result;
      }
      catch (IOException ex) {
        if (retry > 1) {
          retry--;
        }
        else {
          throw new ServiceException(ServiceException.SERVICE_COMM_ERROR, "Communication error, op="+operation.getName(), ex);
        }
      }
    }
  }

  /**
   * Tells this service to shut down, e.g. close database connections
   */
  public void shutdown() {
    // do nothing
  }
}
