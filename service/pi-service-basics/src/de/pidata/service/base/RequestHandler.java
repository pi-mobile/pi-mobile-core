/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.base;

import java.io.IOException;

/**
 * Interface for processing data arriving on a stream connection.
 * If multiple connections are allowed for each connection a new
 * instance of this interface has to be created.
 */
public interface RequestHandler {

  /**
   * Called by server after connection is established
   * @param clientID client's id if available
   * @param clientName client's human readable name if available
   * @param serviceProvider
   */
  public void connected( String clientID, String clientName, ServiceProvider serviceProvider );

  /**
   * Called by server after disconnect
   */
  public void disconnected();

  /**
   * Returns current Serviceprovider
   * @return current Serviceprovider or null if not connected
   */
  public ServiceProvider getServiceProvider();

  /**
   * Returns current clientID
   * @return current clientID or null if not connected
   */
  public String getClientID();

  /**
   * Returns current clientName
   * @return current clientName or null if not connected
   */
  public String getClientName();

  /**
   * Called by server whenever a character is received
   * @param ch the charcter received
   */
  public void received( char ch ) throws IOException;
}
