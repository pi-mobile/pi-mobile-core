/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.base;

import de.pidata.qnames.QName;

import java.util.Date;

/**
 * Created by pru on 26.04.16.
 */
public interface ParameterList {

  String DATAURI_SCHEME_CLASS = "class";

  String getDataUri();

  QName getName( int index );

  ParameterType getType( QName paramName );

  ParameterType getType( int index );

  int size();

  String getString( QName paramName );

  QName getQName( QName paramName );

  Integer getInteger( QName paramName );

  Boolean getBoolean( QName paramName );

  Date getDate( QName paramName );

  Double getDouble( QName paramName );

  String getString( int index );

  QName getQName( int index );

  Integer getInteger( int index );

  Boolean getBoolean( int index );

  Date getDate( int index );

  Double getDouble( int index );

  void setString( QName paramName, String paramValue );

  void setQName( QName paramName, QName paramValue );

  void setInteger( QName paramName, Integer paramValue );

  void setBoolean( QName paramName, Boolean paramValue );

  void setDate( QName paramName, Date dateValue );

  void setDouble( QName paramName, Double paramValue );

  String[] getStringArray( QName paramName );

  QName[] getQNameArray( QName paramName );

  Integer[] getIntegerArray( QName paramName );

  Boolean[] getBooleanArray( QName paramName );

  Date[] getDateArray( QName paramName );

  Double[] getDoubleArray( QName paramName );

  String[] getStringArray( int index );

  QName[] getQNameArray( int index );

  Integer[] getIntegerArray( int index );

  Boolean[] getBooleanArray( int index );

  Date[] getDateArray( int index );

  Double[] getDoubleArray( int index );

  void setStringArray( QName paramName, String[] paramValue );

  void setQNameArray( QName paramName, QName[] paramValue );

  void setIntegerArray( QName paramName, Integer[] paramValue );

  void setBooleanArray( QName paramName, Boolean[] paramValue );

  void setDateArray( QName paramName, Date[] paramValue );

  void setDoubleArray( QName paramName, Double[] paramValue );
}
