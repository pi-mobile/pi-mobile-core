/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.base;

import de.pidata.qnames.Namespace;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;

import java.util.Date;

/**
 * Created by pru on 26.04.16.
 */
public class AbstractParameterList implements ParameterList {

  public static final ParameterList EMPTY = new AbstractParameterList( null, new ParameterType[0], new QName[0], new Object[0] );

  public static final Namespace DEFAULT_NS = Namespace.getInstance( "de.pidata.service" );

  private String dataUri;
  private QName[] nameList;
  private ParameterType[] typeList;
  private Object[] valueList;

  protected AbstractParameterList( int paramCount ) {
    dataUri = DATAURI_SCHEME_CLASS + "://" + getClass().getName();
    nameList = new QName[paramCount];
    typeList = new ParameterType[paramCount];
    valueList = new Object[paramCount];
  }

  protected AbstractParameterList( String dataUri ) {
    this.dataUri = dataUri;
    nameList = new QName[0];
    typeList = new ParameterType[0];
    valueList = new Object[0];
  }

  protected AbstractParameterList( ParameterType type1, QName param1 ) {
    dataUri = DATAURI_SCHEME_CLASS + "://" + getClass().getName();
    nameList = new QName[1];
    typeList = new ParameterType[1];
    typeList[0] = type1;
    nameList[0] = param1;
    valueList = new Object[nameList.length];
  }

  protected AbstractParameterList( ParameterType type1, QName param1, ParameterType type2, QName param2 ) {
    dataUri = DATAURI_SCHEME_CLASS + "://" + getClass().getName();
    nameList = new QName[2];
    typeList = new ParameterType[2];
    typeList[0] = type1;
    nameList[0] = param1;
    typeList[1] = type2;
    nameList[1] = param2;
    valueList = new Object[nameList.length];
  }

  protected AbstractParameterList( ParameterType type1, QName param1, ParameterType type2, QName param2, ParameterType type3, QName param3 ) {
    dataUri = DATAURI_SCHEME_CLASS + "://" + getClass().getName();
    nameList = new QName[3];
    typeList = new ParameterType[3];
    typeList[0] = type1;
    nameList[0] = param1;
    typeList[1] = type2;
    nameList[1] = param2;
    typeList[2] = type3;
    nameList[2] = param3;
    valueList = new Object[nameList.length];
  }

  protected AbstractParameterList( ParameterType type1, QName param1, ParameterType type2, QName param2, ParameterType type3, QName param3, ParameterType type4, QName param4  ) {
    dataUri = DATAURI_SCHEME_CLASS + "://" + getClass().getName();
    nameList = new QName[4];
    typeList = new ParameterType[4];
    typeList[0] = type1;
    nameList[0] = param1;
    typeList[1] = type2;
    nameList[1] = param2;
    typeList[2] = type3;
    nameList[2] = param3;
    typeList[3] = type4;
    nameList[3] = param4;
    valueList = new Object[nameList.length];
  }

  public AbstractParameterList( String dataUri, ParameterType[] typeList, QName[] nameList, Object[] valueList ) {
    if (typeList.length != nameList.length) {
      throw new IllegalArgumentException( "typeList and nameList have different sizes: "+typeList.length+" and "+nameList.length );
    }
    if (valueList.length != nameList.length) {
      throw new IllegalArgumentException( "valueList and nameList have different sizes: "+typeList.length+" and "+nameList.length );
    }
    this.dataUri = dataUri;
    this.typeList = typeList;
    this.nameList = nameList;
    this.valueList = valueList;
  }

  @Override
  public String getDataUri() {
    return dataUri;
  }

  protected void defineParam( int index, ParameterType paramType, QName paramName, Object value ) {
    typeList[index] = paramType;
    nameList[index] = paramName;
    valueList[index] = value;
  }

  protected void setValue( QName paramName, Object paramValue ) {
    int index = indexOf( paramName );
    if (index < 0) {
      throw new IllegalArgumentException( "Unknown parameter name="+paramName+" for parameter list "+getClass() );
    }
    if (paramValue != null) {
      switch (typeList[index]) {
        case BooleanType: {
          if (!(paramValue instanceof Boolean)) {
            throw new IllegalArgumentException( "Wrong type for param name=" + paramName + ", expected Boolean, found " + paramValue.getClass() );
          }
          break;
        }
        case DoubleType: {
          if (!(paramValue instanceof Double)) {
            throw new IllegalArgumentException( "Wrong type for param name=" + paramName + ", expected Double, found " + paramValue.getClass() );
          }
          break;
        }
        case IntegerType: {
          if (!(paramValue instanceof Integer)) {
            throw new IllegalArgumentException( "Wrong type for param name=" + paramName + ", expected Integer, found " + paramValue.getClass() );
          }
          break;
        }
        case DateType: {
          if (!(paramValue instanceof Date)) {
            throw new IllegalArgumentException( "Wrong type for param name=" + paramName + ", expected Date, found " + paramValue.getClass() );
          }
          break;
        }
        case StringType: {
          if (!(paramValue instanceof String)) {
            throw new IllegalArgumentException( "Wrong type for param name=" + paramName + ", expected String, found " + paramValue.getClass() );
          }
          break;
        }
        case QNameType: {
          if (!(paramValue instanceof QName)) {
            throw new IllegalArgumentException( "Wrong type for param name=" + paramName + ", expected QName, found " + paramValue.getClass() );
          }
          break;
        }
        case BooleanArrayType: {
          if (!(paramValue instanceof Boolean[])) {
            throw new IllegalArgumentException( "Wrong type for param name=" + paramName + ", expected Boolean[], found " + paramValue.getClass() );
          }
          break;
        }
        case DoubleArrayType: {
          if (!(paramValue instanceof Double[])) {
            throw new IllegalArgumentException( "Wrong type for param name=" + paramName + ", expected Double[], found " + paramValue.getClass() );
          }
          break;
        }
        case IntegerArrayType: {
          if (!(paramValue instanceof Integer[])) {
            throw new IllegalArgumentException( "Wrong type for param name=" + paramName + ", expected Integer[], found " + paramValue.getClass() );
          }
          break;
        }
        case DateArrayType: {
          if (!(paramValue instanceof Date[])) {
            throw new IllegalArgumentException( "Wrong type for param name=" + paramName + ", expected Date[], found " + paramValue.getClass() );
          }
          break;
        }
        case StringArrayType: {
          if (!(paramValue instanceof String[])) {
            throw new IllegalArgumentException( "Wrong type for param name=" + paramName + ", expected String[], found " + paramValue.getClass() );
          }
          break;
        }
        case QNameArrayType: {
          if (!(paramValue instanceof QName[])) {
            throw new IllegalArgumentException( "Wrong type for param name=" + paramName + ", expected QName[], found " + paramValue.getClass() );
          }
          break;
        }
        default: {
          throw new IllegalArgumentException( "Unknown parameter type: " + typeList[index] );
        }
      }
    }
    valueList[index] = paramValue;
  }

  public int indexOf( QName paramName ) {
    for (int i = 0; i < nameList.length; i++) {
      if (paramName == nameList[i]) {
        return i;
      }
    }
    return -1;
  }

  protected Object getValue( QName paramName ) {
    int index = indexOf( paramName );
    if (index < 0) {
      throw new IllegalArgumentException( "Unknown parameter name="+paramName+" for parameter list "+getClass() );
    }
    return getValue( index );
  }

  protected Object getValue( int index ) {
    return valueList[index];
  }

  @Override
  public QName getName( int index ) {
    return nameList[index];
  }

  @Override
  public ParameterType getType( int index ) {
    return typeList[index];
  }

  @Override
  public ParameterType getType( QName paramName ) {
    int index = indexOf( paramName );
    if (index < 0) {
      throw new IllegalArgumentException( "Unknown parameter name="+paramName+" for parameter list "+getClass() );
    }
    return getType( index );
  }

  @Override
  public int size() {
    return nameList.length;
  }

  @Override
  public String getString( QName paramName ) {
    return (String) getValue( paramName );
  }

  @Override
  public QName getQName( QName paramName ) {
    return (QName) getValue( paramName );
  }

  @Override
  public Integer getInteger( QName paramName ) {
    return (Integer) getValue( paramName );
  }

  @Override
  public Boolean getBoolean( QName paramName ) {
    return (Boolean) getValue( paramName );
  }

  @Override
  public Date getDate( QName paramName ) {
    return (Date) getValue( paramName );
  }

  @Override
  public Double getDouble( QName paramName ) {
    return (Double) getValue( paramName );
  }

  @Override
  public String getString( int index ) {
    return (String) getValue( index );
  }

  @Override
  public QName getQName( int index ) {
    return (QName) getValue( index );
  }

  @Override
  public Integer getInteger( int index ) {
    return (Integer) getValue( index );
  }

  @Override
  public Boolean getBoolean( int index ) {
    return (Boolean) getValue( index );
  }

  @Override
  public Date getDate( int index ) {
    return (Date) getValue( index );
  }

  @Override
  public Double getDouble( int index ) {
    return (Double) getValue( index );
  }

  @Override
  public void setString( QName paramName, String paramValue ) {
    setValue( paramName, paramValue );
  }

  @Override
  public void setQName( QName paramName, QName paramValue ) {
    setValue( paramName, paramValue );
  }

  @Override
  public void setInteger( QName paramName, Integer paramValue ) {
    setValue( paramName, paramValue );
  }

  @Override
  public void setBoolean( QName paramName, Boolean paramValue ) {
    setValue( paramName, paramValue );
  }

  @Override
  public void setDate( QName paramName, Date paramValue ) {
    setValue( paramName, paramValue );
  }

  @Override
  public void setDouble( QName paramName, Double paramValue ) {
    setValue( paramName, paramValue );
  }

  @Override
  public String[] getStringArray( QName paramName ) {
    return (String[]) getValue( paramName );
  }

  @Override
  public QName[] getQNameArray( QName paramName ) {
    return (QName[]) getValue( paramName );
  }

  @Override
  public Integer[] getIntegerArray( QName paramName ) {
    return (Integer[]) getValue( paramName );
  }

  @Override
  public Boolean[] getBooleanArray( QName paramName ) {
    return (Boolean[]) getValue( paramName );
  }

  @Override
  public Date[] getDateArray( QName paramName ) {
    return (Date[]) getValue( paramName );
  }

  @Override
  public Double[] getDoubleArray( QName paramName ) {
    return (Double[]) getValue( paramName );
  }

  @Override
  public String[] getStringArray( int index ) {
    return (String[]) getValue( index );
  }

  @Override
  public QName[] getQNameArray( int index ) {
    return (QName[]) getValue( index );
  }

  @Override
  public Integer[] getIntegerArray( int index ) {
    return (Integer[]) getValue( index );
  }

  @Override
  public Boolean[] getBooleanArray( int index ) {
    return (Boolean[]) getValue( index );
  }

  @Override
  public Date[] getDateArray( int index ) {
    return (Date[]) getValue( index );
  }

  @Override
  public Double[] getDoubleArray( int index ) {
    return (Double[]) getValue( index );
  }

  @Override
  public void setStringArray( QName paramName, String[] paramValue ) {
    setValue( paramName, paramValue );
  }

  @Override
  public void setQNameArray( QName paramName, QName[] paramValue ) {
    setValue( paramName, paramValue );
  }

  @Override
  public void setIntegerArray( QName paramName, Integer[] paramValue ) {
    setValue( paramName, paramValue );
  }

  @Override
  public void setBooleanArray( QName paramName, Boolean[] paramValue ) {
    setValue( paramName, paramValue );
  }

  @Override
  public void setDateArray( QName paramName, Date[] paramValue ) {
    setValue( paramName, paramValue );
  }

  @Override
  public void setDoubleArray( QName paramName, Double[] paramValue ) {
    setValue( paramName, paramValue );
  }

  public void setNamespaceTable( QName paramName, NamespaceTable namespaceTable ) {
    setStringArray( paramName, namespaceTable.toStringArray() );
  }

  public NamespaceTable getNamespaceTable( QName paramName ) {
    return new NamespaceTable( getStringArray( paramName ) );
  }

  public static AbstractParameterList fromArgs( String[] args ) {
    String dataUri = DATAURI_SCHEME_CLASS + "://" + AbstractParameterList.class.getName();
    QName[] nameList = new QName[args.length];
    ParameterType[] typeList = new ParameterType[args.length];
    for (int i = 0; i < args.length; i++) {
      nameList[i] = DEFAULT_NS.getQName( "arg_" + i );
      typeList[i] = ParameterType.StringType;
    }
    return new AbstractParameterList( dataUri, typeList, nameList, args );
  }
}
