/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.base;

import de.pidata.log.Logger;

public class ServiceException extends Exception {

  public static final String SERVICE_NOT_FOUND = "SERVICE_NOT_FOUND";
  public static final String UNKNOWN_OPERATION = "UNKNOWN_OPERATION";
  public static final String SERVICE_FAILED = "SERVICE_FAILED";
  public static final String INVALID_INPUT = "INVALID_INPUT";
  public static final String INVALID_OUTPUT = "INVALID_OUTPUT";
  public static final String SERVICE_CONFIG_ERROR = "SERVICE_CONFIG_ERROR";
  public static final String SERVICE_COMM_ERROR = "SERVICE_COMM_ERROR";

  private Throwable cause;
  private String code;

  public ServiceException( String code, String message ) {
    super( message );
    this.code = code;
  }

  public ServiceException(String code, String message, Throwable ex) {
    super( message );
    this.code = code;
    cause = ex;
  }

  /**
   * Returns the detail message string of this throwable.
   *
   * @return the detail message string of this {@code Throwable} instance
   * (which may be {@code null}).
   */
  @Override
  public String getMessage() {
    return "code="+code+", "+super.getMessage();
  }

  public String getCode() {
    return code;
  }

  public Throwable getCause() {
    return cause;
  }

  public void printStackTrace() {
    super.printStackTrace();
    if (cause != null) {
      Logger.error("caused by:", cause);
    }
  }

  /**
   * Returns a short description of this throwable.
   * The result is the concatenation of:
   * <ul>
   * <li> the {@linkplain Class#getName() name} of the class of this object
   * <li> ": " (a colon and a space)
   * <li> the result of invoking this object's {@link #getLocalizedMessage}
   * method
   * </ul>
   * If {@code getLocalizedMessage} returns {@code null}, then just
   * the class name is returned.
   *
   * @return a string representation of this throwable.
   */
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder( getClass().getName() );
    builder.append( ", " ).append( getMessage() );
    if (cause != null) {
      builder.append( ", caused by " ).append( cause );
    }
    return builder.toString();
  }
}
