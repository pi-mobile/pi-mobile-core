/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.base;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Hashtable;

/**
 * Created by pru on 29.01.16.
 */
public interface ServiceProvider {

  public void init( RequestHandler requestHandler, Hashtable props ) throws Exception;

  public OutputStream getOutStream();

  public void start() throws IOException;

  public void stop();

  /**
   * Return String describing this connection, e.g. a URI
   * @return connection string
   */
  public String getConnectionString();
}
