/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.sap;

import com.sap.mw.jco.IMetaData;
import com.sap.mw.jco.JCO;
import de.pidata.system.base.SystemManager;
import de.pidata.system.desktop.DesktopSystem;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

public class JCo2Schema extends JCoAdapter {

  private static final boolean DEBUG = true;

  private Hashtable typeTable = new Hashtable();

  public JCo2Schema() throws Exception {
    SystemManager sysMan = SystemManager.getInstance();
    int i = 0;
    String portTypeName = sysMan.getProperty("jco.portType_"+i, null );
    while (portTypeName != null) {
      writePortType(portTypeName);
      i++;
      portTypeName = sysMan.getProperty("jco.portType_"+i, null );
    }
  }

  private void writePortType(String portTypeName) throws Exception {
    SystemManager sysMan = SystemManager.getInstance();
    Vector operations = new Vector();
    StringBuffer schemaBuffer = new StringBuffer();
    StringBuffer wsdlBuffer = new StringBuffer();
    String functionName;

    writeBegin(portTypeName, schemaBuffer, wsdlBuffer);
    JCO.Client conn = getSapConnection();
    int i = 0;
    functionName = sysMan.getProperty("jco."+portTypeName+".rfc_"+i, null );
    while (functionName != null) {
      JCO.Function function = this.createFunction(functionName, conn);
      operations.addElement( function2Schema(function, schemaBuffer, wsdlBuffer) );
      i++;
      functionName = sysMan.getProperty("jco."+portTypeName+".rfc_"+i, null );
    }
    writeEnd(schemaBuffer, wsdlBuffer, portTypeName, operations);
    closeSapConnection(conn);

    writeToFile(schemaBuffer, portTypeName+".xsd");
    writeToFile(wsdlBuffer, portTypeName+".wsdl");

  }

  private void writeBegin(String serviceName, StringBuffer schemaBuffer, StringBuffer wsdlBuffer) {
    String nsURI = "de.pidata." + serviceName;
    wsdlBuffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    wsdlBuffer.append("<ws:definitions name=\"").append(serviceName).append("\"\n");
    wsdlBuffer.append("    xmlns:ws=\"http://schemas.xmlsoap.org/wsdl/\"\n");
    wsdlBuffer.append("    xmlns=\"").append(nsURI).append("\"\n");
    wsdlBuffer.append("    targetNamespace=\"").append(nsURI).append("\">\n");
    wsdlBuffer.append("\n");
    wsdlBuffer.append("  <ws:import namespace=\"").append(nsURI);
    wsdlBuffer.append("\" location=\"/").append(serviceName).append(".xsd\"/>\n");

    schemaBuffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    schemaBuffer.append("<xsd:schema id=\"SapFactory\" targetNamespace=\"").append(nsURI).append("\"\n");
    schemaBuffer.append("    xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n");
    schemaBuffer.append("    elementFormDefault=\"unqualified\" attributeFormDefault=\"unqualified\">\n");
    schemaBuffer.append("\n");
  }

  private void writeEnd(StringBuffer schemaBuffer, StringBuffer wsdlBuffer, String serviceName, Vector operations) {
    wsdlBuffer.append("  <ws:portType name=\"").append(serviceName).append("PT\">\n\n");
    for (int i = 0; i < operations.size(); i++) {
      wsdlBuffer.append(operations.elementAt(i)).append("\n");
    }
    wsdlBuffer.append("  </ws:portType>\n");
    wsdlBuffer.append("</ws:definitions>\n");

    schemaBuffer.append("</xsd:schema>\n");
  }

  /**
   * Write a stringBuffer to the given filePath
   *
   * @param data the data StringBuffer
   * @param filePath destination for writing
   */
  private void writeToFile(StringBuffer data, String filePath)
  throws IOException {
    System.out.println("Write to file: "+ filePath);
    File file = new File(filePath);
    File parentDir = new File(new File(file.getAbsolutePath()).getParent());
    parentDir.mkdirs();
    FileWriter fw = new FileWriter(file);
    fw.write(data.toString());
    fw.flush();
    fw.close();
  }

  /**
   *
   * @param function
   * @param schemaBuffer
   * @param wsdlBuffer
   * @return operation block for WSDL
   */
  private String function2Schema(JCO.Function function, StringBuffer schemaBuffer, StringBuffer wsdlBuffer) {
    JCO.ParameterList params;
    IMetaData table;
    int indent = 0;
    String functionName = function.getName();
    Vector tableParts = new Vector();
    String tableRowTypeName, tableName, tableTypeName;
    String inputMsg, outputMsg;
    StringBuffer opBuffer;

    //----- process binding parameters
    params = function.getTableParameterList();
    if (params != null) {
      for (int i = 0; i < params.getFieldCount(); i++) {
        table = params.getMetaData(i);
        tableName = params.getName(i);
        tableRowTypeName = table.getName();
        tableTypeName = tableRowTypeName + "_TABLE";

        //-- Table element
        if (!typeTable.contains( tableTypeName )) {
          schemaBuffer.append( "<xsd:element name=\"" + tableTypeName + "\">\n" );
          schemaBuffer.append( "  <xsd:complexType>\n" );
          schemaBuffer.append( "    <xsd:sequence>\n" );
          schemaBuffer.append( "       <xsd:element name=\"" + tableRowTypeName + "\" type=\"" + tableRowTypeName + "\" minOccurs=\"0\" maxOccurs=\"unbounded\"/>\n" );
          schemaBuffer.append( "    </xsd:sequence>\n" );
          schemaBuffer.append( "  </xsd:complexType>\n" );
          schemaBuffer.append( "</xsd:element>\n" );
          typeTable.put( tableTypeName, tableTypeName );
        }
        //-- Table row type
        if (!typeTable.contains( tableRowTypeName )) {
          schemaBuffer.append( "<xsd:complexType name=\"" ).append( tableRowTypeName ).append( "\">\n" );
          indent++;
          paramList2Attrib( schemaBuffer, indent, table );
          schemaBuffer.append( "</xsd:complexType>\n\n" );
          indent--;
          typeTable.put( tableRowTypeName, tableRowTypeName );
        }
        tableParts.addElement("    <ws:part name=\""+tableName+"\" type=\""+tableTypeName+"\"/>\n");
      }
    }

    //----- process import parameters
    params = function.getImportParameterList();
    if ((params == null) && (tableParts.size() == 0)) {
      inputMsg = null;
    }
    else {
      inputMsg = functionName;
      writeMessage(schemaBuffer, wsdlBuffer, inputMsg, params, tableParts);
    }

    //----- process export parameters
    params = function.getExportParameterList();
    if ((params == null) && (tableParts.size() == 0)) {
      outputMsg = null;
    }
    else {
      outputMsg = functionName + "_REPLY";
      writeMessage(schemaBuffer, wsdlBuffer, outputMsg, params, tableParts);
    }

    //----- create operation definition
    opBuffer = new StringBuffer();
    opBuffer.append("    <ws:operation name=\""+functionName+"\">\n");
    if (inputMsg != null) {
      opBuffer.append("      <ws:input message=\""+inputMsg+"\"/>\n");
    }
    if (outputMsg != null) {
      opBuffer.append("      <ws:output message=\""+outputMsg+"\"/>\n");
    }
    opBuffer.append("    </ws:operation>\n");
    return opBuffer.toString();
  }

  private void writeMessage(StringBuffer schemaBuffer, StringBuffer wsdlBuffer, String msgName, JCO.ParameterList params, Vector tableParts) {
    wsdlBuffer.append("  <ws:message name=\"").append(msgName).append("\">\n");
    if (params != null) paramList2Types(schemaBuffer, wsdlBuffer, params);
    for (int i = 0; i < tableParts.size(); i++) {
      wsdlBuffer.append(tableParts.elementAt(i));
    }
    wsdlBuffer.append("  </ws:message>\n\n");
  }

  private void addTables(StringBuffer wsdlBuffer, Vector tableNames) {

  }

  private void paramList2Types(StringBuffer schemaBuffer, StringBuffer wsdlBuffer, IMetaData params) {
    int fieldCount;
    int fieldNum;
    int fieldType;
    int fieldLen;
    String fieldName, typeName;

    fieldCount = params.getFieldCount();
    for (fieldNum = 0; fieldNum < fieldCount; fieldNum++) {
      fieldType = params.getType(fieldNum);
      fieldName = params.getName(fieldNum);
      typeName = "T"+fieldName;
      if (fieldType == IMetaData.TYPE_STRUCTURE) {
        addComplexType(schemaBuffer, 2, typeName, params.getMetaData(fieldNum));
      }
      else {
        fieldLen = params.getLength(fieldNum);
        typeName = addSimpleType(schemaBuffer, 2, fieldName, fieldType, fieldLen);
      }
      schemaBuffer.append('\n');
      wsdlBuffer.append("    <ws:part name=\"").append(fieldName);
      wsdlBuffer.append("\" type=\"").append(typeName).append("\"/>\n");
    }
  }

  private void paramList2Attrib(StringBuffer xmlBuffer, int indent, IMetaData params) {
    int fieldCount;
    int fieldNum;
    int fieldType;
    int fieldLen;
    String fieldName, typeName;
    StringBuffer typeBuffer;
    int elementCount = 0;

    fieldCount = params.getFieldCount();
    for (fieldNum = 0; fieldNum < fieldCount; fieldNum++) {
      fieldType = params.getType(fieldNum);
      fieldName = params.getName(fieldNum);
      if (fieldType == IMetaData.TYPE_STRUCTURE) {
        elementCount++;
      }
      else {
        addIndent(indent, xmlBuffer);
        xmlBuffer.append("<xsd:attribute name=\"").append(fieldName).append("\"");
        fieldLen = params.getLength(fieldNum);
        typeBuffer = new StringBuffer();
        typeName = addSimpleType(typeBuffer, indent+1, null, fieldType, fieldLen);
        if (typeName != null) {
          xmlBuffer.append(" type=\"").append(typeName).append("\"/>\n");
        }
        else {
          xmlBuffer.append(">\n").append(typeBuffer);
          addIndent(indent, xmlBuffer);
          xmlBuffer.append("</xsd:attribute>\n");
        }
      }
    }

    if (elementCount > 0) {
      addIndent(indent, xmlBuffer);
      xmlBuffer.append("<xsd:sequence>\n");
      indent++;
      for (fieldNum = 0; fieldNum < fieldCount; fieldNum++) {
        fieldType = params.getType(fieldNum);
        fieldName = params.getName(fieldNum);
        if (fieldType == IMetaData.TYPE_STRUCTURE) {
          addElement(xmlBuffer, indent, fieldName, params.getMetaData(fieldNum));
        }
      }
      indent--;
      addIndent(indent, xmlBuffer);
      xmlBuffer.append("</xsd:sequence>\n");
    }
  }

  private void addElement(StringBuffer xmlBuffer, int indent, String fieldName, IMetaData meta) {
    addIndent(indent, xmlBuffer);
    xmlBuffer.append("<xsd:element name=\""+fieldName+"\"> <!-- JCo-Structure -->\n");
    indent++;
    addComplexType(xmlBuffer, indent, null, meta);
    indent--;
    addIndent(indent, xmlBuffer);
    xmlBuffer.append("</xsd:element>\n");
  }

  private void addComplexType(StringBuffer xmlBuffer, int indent, String typeName, IMetaData meta) {
    if (!typeTable.contains( typeName )) {
      addIndent(indent, xmlBuffer);
      xmlBuffer.append( "<xsd:complexType" );
      if (typeName != null) {
        xmlBuffer.append( " name=\"" ).append( typeName ).append( "\"" );
      }
      xmlBuffer.append( ">\n" );
      indent++;
      paramList2Attrib( xmlBuffer, indent, meta );
      indent--;
      addIndent( indent, xmlBuffer );
      xmlBuffer.append( "</xsd:complexType>\n" );
      typeTable.put( typeName, typeName );
    }
  }

  private String addSimpleType(StringBuffer xmlBuffer, int indent, String typeName, int fieldType, int fieldLen) {
    switch (fieldType) {
      case IMetaData.TYPE_BCD : {
        // Java type: BigDecimal
        return "xsd:decimal";
        //TODO fractionDigits und totalDigits setzen, muss aber auch im SimpleType umngesetzt wertden
      }
      case IMetaData.TYPE_CHAR :
      case IMetaData.TYPE_NUM :
      case IMetaData.TYPE_STRING : {
        // Java type: String
        if (fieldLen < 0) {
          return "xsd:string";
        }
        else {
          addStringType(xmlBuffer, indent, typeName, fieldLen);
          return typeName;
        }
      }
      case IMetaData.TYPE_DATE : {
        // Java type: java.util.Date
        return "xsd:date";
      }
      case IMetaData.TYPE_FLOAT : {
        // Java type: double
        return "xsd:double";
        //TODO fractionDigits und totalDigits setzen, muss aber auch im SimpleType umngesetzt wertden
      }
      case IMetaData.TYPE_INT :
      case IMetaData.TYPE_INT1 :
      case IMetaData.TYPE_INT2 : {
        // Java type: int
        return "xsd:int";
        //TODO totalDigits setzen, muss aber auch im SimpleType umngesetzt wertden
      }
      case IMetaData.TYPE_TIME : {
        // Java type: java.util.Date
        return "xsd:time";
      }
      case IMetaData.TYPE_BYTE :
      case IMetaData.TYPE_XSTRING : {
        // Java Type: byte[]
        return "xsd:base64Binary";
      }

      case IMetaData.TYPE_EXCEPTION : {
        return "JCo-Exception";
      }
      case IMetaData.TYPE_ITAB : {
        return "JCo-ITab";
      }
      case IMetaData.TYPE_TABLE : {
        return "JCo-Table";
      }
      default:
        throw new IllegalArgumentException("Unknown type name="+typeName);
    }
  }

  private void addStringType(StringBuffer xmlBuffer, int indent, String typeName, int fieldLen) {
    if (!typeTable.contains( typeName )) {
      addIndent(indent, xmlBuffer);
      xmlBuffer.append("<xsd:simpleType");
      if (typeName != null) {
        xmlBuffer.append(" name=\"").append(typeName).append("\"");
      }
      xmlBuffer.append(">\n");
      addIndent(indent+1, xmlBuffer);
      xmlBuffer.append("<xsd:restriction base=\"xsd:string\">\n");
      addIndent(indent+2, xmlBuffer);
      xmlBuffer.append("<xsd:minLength value=\"0\"/>\n");
      addIndent(indent+2, xmlBuffer);
      xmlBuffer.append("<xsd:maxLength value=\"").append(fieldLen).append("\"/>\n");
      addIndent(indent+1, xmlBuffer);
      xmlBuffer.append("</xsd:restriction>\n");
      addIndent(indent, xmlBuffer);
      xmlBuffer.append("</xsd:simpleType>\n");
      typeTable.put( typeName, typeName );
    }
  }

  private void addIndent(int indent, StringBuffer xmlBuffer) {
    for (int i = 0; i < indent; i++) {
      xmlBuffer.append("  ");
    }
  }

  public static void main(String[] args) {
    try {
      new DesktopSystem();
      new JCo2Schema();
    }
    catch (Exception ex) {
      ex.printStackTrace();
      System.exit(1);
    }
  }
}
