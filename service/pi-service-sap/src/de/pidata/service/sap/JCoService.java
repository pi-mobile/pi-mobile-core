/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.sap;

import com.sap.mw.jco.JCO;
import de.pidata.comm.soap.Body;
import de.pidata.comm.soap.Envelope;
import de.pidata.log.Logger;
import de.pidata.models.config.Binding;
import de.pidata.models.config.Configurator;
import de.pidata.models.service.Operation;
import de.pidata.service.base.ServiceException;
import de.pidata.models.service.ServiceManager;
import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.Relation;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.wsdl.TDefinitions;
import de.pidata.wsdl.TMessage;
import de.pidata.wsdl.TOperation;
import de.pidata.wsdl.TParam;
import de.pidata.wsdl.TPart;
import de.pidata.wsdl.WsdlLoader;
import de.pidata.wsdl.WsdlService;

import java.util.Stack;

public class JCoService extends WsdlService {

  public  static final Namespace NAMESPACE = Namespace.getInstance("http://www.pidata.de/res/service/config");

  protected JCoAdapter jCoAdapter;

  public JCoService() {
  }

  public void init( ServiceManager serviceManager, Configurator configurator, Binding binding ) throws Exception {
    super.init( serviceManager, configurator, binding );
    jCoAdapter = new JCoAdapter();
  }

  protected void clearTables(JCO.Function function) {
    JCO.ParameterList tables = function.getTableParameterList();
    JCO.Table table;
    if (tables != null) {
      for (int i = 0; i < tables.getFieldCount(); i++) {
        table = tables.getTable(i);
        table.clear();
      }
    }
  }

  protected void writeInput(JCO.Function function, Model inputModel) throws ServiceException {
    String    paramName = null;
    Model     parameter;
    Type      type;
    JCO.ParameterList params;

    try {
      params = function.getImportParameterList();

      for (ModelIterator inputIter = inputModel.iterator(null, null); inputIter.hasNext(); ) {
        parameter = inputIter.next();
        type = parameter.type();
        paramName = parameter.getParentRelationID().getName();

        //----- SimpleTypes are mapped to JCO input parameters
        if (parameter instanceof SimpleType) {
          Object jcoValue = jCoAdapter.value2jco((SimpleType) type, parameter.getContent());
          params.setValue(jcoValue, paramName);
        }
        else {

          //----- ComplexType is either a structure ...
          if ((params != null) && (params.hasField( paramName ))) {
            JCO.Structure structure = params.getStructure( paramName );
            int colCount = structure.getNumFields();
            jCoAdapter.writeRecord(structure, parameter);
          }
          //----- ... or a binding, i.e. the parent of the binding rows
          else {
            jCoAdapter.writeTable(function, paramName, parameter, new Stack(), new Stack());
          }
        }
      }
    }
    catch (Exception ex) {
      Logger.error("Error processing input, last paramName="+paramName, ex);
      throw new ServiceException(ServiceException.INVALID_INPUT, "Error processing input, last paramName="+paramName, ex);
    }
  }

  protected void writeInputOld(JCO.Function function, TOperation operation, Model inputModel) throws ServiceException {
    QName paramName = null;
    QName     typeName;
    Model     parameter;
    Type      type;
    TPart     part;
    JCO.Table table;
    JCO.ParameterList params;
    Namespace ns = operation.getName().getNamespace();
    ModelFactory factory;

    try {
      TParam input = operation.getInput();
      TMessage inMsg = (TMessage) defs.get(TDefinitions.ID_MESSAGE, input.getMessage());
      params = function.getImportParameterList();
      if (params != null) {
        for (int i = 0; i < params.getFieldCount(); i++) {
          paramName = ns.getQName(params.getName(i));
          parameter = inputModel.get(paramName, null);
          if (parameter != null) {
            part = (TPart) inMsg.get(TMessage.ID_PART, paramName);
            typeName = part.getType();
            factory = ModelFactoryTable.getInstance().getFactory( typeName.getNamespace() );
            type = factory.getType(typeName);
            if (type instanceof SimpleType) {
              Object jcoValue = jCoAdapter.value2jco((SimpleType) type, parameter.getContent());
              params.setValue(jcoValue, paramName.getName());
            }
            else {
              JCO.Structure structure = params.getStructure(i);
              int colCount = structure.getNumFields();
              jCoAdapter.writeRecordOld(structure, colCount, parameter, new String[0]);
            }
          }
        }
      }

      params = function.getTableParameterList();
      if (params != null) {
        for (int i = 0; i < params.getFieldCount(); i++) {
          paramName = ns.getQName(params.getName(i));
          part = (TPart) inMsg.get(TMessage.ID_PART, paramName);
          typeName = part.getType();
          factory = ModelFactoryTable.getInstance().getFactory( typeName.getNamespace() );
          type = factory.getType(typeName);
          parameter = inputModel.get(paramName, null);
          if (parameter != null) {
            table = jCoAdapter.fetchTable(function, paramName.getName());
            jCoAdapter.writeTableOld(table, parameter.iterator(null, null), new String[0]);
          }
        }
      }
    }
    catch (Exception ex) {
      Logger.error("Error processing input, last paramName="+paramName, ex);
      throw new ServiceException(ServiceException.INVALID_INPUT, "Error processing input, last paramName="+paramName, ex);
    }
  }

  protected ChildList readOutput( com.sap.mw.jco.JCO.Function function, QName opName )
      throws ServiceException {
    TOperation op = (TOperation) findOperation( opName );
    TMessage outMsg = op.getDefinitions().findMessageDef( op.getOutput().getMessage() );
    return readOutput( function, outMsg );
  }

  protected ChildList readOutput( JCO.Function function, TMessage msg ) throws ServiceException {
    ChildList childlist;
    QName paramName;
    childlist = new ChildList();
    paramName = null;

    try {
      com.sap.mw.jco.JCO.ParameterList params = function.getExportParameterList();
      ModelIterator partIter = msg.partIter();
      do {
        if (!partIter.hasNext())
          break;
        TPart part = (TPart) partIter.next();
        Relation relation = WsdlLoader.getPartRelation( part );
        QName relName = relation.getRelationID();
        de.pidata.models.types.Type type = relation.getChildType();
        ModelFactory factory = ModelFactoryTable.getInstance().getFactory( relName.getNamespace() );
        Model valueModel = null;
        if (type instanceof SimpleType) {
          Object value = jCoAdapter.readFromRecord( params, paramName.getName(), (SimpleType) type, paramName.getNamespace() );
          valueModel = factory.createInstance( paramName, type, null, null, null );
          valueModel.setContent( value );
        }
        else if (params != null && params.hasField( relName.getName() )) {
          com.sap.mw.jco.JCO.Structure structure = params.getStructure( relName.getName() );
          ComplexType rowType = (ComplexType) type;
          Object attributes[] = new Object[rowType.attributeCount()];
          Key key = jCoAdapter.readRecord( structure, rowType, attributes );
          int keyAttrCount = rowType.keyAttributeCount();
          if (key == null && keyAttrCount == 0 || key != null && key.keyValueCount() == keyAttrCount)
            valueModel = factory.createInstance( key, rowType, attributes, null, null );
        }
        else {
          ComplexType tableType = (ComplexType) type;
          ChildList rowList = new ChildList();
          QName rowRelName = tableType.relationNames().next();
          ComplexType rowType = (ComplexType) tableType.getChildType( rowRelName );
          jCoAdapter.readTable( function.getTableParameterList(), relName, rowList, rowType, rowRelName, new Stack(), new Stack() );
          valueModel = factory.createInstance( null, tableType, null, null, rowList );
        }
        if (valueModel != null)
          childlist.add( relName, valueModel );
      }
      while (true);
      return childlist;
    }
    catch (Exception ex) {
      Logger.error( "Error processing output, last paramName=" + paramName, ex );
      throw new ServiceException( ServiceException.INVALID_OUTPUT, "Error processing output, last paramName=" + paramName, ex );
    }
  }

  protected JCO.Function prepareFunction(JCO.Client connection, QName operationID) throws ServiceException {
    JCO.Function function = jCoAdapter.createFunction(operationID.getName(), connection);
    if (function == null) {
      throw new ServiceException(ServiceException.UNKNOWN_OPERATION, "createfunction returned null for function name="+operationID.getName());
    }
    clearTables(function);
    return function;
  }

  protected Model invoke( Context caller, Operation operation, Model input) throws ServiceException {
    QName operationID = operation.getName();
    JCO.Client connection = jCoAdapter.getSapConnection();
    JCO.Function function = prepareFunction(connection, operationID);

    writeInput(function, input);
    jCoAdapter.execute(function, connection);
    TMessage outMsg = (TMessage) defs.get(TDefinitions.ID_MESSAGE, ((TOperation) operation).getOutput().getMessage());
    ChildList outChilds = readOutput( function, outMsg );
    Model output = new Body( Envelope.ID_BODY, null, null, outChilds );

    jCoAdapter.closeSapConnection(connection);
    return output;

  }
}
