/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.sap;

import com.sap.mw.jco.IRepository;
import com.sap.mw.jco.JCO;
import de.pidata.log.Logger;
import de.pidata.service.base.ServiceException;
import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.models.types.simple.DecimalObject;
import de.pidata.models.types.simple.DecimalType;
import de.pidata.models.types.simple.IntegerType;
import de.pidata.models.types.simple.QNameType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.system.base.SystemManager;

import java.util.Stack;

public class JCoAdapter {

  private static IRepository repository = null;

  private static String myPoolName;

  protected JCoAdapter() {
    if (repository == null) {
      SystemManager sm = SystemManager.getInstance();
      initialize(sm.getProperty("sap.poolname", null ), sm.getPropertyInt("sap.maxConnection", 100),
                 sm.getProperty("sap.mandant", null ), sm.getProperty("sap.user", null ),
                 sm.getProperty("sap.password", null ), sm.getProperty("sap.language", null ),
                 sm.getProperty("sap.system", null ), sm.getProperty("sap.systemNr", null ));
    }
  }

  public JCO.Client getSapConnection() {
    Logger.debug(">>>>> Before JCO.getClient()");
    JCO.Client connection = JCO.getClient(myPoolName);
    connection.setAbapDebug(false);
    Logger.debug("<<<<< After JCO.getClient()");
    return connection;
  }

  public void closeSapConnection(JCO.Client connection) {
    Logger.debug(">>>>> Before JCO.releaseClient()");
    JCO.releaseClient(connection);
    Logger.debug("<<<<< After JCO.releaseClient()");
  }

  public IRepository getRepository (JCO.Client connection) {
    return repository;
  }

  /**
   * Initializeds the connection pool used by all instances of this class
   * @param poolName
   * @param conn
   * @param prmMandant
   * @param prmUsr
   * @param prmPwd
   * @param language
   * @param prmSapSystem
   * @param sysNR
   */
  public static void initialize(String poolName, int conn, String prmMandant, String prmUsr,
                                String prmPwd, String language, String prmSapSystem, String sysNR) {
    //----- Check parameters:
    int i;
    if (null == prmUsr || 0 >= prmUsr.length()) {
      throw new IllegalArgumentException("prmUser must not be empty or null");
    }
    if (null == prmPwd || 0 >= prmPwd.length()) {
      throw new IllegalArgumentException("prmPwd must not be empty or null");
    }
    if (null == prmSapSystem || 0 >= prmSapSystem.length()) {
      throw new IllegalArgumentException("prmSapSystem must not be empty or null");
    }
    if (null == prmMandant || 0 >= prmMandant.length()) prmMandant = "100";
    if (0 <= (i = prmSapSystem.indexOf('('))) {
      prmSapSystem = prmSapSystem.substring(i + 1);
      if (0 <= (i = prmSapSystem.indexOf(')')))
        prmSapSystem = prmSapSystem.substring(0, i);
    }
    prmSapSystem = prmSapSystem.trim();
    if (0 >= prmSapSystem.length()) {
      throw new IllegalArgumentException("prmSapSystem must be at least one character");
    }

    try {
      JCO.removeClientPool(poolName);
    }
    catch (JCO.Exception e) {
      Logger.error( "JCO: Error removing client pool", e );
    }

    //----- Create connection pool
    Logger.info( "JCO adding client pool name="+poolName+", conn="+conn+", prmMandant="+prmMandant
                        +", prmUsr="+prmUsr+", prmSapSystem="+prmSapSystem+", sysNR="+sysNR );
    try {
      JCO.addClientPool(poolName, conn, prmMandant, prmUsr, prmPwd, language, prmSapSystem, sysNR);
    }
    catch (JCO.Exception e) {
      Logger.error( "JCO: Error adding client pool: '" + poolName + "'", e );
    }
    myPoolName = poolName;
    repository = JCO.createRepository(myPoolName+"Rep", myPoolName);
  }

  /**
   * Creates function for name by using repository
   * @param name the function name
   * @return the function
   * @throws IllegalArgumentException if an exception occured while creation function
   */
  public JCO.Function createFunction(String name, JCO.Client connection) {
    try {
      IRepository repository = getRepository(connection);
      return repository.getFunctionTemplate(name.toUpperCase()).getFunction();
    }
    catch (Exception ex) {
      String msg = "Exception while creating function '" + name;
      Logger.error(msg, ex);
      throw new IllegalArgumentException(msg + "', msg=" + ex.getMessage());
    }
  }

  /**
   * Returns binding tableName from functions's TableParameterList
   * @param function  the function from which the binding is returned
   * @param tableName the binding's name
   * @return binding tableName from functions's TableParameterList
   * @throws IllegalArgumentException if tableName does not exist for function
   */
  public JCO.Table fetchTable(JCO.Function function, String tableName) {
    JCO.ParameterList tableList = function.getTableParameterList();
    if (tableList == null) {
      throw new IllegalArgumentException("Function '" + function.getName() + "': TableParamterList is null");
    }
    JCO.Table table = tableList.getTable(tableName);
    if (table == null) {
      throw new IllegalArgumentException("Function '" + function.getName() + "': Table '"
                                         + tableName + "' not found in repository");
    }
    return table;
  }

  /**
   * Returns structure structureName from functions's ImportParameterList
   * @param function  the function from which the structure is returned
   * @param structureName the structure's name
   * @return structure structureName from functions's ImportParameterList
   * @throws IllegalArgumentException if structureName does not exist
   */
  public JCO.Structure fetchInStructure(JCO.Function function, String structureName) {
    JCO.Structure structure = function.getImportParameterList().getStructure(structureName);
    if (structure == null) {
      throw new IllegalArgumentException("Function '" + function.getName() + "': import structure '"
                                         + structureName + "' not found in repository");
    }
    return structure;
  }

  /**
   * Returns structure structureName from functions's ExportParameterList
   * @param function  the function from which the structure is returned
   * @param structureName the structure's name
   * @return structure structureName from functions's ExportParameterList
   * @throws IllegalArgumentException if structureName does not exist
   */
  public JCO.Structure fetchOutStructure(JCO.Function function, String structureName) {
    JCO.Structure structure = function.getExportParameterList().getStructure(structureName);
    if (structure == null) {
      throw new IllegalArgumentException("Function '" + function.getName() + "': export structure '"
                                         + structureName + "' not found in repository");
    }
    return structure;
  }

  /**
   * Tries to execute function using connection
   * @param function the function to be executed
   */
  public void execute(JCO.Function function, JCO.Client connection) throws ServiceException {
    try {
      connection.execute(function);
    }
    catch (Exception ex) {
      Logger.error("Can't execute function: " + function.getName(), ex);
      throw new ServiceException(ServiceException.SERVICE_FAILED, "Can't execute function: " + function.getName(), ex);
    }
  }

  /**
   * Reads value for fieldName from JCo record
   * @param record    the record to read from
   * @param fieldName the filed to read from
   * @param attrType  the result type
   * @param ns        the namespace to use for QNames
   * @return the value for fieldName from JCo record
   */
  public Object readFromRecord(JCO.Record record, String fieldName, SimpleType attrType, Namespace ns) {
    Object value = null;
    if (record.hasField(fieldName)) {
      // die hasField funktioniert nicht oder nicht immer, deshalb fangen wir die Exception
      try {
        value = record.getValue(fieldName);
      }
      catch (Exception ex) {
        value = null;
      }
      if (value != null) {
        if (attrType instanceof DateTimeType) {
          value = new DateObject( ((DateTimeType) attrType).getType(), ((java.util.Date) value).getTime() );
        }
        else if (attrType instanceof QNameType) {
          value = ns.getQName(value.toString());
        }
        else if (attrType instanceof IntegerType) {
          //BigDecimal temp = new BigDecimal((String) value);
          value = new Integer(record.getInt(fieldName));
        }
        else if (attrType instanceof DecimalType) {
          value = new DecimalObject(record.getString(fieldName));
        }
      }
    }
    return value;
  }

  /**
   * Creates an attribute array from the current record (structure or row of record).
   * The result has the order defined by rowType's attributes.
   * @param record      the record to read from
   * @param attribs     the array to place values read
   * @param rowType     the attribute definietion to use for reading
   * @param keyColCount the number of key columns in record. These columns are ignored
   *                    when creating the row model
   * @return an attribute array created from the current row of record
   */
  public Key readRecordOld( JCO.Record record, Object[] attribs, int colCount, ComplexType rowType, int keyColCount) {
    QName colID;
    int        attrIndex = 0;
    Object     value;
    SimpleType attrType;
    String     colName;
    Namespace  ns = rowType.name().getNamespace();
    int        keyAttrCount = rowType.keyAttributeCount();
    boolean    simpleKey = false;
    Object[]   keyAttrs = null;
    Key        key = null;
    int        keyIndex;

    if (keyAttrCount == 1) {
      simpleKey = true;
    }
    else if (keyAttrCount > 0) {
      keyAttrs = new Object[keyAttrCount];
    }

    for (int i = keyColCount; i < colCount; i++) {
      colName = record.getName(i);
      colID = ns.getQName(colName);
      attrIndex = rowType.indexOfAttribute(colID);
      if (attrIndex < 0) {
        throw new IllegalArgumentException("No attribute for jCo record column name=" + colID.getName());
      }
      attrType = rowType.getAttributeType(attrIndex);
      value = readFromRecord(record, colName, attrType, ns);
      keyIndex = rowType.getKeyIndex(colID);
      if (keyIndex >= 0) {
        if (simpleKey) {
          if (attrType instanceof QNameType) key = (QName) value;
          else key = new SimpleKey( value );
        }
        else {
          keyAttrs[keyIndex] = value;
        }
      }
      else {
        attribs[attrIndex] = value;
      }
    }

    if (keyAttrs != null) {
      key = new CombinedKey( keyAttrs );
    }
    return key;
  }

  public Key readRecord(JCO.Record record, ComplexType recordType, Object[] attributes) {
    int attrCount = recordType.attributeCount();
    Namespace  ns = recordType.name().getNamespace();
    int        keyAttrCount = recordType.keyAttributeCount();
    boolean    simpleKey = false;
    Object[]   keyAttrs = null;
    Key        key = null;
    int        keyIndex;
    ChildList  children = null;

    //----- read attributes and create Key
    if (keyAttrCount == 1) {
      simpleKey = true;
    }
    else if (keyAttrCount > 0) {
      keyAttrs = new Object[keyAttrCount];
    }
    for (int i = 0; i < attrCount; i++ ) {
      SimpleType attrType = recordType.getAttributeType(i);
      QName attrName = recordType.getAttributeName(i);
      Object value = readFromRecord(record, attrName.getName(), attrType, ns);
      keyIndex = recordType.getKeyIndex(attrName);
      if (keyIndex >= 0) {
        if (simpleKey) {
          if (attrType instanceof QNameType) key = (QName) value;
          else key = new SimpleKey( value );
        }
        else {
          keyAttrs[keyIndex] = value;
        }
      }
      else {
        attributes[i] = value;
      }
    }
    if (keyAttrs != null) {
      key = new CombinedKey( keyAttrs );
    }
    return key;
  }

  /**
   * Reads all rows of the given binding into a tableRows. The row Models are created
   * by calling readTableRow().
   * @param table       the tabel to be read
   * @param rowType     the type definition to use for creating row Models
   * @param tableRows   the chidlList to add the created row models
   * @param relationID  the relationID to use when adding row models to tableRows
   * @param keyColCount the number of key columns in binding. These columns are ignored
   *                    when creating the row model
   * @return a ChildList containing all rows of binding
   */
  public ChildList readTableOld(JCO.Table table, ComplexType rowType, ChildList tableRows,
                                QName relationID, int keyColCount) {
    Model    rowModel;
    int      rowCount = table.getNumRows();
    int      colCount = table.getNumColumns();
    ModelFactory sapFactory = ModelFactoryTable.getInstance().getFactory( rowType.name().getNamespace() );

    if (rowCount > 0) {
      for (int i = 0; i < rowCount; i++) {
        table.setRow(i);
        Object[] attribs = new Object[ rowType.attributeCount() ];
        Key key = readRecordOld(table, attribs, colCount, rowType, keyColCount);
        rowModel = sapFactory.createInstance(key, rowType, attribs, null, null);
        tableRows.add(relationID, rowModel);
      }
    }
    return tableRows;
  }

  /**
   * Reads
   * @param tableParams
   * @param rowType
   * @return
   */
  public void readTable(JCO.ParameterList tableParams, QName tableName, ChildList rowList, ComplexType rowType, QName relationName, Stack keyColNames, Stack keyValues) {
    ModelFactory factory;
    JCO.Table table = tableParams.getTable( tableName.getName() );
    int rowCount = table.getNumRows();
    Logger.info("JCO read table '"+tableName.getName()+"', rowCount="+rowCount);
    boolean acceptKey;

    for (int row = 0; row < rowCount; row++) {
      table.setRow(row);
      acceptKey = true;
      for (int i = 0; i< keyColNames.size(); i++) {
        String fieldName = (String) keyColNames.elementAt(i);
        Object value = table.getValue(fieldName);
        if (!value.equals( keyValues.elementAt(i)) ) {
          acceptKey = false;
          break;
        }
      }
      if (acceptKey) {
        //----- Read attributes and key
        Object[] attributes = new Object[ rowType.attributeCount() ];
        Key key = readRecord(table, rowType, attributes);

        //----- Read/create children
        int keyCount = rowType.keyAttributeCount();
        for (int i = 0; i < keyCount; i++) {
          String colName = rowType.getKeyAttribute(i).getName();
          keyColNames.push( colName );
          keyValues.push( table.getValue(colName) );
        }
        ChildList children = new ChildList();
        for (QNameIterator childRelIter = rowType.relationNames(); childRelIter.hasNext(); ) {
          QName relName = childRelIter.next();
          Type type = rowType.getChildType(relName);
          if (tableParams.hasField(relName.getName())) {
            readTable(tableParams, relName, children, (ComplexType) type, relName, keyColNames, keyValues );
          }  
        }
        for (int i = 0; i < keyCount; i++) {
          keyColNames.pop();
          keyValues.pop();
        }

        //----- Create result model
        factory = ModelFactoryTable.getInstance().getFactory( rowType.name().getNamespace() );
        Model rowModel = factory.createInstance(key, rowType, attributes, null, children);
        rowList.add(relationName, rowModel);
      }
    }
  }

  /**
   * Converts value to the corresponding JCo format
   * @param attrType value's type
   * @param value    the value to be converted
   * @return the value converted to JCo format
   */
  public Object value2jco(SimpleType attrType, Object value) {
    if (value == null) {
      return null;
    }
    else {
      if (attrType instanceof DateTimeType) {
        return new java.util.Date(((DateObject) value).getTime());
      }
      else if (attrType instanceof QNameType) {
        return ((QName) value).getName();
      }
      else if (attrType instanceof DecimalType) {
        return value.toString();
      }
      else {
        return value;
      }
    }
  }

  /**
   * Writes rowModel's attributes to the record (structure or record's cuurent row).
   * For each attribute the destination column is the one having the
   * same name as the attribute.
   * @param record    the record to write to
   * @param rowModel the Model having the attributes to be written
   */
  public void writeRecordOld(JCO.Record record, int colCount, Model rowModel, String[] keyCols) {
    QName colID;
    int attrIndex;
    ComplexType rowType = (ComplexType) rowModel.type();
    Object value;
    int keyColCount = keyCols.length;
    String colName;
    Namespace ns = rowType.name().getNamespace();
    SimpleType attrType;

    for (int i = 0; i < keyCols.length; i++) {
      record.setValue(keyCols[i], i);
    }
    for (int i = keyColCount; i < colCount; i++) {
      colName = record.getName(i);
      colID = ns.getQName(colName);
      attrIndex = rowType.indexOfAttribute(colID);
      if (attrIndex < 0) {
        throw new IllegalArgumentException("No attribute for jCo record column name=" + colID.getName());
      }
      value = rowModel.get(colID);
      attrType = rowType.getAttributeType(attrIndex);
      value = value2jco(attrType, value);
      record.setValue(value, i);
    }
  }

  /**
   * Writes rowModel's attributes to the record (structure or record's current row).
   * For each attribute the destination column is the one having the
   * same name as the attribute.
   * Record may have more columns than rowModel, but must have a column for
   * each attribute of rowModel.
   * @param record    the record to write to
   * @param rowModel the Model having the attributes to be written
   */
  public void writeRecord(JCO.Record record, Model rowModel) {
    ComplexType recordType = (ComplexType) rowModel.type();
    int attrCount = recordType.attributeCount();
    for (int i = 0; i < attrCount; i++ ) {
      SimpleType attrType = recordType.getAttributeType(i);
      QName attrName = recordType.getAttributeName(i);
      String colName = attrName.getName();
      if (record.hasField( colName )) {
        Object value = value2jco( attrType, rowModel.get(attrName) );
        // die hasField() funktioniert nicht oder nicht immer, deshalb fangen wir die Exception
        try {
          record.setValue( value, colName );
        }
        catch (Exception ex) {
          Logger.debug("JCO exception write column name="+colName+", msg="+ex.getMessage());
          value = null;
        }
      }
      else {
        Logger.debug("JCO unknown column="+colName);
      }
    }
  }


  /**
   * Writes all rows of the given rowIter to binding. The row Models are written
   * by calling writeTableRow(binding, rowModel)
   * @param table   the tabel to be read
   * @param rowIter an iterator over all row Models to be written to binding
   */
  public void writeTableOld(JCO.Table table, ModelIterator rowIter, String[] keyCols) {
    Model rowModel;
    int colCount = table.getNumColumns();
    while (rowIter.hasNext()) {
      rowModel = rowIter.next();
      table.appendRow();
      writeRecordOld(table, colCount, rowModel, keyCols);
    }
  }

  /**
   * Writes all children of parameter to function's corresponding binding parameters.
   * The child's relationName defines the JCO binding to be used.
   * This method is invoked recursively for each row having child rows.
   * @param function  the function to which the binding rows are written
   * @param parameter the model containing the rows to be written
   */
  public void writeTable(JCO.Function function, String tableName, Model parameter, Stack keyColNames, Stack keyValues) {
    JCO.Table table;
    if (parameter == null) {
      Logger.debug( "Write table called, table=null" );
    }
    else {
      Logger.debug( "Write table called, child count=" + parameter.childCount( null ) );
      for (ModelIterator rowIter = parameter.iterator( null, null ); rowIter.hasNext(); ) {
        Model rowModel = rowIter.next();
        ComplexType rowType = (ComplexType) rowModel.type();
        table = fetchTable( function, tableName );
        table.appendRow();

        Logger.debug( "JCO write 1 row to table name=" + table.getName() );

        for (int i = 0; i < keyColNames.size(); i++) {
          String colName = (String) keyColNames.elementAt( i );
          table.setValue( keyValues.elementAt( i ), colName );
        }
        writeRecord( table, rowModel );

        //----- Write rowModel's children if existing
        if (rowModel.childCount( null ) > 0) {
          int keyCount = rowType.keyAttributeCount();
          for (int i = 0; i < keyCount; i++) {
            String colName = rowType.getKeyAttribute( i ).getName();
            keyColNames.push( colName );
            keyValues.push( table.getValue( colName ) );
          }
          writeTable( function, rowModel.getParentRelationID().getName(), rowModel, keyColNames, keyValues );
          for (int i = 0; i < keyCount; i++) {
            keyColNames.pop();
            keyValues.pop();
          }
        }
      }
    }
  }
}
