/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.sap;

import java.util.Vector;

public class SapTextLine {
  public static final String NEWLINE = "\n";
  public static final String SURFIXSAPTEXT  = "/ ";
  Vector textlines = new Vector();

  public SapTextLine() {
    this.textlines = new Vector();
  }

  public SapTextLine(String text) {
    textlines = makeTextLineVector(text);
  }

  public static Vector makeTextLineVector (String text) {
    StringBuffer buffer;
    int pos;
    Vector resultTextLines = new Vector();
    while (text.indexOf(NEWLINE) != -1) {
      pos = text.indexOf(NEWLINE);
      buffer = new StringBuffer();
      buffer.append(text.substring(0, pos));
      text = text.substring(pos + 1, text.length());
      if (buffer.length() < 68) {
        resultTextLines.add(buffer.insert(0,SURFIXSAPTEXT).toString());
      }
      else {
        cutToLines(resultTextLines, buffer);
      }
    }

    buffer = new StringBuffer(text);
    if (buffer.length() < 68) {
      resultTextLines.add(buffer.insert(0,SURFIXSAPTEXT).toString());
    }
    else {
      cutToLines(resultTextLines, buffer);
    }
    return resultTextLines;
  }

  public static void cutToLines(Vector textline, StringBuffer buffer){
    StringBuffer tempBuffer = new StringBuffer(SURFIXSAPTEXT);
    String temp = buffer.toString();
    int pos;
    while(temp.indexOf(" ") != -1) {
      pos = temp.indexOf(" ");
      if (tempBuffer.length() + pos >= 70){
        textline.add(tempBuffer.toString());
        tempBuffer = new StringBuffer(SURFIXSAPTEXT);
      }
      tempBuffer.append(temp.substring(0, pos)).append(" ");
      temp = temp.substring(pos + 1, temp.length());
    }
    tempBuffer.append(temp);
    textline.add(tempBuffer.toString());
  }

  public String getOneString() {
    String result;
    StringBuffer buffer = new StringBuffer();
    String line;
    for (int i = 0; i < textlines.size(); i++) {
      line = (String) textlines.get(i);
      if (line.length() > 2) {
        String temp = line.substring(2);
        buffer.append(temp);
      }
      buffer.append(NEWLINE);
    }
    result = buffer.toString();
    return result;
  }

  public int lineCount() {
    return textlines.size();
  }

  public String get(int index) {
    return (String) textlines.get(index);
  }

  public void add(String s) {
    textlines.add(s);
  }
}