/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.sapjco3;

import com.sap.conn.jco.*;
import com.sap.conn.jco.server.*;
import de.pidata.log.Logger;

public abstract class JCoServerAdapter implements JCoServerFunctionHandler {

  public static final String STFC_CONNECTION = "STFC_CONNECTION";
  private JCoServer jCoServer;

  public JCoServerAdapter( String serverName ) {

    DefaultServerHandlerFactory.FunctionHandlerFactory factory;
    try {
      jCoServer = JCoServerFactory.getServer( serverName );
      JCoCustomRepository repository = createRepository();
      if (repository != null) {
        jCoServer.setRepository( repository );
      }
      factory = new DefaultServerHandlerFactory.FunctionHandlerFactory();
      factory.registerHandler( STFC_CONNECTION, this );
      addFunctions( factory );
      jCoServer.setCallHandlerFactory( factory );
    }
    catch(Exception ex) {
      throw new RuntimeException("Unable to create the server " + serverName + " because of " + ex.getMessage(), ex);
    }
  }

  /**
   * Create Repository for custom functions
   * @return
   */
  protected abstract JCoCustomRepository createRepository();

  /**
   * Add methods implemented by this Server via "factory.registerHandler( functionName, this );
   * @param factory the factory to add functions to
   */
  protected abstract void addFunctions( DefaultServerHandlerFactory.FunctionHandlerFactory factory );

  public void start() {
    jCoServer.start();
  }

  public void stop() {
    jCoServer.stop();
  }

  protected void doSTFC_CONNECTION( JCoServerContext serverCtx, JCoFunction function ) {
    Logger.info( "  STFC_CONNECTION gwhost=" + serverCtx.getServer().getGatewayHost()
        + ",gwserv=" + serverCtx.getServer().getGatewayService() + ",progid=" + serverCtx.getServer().getProgramID() );
    Logger.info( "  STFC_CONNECTION attributes=" + serverCtx.getConnectionAttributes().toString() );
    Logger.info( "  STFC_CONNECTION CPIC conversation ID=" + serverCtx.getConnectionAttributes().getCPICConversationID() );
    Logger.info( "  STFC_CONNECTION req text=" + function.getImportParameterList().getString( "REQUTEXT" ));
    function.getExportParameterList().setValue( "ECHOTEXT", function.getImportParameterList().getString( "REQUTEXT" ) );
    function.getExportParameterList().setValue( "RESPTEXT", "Hallo vom PI-Data Mobil-Server" );
  }

  public final void handleRequest( JCoServerContext jCoServerContext, JCoFunction jCoFunction ) throws AbapException, AbapClassException {
    String functionName = jCoFunction.getName();
    Logger.info( "Call to SAP Function " + functionName + ",programID=" + jCoServer.getProgramID()
        + ",ConnectionId=" + jCoServerContext.getConnectionID() + ",SessionId=" + jCoServerContext.getSessionID()
        + ",TID=" + jCoServerContext.getTID() + ", repositoryName=" + jCoServerContext.getRepository().getName() );
    if (functionName.equals( STFC_CONNECTION )) {
      doSTFC_CONNECTION( jCoServerContext, jCoFunction );
    }
    else {
      doHandleRequest( jCoServerContext, jCoFunction );
    }
  }

  protected abstract void doHandleRequest( JCoServerContext jCoServerContext, JCoFunction jCoFunction ) throws AbapException, AbapClassException;
}
