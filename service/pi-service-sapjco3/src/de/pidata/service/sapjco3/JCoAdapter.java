/*
 * This file is part of PI-Mobile core (https://gitlab.com/pi-mobile/pi-mobile-core).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.service.sapjco3;

import com.sap.conn.jco.*;
import de.pidata.log.Logger;
import de.pidata.qnames.NamespaceTable;
import de.pidata.service.base.ServiceException;
import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.Type;
import de.pidata.models.types.simple.*;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.service.sapjco3dest.JCoDestinationDataProvider;
import de.pidata.system.base.SystemManager;

import java.util.Stack;

public class JCoAdapter {

  private static String destinationName = null;

  protected JCoAdapter() {
    SystemManager sm = SystemManager.getInstance();
    if (destinationName == null) {
      JCoDestinationDataProvider.initialize( sm.getProperty( "sap.poolname", null ),
          sm.getPropertyInt( "sap.maxConnection", 100 ), sm.getProperty( "sap.mandant", null ),
          sm.getProperty( "sap.user", null ), sm.getProperty( "sap.password", null ),
          sm.getProperty( "sap.language", null ), sm.getProperty( "sap.system", null ),
          sm.getProperty( "sap.systemNr", null ) );
      destinationName = sm.getProperty( "sap.poolname", null );
    }
  }

  public JCoDestination getSapConnection() {
    JCoDestination connection = null;
    try {
      Logger.debug(">>>>> Before JCoDestinationManager.getDestination()");
      connection = JCoDestinationManager.getDestination( destinationName );
      // TODO gibt es nicht mehr bei jCo3?: connection.setAbapDebug( false );
      Logger.debug("<<<<< After JCoDestinationManager.getDestination");
    }
    catch (JCoException ex) {
      String msg = "Exception while getting connection '" + destinationName;
      Logger.error(msg, ex);
      throw new IllegalArgumentException(msg + "', msg=" + ex.getMessage());
    }
    return connection;
  }

  public void closeSapConnection(JCoDestination connection) {
    Logger.debug( ">>>>> Before JCO.releaseClient()" );
    //TODO nicht mehr nötig bei JCo3?: JCO.releaseClient(connection);
    Logger.debug("<<<<< After JCO.releaseClient()");
  }

  public JCoRepository getRepository (JCoDestination connection) throws JCoException {
    return getSapConnection().getRepository();
  }

  /**
   * Creates function for name by using repository
   * @param name the function name
   * @return the function
   * @throws IllegalArgumentException if an exception occured while creation function
   */
  public JCoFunction createFunction(String name, JCoDestination connection) {
    try {
      JCoRepository repository = getRepository(connection);
      String fktName = name.toUpperCase();
      JCoFunctionTemplate template= repository.getFunctionTemplate( fktName );
      if (template == null) {
        Logger.warn( "Function template not found for name="+fktName );
        return repository.getFunction( fktName );
      }
      else {
        return template.getFunction();
      }
    }
    catch (Exception ex) {
      String msg = "Exception while creating function '" + name;
      Logger.error(msg, ex);
      throw new IllegalArgumentException(msg + "', msg=" + ex.getMessage());
    }
  }

  /**
   * Returns binding tableName from functions's TableParameterList
   * @param function  the function from which the binding is returned
   * @param tableName the binding's name
   * @return binding tableName from functions's TableParameterList
   * @throws IllegalArgumentException if tableName does not exist for function
   */
  public JCoTable fetchTable(JCoFunction function, String tableName) {
    JCoParameterList tableList = function.getTableParameterList();
    if (tableList == null) {
      throw new IllegalArgumentException("Function '" + function.getName() + "': TableParamterList is null");
    }
    JCoTable table = tableList.getTable( tableName );
    if (table == null) {
      throw new IllegalArgumentException("Function '" + function.getName() + "': Table '"
                                         + tableName + "' not found in repository");
    }
    return table;
  }

  /**
   * Returns structure structureName from functions's ImportParameterList
   * @param function  the function from which the structure is returned
   * @param structureName the structure's name
   * @return structure structureName from functions's ImportParameterList
   * @throws IllegalArgumentException if structureName does not exist
   */
  public JCoStructure fetchInStructure(JCoFunction function, String structureName) {
    JCoStructure structure = function.getImportParameterList().getStructure(structureName);
    if (structure == null) {
      throw new IllegalArgumentException("Function '" + function.getName() + "': import structure '"
                                         + structureName + "' not found in repository");
    }
    return structure;
  }

  /**
   * Returns structure structureName from functions's ExportParameterList
   * @param function  the function from which the structure is returned
   * @param structureName the structure's name
   * @return structure structureName from functions's ExportParameterList
   * @throws IllegalArgumentException if structureName does not exist
   */
  public JCoStructure fetchOutStructure(JCoFunction function, String structureName) {
    JCoStructure structure = function.getExportParameterList().getStructure(structureName);
    if (structure == null) {
      throw new IllegalArgumentException("Function '" + function.getName() + "': export structure '"
                                         + structureName + "' not found in repository");
    }
    return structure;
  }

  /**
   * Tries to execute function using connection
   * @param function the function to be executed
   */
  public void execute(JCoFunction function, JCoDestination connection) throws ServiceException {
    try {
      function.execute(connection);
    }
    catch (Exception ex) {
      Logger.error("Can't execute function: " + function.getName(), ex);
      throw new ServiceException(ServiceException.SERVICE_FAILED, "Can't execute function: " + function.getName(), ex);
    }
  }

  /**
   * Reads value for fieldName from JCo record
   * @param record    the record to read from
   * @param fieldName the filed to read from
   * @param attrType  the result type
   * @param ns        the namespace to use for QNames
   * @return the value for fieldName from JCo record
   */
  public Object readFromRecord(JCoRecord record, String fieldName, SimpleType attrType, Namespace ns) {
    Object value = null;
    if (record.getMetaData().hasField( fieldName )) {
      try {
        value = record.getValue(fieldName);
      }
      catch (Exception ex) {
        value = null;
      }
      if (value != null) {
        if (attrType instanceof DateTimeType) {
          value = new DateObject( ((DateTimeType) attrType).getType(), ((java.util.Date) value).getTime() );
        }
        else if (attrType instanceof QNameType) {
          value = ns.getQName(value.toString());
        }
        else if (attrType instanceof IntegerType) {
          //BigDecimal temp = new BigDecimal((String) value);
          value = new Integer(record.getInt(fieldName));
        }
        else if (attrType instanceof DecimalType) {
          value = new DecimalObject(record.getString(fieldName));
        }
      }
    }
    return value;
  }

  public Key readRecord( JCoRecord record, ComplexType recordType, Object[] attributes, NamespaceTable nsTable ) {
    int attrCount = recordType.attributeCount();
    Namespace  ns = recordType.name().getNamespace();
    int        keyAttrCount = recordType.keyAttributeCount();
    boolean    simpleKey = false;
    Object[]   keyAttrs = null;
    Key        key = null;
    int        keyIndex;
    ChildList  children = null;

    //----- read attributes and create Key
    if (keyAttrCount == 1) {
      simpleKey = true;
    }
    else if (keyAttrCount > 0) {
      keyAttrs = new Object[keyAttrCount];
    }
    for (int i = 0; i < attrCount; i++ ) {
      SimpleType attrType = recordType.getAttributeType(i);
      QName attrName = recordType.getAttributeName(i);
      Object value = readFromRecord( record, attrName.getName(), attrType, nsTable.getDefaultNamespace() );
      keyIndex = recordType.getKeyIndex(attrName);
      if (keyIndex >= 0) {
        if (simpleKey) {
          if (attrType instanceof QNameType) key = (QName) value;
          else key = new SimpleKey( value );
        }
        else {
          keyAttrs[keyIndex] = value;
        }
      }
      else {
        attributes[i] = value;
      }
    }
    if (keyAttrs != null) {
      key = new CombinedKey( keyAttrs );
    }
    return key;
  }

  /**
   * Reads
   * @param tableParams
   * @param rowType
   * @return
   */
  public void readTable(JCoParameterList tableParams, QName tableName, ChildList rowList, ComplexType rowType, QName relationName, Stack keyColNames, Stack keyValues, NamespaceTable nsTable ) {
    ModelFactory factory;
    JCoTable table = tableParams.getTable( tableName.getName() );
    int rowCount = table.getNumRows();
    Logger.info("JCO read table '"+tableName.getName()+"', rowCount="+rowCount);
    boolean acceptKey;

    for (int row = 0; row < rowCount; row++) {
      table.setRow(row);
      acceptKey = true;
      for (int i = 0; i< keyColNames.size(); i++) {
        String fieldName = (String) keyColNames.elementAt(i);
        Object value = table.getValue(fieldName);
        if (!value.equals( keyValues.elementAt(i)) ) {
          acceptKey = false;
          break;
        }
      }
      if (acceptKey) {
        //----- Read attributes and key
        Object[] attributes = new Object[ rowType.attributeCount() ];
        Key key = readRecord( table, rowType, attributes, nsTable );

        //----- Read/create children
        int keyCount = rowType.keyAttributeCount();
        for (int i = 0; i < keyCount; i++) {
          String colName = rowType.getKeyAttribute(i).getName();
          keyColNames.push( colName );
          keyValues.push( table.getValue(colName) );
        }
        ChildList children = new ChildList();
        for (QNameIterator childRelIter = rowType.relationNames(); childRelIter.hasNext(); ) {
          QName relName = childRelIter.next();
          Type type = rowType.getChildType(relName);
          if (tableParams.getMetaData().hasField( relName.getName() )) {
            readTable( tableParams, relName, children, (ComplexType) type, relName, keyColNames, keyValues, nsTable );
          }  
        }
        for (int i = 0; i < keyCount; i++) {
          keyColNames.pop();
          keyValues.pop();
        }

        //----- Create result model
        factory = ModelFactoryTable.getInstance().getFactory( rowType.name().getNamespace() );
        Model rowModel = factory.createInstance(key, rowType, attributes, null, children);
        rowList.add(relationName, rowModel);
      }
    }
  }

  /**
   * Converts value to the corresponding JCo format
   * @param attrType value's type
   * @param value    the value to be converted
   * @return the value converted to JCo format
   */
  public Object value2jco(SimpleType attrType, Object value) {
    if (value == null) {
      return null;
    }
    else {
      if (attrType instanceof DateTimeType) {
        return new java.util.Date(((DateObject) value).getTime());
      }
      else if (attrType instanceof QNameType) {
        return ((QName) value).getName();
      }
      else if (attrType instanceof DecimalType) {
        return value.toString();
      }
      else {
        return value;
      }
    }
  }

  /**
   * Writes rowModel's attributes to the record (structure or record's current row).
   * For each attribute the destination column is the one having the
   * same name as the attribute.
   * Record may have more columns than rowModel, but must have a column for
   * each attribute of rowModel.
   * @param record    the record to write to
   * @param rowModel the Model having the attributes to be written
   */
  public void writeRecord(JCoRecord record, Model rowModel) {
    ComplexType recordType = (ComplexType) rowModel.type();
    int attrCount = recordType.attributeCount();
    for (int i = 0; i < attrCount; i++ ) {
      SimpleType attrType = recordType.getAttributeType(i);
      QName attrName = recordType.getAttributeName(i);
      String colName = attrName.getName();
      if (record.getMetaData().hasField( colName )) {
        Object value = value2jco( attrType, rowModel.get(attrName) );
        try {
          record.setValue( colName, value );
        }
        catch (Exception ex) {
          Logger.debug("JCO exception write column name="+colName+", msg="+ex.getMessage());
          value = null;
        }
      }
      else {
        Logger.debug("JCO unknown column="+colName);
      }
    }
  }

  /**
   * Writes one row to table of function.
   * The child's relationName defines the JCO binding to be used.
   * This method is invoked recursively for each row having child rows.
   * @param function  the function to which the binding rows are written
   * @param rowModel the row to be written
   */
  public void writeInputRecord( JCoFunction function, String recordName, Model rowModel ) {
    JCoStructure structure = function.getImportParameterList().getStructure( recordName );
    Logger.debug( "JCO write 1 row from " + rowModel.type().name() + " to table name=" + recordName );
    writeRecord( structure, rowModel );
  }

  /**
   * Writes all children of parameter to function's corresponding binding parameters.
   * The child's relationName defines the JCO binding to be used.
   * This method is invoked recursively for each row having child rows.
   * @param function  the function to which the binding rows are written
   * @param rowIter the modelIterator containing the rows to be written
   */
  public void writeTable(JCoFunction function, String tableName, ModelIterator rowIter, Stack keyColNames, Stack keyValues) {
    JCoTable table;
    if (rowIter == null) {
      Logger.debug( "Write table called, table=null" );
    }
    else {
      Logger.debug( "Write table called, rowIter=" + rowIter );
      while ( rowIter.hasNext() ) {
        Model rowModel = rowIter.next();
        ComplexType rowType = (ComplexType) rowModel.type();
        table = fetchTable( function, tableName );
        table.appendRow();

        Logger.debug( "JCO write 1 row from " + rowModel.type().name() + " to table name=" + tableName );

        for (int i = 0; i < keyColNames.size(); i++) {
          String colName = (String) keyColNames.elementAt( i );
          table.setValue( colName, keyValues.elementAt( i ) );
        }
        writeRecord( table, rowModel );

        //----- Write rowModel's children if existing
        if (rowModel.childCount( null ) > 0) {
          int keyCount = rowType.keyAttributeCount();
          for (int i = 0; i < keyCount; i++) {
            String colName = rowType.getKeyAttribute( i ).getName();
            keyColNames.push( colName );
            keyValues.push( table.getValue( colName ) );
          }
          for (QNameIterator childRelationIter = rowType.relationNames(); childRelationIter.hasNext();) {
            QName childRelationName = childRelationIter.next();
            writeTable( function, childRelationName.getName(), rowModel.iterator( childRelationName, null ), keyColNames, keyValues );
          }
          for (int i = 0; i < keyCount; i++) {
            keyColNames.pop();
            keyValues.pop();
          }
        }
      }
    }
  }
}
