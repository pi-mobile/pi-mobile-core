// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)
// Mon Mar 30 14:48:23 CEST 2009

package de.pidata.service.storage;

import de.pidata.comm.soap.Body;
import de.pidata.service.base.ServiceException;
import de.pidata.models.tree.Context;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

public interface IStorageService {

  public static final String SERVICE = "StorageService";

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.pidata.de/res/storage.xsd" );

  public static final QName OP_READ = NAMESPACE.getQName("read");
  public static final QName OP_EXISTS = NAMESPACE.getQName("exists");
  public static final QName OP_FILEINFO = NAMESPACE.getQName("fileinfo");
  public static final QName OP_LIST = NAMESPACE.getQName("list");
  public static final QName OP_WRITE = NAMESPACE.getQName("write");
  public static final QName OP_DELETE = NAMESPACE.getQName("delete");
  public static final QName OP_RENAME = NAMESPACE.getQName("rename");
  public static final QName OP_INSTALL = NAMESPACE.getQName("install");

  public static final QName PATH      = NAMESPACE.getQName( "path" );
  public static final QName NEWNAME   = NAMESPACE.getQName( "newName" );
  public static final QName FILE      = NAMESPACE.getQName( "file" );
  public static final QName SYSTEMID  = NAMESPACE.getQName( "systemID" );
  public static final QName CHUNK     = NAMESPACE.getQName( "chunk" );
  public static final QName EXISTS    = NAMESPACE.getQName( "exists" );
  public static final QName LIST      = NAMESPACE.getQName( "list" );
  public static final QName OFFSET    = NAMESPACE.getQName( "offset" );
  public static final QName PROGRAM   = NAMESPACE.getQName( "program" );

  public static final int BLOCK_SIZE = 4096;

  public Body read( Context caller, String systemid, String path, long offset ) throws ServiceException;

  public Body exists( Context caller, String systemid, String path ) throws ServiceException;

  public Body fileinfo( Context caller, String systemid, String path ) throws ServiceException;

  public Body list( Context caller, String systemid, String path ) throws ServiceException;

  public void write( Context caller, FileChunk chunk ) throws ServiceException;

  public void delete( Context caller, String systemid, String path ) throws ServiceException;

  public void rename( Context caller, String systemid, String path, String newname ) throws ServiceException;

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
