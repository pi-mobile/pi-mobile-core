// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)
// Mon Mar 30 13:56:13 CEST 2009

package de.pidata.service.storage;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.SimpleKey;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.BooleanType;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.models.types.simple.IntegerType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.Hashtable;

public class FileInfo extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.pidata.de/res/storage.xsd" );

  public static final QName ID_STATE = NAMESPACE.getQName("state");
  public static final QName ID_MODIFIED = NAMESPACE.getQName("modified");
  public static final QName ID_NAME = NAMESPACE.getQName("name");
  public static final QName ID_CREATED = NAMESPACE.getQName("created");
  public static final QName ID_SIZE = NAMESPACE.getQName("size");
  public static final QName ID_FILELIST = NAMESPACE.getQName("fileList");
  public static final QName ID_CHECKSUM = NAMESPACE.getQName("checksum");
  public static final QName ID_DIRECTORY = NAMESPACE.getQName("directory");

  public static final ComplexType TYPE;

  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "fileInfo" ), FileInfo.class.getName(), 1 );
    TYPE = type;
    type.addKeyAttributeType( 0, ID_NAME, StringType.getDefString());
    type.addAttributeType( ID_SIZE, IntegerType.getDefLong());
    type.addAttributeType( ID_DIRECTORY, BooleanType.getDefault());
    type.addAttributeType( ID_CREATED, DateTimeType.getDefDateTime());
    type.addAttributeType( ID_MODIFIED, DateTimeType.getDefDateTime());
    type.addAttributeType( ID_CHECKSUM, IntegerType.getDefInt());
    type.addAttributeType( ID_STATE, StringType.getDefString());
    type.addRelation( ID_FILELIST, FileList.TYPE, 0, 1);
  }

  public FileInfo( Key id ) {
    super( id, TYPE, null, null, null );
  }

  public FileInfo(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, TYPE, attributeNames, anyAttribs, childNames);
  }

  protected FileInfo(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the attribute name.
   *
   * @return The attribute name
   */
  public String getName() {
    return (String) get( ID_NAME );
  }

  /**
   * Returns the attribute size.
   *
   * @return The attribute size
   */
  public Long getSize() {
    return (Long) get( ID_SIZE );
  }

  /**
   * Set the attribute size.
   *
   * @param size new value for attribute size
   */
  public void setSize( Long size ) {
    set( ID_SIZE, size );
  }

  /**
   * Returns the attribute directory.
   *
   * @return The attribute directory
   */
  public Boolean getDirectory() {
    return (Boolean) get( ID_DIRECTORY );
  }

  /**
   * Set the attribute directory.
   *
   * @param directory new value for attribute directory
   */
  public void setDirectory( Boolean directory ) {
    set( ID_DIRECTORY, directory );
  }

  /**
   * Returns the attribute created.
   *
   * @return The attribute created
   */
  public DateObject getCreated() {
    return (DateObject) get( ID_CREATED );
  }

  /**
   * Set the attribute created.
   *
   * @param created new value for attribute created
   */
  public void setCreated( DateObject created ) {
    set( ID_CREATED, created );
  }

  /**
   * Returns the attribute modified.
   *
   * @return The attribute modified
   */
  public DateObject getModified() {
    return (DateObject) get( ID_MODIFIED );
  }

  /**
   * Set the attribute modified.
   *
   * @param modified new value for attribute modified
   */
  public void setModified( DateObject modified ) {
    set( ID_MODIFIED, modified );
  }

  /**
   * Returns the attribute checksum.
   *
   * @return The attribute checksum
   */
  public Integer getChecksum() {
    return (Integer) get( ID_CHECKSUM );
  }

  /**
   * Set the attribute checksum.
   *
   * @param checksum new value for attribute checksum
   */
  public void setChecksum( Integer checksum ) {
    set( ID_CHECKSUM, checksum );
  }

  /**
   * Returns the attribute state.
   *
   * @return The attribute state
   */
  public String getState() {
    return (String) get( ID_STATE );
  }

  /**
   * Set the attribute state.
   *
   * @param state new value for attribute state
   */
  public void setState( String state ) {
    set( ID_STATE, state );
  }

  /**
   * Returns the attribute fileList.
   *
   * @return The attribute fileList
   */
  public FileList getFileList() {
    return (FileList) get( ID_FILELIST, null );
  }

  /**
   * Set the attribute fileList.
   *
   * @param fileList new value for attribute fileList
   */
  public void setFileList( FileList fileList ) {
    if (childCount( ID_FILELIST ) > 0) {
      removeAll( ID_FILELIST );
    }
    add( ID_FILELIST, fileList );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  public FileInfo( String fileName ) {
    this( new SimpleKey( fileName ) );
  }

}
